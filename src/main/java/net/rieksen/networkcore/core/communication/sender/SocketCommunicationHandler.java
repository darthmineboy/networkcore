package net.rieksen.networkcore.core.communication.sender;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.logging.Logger;

import org.apache.commons.lang.RandomStringUtils;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import net.rieksen.networkcore.core.communication.IServerCommunicationListener;
import net.rieksen.networkcore.core.communication.IServerCommunicationSender;
import net.rieksen.networkcore.core.communication.ServerCommunication;
import net.rieksen.networkcore.core.server.LocalServerImpl;
import net.rieksen.networkcore.core.server.Server;
import net.rieksen.networkcore.core.server.ServerModule;

/**
 * Is capable of the sending and receiving of {@link ServerCommunication}
 * through sockets.
 * 
 * @author Darthmineboy
 */
@Deprecated
public class SocketCommunicationHandler implements IServerCommunicationSender
{

	private boolean								enabled;
	private ServerSocket						serverSocket;
	private final IServerCommunicationListener	listener;
	private ServerModule						serverModuleImpl;
	private Logger								logger;

	public SocketCommunicationHandler(IServerCommunicationListener listener, ServerModule serverModuleImpl, Logger logger)
	{
		this.listener = listener;
		this.serverModuleImpl = serverModuleImpl;
		this.logger = logger;
	}

	@Override
	public void disable()
	{
		this.enabled = false;
		try
		{
			this.serverSocket.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Should be called from another thread.
	 */
	public void init()
	{
		this.enabled = true;
		try
		{
			this.serverSocket = new ServerSocket();
			String host = this.getHost();
			this.serverSocket.bind(new InetSocketAddress(host, 0));
			// NetworkCoreAPI.getLogger().info("Socket started on " + host + ":"
			// + this.serverSocket.getLocalPort());
			Server server = Server.getLocalServer();
			server.changeSocketHost(host);
			server.changeSocketPort(this.serverSocket.getLocalPort());
			server.changeSocketKey(RandomStringUtils.randomAscii(64));
			while (this.enabled)
			{
				Socket socket = null;
				try
				{
					socket = this.serverSocket.accept();
				} catch (SocketException e)
				{
					// silence for when the socket is closed
					continue;
				}
				new Thread(new Runnable()
				{

					private Socket socket;

					@Override
					public void run()
					{
						try
						{
							this.socket.setSoTimeout(5000);
							InputStream in = this.socket.getInputStream();
							DataInputStream msgin = new DataInputStream(in);
							String key;
							try
							{
								key = msgin.readUTF();
							} catch (Exception e)
							{
								// silence
								this.socket.close();
								return;
							}
							Server server = Server.getLocalServer();
							if (!server.getSocketKey().equals(key))
							{
								// potentionally an issue
								SocketCommunicationHandler.this.logger.warning("Blocked socket connection from "
									+ this.socket.getInetAddress().getHostAddress() + " - invalid key presented");
								this.socket.close();;
								return;
							}
							short len = msgin.readShort();
							int recipientServer = msgin.readInt();
							int pluginId = msgin.readInt();
							int action = msgin.readInt();
							byte[] data = new byte[len - 12];
							msgin.readFully(data);
							// end data
							ServerCommunication communication = new ServerCommunication(null, recipientServer,
								Server.getLocalServer().getServerId(), pluginId, action, data);
							SocketCommunicationHandler.this.listener.onServerCommunicationReceived(communication);
							this.socket.close();
						} catch (Exception e)
						{
							e.printStackTrace();
						}
					}

					private Runnable init(Socket socket)
					{
						this.socket = socket;
						return this;
					}
				}.init(socket)).start();
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public boolean sendServerCommunication(ServerCommunication communication)
	{
		if (communication.getTargetServer() == null)
		{
			for (Server server : this.serverModuleImpl.getServers())
			{
				if (server instanceof LocalServerImpl)
				{
					continue;
				}
				this.sendServerCommunication(server.getServerId(), communication);
			}
			// always returns true
			return true;
		} else
		{
			return this.sendServerCommunication(communication.getTargetServer(), communication);
		}
	}

	private String getHost() throws Exception
	{
		Enumeration<NetworkInterface> ns = NetworkInterface.getNetworkInterfaces();
		while (ns.hasMoreElements())
		{
			NetworkInterface n = ns.nextElement();
			Enumeration<InetAddress> ad = n.getInetAddresses();
			while (ad.hasMoreElements())
			{
				InetAddress a = ad.nextElement();
				if (!(a instanceof Inet4Address))
				{
					continue;
				}
				if (!a.isReachable(1000))
				{
					continue;
				}
				return a.getHostAddress();
			}
		}
		throw new IllegalStateException("Could not find host for socket to bind on");
	}

	private boolean sendServerCommunication(int serverId, ServerCommunication communication)
	{
		try
		{
			Server server = Server.getServer(serverId);
			if (!server.isOnline()) { return false; }
			String host = server.getSocketHost();
			if (host == null) { return false; }
			int port = server.getSocketPort();
			if (port == 0) { return false; }
			String key = server.getSocketKey();
			if (key == null) { return false; }
			Socket socket = new Socket();
			try
			{
				socket.connect(new InetSocketAddress(host, port), 5000);
			} catch (Exception e)
			{
				// silence connection failure
				socket.close();
				return false;
			}
			ByteArrayDataOutput out = ByteStreams.newDataOutput();
			ByteArrayOutputStream msgbytes = new ByteArrayOutputStream();
			DataOutputStream msgout = new DataOutputStream(msgbytes);
			// data
			msgout.writeInt(communication.getRecipientServer());
			msgout.writeInt(communication.getPluginId());
			msgout.writeInt(communication.getAction());
			msgout.write(communication.getData());
			// end data
			out.writeUTF(key);
			out.writeShort(msgbytes.toByteArray().length);
			out.write(msgbytes.toByteArray());
			socket.getOutputStream().write(out.toByteArray());
			socket.close();
			return true;
		} catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
}
