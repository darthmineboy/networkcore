package net.rieksen.networkcore.core.communication;

/**
 * @author Darthmineboy
 */
public enum CommunicationAction
{
	BROADCAST(1),
	BROADCAST_WITH_PERMISSION(2),
	SEND_USER_MESSAGE(3),
	BROADCAST_MESSAGE(4),
	BROADCAST_MESSAGE_WITH_PERMISSION(5),
	// SERVER_START_EVENT(6),
	SEND_USER_MESSAGE_WITH_PERMISSION(7),
	SEND_USER_TO_SERVER(8);

	private final int action;

	CommunicationAction(int action)
	{
		this.action = action;
	}

	public final int getAction()
	{
		return this.action;
	}
}
