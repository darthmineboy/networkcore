package net.rieksen.networkcore.core.communication;

@Deprecated
public interface IServerCommunicationSender
{

	public void disable();

	/**
	 * Send a server communication. This does not guarantee the actual reception
	 * and processing of the message. If this verification is needed, you could
	 * implement some form of response/callback.
	 * 
	 * @param communication
	 * @return true when successful
	 */
	public boolean sendServerCommunication(ServerCommunication communication);
}
