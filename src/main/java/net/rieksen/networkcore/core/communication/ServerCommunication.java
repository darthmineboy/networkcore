package net.rieksen.networkcore.core.communication;

import org.apache.commons.lang.Validate;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

/**
 * @author Darthmineboy
 */
@Deprecated
public class ServerCommunication
{

	protected final int					recipientServer;
	protected final Integer				targetServer;
	protected final int					pluginId;
	protected final int					action;
	protected byte[]					data;
	private ServerCommunicationManager	communicationManager;

	public ServerCommunication(ServerCommunicationManager communicationManager, int recipientServer, Integer targetServer, int pluginId,
		int action, byte[] data)
	{
		Validate.notNull(recipientServer);
		Validate.notNull(pluginId);
		this.communicationManager = communicationManager;
		this.recipientServer = recipientServer;
		this.targetServer = targetServer;
		this.pluginId = pluginId;
		this.action = action;
		this.data = data;
	}

	public ServerCommunication(ServerCommunicationManager communicationManager, int recipientServer, Integer targetServer, int pluginId,
		int action, String data)
	{
		Validate.notNull(recipientServer);
		Validate.notNull(pluginId);
		this.communicationManager = communicationManager;
		this.recipientServer = recipientServer;
		this.targetServer = targetServer;
		this.pluginId = pluginId;
		this.action = action;
		this.setData(data);
	}

	public ServerCommunication(ServerCommunicationManager communicationManager, int recipientServer, Integer targetServer, int pluginId,
		int action, String[] data)
	{
		Validate.notNull(recipientServer);
		Validate.notNull(pluginId);
		this.communicationManager = communicationManager;
		this.recipientServer = recipientServer;
		this.targetServer = targetServer;
		this.pluginId = pluginId;
		this.action = action;
		this.setData(data);
	}

	/**
	 * Get the action which could represent an operation, with expected data.
	 * 
	 * @return
	 */
	public int getAction()
	{
		return this.action;
	}

	/**
	 * The data to send, or received.
	 * 
	 * @return
	 */
	public byte[] getData()
	{
		return this.data;
	}

	public String[] getDataArray()
	{
		ByteArrayDataInput in = ByteStreams.newDataInput(this.data);
		int maxIndex = in.readInt();
		String[] dataArray = new String[maxIndex];
		for (int i = 0; i < maxIndex; i++)
		{
			dataArray[i] = in.readUTF();
		}
		return dataArray;
	}

	public String getDataString()
	{
		return new String(this.data);
	}

	/**
	 * The owning plugin.
	 * 
	 * @return
	 */
	public int getPluginId()
	{
		return this.pluginId;
	}

	/**
	 * The server who initiated this communication.
	 * 
	 * @return
	 */
	public int getRecipientServer()
	{
		return this.recipientServer;
	}

	/**
	 * The server targeted by this communication.
	 * 
	 * @return
	 */
	public Integer getTargetServer()
	{
		return this.targetServer;
	}

	public boolean send()
	{
		return this.communicationManager.sendServerCommunication(this);
	}

	public void setData(String data)
	{
		this.data = data.getBytes();
	}

	/**
	 * Can be read using {@link #getDataArray()}
	 * 
	 * @param data
	 */
	public void setData(String[] data)
	{
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeInt(data.length);
		for (String part : data)
		{
			out.writeUTF(part == null ? "" : part);
		}
		this.data = out.toByteArray();
	}
}
