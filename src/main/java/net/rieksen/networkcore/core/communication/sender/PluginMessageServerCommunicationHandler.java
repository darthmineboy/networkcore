package net.rieksen.networkcore.core.communication.sender;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;

import com.google.common.collect.Iterables;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import net.rieksen.networkcore.core.communication.IServerCommunicationListener;
import net.rieksen.networkcore.core.communication.IServerCommunicationSender;
import net.rieksen.networkcore.core.communication.ServerCommunication;
import net.rieksen.networkcore.core.server.Server;

/**
 * This sender uses the bungeecord plugin messaging channel to deliver
 * {@link ServerCommunication}. It is not reliable, as plugin messages can only
 * be sent and received when recipient and target server have a player online.
 * In this implementation we have chosen to sent the message to all servers,
 * regardless of their online state. BungeeCord will still deliver the message
 * once a player is connected.
 * 
 * @author Darthmineboy
 */
@Deprecated
public class PluginMessageServerCommunicationHandler implements PluginMessageListener, IServerCommunicationSender
{

	private static final String					CHANNEL_NAME	= "BungeeCord";
	private final JavaPlugin					plugin;
	private final IServerCommunicationListener	listener;

	public PluginMessageServerCommunicationHandler(JavaPlugin plugin, IServerCommunicationListener listener)
	{
		Validate.notNull(plugin);
		Validate.notNull(listener);
		this.plugin = plugin;
		this.listener = listener;
	}

	@Override
	public void disable()
	{
		this.plugin.getServer().getMessenger().unregisterIncomingPluginChannel(this.plugin,
			PluginMessageServerCommunicationHandler.CHANNEL_NAME, this);
		this.plugin.getServer().getMessenger().unregisterOutgoingPluginChannel(this.plugin,
			PluginMessageServerCommunicationHandler.CHANNEL_NAME);
	}

	public void init()
	{
		this.plugin.getServer().getMessenger().registerIncomingPluginChannel(this.plugin,
			PluginMessageServerCommunicationHandler.CHANNEL_NAME, this);
		this.plugin.getServer().getMessenger().registerOutgoingPluginChannel(this.plugin,
			PluginMessageServerCommunicationHandler.CHANNEL_NAME);
	}

	@Override
	public void onPluginMessageReceived(String channel, Player player, byte[] message)
	{
		if (!channel.equals(PluginMessageServerCommunicationHandler.CHANNEL_NAME)) { return; }
		try
		{
			ByteArrayDataInput in = ByteStreams.newDataInput(message);
			String subchannel = in.readUTF();
			if (!subchannel.equals("NetworkCore")) { return; }
			short len = in.readShort();
			byte[] msgbytes = new byte[len];
			in.readFully(msgbytes);
			DataInputStream msgin = new DataInputStream(new ByteArrayInputStream(msgbytes));
			// data
			int recipientServer = msgin.readInt();
			int pluginId = msgin.readInt();
			int action = msgin.readInt();
			byte[] data = new byte[len - 12];
			msgin.readFully(data);
			// end data
			ServerCommunication communication =
				new ServerCommunication(null, recipientServer, Server.getLocalServer().getServerId(), pluginId, action, data);
			this.listener.onServerCommunicationReceived(communication);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public boolean sendServerCommunication(ServerCommunication communication)
	{
		try
		{
			ByteArrayDataOutput out = ByteStreams.newDataOutput();
			out.writeUTF("Forward");
			String target;;
			if (communication.getTargetServer() == null)
			{
				target = "ALL";
			} else
			{
				Server targetServer = Server.getServer(communication.getTargetServer());
				if (targetServer == null) { return false; }
				target = targetServer.getName();
			}
			out.writeUTF(target);
			out.writeUTF("NetworkCore");
			ByteArrayOutputStream msgbytes = new ByteArrayOutputStream();
			DataOutputStream msgout = new DataOutputStream(msgbytes);
			// data
			msgout.writeInt(communication.getRecipientServer());
			msgout.writeInt(communication.getPluginId());
			msgout.writeInt(communication.getAction());
			msgout.write(communication.getData());
			// end data
			out.writeShort(msgbytes.toByteArray().length);
			out.write(msgbytes.toByteArray());
			Player player = Iterables.getFirst(Bukkit.getOnlinePlayers(), null);
			if (player == null) { return false; }
			player.sendPluginMessage(this.plugin, PluginMessageServerCommunicationHandler.CHANNEL_NAME, out.toByteArray());
			return true;
		} catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
}
