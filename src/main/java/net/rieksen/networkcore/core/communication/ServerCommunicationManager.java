package net.rieksen.networkcore.core.communication;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.commons.lang.Validate;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.core.server.Server;
import net.rieksen.networkcore.core.socket.ServerSocketRequest;
import net.rieksen.networkcore.core.user.User;

@Deprecated
public class ServerCommunicationManager implements IServerCommunicationListener, IServerCommunicationSender
{

	private final NetworkCore								provider;
	private final Collection<IServerCommunicationListener>	listeners	= new CopyOnWriteArrayList<>();
	private final List<IServerCommunicationSender>			handlers	= new CopyOnWriteArrayList<>();

	public ServerCommunicationManager(NetworkCore provider)
	{
		this.provider = provider;
	}

	public void addHandler(IServerCommunicationSender handler)
	{
		Validate.notNull(handler);
		this.handlers.add(handler);
	}

	public void addListener(IServerCommunicationListener listener)
	{
		Validate.notNull(listener);
		this.listeners.add(listener);
	}

	/**
	 * Broadcast a message on the target spigot server.
	 * 
	 * @param targetServer
	 *            null for all servers except this
	 * @param messageId
	 * @return
	 */
	public void broadcastMessage(Integer targetServer, int messageId)
	{
		this.broadcastMessage(targetServer, messageId, null);
	}

	/**
	 * Broadcast a message on the target spigot server.
	 * 
	 * @param targetServer
	 *            null for all servers except this
	 * @param message
	 * @param variables
	 * @return
	 */
	public void broadcastMessage(Integer targetServer, int messageId, Map<String, String> variables)
	{
		List<String> list = new ArrayList<>();
		list.add(String.valueOf(messageId));
		if (variables != null)
		{
			for (Entry<String, String> entry : variables.entrySet())
			{
				list.add(entry.getKey());
				list.add(entry.getValue());
			}
		}
		String[] data = list.toArray(new String[0]);
		new ServerCommunication(this, this.getServer(), targetServer, this.getPlugin(), CommunicationAction.BROADCAST_MESSAGE.getAction(),
			data).send();
	}

	/**
	 * Broadcast a message on the target spigot server.
	 * 
	 * @param targetServer
	 *            null for all servers except this
	 * @param message
	 */
	public void broadcastMessage(Integer targetServer, String message)
	{
		new ServerCommunication(this, this.getServer(), targetServer, this.getPlugin(), CommunicationAction.BROADCAST.getAction(), message)
			.send();
	}

	/**
	 * Broadcast a message on the target spigot server. All players with the
	 * permission will receive this message.
	 * 
	 * @param targetServer
	 *            null for all servers except this
	 * @param messageId
	 * @param variables
	 * @return
	 */
	public void broadcastMessage(Integer targetServer, String permission, int messageId, Map<String, String> variables)
	{
		List<String> list = new ArrayList<>();
		list.add(permission);
		list.add(String.valueOf(messageId));
		if (variables != null)
		{
			for (Entry<String, String> entry : variables.entrySet())
			{
				list.add(entry.getKey());
				list.add(entry.getValue());
			}
		}
		String[] data = list.toArray(new String[0]);
		new ServerCommunication(this, this.getServer(), targetServer, this.getPlugin(),
			CommunicationAction.BROADCAST_MESSAGE_WITH_PERMISSION.getAction(), data).send();
	}

	/**
	 * Broadcast a message on the target spigot server. All players with the
	 * permission will receive this message.
	 * 
	 * @param targetServer
	 *            null for all servers except this
	 * @param permission
	 * @param message
	 */
	public void broadcastMessage(Integer targetServer, String permission, String message)
	{
		new ServerCommunication(this, this.getServer(), targetServer, this.getPlugin(),
			CommunicationAction.BROADCAST_WITH_PERMISSION.getAction(), new String[] { permission, message }).send();
	}

	@Override
	public void disable()
	{
		for (IServerCommunicationSender sender : this.handlers)
		{
			sender.disable();
		}
	}

	@Override
	public void onServerCommunicationReceived(ServerCommunication communication)
	{
		Validate.notNull(communication);
		for (IServerCommunicationListener listener : this.listeners)
		{
			try
			{
				listener.onServerCommunicationReceived(communication);
			} catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean sendServerCommunication(final ServerCommunication communication)
	{
		this.provider.getSocketModule().sendAll(communication.getPluginId(), "SERVERCOMMUNICATION:" + communication.getAction(),
			new ServerSocketRequest(communication.getData()));
		return true;
	}

	public void sendUserMessage(int userId, String message)
	{
		new ServerCommunication(this, this.getServer(), null, this.getPlugin(), CommunicationAction.SEND_USER_MESSAGE.getAction(),
			new String[] { String.valueOf(userId), message }).send();
	}

	public void sendUserMessage(int userId, String permission, String message)
	{
		new ServerCommunication(this, this.getServer(), null, this.getPlugin(),
			CommunicationAction.SEND_USER_MESSAGE_WITH_PERMISSION.getAction(), new String[] { String.valueOf(userId), permission, message })
				.send();
	}

	public void sendUserToServer(User user, int serverId)
	{
		/*
		 * TODO optimize send user to server 1) Send to all bungeecord proxies
		 * 2) Only the server to which the user is connected
		 */
		new ServerCommunication(this, this.getServer(), null, this.getPlugin(), CommunicationAction.SEND_USER_TO_SERVER.getAction(),
			new String[] { String.valueOf(user.getUserId()), String.valueOf(serverId) }).send();
	}

	private int getPlugin()
	{
		return NetworkPlugin.getPlugin("NetworkCore").getPluginId();
	}

	private int getServer()
	{
		return Server.getLocalServer().getServerId();
	}
}
