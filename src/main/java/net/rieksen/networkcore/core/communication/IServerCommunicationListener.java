package net.rieksen.networkcore.core.communication;

@Deprecated
public interface IServerCommunicationListener
{

	public void onServerCommunicationReceived(ServerCommunication communication);
}
