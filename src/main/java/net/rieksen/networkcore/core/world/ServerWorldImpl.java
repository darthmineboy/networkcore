package net.rieksen.networkcore.core.world;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.server.Server;
import net.rieksen.networkcore.core.world.pojo.ServerWorldPojo;
import net.rieksen.networkcore.core.world.pojo.WorldLocationPojo;

/**
 * ServerWorld is a world in a server. Purposely named ServerWorld to prevent
 * clashes with bukkit world.
 */
public class ServerWorldImpl implements ServerWorld
{

	private final NetworkCore	provider;
	private int					worldId;
	private int					serverId;
	private String				name;
	/*
	 * ICache implementation
	 */
	private boolean				isCached;
	private boolean				keepCached;
	private long				lastUpdate	= System.currentTimeMillis();

	/**
	 * @param worldId
	 * @param serverId
	 * @param name
	 * @throws IllegalStateException
	 *             when serverId or name is null
	 */
	public ServerWorldImpl(NetworkCore provider, int worldId, int serverId, String name)
	{
		Validate.notNull(provider);
		Validate.notNull(name, "Name cannot be null");
		this.provider = provider;
		this.worldId = worldId;
		this.serverId = serverId;
		this.name = name;
	}

	public ServerWorldImpl(NetworkCore provider, ServerWorldPojo pojo)
	{
		Validate.notNull(provider);
		Validate.notNull(pojo);
		Validate.notNull(pojo.getWorldName());
		this.provider = provider;
		this.setPojo(pojo);
	}

	@Override
	public void changeName(String name)
	{
		if (this.name.equals(name)) { return; }
		ServerWorldPojo pojo = this.toPojo();
		pojo.setWorldName(name);
		this.provider.getDAO().getWorldDAO().updateWorld(pojo);
		this.setPojo(pojo);
	}

	@Override
	public WorldLocation createLocation(Location location)
	{
		WorldLocationPojo pojo = new WorldLocationPojo(0, this.worldId, location.getX(), location.getY(), location.getZ(),
			location.getPitch(), location.getYaw());
		pojo.setLocationId(this.provider.getDAO().getWorldDAO().createLocation(pojo));
		return this.provider.getWorldModule().getFactory().createLocation(pojo);
	}

	@Override
	public String getName()
	{
		return this.name;
	}

	@Override
	public int getServerId()
	{
		return this.serverId;
	}

	@Override
	public World getWorld()
	{
		if (Server.getLocalServer().getServerId() != this.serverId) { return null; }
		return Bukkit.getWorld(this.name);
	}

	@Override
	public int getWorldId()
	{
		return this.worldId;
	}

	@Override
	public boolean isCached()
	{
		return this.isCached;
	}

	@Override
	public void isCached(boolean isCached)
	{
		this.isCached = isCached;
	}

	@Override
	public boolean isCacheExpired()
	{
		return System.currentTimeMillis() - this.lastUpdate > 300000L;
	}

	@Override
	public boolean keepCached()
	{
		return this.keepCached;
	}

	@Override
	public void keepCached(boolean keepCached)
	{
		this.keepCached = keepCached;
	}

	@Override
	public void resetCacheExpiration()
	{
		this.lastUpdate = System.currentTimeMillis();
	}

	@Override
	public ServerWorldPojo toPojo()
	{
		return new ServerWorldPojo(this.worldId, this.serverId, this.name);
	}

	@Override
	public String toString()
	{
		return "ServerWorld [worldID=" + this.worldId + ", serverId=" + this.serverId + ", name=" + this.name + "]";
	}

	private void setPojo(ServerWorldPojo pojo)
	{
		this.worldId = pojo.getWorldId();
		this.serverId = pojo.getServerId();
		this.name = pojo.getWorldName();
	}
}
