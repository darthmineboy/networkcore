package net.rieksen.networkcore.core.world;

import org.bukkit.World;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.dao.WorldDAO;
import net.rieksen.networkcore.core.module.ModuleImpl;
import net.rieksen.networkcore.core.server.Server;
import net.rieksen.networkcore.core.world.pojo.ServerWorldPojo;
import net.rieksen.networkcore.core.world.pojo.WorldLocationPojo;

public class WorldModuleImpl extends ModuleImpl implements WorldModule
{

	private final NetworkCore	provider;
	private WorldFactory		factory;

	public WorldModuleImpl(NetworkCore provider)
	{
		this.provider = provider;
		this.factory = new WorldFactoryImpl(provider);
	}

	@Override
	public WorldFactory getFactory()
	{
		return this.factory;
	}

	@Override
	public WorldLocation getLocation(int locationId)
	{
		WorldLocationPojo pojo = this.getWorldDAO().findLocation(locationId);
		return pojo == null ? null : this.getFactory().createLocation(pojo);
	}

	@Override
	public ServerWorld getWorld(int worldId)
	{
		ServerWorldPojo pojo = this.getWorldDAO().findWorld(worldId);
		return pojo == null ? null : this.getFactory().createWorld(pojo);
	}

	@Override
	public ServerWorld getWorld(World world)
	{
		Server server = Server.getLocalServer();
		return server.findWorld(world);
	}

	@Override
	protected void destroyModule() throws Exception
	{}

	protected WorldDAO getWorldDAO()
	{
		return this.provider.getDAO().getWorldDAO();
	}

	@Override
	protected void initModule() throws Exception
	{}
}
