package net.rieksen.networkcore.core.world;

import org.bukkit.Location;

import net.rieksen.networkcore.core.NetworkCoreAPI;
import net.rieksen.networkcore.core.cache.ICacheable;
import net.rieksen.networkcore.core.world.pojo.WorldLocationPojo;

public interface WorldLocation extends ICacheable
{

	public static WorldLocation getLocation(int locationId)
	{
		return NetworkCoreAPI.getWorldModule().getLocation(locationId);
	}

	Location buildLocation();

	void changePitch(float pitch);

	void changeX(double x);

	void changeY(double y);

	void changeYaw(float yaw);

	void changeZ(double z);

	int getLocationId();

	float getPitch();

	int getWorldId();

	double getX();

	double getY();

	float getYaw();

	double getZ();

	WorldLocationPojo toPojo();
}