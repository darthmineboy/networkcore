package net.rieksen.networkcore.core.world.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ServerWorldPojo
{

	private int		worldId;
	private int		serverId;
	private String	worldName;

	public ServerWorldPojo()
	{}

	public ServerWorldPojo(int worldId, int serverId, String worldName)
	{
		this.worldId = worldId;
		this.serverId = serverId;
		this.worldName = worldName;
	}

	/**
	 * @deprecated Use {@link #getWorldName()} instead
	 */
	@Deprecated
	public String getName()
	{
		return this.getWorldName();
	}

	public int getServerId()
	{
		return this.serverId;
	}

	public int getWorldId()
	{
		return this.worldId;
	}

	public String getWorldName()
	{
		return this.worldName;
	}

	/**
	 * @deprecated Use {@link #setWorldName(String)} instead
	 */
	@Deprecated
	public void setName(String name)
	{
		this.setWorldName(name);
	}

	public void setServerId(int serverId)
	{
		this.serverId = serverId;
	}

	public void setWorldId(int worldId)
	{
		this.worldId = worldId;
	}

	public void setWorldName(String name)
	{
		this.worldName = name;
	}
}
