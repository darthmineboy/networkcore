package net.rieksen.networkcore.core.world;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.world.pojo.ServerWorldPojo;
import net.rieksen.networkcore.core.world.pojo.WorldLocationPojo;

public interface WorldFactory
{

	/**
	 * @deprecated Replaced by {@link #createLocation(WorldLocationPojo)}.
	 * @param provider
	 * @param pojo
	 * @return
	 */
	@Deprecated
	public static WorldLocation createLocation(NetworkCore provider, WorldLocationPojo pojo)
	{
		return provider.getWorldModule().getFactory().createLocation(pojo);
	}

	/**
	 * @deprecated Replaced by {@link #createWorld(ServerWorldPojo)}.
	 * @param provider
	 * @param worldId
	 * @param serverId
	 * @param name
	 * @return
	 */
	@Deprecated
	public static ServerWorld createWorld(NetworkCore provider, int worldId, int serverId, String name)
	{
		return new ServerWorldImpl(provider, worldId, serverId, name);
	}

	/**
	 * @deprecated Replaced by {@link #createWorld(ServerWorldPojo)}.
	 * @param provider
	 * @param pojo
	 * @return
	 */
	@Deprecated
	public static ServerWorld createWorld(NetworkCore provider, ServerWorldPojo pojo)
	{
		return provider.getWorldModule().getFactory().createWorld(pojo);
	}

	WorldLocation createLocation(WorldLocationPojo location);

	ServerWorld createWorld(ServerWorldPojo world);
}
