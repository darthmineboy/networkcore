package net.rieksen.networkcore.core.world.pojo;

public class WorldLocationPojo
{

	private int		locationId;
	private int		worldId;
	private double	x;
	private double	y;
	private double	z;
	private float	pitch;
	private float	yaw;

	public WorldLocationPojo()
	{}

	public WorldLocationPojo(int locationId, int worldId, double x, double y, double z, float pitch, float yaw)
	{
		this.locationId = locationId;
		this.worldId = worldId;
		this.x = x;
		this.y = y;
		this.z = z;
		this.pitch = pitch;
		this.yaw = yaw;
	}

	public int getLocationId()
	{
		return this.locationId;
	}

	public float getPitch()
	{
		return this.pitch;
	}

	public int getWorldId()
	{
		return this.worldId;
	}

	public double getX()
	{
		return this.x;
	}

	public double getY()
	{
		return this.y;
	}

	public float getYaw()
	{
		return this.yaw;
	}

	public double getZ()
	{
		return this.z;
	}

	public void setLocationId(int locationId)
	{
		this.locationId = locationId;
	}

	public void setPitch(float pitch)
	{
		this.pitch = pitch;
	}

	public void setWorldId(int worldId)
	{
		this.worldId = worldId;
	}

	public void setX(double x)
	{
		this.x = x;
	}

	public void setY(double y)
	{
		this.y = y;
	}

	public void setYaw(float yaw)
	{
		this.yaw = yaw;
	}

	public void setZ(double z)
	{
		this.z = z;
	}
}
