package net.rieksen.networkcore.core.world;

import org.bukkit.World;

import net.rieksen.networkcore.core.module.Module;

public interface WorldModule extends Module
{

	WorldFactory getFactory();

	WorldLocation getLocation(int locationId);

	ServerWorld getWorld(int worldId);

	ServerWorld getWorld(World world);
}