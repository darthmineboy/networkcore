package net.rieksen.networkcore.core.world;

import org.bukkit.Location;
import org.bukkit.World;

import net.rieksen.networkcore.core.NetworkCoreAPI;
import net.rieksen.networkcore.core.cache.ICacheable;
import net.rieksen.networkcore.core.world.pojo.ServerWorldPojo;

public interface ServerWorld extends ICacheable
{

	public static ServerWorld getWorld(int worldId)
	{
		return NetworkCoreAPI.getWorldModule().getWorld(worldId);
	}

	public static ServerWorld getWorld(World world)
	{
		return NetworkCoreAPI.getWorldModule().getWorld(world);
	}

	/**
	 * Changes the name of the world.
	 * 
	 * @param name
	 */
	void changeName(String name);

	WorldLocation createLocation(Location location);

	/**
	 * Get the name.
	 * 
	 * @return
	 */
	String getName();

	/**
	 * Get the serverId.
	 * 
	 * @return
	 */
	int getServerId();

	World getWorld();

	/**
	 * Get the worldID. Can return null.
	 * 
	 * @return
	 */
	int getWorldId();

	@Override
	boolean isCached();

	@Override
	void isCached(boolean isCached);

	@Override
	boolean isCacheExpired();

	@Override
	boolean keepCached();

	@Override
	void keepCached(boolean keepCached);

	@Override
	void resetCacheExpiration();

	ServerWorldPojo toPojo();
}