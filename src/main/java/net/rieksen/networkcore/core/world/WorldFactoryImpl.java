package net.rieksen.networkcore.core.world;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.world.pojo.ServerWorldPojo;
import net.rieksen.networkcore.core.world.pojo.WorldLocationPojo;

public class WorldFactoryImpl implements WorldFactory
{

	private NetworkCore provider;

	public WorldFactoryImpl(NetworkCore provider)
	{
		this.provider = provider;
	}

	@Override
	public WorldLocation createLocation(WorldLocationPojo location)
	{
		return new WorldLocationImpl(this.provider, location);
	}

	@Override
	public ServerWorld createWorld(ServerWorldPojo world)
	{
		return new ServerWorldImpl(this.provider, world);
	}
}
