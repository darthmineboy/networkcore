package net.rieksen.networkcore.core.world;

import org.apache.commons.lang.Validate;
import org.bukkit.Location;
import org.bukkit.World;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.world.pojo.WorldLocationPojo;

/**
 * A full location to a world, and location.
 * 
 * @author Darthmineboy
 */
public final class WorldLocationImpl implements WorldLocation
{

	private final NetworkCore	provider;
	private int					locationId;
	private int					worldId;
	private double				x;
	private double				y;
	private double				z;
	private float				pitch;
	private float				yaw;
	/*
	 * ICache implementation
	 */
	private boolean				isCached;
	private boolean				keepCached;
	private long				lastUpdate	= System.currentTimeMillis();

	public WorldLocationImpl(NetworkCore provider, int locationId, int worldId, double x, double y, double z, float pitch, float yaw)
	{
		Validate.notNull(provider, "Provider cannot be null");
		this.provider = provider;
		this.locationId = locationId;
		this.worldId = worldId;
		this.x = x;
		this.y = y;
		this.z = z;
		this.pitch = pitch;
		this.yaw = yaw;
	}

	public WorldLocationImpl(NetworkCore provider, WorldLocationPojo location)
	{
		Validate.notNull(provider, "Provider cannot be null");
		this.provider = provider;
		this.setPojo(location);
	}

	@Override
	public Location buildLocation()
	{
		World world = this.provider.getWorldModule().getWorld(this.worldId).getWorld();
		return new Location(world, this.x, this.y, this.z, this.yaw, this.pitch);
	}

	@Override
	public void changePitch(float pitch)
	{
		WorldLocationPojo pojo = this.toPojo();
		pojo.setPitch(pitch);
		this.provider.getDAO().getWorldDAO().updateLocation(pojo);
		this.setPojo(pojo);
	}

	@Override
	public void changeX(double x)
	{
		WorldLocationPojo pojo = this.toPojo();
		pojo.setX(x);
		this.provider.getDAO().getWorldDAO().updateLocation(pojo);
		this.setPojo(pojo);
	}

	@Override
	public void changeY(double y)
	{
		WorldLocationPojo pojo = this.toPojo();
		pojo.setY(y);
		this.provider.getDAO().getWorldDAO().updateLocation(pojo);
		this.setPojo(pojo);
	}

	@Override
	public void changeYaw(float yaw)
	{
		WorldLocationPojo pojo = this.toPojo();
		pojo.setYaw(yaw);
		this.provider.getDAO().getWorldDAO().updateLocation(pojo);
		this.setPojo(pojo);
	}

	@Override
	public void changeZ(double z)
	{
		WorldLocationPojo pojo = this.toPojo();
		pojo.setZ(z);
		this.provider.getDAO().getWorldDAO().updateLocation(pojo);
		this.setPojo(pojo);
	}

	@Override
	public int getLocationId()
	{
		return this.locationId;
	}

	@Override
	public float getPitch()
	{
		return this.pitch;
	}

	@Override
	public int getWorldId()
	{
		return this.worldId;
	}

	@Override
	public double getX()
	{
		return this.x;
	}

	@Override
	public double getY()
	{
		return this.y;
	}

	@Override
	public float getYaw()
	{
		return this.yaw;
	}

	@Override
	public double getZ()
	{
		return this.z;
	}

	@Override
	public boolean isCached()
	{
		return this.isCached;
	}

	@Override
	public void isCached(boolean isCached)
	{
		this.isCached = isCached;
	}

	@Override
	public boolean isCacheExpired()
	{
		return System.currentTimeMillis() - this.lastUpdate > 300000L;
	}

	@Override
	public boolean keepCached()
	{
		return this.keepCached;
	}

	@Override
	public void keepCached(boolean keepCached)
	{
		this.keepCached = keepCached;
	}

	@Override
	public void resetCacheExpiration()
	{
		this.lastUpdate = System.currentTimeMillis();
	}

	@Override
	public WorldLocationPojo toPojo()
	{
		return new WorldLocationPojo(this.locationId, this.worldId, this.x, this.y, this.z, this.pitch, this.yaw);
	}

	private void setPojo(WorldLocationPojo pojo)
	{
		this.locationId = pojo.getLocationId();
		this.worldId = pojo.getWorldId();
		this.x = pojo.getX();
		this.y = pojo.getY();
		this.z = pojo.getZ();
		this.pitch = pojo.getPitch();
		this.yaw = pojo.getYaw();
	}
}
