package net.rieksen.networkcore.core.module;

/**
 * A module of the software. Modules can be initialized, and destroyed. Modules
 * may depend on each other.
 */
public interface Module
{

	/**
	 * Destroys the module. If successful {@link #getStatus()} should return
	 * {@link Status.DESTROYED}, else {@link Status.ERROR}.
	 */
	void destroy();

	/**
	 * Name of the module.
	 * 
	 * @return
	 */
	String getName();

	/**
	 * Status of the module.
	 * 
	 * @return
	 */
	Status getStatus();

	/**
	 * Initializes the module. If successful {@link #getStatus()} should return
	 * {@link Status.INITIALIZED}, else {@link Status.ERROR}.
	 */
	void init();

	/**
	 * The modules that must be initialized before this module.
	 * 
	 * @return
	 */
	Module[] requiresModules();

	public static enum Status
	{
		INITIALIZED,
		DESTROYED,
		ERROR;
	}
}
