package net.rieksen.networkcore.core.module;

public abstract class ModuleImpl implements Module
{

	protected Status status = Status.DESTROYED;

	@Override
	public final void destroy()
	{
		try
		{
			this.destroyModule();
			this.status = Status.DESTROYED;
		} catch (Exception e)
		{
			this.status = Status.ERROR;
			e.printStackTrace();
		}
	}

	@Override
	public final String getName()
	{
		return this.getClass().getSimpleName();
	}

	@Override
	public final Status getStatus()
	{
		return this.status;
	}

	@Override
	public final void init()
	{
		try
		{
			this.initModule();
			this.status = Status.INITIALIZED;
		} catch (Exception e)
		{
			this.status = Status.ERROR;
			e.printStackTrace();
		}
	}

	@Override
	public Module[] requiresModules()
	{
		return null;
	}

	protected abstract void destroyModule() throws Exception;

	protected abstract void initModule() throws Exception;
}