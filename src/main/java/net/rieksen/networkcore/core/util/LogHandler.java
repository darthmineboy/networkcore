package net.rieksen.networkcore.core.util;

import java.util.Date;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

import org.bukkit.scheduler.BukkitRunnable;

import com.google.common.collect.Iterables;

import net.rieksen.networkcore.core.server.Server;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;

public class LogHandler extends Handler
{

	private final static int	MAX_MESSAGE_LENGTH	= 1024;
	private final static int	RATE_LIMIT			= 30;
	private NetworkCoreSpigot	provider;
	private Queue<Record>		records				= new ArrayBlockingQueue<>(LogHandler.RATE_LIMIT);

	public LogHandler(NetworkCoreSpigot provider)
	{
		this.provider = provider;
	}

	@Override
	public void close() throws SecurityException
	{}

	@Override
	public void flush()
	{}

	public void init()
	{
		new BukkitRunnable()
		{

			@Override
			public void run()
			{
				for (int i = 0; i < LogHandler.RATE_LIMIT; i++)
				{
					Record recordTmp = LogHandler.this.records.poll();
					if (recordTmp == null) { return; }
					LogRecord record = recordTmp.record;
					String message = record.getMessage();
					if (message == null)
					{
						Throwable t = record.getThrown();
						if (t != null)
						{
							StringBuilder str = new StringBuilder(t.getMessage());
							for (StackTraceElement stack : t.getStackTrace())
							{
								str.append(stack.toString());
							}
							message = str.toString();
						}
					}
					if (message == null) { return; }
					if (message.length() > LogHandler.MAX_MESSAGE_LENGTH)
					{
						message = message.substring(0, LogHandler.MAX_MESSAGE_LENGTH);
					}
					Server server = Server.getLocalServer();
					if (server != null)
					{
						server.createLog(new Date(record.getMillis()), record.getLevel(), message, recordTmp.skipped);
					}
				}
			}
		}.runTaskTimerAsynchronously(this.provider, 0L, 20L);
	}

	@Override
	public void publish(LogRecord record)
	{
		if (!this.provider.isEnabled()) { return; }
		try
		{
			if (!this.provider.getPlugin().getOptionSection("Config").getOption("console-logging")
				.getBoolean(Server.getLocalServer().getServerId())) { return; }
		} catch (Exception e)
		{
			// Silence
		}
		if (!this.records.offer(new Record(record)))
		{
			Record last = Iterables.getLast(this.records, null);
			if (last != null)
			{
				last.skipped++;
			}
		}
	}

	private static class Record
	{

		private LogRecord	record;
		private int			skipped;

		public Record(LogRecord record)
		{
			this.record = record;
		}
	}
}
