package net.rieksen.networkcore.core.util;

import java.util.concurrent.TimeUnit;

public class TimeUtil
{

	/**
	 * Get a basic time format.
	 * <p>
	 * {days} days, {hrs} hours<br>
	 * {hrs} hours, {mins} minutes<br>
	 * {mins} minutes, {secs} seconds<br>
	 * {secs} seconds
	 * 
	 * @param time
	 *            in ms
	 * @return
	 */
	public static String formatTime(long millis)
	{
		long days = TimeUnit.MILLISECONDS.toDays(millis);
		millis -= TimeUnit.DAYS.toMillis(days);
		long hours = TimeUnit.MILLISECONDS.toHours(millis);
		millis -= TimeUnit.HOURS.toMillis(hours);
		long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
		millis -= TimeUnit.MINUTES.toMillis(minutes);
		long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);
		if (days != 0)
		{
			return days + " days, " + hours + " hours";
		} else if (hours != 0)
		{
			return hours + " hours, " + minutes + " minutes";
		} else if (minutes != 0) { return minutes + " minutes, " + seconds + " seconds"; }
		return seconds + " seconds";
	}
}
