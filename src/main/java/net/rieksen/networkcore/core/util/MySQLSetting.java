package net.rieksen.networkcore.core.util;

import org.apache.commons.lang.Validate;

/**
 * Stores MySQL settings, with some defaults.
 * 
 * @author Darthmineboy
 */
public class MySQLSetting
{

	private final String	host;
	private int				port				= 3306;
	private final String	database;
	private final String	username;
	private final String	password;
	private int				minimumIdle			= 1;
	private int				maximumPoolSize		= 50;
	private long			idleTimeout			= 60000L;
	private String			testQuery			= "SELECT 1";
	private long			connectionTimeout	= 5000L;
	private String			poolName;
	// private String timezone = "UTC+1:00";

	/**
	 * @param host
	 * @param database
	 * @param username
	 * @param password
	 * @throws IllegalStateException
	 *             when host, database, username or password is null
	 */
	public MySQLSetting(String host, String database, String username, String password)
	{
		Validate.notNull(host, "Host cannot be null");
		Validate.notNull(database, "Database cannot be null");
		Validate.notNull(username, "Username cannot be null");
		Validate.notNull(password, "Password cannot be null");
		if (host.contains(":"))
		{
			String[] parts = host.split(":");
			this.host = parts[0];
			try
			{
				this.port = Integer.parseInt(parts[1]);
			} catch (NumberFormatException e)
			{
				// silence
			}
		} else
		{
			this.host = host;
		}
		this.database = database;
		this.username = username;
		this.password = password;
	}

	/**
	 * Get the connection timeout.
	 * 
	 * @return
	 */
	public long getConnectionTimeout()
	{
		return this.connectionTimeout;
	}

	/**
	 * Get the database.
	 * 
	 * @return
	 */
	public String getDatabase()
	{
		return this.database;
	}

	/**
	 * Get the host.
	 * 
	 * @return
	 */
	public String getHost()
	{
		return this.host;
	}

	/**
	 * Get the idle timeout. After how many ms should an idle connection be
	 * removed from the pool.
	 * 
	 * @return
	 */
	public long getIdleTimeout()
	{
		return this.idleTimeout;
	}

	/**
	 * Get the maximum pool size.
	 * 
	 * @return
	 */
	public int getMaximumPoolSize()
	{
		return this.maximumPoolSize;
	}

	/**
	 * Get the minimum idle connections.
	 * 
	 * @return
	 */
	public int getMinimumIdle()
	{
		return this.minimumIdle;
	}

	/**
	 * Get the password.
	 * 
	 * @return
	 */
	public String getPassword()
	{
		return this.password;
	}

	public String getPoolName()
	{
		return this.poolName;
	}

	/**
	 * Get the port.
	 * 
	 * @return
	 */
	public int getPort()
	{
		return this.port;
	}

	/**
	 * Get the test query.
	 * 
	 * @return
	 */
	public String getTestQuery()
	{
		return this.testQuery;
	}

	/*
	 * public String getTimezone() { return this.timezone; }
	 */
	/**
	 * Get the username.
	 * 
	 * @return
	 */
	public String getUsername()
	{
		return this.username;
	}

	/**
	 * Set the connection timeout.
	 * 
	 * @param connectionTimeout
	 */
	public void setConnectionTimeout(long connectionTimeout)
	{
		this.connectionTimeout = connectionTimeout;
	}

	/**
	 * Set the idle timeout. After how many ms should an idle connection be
	 * removed from the pool.
	 * 
	 * @param idleTimeout
	 *            in ms
	 */
	public void setIdleTimeout(long idleTimeout)
	{
		this.idleTimeout = idleTimeout;
	}

	/**
	 * Set the maximum pool size.
	 * 
	 * @param maximumPoolSize
	 */
	public void setMaximumPoolSize(int maximumPoolSize)
	{
		this.maximumPoolSize = maximumPoolSize;
	}

	/**
	 * Set the minimum idle connections.
	 * 
	 * @param minimumIdle
	 */
	public void setMinimumIdle(int minimumIdle)
	{
		this.minimumIdle = minimumIdle;
	}

	public void setPoolName(String poolName)
	{
		this.poolName = poolName;
	}

	/**
	 * Set the port.
	 * 
	 * @param port
	 */
	public void setPort(int port)
	{
		this.port = port;
	}

	/**
	 * Set the test query.
	 * 
	 * @param testQuery
	 */
	public void setTestQuery(String testQuery)
	{
		this.testQuery = testQuery;
	}
	/*
	 * public void setTimezone(String timezone) { this.timezone = timezone; }
	 */
}
