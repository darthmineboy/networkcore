package net.rieksen.networkcore.core.util;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MojangAPI
{

	/**
	 * Get the name history. If the provided uuid is an offline mode uuid null
	 * is returned.
	 * 
	 * @param uuid
	 * @return
	 * @throws Exception
	 */
	public List<NameHistory> getNameHistory(UUID uuid) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		URL url = new URL("https://api.mojang.com/user/profiles/" + uuid.toString().replace("-", "") + "/names");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		if (connection.getResponseCode() == 204)
		{
			return null;
		} else
		{
			return mapper.readValue(connection.getInputStream(), new TypeReference<List<NameHistory>>()
			{});
		}
	}

	public static class NameHistory
	{

		public static Comparator<NameHistory> comparator()
		{
			return new Comparator<MojangAPI.NameHistory>()
			{

				@Override
				public int compare(NameHistory o1, NameHistory o2)
				{
					if (o1.changedToAt == null && o2.changedToAt != null) { return -1; }
					if (o1.changedToAt == o2.changedToAt) { return 0; }
					if (o1.changedToAt != null && o2.changedToAt == null) { return 1; }
					return o1.changedToAt.compareTo(o2.changedToAt);
				}
			};
		}

		private String	name;
		private Date	changedToAt;

		public NameHistory(String name, Date changedToAt)
		{
			this.name = name;
			this.changedToAt = changedToAt;
		}

		NameHistory()
		{
			// Bean test
		}

		public Date getChangedToAt()
		{
			return this.changedToAt;
		}

		public String getName()
		{
			return this.name;
		}

		public void setChangedToAt(Date changedToAt)
		{
			this.changedToAt = changedToAt;
		}

		public void setName(String name)
		{
			this.name = name;
		}
	}
}
