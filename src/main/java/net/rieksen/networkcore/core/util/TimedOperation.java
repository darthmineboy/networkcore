package net.rieksen.networkcore.core.util;

public class TimedOperation
{

	private boolean	stopped;
	private long	startTime;
	private long	stopTime;

	public TimedOperation()
	{
		this(System.currentTimeMillis());
	}

	public TimedOperation(long startTime)
	{
		this.startTime = startTime;
	}

	public void reset()
	{
		this.startTime = System.currentTimeMillis();
		this.stopped = false;
	}

	public void stop()
	{
		this.stopTime = System.currentTimeMillis();
		this.stopped = true;
	}

	public long timeElapsed()
	{
		if (this.stopped) { return this.startTime - this.stopTime; }
		return System.currentTimeMillis() - this.startTime;
	}
}
