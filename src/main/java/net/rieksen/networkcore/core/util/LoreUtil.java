package net.rieksen.networkcore.core.util;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;

/**
 * Utilities for lore.
 * 
 * @author Darthmineboy
 */
public class LoreUtil
{

	public static final ChatColor	DEFAULT_COLOR	= ChatColor.WHITE;
	public static final int			LORE_LENGTH		= 30;

	/**
	 * Creates the lore with defaults
	 * {@link #createLore(String, boolean, boolean, int, ChatColor)}.
	 * <p>
	 * color = true<br>
	 * newline = true<br>
	 * loreLength = 30<br>
	 * defaultColor = WHITE
	 * 
	 * @param input
	 * @return
	 */
	public static List<String> createLore(String input)
	{
		return LoreUtil.createLore(input, true, true, LoreUtil.LORE_LENGTH, LoreUtil.DEFAULT_COLOR);
	}

	/**
	 * Creates the lore.
	 * 
	 * @param input
	 * @param color
	 *            whether to translate colors
	 * @param newline
	 *            whether to split the message at newline characters
	 * @param loreLength
	 *            the length at which a lore is split
	 * @param defaultColor
	 *            the default color of the message
	 * @return
	 */
	public static List<String> createLore(String input, boolean color, boolean newline, int loreLength, ChatColor defaultColor)
	{
		if (color)
		{
			input = ChatColor.translateAlternateColorCodes('&', input);
		}
		input = TextUtil.fixNewLineCharacter(input);
		String[] splitMessage = newline ? input.split("\n") : new String[] { input };
		List<String> lore = new ArrayList<>();
		for (String message : splitMessage)
		{
			lore.addAll(TextUtil.splitMessage(defaultColor + message, loreLength));
		}
		return lore;
	}
}
