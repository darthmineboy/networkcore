package net.rieksen.networkcore.core.util;

import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class SimplePage<T>
{

	private int				currentPage;
	private int				maxResults;
	private int				totalResults;
	private Collection<T>	data;

	/**
	 * @param currentPage
	 *            first page is 0
	 * @param maxResults
	 *            the maximum size the data collection should be
	 * @param totalResults
	 *            the total size of the collection if pagination wasn't applied
	 * @param data
	 *            the data of the current page
	 */
	public SimplePage(int currentPage, int maxResults, int totalResults, Collection<T> data)
	{
		this.currentPage = currentPage;
		this.maxResults = maxResults;
		this.totalResults = totalResults;
		this.data = data;
	}

	public int getCurrentPage()
	{
		return this.currentPage;
	}

	public Collection<T> getData()
	{
		return this.data;
	}

	public int getMaxPage()
	{
		int pages = this.totalResults / this.maxResults;
		if (this.totalResults % this.maxResults != 0)
		{
			pages++;
		}
		return pages;
	}

	public int getMaxResults()
	{
		return this.maxResults;
	}

	public int getTotalResults()
	{
		return this.totalResults;
	}

	public void setCurrentPage(int currentPage)
	{
		this.currentPage = currentPage;
	}

	public void setData(Collection<T> data)
	{
		this.data = data;
	}

	public void setMaxResults(int maxResults)
	{
		this.maxResults = maxResults;
	}

	public void setTotalResults(int totalResults)
	{
		this.totalResults = totalResults;
	}
}
