package net.rieksen.networkcore.core.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.ChatColor;

import com.google.common.base.Splitter;

public class TextUtil
{

	public static String fixNewLineCharacter(String input)
	{
		return input.replace("\\n", "\n");
	}

	public static String replaceVariables(String text, Map<String, String> variables)
	{
		if (variables != null && text != null)
		{
			for (Entry<String, String> variable : variables.entrySet())
			{
				text = text.replace(variable.getKey(), variable.getValue() == null ? "" : variable.getValue());
			}
		}
		return text;
	}

	/**
	 * Splits a message up after given character length, and applies colors of
	 * previous line.
	 * 
	 * @param message
	 * @param length
	 * @return
	 */
	public static List<String> splitMessage(String message, int length)
	{
		TextSplitter s = new TextSplitter(message, length);
		return s.splitMessage();
	}

	private static class TextSplitter
	{

		private final String	message;
		private final int		length;
		private List<String>	list;
		private StringBuilder	buffer;

		public TextSplitter(String message, int length)
		{
			this.message = message;
			this.length = length;
		}

		public List<String> splitMessage()
		{
			this.list = new ArrayList<>();
			this.buffer = new StringBuilder();
			String[] args = this.message.split(" ");
			for (String arg : args)
			{
				int bufferLength = this.getBufferLength();
				int argLength = arg.length();
				if (argLength > this.length)
				{
					if (bufferLength != 0)
					{
						this.writeBuffer();
					}
					List<String> wordParts = Splitter.fixedLength(30).splitToList(arg);
					for (String wordPart : wordParts)
					{
						this.writeInBuffer(wordPart + " ");
					}
					continue;
				}
				if (bufferLength + argLength >= this.length)
				{
					this.writeBuffer();
				}
				this.writeInBuffer(arg + " ");
			}
			if (this.getBufferLength() != 0)
			{
				this.writeBuffer();
			}
			this.fixColors(this.list);
			return this.list;
		}

		private void fixColors(List<String> list)
		{
			String previousColor = null;
			for (int i = 0; i < list.size(); i++)
			{
				if (previousColor == null)
				{
					previousColor = ChatColor.getLastColors(list.get(i));
					continue;
				}
				list.set(i, previousColor + list.get(i));
				previousColor = ChatColor.getLastColors(list.get(i));
			}
		}

		private int getBufferLength()
		{
			return this.buffer.length();
		}

		private void writeBuffer()
		{
			this.list.add(this.buffer.toString());
			this.buffer = new StringBuilder();
		}

		private void writeInBuffer(String message)
		{
			this.buffer.append(message);
			if (this.getBufferLength() >= this.length)
			{
				this.writeBuffer();
			}
		}
	}
}
