package net.rieksen.networkcore.core.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.Validate;

public class ListUtil
{

	/**
	 * Returns a sublist of type T where the sublist starts at
	 * <code>page * itemsPerPage</code> and ends itemsPerPage later. The
	 * starting and end index are adjusted to prevent IndexOutOfBoundsException,
	 * and this method will return an empty ArrayList if the resulting sublist
	 * would be empty.
	 * 
	 * @throws IllegalArgumentException
	 *             page < 0
	 * @throws IllegalArgumentException
	 *             itemsPerPage < 0
	 * @param list
	 * @param page
	 *            starts at 0
	 * @param itemsPerPage
	 * @return null when list is null
	 */
	public static <T> List<T> getSubList(List<T> list, int page, int itemsPerPage)
	{
		Validate.isTrue(page >= 0, "Page must be bigger or equal to 0; actual " + page);
		Validate.isTrue(itemsPerPage >= 0, "Items per page must be bigger or equal to 0; actual " + itemsPerPage);
		if (list == null) { return null; }
		int firstIndex = page * itemsPerPage;
		int listSize = list.size();
		if (firstIndex >= listSize) { return new ArrayList<>(); }
		int lastIndex = firstIndex + itemsPerPage;
		if (lastIndex > listSize)
		{
			lastIndex = listSize;
		}
		return list.subList(firstIndex, lastIndex);
	}
}
