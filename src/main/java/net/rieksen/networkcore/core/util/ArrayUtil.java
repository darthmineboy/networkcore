package net.rieksen.networkcore.core.util;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.ChatColor;

import com.google.common.collect.Lists;

public class ArrayUtil
{

	public static String append(String[] args)
	{
		return append(args, " ");
	}

	public static String append(String[] args, String split)
	{
		if (args.length == 0) { return ""; }
		StringBuilder str = new StringBuilder();
		for (String arg : args)
		{
			str.append(arg).append(split);
		}
		String argument = str.toString().substring(0, str.length() - 1);
		return argument;
	}

	/**
	 * Splits a string at whitespace characters, but keeps characters between
	 * doublequotes together. Characters at the end of a string, which are
	 * prefixed by a double quote are kept together.
	 * <p>
	 * <code>
	 * Normal Argument Input = {"Normal", "Argument", "Input"}<br>
	 * "With" "Doublequotes" = {"With", "Doublequotes"} <br>
	 * Mixed "With Doublequotes" = {"Mixed", "With Doublequotes"}<br>
	 * Mixed "With Doublequotes" "Unfinished argument = {"Mixed", "With Doublequotes", "Unfinished Argument"}</code>
	 * 
	 * @param argument
	 * @return
	 */
	public static String[] splitArguments(String argument)
	{
		Pattern p = Pattern.compile("\"(.*?)\"|([^\"\\s]+)|(\".*)");
		Matcher m = p.matcher(argument);
		List<String> list = Lists.newArrayList();
		while (m.find())
		{
			String group = m.group(1);
			if (group == null)
			{
				group = m.group(2);
			}
			if (group == null)
			{
				group = m.group(3);
			}
			if (group != null)
			{
				group = group.replace("\"", "");
			}
			list.add(group);
		}
		return list.toArray(new String[0]);
	}

	/**
	 * Splits the arguments like {@link #splitArguments(String)}, but replaces
	 * empty arguments with two doublequotes to retain the empty argument.
	 * Useful for splitting command arguments.
	 * 
	 * @param args
	 * @return
	 */
	public static String[] splitArguments(String[] args)
	{
		if (args.length == 0) { return new String[0]; }
		StringBuilder str = new StringBuilder();
		for (String arg : args)
		{
			if (arg.isEmpty())
			{
				arg = "\"\"";
			}
			str.append(arg).append(" ");
		}
		String argument = str.toString().substring(0, str.length() - 1);
		return splitArguments(argument);
	}

	public static void color(String[] args, char c)
	{
		for (int i = 0; i < args.length; i++)
		{
			args[i] = ChatColor.translateAlternateColorCodes(c, args[i]);
		}
	}
}
