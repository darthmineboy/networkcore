package net.rieksen.networkcore.core.util;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;

public class PluginUtil
{

	/**
	 * Returns a double based on a version string.
	 * <p>
	 * 1.0.0-BETA returns 1.0 <br>
	 * 1.1 returns 1.1 <br>
	 * 1.1.1 returns 1.11
	 * 
	 * @param version
	 * @return
	 */
	@Deprecated
	public static double getVersion(String version)
	{
		Validate.notNull(version, "Version cannot be null");
		double newVersion = 0;
		String versionNumbers = version.replaceAll("[^\\d.]", "");
		String[] versionParts = versionNumbers.split("\\.");
		for (int i = 0; i < versionParts.length; i++)
		{
			double versionPart = Double.parseDouble(versionParts[i]);
			newVersion += versionPart / Math.pow(10, i);
		}
		return newVersion;
	}

	/**
	 * @param oldVersion
	 * @param newVersion
	 * @return true if newVersion > oldVersion
	 */
	public static boolean isVersionGreater(String oldVersion, String newVersion)
	{
		String[] oldVersionParts = PluginUtil.getVersionParts(oldVersion);
		String[] newVersionParts = PluginUtil.getVersionParts(newVersion);
		int max = Math.max(oldVersionParts.length, newVersionParts.length);
		for (int i = 0; i < max; i++)
		{
			if (oldVersionParts.length > i && "".equals(oldVersionParts[i]))
			{
				oldVersionParts[i] = String.valueOf(0);
			}
			if (newVersionParts.length > i && "".equals(newVersionParts[i]))
			{
				newVersionParts[i] = String.valueOf(0);
			}
			long oldVersionValue, newVersionValue;
			try
			{
				oldVersionValue = oldVersionParts.length > i ? Long.parseLong(oldVersionParts[i]) : 0;
			} catch (NumberFormatException e)
			{
				oldVersionValue = Long.MAX_VALUE;
			}
			try
			{
				newVersionValue = newVersionParts.length > i ? Long.parseLong(newVersionParts[i]) : 0;
			} catch (NumberFormatException e)
			{
				newVersionValue = Long.MAX_VALUE;
			}
			if (newVersionValue > oldVersionValue)
			{
				return true;
			} else if (newVersionValue != oldVersionValue) { return false; }
		}
		// Release is greater than snapshot
		if (StringUtils.endsWithIgnoreCase(oldVersion, "-SNAPSHOT")
			&& !StringUtils.endsWithIgnoreCase(newVersion, "-SNAPSHOT")) { return true; }
		return false;
	}

	private static String[] getVersionParts(String version)
	{
		return PluginUtil.removeClutterFromVersion(version).split("\\.");
	}

	private static String removeClutterFromVersion(String version)
	{
		return version.replaceAll("[^\\d.]", "");
	}
}
