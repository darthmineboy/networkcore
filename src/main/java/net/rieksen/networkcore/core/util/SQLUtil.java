package net.rieksen.networkcore.core.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

public class SQLUtil
{

	public static String arrayToString(String[] array)
	{
		StringBuilder str = new StringBuilder();
		str.append("(");
		int i = 1;
		for (String e : array)
		{
			str.append("\"");
			str.append(SQLUtil.escapeString(e, true));
			str.append("\"");
			if (i < array.length)
			{
				str.append(",");
				i++;
			}
		}
		str.append(")");
		return str.toString();
	}

	/**
	 * Converts {@link Date} to {@link Timestamp}.
	 * 
	 * @param date
	 * @return null when date is null
	 */
	@SuppressWarnings("deprecation")
	public static Timestamp dateToTimestamp(Date date)
	{
		if (date == null) { return null; }
		if (date.getYear() + 1900 > 9999)
		{
			date.setYear(9999 - 1900);
		}
		Timestamp ts = new Timestamp(date.getTime());
		return ts;
	}

	public static String escapeString(String x, boolean escapeDoubleQuotes)
	{
		StringBuilder sBuilder = new StringBuilder(x.length() * 11 / 10);
		int stringLength = x.length();
		for (int i = 0; i < stringLength; ++i)
		{
			char c = x.charAt(i);
			switch (c)
			{
				case 0: /* Must be escaped for 'mysql' */
					sBuilder.append('\\');
					sBuilder.append('0');
					break;
				case '\n': /* Must be escaped for logs */
					sBuilder.append('\\');
					sBuilder.append('n');
					break;
				case '\r':
					sBuilder.append('\\');
					sBuilder.append('r');
					break;
				case '\\':
					sBuilder.append('\\');
					sBuilder.append('\\');
					break;
				case '\'':
					sBuilder.append('\\');
					sBuilder.append('\'');
					break;
				case '"': /* Better safe than sorry */
					if (escapeDoubleQuotes)
					{
						sBuilder.append('\\');
					}
					sBuilder.append('"');
					break;
				case '\032': /* This gives problems on Win32 */
					sBuilder.append('\\');
					sBuilder.append('Z');
					break;
				case '\u00a5':
				case '\u20a9':
					// escape characters interpreted as backslash by mysql
					// fall through
				default:
					sBuilder.append(c);
			}
		}
		return sBuilder.toString();
	}

	public static Integer getInteger(ResultSet rs, String column) throws SQLException
	{
		Integer value = rs.getInt(column);
		return rs.wasNull() ? null : value;
	}

	public static Long getLong(ResultSet rs, String column) throws SQLException
	{
		Long value = rs.getLong(column);
		return rs.wasNull() ? null : value;
	}

	/**
	 * Converts {@link Timestamp} to {@link Date}.
	 * 
	 * @param stamp
	 * @return null when stamp is null
	 */
	public static Date timestampToDate(Timestamp stamp)
	{
		if (stamp == null) { return null; }
		return new Date(stamp.getTime());
	}
}
