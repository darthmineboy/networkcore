package net.rieksen.networkcore.core.util;

import java.sql.Connection;
import java.sql.SQLException;

public interface IMySQL
{

	void destroy() throws SQLException;

	Connection getConnection() throws SQLException;
}