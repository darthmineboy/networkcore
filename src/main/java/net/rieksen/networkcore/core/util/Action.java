package net.rieksen.networkcore.core.util;

/**
 * Runs an action {@link Action.onAction()}.
 * 
 * @author Darthmineboy
 */
public abstract class Action
{

	/**
	 * Runs the action
	 */
	public abstract void onAction();
}
