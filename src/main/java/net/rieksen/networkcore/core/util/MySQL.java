package net.rieksen.networkcore.core.util;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.lang.Validate;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.pool.PoolInitializationException;

/**
 * The container class for a {@link HikariDataSource}. An efficient mySQL
 * connection pool.
 * 
 * @author Darthmineboy
 */
public class MySQL implements IMySQL
{

	private final HikariDataSource	dataSource;
	private final MySQLSetting		setting;

	/**
	 * Sets up the {@link HikariDataSource}.
	 * 
	 * @throws IllegalStateException
	 *             when setting is null
	 * @throws PoolInitializationException
	 *             when connection fails
	 */
	public MySQL(MySQLSetting setting)
	{
		Validate.notNull(setting, "Setting cannot be null");
		this.setting = setting;
		HikariConfig config = new HikariConfig();
		config.setJdbcUrl("jdbc:mysql://" + setting.getHost() + ":" + setting.getPort() + "/" + setting.getDatabase());
		config.setDriverClassName("com.mysql.jdbc.Driver");
		// String timezone = setting.getTimezone();
		// config.setConnectionInitSql("SET time_zone = '" + timezone + "'");
		config.setUsername(setting.getUsername());
		config.setPassword(setting.getPassword());
		config.setMinimumIdle(setting.getMinimumIdle());
		config.setMaximumPoolSize(setting.getMaximumPoolSize());
		config.setIdleTimeout(setting.getIdleTimeout());
		config.setConnectionTimeout(setting.getConnectionTimeout());
		config.setConnectionTestQuery(setting.getTestQuery());
		if (setting.getPoolName() != null)
		{
			config.setPoolName(setting.getPoolName());
		}
		try
		{
			this.dataSource = new HikariDataSource(config);
		} catch (Exception e)
		{
			try
			{
				this.destroy();
			} catch (SQLException e1)
			{
				//
			}
			throw e;
		}
	}

	@Override
	public void destroy() throws SQLException
	{
		if (this.dataSource != null)
		{
			this.dataSource.close();
		}
	}

	@Override
	public Connection getConnection() throws SQLException
	{
		return this.dataSource.getConnection();
	}

	/**
	 * Get the {@link HikariDataSource}.
	 */
	public HikariDataSource getDataSource()
	{
		return this.dataSource;
	}

	/**
	 * Get the MySQL settings.
	 * 
	 * @return
	 */
	public MySQLSetting getMySQLSetting()
	{
		return this.setting;
	}
}
