package net.rieksen.networkcore.core.util;

import java.util.Date;

public final class DateUtil
{

	/**
	 * Checks whether the difference between the two dates is less than the
	 * provided ms.
	 * 
	 * @param a
	 * @param b
	 * @param ms
	 * @return false if one or both of the dates are null
	 */
	public static boolean differenceLessThan(Date a, Date b, long ms)
	{
		if (a == null || b == null) { return false; }
		return DateUtil.getDifference(a, b) < ms;
	}

	/**
	 * Calculates the time difference.
	 * 
	 * @param a
	 * @param b
	 * @return {@link Long.MAX_VALUE} if one or both of the dates are null, else
	 *         difference in millis
	 */
	public static long getDifference(Date a, Date b)
	{
		if (a == null || b == null) { return Long.MAX_VALUE; }
		long difference = a.getTime() - b.getTime();
		if (difference < 0)
		{
			difference *= -1;
		}
		return difference;
	}

	private DateUtil()
	{}
}
