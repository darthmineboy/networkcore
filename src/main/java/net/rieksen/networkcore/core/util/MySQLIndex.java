package net.rieksen.networkcore.core.util;

public class MySQLIndex
{

	private String indexName;

	public MySQLIndex(String indexName)
	{
		this.indexName = indexName;
	}

	public String getIndexName()
	{
		return this.indexName;
	}
}
