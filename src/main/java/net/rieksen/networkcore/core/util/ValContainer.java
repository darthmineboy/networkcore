package net.rieksen.networkcore.core.util;

public class ValContainer<T>
{

	private T value;

	public ValContainer()
	{}

	public ValContainer(T value)
	{
		this.value = value;
	}

	public T getValue()
	{
		return this.value;
	}

	public void setValue(T value)
	{
		this.value = value;
	}
}
