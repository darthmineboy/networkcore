package net.rieksen.networkcore.core.web;

import java.io.InputStream;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.internal.RuntimeDelegateImpl;
import org.glassfish.jersey.jackson.JacksonFeature;

public class NetworkWebsiteClient
{

	private static final String	BASE_URL	= "https://api.networkcore.org/";
	private Client				client;

	public NetworkWebsiteClient()
	{
		SSLContext stc;
		try
		{
			stc = SSLContext.getInstance("SSL");
			TrustManager[] certs = new TrustManager[] { new X509TrustManager()
			{

				@Override
				public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException
				{}

				@Override
				public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException
				{}

				@Override
				public X509Certificate[] getAcceptedIssuers()
				{
					return null;
				}
			} };
			stc.init(null, certs, new SecureRandom());
		} catch (Exception e)
		{
			throw new RuntimeException(e);
		}
		this.client = ClientBuilder.newBuilder().register(JacksonFeature.class).hostnameVerifier(new HostnameVerifier()
		{

			@Override
			public boolean verify(String hostname, SSLSession session)
			{
				return true;
			}
		}).sslContext(stc).build();
	}

	public InstallationPojo createInstallation()
	{
		WebTarget target = this.getRootTarget().path("/installations");
		Response response = target.request().post(null);
		InstallationPojo pojo = response.readEntity(InstallationPojo.class);
		return pojo;
	}

	public InputStream downloadResource(int resourceId, String version)
	{
		WebTarget target = this.getRootTarget().path("/resources/" + resourceId + "/versions/" + version + "/download");
		Response response = target.request().get();
		if (response.getStatus() != 200) { throw new RuntimeException(
			"Failed to download resource, response status is not 200 but " + response.getStatus()); }
		InputStream is = response.readEntity(InputStream.class);
		return is;
	}

	public DownloadResponse downloadResourceWithInstallation(int resourceId, String version, String installationSecret)
	{
		WebTarget target = this.getRootTarget().path("/resources/" + resourceId + "/versions/" + version + "/download");
		Response response = target.request().header("installationSecret", installationSecret).get();
		if (response.getStatus() != 200) { throw new RuntimeException(
			"Failed to download resource, response status is not 200 but " + response.getStatus()); }
		InputStream is = response.readEntity(InputStream.class);
		long size = -1;
		try
		{
			size = Long.parseLong(response.getHeaderString("Content-Length"));
		} catch (NumberFormatException e)
		{
			this.getLogger().log(Level.WARNING, String.format("Failed to determine size of resource being downloaded, Content-Length: %s",
				response.getHeaderString("Content-Length")), e);
		}
		return new DownloadResponse(is, size);
	}

	public ResourcePojo findResource(int resourceId)
	{
		WebTarget target = this.getRootTarget().path("/resources/" + resourceId);
		Response response = target.request().get();
		return response.readEntity(ResourcePojo.class);
	}

	public WebTarget getRootTarget()
	{
		this.client.register(RuntimeDelegateImpl.class);
		return this.client.target(NetworkWebsiteClient.BASE_URL);
	}

	public ResourceLicensePojo requestLicense(String installationSecret, LicenseRequestPojo request)
	{
		WebTarget target = this.getRootTarget().path("/installations/me/licenses");
		Response response = target.request().header("Authorization", "Bearer " + installationSecret).post(Entity.json(request));
		if (response.getStatus() != 200) { throw new RuntimeException(response.readEntity(String.class)); }
		return response.readEntity(ResourceLicensePojo.class);
	}

	private Logger getLogger()
	{
		return Logger.getLogger("NetworkCore");
	}
}
