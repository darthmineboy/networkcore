package net.rieksen.networkcore.core.web.resource;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResourceFeaturePojo
{

	private int		resourceId;
	private String	name;
	private String	shortDescription;
	private String	description;
	private int		order;

	public ResourceFeaturePojo()
	{}

	public ResourceFeaturePojo(int resourceId, String name, String shortDescription, String description, int order)
	{
		this.resourceId = resourceId;
		this.name = name;
		this.shortDescription = shortDescription;
		this.description = description;
		this.order = order;
	}

	public String getDescription()
	{
		return this.description;
	}

	public String getName()
	{
		return this.name;
	}

	public int getOrder()
	{
		return this.order;
	}

	public int getResourceId()
	{
		return this.resourceId;
	}

	public String getShortDescription()
	{
		return this.shortDescription;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setOrder(int order)
	{
		this.order = order;
	}

	public void setResourceId(int resourceId)
	{
		this.resourceId = resourceId;
	}

	public void setShortDescription(String shortDescription)
	{
		this.shortDescription = shortDescription;
	}
}
