package net.rieksen.networkcore.core.web;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResourceLicensePojo
{

	@XmlElement
	private long	licenseId;
	@XmlElement
	private int		resourceId;
	private String	licenseSecret;
	private long	downloadId;
	@XmlElement
	private boolean	licenseValid;
	private String	requestHost;
	private Date	requestDate;
	private Date	lastSeen;

	public ResourceLicensePojo()
	{}

	public ResourceLicensePojo(long licenseId, int resourceId, String licenseSecret, long downloadId, boolean licenseValid,
		String requestHost, Date requestDate, Date lastSeen)
	{
		this.licenseId = licenseId;
		this.resourceId = resourceId;
		this.licenseSecret = licenseSecret;
		this.downloadId = downloadId;
		this.licenseValid = licenseValid;
		this.requestHost = requestHost;
		this.requestDate = requestDate;
		this.lastSeen = lastSeen;
	}

	public long getDownloadId()
	{
		return this.downloadId;
	}

	public Date getLastSeen()
	{
		return this.lastSeen;
	}

	public long getLicenseId()
	{
		return this.licenseId;
	}

	public String getLicenseSecret()
	{
		return this.licenseSecret;
	}

	public Date getRequestDate()
	{
		return this.requestDate;
	}

	public String getRequestHost()
	{
		return this.requestHost;
	}

	public int getResourceId()
	{
		return this.resourceId;
	}

	public boolean isLicenseValid()
	{
		return this.licenseValid;
	}

	public void setDownloadId(long downloadId)
	{
		this.downloadId = downloadId;
	}

	public void setLastSeen(Date lastSeen)
	{
		this.lastSeen = lastSeen;
	}

	public void setLicenseId(long licenseId)
	{
		this.licenseId = licenseId;
	}

	public void setLicenseSecret(String licenseSecret)
	{
		this.licenseSecret = licenseSecret;
	}

	public void setLicenseValid(boolean licenseValid)
	{
		this.licenseValid = licenseValid;
	}

	public void setRequestDate(Date requestDate)
	{
		this.requestDate = requestDate;
	}

	public void setRequestHost(String requestHost)
	{
		this.requestHost = requestHost;
	}

	public void setResourceId(int resourceId)
	{
		this.resourceId = resourceId;
	}
}
