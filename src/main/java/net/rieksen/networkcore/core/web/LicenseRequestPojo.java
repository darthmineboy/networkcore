package net.rieksen.networkcore.core.web;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class LicenseRequestPojo
{

	private int		userId;
	private int		resourceId;
	private String	uniqueDownload;
	private String	licenseSecret;

	public LicenseRequestPojo()
	{}

	public LicenseRequestPojo(int userId, int resourceId, String uniqueDownload)
	{
		this.userId = userId;
		this.resourceId = resourceId;
		this.uniqueDownload = uniqueDownload;
	}

	public LicenseRequestPojo(int resourceId, String licenseSecret)
	{
		this.resourceId = resourceId;
		this.licenseSecret = licenseSecret;
	}

	public String getLicenseSecret()
	{
		return this.licenseSecret;
	}

	public int getResourceId()
	{
		return this.resourceId;
	}

	public String getUniqueDownload()
	{
		return this.uniqueDownload;
	}

	public int getUserId()
	{
		return this.userId;
	}

	public void setLicenseSecret(String licenseSecret)
	{
		this.licenseSecret = licenseSecret;
	}

	public void setResourceId(int resourceId)
	{
		this.resourceId = resourceId;
	}

	public void setUniqueDownload(String uniqueDownload)
	{
		this.uniqueDownload = uniqueDownload;
	}

	public void setUserId(int userId)
	{
		this.userId = userId;
	}
}
