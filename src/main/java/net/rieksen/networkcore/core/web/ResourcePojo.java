package net.rieksen.networkcore.core.web;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResourcePojo
{

	private int						resourceId;
	private int						authorId;
	private String					resourceName;
	private String					shortDescription;
	private String					description;
	private String					imageUrl;
	private boolean					premium;
	private String					latestVersion;
	private List<ResourceLinkPojo>	links;

	public ResourcePojo()
	{
		super();
	}

	public ResourcePojo(int resourceId, int authorId, String resourceName, String shortDescription, String description, String imageUrl,
		boolean premium, String latestVersion)
	{
		this.resourceId = resourceId;
		this.authorId = authorId;
		this.resourceName = resourceName;
		this.shortDescription = shortDescription;
		this.description = description;
		this.imageUrl = imageUrl;
		this.premium = premium;
		this.latestVersion = latestVersion;
	}

	public int getAuthorId()
	{
		return this.authorId;
	}

	public String getDescription()
	{
		return this.description;
	}

	public String getImageUrl()
	{
		return this.imageUrl;
	}

	public String getLatestVersion()
	{
		return this.latestVersion;
	}

	public List<ResourceLinkPojo> getLinks()
	{
		return this.links;
	}

	public int getResourceId()
	{
		return this.resourceId;
	}

	public String getResourceName()
	{
		return this.resourceName;
	}

	public String getShortDescription()
	{
		return this.shortDescription;
	}

	public boolean isPremium()
	{
		return this.premium;
	}

	public void setAuthorId(int authorId)
	{
		this.authorId = authorId;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setImageUrl(String imageUrl)
	{
		this.imageUrl = imageUrl;
	}

	public void setLatestVersion(String latestVersion)
	{
		this.latestVersion = latestVersion;
	}

	public void setLinks(List<ResourceLinkPojo> links)
	{
		this.links = links;
	}

	public void setPremium(boolean premium)
	{
		this.premium = premium;
	}

	public void setResourceId(int resourceId)
	{
		this.resourceId = resourceId;
	}

	public void setResourceName(String resourceName)
	{
		this.resourceName = resourceName;
	}

	public void setShortDescription(String shortDescription)
	{
		this.shortDescription = shortDescription;
	}
}
