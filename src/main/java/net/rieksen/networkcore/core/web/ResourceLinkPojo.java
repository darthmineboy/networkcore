package net.rieksen.networkcore.core.web;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResourceLinkPojo
{

	private int		resourceId;
	private String	link;
	private String	value;

	public ResourceLinkPojo()
	{}

	public ResourceLinkPojo(int resourceId, String link, String value)
	{
		this.resourceId = resourceId;
		this.link = link;
		this.value = value;
	}

	public String getLink()
	{
		return this.link;
	}

	public int getResourceId()
	{
		return this.resourceId;
	}

	public String getValue()
	{
		return this.value;
	}

	public void setLink(String link)
	{
		this.link = link;
	}

	public void setResourceId(int resourceId)
	{
		this.resourceId = resourceId;
	}

	public void setValue(String value)
	{
		this.value = value;
	}
}
