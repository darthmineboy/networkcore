package net.rieksen.networkcore.core.web;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class InstallationPojo
{

	@XmlElement
	private int		installationId;
	@XmlElement
	private String	installationSecret;
	@XmlElement
	private String	requestHost;
	@XmlElement
	private Date	requestDate;
	@XmlElement
	private Date	lastSeen;

	public InstallationPojo()
	{}

	public InstallationPojo(int installationId, String installationSecret, String requestHost, Date requestDate, Date lastSeen)
	{
		this.installationId = installationId;
		this.installationSecret = installationSecret;
		this.requestHost = requestHost;
		this.requestDate = requestDate;
		this.lastSeen = lastSeen;
	}

	public int getInstallationId()
	{
		return this.installationId;
	}

	public String getInstallationSecret()
	{
		return this.installationSecret;
	}

	public Date getLastSeen()
	{
		return this.lastSeen;
	}

	public Date getRequestDate()
	{
		return this.requestDate;
	}

	public String getRequestHost()
	{
		return this.requestHost;
	}

	public void setInstallationId(int installationId)
	{
		this.installationId = installationId;
	}

	public void setInstallationSecret(String installationSecret)
	{
		this.installationSecret = installationSecret;
	}

	public void setLastSeen(Date lastSeen)
	{
		this.lastSeen = lastSeen;
	}

	public void setRequestDate(Date requestDate)
	{
		this.requestDate = requestDate;
	}

	public void setRequestHost(String requestHost)
	{
		this.requestHost = requestHost;
	}
}
