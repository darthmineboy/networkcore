package net.rieksen.networkcore.core.web;

/**
 * The resource download source tells from where a copy of a resource was
 * downloaded. Based on the source there are different strategies available for
 * licensing.
 */
public enum ResourceDownloadSource
{
	/**
	 * https://www.spigotmc.org/
	 */
	SPIGOTMC;
}
