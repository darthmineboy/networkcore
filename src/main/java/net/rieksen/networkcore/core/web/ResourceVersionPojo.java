package net.rieksen.networkcore.core.web;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResourceVersionPojo
{

	private int		resourceId;
	private String	resourceVersion;
	private Date	releaseDate;
	private String	previousVersion;
	private Long	fileId;
	private int		timesDownloaded;
	private Long	fileSize;

	public ResourceVersionPojo()
	{}

	public ResourceVersionPojo(int resourceId, String resourceVersion, Date releaseDate, String previousVersion, Long fileId,
		int timesDownloaded)
	{
		this.resourceId = resourceId;
		this.resourceVersion = resourceVersion;
		this.releaseDate = releaseDate;
		this.previousVersion = previousVersion;
		this.fileId = fileId;
		this.timesDownloaded = timesDownloaded;
	}

	public ResourceVersionPojo(int resourceId, String resourceVersion, Date releaseDate, String previousVersion, Long fileId,
		int timesDownloaded, Long fileSize)
	{
		this.resourceId = resourceId;
		this.resourceVersion = resourceVersion;
		this.releaseDate = releaseDate;
		this.previousVersion = previousVersion;
		this.fileId = fileId;
		this.timesDownloaded = timesDownloaded;
		this.fileSize = fileSize;
	}

	public Long getFileId()
	{
		return this.fileId;
	}

	public Long getFileSize()
	{
		return this.fileSize;
	}

	public String getPreviousVersion()
	{
		return this.previousVersion;
	}

	public Date getReleaseDate()
	{
		return this.releaseDate;
	}

	public int getResourceId()
	{
		return this.resourceId;
	}

	public String getResourceVersion()
	{
		return this.resourceVersion;
	}

	public int getTimesDownloaded()
	{
		return this.timesDownloaded;
	}

	public void setFileId(Long fileId)
	{
		this.fileId = fileId;
	}

	public void setFileSize(Long fileSize)
	{
		this.fileSize = fileSize;
	}

	public void setPreviousVersion(String previousVersion)
	{
		this.previousVersion = previousVersion;
	}

	public void setReleaseDate(Date releaseDate)
	{
		this.releaseDate = releaseDate;
	}

	public void setResourceId(int resourceId)
	{
		this.resourceId = resourceId;
	}

	public void setResourceVersion(String resourceVersion)
	{
		this.resourceVersion = resourceVersion;
	}

	public void setTimesDownloaded(int timesDownloaded)
	{
		this.timesDownloaded = timesDownloaded;
	}
}
