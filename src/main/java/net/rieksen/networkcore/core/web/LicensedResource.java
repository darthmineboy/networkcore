package net.rieksen.networkcore.core.web;

public interface LicensedResource
{

	/**
	 * The resourceId as on the NetworkCore website.
	 * 
	 * @return
	 */
	int getResourceId();

	/**
	 * The unique download identifier.
	 * 
	 * @return
	 */
	String getUniqueDownload();

	/**
	 * The user who downloaded this resource.
	 * 
	 * @return
	 */
	String getUserId();
}
