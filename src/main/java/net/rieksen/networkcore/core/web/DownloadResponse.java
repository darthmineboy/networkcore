package net.rieksen.networkcore.core.web;

import java.io.InputStream;

public class DownloadResponse
{

	private InputStream	inputStream;
	private long		size;

	/**
	 * @param inputStream
	 * @param size
	 *            Size of the stream. -1 if size could not be determined
	 */
	public DownloadResponse(InputStream inputStream, long size)
	{
		this.inputStream = inputStream;
		this.size = size;
	}

	public InputStream getInputStream()
	{
		return this.inputStream;
	}

	/**
	 * The size of the stream.
	 * 
	 * @return -1 if size could not be determined
	 */
	public long getSize()
	{
		return this.size;
	}
}
