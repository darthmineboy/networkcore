package net.rieksen.networkcore.core.message;

/**
 * This binds a localeCode (e.g. en_US) to a language.<br>
 * <a href="http://minecraft.gamepedia.com/Language">Minecraft Languages</a>
 */
public interface LocaleLanguage
{

	void changeLanguageId(Integer languageId);

	Language getLanguage();

	Integer getLanguageId();

	/**
	 * Name of the language of the localeCode. This is not necessarily the name
	 * of the language that is configured as preferred language, but it could be
	 * depending on your configuration.
	 * 
	 * @return
	 */
	String getLanguageName();

	String getLocaleCode();
}
