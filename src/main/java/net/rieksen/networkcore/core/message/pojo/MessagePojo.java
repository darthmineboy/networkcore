package net.rieksen.networkcore.core.message.pojo;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MessagePojo
{

	private int								messageId;
	private int								sectionId;
	private String							messageName;
	private String							description;
	@SuppressWarnings("unused")
	private List<MessageTranslationPojo>	translations;
	@SuppressWarnings("unused")
	private List<MessageVariablePojo>		variables;
	@SuppressWarnings("unused")
	private List<LanguagePojo>				missingTranslations;

	public MessagePojo()
	{}

	public MessagePojo(int messageId, int sectionId, String messageName, String description)
	{
		this.messageId = messageId;
		this.sectionId = sectionId;
		this.messageName = messageName;
		this.description = description;
	}

	public String getDescription()
	{
		return this.description;
	}

	public int getMessageId()
	{
		return this.messageId;
	}

	public String getMessageName()
	{
		return this.messageName;
	}

	public String getName()
	{
		return this.messageName;
	}

	public int getSectionId()
	{
		return this.sectionId;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setMessageId(int messageId)
	{
		this.messageId = messageId;
	}

	public void setMessageName(String messageName)
	{
		this.messageName = messageName;
	}

	public void setMissingTranslations(List<LanguagePojo> missingTranslations)
	{
		this.missingTranslations = missingTranslations;
	}

	public void setName(String name)
	{
		this.messageName = name;
	}

	public void setSectionId(int sectionId)
	{
		this.sectionId = sectionId;
	}

	public void setTranslations(List<MessageTranslationPojo> translations)
	{
		this.translations = translations;
	}

	public void setVariables(List<MessageVariablePojo> variables)
	{
		this.variables = variables;
	}
}
