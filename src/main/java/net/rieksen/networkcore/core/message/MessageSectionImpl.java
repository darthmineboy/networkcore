package net.rieksen.networkcore.core.message;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.NetworkCoreAPI;
import net.rieksen.networkcore.core.cache.CacheComponent;
import net.rieksen.networkcore.core.message.pojo.MessagePojo;
import net.rieksen.networkcore.core.message.pojo.MessageSectionPojo;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;

/**
 * Message sections cache messages. This is above the already existent cache of
 * the datasource.
 */
public class MessageSectionImpl implements MessageSection
{

	/**
	 * @deprecated Replaced by {@link MessageSection#getSection(int)}. Get a
	 *             section by id. Can return null.
	 * @param sectionId
	 * @return
	 */
	@Deprecated
	public static MessageSection getSection(int sectionId)
	{
		return NetworkCoreAPI.getMessageModule().getSection(sectionId);
	}

	/**
	 * @deprecated Replaced by {@link MessageSection#getSection(int, String)}.
	 *             Get a section by plugin id and name. Can return null.
	 * @param pluginId
	 * @param name
	 * @return
	 */
	@Deprecated
	public static MessageSection getSection(int pluginId, String name)
	{
		return NetworkPlugin.getPlugin(pluginId).getMessageSection(name);
	}

	/**
	 * @deprecated Replaced by
	 *             {@link MessageSection#getSection(String, String)}. Get a
	 *             section by plugin name and name. Can return null.
	 * @param pluginName
	 * @param name
	 * @return
	 */
	@Deprecated
	public static MessageSection getSection(String pluginName, String name)
	{
		NetworkPlugin plugin = NetworkPlugin.getPlugin(pluginName);
		if (plugin == null) { return null; }
		return MessageSectionImpl.getSection(plugin.getPluginId(), name);
	}

	private final NetworkCore				provider;
	private final int						sectionId;
	private final int						pluginId;
	private final String					name;
	private String							description;
	private CacheComponent<List<Message>>	messageCache	= new CacheComponent<List<Message>>(TimeUnit.SECONDS.toMillis(300))
															{

																@Override
																public void refreshCache()
																{
																	List<Message> list = new ArrayList<>();
																	MessageSectionImpl.this.provider.getDAO().getMessageDAO()
																		.findMessages(MessageSectionImpl.this.sectionId).forEach(pojo ->
																																{
																																	list.add(
																																		MessageSectionImpl.this.provider
																																			.getMessageModule()
																																			.getFactory()
																																			.createMessage(
																																				pojo));
																																});
																	this.cachedObject = list;
																}
															};;
	/*
	 * ICache implementation
	 */
	private boolean							isCached;
	private boolean							keepCached;
	private long							lastUpdate		= System.currentTimeMillis();

	/**
	 * @param sectionId
	 * @param pluginId
	 * @param name
	 * @param description
	 * @throws IllegalStateException
	 *             when pluginId is null, name is null
	 */
	public MessageSectionImpl(NetworkCore provider, int sectionId, int pluginId, String name, String description)
	{
		Validate.notNull(provider, "Provider cannot be null");
		Validate.notNull(pluginId, "pluginId cannot be null");
		Validate.notNull(name, "Name cannot be null");
		this.provider = provider;
		this.sectionId = sectionId;
		this.pluginId = pluginId;
		this.name = name;
		this.description = description;
	}

	public MessageSectionImpl(NetworkCore provider, MessageSectionPojo pojo)
	{
		Validate.notNull(provider);
		Validate.notNull(pojo);
		Validate.notNull(pojo.getName());
		this.provider = provider;
		this.sectionId = pojo.getSectionId();
		this.pluginId = pojo.getPluginId();
		this.name = pojo.getName();
		this.description = pojo.getDescription();
	}

	@Override
	public void changeDescription(String description)
	{
		if (StringUtils.equals(this.description, description)) { return; }
		MessageSectionPojo pojo = this.toPojo();
		pojo.setDescription(description);
		this.provider.getDAO().getMessageDAO().updateSection(pojo);
		this.description = description;
	}

	@Override
	public Message createMessage(String messageName, String description)
	{
		MessagePojo pojo = new MessagePojo(0, this.sectionId, messageName, description);
		pojo.setMessageId(this.provider.getDAO().getMessageDAO().createMessage(pojo));
		Message message = this.provider.getMessageModule().getFactory().createMessage(pojo);
		this.getMessages().add(message);
		return message;
	}

	@Override
	public Message createMessage(String messageName, String defaultMessage, MessageType type, String description)
	{
		MessagePojo pojo = new MessagePojo(0, this.sectionId, messageName, description);
		pojo.setMessageId(this.provider.getDAO().getMessageDAO().createMessage(pojo));
		Message message = this.provider.getMessageModule().getFactory().createMessage(pojo);
		message.createDefaultTranslation(defaultMessage, type);
		this.getMessages().add(message);
		return message;
	}

	@Override
	public void deleteMessage(String name)
	{
		Message message = this.getMessage(name);
		if (message != null)
		{
			this.provider.getDAO().getMessageDAO().deleteMessage(message.getMessageId());
			this.messageCache.getCache().remove(message);
		}
	}

	@Override
	public String getDescription()
	{
		return this.description;
	}

	@Override
	public Message getMessage(int messageId)
	{
		return this.getMessages().stream().filter(message -> message.getMessageId() == messageId).findAny().orElse(null);
	}

	@Override
	public Message getMessage(String name) throws MessageNotFoundException
	{
		return this.messageCache.getCache().stream().filter(message -> message.getName().equalsIgnoreCase(name)).findAny().orElse(null);
	}

	@Override
	public List<Message> getMessages()
	{
		return this.messageCache.getCache();
	}

	@Override
	public String getName()
	{
		return this.name;
	}

	@Override
	public NetworkPlugin getNetworkPlugin()
	{
		return this.provider.getPluginModule().getPlugin(this.pluginId);
	}

	@Override
	public int getPluginId()
	{
		return this.pluginId;
	}

	@Override
	public int getSectionId()
	{
		return this.sectionId;
	}

	@Override
	public boolean isCached()
	{
		return this.isCached;
	}

	@Override
	public void isCached(boolean isCached)
	{
		this.isCached = isCached;
	}

	@Override
	public boolean isCacheExpired()
	{
		return System.currentTimeMillis() - this.lastUpdate > 300000L;
	}

	@Override
	public boolean keepCached()
	{
		return this.keepCached;
	}

	@Override
	public void keepCached(boolean keepCached)
	{
		this.keepCached = keepCached;
	}

	@Override
	public void refreshMessageCache()
	{
		this.messageCache.refreshCache();
	}

	@Override
	public void resetCacheExpiration()
	{
		this.lastUpdate = System.currentTimeMillis();
	}

	private MessageSectionPojo toPojo()
	{
		return new MessageSectionPojo(this.sectionId, this.pluginId, this.name, this.description);
	}
}
