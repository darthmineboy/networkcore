package net.rieksen.networkcore.core.message;

import org.apache.commons.lang.Validate;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.message.pojo.GlobalMessageVariablePojo;

/**
 * Global message variables are variables that are replaced in every message. In
 * this plugin they are utilized to handle message colors, but they can also be
 * used for constants such as network name.
 */
public class GlobalMessageVariableImpl implements GlobalMessageVariable
{

	private final NetworkCore	provider;
	private final int			variableId;
	private String				name;
	private String				replacement;

	public GlobalMessageVariableImpl(NetworkCore provider, GlobalMessageVariablePojo pojo)
	{
		Validate.notNull(provider);
		Validate.notNull(pojo);
		Validate.notNull(pojo.getName());
		Validate.notNull(pojo.getReplacement());
		this.provider = provider;
		this.variableId = pojo.getVariableId();
		this.name = pojo.getName();
		this.replacement = pojo.getReplacement();
	}

	public GlobalMessageVariableImpl(NetworkCore provider, int variableId, String name, String replacement)
	{
		Validate.notNull(provider);
		Validate.notNull(name);
		Validate.notNull(replacement);
		this.provider = provider;
		this.variableId = variableId;
		this.name = name;
		this.replacement = replacement;
	}

	@Override
	public void changeName(String name)
	{
		Validate.notNull(name);
		GlobalMessageVariablePojo pojo = this.toPojo();
		pojo.setName(name);
		this.provider.getDAO().getMessageDAO().updateGlobalVariable(pojo);
		this.name = name;
	}

	@Override
	public void changeReplacement(String replacement)
	{
		Validate.notNull(replacement);
		GlobalMessageVariablePojo pojo = this.toPojo();
		pojo.setReplacement(replacement);
		this.provider.getDAO().getMessageDAO().updateGlobalVariable(pojo);
		this.replacement = replacement;
	}

	@Override
	public String getName()
	{
		return this.name;
	}

	@Override
	public String getReplacement()
	{
		return this.replacement;
	}

	@Override
	public int getVariableId()
	{
		return this.variableId;
	}

	private GlobalMessageVariablePojo toPojo()
	{
		return new GlobalMessageVariablePojo(this.variableId, this.name, this.replacement);
	}
}
