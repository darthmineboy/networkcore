package net.rieksen.networkcore.core.message.pojo;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LocaleLanguagePojo
{

	private String	localeCode;
	private String	languageName;
	private Integer	languageId;

	public LocaleLanguagePojo()
	{}

	public LocaleLanguagePojo(String localeCode, String languageName, Integer languageId)
	{
		this.localeCode = localeCode;
		this.languageName = languageName;
		this.languageId = languageId;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) return true;
		if (obj == null) return false;
		if (this.getClass() != obj.getClass()) return false;
		LocaleLanguagePojo other = (LocaleLanguagePojo) obj;
		if (this.languageId == null)
		{
			if (other.languageId != null) return false;
		} else if (!this.languageId.equals(other.languageId)) return false;
		if (this.languageName == null)
		{
			if (other.languageName != null) return false;
		} else if (!this.languageName.equals(other.languageName)) return false;
		if (this.localeCode == null)
		{
			if (other.localeCode != null) return false;
		} else if (!this.localeCode.equals(other.localeCode)) return false;
		return true;
	}

	public Integer getLanguageId()
	{
		return this.languageId;
	}

	public String getLanguageName()
	{
		return this.languageName;
	}

	public String getLocaleCode()
	{
		return this.localeCode;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.languageId == null) ? 0 : this.languageId.hashCode());
		result = prime * result + ((this.languageName == null) ? 0 : this.languageName.hashCode());
		result = prime * result + ((this.localeCode == null) ? 0 : this.localeCode.hashCode());
		return result;
	}

	public void setLanguageId(Integer languageId)
	{
		this.languageId = languageId;
	}

	public void setLanguageName(String languageName)
	{
		this.languageName = languageName;
	}

	public void setLocaleCode(String localeCode)
	{
		this.localeCode = localeCode;
	}
}
