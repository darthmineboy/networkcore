package net.rieksen.networkcore.core.message.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public class LanguagePojo
{

	private int		languageId;
	private String	languageName;

	public LanguagePojo()
	{}

	public LanguagePojo(int languageId, String name)
	{
		this.languageId = languageId;
		this.languageName = name;
	}

	public int getLanguageId()
	{
		return this.languageId;
	}

	public String getName()
	{
		return this.languageName;
	}

	public void setLanguageId(int languageId)
	{
		this.languageId = languageId;
	}

	public void setName(String name)
	{
		this.languageName = name;
	}
}
