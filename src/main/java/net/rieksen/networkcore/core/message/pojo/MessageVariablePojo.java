package net.rieksen.networkcore.core.message.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class MessageVariablePojo
{

	private int		messageId;
	private String	variableName;
	private String	description;

	public MessageVariablePojo()
	{}

	public MessageVariablePojo(int messageId, String name, String description)
	{
		this.messageId = messageId;
		this.variableName = name;
		this.description = description;
	}

	public String getDescription()
	{
		return this.description;
	}

	public int getMessageId()
	{
		return this.messageId;
	}

	public String getName()
	{
		return this.variableName;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setMessageId(int messageId)
	{
		this.messageId = messageId;
	}

	public void setName(String name)
	{
		this.variableName = name;
	}
}
