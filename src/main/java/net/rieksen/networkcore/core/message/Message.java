package net.rieksen.networkcore.core.message;

import java.util.List;
import java.util.Map;

import org.bukkit.command.CommandSender;

import net.rieksen.networkcore.core.NetworkCoreAPI;
import net.rieksen.networkcore.core.cache.ICacheable;
import net.rieksen.networkcore.core.message.pojo.MessageTranslationPojo;
import net.rieksen.networkcore.core.message.pojo.MessageVariablePojo;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.core.user.User;

public interface Message extends ICacheable
{

	public static Message getMessage(int messageId)
	{
		return NetworkCoreAPI.getMessageModule().getMessage(messageId);
	}

	/**
	 * A quick way to grab a message.
	 * 
	 * @param pluginName
	 * @param sectionName
	 * @param messageName
	 * @throws IllegalStateException
	 *             when the section or plugin does not exist.
	 * @return
	 */
	public static Message getMessage(String pluginName, String sectionName, String messageName)
	{
		NetworkPlugin plugin = NetworkPlugin.getPlugin(pluginName);
		if (plugin == null) { throw new IllegalStateException(); }
		MessageSection section = plugin.getMessageSection(sectionName);
		if (section == null) { throw new IllegalStateException("The section " + sectionName + " does not exist for plugin " + pluginName); }
		return section.getMessage(messageName);
	}

	public MessageVariable createVariable(String name, String description);

	public void deleteVariable(String name);

	public String getMessage(UserLanguagePreferences preferences);

	public List<Language> getMissingTranslations();

	public MessageTranslation getTranslation(UserLanguagePreferences preferences);

	void changeDescription(String description);

	MessageTranslation createDefaultTranslation(String defaultMessage, MessageType type);

	/**
	 * @deprecated Replaced by
	 *             {@link #createTranslation(int, String, MessageType)}.
	 * @param languageId
	 * @param messageTranslation
	 * @return
	 */
	@Deprecated
	MessageTranslation createTranslation(int languageId, String messageTranslation);

	MessageTranslation createTranslation(int languageId, String translation, MessageType type);

	MessageTranslation createTranslation(MessageTranslationPojo message);

	MessageVariable createVariable(MessageVariablePojo variable);

	void deleteTranslation(int languageId);

	/**
	 * The translation for the default language of the {@link NetworkPlugin}.
	 * 
	 * @return
	 */
	MessageTranslation getDefaultPluginTranslation();

	/**
	 * The default translation of the message. This message should be managed by
	 * the plugin.
	 * 
	 * @return
	 */
	MessageTranslation getDefaultTranslation();

	String getDescription();

	/**
	 * Get the message with regards to user language preferences. Never returns
	 * null. When the default translation does not exist, and the existing
	 * languages to not match the language preference an error message is
	 * returned @{link {@link MessageImpl#getMissingDefaultTranslationMessage()}
	 * 
	 * @param user
	 *            can be null for default message
	 * @return
	 */
	String getMessage(User user);

	int getMessageId();

	String getName();

	int getSectionId();

	MessageTranslation getTranslation(int languageId);

	List<MessageTranslation> getTranslations();

	MessageVariable getVariable(String name);

	List<MessageVariable> getVariables();

	@Override
	boolean isCached();

	@Override
	void isCached(boolean isCached);

	@Override
	boolean isCacheExpired();

	@Override
	boolean keepCached();

	@Override
	void keepCached(boolean keepCached);

	@Override
	void resetCacheExpiration();

	void sendMessage(CommandSender sender);

	void sendMessage(CommandSender sender, Map<String, String> variables);

	void sendMessage(User user);

	void sendMessage(User user, Map<String, String> variables);
}