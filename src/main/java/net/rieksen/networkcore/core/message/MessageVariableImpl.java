package net.rieksen.networkcore.core.message;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.message.pojo.MessageVariablePojo;

public class MessageVariableImpl implements MessageVariable
{

	private final NetworkCore	provider;
	private final int			messageId;
	private final String		name;
	private String				description;

	/**
	 * @param messageId
	 * @param name
	 * @param description
	 * @throws IllegalStateException
	 *             messageId, or name is null
	 */
	public MessageVariableImpl(NetworkCore provider, int messageId, String name, String description)
	{
		Validate.notNull(provider);
		Validate.notNull(name);
		this.provider = provider;
		this.messageId = messageId;
		this.name = name;
		this.description = description;
	}

	public MessageVariableImpl(NetworkCore provider, MessageVariablePojo pojo)
	{
		Validate.notNull(provider);
		Validate.notNull(pojo);
		Validate.notNull(pojo.getName());
		this.provider = provider;
		this.messageId = pojo.getMessageId();
		this.name = pojo.getName();
		this.description = pojo.getDescription();
	}

	@Override
	public void changeDescription(String description)
	{
		if (StringUtils.equals(this.description, description)) { return; }
		MessageVariablePojo pojo = this.toPojo();
		pojo.setDescription(description);
		this.provider.getDAO().getMessageDAO().updateVariable(pojo);
		this.description = description;
	}

	@Override
	public String getDescription()
	{
		return this.description;
	}

	@Override
	public int getMessageId()
	{
		return this.messageId;
	}

	@Override
	public String getName()
	{
		return this.name;
	}

	private MessageVariablePojo toPojo()
	{
		return new MessageVariablePojo(this.messageId, this.name, this.description);
	}
}
