package net.rieksen.networkcore.core.message;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.message.pojo.GlobalMessageVariablePojo;
import net.rieksen.networkcore.core.message.pojo.LanguagePojo;
import net.rieksen.networkcore.core.message.pojo.LocaleLanguagePojo;
import net.rieksen.networkcore.core.message.pojo.MessagePojo;
import net.rieksen.networkcore.core.message.pojo.MessageSectionPojo;
import net.rieksen.networkcore.core.message.pojo.MessageTranslationPojo;
import net.rieksen.networkcore.core.message.pojo.MessageVariablePojo;

public class MessageFactoryImpl implements MessageFactory
{

	private NetworkCore provider;

	public MessageFactoryImpl(NetworkCore provider)
	{
		this.provider = provider;
	}

	@Override
	public GlobalMessageVariable createGlobalVariable(GlobalMessageVariablePojo pojo)
	{
		return new GlobalMessageVariableImpl(this.provider, pojo);
	}

	@Override
	public Language createLanguage(LanguagePojo pojo)
	{
		return new LanguageImpl(pojo);
	}

	@Override
	public LocaleLanguage createLocaleLanguage(LocaleLanguagePojo locale)
	{
		return new LocaleLanguageImpl(this.provider, locale);
	}

	@Override
	public Message createMessage(MessagePojo pojo)
	{
		return new MessageImpl(this.provider, pojo);
	}

	@Override
	public MessageSection createSection(MessageSectionPojo pojo)
	{
		return new MessageSectionImpl(this.provider, pojo);
	}

	@Override
	public MessageTranslation createTranslation(MessageTranslationPojo pojo)
	{
		return new MessageTranslationImpl(this.provider, pojo);
	}

	@Override
	public MessageVariable createVariable(MessageVariablePojo pojo)
	{
		return new MessageVariableImpl(this.provider, pojo);
	}
}
