package net.rieksen.networkcore.core.message;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.message.pojo.MessageTranslationPojo;
import net.rieksen.networkcore.core.util.TextUtil;

public class MessageTranslationImpl implements MessageTranslation
{

	private NetworkCore	provider;
	private int			messageId;
	private int			languageId;
	private String		message;
	private MessageType	type;
	private String		defaultMessage;
	private MessageType	defaultMessageType;
	private boolean		useDefaultMessage;
	private boolean		requiresReview;
	private Date		lastReviewed;

	@Deprecated
	public MessageTranslationImpl(NetworkCore provider, int messageId, int languageId, String message)
	{
		Validate.notNull(provider);
		Validate.notNull(message);
		this.provider = provider;
		this.messageId = messageId;
		this.languageId = languageId;
		this.message = message;
	}

	@Deprecated
	public MessageTranslationImpl(NetworkCore provider, int messageId, int languageId, String message, MessageType type)
	{
		Validate.notNull(provider);
		Validate.notNull(message);
		this.provider = provider;
		this.messageId = messageId;
		this.languageId = languageId;
		this.message = message;
		this.type = type;
	}

	public MessageTranslationImpl(NetworkCore provider, MessageTranslationPojo pojo)
	{
		Validate.notNull(provider);
		Validate.notNull(pojo);
		Validate.notNull(pojo.getMessage());
		this.provider = provider;
		this.setPojo(pojo);
	}

	@Override
	public void changeDefaultMessage(String message)
	{
		if (StringUtils.equals(this.defaultMessage, message)) { return; }
		MessageTranslationPojo pojo = this.toPojo();
		pojo.setDefaultMessage(message);
		// For previous versions that didn't have the defaultMessage, we want to
		// make sure useDefaultMessage is set to true whenever the message is
		// default
		if (StringUtils.equals(this.message, message))
		{
			pojo.useDefaultMessage(true);
		}
		pojo.requiresReview(!pojo.useDefaultMessage());
		this.provider.getDAO().getMessageDAO().updateTranslation(pojo);
		this.setPojo(pojo);
		if (this.isDefaultTranslation())
		{
			Message parent = this.getParent();
			parent.getTranslations().stream()
				.filter(translation -> translation.getLanguageId() != this.languageId && !translation.useDefaultMessage())
				.forEach(translation -> translation.changeRequiresReview(true));
		}
	}

	@Override
	public void changeDefaultMessageType(MessageType type)
	{
		if (this.defaultMessageType == type) { return; }
		MessageTranslationPojo pojo = this.toPojo();
		pojo.setDefaultMessageType(type);
		this.provider.getDAO().getMessageDAO().updateTranslation(pojo);
		this.setPojo(pojo);
	}

	@Override
	public void changeMessage(String message)
	{
		Validate.notNull(message, "message cannot be null");
		if (StringUtils.equals(this.message, message)) { return; }
		MessageTranslationPojo pojo = this.toPojo();
		pojo.setMessage(message);
		this.provider.getDAO().getMessageDAO().updateTranslation(pojo);
		this.message = message;
	}

	@Override
	public void changeMessageType(MessageType type)
	{
		if (this.type == type) { return; }
		MessageTranslationPojo pojo = this.toPojo();
		pojo.setMessageType(type);
		this.provider.getDAO().getMessageDAO().updateTranslation(pojo);
		this.setPojo(pojo);
	}

	@Override
	public void changeRequiresReview(boolean requiresReview)
	{
		MessageTranslationPojo pojo = this.toPojo();
		pojo.requiresReview(requiresReview);
		this.provider.getDAO().getMessageDAO().updateTranslation(pojo);
		this.setPojo(pojo);
	}

	@Override
	public String getDefaultMessage()
	{
		return this.defaultMessage;
	}

	@Override
	public MessageType getDefaultMessageType()
	{
		return this.defaultMessageType;
	}

	@Override
	public Language getLanguage()
	{
		return this.provider.getMessageModule().getLanguage(this.languageId);
	}

	@Override
	public int getLanguageId()
	{
		return this.languageId;
	}

	@Override
	public Date getLastReviewed()
	{
		return this.lastReviewed;
	}

	@Override
	public String getMessage()
	{
		return TextUtil.fixNewLineCharacter(this.useDefaultMessage ? this.defaultMessage : this.message);
	}

	@Override
	public int getMessageId()
	{
		return this.messageId;
	}

	@Override
	public MessageType getMessageType()
	{
		return this.useDefaultMessage ? this.defaultMessageType : this.type;
	}

	public Message getParent()
	{
		return this.provider.getMessageModule().getMessage(this.messageId);
	}

	@Override
	public boolean requiresReview()
	{
		return this.requiresReview;
	}

	@Override
	public MessageTranslationPojo toPojo()
	{
		return new MessageTranslationPojo(this.messageId, this.languageId, this.message, this.type, this.defaultMessage,
			this.defaultMessageType, this.useDefaultMessage, this.requiresReview, this.lastReviewed);
	}

	@Override
	public boolean useDefaultMessage()
	{
		return this.useDefaultMessage;
	}

	private boolean isDefaultTranslation()
	{
		return this.provider.getMessageModule().getDefaultLanguage().getLanguageId() == this.languageId;
	}

	private void setPojo(MessageTranslationPojo pojo)
	{
		this.messageId = pojo.getMessageId();
		this.languageId = pojo.getLanguageId();
		this.message = pojo.getMessage();
		this.type = pojo.getMessageType();
		this.defaultMessage = pojo.getDefaultMessage();
		this.defaultMessageType = pojo.getDefaultMessageType();
		this.useDefaultMessage = pojo.useDefaultMessage();
		this.requiresReview = pojo.requiresReview();
		this.lastReviewed = pojo.getLastReviewed();
	}
}
