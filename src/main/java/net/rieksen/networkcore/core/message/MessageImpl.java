package net.rieksen.networkcore.core.message;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.chat.ComponentSerializer;
import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.message.pojo.MessagePojo;
import net.rieksen.networkcore.core.message.pojo.MessageTranslationPojo;
import net.rieksen.networkcore.core.message.pojo.MessageVariablePojo;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.user.pojo.UserLanguagePojo;
import net.rieksen.networkcore.spigot.util.MinecraftUtil;

/**
 * Messages cache variables and translations.
 * 
 * @author Darthmineboy
 */
public class MessageImpl implements Message
{

	private final int					messageId;
	private final int					sectionId;
	private final String				name;
	private String						description;
	private List<MessageVariable>		variables;
	private List<MessageTranslation>	translations;
	/*
	 * ICache implementation
	 */
	private boolean						isCached;
	private boolean						keepCached;
	private long						lastUpdate	= System.currentTimeMillis();
	private NetworkCore					provider;

	@Deprecated
	public MessageImpl(NetworkCore provider, int messageId, int sectionId, String name, String defaultMessage, String description)
	{
		Validate.notNull(provider);
		Validate.notNull(name);
		this.provider = provider;
		this.messageId = messageId;
		this.sectionId = sectionId;
		this.name = name;
		this.description = description;
	}

	public MessageImpl(NetworkCore provider, MessagePojo pojo)
	{
		Validate.notNull(provider);
		Validate.notNull(pojo);
		Validate.notNull(pojo.getName());
		this.provider = provider;
		this.messageId = pojo.getMessageId();
		this.sectionId = pojo.getSectionId();
		this.name = pojo.getName();
		this.description = pojo.getDescription();
	}

	@Override
	public void changeDescription(String description)
	{
		if (StringUtils.equals(this.description, description)) { return; }
		MessagePojo pojo = this.toPojo();
		pojo.setDescription(description);
		this.provider.getDAO().getMessageDAO().updateMessage(pojo);
		this.description = description;
	}

	@Override
	public MessageTranslation createDefaultTranslation(String defaultMessage, MessageType type)
	{
		return this.createTranslation(
			new MessageTranslationPojo(this.messageId, this.provider.getMessageModule().getDefaultLanguage().getLanguageId(),
				defaultMessage, type, defaultMessage, type, true, false, null));
	}

	@Override
	public MessageTranslation createTranslation(int languageId, String message)
	{
		MessageTranslationPojo pojo = new MessageTranslationPojo(this.messageId, languageId, message, MessageType.REGULAR, message,
			MessageType.REGULAR, true, false, new Date());
		return this.createTranslation(pojo);
	}

	@Override
	public MessageTranslation createTranslation(int languageId, String translation, MessageType type)
	{
		return this.createTranslation(
			new MessageTranslationPojo(this.messageId, languageId, translation, type, translation, type, true, false, null));
	}

	@Override
	public MessageTranslation createTranslation(MessageTranslationPojo pojo)
	{
		Validate.notNull(pojo);
		this.initializeTranslations();
		pojo.setMessageId(this.messageId);
		this.provider.getDAO().getMessageDAO().createTranslation(pojo);
		MessageTranslation translation = this.provider.getMessageModule().getFactory().createTranslation(pojo);
		this.translations.add(translation);
		return translation;
	}

	@Override
	public MessageVariable createVariable(MessageVariablePojo pojo)
	{
		Validate.notNull(pojo, "Variable cannot be null");
		this.initializeVariables();
		pojo.setMessageId(this.messageId);
		this.provider.getDAO().getMessageDAO().createVariable(pojo);
		MessageVariable variable = this.provider.getMessageModule().getFactory().createVariable(pojo);
		this.variables.add(variable);
		return variable;
	}

	@Override
	public MessageVariable createVariable(String name, String description)
	{
		return this.createVariable(new MessageVariablePojo(this.messageId, name, description));
	}

	@Override
	public void deleteTranslation(int languageId)
	{
		this.initializeTranslations();
		this.provider.getDAO().getMessageDAO().deleteTranslation(this.messageId, languageId);
		this.translations.removeIf(translation -> translation.getLanguageId() == languageId);
	}

	@Override
	public void deleteVariable(String name)
	{
		MessageVariable variable = this.getVariable(name);
		if (variable != null)
		{
			this.provider.getDAO().getMessageDAO().deleteVariable(this.messageId, name);
			this.variables.remove(variable);
		}
	}

	@Override
	public MessageTranslation getDefaultPluginTranslation()
	{
		Integer languageId = this.getNetworkPlugin().getLanguageId();
		return languageId == null ? null : this.getTranslation(languageId);
	}

	@Override
	public MessageTranslation getDefaultTranslation()
	{
		return this.getTranslation(this.provider.getMessageModule().getDefaultLanguage().getLanguageId());
	}

	@Override
	public String getDescription()
	{
		return this.description;
	}

	@Override
	public String getMessage(User user)
	{
		UserLanguagePreferences preferences = user == null ? null : user.getLanguagePreferences();
		return this.getMessage(preferences);
	}

	@Override
	public String getMessage(UserLanguagePreferences preferences)
	{
		MessageTranslation translation = this.getTranslation(preferences);
		String translatedMessage =
			translation == null ? String.format("Missing default translation for message #%s", this.messageId) : translation.getMessage();
		return this.provider.getMessageModule().replaceGlobalVariables(translatedMessage);
	}

	@Override
	public int getMessageId()
	{
		return this.messageId;
	}

	@Override
	public List<Language> getMissingTranslations()
	{
		return this.provider.getMessageModule().getLanguages().stream()
			.filter(language -> this.getTranslation(language.getLanguageId()) == null).collect(Collectors.toList());
	}

	@Override
	public String getName()
	{
		return this.name;
	}

	@Override
	public int getSectionId()
	{
		return this.sectionId;
	}

	@Override
	public MessageTranslation getTranslation(int languageId)
	{
		this.initializeTranslations();
		return this.translations.stream().filter(translation -> translation.getLanguageId() == languageId).findAny().orElse(null);
	}

	@Override
	public MessageTranslation getTranslation(UserLanguagePreferences preferences)
	{
		if (preferences != null)
		{
			for (UserLanguagePojo language : preferences.getLanguages())
			{
				MessageTranslation messageTranslation = this.getTranslation(language.getLanguageId());
				if (messageTranslation == null)
				{
					continue;
				}
				return messageTranslation;
			}
		}
		MessageTranslation translation = this.getDefaultPluginTranslation();
		return translation == null ? this.getDefaultTranslation() : translation;
	}

	@Override
	public List<MessageTranslation> getTranslations()
	{
		this.initializeTranslations();
		return Collections.unmodifiableList(this.translations);
	}

	@Override
	public MessageVariable getVariable(String name)
	{
		return this.getVariables().stream().filter(var -> var.getName().equalsIgnoreCase(name)).findAny().orElse(null);
	}

	@Override
	public List<MessageVariable> getVariables()
	{
		this.initializeVariables();
		return Collections.unmodifiableList(this.variables);
	}

	@Override
	public boolean isCached()
	{
		return this.isCached;
	}

	@Override
	public void isCached(boolean isCached)
	{
		this.isCached = isCached;
	}

	@Override
	public boolean isCacheExpired()
	{
		return System.currentTimeMillis() - this.lastUpdate > 300000L;
	}

	@Override
	public boolean keepCached()
	{
		return this.keepCached;
	}

	@Override
	public void keepCached(boolean keepCached)
	{
		this.keepCached = keepCached;
	}

	@Override
	public void resetCacheExpiration()
	{
		this.lastUpdate = System.currentTimeMillis();
	}

	@Override
	public void sendMessage(CommandSender sender)
	{
		this.sendMessage(sender, null);
	}

	@Override
	public void sendMessage(CommandSender sender, Map<String, String> variables)
	{
		Validate.notNull(sender, "sender cannot be null");
		MessageTranslation translation = this.getTranslation(this.provider.getUserModule().getUser(sender).getLanguagePreferences());
		String msg = translation.getMessage();
		if (translation.getMessageType() == MessageType.JSON)
		{
			BaseComponent[] components = ComponentSerializer.parse(msg);
			components = MinecraftUtil.replaceVariables(components, variables);
			if (sender instanceof Player)
			{
				((Player) sender).spigot().sendMessage(components);
			} else
			{
				for (BaseComponent c : components)
				{
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', c.toLegacyText()));
				}
			}
		} else
		{
			if (variables != null)
			{
				for (Entry<String, String> entry : variables.entrySet())
				{
					msg = msg.replace(entry.getKey(), entry.getValue());
				}
			}
			msg = this.provider.getMessageModule().replaceGlobalVariables(msg);
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
		}
	}

	@Override
	public void sendMessage(User user)
	{
		this.sendMessage(user, null);
	}

	@Override
	public void sendMessage(User user, Map<String, String> variables)
	{
		String msg = this.getMessage(user);
		if (variables != null)
		{
			for (Entry<String, String> entry : variables.entrySet())
			{
				msg = msg.replace(entry.getKey(), entry.getValue());
			}
		}
		msg = this.provider.getMessageModule().replaceGlobalVariables(msg);
		user.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
	}

	private MessageSection getMessageSection()
	{
		return this.provider.getMessageModule().getSection(this.sectionId);
	}

	private NetworkPlugin getNetworkPlugin()
	{
		return this.getMessageSection().getNetworkPlugin();
	}

	/**
	 * Initializes and loads the translations when not already initialized.
	 */
	private synchronized void initializeTranslations()
	{
		if (this.translations != null) { return; }
		List<MessageTranslation> translations = new ArrayList<>();
		this.provider.getDAO().getMessageDAO().findTranslations(this.messageId).forEach(translation ->
		{
			translations.add(this.provider.getMessageModule().getFactory().createTranslation(translation));
		});
		this.translations = translations;
	}

	/**
	 * Initializes and loads the variables when not already initialized.
	 */
	private synchronized void initializeVariables()
	{
		if (this.variables != null) { return; }
		List<MessageVariable> variables = new ArrayList<>();
		this.provider.getDAO().getMessageDAO().findVariables(this.messageId).forEach(pojo ->
		{
			variables.add(this.provider.getMessageModule().getFactory().createVariable(pojo));
		});
		this.variables = variables;
	}

	private MessagePojo toPojo()
	{
		// use constructor with defaultMessage for compatability reasons
		return new MessagePojo(this.messageId, this.sectionId, this.name, this.description);
	}
}
