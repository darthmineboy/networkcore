package net.rieksen.networkcore.core.message;

import org.apache.commons.lang.Validate;

import net.rieksen.networkcore.core.message.pojo.LanguagePojo;

public class LanguageImpl implements Language
{

	private int		languageId;
	private String	name;

	public LanguageImpl(int languageId, String name)
	{
		Validate.notNull(name, "Name cannot be null");
		this.languageId = languageId;
		this.name = name;
	}

	public LanguageImpl(LanguagePojo pojo)
	{
		this.setPojo(pojo);
	}

	@Override
	public int getLanguageId()
	{
		return this.languageId;
	}

	@Override
	public String getName()
	{
		return this.name;
	}

	@Override
	public LanguagePojo toPojo()
	{
		return new LanguagePojo(this.languageId, this.name);
	}

	private void setPojo(LanguagePojo pojo)
	{
		this.languageId = pojo.getLanguageId();
		this.name = pojo.getName();
	}
}
