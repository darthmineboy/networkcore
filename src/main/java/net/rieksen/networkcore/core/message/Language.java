package net.rieksen.networkcore.core.message;

import net.rieksen.networkcore.core.NetworkCoreAPI;
import net.rieksen.networkcore.core.message.pojo.LanguagePojo;

public interface Language
{

	public static final String	DEFAULT	= "Default";
	public static final String	ENGLISH	= "English";
	public static final String	DUTCH	= "Nederlands";
	public static final String	FRENCH	= "Fran�ais";
	public static final String	GERMAN	= "Deutsch";
	public static final String	SPANISH	= "Espa�ol";

	/**
	 * Get a language by id.
	 * 
	 * @param languageId
	 * @return
	 */
	public static Language getLanguage(int languageId)
	{
		return NetworkCoreAPI.getMessageModule().getLanguage(languageId);
	}

	/**
	 * Get a language by name.
	 * 
	 * @param name
	 * @return
	 */
	public static Language getLanguage(String name)
	{
		return NetworkCoreAPI.getMessageModule().getLanguage(name);
	}

	int getLanguageId();

	String getName();

	LanguagePojo toPojo();
}