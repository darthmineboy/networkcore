package net.rieksen.networkcore.core.message;

import org.apache.commons.lang.Validate;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.message.pojo.LocaleLanguagePojo;

public class LocaleLanguageImpl implements LocaleLanguage
{

	private NetworkCore	provider;
	private String		localeCode;
	private String		languageName;
	private Integer		languageId;

	public LocaleLanguageImpl(NetworkCore provider, LocaleLanguagePojo pojo)
	{
		Validate.notNull(provider);
		this.provider = provider;
		this.setPojo(pojo);
	}

	public LocaleLanguageImpl(NetworkCore provider, String localeCode, String languageName, Integer languageId)
	{
		Validate.notNull(provider);
		Validate.notNull(localeCode);
		this.provider = provider;
		this.localeCode = localeCode;
		this.languageName = languageName;
		this.languageId = languageId;
	}

	@Override
	public void changeLanguageId(Integer languageId)
	{
		if (this.languageId == languageId) { return; }
		LocaleLanguagePojo pojo = this.toPojo();
		pojo.setLanguageId(languageId);
		this.provider.getDAO().getMessageDAO().updateLocaleLanguage(pojo);
		this.languageId = languageId;
	}

	@Override
	public Language getLanguage()
	{
		return this.languageId == null ? null : this.provider.getMessageModule().getLanguage(this.languageId);
	}

	@Override
	public Integer getLanguageId()
	{
		return this.languageId;
	}

	@Override
	public String getLanguageName()
	{
		return this.languageName;
	}

	@Override
	public String getLocaleCode()
	{
		return this.localeCode;
	}

	protected void setPojo(LocaleLanguagePojo pojo)
	{
		this.localeCode = pojo.getLocaleCode();
		this.languageName = pojo.getLanguageName();
		this.languageId = pojo.getLanguageId();
	}

	protected LocaleLanguagePojo toPojo()
	{
		return new LocaleLanguagePojo(this.localeCode, this.languageName, this.languageId);
	}
}
