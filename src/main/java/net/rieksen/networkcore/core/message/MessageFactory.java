package net.rieksen.networkcore.core.message;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.message.pojo.GlobalMessageVariablePojo;
import net.rieksen.networkcore.core.message.pojo.LanguagePojo;
import net.rieksen.networkcore.core.message.pojo.LocaleLanguagePojo;
import net.rieksen.networkcore.core.message.pojo.MessagePojo;
import net.rieksen.networkcore.core.message.pojo.MessageSectionPojo;
import net.rieksen.networkcore.core.message.pojo.MessageTranslationPojo;
import net.rieksen.networkcore.core.message.pojo.MessageVariablePojo;

public interface MessageFactory
{

	@Deprecated
	public static GlobalMessageVariable createGlobalVariable(NetworkCore provider, GlobalMessageVariablePojo pojo)
	{
		return provider.getMessageModule().getFactory().createGlobalVariable(pojo);
	}

	@Deprecated
	public static GlobalMessageVariable createGlobalVariable(NetworkCore provider, int variableId, String name, String replacement)
	{
		return new GlobalMessageVariableImpl(provider, variableId, name, replacement);
	}

	@Deprecated
	public static Language createLanguage(int languageId, String name)
	{
		return new LanguageImpl(languageId, name);
	}

	/**
	 * @deprecated Replaced by non-static
	 *             {@link #createLocaleLanguage(LocaleLanguagePojo)}.
	 * @param provider
	 * @param localeCode
	 * @param languageName
	 * @param languageId
	 * @return
	 */
	@Deprecated
	public static LocaleLanguage createLocaleLanguage(NetworkCore provider, String localeCode, String languageName, Integer languageId)
	{
		return new LocaleLanguageImpl(provider, localeCode, languageName, languageId);
	}

	/**
	 * @deprecated Replaced by non-static {@link #createMessage(MessagePojo)}.
	 * @param provider
	 * @param messageId
	 * @param sectionId
	 * @param name
	 * @param defaultMessage
	 * @param description
	 * @return
	 */
	@Deprecated
	public static Message createMessage(NetworkCore provider, int messageId, int sectionId, String name, String defaultMessage,
		String description)
	{
		return new MessageImpl(provider, messageId, sectionId, name, defaultMessage, description);
	}

	/**
	 * @deprecated Replaced by non-static {@link #createMessage(MessagePojo)}.
	 * @param provider
	 * @param pojo
	 * @return
	 */
	@Deprecated
	public static Message createMessage(NetworkCore provider, MessagePojo pojo)
	{
		return provider.getMessageModule().getFactory().createMessage(pojo);
	}

	/**
	 * @deprecated Replaced by non-static
	 *             {@link #createSection(MessageSectionPojo)}.
	 * @param provider
	 * @param sectionId
	 * @param pluginId
	 * @param name
	 * @param description
	 * @return
	 */
	@Deprecated
	public static MessageSection createSection(NetworkCore provider, int sectionId, int pluginId, String name, String description)
	{
		return new MessageSectionImpl(provider, sectionId, pluginId, name, description);
	}

	/**
	 * @deprecated Replaced by non-static
	 *             {@link #createSection(MessageSectionPojo)}.
	 * @param provider
	 * @param pojo
	 * @return
	 */
	@Deprecated
	public static MessageSection createSection(NetworkCore provider, MessageSectionPojo pojo)
	{
		return provider.getMessageModule().getFactory().createSection(pojo);
	}

	/**
	 * @deprecated Replaced by non-static
	 *             {@link #createTranslation(MessageTranslationPojo)}.
	 * @param provider
	 * @param messageId
	 * @param languageId
	 * @param message
	 * @return
	 */
	@Deprecated
	public static MessageTranslation createTranslation(NetworkCore provider, int messageId, int languageId, String message)
	{
		return new MessageTranslationImpl(provider, messageId, languageId, message);
	}

	/**
	 * @deprecated Replaced by non-static
	 *             {@link #createTranslation(MessageTranslationPojo)}.
	 * @param provider
	 * @param messageId
	 * @param languageId
	 * @param message
	 * @param type
	 * @return
	 */
	@Deprecated
	public static MessageTranslation createTranslation(NetworkCore provider, int messageId, int languageId, String message,
		MessageType type)
	{
		return new MessageTranslationImpl(provider, messageId, languageId, message, type);
	}

	/**
	 * @deprecated Replaced by non-static
	 *             {@link #createTranslation(MessageTranslationPojo)}.
	 * @param provider
	 * @param pojo
	 * @return
	 */
	@Deprecated
	public static MessageTranslation createTranslation(NetworkCore provider, MessageTranslationPojo pojo)
	{
		return provider.getMessageModule().getFactory().createTranslation(pojo);
	}

	/**
	 * @deprecated Replaced by non-static
	 *             {@link #createVariable(MessageVariablePojo)}.
	 * @param provider
	 * @param messageId
	 * @param name
	 * @param description
	 * @return
	 */
	@Deprecated
	public static MessageVariable createVariable(NetworkCore provider, int messageId, String name, String description)
	{
		return new MessageVariableImpl(provider, messageId, name, description);
	}

	/**
	 * @deprecated Replaced by non-static
	 *             {@link #createVariable(MessageVariablePojo)}.
	 * @param provider
	 * @param pojo
	 * @return
	 */
	@Deprecated
	public static MessageVariable createVariable(NetworkCore provider, MessageVariablePojo pojo)
	{
		return provider.getMessageModule().getFactory().createVariable(pojo);
	}

	GlobalMessageVariable createGlobalVariable(GlobalMessageVariablePojo pojo);

	Language createLanguage(LanguagePojo pojo);

	LocaleLanguage createLocaleLanguage(LocaleLanguagePojo locale);

	Message createMessage(MessagePojo pojo);

	MessageSection createSection(MessageSectionPojo pojo);

	MessageTranslation createTranslation(MessageTranslationPojo pojo);

	MessageVariable createVariable(MessageVariablePojo pojo);
}
