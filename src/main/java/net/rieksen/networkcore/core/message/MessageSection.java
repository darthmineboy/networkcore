package net.rieksen.networkcore.core.message;

import java.util.List;

import net.rieksen.networkcore.core.NetworkCoreAPI;
import net.rieksen.networkcore.core.cache.ICacheable;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;

public interface MessageSection extends ICacheable
{

	/**
	 * Get a section by id. Can return null.
	 * 
	 * @param sectionId
	 * @return
	 */
	public static MessageSection getSection(int sectionId)
	{
		return NetworkCoreAPI.getMessageModule().getSection(sectionId);
	}

	/**
	 * Get a section by plugin id and name. Can return null.
	 * 
	 * @param pluginId
	 * @param name
	 * @return
	 */
	public static MessageSection getSection(int pluginId, String name)
	{
		return NetworkPlugin.getPlugin(pluginId).getMessageSection(name);
	}

	/**
	 * Get a section by plugin name and name. Can return null.
	 * 
	 * @param pluginName
	 * @param name
	 * @return
	 */
	public static MessageSection getSection(String pluginName, String name)
	{
		NetworkPlugin plugin = NetworkPlugin.getPlugin(pluginName);
		if (plugin == null) { return null; }
		return MessageSection.getSection(plugin.getPluginId(), name);
	}

	void changeDescription(String description);

	Message createMessage(String messageName, String description);

	Message createMessage(String messageName, String defaultMessage, MessageType type, String description);

	void deleteMessage(String name);

	/**
	 * Get the description.
	 * 
	 * @return
	 */
	String getDescription();

	Message getMessage(int messageId);

	/**
	 * Get the message from this section. The message is taken from cache when
	 * available. If the cache has expired the message is refreshed.
	 * 
	 * @param name
	 * @return
	 */
	Message getMessage(String name) throws MessageNotFoundException;

	List<Message> getMessages();

	/**
	 * Get the name.
	 * 
	 * @return
	 */
	String getName();

	NetworkPlugin getNetworkPlugin();

	/**
	 * Get the pluginId.
	 * 
	 * @return
	 */
	int getPluginId();

	/**
	 * Get the sectionId. Can return null.
	 * 
	 * @return
	 */
	int getSectionId();

	@Override
	boolean isCached();

	@Override
	void isCached(boolean isCached);

	@Override
	boolean isCacheExpired();

	@Override
	boolean keepCached();

	@Override
	void keepCached(boolean keepCached);

	void refreshMessageCache();

	@Override
	void resetCacheExpiration();
}