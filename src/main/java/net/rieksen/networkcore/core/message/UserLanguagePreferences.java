package net.rieksen.networkcore.core.message;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.Validate;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.user.pojo.UserLanguagePojo;

/**
 * Holds user language preferences.
 * 
 * @author Darthmineboy
 */
public class UserLanguagePreferences
{

	private final NetworkCore		provider;
	private final int				userId;
	private List<UserLanguagePojo>	languages;

	/**
	 * @param userId
	 * @param languageList
	 * @throws IllegalStateException
	 *             when userId is null
	 */
	public UserLanguagePreferences(NetworkCore provider, int userId, List<UserLanguagePojo> languages)
	{
		Validate.notNull(provider);
		Validate.notNull(languages);
		this.provider = provider;
		this.userId = userId;
		this.languages = languages;
	}

	public void addLanguage(int languageId)
	{
		this.setLanguage(languageId, this.calculateHighestPriority() + 1);
	}

	public void deleteLanguage(int languageId)
	{
		this.languages.removeIf(language -> language.getLanguageId() == languageId);
		this.provider.getDAO().getUserDAO().deleteUserLanguage(this.userId, languageId);
	}

	public List<UserLanguagePojo> getLanguages()
	{
		return this.languages;
	}

	public List<Language> getMissingLanguages()
	{
		return this.provider.getMessageModule().getLanguages().stream().filter(language -> !this.hasLanguage(language.getLanguageId()))
			.collect(Collectors.toList());
	}

	public boolean hasLanguage(int languageId)
	{
		return this.languages.stream().filter(language -> language.getLanguageId() == languageId).findAny().isPresent();
	}

	public boolean hasLanguages()
	{
		return !this.languages.isEmpty();
	}

	public void setLanguage(int languageId)
	{
		this.deleteLanguages();
		this.setLanguage(languageId, 0);
	}

	public void setLanguage(int languageId, int priority)
	{
		UserLanguagePojo pojo = new UserLanguagePojo(this.userId, languageId, priority);
		this.provider.getDAO().getUserDAO().createUserLanguage(pojo);
		this.languages.add(pojo);
	}

	private int calculateHighestPriority()
	{
		int priority = 0;
		for (UserLanguagePojo pojo : this.languages)
		{
			priority = pojo.getPriority() > priority ? pojo.getPriority() : priority;
		}
		return priority;
	}

	private void deleteLanguages()
	{
		this.provider.getDAO().getUserDAO().deleteUserLanguages(this.userId);
		this.languages.clear();
	}
}
