package net.rieksen.networkcore.core.message;

public enum MessageType
{
	REGULAR('R'),
	JSON('J');

	public static MessageType fromId(char id)
	{
		for (MessageType t : MessageType.values())
		{
			if (t.id == id) { return t; }
		}
		throw new IllegalStateException(String.format("Unknown MessageType '%s', plugin is outdated or data is corrupt", id));
	}

	private char id;

	private MessageType(char id)
	{
		this.id = id;
	}

	public char getId()
	{
		return this.id;
	}
}
