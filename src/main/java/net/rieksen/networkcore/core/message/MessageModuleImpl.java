package net.rieksen.networkcore.core.message;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.cache.CacheCleanupThread;
import net.rieksen.networkcore.core.cache.CacheComponent;
import net.rieksen.networkcore.core.message.pojo.GlobalMessageVariablePojo;
import net.rieksen.networkcore.core.message.pojo.LanguagePojo;
import net.rieksen.networkcore.core.message.pojo.LocaleLanguagePojo;
import net.rieksen.networkcore.core.message.pojo.MessagePojo;
import net.rieksen.networkcore.core.message.pojo.MessageSectionPojo;
import net.rieksen.networkcore.core.module.ModuleImpl;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;

public class MessageModuleImpl extends ModuleImpl implements MessageModule
{

	private final NetworkCore							provider;
	private final MessageFactory						factory;
	private CacheComponent<List<GlobalMessageVariable>>	globalVariableCache	=
		new CacheComponent<List<GlobalMessageVariable>>(TimeUnit.MINUTES.toMillis(5))
																				{

																					@Override
																					public void refreshCache()
																					{
																						List<GlobalMessageVariable> list =
																							new ArrayList<>();
																						MessageModuleImpl.this.provider.getDAO()
																							.getMessageDAO().findGlobalVariables()
																							.forEach(pojo ->
																																									{
																																										list.add(
																																											MessageModuleImpl.this.factory
																																												.createGlobalVariable(
																																													pojo));
																																									});
																						this.cachedObject = list;
																					}
																				};
	private Collection<MessageSection>					sections			=
		Collections.synchronizedCollection(new HashSet<MessageSection>());
	private CacheCleanupThread<MessageSection>			sectionCacheThread;

	public MessageModuleImpl(NetworkCore provider)
	{
		this.provider = provider;
		this.factory = new MessageFactoryImpl(provider);
	}

	@Override
	public GlobalMessageVariable createGlobalVariable(GlobalMessageVariablePojo pojo)
	{
		pojo.setVariableId(this.provider.getDAO().getMessageDAO().createGlobalVariable(pojo));
		GlobalMessageVariable variable = this.getFactory().createGlobalVariable(pojo);
		this.globalVariableCache.getCache().add(variable);
		return variable;
	}

	@Override
	public Language createLanguage(LanguagePojo pojo)
	{
		int languageId = this.provider.getDAO().getMessageDAO().createLanguage(pojo);
		pojo.setLanguageId(languageId);
		return this.getFactory().createLanguage(pojo);
	}

	@Override
	public void deleteLanguage(int languageId)
	{
		this.provider.getDAO().getMessageDAO().deleteLanguage(languageId);
	}

	@Override
	public Language getDefaultLanguage()
	{
		Language language = this.getLanguage(Language.DEFAULT);
		if (language == null)
		{
			language = this.createLanguage(new LanguagePojo(0, Language.DEFAULT));
		}
		return language;
	}

	@Override
	public MessageFactory getFactory()
	{
		return this.factory;
	}

	@Override
	public GlobalMessageVariable getGlobalVariable(String name)
	{
		return this.globalVariableCache.getCache().stream().filter(var -> var.getName().equalsIgnoreCase(name)).findAny().orElse(null);
	}

	@Override
	public Language getLanguage(int languageId)
	{
		LanguagePojo pojo = this.provider.getDAO().getMessageDAO().findLanguage(languageId);
		// TODO language cache
		return pojo == null ? null : this.getFactory().createLanguage(pojo);
	}

	@Override
	public Language getLanguage(String languageName)
	{
		LanguagePojo pojo = this.provider.getDAO().getMessageDAO().findLanguage(languageName);
		// TODO language cache
		return pojo == null ? null : this.getFactory().createLanguage(pojo);
	}

	@Override
	public List<Language> getLanguages()
	{
		List<Language> languages = new ArrayList<>();
		this.provider.getDAO().getMessageDAO().findLanguages().forEach(pojo ->
		{
			languages.add(this.getFactory().createLanguage(pojo));
		});
		return languages;
	}

	@Override
	public LocaleLanguage getLocaleLanguage(String locale)
	{
		LocaleLanguagePojo pojo = this.provider.getDAO().getMessageDAO().findLocale(locale);
		return pojo == null ? null : this.getFactory().createLocaleLanguage(pojo);
	}

	@Override
	public Message getMessage(int messageId)
	{
		Message message;
		for (NetworkPlugin plugin : this.provider.getPluginModule().getPlugins())
		{
			if ((message = plugin.getMessage(messageId)) != null) { return message; }
		}
		MessagePojo pojo = this.provider.getDAO().getMessageDAO().findMessage(messageId);
		return pojo == null ? null : this.getFactory().createMessage(pojo);
	}

	@Override
	public MessageSection getSection(int sectionId)
	{
		MessageSectionPojo pojo = this.provider.getDAO().getMessageDAO().findSection(sectionId);
		if (pojo == null) { return null; }
		NetworkPlugin plugin = NetworkPlugin.getPlugin(pojo.getPluginId());
		if (plugin == null) { return null; }
		return plugin.getMessageSection(pojo.getName());
	}

	@Override
	public String replaceGlobalVariables(String message)
	{
		if (message == null) { return null; }
		for (GlobalMessageVariable var : this.globalVariableCache.getCache())
		{
			if (message.contains(var.getName()))
			{
				message = message.replace(var.getName(), var.getReplacement());
			}
		}
		return message;
	}

	@Override
	protected void destroyModule() throws Exception
	{
		this.sectionCacheThread.disable();
	}

	@Override
	protected void initModule() throws Exception
	{
		this.sectionCacheThread = new CacheCleanupThread<MessageSection>("MessageManager", "messagesection", TimeUnit.SECONDS.toMillis(300))
		{

			@Override
			public Collection<MessageSection> getCache()
			{
				return MessageModuleImpl.this.sections;
			}
		};
		this.sectionCacheThread.start();
	}
}
