package net.rieksen.networkcore.core.message.pojo;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import net.rieksen.networkcore.core.message.MessageType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MessageTranslationPojo
{

	private int			messageId;
	private int			languageId;
	private String		message;
	private MessageType	messageType;
	private String		defaultMessage;
	private MessageType	defaultMessageType;
	private boolean		useDefaultMessage;
	private boolean		requiresReview;
	private Date		lastReviewed;

	public MessageTranslationPojo()
	{}

	@Deprecated
	public MessageTranslationPojo(int messageId, int languageId, String message)
	{
		this.messageId = messageId;
		this.languageId = languageId;
		this.message = message;
	}

	@Deprecated
	public MessageTranslationPojo(int messageId, int languageId, String message, MessageType messageType)
	{
		this.messageId = messageId;
		this.languageId = languageId;
		this.message = message;
		this.messageType = messageType;
	}

	@Deprecated
	public MessageTranslationPojo(int messageId, int languageId, String message, MessageType messageType, String defaultMessage,
		boolean useDefaultMessage, boolean requiresReview, Date lastReviewed)
	{
		this.messageId = messageId;
		this.languageId = languageId;
		this.message = message;
		this.messageType = messageType;
		this.defaultMessage = defaultMessage;
		this.useDefaultMessage = useDefaultMessage;
		this.requiresReview = requiresReview;
		this.lastReviewed = lastReviewed;
	}

	public MessageTranslationPojo(int messageId, int languageId, String message, MessageType messageType, String defaultMessage,
		MessageType defaultMessageType, boolean useDefaultMessage, boolean requiresReview, Date lastReviewed)
	{
		this.messageId = messageId;
		this.languageId = languageId;
		this.message = message;
		this.messageType = messageType;
		this.defaultMessage = defaultMessage;
		this.defaultMessageType = defaultMessageType;
		this.useDefaultMessage = useDefaultMessage;
		this.requiresReview = requiresReview;
		this.lastReviewed = lastReviewed;
	}

	public String getDefaultMessage()
	{
		return this.defaultMessage;
	}

	public MessageType getDefaultMessageType()
	{
		return this.defaultMessageType;
	}

	public int getLanguageId()
	{
		return this.languageId;
	}

	public Date getLastReviewed()
	{
		return this.lastReviewed;
	}

	public String getMessage()
	{
		return this.message;
	}

	public int getMessageId()
	{
		return this.messageId;
	}

	public MessageType getMessageType()
	{
		return this.messageType;
	}

	/**
	 * @deprecated Replaced by {@link #getMessageType()}.
	 * @return
	 */
	@Deprecated
	public MessageType getType()
	{
		return this.messageType;
	}

	public boolean requiresReview()
	{
		return this.requiresReview;
	}

	public void requiresReview(boolean requiresReview)
	{
		this.requiresReview = requiresReview;
	}

	public void setDefaultMessage(String defaultMessage)
	{
		this.defaultMessage = defaultMessage;
	}

	public void setDefaultMessageType(MessageType defaultMessageType)
	{
		this.defaultMessageType = defaultMessageType;
	}

	public void setLanguageId(int languageId)
	{
		this.languageId = languageId;
	}

	public void setLastReviewed(Date lastReviewed)
	{
		this.lastReviewed = lastReviewed;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public void setMessageId(int messageId)
	{
		this.messageId = messageId;
	}

	public void setMessageType(MessageType messageType)
	{
		this.messageType = messageType;
	}

	/**
	 * @deprecated Replaced by {@link #setMessageType(MessageType)}.
	 * @param type
	 */
	@Deprecated
	public void setType(MessageType type)
	{
		this.messageType = type;
	}

	public boolean useDefaultMessage()
	{
		return this.useDefaultMessage;
	}

	public void useDefaultMessage(boolean useDefault)
	{
		this.useDefaultMessage = useDefault;
	}
}
