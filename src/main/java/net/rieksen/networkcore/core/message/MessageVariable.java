package net.rieksen.networkcore.core.message;

public interface MessageVariable
{

	/**
	 * Get the description.
	 * 
	 * @return
	 */
	String getDescription();

	/**
	 * Get the messageId.
	 * 
	 * @return
	 */
	int getMessageId();

	/**
	 * Get the name.
	 * 
	 * @return
	 */
	String getName();

	/**
	 * Set the description.
	 * 
	 * @param description
	 */
	void changeDescription(String description);
}