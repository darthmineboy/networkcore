package net.rieksen.networkcore.core.message.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GlobalMessageVariablePojo
{

	private int		variableId;
	private String	variableName;
	private String	replacement;

	public GlobalMessageVariablePojo()
	{}

	public GlobalMessageVariablePojo(int variableId, String name, String replacement)
	{
		this.variableId = variableId;
		this.variableName = name;
		this.replacement = replacement;
	}

	public String getName()
	{
		return this.variableName;
	}

	public String getReplacement()
	{
		return this.replacement;
	}

	public int getVariableId()
	{
		return this.variableId;
	}

	public void setName(String name)
	{
		this.variableName = name;
	}

	public void setReplacement(String replacement)
	{
		this.replacement = replacement;
	}

	public void setVariableId(int variableId)
	{
		this.variableId = variableId;
	}
}
