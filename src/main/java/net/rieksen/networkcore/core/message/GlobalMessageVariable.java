package net.rieksen.networkcore.core.message;

public interface GlobalMessageVariable
{

	void changeName(String name);

	void changeReplacement(String replacement);

	/**
	 * The unique name.
	 * 
	 * @return
	 */
	String getName();

	/**
	 * The replacing string.
	 * 
	 * @return
	 */
	String getReplacement();

	int getVariableId();
}