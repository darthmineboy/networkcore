package net.rieksen.networkcore.core.message;

import java.util.Date;

import net.rieksen.networkcore.core.dao.MessageDAO;
import net.rieksen.networkcore.core.message.pojo.MessageTranslationPojo;

public interface MessageTranslation
{

	/**
	 * Sets {@link #getDefaultMessage()} to the value provided, and updates the
	 * value in the {@link MessageDAO}. If {@link #useDefaultMessage()} is
	 * false, the {@link #requiresReview()} is set to true.
	 * <p>
	 * To extend on that, if this is the default translation, the
	 * {@link #requiresReview()} of all other translations is set to true.
	 * 
	 * @param message
	 */
	void changeDefaultMessage(String message);

	/**
	 * Sets {@link #getDefaultMessageType()} to the value provided, and updates
	 * the value in the {@link MessageDAO}.
	 * 
	 * @param type
	 */
	void changeDefaultMessageType(MessageType type);

	/**
	 * Sets {@link #getMessage()} to the value provided, and updates the value
	 * in the {@link MessageDAO}.
	 * 
	 * @param message
	 */
	void changeMessage(String message);

	/**
	 * Sets {@link #getMessageType()} to the value provided, and updates the
	 * value in the {@link MessageDAO}.
	 * 
	 * @param type
	 */
	void changeMessageType(MessageType type);

	/**
	 * Sets {@link #requiresReview()} to the value provided, and updates the
	 * value in the {@link MessageDAO}.
	 * 
	 * @param requiresReview
	 */
	void changeRequiresReview(boolean requiresReview);

	/**
	 * The default message provided by the plugin. This default message should
	 * only be changed by the plugin.
	 * 
	 * @see #getMessage()
	 * @return
	 */
	String getDefaultMessage();

	MessageType getDefaultMessageType();

	Language getLanguage();

	int getLanguageId();

	/**
	 * The last time the translation was reviewed.
	 * 
	 * @return
	 */
	Date getLastReviewed();

	/**
	 * If {@link #useDefaultMessage()} is true this returns
	 * {@link #getDefaultMessage()}.
	 * 
	 * @return
	 */
	String getMessage();

	int getMessageId();

	/**
	 * If {@link #useDefaultMessage()} is true this returns
	 * {@link #getDefaultMessageType()}.
	 * 
	 * @return
	 */
	MessageType getMessageType();

	/**
	 * Whether this translation should be reviewed. This is useful when messages
	 * introduce new variables, and the translation could/should be changed.
	 * 
	 * @return
	 */
	boolean requiresReview();

	MessageTranslationPojo toPojo();

	/**
	 * If true {@link #getMessage()} returns {@link #getDefaultMessage()}.
	 * 
	 * @return
	 */
	boolean useDefaultMessage();
}