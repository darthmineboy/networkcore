package net.rieksen.networkcore.core.message.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class MessageSectionPojo
{

	private int		sectionId;
	private int		pluginId;
	private String	sectionName;
	private String	description;

	public MessageSectionPojo()
	{}

	public MessageSectionPojo(int sectionId, int pluginId, String name, String description)
	{
		this.sectionId = sectionId;
		this.pluginId = pluginId;
		this.sectionName = name;
		this.description = description;
	}

	public String getDescription()
	{
		return this.description;
	}

	public String getName()
	{
		return this.sectionName;
	}

	public int getPluginId()
	{
		return this.pluginId;
	}

	public int getSectionId()
	{
		return this.sectionId;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setName(String name)
	{
		this.sectionName = name;
	}

	public void setPluginId(int pluginId)
	{
		this.pluginId = pluginId;
	}

	public void setSectionId(int sectionId)
	{
		this.sectionId = sectionId;
	}
}
