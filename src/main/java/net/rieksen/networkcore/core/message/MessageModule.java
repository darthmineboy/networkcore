package net.rieksen.networkcore.core.message;

import java.util.List;

import net.rieksen.networkcore.core.dao.MessageDAO;
import net.rieksen.networkcore.core.message.pojo.GlobalMessageVariablePojo;
import net.rieksen.networkcore.core.message.pojo.LanguagePojo;
import net.rieksen.networkcore.core.module.Module;

public interface MessageModule extends Module
{

	GlobalMessageVariable createGlobalVariable(GlobalMessageVariablePojo pojo);

	Language createLanguage(LanguagePojo pojo);

	void deleteLanguage(int languageId);

	/**
	 * The default language. If it does not exists it is created.
	 * 
	 * @return
	 */
	Language getDefaultLanguage();

	MessageFactory getFactory();

	GlobalMessageVariable getGlobalVariable(String name);

	Language getLanguage(int languageId);

	Language getLanguage(String languageName);

	List<Language> getLanguages();

	LocaleLanguage getLocaleLanguage(String locale);

	/**
	 * Searches the cache first for a message. If no cached value is available,
	 * the {@link MessageDAO} is used.
	 * 
	 * @param messageId
	 * @return null if message does not exist
	 */
	Message getMessage(int messageId);

	MessageSection getSection(int sectionId);

	/**
	 * Replaces all occurrences of global variables in the message, and returns
	 * the resulting string.
	 * 
	 * @param message
	 * @return
	 */
	String replaceGlobalVariables(String message);
}