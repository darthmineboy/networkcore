package net.rieksen.networkcore.core.enums;

import net.rieksen.networkcore.core.message.Message;

public enum NetworkCoreSocketMessage
{
	UPDATE_SERVER,
	RESTART_SERVER,
	STOP_SERVER,
	RELOAD_SERVER,
	DELAYED_SOCKET_MODULE_RESTART,
	/**
	 * Broadcast a string.
	 */
	BROADCAST,
	/**
	 * Broadcast a {@link Message}.
	 */
	BROADCAST_MESSAGE,
	/**
	 * Send a string message to a user.
	 */
	SEND_MESSAGE,
	/**
	 * Send a user to a server.
	 */
	SEND_USER_TO_SERVER,
	/**
	 * Status check.
	 */
	STATUS;
}
