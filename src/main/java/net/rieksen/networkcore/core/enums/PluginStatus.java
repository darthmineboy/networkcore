package net.rieksen.networkcore.core.enums;

public enum PluginStatus
{
	UNINITIALIZED(false),
	ENABLING(true),
	SUCCESS(true),
	DISABLING(true),
	DISABLED(false),
	ERROR(false);

	private final boolean active;

	private PluginStatus(boolean active)
	{
		this.active = active;
	}

	/**
	 * Whether the plugin is in a mode where normal operation is expected.
	 * 
	 * @return
	 */
	public boolean isActive()
	{
		return this.active;
	}
}
