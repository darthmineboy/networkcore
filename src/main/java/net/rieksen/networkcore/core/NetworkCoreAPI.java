package net.rieksen.networkcore.core;

import java.util.logging.Logger;

import net.rieksen.networkcore.core.dao.DAOManager;
import net.rieksen.networkcore.core.message.MessageModule;
import net.rieksen.networkcore.core.network.NetworkModule;
import net.rieksen.networkcore.core.option.OptionModule;
import net.rieksen.networkcore.core.plugin.PluginModule;
import net.rieksen.networkcore.core.server.ServerModule;
import net.rieksen.networkcore.core.socket.ServerSocketModule;
import net.rieksen.networkcore.core.user.UserModule;
import net.rieksen.networkcore.core.world.WorldModule;

public class NetworkCoreAPI
{

	private static NetworkCore provider;

	public static void debug(String message)
	{
		NetworkCoreAPI.provider.debug(message);
	}

	public static DAOManager getDAO()
	{
		return NetworkCoreAPI.provider.getDAO();
	}

	/**
	 * Get the logged used by NetworkCore.
	 * 
	 * @return
	 */
	public static Logger getLogger()
	{
		return NetworkCoreAPI.provider.getLogger();
	}

	public static MessageModule getMessageModule()
	{
		return NetworkCoreAPI.provider.getMessageModule();
	}

	public static NetworkModule getNetworkModule()
	{
		return NetworkCoreAPI.provider.getNetworkModule();
	}

	public static OptionModule getOptionModule()
	{
		return NetworkCoreAPI.provider.getOptionModule();
	}

	public static PluginModule getPluginModule()
	{
		return NetworkCoreAPI.provider.getPluginModule();
	}

	public static NetworkCore getProvider()
	{
		return NetworkCoreAPI.provider;
	}

	public static ServerModule getServerModule()
	{
		return NetworkCoreAPI.provider.getServerModule();
	}

	public static ServerSocketModule getSocketModule()
	{
		return NetworkCoreAPI.provider.getSocketModule();
	}

	public static UserModule getUserModule()
	{
		return NetworkCoreAPI.provider.getUserModule();
	}

	public static WorldModule getWorldModule()
	{
		return NetworkCoreAPI.provider.getWorldModule();
	}

	public static void setProvider(NetworkCore networkCore)
	{
		NetworkCoreAPI.provider = networkCore;
	}
}
