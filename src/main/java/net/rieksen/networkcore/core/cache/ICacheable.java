package net.rieksen.networkcore.core.cache;

public interface ICacheable
{

	public boolean isCached();

	public void isCached(boolean isCached);

	public boolean isCacheExpired();

	public boolean keepCached();

	public void keepCached(boolean keepCached);

	public void resetCacheExpiration();
}
