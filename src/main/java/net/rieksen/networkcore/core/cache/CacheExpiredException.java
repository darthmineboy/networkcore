package net.rieksen.networkcore.core.cache;

@SuppressWarnings("serial")
public class CacheExpiredException extends CacheException
{

	public CacheExpiredException(String message)
	{
		super(message);
	}

	public CacheExpiredException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public CacheExpiredException(Throwable cause)
	{
		super(cause);
	}
}
