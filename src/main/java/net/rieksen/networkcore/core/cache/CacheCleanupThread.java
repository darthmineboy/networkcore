package net.rieksen.networkcore.core.cache;

import java.util.Collection;
import java.util.Iterator;

public abstract class CacheCleanupThread<T extends ICacheable> extends Thread
{

	private boolean	enabled;
	private long	pause;

	public CacheCleanupThread(String manager, String name, long pause)
	{
		this.pause = pause;
	}

	public void disable()
	{
		this.enabled = false;
	}

	public abstract Collection<T> getCache();

	@Override
	public void run()
	{
		this.pause();
		while (this.enabled)
		{
			Collection<T> cache = this.getCache();
			if (cache != null)
			{
				Iterator<T> it = cache.iterator();
				while (it.hasNext())
				{
					T cacheObject = it.next();
					if (cacheObject.keepCached() || !cacheObject.isCacheExpired())
					{
						continue;
					}
					cacheObject.isCached(false);
					it.remove();
				}
			}
			this.pause();
		}
	}

	@Override
	public synchronized void start()
	{
		super.start();
		this.enabled = true;
	}

	private void pause()
	{
		try
		{
			Thread.sleep(this.pause);
		} catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}
}
