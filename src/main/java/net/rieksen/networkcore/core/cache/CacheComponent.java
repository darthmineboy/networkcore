package net.rieksen.networkcore.core.cache;

/**
 * Caching components in an aggregate.
 * 
 * @author Darthmineboy
 * @param <T>
 */
public abstract class CacheComponent<T>
{

	protected T			cachedObject;
	protected long		lastRefreshTime;
	protected long		expirationTime;
	protected boolean	refreshFirst;

	public CacheComponent(long expirationTime)
	{
		this(null, expirationTime, true);
	}

	/**
	 * @param expirationTime
	 * @param refreshFirst
	 */
	public CacheComponent(long expirationTime, boolean refreshFirst)
	{
		this(null, expirationTime, refreshFirst);
	}

	/**
	 * @param cachedObject
	 * @param expirationTime
	 * @param refreshFirst
	 *            ;
	 */
	public CacheComponent(T cachedObject, long expirationTime, boolean refreshFirst)
	{
		this(cachedObject, System.currentTimeMillis(), expirationTime, refreshFirst);
	}

	/**
	 * @param cachedObject
	 * @param lastRefreshTime
	 * @param expirationTime
	 * @param refreshFirst
	 *            whether the cache should be refreshed the first time
	 *            {@link #getCache()} is invoked
	 */
	public CacheComponent(T cachedObject, long lastRefreshTime, long expirationTime, boolean refreshFirst)
	{
		this.cachedObject = cachedObject;
		this.lastRefreshTime = lastRefreshTime;
		this.expirationTime = expirationTime;
		this.refreshFirst = refreshFirst;
	}

	/**
	 * Get the cached value.
	 * 
	 * @return
	 */
	public T getCache()
	{
		if (this.isCacheExpired() || this.refreshFirst)
		{
			this.refreshCache();
			this.resetLastRefreshTime();
			this.refreshFirst = false;
		}
		return this.cachedObject;
	}

	public boolean isCacheExpired()
	{
		return System.currentTimeMillis() >= this.lastRefreshTime + this.expirationTime;
	}

	/**
	 * Refreshes the cached object.
	 */
	public abstract void refreshCache();

	/**
	 * Sets the cached object.
	 * 
	 * @param cachedObject
	 */
	public void setCache(T cachedObject)
	{
		this.cachedObject = cachedObject;
	}

	/**
	 * Sets the last refresh time equal to the current system time.
	 */
	protected void resetLastRefreshTime()
	{
		this.lastRefreshTime = System.currentTimeMillis();
	}
}
