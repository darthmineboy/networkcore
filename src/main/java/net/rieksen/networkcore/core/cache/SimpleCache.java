package net.rieksen.networkcore.core.cache;

public class SimpleCache implements ICacheable
{

	private long	expirationTime;
	private long	lastRefresh;
	private boolean	keepCached;
	private boolean	isCached;

	public SimpleCache(long expirationTime)
	{
		this(expirationTime, false);
	}

	public SimpleCache(long expirationTime, boolean isCached)
	{
		this(expirationTime, System.currentTimeMillis(), false, isCached);
	}

	public SimpleCache(long expirationTime, long lastRefresh, boolean keepCached, boolean isCached)
	{
		this.expirationTime = expirationTime;
		this.lastRefresh = lastRefresh;
		this.keepCached = keepCached;
		this.isCached = isCached;
	}

	@Override
	public boolean isCached()
	{
		return this.isCached;
	}

	@Override
	public void isCached(boolean isCached)
	{
		this.isCached = isCached;
	}

	@Override
	public boolean isCacheExpired()
	{
		if (this.keepCached) { return false; }
		return System.currentTimeMillis() > this.expirationTime + this.lastRefresh;
	}

	@Override
	public boolean keepCached()
	{
		return this.keepCached;
	}

	@Override
	public void keepCached(boolean keepCached)
	{
		this.keepCached = keepCached;
	}

	@Override
	public void resetCacheExpiration()
	{
		this.lastRefresh = System.currentTimeMillis();
	}
}
