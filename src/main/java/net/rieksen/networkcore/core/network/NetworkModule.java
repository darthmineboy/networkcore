package net.rieksen.networkcore.core.network;

import java.util.Map;

import net.md_5.bungee.api.chat.BaseComponent;
import net.rieksen.networkcore.core.module.Module;
import net.rieksen.networkcore.core.user.User;

public interface NetworkModule extends Module
{

	/**
	 * Broadcasts a message to the targetServer, or all servers (including this
	 * one) if the targetServer is NULL.
	 * 
	 * @param targetServer
	 * @param messageId
	 */
	void broadcast(Integer targetServer, String message);

	/**
	 * Broadcasts a message to the targetServer, or all servers (including this
	 * one) if the targetServer is NULL.
	 * 
	 * @param targetServer
	 * @param messageId
	 */
	void broadcastMessage(Integer targetServer, int messageId);

	/**
	 * Broadcasts a message to the targetServer, or all servers (including this
	 * one) if the targetServer is NULL. The keys in the variables map will be
	 * replaced with the value in the message.
	 * 
	 * @param targetServer
	 * @param messageId
	 */
	void broadcastMessage(Integer targetServer, int messageId, Map<String, String> variables);

	/**
	 * Broadcasts a message to the targetServer, or all servers (including this
	 * one) if the targetServer is NULL. The keys in the variables map will be
	 * replaced with the value in the message. Only players with the permission
	 * will receive the message.
	 * 
	 * @param targetServer
	 * @param messageId
	 */
	void broadcastMessageWithPermission(Integer targetServer, String permission, int messageId, Map<String, String> variables);

	/**
	 * Broadcasts a message to the targetServer, or all servers (including this
	 * one) if the targetServer is NULL. Only players with the permission will
	 * receive the message.
	 * 
	 * @param targetServer
	 * @param messageId
	 */
	void broadcastWithPermission(Integer targetServer, String permission, String message);

	/**
	 * Sends a message to a user. The user can be on any server.
	 * 
	 * @param userId
	 * @param message
	 */
	void sendMessage(int userId, String message);

	/**
	 * Sends a message to a user. The user only receives the message if he has
	 * the permission specified.
	 * 
	 * @param userId
	 * @param permission
	 * @param components
	 */
	void sendMessageWithPermission(int userId, String permission, BaseComponent... components);

	/**
	 * Sends a message to a user. The user only receives the message if he has
	 * the permission specified.
	 * 
	 * @param userId
	 * @param message
	 * @param permission
	 */
	void sendMessageWithPermission(int userId, String message, String permission);

	/**
	 * Sends the user to the specified server.
	 * 
	 * @param user
	 * @param serverId
	 */
	void sendUserToServer(User user, int serverId);
}
