package net.rieksen.networkcore.core.network;

import java.util.Map;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.chat.ComponentSerializer;
import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.enums.NetworkCoreSocketMessage;
import net.rieksen.networkcore.core.module.ModuleImpl;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.core.socket.IServerSocketResponse;
import net.rieksen.networkcore.core.socket.ServerSocketRequest;
import net.rieksen.networkcore.core.socket.ServerSocketRequestHandler;
import net.rieksen.networkcore.core.user.User;

public class NetworkModuleImpl extends ModuleImpl implements NetworkModule
{

	private NetworkCore provider;

	public NetworkModuleImpl(NetworkCore provider)
	{
		this.provider = provider;
	}

	@Override
	public void broadcast(Integer targetServer, String message)
	{
		this.broadcastWithPermission(targetServer, null, message);
	}

	@Override
	public void broadcastMessage(Integer targetServer, int messageId)
	{
		this.broadcastMessage(targetServer, messageId, null);
	}

	@Override
	public void broadcastMessage(Integer targetServer, int messageId, Map<String, String> variables)
	{
		this.broadcastMessageWithPermission(targetServer, null, messageId, variables);
	}

	@Override
	public void broadcastMessageWithPermission(Integer targetServer, String permission, int messageId, Map<String, String> variables)
	{
		BroadcastMessage object = new BroadcastMessage(messageId, permission, variables);
		if (targetServer == null)
		{
			this.provider.getServerModule().getLocalServer().broadcastMessageWithPermission(permission, messageId, variables);
			this.provider.getSocketModule().sendAll(this.provider.getNetworkPlugin().getPluginId(),
				NetworkCoreSocketMessage.BROADCAST_MESSAGE.name(), new ServerSocketRequest(object));
		} else
		{
			this.provider.getSocketModule().send(targetServer, this.provider.getNetworkPlugin().getPluginId(),
				NetworkCoreSocketMessage.BROADCAST_MESSAGE.name(), new ServerSocketRequest(object));
		}
	}

	@Override
	public void broadcastWithPermission(Integer targetServer, String permission, String message)
	{
		Broadcast object = new Broadcast(message, permission);
		if (targetServer == null)
		{
			this.provider.getServerModule().getLocalServer().broadcastWithPermission(message, permission);
			this.provider.getSocketModule().sendAll(this.provider.getNetworkPlugin().getPluginId(),
				NetworkCoreSocketMessage.BROADCAST.name(), new ServerSocketRequest(object));
		} else
		{
			this.provider.getSocketModule().send(targetServer, this.provider.getNetworkPlugin().getPluginId(),
				NetworkCoreSocketMessage.BROADCAST.name(), new ServerSocketRequest(object));
		}
	}

	@Override
	public void sendMessage(int userId, String message)
	{
		this.sendMessageWithPermission(userId, message, (String) null);
	}

	@Override
	public void sendMessageWithPermission(int userId, String permission, BaseComponent... components)
	{
		this.provider.getSocketModule().sendAll(this.provider.getNetworkPlugin().getPluginId(),
			NetworkCoreSocketMessage.SEND_MESSAGE.name(),
			new ServerSocketRequest(new Message(userId, ComponentSerializer.toString(components), permission, true)));
	}

	@Override
	public void sendMessageWithPermission(int userId, String message, String permission)
	{
		this.provider.getSocketModule().sendAll(this.provider.getNetworkPlugin().getPluginId(),
			NetworkCoreSocketMessage.SEND_MESSAGE.name(), new ServerSocketRequest(new Message(userId, message, permission)));
	}

	@Override
	public void sendUserToServer(User user, int serverId)
	{
		// TODO send user to server
		/*
		 * String[] data = c.getDataArray(); int userId =
		 * Integer.parseInt(data[0]); int serverId = Integer.parseInt(data[1]);
		 * User user = User.getUser(userId); if (user == null) { return; }
		 * Player player = this.plugin.getServer().getPlayer(user.getUUID()); if
		 * (player == null) { return; } ByteArrayDataOutput out =
		 * ByteStreams.newDataOutput(); out.writeUTF("Connect");
		 * out.writeUTF(Server.getServer(serverId).getName());
		 * player.sendPluginMessage(this.plugin, "BungeeCord",
		 * out.toByteArray());
		 */
		throw new UnsupportedOperationException();
	}

	@Override
	protected void destroyModule() throws Exception
	{}

	@Override
	protected void initModule() throws Exception
	{
		NetworkPlugin plugin = this.provider.getNetworkPlugin();
		plugin.registerSocketRequestHandler(NetworkCoreSocketMessage.BROADCAST.name(), new ServerSocketRequestHandler()
		{

			@Override
			public IServerSocketResponse process(byte[] data)
			{
				Broadcast obj = this.getObject(data, Broadcast.class);
				NetworkModuleImpl.this.provider.getServerModule().getLocalServer().broadcastWithPermission(obj.message, obj.permission);
				return null;
			}
		});
		plugin.registerSocketRequestHandler(NetworkCoreSocketMessage.BROADCAST_MESSAGE.name(), new ServerSocketRequestHandler()
		{

			@Override
			public IServerSocketResponse process(byte[] data)
			{
				BroadcastMessage obj = this.getObject(data, BroadcastMessage.class);
				NetworkModuleImpl.this.provider.getServerModule().getLocalServer().broadcastMessageWithPermission(obj.permission,
					obj.messageId, obj.variables);
				return null;
			}
		});
		plugin.registerSocketRequestHandler(NetworkCoreSocketMessage.SEND_MESSAGE.name(), new ServerSocketRequestHandler()
		{

			@Override
			public IServerSocketResponse process(byte[] data)
			{
				Message obj = this.getObject(data, Message.class);
				User user = NetworkModuleImpl.this.provider.getUserModule().findCachedUser(obj.userId);
				if (user != null)
				{
					if (obj.json)
					{
						user.sendLocalMessageWithPermission(obj.permission, ComponentSerializer.parse(obj.message));
					} else
					{
						user.sendLocalMessageWithPermission(obj.message, obj.permission);
					}
				}
				return null;
			}
		});
	}

	static class Broadcast
	{

		private String	message;
		private String	permission;

		public Broadcast()
		{}

		public Broadcast(String message, String permission)
		{
			this.message = message;
			this.permission = permission;
		}
	}

	static class BroadcastMessage
	{

		private int					messageId;
		private String				permission;
		private Map<String, String>	variables;

		public BroadcastMessage()
		{}

		public BroadcastMessage(int messageId, String permission, Map<String, String> variables)
		{
			this.messageId = messageId;
			this.permission = permission;
			this.variables = variables;
		}
	}

	static class Message
	{

		private int		userId;
		private String	message, permission;
		private boolean	json;

		public Message()
		{}

		public Message(int userId, String message, String permission)
		{
			this.userId = userId;
			this.message = message;
			this.permission = permission;
		}

		public Message(int userId, String message, String permission, boolean json)
		{
			this.userId = userId;
			this.message = message;
			this.permission = permission;
			this.json = json;
		}
	}
}
