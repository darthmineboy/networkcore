package net.rieksen.networkcore.core.info;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang.Validate;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.module.ModuleImpl;

public class InfoModuleImpl extends ModuleImpl implements InfoModule
{

	private NetworkCore					provider;
	private Map<NetworkCoreInfo, String>	cache	= new ConcurrentHashMap<>();

	public InfoModuleImpl(NetworkCore provider)
	{
		this.provider = provider;
	}

	@Override
	public void destroyModule()
	{}

	/* 
	 */
	@Override
	public String getInfo(NetworkCoreInfo info)
	{
		return this.cache.get(info);
	}

	@Override
	public void initModule()
	{
		this.refreshCache();
	}

	/* 
	 */
	@Override
	public void setInfo(NetworkCoreInfo info, String value)
	{
		Validate.notNull(info);
		this.provider.getDAO().getInfoDAO().setInfo(info, value);
		if (value == null)
		{
			this.cache.remove(info);
		} else
		{
			this.cache.put(info, value);
		}
	}

	private void refreshCache()
	{
		Map<NetworkCoreInfo, String> infoMap = this.provider.getDAO().getInfoDAO().findAllInfo();
		for (NetworkCoreInfo info : NetworkCoreInfo.values())
		{
			if (infoMap.get(info) != null)
			{
				this.cache.put(info, infoMap.get(info));
			} else
			{
				this.cache.remove(info);
			}
		}
	}
}
