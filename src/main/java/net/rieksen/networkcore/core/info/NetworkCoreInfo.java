package net.rieksen.networkcore.core.info;

/**
 * NetworkCoreInfo is used to store key/value information about a NetworkCore
 * installation.
 * 
 * @author Darthmineboy
 */
public enum NetworkCoreInfo
{
	/**
	 * The database version is stored here, and not in the plugin, because that
	 * is likely to break at some point.
	 */
	DATABASE_VERSION,
	/**
	 * The installationId that is assigned to this installation.
	 */
	INSTALLATION_ID,
	/**
	 * The secret that verifies ownership of the installation.
	 */
	INSTALLATION_SECRET,
	/**
	 * The networkId that is assigned when connecting the NetworkCore website
	 * with your database.
	 */
	NETWORK_ID,
	/**
	 * The secret that verifies that a database is the networkId it claims to
	 * be.
	 */
	NETWORK_SECRET,
	/**
	 * The base URL at which this network can be viewed on the NetworkCore
	 * website.
	 */
	NETWORK_BASE_URL,
	/**
	 * The secret key used to encrypt socket connections.
	 */
	SOCKET_ENCRYPTION_SECRET;
}
