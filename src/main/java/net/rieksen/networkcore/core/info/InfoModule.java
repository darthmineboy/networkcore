package net.rieksen.networkcore.core.info;

import net.rieksen.networkcore.core.module.Module;

public interface InfoModule extends Module
{

	String getInfo(NetworkCoreInfo info);

	void setInfo(NetworkCoreInfo info, String value);
}