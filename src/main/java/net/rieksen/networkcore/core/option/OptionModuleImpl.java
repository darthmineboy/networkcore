package net.rieksen.networkcore.core.option;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.module.ModuleImpl;
import net.rieksen.networkcore.core.option.pojo.OptionPojo;
import net.rieksen.networkcore.core.option.pojo.OptionSectionPojo;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;

public class OptionModuleImpl extends ModuleImpl implements OptionModule
{

	private NetworkCore provider;

	public OptionModuleImpl(NetworkCore provider)
	{
		this.provider = provider;
	}

	@Override
	public OptionSection findSection(int sectionId)
	{
		OptionSectionPojo section = this.provider.getDAO().getOptionDAO().findSection(sectionId);
		if (section == null) { return null; }
		NetworkPlugin plugin = NetworkPlugin.getPlugin(section.getPluginId());
		return plugin.getOptionSection(section.getName());
	}

	@Override
	public Option getOption(int optionId)
	{
		OptionPojo option = this.provider.getDAO().getOptionDAO().findOption(optionId);
		if (option == null) { return null; }
		return this.findSection(option.getSectionId()).getOption(option.getName());
	}

	@Override
	protected void destroyModule() throws Exception
	{}

	@Override
	protected void initModule() throws Exception
	{}
}
