package net.rieksen.networkcore.core.option;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.Validate;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.NetworkCoreAPI;
import net.rieksen.networkcore.core.option.pojo.OptionPojo;
import net.rieksen.networkcore.core.option.pojo.OptionValuePojo;

public class OptionImpl implements Option
{

	public static Option getOption(int optionId)
	{
		return NetworkCoreAPI.getOptionModule().getOption(optionId);
	}

	private int					optionId;
	private int					sectionId;
	private String				name;
	private OptionType			type;
	private String				description;
	private List<OptionValue>	values;
	private NetworkCore			provider;

	public OptionImpl(NetworkCore provider, OptionPojo pojo)
	{
		Validate.notNull(provider);
		Validate.notNull(pojo);
		Validate.notNull(pojo.getName());
		Validate.notNull(pojo.getOptionType());
		this.provider = provider;
		this.optionId = pojo.getOptionId();
		this.sectionId = pojo.getSectionId();
		this.name = pojo.getName();
		this.type = pojo.getOptionType();
		this.description = pojo.getDescription();
	}

	protected OptionImpl(NetworkCore provider, int optionId, int sectionId, String name, OptionType type, String description)
	{
		Validate.notNull(provider, "Provider cannot be null");
		Validate.notNull(sectionId, "sectionId cannot be null");
		Validate.notNull(name, "Name cannot be null");
		Validate.notNull(type, "Type cannot be null");
		this.optionId = optionId;
		this.sectionId = sectionId;
		this.name = name;
		this.type = type;
		this.description = description;
		this.provider = provider;
	}

	@Override
	public void changeDescription(String description)
	{
		OptionPojo pojo = this.toPojo();
		pojo.setDescription(description);
		this.provider.getDAO().getOptionDAO().updateOption(pojo);
		this.description = description;
	}

	@Override
	public void createDefault(OptionValuePojo pojo)
	{
		if (!this.findValues(null).isEmpty()) { return; }
		this.createValue(pojo);
	}

	@Override
	public OptionValue createValue(Integer serverId, int index, String value)
	{
		return this.createValue(new OptionValuePojo(0, 0, serverId, index, value));
	}

	@Override
	public OptionValue createValue(OptionValuePojo pojo)
	{
		this.initValues();
		pojo.setOptionId(this.optionId);
		pojo.setValueId(this.provider.getDAO().getOptionDAO().createValue(pojo));
		OptionValue value = OptionFactory.createValue(this.provider, pojo);
		this.values.add(value);
		return value;
	}

	@Override
	public void deleteValue(int valueId)
	{
		OptionValue value = this.findValue(valueId);
		if (value != null)
		{
			this.provider.getDAO().getOptionDAO().deleteValue(valueId);
			this.values.remove(value);
		}
	}

	@Override
	public void deleteValue(Integer serverId, int index)
	{
		OptionValue value = this.findValue(serverId, index);
		if (value != null)
		{
			this.provider.getDAO().getOptionDAO().deleteValue(value.getValueId());
			this.values.remove(value);
		}
	}

	@Override
	public OptionValue findValue(int valueId)
	{
		this.initValues();
		return this.values.stream().filter(value -> value.getValueId() == valueId).findAny().orElse(null);
	}

	@Override
	public OptionValue findValue(Integer serverId, int index)
	{
		this.initValues();
		return this.values.stream().filter(value -> value.getServerId() == serverId && value.getIndex() == index).findAny().orElse(null);
	}

	@Override
	public List<OptionValue> findValues()
	{
		this.initValues();
		return Collections.unmodifiableList(this.values);
	}

	@Override
	public List<OptionValue> findValues(Integer serverId)
	{
		this.initValues();
		List<OptionValue> search = new ArrayList<>();
		for (OptionValue value : this.values)
		{
			if (value.getServerId() == serverId || value.getServerId() != null && value.getServerId().equals(serverId))
			{
				search.add(value);
			}
		}
		Collections.sort(search);
		return search;
	}

	@Override
	public List<OptionValue> getApplyingValues(Integer serverId)
	{
		this.initValues();
		List<OptionValue> search = this.findValues(serverId);
		if (search.isEmpty() && serverId != null)
		{
			search = this.findValues(null);
		}
		return search;
	}

	@Override
	public Boolean getBoolean(Integer serverId)
	{
		this.initValues();
		this.verifyDataType(OptionType.BOOLEAN);
		List<OptionValue> values = this.getApplyingValues(serverId);
		this.verifyValuesNotEmpty(values);
		String value = values.get(0).getValue();
		return value == null ? null : Boolean.parseBoolean(values.get(0).getValue());
	}

	@Override
	public String getDescription()
	{
		return this.description;
	}

	@Override
	public Double getDouble(Integer serverId)
	{
		this.initValues();
		this.verifyDataType(OptionType.DOUBLE);
		List<OptionValue> values = this.getApplyingValues(serverId);
		this.verifyValuesNotEmpty(values);
		String value = values.get(0).getValue();
		return value == null ? null : Double.parseDouble(values.get(0).getValue());
	}

	@Override
	public Float getFloat(Integer serverId)
	{
		this.initValues();
		this.verifyDataType(OptionType.FLOAT);
		List<OptionValue> values = this.getApplyingValues(serverId);
		this.verifyValuesNotEmpty(values);
		String value = values.get(0).getValue();
		return value == null ? null : Float.parseFloat(values.get(0).getValue());
	}

	@Override
	public int getHighestIndex(Integer serverId)
	{
		return this.findValues(serverId).stream().mapToInt(OptionValue::getIndex).max().orElse(-1);
	}

	@Override
	public Integer getInteger(Integer serverId)
	{
		this.initValues();
		this.verifyDataType(OptionType.INT);
		List<OptionValue> values = this.getApplyingValues(serverId);
		this.verifyValuesNotEmpty(values);
		String value = values.get(0).getValue();
		return value == null ? null : Integer.parseInt(values.get(0).getValue());
	}

	@Override
	public List<String> getList(Integer serverId)
	{
		this.initValues();
		this.verifyDataType(OptionType.LIST);
		List<OptionValue> values = this.getApplyingValues(serverId);
		List<String> list = new ArrayList<>(values.size());
		for (OptionValue value : values)
		{
			list.add(value.getValue());
		}
		return list;
	}

	@Override
	public Long getLong(Integer serverId)
	{
		this.initValues();
		this.verifyDataType(OptionType.LONG);
		List<OptionValue> values = this.getApplyingValues(serverId);
		this.verifyValuesNotEmpty(values);
		String value = values.get(0).getValue();
		return value == null ? null : Long.parseLong(values.get(0).getValue());
	}

	@Override
	public String getName()
	{
		return this.name;
	}

	@Override
	public int getOptionId()
	{
		return this.optionId;
	}

	@Override
	public int getSectionId()
	{
		return this.sectionId;
	}

	@Override
	public List<Integer> getServersWithSettings()
	{
		this.initValues();
		List<Integer> list = new ArrayList<>();
		for (OptionValue value : this.values)
		{
			if (value.getServerId() == null)
			{
				continue;
			}
			if (!list.contains(value.getServerId()))
			{
				list.add(value.getServerId());
			}
		}
		return list;
	}

	@Override
	public String getString(Integer serverId)
	{
		this.initValues();
		this.verifyDataType(OptionType.STRING);
		List<OptionValue> values = this.getApplyingValues(serverId);
		this.verifyValuesNotEmpty(values);
		String value = values.get(0).getValue();
		return value;
	}

	@Override
	public OptionType getType()
	{
		return this.type;
	}

	@Override
	public OptionValue getValue(Integer serverId, int index)
	{
		this.initValues();
		return this.values.stream().filter(value -> value.getServerId() == serverId && value.getIndex() == index).findAny().orElse(null);
	}

	private void initValues()
	{
		if (this.values == null)
		{
			this.values = new ArrayList<>();
			this.provider.getDAO().getOptionDAO().findValues(this.optionId).forEach(pojo ->
			{
				this.values.add(OptionFactory.createValue(this.provider, pojo));
			});
		}
	}

	private OptionPojo toPojo()
	{
		return new OptionPojo(this.optionId, this.sectionId, this.name, this.type, this.description);
	}

	/**
	 * @throws IllegalArgumentException
	 *             <code>type == null</code>
	 * @throws IllegalStateException
	 *             <code>this.type != type</code>
	 * @param type
	 */
	private void verifyDataType(OptionType type)
	{
		Validate.notNull(type);
		if (this.type != type) { throw new IllegalStateException(
			"Cannot convert from " + this.type.toString() + " to " + type.toString()); }
	}

	/**
	 * @throws IllegalStateException
	 *             <code>values == null || values.isEmpty()</code>
	 * @param values
	 */
	private void verifyValuesNotEmpty(List<OptionValue> values)
	{
		if (values == null
			|| values.isEmpty()) { throw new IllegalStateException(String.format("No values found for option %s", this.name)); }
	}
}
