package net.rieksen.networkcore.core.option;

public enum OptionType
{
	INT(1, "Int")
	{

		@Override
		void checkInput(String input)
		{
			Integer.parseInt(input);
		}
	},
	LONG(2, "Long")
	{

		@Override
		void checkInput(String input)
		{
			Long.parseLong(input);
		}
	},
	STRING(3, "String"),
	BOOLEAN(4, "Boolean")
	{

		@Override
		void checkInput(String input)
		{
			Boolean.parseBoolean(input);
		}
	},
	DOUBLE(5, "Double")
	{

		@Override
		void checkInput(String input)
		{
			Double.parseDouble(input);
		}
	},
	FLOAT(6, "Float")
	{

		@Override
		void checkInput(String input)
		{
			Float.parseFloat(input);
		}
	},
	LIST(7, "List"),;

	public static OptionType fromInt(int value)
	{
		for (OptionType type : OptionType.values())
		{
			if (type.i == value) { return type; }
		}
		return null;
	}

	private final int		i;
	private final String	displayName;

	OptionType(int i, String displayName)
	{
		this.i = i;
		this.displayName = displayName;
	}

	public int getValue()
	{
		return this.i;
	}

	public boolean isApplicable(String input)
	{
		try
		{
			this.checkInput(input);
			return true;
		} catch (Exception e)
		{
			return false;
		}
	}

	@Override
	public String toString()
	{
		return this.displayName;
	}

	void checkInput(String input)
	{}
}
