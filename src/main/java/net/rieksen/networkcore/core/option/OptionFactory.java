package net.rieksen.networkcore.core.option;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.option.pojo.OptionPojo;
import net.rieksen.networkcore.core.option.pojo.OptionSectionPojo;
import net.rieksen.networkcore.core.option.pojo.OptionValuePojo;

public class OptionFactory
{

	public static Option createOption(NetworkCore provider, int optionId, int sectionId, String name, OptionType type, String description)
	{
		return new OptionImpl(provider, optionId, sectionId, name, type, description);
	}

	public static Option createOption(NetworkCore provider, OptionPojo pojo)
	{
		return new OptionImpl(provider, pojo);
	}

	public static OptionSection createSection(NetworkCore provider, int sectionId, int pluginId, String name, String description)
	{
		return new OptionSectionImpl(provider, sectionId, pluginId, name, description);
	}

	public static OptionSection createSection(NetworkCore provider, OptionSectionPojo pojo)
	{
		return new OptionSectionImpl(provider, pojo);
	}

	public static OptionValue createValue(NetworkCore provider, int valueId, int optionId, Integer serverId, int index, String value)
	{
		return new OptionValueImpl(provider, valueId, optionId, serverId, index, value);
	}

	public static OptionValue createValue(NetworkCore provider, OptionValuePojo pojo)
	{
		return new OptionValueImpl(provider, pojo);
	}
}
