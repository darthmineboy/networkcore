package net.rieksen.networkcore.core.option.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import net.rieksen.networkcore.core.option.OptionType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OptionPojo
{

	private int			optionId;
	private int			sectionId;
	private String		optionName;
	private OptionType	optionType;
	private String		description;

	public OptionPojo()
	{}

	public OptionPojo(int optionId, int sectionId, String optionName, OptionType optionType, String description)
	{
		this.optionId = optionId;
		this.sectionId = sectionId;
		this.optionName = optionName;
		this.optionType = optionType;
		this.description = description;
	}

	public String getDescription()
	{
		return this.description;
	}

	public String getName()
	{
		return this.optionName;
	}

	public int getOptionId()
	{
		return this.optionId;
	}

	public OptionType getOptionType()
	{
		return this.optionType;
	}

	public int getSectionId()
	{
		return this.sectionId;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setName(String name)
	{
		this.optionName = name;
	}

	public void setOptionId(int optionId)
	{
		this.optionId = optionId;
	}

	public void setOptionType(OptionType optionType)
	{
		this.optionType = optionType;
	}

	public void setSectionId(int sectionId)
	{
		this.sectionId = sectionId;
	}
}
