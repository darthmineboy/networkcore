package net.rieksen.networkcore.core.option;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.NetworkCoreAPI;
import net.rieksen.networkcore.core.cache.SimpleCache;
import net.rieksen.networkcore.core.dao.exception.DAOIntegrityException;
import net.rieksen.networkcore.core.option.pojo.OptionPojo;
import net.rieksen.networkcore.core.option.pojo.OptionSectionPojo;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;

public class OptionSectionImpl implements OptionSection
{

	public static OptionSection getSection(int sectionId)
	{
		return NetworkCoreAPI.getOptionModule().findSection(sectionId);
	}

	public static OptionSection getSection(int pluginId, String sectionName)
	{
		NetworkPlugin plugin = NetworkPlugin.getPlugin(pluginId);
		if (plugin == null) { return null; }
		return plugin.getOptionSection(sectionName);
	}

	private int				sectionId;
	private int				pluginId;
	private String			name;
	private String			description;
	private List<Option>	options;
	private SimpleCache		cache	= new SimpleCache(TimeUnit.SECONDS.toMillis(300));
	private NetworkCore		provider;

	public OptionSectionImpl(NetworkCore provider, int sectionId, int pluginId, String name, String description)
	{
		Validate.notNull(provider, "Provider cannot be null");
		Validate.notNull(pluginId, "pluginId cannot be null");
		Validate.notNull(name, "name cannot be null");
		this.provider = provider;
		this.sectionId = sectionId;
		this.pluginId = pluginId;
		this.name = name;
		this.description = description;
	}

	public OptionSectionImpl(NetworkCore provider, OptionSectionPojo pojo)
	{
		Validate.notNull(provider);
		Validate.notNull(pojo);
		Validate.notNull(pojo.getName());
		this.provider = provider;
		this.sectionId = pojo.getSectionId();
		this.pluginId = pojo.getPluginId();
		this.name = pojo.getName();
		this.description = pojo.getDescription();
	}

	@Override
	public void changeDescription(String description)
	{
		if (StringUtils.equals(this.description, description)) { return; }
		OptionSectionPojo pojo = this.toPojo();
		pojo.setDescription(description);
		this.provider.getDAO().getOptionDAO().updateSection(pojo);
		this.description = description;
	}

	@Override
	public Option createOption(OptionPojo pojo)
	{
		pojo.setSectionId(this.sectionId);
		try
		{
			pojo.setOptionId(this.provider.getDAO().getOptionDAO().createOption(pojo));
		} catch (DAOIntegrityException e)
		{
			this.refresh();
			throw e;
		}
		Option option = OptionFactory.createOption(this.provider, pojo);
		this.options.add(option);
		return option;
	}

	@Override
	public void deleteOption(String name)
	{
		Option option = this.getOption(name);
		if (option != null)
		{
			this.provider.getDAO().getOptionDAO().deleteOption(option.getOptionId());
			this.options.remove(option);
		}
	}

	@Override
	public String getDescription()
	{
		return this.description;
	}

	@Override
	public String getName()
	{
		return this.name;
	}

	@Override
	public Option getOption(int optionId)
	{
		Validate.notNull(optionId, "OptionId cannot be null");
		this.initOptions();
		return this.options.stream().filter(option -> option.getOptionId() == optionId).findAny().orElse(null);
	}

	@Override
	public Option getOption(String name)
	{
		Validate.notNull(name, "Name cannot be null");
		this.initOptions();
		return this.options.stream().filter(option -> option.getName().equalsIgnoreCase(name)).findAny().orElse(null);
	}

	@Override
	public List<Option> getOptions()
	{
		this.initOptions();
		return Collections.unmodifiableList(this.options);
	}

	@Override
	public Option getOrCreateOption(OptionPojo option)
	{
		Option found = this.getOption(option.getName());
		if (found == null)
		{
			found = this.createOption(option);
		}
		return found;
	}

	@Override
	public int getPluginId()
	{
		return this.pluginId;
	}

	@Override
	public int getSectionId()
	{
		return this.sectionId;
	}

	@Override
	public boolean isCached()
	{
		return this.cache.isCached();
	}

	@Override
	public void isCached(boolean isCached)
	{
		this.cache.isCached(isCached);
	}

	@Override
	public boolean isCacheExpired()
	{
		return this.cache.isCacheExpired();
	}

	@Override
	public boolean keepCached()
	{
		return this.cache.keepCached();
	}

	@Override
	public void keepCached(boolean keepCached)
	{
		this.cache.keepCached(keepCached);
	}

	@Override
	public void resetCacheExpiration()
	{
		this.cache.resetCacheExpiration();
	}

	@Override
	public String toString()
	{
		return "OptionSection [sectionId=" + this.sectionId + ", pluginId=" + this.pluginId + ", name=" + this.name + ", description="
			+ this.description + "]";
	}

	private synchronized void initOptions()
	{
		if (this.options != null) { return; }
		this.refresh();
	}

	private void refresh()
	{
		List<Option> options = new ArrayList<>();
		this.provider.getDAO().getOptionDAO().findOptions(this.sectionId).forEach(pojo ->
		{
			options.add(OptionFactory.createOption(this.provider, pojo));
		});
		this.options = options;
	}

	private OptionSectionPojo toPojo()
	{
		return new OptionSectionPojo(this.sectionId, this.pluginId, this.name, this.description);
	}
}
