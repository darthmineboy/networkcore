package net.rieksen.networkcore.core.option;

import net.rieksen.networkcore.core.module.Module;

public interface OptionModule extends Module
{

	OptionSection findSection(int sectionId);

	Option getOption(int optionId);
}