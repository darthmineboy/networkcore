package net.rieksen.networkcore.core.option;

import org.apache.commons.lang.Validate;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.option.pojo.OptionValuePojo;

public class OptionValueImpl implements OptionValue
{

	private final NetworkCore	provider;
	private final int			valueId;
	private final int			optionId;
	private final Integer		serverId;
	private int					index;
	private String				value;

	public OptionValueImpl(NetworkCore provider, int valueId, int optionId, Integer serverId, int index, String value)
	{
		Validate.notNull(provider);
		this.provider = provider;
		this.valueId = valueId;
		this.optionId = optionId;
		this.serverId = serverId;
		this.index = index;
		this.value = value;
	}

	public OptionValueImpl(NetworkCore provider, OptionValuePojo pojo)
	{
		Validate.notNull(provider);
		Validate.notNull(pojo);
		this.provider = provider;
		this.valueId = pojo.getValueId();
		this.optionId = pojo.getOptionId();
		this.serverId = pojo.getServerId();
		this.index = pojo.getIndex();
		this.value = pojo.getValue();
	}

	@Override
	public void changeIndex(int index)
	{
		OptionValuePojo pojo = this.toPojo();
		pojo.setIndex(index);
		this.provider.getDAO().getOptionDAO().updateValue(pojo);
		this.index = index;
	}

	@Override
	public void changeValue(String value)
	{
		OptionValuePojo pojo = this.toPojo();
		pojo.setValue(value);
		this.provider.getDAO().getOptionDAO().updateValue(pojo);
		this.value = value;
	}

	@Override
	public int compareTo(OptionValue o)
	{
		return Integer.compare(this.index, o.getIndex());
	}

	@Override
	public int getIndex()
	{
		return this.index;
	}

	@Override
	public int getOptionId()
	{
		return this.optionId;
	}

	@Override
	public Integer getServerId()
	{
		return this.serverId;
	}

	@Override
	public String getValue()
	{
		return this.value;
	}

	@Override
	public int getValueId()
	{
		return this.valueId;
	}

	private OptionValuePojo toPojo()
	{
		return new OptionValuePojo(this.valueId, this.optionId, this.serverId, this.index, this.value);
	}
}
