package net.rieksen.networkcore.core.option;

public interface OptionValue extends Comparable<OptionValue>
{

	void changeIndex(int index);

	void changeValue(String value);

	@Override
	int compareTo(OptionValue o);

	int getIndex();

	int getOptionId();

	Integer getServerId();

	String getValue();

	int getValueId();
}