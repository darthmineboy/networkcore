package net.rieksen.networkcore.core.option;

import java.util.List;

import net.rieksen.networkcore.core.option.pojo.OptionValuePojo;

public interface Option
{

	/**
	 * Set the description.
	 * 
	 * @param description
	 */
	void changeDescription(String description);

	OptionValue createValue(Integer serverId, int index, String value);

	OptionValue createValue(OptionValuePojo pojo);

	void deleteValue(int valueId);

	void deleteValue(Integer serverId, int index);

	OptionValue findValue(int valueId);

	OptionValue findValue(Integer serverId, int index);

	List<OptionValue> findValues();

	/**
	 * Searches for values with equal {@link serverId} as provided. <br>
	 * The returned list is sorted on {@link OptionValueImpl#getIndex()}.<br>
	 * 
	 * @see OptionValueImpl#compareTo(OptionValueImpl)
	 * @param serverId
	 * @return
	 */
	List<OptionValue> findValues(Integer serverId);

	/**
	 * Get the applying values for a {@link serverId}. If the searched values
	 * list is empty, this method will return the default applying values
	 * (null).
	 * 
	 * @return
	 */
	List<OptionValue> getApplyingValues(Integer serverId);

	/**
	 * Get the first applying value, parses it as a boolean.
	 * 
	 * @see #getApplyingValues(serverId)
	 * @throws IllegalArgumentException
	 * @throws NumberFormatException
	 * @param serverId
	 * @return
	 */
	Boolean getBoolean(Integer serverId);

	/**
	 * Get the description. Can return null.
	 */
	String getDescription();

	/**
	 * Get the first applying value, parses it as a double.
	 * 
	 * @see #getApplyingValues(serverId)
	 * @throws IllegalArgumentException
	 * @throws NumberFormatException
	 * @param serverId
	 * @return
	 */
	Double getDouble(Integer serverId);

	/**
	 * Get the first applying value, parses it as a float.
	 * 
	 * @see #getApplyingValues(serverId)
	 * @throws IllegalArgumentException
	 * @throws NumberFormatException
	 * @param serverId
	 * @return
	 */
	Float getFloat(Integer serverId);

	/**
	 * Get the highest index of existing values of that server.
	 * 
	 * @param serverId
	 * @return -1 if no values, else highest index
	 */
	int getHighestIndex(Integer serverId);

	/**
	 * Get the first applying value, parses it as a integer.
	 * 
	 * @see #getApplyingValues(serverId)
	 * @throws IllegalArgumentException
	 * @throws NumberFormatException
	 * @param serverId
	 * @return
	 */
	Integer getInteger(Integer serverId);

	List<String> getList(Integer serverId);

	/**
	 * Get the first applying value, parses it as a long.
	 * 
	 * @see #getApplyingValues(serverId)
	 * @throws IllegalArgumentException
	 * @throws NumberFormatException
	 * @param serverId
	 * @return
	 */
	Long getLong(Integer serverId);

	String getName();

	/**
	 * Get the option id.
	 * 
	 * @return
	 */
	int getOptionId();

	/**
	 * Get the section id.
	 * 
	 * @return
	 */
	int getSectionId();

	/**
	 * Get all the servers with settings. This excludes the default server which
	 * should always have settings.
	 * 
	 * @return
	 */
	List<Integer> getServersWithSettings();

	/**
	 * Get the first applying value.
	 * 
	 * @see #getApplyingValues(serverId)
	 * @throws IllegalArgumentException
	 * @param serverId
	 * @return
	 */
	String getString(Integer serverId);

	/**
	 * Get the type.
	 * 
	 * @return
	 */
	OptionType getType();

	OptionValue getValue(Integer serverId, int index);

	/**
	 * Creates default if no default is present.
	 * 
	 * @param pojo
	 */
	void createDefault(OptionValuePojo pojo);
}