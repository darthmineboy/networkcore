package net.rieksen.networkcore.core.option;

import java.util.List;

import net.rieksen.networkcore.core.cache.ICacheable;
import net.rieksen.networkcore.core.option.pojo.OptionPojo;

public interface OptionSection extends ICacheable
{

	void changeDescription(String description);

	Option createOption(OptionPojo option);

	void deleteOption(String name);

	String getDescription();

	String getName();

	Option getOption(int optionId);

	Option getOption(String name);

	List<Option> getOptions();

	/**
	 * Get a @{link OptionPojo} by its name, or create if it does not exist.
	 * 
	 * @param option
	 * @return
	 */
	Option getOrCreateOption(OptionPojo option);

	int getPluginId();

	int getSectionId();
}