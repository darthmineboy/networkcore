package net.rieksen.networkcore.core.option.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OptionValuePojo
{

	private int		valueId;
	private int		optionId;
	private Integer	serverId;
	private int		index;
	private String	value;

	public OptionValuePojo()
	{}

	public OptionValuePojo(int valueId, int optionId, Integer serverId, int index, String value)
	{
		this.valueId = valueId;
		this.optionId = optionId;
		this.serverId = serverId;
		this.index = index;
		this.value = value;
	}

	public int getIndex()
	{
		return this.index;
	}

	public int getOptionId()
	{
		return this.optionId;
	}

	public Integer getServerId()
	{
		return this.serverId;
	}

	public String getValue()
	{
		return this.value;
	}

	public int getValueId()
	{
		return this.valueId;
	}

	public void setIndex(int index)
	{
		this.index = index;
	}

	public void setOptionId(int optionId)
	{
		this.optionId = optionId;
	}

	public void setServerId(Integer serverId)
	{
		this.serverId = serverId;
	}

	public void setValue(String value)
	{
		this.value = value;
	}

	public void setValueId(int valueId)
	{
		this.valueId = valueId;
	}
}
