package net.rieksen.networkcore.core.update;

/**
 * A plugin update represents a change from one version to another version. This
 * update may be allowed in both ways, the other way would be a downgrade, but
 * this is optional.
 */
public interface IPluginUpdate
{

	/**
	 * Downgrades to {@link #getVersionFrom()}.
	 */
	public void downgrade() throws UpdateException;

	/**
	 * Get the description of this update.
	 * 
	 * @return
	 */
	public String getDescription();

	/**
	 * Get the version from which this update can be performed.
	 * 
	 * @return
	 */
	public String getVersionFrom();

	/**
	 * Get the version of this update.
	 * 
	 * @return
	 */
	public String getVersionTo();

	/**
	 * Upgrades to {@link #getVersionTo()}.
	 * 
	 * @throws UpdateException
	 */
	public void upgrade() throws UpdateException;

	/**
	 * Verify that the current installation is correct.
	 * 
	 * @throws UpdateException
	 */
	public void verifyUpdate() throws UpdateException;
}
