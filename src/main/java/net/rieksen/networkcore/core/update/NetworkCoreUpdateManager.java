package net.rieksen.networkcore.core.update;

import java.util.logging.Logger;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.dao.DAOManager;

public class NetworkCoreUpdateManager extends PluginUpdateManager
{

	private NetworkCore	provider;
	private DAOManager		dao;

	public NetworkCoreUpdateManager(NetworkCore provider, DAOManager dao)
	{
		this.provider = provider;
		this.dao = dao;
	}

	@Override
	public Logger getLogger()
	{
		return this.provider.getLogger();
	}

	@Override
	public String getVersion()
	{
		return this.dao.getDatabaseVersion();
	}

	@Override
	public void setVersion(String version)
	{
		this.dao.setDatabaseVersion(version);
	}
}
