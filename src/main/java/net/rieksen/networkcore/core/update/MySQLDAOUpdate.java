package net.rieksen.networkcore.core.update;

import java.sql.Connection;
import java.sql.SQLException;

import net.rieksen.networkcore.core.util.IMySQL;

public abstract class MySQLDAOUpdate extends PluginUpdate
{

	private IMySQL mysql;

	public MySQLDAOUpdate(IMySQL mysql, String versionFrom, String versionTo, String description)
	{
		super(versionFrom, versionTo, description);
		this.mysql = mysql;
	}

	public abstract void doTransaction(Connection connection) throws SQLException;

	public final Connection getConnection() throws SQLException
	{
		return this.mysql.getConnection();
	}

	@Override
	public final void upgrade() throws UpdateException
	{
		Connection connection = null;
		try
		{
			connection = this.getConnection();
			connection.setAutoCommit(false);
			this.doTransaction(connection);
			connection.commit();
		} catch (Exception e)
		{
			if (connection != null)
			{
				try
				{
					connection.rollback();
				} catch (SQLException rollbackException)
				{
					rollbackException.addSuppressed(e);
					throw new UpdateException("Failed to upgrade and rollback", rollbackException);
				}
			}
			throw new UpdateException("Failed to upgrade", e);
		} finally
		{
			if (connection != null)
			{
				try
				{
					connection.setAutoCommit(true);
				} catch (SQLException e)
				{
					throw new UpdateException("Failed to set auto commit back to true", e);
				}
			}
		}
	}
}
