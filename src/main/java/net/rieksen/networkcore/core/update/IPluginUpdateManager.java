package net.rieksen.networkcore.core.update;

import java.util.logging.Logger;

public interface IPluginUpdateManager
{

	Logger getLogger();

	IPluginUpdate getUpdateFrom(String version);

	String getVersion();

	void registerUpdate(IPluginUpdate update);

	void setVersion(String version);

	void update() throws UpdateException;
}