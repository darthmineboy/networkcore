package net.rieksen.networkcore.core.update;

public abstract class PluginUpdate implements IPluginUpdate
{

	private String	versionFrom;
	private String	versionTo;
	private String	description;

	public PluginUpdate(String versionFrom, String versionTo, String description)
	{
		this.versionFrom = versionFrom;
		this.versionTo = versionTo;
		this.description = description;
	}

	@Override
	public void downgrade() throws UpdateException
	{
		// Downgrade should be optional to implement, as downgrading can be
		// risky. Restoring to a backup is a better idea
		throw new UpdateException("Downgrade not available from version " + this.versionTo + " -> " + this.getVersionFrom());
	}

	@Override
	public String getDescription()
	{
		return this.description;
	}

	@Override
	public String getVersionFrom()
	{
		return this.versionFrom;
	}

	@Override
	public String getVersionTo()
	{
		return this.versionTo;
	}

	@Override
	public void verifyUpdate() throws UpdateException
	{
		// Verifying the update is optional to implement, but it is recommended
		// to implement for obvious reasons
	}
}
