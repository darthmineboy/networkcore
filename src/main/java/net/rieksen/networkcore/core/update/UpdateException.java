package net.rieksen.networkcore.core.update;

public class UpdateException extends Exception
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6415054080623873488L;

	public UpdateException()
	{
		super();
	}

	public UpdateException(String message)
	{
		super(message);
	}

	public UpdateException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public UpdateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public UpdateException(Throwable cause)
	{
		super(cause);
	}
}
