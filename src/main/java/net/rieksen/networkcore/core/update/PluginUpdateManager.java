package net.rieksen.networkcore.core.update;

import java.util.ArrayList;
import java.util.List;

public abstract class PluginUpdateManager implements IPluginUpdateManager
{

	public List<IPluginUpdate> updates = new ArrayList<>();

	/* 
	 */
	@Override
	public IPluginUpdate getUpdateFrom(String version)
	{
		for (IPluginUpdate update : this.updates)
		{
			if (update.getVersionFrom() == null)
			{
				if (version == null) { return update; }
			} else
			{
				if (update.getVersionFrom().equalsIgnoreCase(version)) { return update; }
			}
		}
		return null;
	}

	/* 
	 */
	@Override
	public void registerUpdate(IPluginUpdate update)
	{
		String versionFrom = update.getVersionFrom();
		IPluginUpdate updateFound = this.getUpdateFrom(versionFrom);
		if (updateFound != null) { throw new IllegalStateException(
			"There is already a PluginUpdate registered with versionFrom " + versionFrom); }
		this.updates.add(update);
	}

	/* 
	 */
	@Override
	public void update() throws UpdateException
	{
		String version = this.getVersion();
		this.updateFrom(version);
		this.getLogger().info("Up to date!");
	}

	private void updateFrom(String version) throws UpdateException
	{
		this.getLogger().info("Checking for updates, current version v" + version + "...");
		IPluginUpdate update = this.getUpdateFrom(version);
		if (update == null) { return; }
		this.getLogger().info("Update found!");
		String versionTo = update.getVersionTo();
		this.getLogger().info("Updating from v" + version + " -> v" + versionTo);
		String description = update.getDescription();
		if (description != null)
		{
			this.getLogger().info("Update description: " + description);
		}
		update.upgrade();
		this.getLogger().info("Verifying update...");
		update.verifyUpdate();
		this.getLogger().info("Changing version to v" + versionTo);
		this.setVersion(versionTo);
		String currentVersion = this.getVersion();
		if (!versionTo.equalsIgnoreCase(currentVersion)) { throw new UpdateException("Version was not changed to v" + versionTo); }
		this.getLogger().info("Successfully updated to v" + versionTo);
		this.updateFrom(currentVersion);
	}
}
