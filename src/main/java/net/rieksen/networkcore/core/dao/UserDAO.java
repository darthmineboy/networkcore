package net.rieksen.networkcore.core.dao;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import net.rieksen.networkcore.core.message.UserLanguagePreferences;
import net.rieksen.networkcore.core.user.CooldownAttachment;
import net.rieksen.networkcore.core.user.UserType;
import net.rieksen.networkcore.core.user.note.UserNotePojo;
import net.rieksen.networkcore.core.user.pojo.IpPojo;
import net.rieksen.networkcore.core.user.pojo.UserActivityPojo;
import net.rieksen.networkcore.core.user.pojo.UserChatPojo;
import net.rieksen.networkcore.core.user.pojo.UserConnectPojo;
import net.rieksen.networkcore.core.user.pojo.UserIpPojo;
import net.rieksen.networkcore.core.user.pojo.UserLanguagePojo;
import net.rieksen.networkcore.core.user.pojo.UserNameHistoryPojo;
import net.rieksen.networkcore.core.user.pojo.UserPojo;
import net.rieksen.networkcore.core.util.SimplePage;

public interface UserDAO
{

	/**
	 * @param userId
	 * @return time in seconds
	 * @throws DAOException
	 */
	public long calculateOnlineTime(int userId) throws DAOException;

	public long createChat(UserChatPojo chat) throws DAOException;

	public int createConnect(UserConnectPojo connect) throws DAOException;

	/**
	 * @param user
	 * @return
	 * @throws DAOException
	 */
	public int createUser(UserPojo user) throws DAOException;

	public void createUserLanguage(UserLanguagePojo pojo) throws DAOException;

	/**
	 * Purge all chats prior to given date.
	 * 
	 * @param dateBefore
	 * @return deleted chats
	 */
	public int deleteChatsBefore(Date dateBefore);

	public void deleteExpiredCooldowns() throws DAOException;

	/**
	 * @param userId
	 * @throws DAOException
	 */
	public void deleteUser(int userId) throws DAOException;

	public void deleteUserLanguage(int userId, int languageId) throws DAOException;

	public UserChatPojo findChat(long chatId) throws DAOException;

	public List<UserChatPojo> findChats(int userId) throws DAOException;

	public List<UserChatPojo> findChats(int userId, int startIndex, int maxResults) throws DAOException;

	public int findChatsCount(int userId, String search);

	public UserConnectPojo findConnect(int connectId) throws DAOException;

	public List<UserConnectPojo> findConnects(int userId) throws DAOException;

	public List<UserConnectPojo> findConnects(int userId, int startIndex, int maxResults) throws DAOException;

	public int findConnectsOfUserCount(int userId);

	public UserConnectPojo findFirstConnect(int userId) throws DAOException;

	public List<UserChatPojo> findLatestChats(int userId, int startIndex, int maxResults) throws DAOException;

	public List<UserChatPojo> findLatestChats(int userId, String search, int startIndex, Integer maxResults);

	public UserConnectPojo findLatestConnect(int userId) throws DAOException;

	public List<UserConnectPojo> findLatestConnects(int userId, int startIndex, int maxResults) throws DAOException;

	/**
	 * Find all users who joined first after joinedAfter.
	 * 
	 * @param joinedAfter
	 * @return
	 * @throws DAOException
	 */
	public List<UserPojo> findNewUsers(Date joinedAfter) throws DAOException;

	public int findOnlineUserCount(Date date) throws DAOException;

	public List<UserPojo> findOnlineUsersOfServer(int serverId);

	/**
	 * Find connects where quitDate is null.
	 * 
	 * @param userId
	 * @return
	 * @throws DAOException
	 */
	public List<UserConnectPojo> findOpenConnects(int serverId) throws DAOException;

	/**
	 * Looks for a user with matching {@link UserId}.
	 * 
	 * @param userId
	 * @return
	 * @throws DAOException
	 */
	public UserPojo findUserById(int userId) throws DAOException;

	/**
	 * Looks for a user with matching {@link UserType} and name.
	 * 
	 * @param type
	 * @param name
	 * @return
	 * @throws DAOException
	 */
	public UserPojo findUserByTypeAndName(UserType type, String name) throws DAOException;

	/**
	 * Looks for a user with matching {@link UUID}.
	 * 
	 * @param uuid
	 * @return
	 * @throws DAOException
	 */
	public UserPojo findUserByUUID(UUID uuid) throws DAOException;

	public int findUserCount() throws DAOException;

	public UserLanguagePojo findUserLanguage(int userId, int languageId);

	/**
	 * This method should never return null, instead an empty
	 * {@link UserLanguagePreferences}
	 * 
	 * @return
	 * @throws DAOException
	 */
	public List<UserLanguagePojo> findUserLanguages(int userId) throws DAOException;

	/**
	 * Finds all the users stored.
	 * 
	 * @return
	 * @throws DAOException
	 */
	public List<UserPojo> findUsers() throws DAOException;

	/**
	 * Finds a selective amount of users.
	 * 
	 * @param startIndex
	 *            starts at 0
	 * @param count
	 *            the maximum size of the returned list
	 * @return
	 * @throws DAOException
	 */
	public List<UserPojo> findUsers(int startIndex, int count) throws DAOException;

	public SimplePage<UserIpPojo> findUsersByIp(String ip, int startIndex, int maxResults);

	/**
	 * Finds users whose name starts with <code>name</code>
	 * 
	 * @param name
	 * @param startIndex
	 * @param count
	 * @return
	 * @throws DAOException
	 */
	public List<UserPojo> findUsersByName(String name, int startIndex, int count) throws DAOException;

	/**
	 * The amount of users found whose name contains <code>filterName</code>.
	 * 
	 * @param filterName
	 * @return
	 */
	public int findUsersByNameCount(String filterName);

	public void loadCooldown(CooldownAttachment cooldown) throws DAOException;

	/**
	 * Sets or create the cooldown.
	 * 
	 * @param userId
	 * @param pluginId
	 * @param key
	 * @param expiration
	 */
	public void setCooldown(int userId, int pluginId, String key, long expiration);

	public void updateConnect(UserConnectPojo connect) throws DAOException;

	/**
	 * Updates a user with {@link UserId}.
	 * 
	 * @param user
	 * @throws DAOException
	 */
	public void updateUser(UserPojo user) throws DAOException;

	public void updateUserLanguage(UserLanguagePojo pojo) throws DAOException;

	/**
	 * Creates a note.
	 * 
	 * @param note
	 * @return generated id
	 */
	int createNote(UserNotePojo note);

	void createUserNameHistory(UserNameHistoryPojo history);

	/**
	 * Deletes a note
	 * 
	 * @param id
	 */
	void deleteNote(int id);

	void deleteUserLanguages(int userId);

	SimplePage<UserChatPojo> findAllLatestChats(Integer userId, String search, int startIndex, int maxResults);

	IpPojo findIp(String ip);

	/**
	 * Find a note by id.
	 * 
	 * @param noteId
	 * @return
	 */
	UserNotePojo findNote(int noteId);

	/**
	 * Finds user notes.
	 * 
	 * @param startIndex
	 * @param maxResults
	 * @return
	 */
	SimplePage<UserNotePojo> findNotes(int userId, int startIndex, int maxResults);

	/**
	 * Find all the Ips that have at one point been used by the player.
	 * 
	 * @param userId
	 * @param startIndex
	 * @param maxResult
	 * @return
	 */
	SimplePage<UserIpPojo> findUserIps(int userId, int startIndex, int maxResults);

	List<UserNameHistoryPojo> findUserNameHistory(int userId);

	/**
	 * Find a limited selection of user activity, starting with the latest
	 * activity.
	 * 
	 * @param startIndex
	 * @param maxResults
	 * @return
	 */
	List<UserActivityPojo> findUsersActivity(int startIndex, int maxResults);

	/**
	 * Finds users where checkNameHistory=true.
	 * 
	 * @param limit
	 * @return
	 */
	List<UserPojo> findUsersWithCheckNameHistory(int limit);

	/**
	 * Find all users whose latest Ip equals the provided Ip.
	 * 
	 * @param ip
	 * @return
	 */
	List<UserPojo> findUsersWithLatestIp(String ip);
}
