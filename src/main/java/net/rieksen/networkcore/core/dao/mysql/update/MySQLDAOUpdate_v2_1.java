package net.rieksen.networkcore.core.dao.mysql.update;

import java.sql.Connection;
import java.sql.SQLException;

import net.rieksen.networkcore.core.update.MySQLDAOUpdate;
import net.rieksen.networkcore.core.util.IMySQL;

class MySQLDAOUpdate_v2_1 extends MySQLDAOUpdate
{

	public MySQLDAOUpdate_v2_1(IMySQL mysql)
	{
		super(mysql, "2.0.2", "2.1", "Adds network statistics & plugin tasks");
	}

	@Override
	public void doTransaction(Connection connection) throws SQLException
	{
		connection.createStatement()
			.executeUpdate("CREATE TABLE IF NOT EXISTS ncore_network_statistic (" + "statistic_date DATETIME NOT NULL,"
				+ "online_players INT NOT NULL," + "PRIMARY KEY (statistic_date)" + ") ENGINE=innodb DEFAULT CHARSET=utf8");
		connection.createStatement()
			.executeUpdate("CREATE TABLE IF NOT EXISTS ncore_plugin_task (" + "plugin_id INT NOT NULL," + "task_name VARCHAR(255) NOT NULL,"
				+ "task_interval INT NOT NULL," + "task_last_run DATETIME NOT NULL," + "PRIMARY KEY (plugin_id, task_name),"
				+ "CONSTRAINT fk_ncore_network_statistic_plugin FOREIGN KEY (plugin_id) REFERENCES ncore_plugin(plugin_id) ON UPDATE CASCADE ON DELETE CASCADE"
				+ ") ENGINE=innodb DEFAULT CHARSET=utf8");
	}
}
