package net.rieksen.networkcore.core.dao;

import java.util.List;

import net.rieksen.networkcore.core.issue.IssuePojo;
import net.rieksen.networkcore.core.util.SimplePage;

public interface IssueDAO
{

	/**
	 * Creates the issue, and returns an issue pojo containing the generated
	 * issueId.
	 * 
	 * @param issue
	 * @return
	 * @throws DAOException
	 */
	IssuePojo createIssue(IssuePojo issue) throws DAOException;

	IssuePojo findIssue(int issueId) throws DAOException;

	SimplePage<IssuePojo> findUnresolvedIssues(int startIndex, int maxResults);

	List<IssuePojo> findUnresolvedIssuesOfGenerator(int pluginId, String name);

	IssuePojo updateIssue(int taskId, IssuePojo issue);
}
