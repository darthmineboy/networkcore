package net.rieksen.networkcore.core.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import net.rieksen.networkcore.core.dao.DAOException;
import net.rieksen.networkcore.core.dao.WorldDAO;
import net.rieksen.networkcore.core.util.IMySQL;
import net.rieksen.networkcore.core.world.pojo.ServerWorldPojo;
import net.rieksen.networkcore.core.world.pojo.WorldLocationPojo;

public class MySQLWorldDAO extends MySQLBaseDAO implements WorldDAO
{

	public MySQLWorldDAO(IMySQL mysql) throws DAOException
	{
		super(mysql);
	}

	@Override
	public int createLocation(WorldLocationPojo location) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"INSERT INTO ncore_location (world_id, x, y, z, pitch, yaw) VALUES (?, ?, ?, ?, ?, ?)", new String[] { "location_id" });
			ps.setInt(1, location.getWorldId());
			ps.setDouble(2, location.getX());
			ps.setDouble(3, location.getY());
			ps.setDouble(4, location.getZ());
			ps.setFloat(5, location.getPitch());
			ps.setFloat(6, location.getYaw());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) { return rs.getInt(1); }
			throw new DAOException("Expected generated column");
		} catch (SQLException e)
		{
			throw new DAOException("Failed to create location", e);
		}
	}

	@Override
	public int createWorld(ServerWorldPojo world) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps =
				connection.prepareStatement("INSERT INTO ncore_world (server_id, name) VALUES (?,?)", new String[] { "world_id" });
			ps.setInt(1, world.getServerId());
			ps.setString(2, world.getWorldName());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) { return rs.getInt(1); }
			throw new DAOException("Expected generated column");
		} catch (SQLException e)
		{
			throw new DAOException("Failed to create world", e);
		}
	}

	@Override
	public void deleteLocation(int locationId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("DELETE FROM ncore_location WHERE location_id=?");
			ps.setInt(1, locationId);
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to delete location", e);
		}
	}

	@Override
	public WorldLocationPojo findLocation(int locationId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_location WHERE location_id=?");
			ps.setInt(1, locationId);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractLocation(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find location", e);
		}
	}

	@Override
	public ServerWorldPojo findWorld(int worldId)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_world WHERE world_id=?");
			ps.setInt(1, worldId);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractWorld(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find world", e);
		}
	}

	@Override
	public ServerWorldPojo findWorld(int serverId, String name)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_world WHERE server_id=? AND name=?");
			ps.setInt(1, serverId);
			ps.setString(2, name);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractWorld(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find world", e);
		}
	}

	@Override
	public List<ServerWorldPojo> findWorlds()
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_world");
			ResultSet rs = ps.executeQuery();
			return this.extractWorlds(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find worlds", e);
		}
	}

	@Override
	public List<ServerWorldPojo> findWorlds(int serverId)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_world WHERE server_id=?");
			ps.setInt(1, serverId);
			ResultSet rs = ps.executeQuery();
			return this.extractWorlds(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find worlds", e);
		}
	}

	@Override
	public void updateLocation(WorldLocationPojo location) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps =
				connection.prepareStatement("UPDATE ncore_location SET world_id=?, x=?, y=?, z=?, pitch=?, yaw=? WHERE location_id=?");
			ps.setInt(1, location.getWorldId());
			ps.setDouble(2, location.getX());
			ps.setDouble(3, location.getY());
			ps.setDouble(4, location.getZ());
			ps.setFloat(5, location.getPitch());
			ps.setFloat(6, location.getYaw());
			ps.setInt(7, location.getLocationId());
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to update location", e);
		}
	}

	@Override
	public void updateWorld(ServerWorldPojo world) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("UPDATE ncore_world SET name=? WHERE world_id=?");
			ps.setString(1, world.getWorldName());
			ps.setInt(2, world.getWorldId());
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to update world", e);
		}
	}

	private WorldLocationPojo extractLocation(ResultSet rs) throws SQLException
	{
		return new WorldLocationPojo(rs.getInt("location_id"), rs.getInt("world_id"), rs.getDouble("x"), rs.getDouble("y"),
			rs.getDouble("z"), rs.getFloat("pitch"), rs.getFloat("yaw"));
	}

	private ServerWorldPojo extractWorld(ResultSet rs) throws SQLException
	{
		return new ServerWorldPojo(rs.getInt("world_id"), rs.getInt("server_id"), rs.getString("name"));
	}

	private List<ServerWorldPojo> extractWorlds(ResultSet rs) throws SQLException
	{
		List<ServerWorldPojo> worlds = new ArrayList<>();
		while (rs.next())
		{
			worlds.add(this.extractWorld(rs));
		}
		return worlds;
	}
}
