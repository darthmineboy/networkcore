package net.rieksen.networkcore.core.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.Validate;

import net.rieksen.networkcore.core.dao.DAOException;
import net.rieksen.networkcore.core.dao.StatisticDAO;
import net.rieksen.networkcore.core.statistic.NetworkMonthlyUsersStatisticPojo;
import net.rieksen.networkcore.core.statistic.NetworkStatisticPojo;
import net.rieksen.networkcore.core.util.IMySQL;
import net.rieksen.networkcore.core.util.SQLUtil;

public class MySQLStatisticDAO extends MySQLBaseDAO implements StatisticDAO
{

	public MySQLStatisticDAO(IMySQL mysql)
	{
		super(mysql);
	}

	@Override
	public void createNetworkStatistic(NetworkStatisticPojo pojo)
	{
		Validate.notNull(pojo);
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"INSERT INTO ncore_network_statistic (statistic_date, online_players, online_servers, cpu_usage, memory_usage) VALUES (?, ?, ?, ?, ?)");
			ps.setTimestamp(1, SQLUtil.dateToTimestamp(pojo.getDate()));;
			ps.setInt(2, (int) pojo.getOnlinePlayers());
			ps.setInt(3, (int) pojo.getOnlineServers());
			ps.setFloat(4, pojo.getCpuUsage());
			ps.setLong(5, pojo.getMemoryUsage());
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to create network statistic", e);
		}
	}

	@Override
	public List<NetworkMonthlyUsersStatisticPojo> findNetworkMonthlyUsersStatistic()
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"SELECT YEAR(statistic_date) AS year, MONTH(statistic_date) AS month, MIN(online_players) AS min_users, MAX(online_players) AS max_users, AVG(online_players) AS avg_users FROM ncore_network_statistic GROUP BY YEAR(statistic_date), MONTH(statistic_date) ORDER BY YEAR(statistic_date) DESC, MONTH(statistic_date) DESC");
			ResultSet rs = ps.executeQuery();
			return this.extractUsersStatistic(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find network monthly users statistic", e);
		}
	}

	@Override
	public List<NetworkStatisticPojo> findNetworkStatisticsSince(Date since)
	{
		Validate.notNull(since);
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_network_statistic WHERE statistic_date >= ?");
			ps.setTimestamp(1, SQLUtil.dateToTimestamp(since));
			ResultSet rs = ps.executeQuery();
			return this.extractNetworkStatistics(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find network statistics since", e);
		}
	}

	private NetworkStatisticPojo extractNetworkStatistic(ResultSet rs) throws SQLException
	{
		return new NetworkStatisticPojo(SQLUtil.timestampToDate(rs.getTimestamp("statistic_date")), rs.getInt("online_players"),
			rs.getInt("online_servers"), rs.getFloat("cpu_usage"), rs.getLong("memory_usage"));
	}

	private List<NetworkStatisticPojo> extractNetworkStatistics(ResultSet rs) throws SQLException
	{
		List<NetworkStatisticPojo> pojos = new ArrayList<>();
		while (rs.next())
		{
			pojos.add(this.extractNetworkStatistic(rs));
		}
		return pojos;
	}

	private List<NetworkMonthlyUsersStatisticPojo> extractUsersStatistic(ResultSet rs) throws SQLException
	{
		List<NetworkMonthlyUsersStatisticPojo> pojos = new ArrayList<>();
		while (rs.next())
		{
			pojos.add(this.extractUserStatistic(rs));
		}
		return pojos;
	}

	private NetworkMonthlyUsersStatisticPojo extractUserStatistic(ResultSet rs) throws SQLException
	{
		Calendar calendar = Calendar.getInstance();
		calendar.set(rs.getInt("year"), rs.getInt("month"), 0);
		return new NetworkMonthlyUsersStatisticPojo(rs.getInt("year"), calendar.getTime(), rs.getInt("min_users"), rs.getInt("max_users"),
			rs.getInt("avg_users"));
	}
}
