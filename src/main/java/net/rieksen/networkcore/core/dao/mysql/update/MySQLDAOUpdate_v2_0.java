package net.rieksen.networkcore.core.dao.mysql.update;

import net.rieksen.networkcore.core.dao.mysql.MySQLMessageDAO;
import net.rieksen.networkcore.core.dao.mysql.MySQLOptionDAO;
import net.rieksen.networkcore.core.dao.mysql.MySQLServerDAO;
import net.rieksen.networkcore.core.mysql.MySQLConstraintType;
import net.rieksen.networkcore.core.mysql.MySQLUtil;
import net.rieksen.networkcore.core.update.MySQLDAOUpdate;
import net.rieksen.networkcore.core.util.IMySQL;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Logger;

/**
 * This updates the DAO from the messy 1.x to 2.0. In 1.x updates were handled
 * very messy, as of 2.0 updates are handled in a much cleaner way.
 */
class MySQLDAOUpdate_v2_0 extends MySQLDAOUpdate {

    private final Logger logger = Logger.getLogger(this.getClass().getSimpleName());
    private final String[] tables = {"ncore_language", "ncore_location", "ncore_message", "ncore_message_section",
            "ncore_message_translation", "ncore_message_variable", "ncore_global_message_variable", "ncore_option", "ncore_option_section",
            "ncore_option_value", "ncore_server_log", "ncore_server_resource_usage", "ncore_plugin", "ncore_server", "ncore_server_meta",
            "ncore_server_runtime", "ncore_simple_location", "ncore_table_version", "ncore_user", "ncore_user_chat", "ncore_user_connect",
            "ncore_user_cooldown", "ncore_user_language", "ncore_world"};

    public MySQLDAOUpdate_v2_0(IMySQL mysql) {
        super(mysql, null, "2.0", "Update from v1.x to 2.0");
    }

    @Override
    public void doTransaction(Connection connection) throws SQLException {
        this.renameTables(connection);
        this.createTables(connection);
        this.performOptionalUpdates(connection);
        this.dropConstraintsExceptPrimary(connection);
        this.createNamedIndexes(connection);
    }

    /**
     * Creates the indexes, this time named, so they can be referenced to in
     * future updates.
     *
     * @throws SQLException
     */
    private void createNamedIndexes(Connection connection) throws SQLException {
        /*
         * Language Table
         */
        MySQLUtil.addTableConstraint(connection, "ncore_language", "uq_ncore_language_name", "UNIQUE (name)");
        /*
         * Message Section Table
         */
        MySQLUtil.addTableConstraint(connection, "ncore_message_section", "uq_ncore_message_section_unique_name",
                "UNIQUE (plugin_id, name)");
        MySQLUtil.addTableConstraint(connection, "ncore_message_section", "fk_ncore_message_section_plugin",
                "FOREIGN KEY (plugin_id) REFERENCES ncore_plugin(plugin_id) ON UPDATE CASCADE ON DELETE CASCADE");
        /*
         * Message Table
         */
        MySQLUtil.addTableConstraint(connection, "ncore_message", "fk_ncore_message_message_section",
                "FOREIGN KEY (message_section_id) REFERENCES ncore_message_section(message_section_id) ON UPDATE CASCADE ON DELETE CASCADE");
        MySQLUtil.addTableConstraint(connection, "ncore_message", "uq_ncore_message_unique_name", "UNIQUE (message_section_id, name)");
        MySQLUtil.addTableConstraint(connection, "ncore_message", "fk_ncore_message_language",
                "FOREIGN KEY (language_id) REFERENCES ncore_language(language_id) ON UPDATE CASCADE ON DELETE CASCADE");
        /*
         * Message Translation Table
         */
        MySQLUtil.addTableConstraint(connection, "ncore_message_translation", "fk_ncore_message_translation_message",
                "FOREIGN KEY (message_id) REFERENCES ncore_message(message_id) ON UPDATE CASCADE ON DELETE CASCADE");
        MySQLUtil.addTableConstraint(connection, "ncore_message_translation", "fk_ncore_message_translation_language",
                "FOREIGN KEY (language_id) REFERENCES ncore_language(language_id) ON UPDATE CASCADE ON DELETE CASCADE");
        /*
         * Message Variable Table
         */
        MySQLUtil.addTableConstraint(connection, "ncore_message_variable", "fk_ncore_message_variable_message",
                "FOREIGN KEY (message_id) REFERENCES ncore_message(message_id) ON UPDATE CASCADE ON DELETE CASCADE");
        /*
         * Global Variable Table
         */
        MySQLUtil.addTableConstraint(connection, MySQLMessageDAO.GLOBAL_VARIABLE_TABLE, "uq_ncore_global_message_variable_name",
                "UNIQUE(name)");
        /*
         * Option Section Table
         */
        MySQLUtil.addTableConstraint(connection, MySQLOptionDAO.OPTION_SECTION_TABLE, "uq_ncore_option_section_name",
                "UNIQUE(plugin_id, name)");
        MySQLUtil.addTableConstraint(connection, MySQLOptionDAO.OPTION_SECTION_TABLE, "fk_ncore_option_section_plugin",
                "FOREIGN KEY (plugin_id) REFERENCES ncore_plugin(plugin_id) ON UPDATE CASCADE ON DELETE CASCADE");
        /*
         * Option Table
         */
        MySQLUtil.addTableConstraint(connection, MySQLOptionDAO.OPTION_TABLE, "uq_option_name", "UNIQUE(section_id,name)");
        MySQLUtil.addTableConstraint(connection, MySQLOptionDAO.OPTION_TABLE, "fk_ncore_option_section",
                "FOREIGN KEY (section_id) REFERENCES ncore_option_section(section_id) ON UPDATE CASCADE ON DELETE CASCADE");
        /*
         * Option Value Table
         */
        MySQLUtil.addTableConstraint(connection, MySQLOptionDAO.OPTION_VALUE_TABLE, "fk_ncore_option_value_option",
                "FOREIGN KEY (option_id) REFERENCES ncore_option(option_id) ON UPDATE CASCADE ON DELETE CASCADE");
        MySQLUtil.addTableConstraint(connection, MySQLOptionDAO.OPTION_VALUE_TABLE, "fk_ncore_option_value_server",
                "FOREIGN KEY (server_id) REFERENCES ncore_server(server_id) ON UPDATE CASCADE ON DELETE CASCADE");
        /*
         * Plugin Table
         */
        MySQLUtil.addTableConstraint(connection, "ncore_plugin", "uq_plugin_name", "UNIQUE (name)");
        /*
         * Server Table
         */
        MySQLUtil.addTableConstraint(connection, MySQLServerDAO.SERVER_TABLE, "uq_ncore_server_name", "UNIQUE (name)");
        /*
         * Ncore Server Resource Usage Table
         */
        MySQLUtil.addTableConstraint(connection, "ncore_server_resource_usage", "fk_ncore_server_resource_usage_server",
                "FOREIGN KEY (server_id) REFERENCES ncore_server(server_id) ON UPDATE CASCADE ON DELETE CASCADE");
        /*
         * Ncore Server Log Table
         */
        MySQLUtil.addTableConstraint(connection, "ncore_server_log", "fk_ncore_server_log_server",
                "FOREIGN KEY (server_id) REFERENCES ncore_server(server_id) ON UPDATE CASCADE ON DELETE CASCADE");
        /*
         * Server Runtime Table
         */
        MySQLUtil.addTableConstraint(connection, "ncore_server_runtime", "fk_ncore_server_runtime_server",
                "FOREIGN KEY (server_id) REFERENCES ncore_server(server_id) ON UPDATE CASCADE ON DELETE CASCADE");
        /*
         * User Table
         */
        MySQLUtil.addTableConstraint(connection, "ncore_user", "uq_ncore_user_name", "UNIQUE (type, name)");
        MySQLUtil.addTableConstraint(connection, "ncore_user", "uq_ncore_user_uuid", "UNIQUE (uuid)");
        /*
         * User Connect Table
         */
        MySQLUtil.addTableConstraint(connection, "ncore_user_connect", "fk_ncore_user_connect_user",
                "FOREIGN KEY (user_id) REFERENCES ncore_user(user_id) ON UPDATE CASCADE ON DELETE CASCADE");
        MySQLUtil.addTableConstraint(connection, "ncore_user_connect", "fk_ncore_user_connect_runtime",
                "FOREIGN KEY (runtime_id) REFERENCES ncore_server_runtime(runtime_id) ON UPDATE CASCADE ON DELETE CASCADE");
        /*
         * User Chat Table
         */
        MySQLUtil.addTableConstraint(connection, "ncore_user_chat", "fk_ncore_user_chat_connect",
                "FOREIGN KEY (connect_id) REFERENCES ncore_user_connect(connect_id) ON UPDATE CASCADE ON DELETE CASCADE");
        /*
         * User Language Table
         */
        MySQLUtil.addTableConstraint(connection, "ncore_user_language", "fk_ncore_user_language_user",
                "FOREIGN KEY (user_id) REFERENCES ncore_user(user_id) ON UPDATE CASCADE ON DELETE CASCADE");
        MySQLUtil.addTableConstraint(connection, "ncore_user_language", "fk_ncore_user_language_language",
                "FOREIGN KEY (language_id) REFERENCES ncore_language(language_id) ON UPDATE CASCADE ON DELETE CASCADE");
        /*
         * User Cooldown Table
         */
        MySQLUtil.addTableConstraint(connection, "ncore_user_cooldown", "fk_ncore_user_cooldown_user",
                "FOREIGN KEY (user_id) REFERENCES ncore_user(user_id) ON UPDATE CASCADE ON DELETE CASCADE");
        MySQLUtil.addTableConstraint(connection, "ncore_user_cooldown", "fk_ncore_user_cooldown_plugin",
                "FOREIGN KEY (plugin_id) REFERENCES ncore_plugin(plugin_id) ON UPDATE CASCADE ON DELETE CASCADE");
        /*
         * World Table
         */
        MySQLUtil.addTableConstraint(connection, "ncore_world", "fk_ncore_world_server",
                "FOREIGN KEY (server_id) REFERENCES ncore_server(server_id) ON UPDATE CASCADE ON DELETE CASCADE");
        MySQLUtil.addTableConstraint(connection, "ncore_world", "uq_ncore_world_name", "UNIQUE (server_id, name)");
        /*
         * Location Table
         */
        MySQLUtil.addTableConstraint(connection, "ncore_location", "fk_ncore_location_world",
                "FOREIGN KEY (world_id) REFERENCES ncore_world(world_id) ON UPDATE CASCADE ON DELETE CASCADE");
    }

    /**
     * Create the tables according to v1.1.4
     *
     * @throws SQLException
     */
    private void createTables(Connection connection) throws SQLException {
        /*
         * Info Table
         */
        connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS ncore_info (" + "info VARCHAR(32) NOT NULL,"
                + "value VARCHAR(255) NULL," + "PRIMARY KEY (info)" + ") ENGINE=INNODB DEFAULT CHARSET=UTF8");
        /*
         * Language Table
         */
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS ncore_language (" + "language_id INT NOT NULL AUTO_INCREMENT,"
                        + "name VARCHAR(255) NOT NULL" + ", PRIMARY KEY (language_id)" + ") ENGINE=INNODB DEFAULT CHARSET=UTF8");
        /*
         * Message Section Table
         */
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS ncore_message_section (" + "message_section_id INT NOT NULL AUTO_INCREMENT,"
                        + "plugin_id INT NOT NULL," + "name VARCHAR(255) NOT NULL," + "description VARCHAR(255)"
                        + ", PRIMARY KEY (message_section_id)" + ") ENGINE=INNODB DEFAULT CHARSET=UTF8");
        /*
         * Message Table
         */
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS ncore_message (" + "message_id INT NOT NULL AUTO_INCREMENT,"
                        + "message_section_id INT NOT NULL," + "name VARCHAR(255) NOT NULL," + "description VARCHAR(255),"
                        + "language_id INT NOT NULL" + ", PRIMARY KEY (message_id)" + ") ENGINE=INNODB DEFAULT CHARSET=UTF8");
        /*
         * Message Translation Table
         */
        connection.createStatement().executeUpdate(
                "CREATE TABLE IF NOT EXISTS ncore_message_translation (" + "message_id INT NOT NULL," + "language_id INT NOT NULL,"
                        + "message TEXT NOT NULL" + ", PRIMARY KEY (message_id, language_id)" + ") ENGINE=INNODB DEFAULT CHARSET=UTF8");
        /*
         * Message Variable Table
         */
        connection.createStatement().executeUpdate(
                "CREATE TABLE IF NOT EXISTS ncore_message_variable (" + "message_id INT NOT NULL," + "name VARCHAR(255) NOT NULL,"
                        + "description VARCHAR(255)" + ", PRIMARY KEY (message_id, name)" + ") ENGINE=INNODB DEFAULT CHARSET=UTF8");
        /*
         * Global Variable Table
         */
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS " + MySQLMessageDAO.GLOBAL_VARIABLE_TABLE + "("
                        + "variable_id INT NOT NULL AUTO_INCREMENT," + "name VARCHAR(255) NOT NULL," + "replacement VARCHAR(255) NOT NULL"
                        + ", PRIMARY KEY (variable_id)" + ") ENGINE=INNODB DEFAULT CHARSET=UTF8");
        /*
         * Option Section Table
         */
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS " + MySQLOptionDAO.OPTION_SECTION_TABLE + "("
                        + "section_id INT NOT NULL AUTO_INCREMENT," + "plugin_id INT NOT NULL," + "name VARCHAR(255) NOT NULL,"
                        + "description VARCHAR(255) NULL" + ", PRIMARY KEY (section_id)" + ") ENGINE=INNODB DEFAULT CHARSET=UTF8");
        /*
         * Option Table
         */
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS " + MySQLOptionDAO.OPTION_TABLE + "(" + "option_id INT NOT NULL AUTO_INCREMENT,"
                        + "section_id INT NOT NULL," + "name VARCHAR(255) NOT NULL," + "type TINYINT NOT NULL," + "description VARCHAR(255) NULL"
                        + ", PRIMARY KEY (option_id)" + ") ENGINE=INNODB DEFAULT CHARSET=UTF8");
        /*
         * Option Value Table
         */
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS " + MySQLOptionDAO.OPTION_VALUE_TABLE + "(" + "value_id INT NOT NULL AUTO_INCREMENT,"
                        + "option_id INT NOT NULL," + "server_id INT NULL," + "`index` INT NOT NULL," + "`value` VARCHAR(2000) NULL"
                        + ", PRIMARY KEY(value_id)" + ")ENGINE=INNODB DEFAULT CHARSET=UTF8");
        /*
         * Plugin Table
         */
        connection.createStatement().executeUpdate(
                "CREATE TABLE IF NOT EXISTS ncore_plugin (" + "plugin_id INT NOT NULL AUTO_INCREMENT," + "name VARCHAR(255) NOT NULL,"
                        + "database_version VARCHAR(255) NULL" + ", PRIMARY KEY (plugin_id)" + ") ENGINE=INNODB DEFAULT CHARSET=UTF8");
        /*
         * Server Table
         */
        connection.createStatement().executeUpdate(
                "CREATE TABLE IF NOT EXISTS ncore_server (" + "server_id int NOT NULL AUTO_INCREMENT," + "name varchar(255) NOT NULL,"
                        + "type tinyint NOT NULL" + ", PRIMARY KEY (server_id)" + ") ENGINE=InnoDB DEFAULT CHARSET=UTF8");
        /*
         * Server Resource Usage Table
         */
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS ncore_server_resource_usage (" + "server_id INT NOT NULL,"
                        + "`current_date` DATETIME NOT NULL," + "tps FLOAT(4,2) NOT NULL," + "node_cpu_usage FLOAT(6,2) NOT NULL,"
                        + "server_cpu_usage FLOAT(6,2) NOT NULL," + "memory_used INT NOT NULL," + "memory_available INT NOT NULL,"
                        + "memory_total INT NOT NULL" + ", PRIMARY KEY (server_id, `current_date`)" + ") ENGINE=InnoDB DEFAULT CHARSET=utf8");
        /*
         * Server Log Table
         */
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS ncore_server_log (" + "log_id BIGINT NOT NULL AUTO_INCREMENT,"
                        + "server_id INT NOT NULL," + "log_date DATETIME NOT NULL," + "level VARCHAR(32) NULL," + "message VARCHAR(1024) NOT NULL"
                        + ", PRIMARY KEY (log_id)" + ") ENGINE=InnoDB DEFAULT CHARSET=UTF8");
        /*
         * Server Runtime Table
         */
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS ncore_server_runtime (" + "runtime_id INT NOT NULL AUTO_INCREMENT,"
                        + "server_id INT NOT NULL," + "start_date DATETIME NOT NULL," + "last_ping_date DATETIME NOT NULL," + "stop_date DATETIME"
                        + ", PRIMARY KEY (runtime_id)" + ") ENGINE=InnoDB DEFAULT CHARSET=UTF8");
        /*
         * User Table
         */
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS ncore_user (" + "user_id int NOT NULL AUTO_INCREMENT," + "type tinyint NOT NULL,"
                        + "uuid varchar(255) NOT NULL," + "name varchar(255) NOT NULL" + ", PRIMARY KEY (user_id)"
                        + ") ENGINE=InnoDB DEFAULT CHARSET=UTF8");
        /*
         * User Connect Table
         */
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS ncore_user_connect (" + "connect_id INT NOT NULL AUTO_INCREMENT,"
                        + "user_id INT NOT NULL," + "runtime_id INT NOT NULL," + "name VARCHAR(255) NOT NULL," + "ip VARCHAR(255) NOT NULL,"
                        + "join_date DATETIME NOT NULL," + "quit_date DATETIME NULL" + ", PRIMARY KEY (connect_id)"
                        + ") ENGINE=InnoDB DEFAULT CHARSET=UTF8");
        /*
         * User Chat Table
         */
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS ncore_user_chat (" + "chat_id BIGINT NOT NULL AUTO_INCREMENT,"
                        + "connect_id INT NOT NULL," + "date DATETIME NOT NULL," + "message VARCHAR(4096) NOT NULL," + "cancelled BOOLEAN NOT NULL"
                        + ", PRIMARY KEY (chat_id)" + ") ENGINE = InnoDB DEFAULT CHARSET=utf8");
        /*
         * User Language Table
         */
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS ncore_user_language (" + "user_id INT NOT NULL," + "language_id INT NOT NULL,"
                        + "priority TINYINT NOT NULL" + ", PRIMARY KEY (user_id, language_id)" + ") ENGINE=INNODB DEFAULT CHARSET=UTF8");
        /*
         * User Cooldown Table
         */
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS ncore_user_cooldown (" + "user_id INT NOT NULL," + "plugin_id INT NOT NULL,"
                        + "unique_key VARCHAR(255) NOT NULL," + "expiration DATETIME NOT NULL" + ", PRIMARY KEY (user_id, plugin_id, unique_key)"
                        + ") ENGINE=InnoDB DEFAULT CHARSET=UTF8");
        /*
         * World Table
         */
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS ncore_world (" + "world_id INT NOT NULL AUTO_INCREMENT," + "server_id INT NOT NULL,"
                        + "name VARCHAR(255) NOT NULL" + ", PRIMARY KEY (world_id)" + ") ENGINE=INNODB DEFAULT CHARSET=UTF8");
        /*
         * Location Table
         */
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS ncore_location (" + "location_id INT NOT NULL AUTO_INCREMENT,"
                        + "world_id INT NOT NULL," + "x DOUBLE PRECISION NOT NULL," + "y DOUBLE PRECISION NOT NULL,"
                        + "z DOUBLE PRECISION NOT NULL," + "pitch FLOAT NOT NULL," + "yaw FLOAT NOT NULL" + ", PRIMARY KEY (location_id)"
                        + ") ENGINE=INNODB DEFAULT CHARSET=UTF8");
    }

    /**
     * Drops the indexes of all tables
     *
     * @throws SQLException
     */
    private void dropConstraintsExceptPrimary(Connection connection) throws SQLException {
        this.dropConstraintsOfType(connection, MySQLConstraintType.FOREIGN_KEY);
        this.dropConstraintsOfType(connection, MySQLConstraintType.UNIQUE);
    }

    private void dropConstraintsOfType(Connection connection, MySQLConstraintType type) throws SQLException {
        for (String tableName : this.tables) {
            if (!MySQLUtil.dropConstraintsOnTable(connection, tableName, type)) {
                logger.warning(String.format("Failed to drop constraints on table %s of type %s. Upgrade will continue but may fail", tableName, type));
            }
        }
    }

    /**
     * Performs updates on the given tables, if they weren't yet performed.
     *
     * @throws SQLException
     */
    private void performOptionalUpdates(Connection connection) throws SQLException {
        /*
         * Plugin Table
         */
        if (!MySQLUtil.columnExists(connection, "ncore_plugin", "database_version")) {
            connection.createStatement().executeUpdate("ALTER TABLE ncore_plugin ADD COLUMN database_version VARCHAR(255) NULL");
        }
        /*
         * Server Table
         */
        if (!MySQLUtil.columnExists(connection, MySQLServerDAO.SERVER_TABLE, "socket_host")) {
            connection.createStatement().executeUpdate("ALTER TABLE ncore_server ADD COLUMN socket_host CHAR(15) NULL");
        }
        if (!MySQLUtil.columnExists(connection, MySQLServerDAO.SERVER_TABLE, "socket_port")) {
            connection.createStatement().executeUpdate("ALTER TABLE ncore_server ADD COLUMN socket_port INT NULL");
        }
        if (!MySQLUtil.columnExists(connection, MySQLServerDAO.SERVER_TABLE, "socket_key")) {
            connection.createStatement().executeUpdate("ALTER TABLE ncore_server ADD COLUMN socket_key CHAR(64) NULL");
        }
        /*
         * User Connect Table
         */
        if (!MySQLUtil.columnExists(connection, "ncore_user_connect", "ip")) {
            connection.createStatement().executeUpdate("ALTER TABLE ncore_user_connect ADD COLUMN ip VARCHAR(255) NOT NULL AFTER name");
        }
    }

    private void renameTableIfExists(Connection connection, String oldName, String newName) throws SQLException {
        if (MySQLUtil.tableExists(connection, oldName) && !MySQLUtil.tableExists(connection, newName)) {
            connection.createStatement().executeUpdate("RENAME TABLE `" + oldName + "` TO `" + newName + "`");
        }
    }

    /**
     * Renames all tables so they are prefixed with ncore_
     *
     * @param connection
     * @throws SQLException
     */
    private void renameTables(Connection connection) throws SQLException {
        // Table names from before update
        final String[] tables = {"language", "location", "message", "message_section", "message_translation", "message_variable",
                "ncore_global_message_variable", "ncore_option", "ncore_option_section", "ncore_option_value", "ncore_server_log",
                "ncore_server_resource_usage", "plugin", "server", "server_meta", "server_runtime", "simple_location", "table_version", "user",
                "user_chat", "user_connect", "user_cooldown", "user_language", "world"};
        final String prefix = "ncore_";
        for (String table : tables) {
            if (!table.startsWith(prefix)) {
                this.renameTableIfExists(connection, table, prefix + table);
            }
        }
    }
}
