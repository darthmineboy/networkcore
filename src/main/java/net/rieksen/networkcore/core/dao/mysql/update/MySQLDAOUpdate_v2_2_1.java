package net.rieksen.networkcore.core.dao.mysql.update;

import java.sql.Connection;
import java.sql.SQLException;

import net.rieksen.networkcore.core.update.MySQLDAOUpdate;
import net.rieksen.networkcore.core.util.IMySQL;

public class MySQLDAOUpdate_v2_2_1 extends MySQLDAOUpdate
{

	public MySQLDAOUpdate_v2_2_1(IMySQL mysql)
	{
		super(mysql, "2.2", "2.2.1", "Add column `is_online` to `ncore_server` table");
	}

	@Override
	public void doTransaction(Connection connection) throws SQLException
	{
		connection.createStatement().executeUpdate("ALTER TABLE ncore_server ADD COLUMN is_online BOOLEAN NOT NULL AFTER type");
	}
}
