package net.rieksen.networkcore.core.dao.mysql.update;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.dao.DAOException;
import net.rieksen.networkcore.core.dao.mysql.MySQLDAOManager;
import net.rieksen.networkcore.core.mysql.MySQLUtil;
import net.rieksen.networkcore.core.plugin.PluginTaskExecutorExtra;
import net.rieksen.networkcore.core.update.MySQLDAOUpdate;
import net.rieksen.networkcore.core.update.NetworkCoreUpdateManager;
import net.rieksen.networkcore.core.update.UpdateException;

public class MySQLDAOUpdateManager
{

	private NetworkCore		provider;
	private MySQLDAOManager	dao;

	public MySQLDAOUpdateManager(NetworkCore provider, MySQLDAOManager dao)
	{
		this.provider = provider;
		this.dao = dao;
	}

	public void update() throws DAOException
	{
		NetworkCoreUpdateManager updateManager = new NetworkCoreUpdateManager(this.provider, this.dao);
		this.registerUpdates(updateManager);
		try
		{
			updateManager.update();
		} catch (UpdateException e)
		{
			throw new DAOException("Failed to update DAO", e);
		}
	}

	private void registerUpdates(NetworkCoreUpdateManager updateManager)
	{
		updateManager.registerUpdate(new MySQLDAOUpdate_v2_0(this.dao.getMySQL()));
		updateManager.registerUpdate(new MySQLDAOUpdate_v2_0_1(this.dao.getMySQL()));
		updateManager.registerUpdate(new MySQLDAOUpdate_v2_0_2(this.dao.getMySQL()));
		updateManager.registerUpdate(new MySQLDAOUpdate_v2_1(this.dao.getMySQL()));
		updateManager.registerUpdate(new MySQLDAOUpdate_v2_2(this.dao.getMySQL()));
		updateManager.registerUpdate(new MySQLDAOUpdate_v2_2_1(this.dao.getMySQL()));
		updateManager.registerUpdate(new MySQLDAOUpdate_v2_3_5(this.dao.getMySQL()));
		updateManager.registerUpdate(new MySQLDAOUpdate_v2_3_6(this.dao.getMySQL()));
		updateManager
			.registerUpdate(new MySQLDAOUpdate(this.dao.getMySQL(), "2.3.6", "2.3.7", "Add `default_message` column to `ncore_message`")
			{

				@Override
				public void doTransaction(Connection connection) throws SQLException
				{
					if (!MySQLUtil.columnExists(connection, "ncore_message", "default_message"))
					{
						connection.createStatement().executeUpdate("ALTER TABLE ncore_message ADD COLUMN default_message TEXT AFTER name");
					}
				}
			});
		updateManager.registerUpdate(new MySQLDAOUpdate(this.dao.getMySQL(), "2.3.7", "2.3.8", "Add `connect_id` column to `ncore_user`")
		{

			@Override
			public void doTransaction(Connection connection) throws SQLException
			{
				if (!MySQLUtil.columnExists(connection, "ncore_user", "connect_id"))
				{
					connection.createStatement().executeUpdate("ALTER TABLE ncore_user ADD COLUMN connect_id INT");
					connection.createStatement().executeUpdate(
						"ALTER TABLE ncore_user ADD CONSTRAINT fk_ncore_user_connect FOREIGN KEY (connect_id) REFERENCES ncore_user_connect(connect_id) ON DELETE SET NULL");
				}
			}
		});
		updateManager.registerUpdate(
			new MySQLDAOUpdate(this.dao.getMySQL(), "2.3.8", "2.3.9", "Increase description length of message/option (section)")
			{

				@Override
				public void doTransaction(Connection connection) throws SQLException
				{
					connection.createStatement().executeUpdate("ALTER TABLE ncore_message MODIFY COLUMN description VARCHAR(1024)");
					connection.createStatement().executeUpdate("ALTER TABLE ncore_message_section MODIFY COLUMN description VARCHAR(1024)");
					connection.createStatement().executeUpdate("ALTER TABLE ncore_option MODIFY COLUMN description VARCHAR(1024)");
					connection.createStatement().executeUpdate("ALTER TABLE ncore_option_section MODIFY COLUMN description VARCHAR(1024)");
				}
			});
		updateManager.registerUpdate(new MySQLDAOUpdate(this.dao.getMySQL(), "2.3.9", "2.4", "Add column `skipped` to `ncore_server_log`")
		{

			@Override
			public void doTransaction(Connection connection) throws SQLException
			{
				if (!MySQLUtil.columnExists(connection, "ncore_server_log", "skipped"))
				{
					connection.createStatement().executeUpdate("ALTER TABLE ncore_server_log ADD COLUMN skipped SMALLINT NOT NULL");
				}
			}
		});
		updateManager
			.registerUpdate(new MySQLDAOUpdate(this.dao.getMySQL(), "2.4", "2.4.1", "Add column `execution_status` to `ncore_plugin_task`")
			{

				@Override
				public void doTransaction(Connection connection) throws SQLException
				{
					if (!MySQLUtil.columnExists(connection, "ncore_plugin_task", "execution_status"))
					{
						connection.createStatement()
							.executeUpdate("ALTER TABLE ncore_plugin_task ADD COLUMN execution_status CHAR(1) NOT NULL DEFAULT 'S'");
					}
				}
			});
		updateManager.registerUpdate(new MySQLDAOUpdate(this.dao.getMySQL(), "2.4.1", "2.4.2",
			"Add table `ncore_user_name_history` and column `check_name_history` to `ncore_user`")
		{

			@Override
			public void doTransaction(Connection connection) throws SQLException
			{
				connection.createStatement().executeUpdate(
					"CREATE TABLE IF NOT EXISTS ncore_user_name_history (user_id INT NOT NULL, changed_at TIMESTAMP NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY (user_id, changed_at), CONSTRAINT fk_ncore_user_name_history_user FOREIGN KEY (user_id) REFERENCES ncore_user(user_id) ON DELETE CASCADE) ENGINE=InnoDb DEFAULT CHARSET=utf8");
				if (!MySQLUtil.columnExists(connection, "ncore_user", "check_name_history"))
				{
					connection.createStatement()
						.executeUpdate("ALTER TABLE ncore_user ADD COLUMN check_name_history BOOLEAN NOT NULL DEFAULT 1");
				}
			}
		});
		updateManager.registerUpdate(new MySQLDAOUpdate(this.dao.getMySQL(), "2.4.2", "2.4.3",
			"Remove column `language_id` from table `ncore_message` and add column `language_id` to table `ncore_plugin`")
		{

			@Override
			public void doTransaction(Connection connection) throws SQLException
			{
				if (MySQLUtil.columnExists(connection, "ncore_message", "language_id"))
				{
					connection.createStatement().executeUpdate("ALTER TABLE ncore_message DROP FOREIGN KEY fk_ncore_message_language");
					connection.createStatement().executeUpdate("ALTER TABLE ncore_message DROP COLUMN language_id");
				}
				if (!MySQLUtil.columnExists(connection, "ncore_plugin", "language_id"))
				{
					connection.createStatement().executeUpdate("ALTER TABLE ncore_plugin ADD COLUMN language_id INT");
					connection.createStatement().executeUpdate(
						"ALTER TABLE ncore_plugin ADD CONSTRAINT fk_ncore_plugin_language FOREIGN KEY (language_id) REFERENCES ncore_language (language_id)");
				}
			}
		});
		updateManager.registerUpdate(
			new MySQLDAOUpdate(this.dao.getMySQL(), "2.4.3", "2.4.4", "Change column `task_priority` from VARCHAR(15) to INT")
			{

				@Override
				public void doTransaction(Connection connection) throws SQLException
				{
					if (MySQLUtil.columnExists(connection, "ncore_task", "task_priority"))
					{
						connection.createStatement().executeUpdate("DELETE FROM ncore_task");
						connection.createStatement().executeUpdate("ALTER TABLE ncore_task MODIFY COLUMN task_priority INT NOT NULL");
					}
				}
			});
		updateManager.registerUpdate(new MySQLDAOUpdate(this.dao.getMySQL(), "2.4.4", "2.4.5",
			"Rename table `ncore_task` to `ncore_issue`, and columns from `task_*` to `issue_*`")
		{

			@Override
			public void doTransaction(Connection connection) throws SQLException
			{
				if (MySQLUtil.columnExists(connection, "ncore_task", "task_id"))
				{
					connection.createStatement()
						.executeUpdate("ALTER TABLE ncore_task CHANGE `task_id` `issue_id` INT NOT NULL AUTO_INCREMENT");
				}
				if (MySQLUtil.columnExists(connection, "ncore_task", "task_name"))
				{
					connection.createStatement()
						.executeUpdate("ALTER TABLE ncore_task CHANGE `task_name` `issue_name` VARCHAR(255) NOT NULL");
				}
				if (MySQLUtil.columnExists(connection, "ncore_task", "task_priority"))
				{
					connection.createStatement()
						.executeUpdate("ALTER TABLE ncore_task CHANGE `task_priority` `issue_priority` INT NOT NULL");
				}
				if (MySQLUtil.columnExists(connection, "ncore_task", "task_generator"))
				{
					connection.createStatement()
						.executeUpdate("ALTER TABLE ncore_task CHANGE `task_generator` `issue_generator` VARCHAR(255)");
				}
				if (MySQLUtil.columnExists(connection, "ncore_task", "task_data"))
				{
					connection.createStatement().executeUpdate("ALTER TABLE ncore_task CHANGE `task_data` `issue_data` VARCHAR(4096)");
				}
				if (MySQLUtil.tableExists(connection, "ncore_task"))
				{
					connection.createStatement().executeUpdate("RENAME TABLE ncore_task TO ncore_issue");
				}
			}
		});
		updateManager.registerUpdate(
			new MySQLDAOUpdate(this.dao.getMySQL(), "2.4.5", "2.4.6", "Add column `language_name` to table `ncore_locale_language`")
			{

				@Override
				public void doTransaction(Connection connection) throws SQLException
				{
					if (!MySQLUtil.columnExists(connection, "ncore_locale_language", "language_name"))
					{
						connection.createStatement()
							.executeUpdate("ALTER TABLE ncore_locale_language ADD COLUMN language_name VARCHAR(255) AFTER locale_code");
					}
				}
			});
		updateManager.registerUpdate(new MySQLDAOUpdate(this.dao.getMySQL(), "2.4.6", "2.4.7",
			"Modify table `ncore_user_name_history` to allow nulls as `changed_at`")
		{

			@Override
			public void doTransaction(Connection connection) throws SQLException
			{
				List<String> statements =
					Arrays.asList("ALTER TABLE ncore_user_name_history DROP FOREIGN KEY fk_ncore_user_name_history_user",
						"ALTER TABLE ncore_user_name_history DROP PRIMARY KEY",
						"ALTER TABLE ncore_user_name_history MODIFY COLUMN changed_at DATETIME NULL",
						"ALTER TABLE ncore_user_name_history ADD CONSTRAINT uq_ncore_user_name_history_user_changed_at UNIQUE (user_id, changed_at)",
						"ALTER TABLE ncore_user_name_history ADD CONSTRAINT fk_ncore_user_name_history_user FOREIGN KEY (user_id) REFERENCES ncore_user(user_id) ON DELETE CASCADE",
						"UPDATE ncore_user SET check_name_history = 1");
				for (String statement : statements)
				{
					connection.createStatement().executeUpdate(statement);
				}
			}
		});
		updateManager.registerUpdate(
			new MySQLDAOUpdate(this.dao.getMySQL(), "2.4.7", "2.4.8", "Modify table `ncore_user` add column `auto_language`")
			{

				@Override
				public void doTransaction(Connection connection) throws SQLException
				{
					connection.createStatement()
						.executeUpdate("ALTER TABLE ncore_user ADD COLUMN auto_language BOOLEAN NOT NULL DEFAULT 1");
				}
			});
		updateManager.registerUpdate(new MySQLDAOUpdate(this.dao.getMySQL(), "2.4.8", "2.4.9", "Add support for JSON translations")
		{

			@Override
			public void doTransaction(Connection connection) throws SQLException
			{
				connection.createStatement()
					.executeUpdate("ALTER TABLE ncore_message_translation ADD COLUMN message_type CHAR(1) NOT NULL DEFAULT 'R'");
			}
		});
		updateManager
			.registerUpdate(new MySQLDAOUpdate(this.dao.getMySQL(), "2.4.9", "2.5", "Add defaultMessage and reviewing to translation")
			{

				@Override
				public void doTransaction(Connection connection) throws SQLException
				{
					final String table = "ncore_message_translation";
					if (!MySQLUtil.columnExists(connection, table, "default_message"))
					{
						connection.createStatement().executeUpdate("ALTER TABLE " + table + " ADD COLUMN default_message TEXT NOT NULL");
					}
					if (!MySQLUtil.columnExists(connection, table, "default_message_type"))
					{
						connection.createStatement()
							.executeUpdate("ALTER TABLE " + table + " ADD COLUMN default_message_type CHAR(1) NOT NULL DEFAULT 'R'");
					}
					if (!MySQLUtil.columnExists(connection, table, "use_default_message"))
					{
						connection.createStatement()
							.executeUpdate("ALTER TABLE " + table + " ADD COLUMN use_default_message BOOLEAN NOT NULL DEFAULT 0");
					}
					if (!MySQLUtil.columnExists(connection, table, "requires_review"))
					{
						connection.createStatement()
							.executeUpdate("ALTER TABLE " + table + " ADD COLUMN requires_review BOOLEAN NOT NULL DEFAULT 0");
					}
					if (!MySQLUtil.columnExists(connection, table, "last_reviewed"))
					{
						connection.createStatement().executeUpdate("ALTER TABLE " + table + " ADD COLUMN last_reviewed DATETIME");
					}
				}
			});
		updateManager.registerUpdate(new MySQLDAOUpdate(this.dao.getMySQL(), "2.5", "2.6", "Add previousPluginVersion to server plugin")
		{

			@Override
			public void doTransaction(Connection connection) throws SQLException
			{
				if (!MySQLUtil.columnExists(connection, "ncore_server_plugin", "previous_plugin_version"))
				{
					connection.createStatement()
						.executeUpdate("ALTER TABLE ncore_server_plugin ADD COLUMN previous_plugin_version VARCHAR(255)");
				}
			}
		});
		updateManager
			.registerUpdate(new MySQLDAOUpdate(this.dao.getMySQL(), "2.6", "2.7", "Add claimedTill and claimedServerId to plugin task")
			{

				@Override
				public void doTransaction(Connection connection) throws SQLException
				{
					if (!MySQLUtil.columnExists(connection, "ncore_plugin_task", "claimed_till"))
					{
						connection.createStatement().executeUpdate("ALTER TABLE ncore_plugin_task ADD COLUMN claimed_till DATETIME");
					}
					if (!MySQLUtil.columnExists(connection, "ncore_plugin_task", "claimed_server_id"))
					{
						connection.createStatement().executeUpdate("ALTER TABLE ncore_plugin_task ADD COLUMN claimed_server_id INT");
						connection.createStatement().executeUpdate(
							"ALTER TABLE ncore_plugin_task ADD CONSTRAINT fk_ncore_plugin_task_server FOREIGN KEY (claimed_server_id) REFERENCES ncore_server(server_id) ON DELETE SET NULL");
					}
				}
			});
		updateManager.registerUpdate(new MySQLDAOUpdate(this.dao.getMySQL(), "2.7", "2.8", "Extend network statistics")
		{

			@Override
			public void doTransaction(Connection connection) throws SQLException
			{
				if (!MySQLUtil.columnExists(connection, "ncore_network_statistic", "online_servers"))
				{
					connection.createStatement()
						.executeUpdate("ALTER TABLE ncore_network_statistic ADD COLUMN online_servers INT NOT NULL");
				}
				if (!MySQLUtil.columnExists(connection, "ncore_network_statistic", "cpu_usage"))
				{
					connection.createStatement().executeUpdate("ALTER TABLE ncore_network_statistic ADD COLUMN cpu_usage FLOAT NOT NULL");
				}
				if (!MySQLUtil.columnExists(connection, "ncore_network_statistic", "memory_usage"))
				{
					connection.createStatement()
						.executeUpdate("ALTER TABLE ncore_network_statistic ADD COLUMN memory_usage BIGINT NOT NULL");
				}
			}
		});
		updateManager.registerUpdate(new MySQLDAOUpdate(this.dao.getMySQL(), "2.8", "2.9", "Support execution of plugin tasks per server")
		{

			@Override
			public void doTransaction(Connection connection) throws SQLException
			{
				if (!MySQLUtil.columnExists(connection, "ncore_plugin_task", "is_global"))
				{
					connection.createStatement()
						.executeUpdate("ALTER TABLE ncore_plugin_task ADD COLUMN is_global BOOLEAN NOT NULL DEFAULT 1");
				}
				connection.createStatement().executeUpdate(
					"CREATE TABLE IF NOT EXISTS ncore_plugin_task_execution (plugin_id INT NOT NULL, task_name VARCHAR(255) NOT NULL, server_id INT NOT NULL, execution_date DATETIME NOT NULL, execution_status CHAR(1) NOT NULL, PRIMARY KEY (plugin_id, task_name, server_id, execution_date), CONSTRAINT fk_ncore_plugin_task_execution_server FOREIGN KEY (server_id) REFERENCES ncore_server(server_id) ON DELETE CASCADE, CONSTRAINT fk_ncore_plugin_task_execution_task FOREIGN KEY (plugin_id, task_name) REFERENCES ncore_plugin_task(plugin_id, task_name) ON DELETE CASCADE) ENGINE=InnoDb DEFAULT CHARSET=utf8");
			}
		});
		updateManager.registerUpdate(new MySQLDAOUpdate(this.dao.getMySQL(), "2.9", "2.10", "Statistics per server")
		{

			@Override
			public void doTransaction(Connection connection) throws SQLException
			{
				connection.createStatement().executeUpdate(
					"CREATE TABLE IF NOT EXISTS ncore_server_statistic (server_id INT NOT NULL, `current_date` DATETIME NOT NULL, online_players INT NOT NULL, tps FLOAT(4,2) NOT NULL, server_cpu_usage FLOAT(6,2) NOT NULL, memory_used INT NOT NULL, memory_available INT NOT NULL, memory_total INT NOT NULL, PRIMARY KEY (server_id, `current_date`), CONSTRAINT fk_ncore_server_statistic_server FOREIGN KEY (server_id) REFERENCES ncore_server(server_id) ON DELETE CASCADE) ENGINE=InnoDb DEFAULT CHARSET=utf8");
			}
		});
		updateManager.registerUpdate(new MySQLDAOUpdate(this.dao.getMySQL(), "2.10", "2.11", "Summary of plugin task execution")
		{

			@Override
			public void doTransaction(Connection connection) throws SQLException
			{
				if (!MySQLUtil.columnExists(connection, "ncore_plugin_task_execution", "summary"))
				{
					connection.createStatement().executeUpdate("ALTER TABLE ncore_plugin_task_execution ADD COLUMN summary VARCHAR("
						+ PluginTaskExecutorExtra.MAX_SUMMARY_LENGTH + ")");
				}
			}
		});
		updateManager.registerUpdate(new MySQLDAOUpdate(this.dao.getMySQL(), "2.11", "2.12", "Duration of plugin task execution")
		{

			@Override
			public void doTransaction(Connection connection) throws SQLException
			{
				if (!MySQLUtil.columnExists(connection, "ncore_plugin_task_execution", "duration"))
				{
					connection.createStatement()
						.executeUpdate("ALTER TABLE ncore_plugin_task_execution ADD COLUMN duration BIGINT NOT NULL default 0");
				}
			}
		});
		updateManager.registerUpdate(new MySQLDAOUpdate(this.dao.getMySQL(), "2.12", "2.13", "Add table `ncore_user_note`")
		{

			@Override
			public void doTransaction(Connection connection) throws SQLException
			{
				connection.createStatement().executeUpdate(
					"CREATE TABLE IF NOT EXISTS ncore_user_note (id INT NOT NULL AUTO_INCREMENT, user_id INT NOT NULL, created_by_id INT NOT NULL, creation_date DATETIME NOT NULL, message VARCHAR(1024) NOT NULL, PRIMARY KEY (id), CONSTRAINT fk_ncore_user_note_user FOREIGN KEY (user_id) REFERENCES ncore_user(user_id) ON DELETE CASCADE, CONSTRAINT fk_ncore_user_note_created_by FOREIGN KEY (created_by_id) REFERENCES ncore_user(user_id)) ENGINE=InnoDb DEFAULT CHARSET=utf8");
			}
		});
	}
}
