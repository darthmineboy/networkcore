package net.rieksen.networkcore.core.dao.mysql.update;

import java.sql.Connection;
import java.sql.SQLException;

import net.rieksen.networkcore.core.mysql.MySQLUtil;
import net.rieksen.networkcore.core.update.MySQLDAOUpdate;
import net.rieksen.networkcore.core.util.IMySQL;

class MySQLDAOUpdate_v2_3_6 extends MySQLDAOUpdate
{

	public MySQLDAOUpdate_v2_3_6(IMySQL mysql)
	{
		super(mysql, "2.3.5", "2.3.6", "Adds column `socket_bind_host` to table `ncore_server`");
	}

	@Override
	public void doTransaction(Connection connection) throws SQLException
	{
		if (!MySQLUtil.columnExists(connection, "ncore_server", "socket_bind_host"))
		{
			connection.createStatement()
				.executeUpdate("ALTER TABLE ncore_server ADD COLUMN socket_bind_host VARCHAR(255) AFTER socket_auto");
		}
	}
}
