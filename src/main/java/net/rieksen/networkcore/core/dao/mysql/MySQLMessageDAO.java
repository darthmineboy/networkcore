package net.rieksen.networkcore.core.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import net.rieksen.networkcore.core.dao.DAOException;
import net.rieksen.networkcore.core.dao.MessageDAO;
import net.rieksen.networkcore.core.message.MessageType;
import net.rieksen.networkcore.core.message.pojo.GlobalMessageVariablePojo;
import net.rieksen.networkcore.core.message.pojo.LanguagePojo;
import net.rieksen.networkcore.core.message.pojo.LocaleLanguagePojo;
import net.rieksen.networkcore.core.message.pojo.MessagePojo;
import net.rieksen.networkcore.core.message.pojo.MessageSectionPojo;
import net.rieksen.networkcore.core.message.pojo.MessageTranslationPojo;
import net.rieksen.networkcore.core.message.pojo.MessageVariablePojo;
import net.rieksen.networkcore.core.util.IMySQL;
import net.rieksen.networkcore.core.util.SQLUtil;

public class MySQLMessageDAO extends MySQLBaseDAO implements MessageDAO
{

	public final static String	LANGUAGE_TABLE				= "ncore_language";
	public final static String	MESSAGE_SECTION_TABLE		= "ncore_message_section";
	public final static String	MESSAGE_TABLE				= "ncore_message";
	public final static String	MESSAGE_TRANSLATION_TABLE	= "ncore_message_translation";
	public final static String	MESSAGE_VARIABLE_TABLE		= "ncore_message_variable";
	public final static String	GLOBAL_VARIABLE_TABLE		= "ncore_global_message_variable";

	public MySQLMessageDAO(IMySQL mysql) throws DAOException
	{
		super(mysql);
	}

	@Override
	public int createGlobalVariable(GlobalMessageVariablePojo pojo) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps =
				connection.prepareStatement("INSERT INTO " + MySQLMessageDAO.GLOBAL_VARIABLE_TABLE + " (name, replacement) VALUES (?, ?)",
					new String[] { "variable_id" });
			ps.setString(1, pojo.getName());
			ps.setString(2, pojo.getReplacement());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) { return rs.getInt(1); }
			throw new DAOException("Expected generated column");
		} catch (SQLException e)
		{
			throw new DAOException("Failed to create global message variable", e);
		}
	}

	@Override
	public int createLanguage(LanguagePojo language) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps =
				connection.prepareStatement("INSERT INTO ncore_language (name) VALUES (?)", new String[] { "language_id" });
			ps.setString(1, language.getName());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) { return rs.getInt(1); }
			throw new DAOException("Expected generated column");
		} catch (SQLException e)
		{
			throw new DAOException(e);
		}
	}

	@Override
	public void createLocaleLanguage(LocaleLanguagePojo localeLanguage) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps =
				connection.prepareStatement("INSERT INTO ncore_locale_language (locale_code, language_name,language_id) VALUES (?, ?, ?)");
			ps.setString(1, localeLanguage.getLocaleCode());
			ps.setString(2, localeLanguage.getLanguageName());
			ps.setObject(3, localeLanguage.getLanguageId());
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to create locale language", e);
		}
	}

	@Override
	public int createMessage(MessagePojo message) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"INSERT INTO ncore_message (message_section_id, name, description) VALUES (?, ?, ?)", new String[] { "message_id" });
			ps.setInt(1, message.getSectionId());
			ps.setString(2, message.getName());
			ps.setString(3, message.getDescription());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) { return rs.getInt(1); }
			throw new DAOException("Expected generated column");
		} catch (SQLException e)
		{
			throw new DAOException("Failed to create message", e);
		}
	}

	@Override
	public int createSection(MessageSectionPojo section) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"INSERT INTO ncore_message_section (plugin_id, name, description) VALUES (?, ?, ?)", new String[] { "message_section_id" });
			ps.setInt(1, section.getPluginId());
			ps.setString(2, section.getName());
			ps.setString(3, section.getDescription());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) { return rs.getInt(1); }
			throw new DAOException();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to create message section", e);
		}
	}

	@Override
	public void createTranslation(MessageTranslationPojo translation) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"INSERT INTO ncore_message_translation (message_id, language_id, message, message_type, default_message, default_message_type, use_default_message, requires_review, last_reviewed) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
			ps.setInt(1, translation.getMessageId());
			ps.setInt(2, translation.getLanguageId());
			ps.setString(3, translation.getMessage());
			ps.setString(4, String.valueOf(translation.getMessageType().getId()));
			ps.setString(5, translation.getDefaultMessage());
			ps.setString(6, String.valueOf(translation.getDefaultMessageType().getId()));
			ps.setBoolean(7, translation.useDefaultMessage());
			ps.setBoolean(8, translation.requiresReview());
			ps.setTimestamp(9, SQLUtil.dateToTimestamp(translation.getLastReviewed()));
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to create translation", e);
		}
	}

	@Override
	public void createVariable(MessageVariablePojo variable)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps =
				connection.prepareStatement("INSERT INTO ncore_message_variable (message_id, name, description) VALUES (?,?,?)");
			ps.setInt(1, variable.getMessageId());
			ps.setString(2, variable.getName());
			ps.setString(3, variable.getDescription());
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to create message variable", e);
		}
	}

	@Override
	public void deleteGlobalVariable(int variableId)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps =
				connection.prepareStatement("DELETE FROM " + MySQLMessageDAO.GLOBAL_VARIABLE_TABLE + " WHERE variable_id=?");
			ps.setInt(1, variableId);
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to delete global message variable", e);
		}
	}

	@Override
	public void deleteLanguage(int languageId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("DELETE FROM " + MySQLMessageDAO.LANGUAGE_TABLE + " WHERE language_id=?");
			ps.setInt(1, languageId);
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to delete language", e);
		}
	}

	@Override
	public void deleteLocaleLanguage(String localeCode) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("DELETE FROM ncore_locale_language WHERE locale_code=?");
			ps.setString(1, localeCode);
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to delete locale language", e);
		}
	}

	@Override
	public void deleteMessage(int messageId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("DELETE FROM ncore_message WHERE message_id=?");
			ps.setInt(1, messageId);
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to delete message", e);
		}
	}

	@Override
	public void deleteSection(int sectionId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("DELETE FROM ncore_message_section WHERE message_section_id=?");
			ps.setInt(1, sectionId);
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to delete message section", e);
		}
	}

	@Override
	public void deleteTranslation(int messageId, int languageId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps =
				connection.prepareStatement("DELETE FROM ncore_message_translation WHERE message_id=? AND language_id=?");
			ps.setInt(1, messageId);
			ps.setInt(2, languageId);
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to delete message translation", e);
		}
	}

	@Override
	public void deleteVariable(int messageId, String name) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("DELETE FROM ncore_message_variable WHERE message_id=? AND name=?");
			ps.setInt(1, messageId);
			ps.setString(2, name);
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to delete variable", e);
		}
	}

	public List<GlobalMessageVariablePojo> extractGlobalMessageVariables(ResultSet rs) throws SQLException
	{
		List<GlobalMessageVariablePojo> variables = new ArrayList<>();
		while (rs.next())
		{
			GlobalMessageVariablePojo var = this.extractGlobalMessageVariable(rs);
			variables.add(var);
		}
		return variables;
	}

	@Override
	public GlobalMessageVariablePojo findGlobalVariable(int variableId)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps =
				connection.prepareStatement("SELECT * FROM " + MySQLMessageDAO.GLOBAL_VARIABLE_TABLE + " WHERE variable_id=?");
			ps.setInt(1, variableId);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractGlobalMessageVariablePojo(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find global message variable", e);
		}
	}

	@Override
	public GlobalMessageVariablePojo findGlobalVariable(String name) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM " + MySQLMessageDAO.GLOBAL_VARIABLE_TABLE + " WHERE name=?");
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractGlobalMessageVariable(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find global message variable", e);
		}
	}

	@Override
	public List<GlobalMessageVariablePojo> findGlobalVariables() throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			ResultSet rs = connection.createStatement().executeQuery("SELECT * FROM " + MySQLMessageDAO.GLOBAL_VARIABLE_TABLE);
			return this.extractGlobalMessageVariables(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find global message variables", e);
		}
	}

	@Override
	public LanguagePojo findLanguage(int languageId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_language WHERE language_id=?");
			ps.setInt(1, languageId);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractLanguage(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find language", e);
		}
	}

	@Override
	public LanguagePojo findLanguage(String language) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_language WHERE name=?");
			ps.setString(1, language);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractLanguage(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find language", e);
		}
	}

	@Override
	public List<LanguagePojo> findLanguages() throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_language");
			ResultSet rs = ps.executeQuery();
			return this.extractLanguages(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find languages", e);
		}
	}

	@Override
	public LocaleLanguagePojo findLocale(String locale)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_locale_language WHERE locale_code=?");
			ps.setString(1, locale);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractLocaleLanguage(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find locale", e);
		}
	}

	@Override
	public List<LocaleLanguagePojo> findLocaleLanguages() throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_locale_language");
			ResultSet rs = ps.executeQuery();
			return this.extractLocaleLanguages(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find locale languages", e);
		}
	}

	@Override
	public MessagePojo findMessage(int messageId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_message WHERE message_id=?");
			ps.setInt(1, messageId);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractMessage(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find message", e);
		}
	}

	@Override
	public MessagePojo findMessage(int sectionId, String name) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_message WHERE message_section_id=? AND name=?");
			ps.setInt(1, sectionId);
			ps.setString(2, name);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractMessage(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find message", e);
		}
	}

	@Override
	public int findMessageCount() throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT COUNT(*) FROM ncore_message");
			ResultSet rs = ps.executeQuery();
			while (rs.next())
			{
				return rs.getInt(1);
			}
			throw new DAOException("Expected result");
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find message count", e);
		}
	}

	@Override
	public List<MessagePojo> findMessages(int sectionId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_message WHERE message_section_id=?");
			ps.setInt(1, sectionId);
			ResultSet rs = ps.executeQuery();
			return this.extractMessages(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find messages", e);
		}
	}

	@Override
	public List<LanguagePojo> findMissingTranslations(int messageId)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"SELECT * FROM ncore_language WHERE language_id NOT IN (SELECT language_id FROM ncore_message_translation WHERE message_id=?)");
			ps.setInt(1, messageId);
			ResultSet rs = ps.executeQuery();
			return this.extractLanguages(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find missing translations", e);
		}
	}

	@Override
	public MessageSectionPojo findSection(int sectionId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_message_section WHERE message_section_id=?");
			ps.setInt(1, sectionId);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractMessageSection(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find message section", e);
		}
	}

	@Override
	public MessageSectionPojo findSection(int pluginId, String name) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_message_section WHERE plugin_id=? AND name=?");
			ps.setInt(1, pluginId);
			ps.setString(2, name);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractMessageSection(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find message section", e);
		}
	}

	@Override
	public int findSectionCount(int pluginId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT COUNT(*) FROM ncore_message_section WHERE plugin_id=?");
			ps.setInt(1, pluginId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) { return rs.getInt(1); }
			throw new DAOException("Expected result");
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find message section count", e);
		}
	}

	@Override
	public List<MessageSectionPojo> findSections(int pluginId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_message_section WHERE plugin_id=?");
			ps.setInt(1, pluginId);
			ResultSet rs = ps.executeQuery();
			return this.extractMessageSections(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find message sections", e);
		}
	}

	@Override
	public MessageTranslationPojo findTranslation(int messageId, int languageId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps =
				connection.prepareStatement("SELECT * FROM ncore_message_translation WHERE message_id=? AND language_id=?");
			ps.setInt(1, messageId);
			ps.setInt(2, languageId);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractTranslation(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find message translation", e);
		}
	}

	@Override
	public int findTranslationCount(int languageId)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps =
				connection.prepareStatement("SELECT COUNT(*) FROM " + MySQLMessageDAO.MESSAGE_TRANSLATION_TABLE + " WHERE language_id=?");
			ps.setInt(1, languageId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) { return rs.getInt(1); }
			throw new DAOException("Expected result");
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find translation count", e);
		}
	}

	@Override
	public List<MessageTranslationPojo> findTranslations(int messageId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_message_translation WHERE message_id=?");
			ps.setInt(1, messageId);
			ResultSet rs = ps.executeQuery();
			return this.extractTranslations(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find message translations", e);
		}
	}

	@Override
	public List<MessageVariablePojo> findVariables(int messageId)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_message_variable WHERE message_id=?");
			ps.setInt(1, messageId);
			ResultSet rs = ps.executeQuery();
			return this.extractMessageVariables(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find message variables", e);
		}
	}

	@Override
	public void updateGlobalVariable(GlobalMessageVariablePojo variable)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection
				.prepareStatement("UPDATE " + MySQLMessageDAO.GLOBAL_VARIABLE_TABLE + " SET name=?, replacement=? WHERE variable_id=?");
			ps.setString(1, variable.getName());
			ps.setString(2, variable.getReplacement());
			ps.setInt(3, variable.getVariableId());
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to update global message variable", e);
		}
	}

	@Override
	public void updateLocaleLanguage(LocaleLanguagePojo localeLanguage) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps =
				connection.prepareStatement("UPDATE ncore_locale_language SET language_id=?, language_name=? WHERE locale_code=?");
			ps.setObject(1, localeLanguage.getLanguageId());
			ps.setString(2, localeLanguage.getLanguageName());
			ps.setString(3, localeLanguage.getLocaleCode());
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to update locale language", e);
		}
	}

	@Override
	public void updateMessage(MessagePojo message) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("UPDATE ncore_message SET name=?, description=? WHERE message_id=?");
			ps.setString(1, message.getName());
			ps.setString(2, message.getDescription());
			ps.setInt(3, message.getMessageId());
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to update message", e);
		}
	}

	@Override
	public void updateSection(MessageSectionPojo section) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection
				.prepareStatement("UPDATE ncore_message_section SET plugin_id=?, name=?, description=? WHERE message_section_id=?");
			ps.setInt(1, section.getPluginId());
			ps.setString(2, section.getName());
			ps.setString(3, section.getDescription());
			ps.setInt(4, section.getSectionId());
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to update message section", e);
		}
	}

	@Override
	public void updateTranslation(MessageTranslationPojo translation) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"UPDATE ncore_message_translation SET message=?, message_type=?, default_message=?, default_message_type=?, use_default_message=?, requires_review=?, last_reviewed=? WHERE message_id=? AND language_id=?");
			ps.setString(1, translation.getMessage());
			ps.setString(2, String.valueOf(translation.getMessageType().getId()));
			ps.setString(3, translation.getDefaultMessage());
			ps.setString(4, String.valueOf(translation.getDefaultMessageType().getId()));
			ps.setBoolean(5, translation.useDefaultMessage());
			ps.setBoolean(6, translation.requiresReview());
			ps.setTimestamp(7, SQLUtil.dateToTimestamp(translation.getLastReviewed()));
			// Id
			ps.setInt(8, translation.getMessageId());
			ps.setInt(9, translation.getLanguageId());
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to update message translation", e);
		}
	}

	@Override
	public void updateVariable(MessageVariablePojo variable) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"UPDATE " + MySQLMessageDAO.MESSAGE_VARIABLE_TABLE + " SET description = ? WHERE message_id = ? AND name = ?");
			ps.setString(1, variable.getDescription());
			ps.setInt(2, variable.getMessageId());
			ps.setString(3, variable.getName());
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to update message variable", e);
		}
	}

	private GlobalMessageVariablePojo extractGlobalMessageVariable(ResultSet rs) throws SQLException
	{
		return new GlobalMessageVariablePojo(rs.getInt("variable_id"), rs.getString("name"), rs.getString("replacement"));
	}

	private GlobalMessageVariablePojo extractGlobalMessageVariablePojo(ResultSet rs) throws SQLException
	{
		return new GlobalMessageVariablePojo(rs.getInt("variable_id"), rs.getString("name"), rs.getString("replacement"));
	}

	private LanguagePojo extractLanguage(ResultSet rs) throws SQLException
	{
		return new LanguagePojo(rs.getInt("language_id"), rs.getString("name"));
	}

	private List<LanguagePojo> extractLanguages(ResultSet rs) throws SQLException
	{
		List<LanguagePojo> pojos = new ArrayList<>();
		while (rs.next())
		{
			pojos.add(this.extractLanguage(rs));
		}
		return pojos;
	}

	private LocaleLanguagePojo extractLocaleLanguage(ResultSet rs) throws SQLException
	{
		Integer languageId = rs.getInt("language_id");
		languageId = rs.wasNull() ? null : languageId;
		return new LocaleLanguagePojo(rs.getString("locale_code"), rs.getString("language_name"), languageId);
	}

	private List<LocaleLanguagePojo> extractLocaleLanguages(ResultSet rs) throws SQLException
	{
		List<LocaleLanguagePojo> pojos = new ArrayList<>();
		while (rs.next())
		{
			pojos.add(this.extractLocaleLanguage(rs));
		}
		return pojos;
	}

	private MessagePojo extractMessage(ResultSet rs) throws SQLException
	{
		return new MessagePojo(rs.getInt("message_id"), rs.getInt("message_section_id"), rs.getString("name"), rs.getString("description"));
	}

	private List<MessagePojo> extractMessages(ResultSet rs) throws SQLException
	{
		List<MessagePojo> pojos = new ArrayList<>();
		while (rs.next())
		{
			pojos.add(this.extractMessage(rs));
		}
		return pojos;
	}

	private MessageSectionPojo extractMessageSection(ResultSet rs) throws SQLException
	{
		return new MessageSectionPojo(rs.getInt("message_section_id"), rs.getInt("plugin_id"), rs.getString("name"),
			rs.getString("description"));
	}

	private List<MessageSectionPojo> extractMessageSections(ResultSet rs) throws SQLException
	{
		List<MessageSectionPojo> pojos = new ArrayList<>();
		while (rs.next())
		{
			pojos.add(this.extractMessageSection(rs));
		}
		return pojos;
	}

	private MessageVariablePojo extractMessageVariable(ResultSet rs) throws SQLException
	{
		return new MessageVariablePojo(rs.getInt("message_id"), rs.getString("name"), rs.getString("description"));
	}

	private List<MessageVariablePojo> extractMessageVariables(ResultSet rs) throws SQLException
	{
		List<MessageVariablePojo> pojos = new ArrayList<>();
		while (rs.next())
		{
			pojos.add(this.extractMessageVariable(rs));
		}
		return pojos;
	}

	private MessageTranslationPojo extractTranslation(ResultSet rs) throws SQLException
	{
		return new MessageTranslationPojo(rs.getInt("message_id"), rs.getInt("language_id"), rs.getString("message"),
			MessageType.fromId(rs.getString("message_type").charAt(0)), rs.getString("default_message"),
			MessageType.fromId(rs.getString("default_message_type").charAt(0)), rs.getBoolean("use_default_message"),
			rs.getBoolean("requires_review"), SQLUtil.timestampToDate(rs.getTimestamp("last_reviewed")));
	}

	private List<MessageTranslationPojo> extractTranslations(ResultSet rs) throws SQLException
	{
		List<MessageTranslationPojo> pojos = new ArrayList<>();
		while (rs.next())
		{
			pojos.add(this.extractTranslation(rs));
		}
		return pojos;
	}
}
