package net.rieksen.networkcore.core.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import net.rieksen.networkcore.core.dao.DAOException;
import net.rieksen.networkcore.core.dao.IssueDAO;
import net.rieksen.networkcore.core.issue.IssuePojo;
import net.rieksen.networkcore.core.util.IMySQL;
import net.rieksen.networkcore.core.util.SQLUtil;
import net.rieksen.networkcore.core.util.SimplePage;

public class MySQLIssueDAO extends MySQLBaseDAO implements IssueDAO
{

	public MySQLIssueDAO(IMySQL mysql)
	{
		super(mysql);
	}

	@Override
	public IssuePojo createIssue(IssuePojo issue) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"INSERT INTO ncore_issue (plugin_id, issue_name, issue_priority, creation_date, description, issue_generator, resolved, issue_data) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
				new String[] { "issue_id" });
			ps.setInt(1, issue.getPluginId());
			ps.setString(2, issue.getIssueName());
			ps.setInt(3, issue.getPriority());
			ps.setTimestamp(4, SQLUtil.dateToTimestamp(issue.getCreationDate()));
			ps.setString(5, issue.getDescription());
			ps.setString(6, issue.getIssueGenerator());
			ps.setBoolean(7, issue.isResolved());
			ps.setString(8, issue.getIssueData());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next())
			{
				issue.setIssueId(rs.getInt(1));
				return issue;
			}
			throw new DAOException("Expected generated column");
		} catch (SQLException e)
		{
			throw new DAOException("Failed to create issue", e);
		}
	}

	@Override
	public IssuePojo findIssue(int issueId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_issue WHERE issue_id=?");
			ps.setInt(1, issueId);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractIssue(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find issue", e);
		}
	}

	@Override
	public SimplePage<IssuePojo> findUnresolvedIssues(int startIndex, int maxResults)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection
				.prepareStatement("SELECT SQL_CALC_FOUND_ROWS * FROM ncore_issue WHERE resolved=0 ORDER BY issue_priority DESC LIMIT ?,?");
			ps.setInt(1, startIndex);
			ps.setInt(2, maxResults);
			ResultSet rs = ps.executeQuery();
			List<IssuePojo> data = this.extractIssues(rs);
			ResultSet totalRs = ps.executeQuery("SELECT FOUND_ROWS()");
			int total = totalRs.next() ? totalRs.getInt(1) : -1;
			return new SimplePage<>(startIndex * maxResults, maxResults, total, data);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find unresolved issues", e);
		}
	}

	@Override
	public List<IssuePojo> findUnresolvedIssuesOfGenerator(int pluginId, String name)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps =
				connection.prepareStatement("SELECT * FROM ncore_issue WHERE plugin_id=? AND issue_generator=? AND resolved=0");
			ps.setInt(1, pluginId);
			ps.setString(2, name);
			ResultSet rs = ps.executeQuery();
			return this.extractIssues(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find unresolved issues of generator", e);
		}
	}

	@Override
	public IssuePojo updateIssue(int issueId, IssuePojo issue)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"UPDATE ncore_issue SET plugin_id=?, issue_name=?, issue_priority=?, creation_date=?, description=?, issue_generator=?, resolved=?, issue_data=? WHERE issue_id=?");
			ps.setInt(1, issue.getPluginId());
			ps.setString(2, issue.getIssueName());
			ps.setInt(3, issue.getPriority());
			ps.setTimestamp(4, SQLUtil.dateToTimestamp(issue.getCreationDate()));
			ps.setString(5, issue.getDescription());
			ps.setString(6, issue.getIssueGenerator());
			ps.setBoolean(7, issue.isResolved());
			ps.setString(8, issue.getIssueData());
			ps.setInt(9, issue.getIssueId());
			ps.executeUpdate();
			return issue;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to update issue", e);
		}
	}

	private IssuePojo extractIssue(ResultSet rs) throws SQLException
	{
		return new IssuePojo(rs.getInt("issue_id"), rs.getInt("plugin_id"), rs.getString("issue_name"), rs.getInt("issue_priority"),
			SQLUtil.timestampToDate(rs.getTimestamp("creation_date")), rs.getString("description"), rs.getString("issue_generator"),
			rs.getBoolean("resolved"), rs.getString("issue_data"));
	}

	private List<IssuePojo> extractIssues(ResultSet rs) throws SQLException
	{
		List<IssuePojo> pojos = new ArrayList<>();
		while (rs.next())
		{
			pojos.add(this.extractIssue(rs));
		}
		return pojos;
	}
}
