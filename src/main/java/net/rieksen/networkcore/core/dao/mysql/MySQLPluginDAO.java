package net.rieksen.networkcore.core.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.rieksen.networkcore.core.dao.DAOException;
import net.rieksen.networkcore.core.dao.PluginDAO;
import net.rieksen.networkcore.core.plugin.ServerPluginPojo;
import net.rieksen.networkcore.core.plugin.pojo.NetworkPluginPojo;
import net.rieksen.networkcore.core.plugin.pojo.PluginTaskExecutionPojo;
import net.rieksen.networkcore.core.plugin.pojo.PluginTaskPojo;
import net.rieksen.networkcore.core.util.IMySQL;
import net.rieksen.networkcore.core.util.SQLUtil;

public class MySQLPluginDAO extends MySQLBaseDAO implements PluginDAO
{

	public final static String PLUGIN_TABLE = "ncore_plugin";

	public MySQLPluginDAO(IMySQL mysql)
	{
		super(mysql);
	}

	@Override
	public int createPlugin(NetworkPluginPojo plugin)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"INSERT INTO " + MySQLPluginDAO.PLUGIN_TABLE + " (name, database_version, language_id) VALUES (?, ?, ?)",
				new String[] { "plugin_id" });
			ps.setString(1, plugin.getName());
			ps.setString(2, plugin.getDatabaseVersion());
			ps.setObject(3, plugin.getLanguageId());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) { return rs.getInt(1); }
			throw new DAOException("Expected generated column");
		} catch (SQLException e)
		{
			throw new DAOException("Failed to create plugin", e);
		}
	}

	@Override
	public void createPluginTask(PluginTaskPojo pojo)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"INSERT INTO ncore_plugin_task (plugin_id, task_name, task_interval, task_last_run, execution_status, claimed_till, is_global) VALUES (?, ?, ?, ?, ?, ?, ?)");
			ps.setInt(1, pojo.getPluginId());
			ps.setString(2, pojo.getTaskName());
			ps.setInt(3, pojo.getTaskInterval());
			ps.setTimestamp(4, SQLUtil.dateToTimestamp(pojo.getLastRun()));
			ps.setString(5, String.valueOf(pojo.getExecutionStatus().getId()));
			ps.setTimestamp(6, SQLUtil.dateToTimestamp(pojo.getClaimedTill()));
			ps.setBoolean(7, pojo.isGlobal());
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to create plugin task", e);
		}
	}

	@Override
	public PluginTaskExecutionPojo createPluginTaskExecution(PluginTaskExecutionPojo execution)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"INSERT INTO ncore_plugin_task_execution (plugin_id, task_name, server_id, execution_date, execution_status, summary, duration) VALUES (?, ?, ?, ?, ?, ?, ?)");
			ps.setInt(1, execution.getPluginId());
			ps.setString(2, execution.getTaskName());
			ps.setInt(3, execution.getServerId());
			ps.setTimestamp(4, SQLUtil.dateToTimestamp(execution.getExecutionDate()));
			ps.setString(5, String.valueOf(execution.getExecutionStatus().getId()));
			ps.setString(6, execution.getSummary());
			ps.setLong(7, execution.getDuration());
			ps.executeUpdate();
			return execution;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to create plugin task execution", e);
		}
	}

	@Override
	public void createServerPlugin(ServerPluginPojo pojo)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"INSERT INTO ncore_server_plugin (server_id, plugin_id, plugin_version, enabled, previous_plugin_version) VALUES (?, ?, ?, ?, ?)");
			ps.setInt(1, pojo.getServerId());
			ps.setInt(2, pojo.getPluginId());
			ps.setString(3, pojo.getPluginVersion());
			ps.setBoolean(4, pojo.isEnabled());
			ps.setString(5, pojo.getPreviousPluginVersion());
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to create server plugin", e);
		}
	}

	@Override
	public int deleteOldestPluginTaskExecutionsTill(int pluginId, String taskName, int amount)
	{
		amount--;
		if (amount < 0)
		{
			amount = 0;
		}
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"DELETE FROM ncore_plugin_task_execution WHERE plugin_id=? AND task_name=? AND execution_date < (SELECT * FROM (SELECT execution_date FROM ncore_plugin_task_execution WHERE plugin_id=? AND task_name=? ORDER BY execution_date DESC LIMIT "
					+ amount + ",1) AS t)");
			ps.setInt(1, pluginId);
			ps.setString(2, taskName);
			ps.setInt(3, pluginId);
			ps.setString(4, taskName);
			return ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to delete oldest plugin task executions", e);
		}
	}

	@Override
	public void deletePluginTask(int pluginId, String taskName)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("DELETE FROM ncore_plugin_task WHERE plugin_id=? AND task_name=?");
			ps.setInt(1, pluginId);
			ps.setString(2, taskName);
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to delete plugin task");
		}
	}

	@Override
	public List<PluginTaskPojo> findPendingPluginTasks(int serverId)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"SELECT * FROM ncore_plugin_task WHERE ? > DATE_ADD(task_last_run, INTERVAL task_interval SECOND) AND plugin_id IN (SELECT plugin_id FROM ncore_server_plugin WHERE server_id = ? AND enabled = 1)");
			ps.setTimestamp(1, SQLUtil.dateToTimestamp(new Date()));
			ps.setInt(2, serverId);
			ResultSet rs = ps.executeQuery();
			return this.extractPluginTasks(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find pending plugin tasks", e);
		}
	}

	@Override
	public List<PluginTaskPojo> findPendingPluginTasks(int serverId, Date claimedTill)
	{
		if (claimedTill != null)
		{
			// round to seconds
			claimedTill.setTime(claimedTill.getTime() - claimedTill.getTime() % 1000);
		}
		try (Connection connection = this.getConnection())
		{
			connection.setTransactionIsolation(Connection.TRANSACTION_REPEATABLE_READ);
			// claim
			PreparedStatement psUpdate = connection.prepareStatement(
				"UPDATE ncore_plugin_task SET claimed_till=?, claimed_server_id=? WHERE (plugin_id, task_name) IN (SELECT * FROM (SELECT plugin_id, task_name FROM ncore_plugin_task WHERE is_global=1 AND ? > DATE_ADD(task_last_run, INTERVAL task_interval SECOND) AND (claimed_till IS NULL OR claimed_till < ?) AND plugin_id IN (SELECT plugin_id FROM ncore_server_plugin WHERE server_id = ? AND enabled = 1)) AS X)");
			psUpdate.setTimestamp(1, SQLUtil.dateToTimestamp(claimedTill));
			psUpdate.setInt(2, serverId);
			Date currentDate = new Date();
			psUpdate.setTimestamp(3, SQLUtil.dateToTimestamp(currentDate));
			psUpdate.setTimestamp(4, SQLUtil.dateToTimestamp(currentDate));
			psUpdate.setInt(5, serverId);
			psUpdate.executeUpdate();
			// read
			PreparedStatement ps = connection.prepareStatement(
				"SELECT * FROM ncore_plugin_task pt WHERE claimed_server_id=? AND claimed_till=? OR (is_global=0 AND (NOT EXISTS (SELECT 1 FROM ncore_plugin_task_execution pte WHERE pte.plugin_id=pt.plugin_id AND pte.task_name=pt.task_name AND server_id=?) OR ? > DATE_ADD((SELECT MAX(execution_date) FROM ncore_plugin_task_execution pte WHERE pte.plugin_id=pt.plugin_id AND pte.task_name=pt.task_name AND pte.server_id=?), INTERVAL task_interval SECOND)))");
			ps.setInt(1, serverId);
			ps.setTimestamp(2, SQLUtil.dateToTimestamp(claimedTill));
			ps.setInt(3, serverId);
			ps.setTimestamp(4, SQLUtil.dateToTimestamp(currentDate));
			ps.setInt(5, serverId);
			ResultSet rs = ps.executeQuery();
			return this.extractPluginTasks(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find pending plugin tasks", e);
		}
	}

	@Override
	public NetworkPluginPojo findPlugin(int pluginId)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM " + MySQLPluginDAO.PLUGIN_TABLE + " WHERE plugin_id=?");
			ps.setInt(1, pluginId);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractPlugin(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find plugin", e);
		}
	}

	@Override
	public NetworkPluginPojo findPlugin(String name)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM " + MySQLPluginDAO.PLUGIN_TABLE + " WHERE name=?");
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractPlugin(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find plugin", e);
		}
	}

	@Override
	public List<NetworkPluginPojo> findPlugins()
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM " + MySQLPluginDAO.PLUGIN_TABLE + "");
			ResultSet rs = ps.executeQuery();
			return this.extractPlugins(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find plugins", e);
		}
	}

	@Override
	public List<NetworkPluginPojo> findPlugins(int startIndex, int count)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM " + MySQLPluginDAO.PLUGIN_TABLE + " LIMIT ?, ?");
			ps.setInt(1, startIndex);
			ps.setInt(2, count);
			ResultSet rs = ps.executeQuery();
			return this.extractPlugins(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find plugins", e);
		}
	}

	@Override
	public PluginTaskPojo findPluginTask(int pluginId, String taskName)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_plugin_task WHERE plugin_id=? AND task_name=?");
			ps.setInt(1, pluginId);
			ps.setString(2, taskName);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractPluginTask(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find plugin task", e);
		}
	}

	@Override
	public List<PluginTaskExecutionPojo> findPluginTaskExecutions(int pluginId, String taskName)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"SELECT * FROM ncore_plugin_task_execution WHERE plugin_id=? AND task_name=? ORDER BY execution_date DESC");
			ps.setInt(1, pluginId);
			ps.setString(2, taskName);
			ResultSet rs = ps.executeQuery();
			return this.extractPluginTaskExecutions(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find plugin task executions", e);
		}
	}

	@Override
	public List<PluginTaskPojo> findPluginTasks(int pluginId)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_plugin_task WHERE plugin_id=?");
			ps.setInt(1, pluginId);
			ResultSet rs = ps.executeQuery();
			return this.extractPluginTasks(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find plugin tasks", e);
		}
	}

	@Override
	public ServerPluginPojo findServerPlugin(int serverId, int pluginId)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_server_plugin WHERE server_id=? AND plugin_id=?");
			ps.setInt(1, serverId);
			ps.setInt(2, pluginId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) { return this.extractServerPlugin(rs); }
			return null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find server plugin", e);
		}
	}

	@Override
	public List<ServerPluginPojo> findServerPlugins(int serverId)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_server_plugin WHERE server_id=?");
			ps.setInt(1, serverId);
			ResultSet rs = ps.executeQuery();
			return this.extractServerPlugins(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find server plugins", e);
		}
	}

	@Override
	public List<ServerPluginPojo> findServersWithPlugin(int pluginId)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_server_plugin WHERE plugin_id=?");
			ps.setInt(1, pluginId);
			ResultSet rs = ps.executeQuery();
			return this.extractServerPlugins(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find servers with plugin", e);
		}
	}

	@Override
	public void updatePlugin(NetworkPluginPojo plugin)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"UPDATE " + MySQLPluginDAO.PLUGIN_TABLE + " SET name=?, database_version=?, language_id=? WHERE plugin_id=?");
			ps.setString(1, plugin.getName());
			ps.setString(2, plugin.getDatabaseVersion());
			ps.setObject(3, plugin.getLanguageId());
			ps.setInt(4, plugin.getPluginId());
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to update plugin", e);
		}
	}

	@Override
	public void updatePluginTask(PluginTaskPojo pojo)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"UPDATE ncore_plugin_task SET task_interval=?, task_last_run=?, execution_status=?, claimed_till=?, claimed_server_id=?, is_global=? WHERE plugin_id=? AND task_name=?");
			ps.setInt(1, pojo.getTaskInterval());
			ps.setTimestamp(2, SQLUtil.dateToTimestamp(pojo.getLastRun()));
			ps.setString(3, String.valueOf(pojo.getExecutionStatus().getId()));
			ps.setTimestamp(4, SQLUtil.dateToTimestamp(pojo.getClaimedTill()));
			ps.setObject(5, pojo.getClaimedServerId());
			ps.setBoolean(6, pojo.isGlobal());
			ps.setInt(7, pojo.getPluginId());
			ps.setString(8, pojo.getTaskName());
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to update plugin task", e);
		}
	}

	@Override
	public void updateServerPlugin(ServerPluginPojo pojo)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"UPDATE ncore_server_plugin SET plugin_version=?, enabled=?, previous_plugin_version=? WHERE server_id=? AND plugin_id=?");
			ps.setString(1, pojo.getPluginVersion());
			ps.setBoolean(2, pojo.isEnabled());
			ps.setString(3, pojo.getPreviousPluginVersion());
			ps.setInt(4, pojo.getServerId());
			ps.setInt(5, pojo.getPluginId());
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to update server plugin", e);
		}
	}

	private NetworkPluginPojo extractPlugin(ResultSet rs) throws SQLException
	{
		return new NetworkPluginPojo(rs.getInt("plugin_id"), rs.getString("name"), rs.getString("database_version"),
			SQLUtil.getInteger(rs, "language_id"));
	}

	private List<NetworkPluginPojo> extractPlugins(ResultSet rs) throws SQLException
	{
		List<NetworkPluginPojo> list = new ArrayList<>();
		while (rs.next())
		{
			list.add(this.extractPlugin(rs));
		}
		return list;
	}

	private PluginTaskPojo extractPluginTask(ResultSet rs) throws SQLException
	{
		return new PluginTaskPojo(rs.getInt("plugin_id"), rs.getString("task_name"), rs.getInt("task_interval"),
			SQLUtil.timestampToDate(rs.getTimestamp("task_last_run")),
			PluginTaskPojo.ExecutionStatus.fromId(rs.getString("execution_status").charAt(0)),
			SQLUtil.timestampToDate(rs.getTimestamp("claimed_till")), SQLUtil.getInteger(rs, "claimed_server_id"),
			rs.getBoolean("is_global"));
	}

	private PluginTaskExecutionPojo extractPluginTaskExecution(ResultSet rs) throws SQLException
	{
		return new PluginTaskExecutionPojo(rs.getInt("plugin_id"), rs.getString("task_name"), rs.getInt("server_id"),
			SQLUtil.timestampToDate(rs.getTimestamp("execution_date")),
			PluginTaskPojo.ExecutionStatus.fromId(rs.getString("execution_status").charAt(0)), rs.getString("summary"),
			rs.getLong("duration"));
	}

	private List<PluginTaskExecutionPojo> extractPluginTaskExecutions(ResultSet rs) throws SQLException
	{
		List<PluginTaskExecutionPojo> list = new ArrayList<>();
		while (rs.next())
		{
			list.add(this.extractPluginTaskExecution(rs));
		}
		return list;
	}

	private List<PluginTaskPojo> extractPluginTasks(ResultSet rs) throws SQLException
	{
		List<PluginTaskPojo> pojos = new ArrayList<>();
		while (rs.next())
		{
			pojos.add(this.extractPluginTask(rs));
		}
		return pojos;
	}

	private ServerPluginPojo extractServerPlugin(ResultSet rs) throws SQLException
	{
		return new ServerPluginPojo(rs.getInt("server_id"), rs.getInt("plugin_id"), rs.getString("plugin_version"),
			rs.getBoolean("enabled"), rs.getString("previous_plugin_version"));
	}

	private List<ServerPluginPojo> extractServerPlugins(ResultSet rs) throws SQLException
	{
		List<ServerPluginPojo> pojos = new ArrayList<>();
		while (rs.next())
		{
			pojos.add(this.extractServerPlugin(rs));
		}
		return pojos;
	}
}
