package net.rieksen.networkcore.core.dao;

import java.util.List;

import net.rieksen.networkcore.core.world.pojo.ServerWorldPojo;
import net.rieksen.networkcore.core.world.pojo.WorldLocationPojo;

public interface WorldDAO
{

	public int createLocation(WorldLocationPojo location) throws DAOException;

	public int createWorld(ServerWorldPojo world) throws DAOException;

	public void deleteLocation(int locationId) throws DAOException;

	public WorldLocationPojo findLocation(int locationId) throws DAOException;

	public ServerWorldPojo findWorld(int worldId) throws DAOException;

	public ServerWorldPojo findWorld(int serverId, String name) throws DAOException;

	public List<ServerWorldPojo> findWorlds() throws DAOException;

	public List<ServerWorldPojo> findWorlds(int serverId) throws DAOException;

	public void updateLocation(WorldLocationPojo location) throws DAOException;

	/**
	 * Updates the world.
	 * 
	 * @param world
	 * @throws DAOException
	 */
	void updateWorld(ServerWorldPojo world) throws DAOException;
}
