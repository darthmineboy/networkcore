package net.rieksen.networkcore.core.dao.mysql.update;

import java.sql.Connection;
import java.sql.SQLException;

import net.rieksen.networkcore.core.update.MySQLDAOUpdate;
import net.rieksen.networkcore.core.util.IMySQL;

class MySQLDAOUpdate_v2_0_2 extends MySQLDAOUpdate
{

	public MySQLDAOUpdate_v2_0_2(IMySQL mysql)
	{
		super(mysql, "2.0.1", "2.0.2", "Track plugins that are installed on each server");
	}

	@Override
	public void doTransaction(Connection connection) throws SQLException
	{
		connection.createStatement()
			.executeUpdate("CREATE TABLE IF NOT EXISTS ncore_server_plugin (" + "server_id INT NOT NULL," + "plugin_id INT NOT NULL,"
				+ "plugin_version VARCHAR(255) NOT NULL," + "enabled BOOLEAN NOT NULL," + "PRIMARY KEY (server_id, plugin_id),"
				+ "CONSTRAINT fk_ncore_server_plugin_server FOREIGN KEY (server_id) REFERENCES ncore_server(server_id) ON UPDATE CASCADE ON DELETE CASCADE,"
				+ "CONSTRAINT fk_ncore_server_plugin_plugin FOREIGN KEY (plugin_id) REFERENCES ncore_plugin(plugin_id) ON UPDATE CASCADE ON DELETE CASCADE"
				+ ") ENGINE=InnoDB DEFAULT CHARSET=utf8");
	}
}
