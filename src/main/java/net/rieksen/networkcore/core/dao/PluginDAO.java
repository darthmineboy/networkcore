package net.rieksen.networkcore.core.dao;

import java.util.Date;
import java.util.List;

import net.rieksen.networkcore.core.plugin.PluginModule;
import net.rieksen.networkcore.core.plugin.ServerPluginPojo;
import net.rieksen.networkcore.core.plugin.pojo.NetworkPluginPojo;
import net.rieksen.networkcore.core.plugin.pojo.PluginTaskExecutionPojo;
import net.rieksen.networkcore.core.plugin.pojo.PluginTaskPojo;

public interface PluginDAO
{

	public int createPlugin(NetworkPluginPojo plugin);

	/**
	 * Deletes a plugin task.
	 * 
	 * @param pluginId
	 * @param taskName
	 */
	public void deletePluginTask(int pluginId, String taskName);

	public NetworkPluginPojo findPlugin(int pluginId);

	public NetworkPluginPojo findPlugin(String name);

	public List<NetworkPluginPojo> findPlugins();

	public List<NetworkPluginPojo> findPlugins(int startIndex, int count);

	public PluginTaskPojo findPluginTask(int pluginId, String taskName);

	public List<PluginTaskPojo> findPluginTasks(int pluginId);

	public List<ServerPluginPojo> findServerPlugins(int serverId);

	void createPluginTask(PluginTaskPojo pojo);

	PluginTaskExecutionPojo createPluginTaskExecution(PluginTaskExecutionPojo execution);

	void createServerPlugin(ServerPluginPojo pojo);

	/**
	 * Deletes the oldest plugin task executions so that every task has a
	 * maximum of <code>amount</code> executions left.
	 * 
	 * @param amount
	 * @return executions deleted
	 */
	int deleteOldestPluginTaskExecutionsTill(int pluginId, String taskName, int amount);

	/**
	 * @deprecated Replaced by {@link #findPendingPluginTasks(int, Date)}.
	 * @param serverId
	 * @return
	 */
	@Deprecated
	List<PluginTaskPojo> findPendingPluginTasks(int serverId);

	/**
	 * Finds pending plugin tasks and claims them for
	 * {@link PluginModule#PLUGIN_TASK_CLAIM_MS}.
	 * 
	 * @param serverId
	 * @param claimedTill
	 *            only plugin tasks where claimedTill is null or in the past are
	 *            returned
	 * @return
	 */
	List<PluginTaskPojo> findPendingPluginTasks(int serverId, Date claimedTill);

	List<PluginTaskExecutionPojo> findPluginTaskExecutions(int pluginId, String taskName);

	ServerPluginPojo findServerPlugin(int serverId, int pluginId);

	/**
	 * Finds all servers who have the provided plugin installed.
	 * 
	 * @param pluginId
	 * @return
	 */
	List<ServerPluginPojo> findServersWithPlugin(int pluginId);

	void updatePlugin(NetworkPluginPojo plugin);

	void updatePluginTask(PluginTaskPojo pojo);

	void updateServerPlugin(ServerPluginPojo pojo);
}
