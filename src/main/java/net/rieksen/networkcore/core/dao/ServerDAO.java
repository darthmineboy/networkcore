package net.rieksen.networkcore.core.dao;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import net.rieksen.networkcore.core.server.pojo.ServerGroupPojo;
import net.rieksen.networkcore.core.server.pojo.ServerLogPojo;
import net.rieksen.networkcore.core.server.pojo.ServerPojo;
import net.rieksen.networkcore.core.server.pojo.ServerResourceUsagePojo;
import net.rieksen.networkcore.core.server.pojo.ServerRuntimePojo;
import net.rieksen.networkcore.core.server.pojo.ServerStatisticPojo;

public interface ServerDAO
{

	void createGroupServer(int groupId, int serverId) throws DAOException;

	long createLog(ServerLogPojo log) throws DAOException;

	int createRuntime(ServerRuntimePojo runtime) throws DAOException;

	int createServer(ServerPojo server) throws DAOException;

	/**
	 * Creates a new server group.
	 * 
	 * @param group
	 * @return the generated groupId
	 * @throws DAOException
	 */
	int createServerGroup(ServerGroupPojo group) throws DAOException;

	void createServerResourceUsage(ServerResourceUsagePojo resource) throws DAOException;

	void createServerStatistic(ServerStatisticPojo statistic) throws DAOException;

	void deleteGroupServer(int groupId, int serverId) throws DAOException;

	/**
	 * Purge all logs before given date.
	 * 
	 * @param dateBefore
	 * @return deleted logs
	 */
	int deleteLogsBefore(Date dateBefore);

	void deleteServer(int serverId);

	void deleteServerGroup(int groupId) throws DAOException;

	/**
	 * Purge all resource usage statistics before given date.
	 * 
	 * @param dateBefore
	 * @return deleted resourec usages
	 */
	int deleteServerResourceUsageBefore(Date dateBefore);

	List<ServerPojo> findGroupServers(int groupId) throws DAOException;

	List<ServerGroupPojo> findGroupsOfServer(int serverId) throws DAOException;

	List<ServerLogPojo> findLatestLogs(Integer serverId, int startIndex, int maxResults) throws DAOException;

	List<ServerLogPojo> findLatestLogs(Integer serverId, Level level, String search, int startIndex, int maxResults);

	List<ServerLogPojo> findLatestLogs(Integer serverId, String search, int startIndex, int maxResults);

	ServerRuntimePojo findLatestRuntime(int serverId) throws DAOException;

	List<ServerRuntimePojo> findLatestRuntimes(int serverId, int startIndex, int count) throws DAOException;

	ServerResourceUsagePojo findLatestServerResourceUsage(int serverId) throws DAOException;

	ServerLogPojo findLog(long logId);

	int findLogsCount(Integer serverId);

	int findLogsCount(Integer serverId, Level level, String search);

	int findLogsCount(Integer serverId, String search);

	int findOnlineServerCount(Date date) throws DAOException;

	List<ServerRuntimePojo> findOpenRuntimes(int serverId) throws DAOException;

	ServerRuntimePojo findRuntime(int runtimeId) throws DAOException;

	int findRuntimesOfServerCount(int serverId);

	ServerPojo findServer(int serverId) throws DAOException;

	ServerPojo findServer(String name) throws DAOException;

	ServerGroupPojo findServerGroup(int groupId) throws DAOException;

	ServerGroupPojo findServerGroup(String groupName) throws DAOException;

	List<ServerGroupPojo> findServerGroups() throws DAOException;

	List<ServerGroupPojo> findServerGroups(String search, int startIndex, int maxResults);

	List<ServerPojo> findServers() throws DAOException;

	List<ServerPojo> findServers(int startIndex, int count) throws DAOException;

	/**
	 * Finds servers containing this name.
	 * 
	 * @param name
	 * @return
	 * @throws DAOException
	 */
	List<ServerPojo> findServers(String name) throws DAOException;

	List<ServerPojo> findServersByName(String name, int startIndex, int count);

	int findServersByNameCount(String name);

	int findServersCount();

	List<ServerStatisticPojo> findServerStatistics(int serverId, Date since);

	List<ServerPojo> findServersWithStatus(boolean online) throws DAOException;

	/**
	 * Checks whether a server is part of a server group.
	 * 
	 * @param groupId
	 * @param serverId
	 * @return
	 */
	boolean isServerInGroup(int groupId, int serverId);

	void updateRuntime(ServerRuntimePojo runtime) throws DAOException;

	void updateServer(ServerPojo server) throws DAOException;

	void updateServerGroup(int groupId, ServerGroupPojo group) throws DAOException;
}
