package net.rieksen.networkcore.core.dao;

/**
 * 
 *
 */
public interface DAOManager
{

	void destroy();

	String getDatabaseVersion();

	InfoDAO getInfoDAO();

	MessageDAO getMessageDAO();

	OptionDAO getOptionDAO();

	PluginDAO getPluginDAO();

	ServerDAO getServerDAO();

	StatisticDAO getStatisticDAO();

	IssueDAO getIssueDAO();

	UserDAO getUserDAO();

	WorldDAO getWorldDAO();

	void init();

	void setDatabaseVersion(String version);
}
