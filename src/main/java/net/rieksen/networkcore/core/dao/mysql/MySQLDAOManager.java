package net.rieksen.networkcore.core.dao.mysql;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.dao.DAOException;
import net.rieksen.networkcore.core.dao.DAOManager;
import net.rieksen.networkcore.core.dao.InfoDAO;
import net.rieksen.networkcore.core.dao.MessageDAO;
import net.rieksen.networkcore.core.dao.OptionDAO;
import net.rieksen.networkcore.core.dao.PluginDAO;
import net.rieksen.networkcore.core.dao.ServerDAO;
import net.rieksen.networkcore.core.dao.StatisticDAO;
import net.rieksen.networkcore.core.dao.UserDAO;
import net.rieksen.networkcore.core.dao.WorldDAO;
import net.rieksen.networkcore.core.dao.IssueDAO;
import net.rieksen.networkcore.core.dao.mysql.update.MySQLDAOUpdateManager;
import net.rieksen.networkcore.core.info.NetworkCoreInfo;
import net.rieksen.networkcore.core.mysql.MySQLUtil;
import net.rieksen.networkcore.core.util.IMySQL;

public class MySQLDAOManager implements DAOManager
{

	private final IMySQL	mysql;
	private MySQLUserDAO	userDAO;
	private MySQLServerDAO	serverDAO;
	private PluginDAO		pluginDAO;
	private MySQLWorldDAO	worldDAO;
	private MySQLMessageDAO	messageDAO;
	private OptionDAO		optionDAO;
	private NetworkCore	provider;
	private MySQLInfoDAO	infoDAO;
	private StatisticDAO	statisticDAO;
	private MySQLIssueDAO	issueDAO;

	public MySQLDAOManager(NetworkCore provider, IMySQL mysql, boolean createTables) throws DAOException
	{
		this.provider = provider;
		this.mysql = mysql;
		this.infoDAO = new MySQLInfoDAO(mysql);
		this.serverDAO = new MySQLServerDAO(mysql);
		this.pluginDAO = new MySQLPluginDAO(mysql);
		this.optionDAO = new MySQLOptionDAO(mysql);
		this.messageDAO = new MySQLMessageDAO(mysql);
		this.worldDAO = new MySQLWorldDAO(mysql);
		this.userDAO = new MySQLUserDAO(mysql);
		this.statisticDAO = new MySQLStatisticDAO(mysql);
		this.issueDAO = new MySQLIssueDAO(mysql);
	}

	/**
	 * Silently close a connection. Connection may be null.
	 * 
	 * @param connection
	 */
	public void closeConnection(Connection connection)
	{
		if (connection != null)
		{
			try
			{
				connection.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
	}

	public boolean columnExists(Connection con, String tableName, String columnName) throws SQLException
	{
		DatabaseMetaData dbm = con.getMetaData();
		ResultSet columns = dbm.getColumns(null, null, tableName, columnName);
		while (columns.next())
		{
			if (this.isTableInResultSet(columns, tableName) && this.isColumnInResultSet(columnName, columns)) { return true; }
		}
		return false;
	}

	@Override
	public void destroy()
	{
		try
		{
			this.mysql.destroy();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to destroy", e);
		} ;
	}

	public Connection getConnection() throws SQLException
	{
		return this.mysql.getConnection();
	}

	@Override
	public String getDatabaseVersion()
	{
		try
		{
			return this.infoDAO.findInfo(NetworkCoreInfo.DATABASE_VERSION);
		} catch (Exception e)
		{
			this.provider.getLogger().warning("Failed to find database version in regular way, lets investigate!");
		}
		try (Connection connection = this.getConnection())
		{
			/**
			 * Database version is no longer stored in the `ncore_plugin` table,
			 * because that is more likely to break than this simple key/value
			 * table.
			 */
			if (!MySQLUtil.tableExists(connection, "ncore_info"))
			{
				this.provider.getLogger()
					.warning("Table `ncore_info` does not exists, assuming fresh installation (or updating from v1.x -> 2.x)");
				return null;
			}
			/**
			 * Initial code to check database version; see above for new result
			 */
			if (!MySQLUtil.tableExists(connection, "plugin") && !MySQLUtil.tableExists(connection, "ncore_plugin"))
			{
				this.provider.getLogger().warning("Table `plugin` and `ncore_plugin` do not exist, assuming fresh installation");
				return null;
			}
			if (!MySQLUtil.tableExists(connection, "ncore_plugin"))
			{
				this.provider.getLogger().warning(
					"Table `ncore_plugin` does not exist, assuming database version v1.x (ignore this when upgrading from v1.x -> 2.x)");
				return null;
			}
			if (!MySQLUtil.columnExists(connection, "ncore_plugin", "database_version"))
			{
				this.provider.getLogger().warning(
					"Column `ncore_plugin`.`database_version` does not exist, assuming database version v1.x (ignore this when upgrading from v1.x -> 2.x)");
				return null;
			}
		} catch (SQLException e)
		{
			throw new DAOException("Failed to get database version: an unexpected error occurred", e);
		}
		throw new IllegalStateException("Failed to get database version: could not find root of problem");
	}

	@Override
	public InfoDAO getInfoDAO()
	{
		return this.infoDAO;
	}

	@Override
	public MessageDAO getMessageDAO()
	{
		return this.messageDAO;
	}

	public IMySQL getMySQL()
	{
		return this.mysql;
	}

	@Override
	public OptionDAO getOptionDAO()
	{
		return this.optionDAO;
	}

	@Override
	public PluginDAO getPluginDAO()
	{
		return this.pluginDAO;
	}

	public NetworkCore getProvider()
	{
		return this.provider;
	}

	@Override
	public ServerDAO getServerDAO()
	{
		return this.serverDAO;
	}

	@Override
	public StatisticDAO getStatisticDAO()
	{
		return this.statisticDAO;
	}

	@Override
	public IssueDAO getIssueDAO()
	{
		return this.issueDAO;
	}

	@Override
	public UserDAO getUserDAO()
	{
		return this.userDAO;
	}

	@Override
	public WorldDAO getWorldDAO()
	{
		return this.worldDAO;
	}

	@Override
	public void init()
	{
		new MySQLDAOUpdateManager(this.provider, this).update();
	}

	public boolean isColumnDataTypeEqual(Connection con, String tableName, String columnName, int type, Integer length) throws SQLException
	{
		DatabaseMetaData dbm = con.getMetaData();
		ResultSet columns = dbm.getColumns(null, null, tableName, columnName);
		boolean result = false;
		while (columns.next())
		{
			if (this.isTableInResultSet(columns, tableName) && this.isColumnInResultSet(columnName, columns)
				&& this.isDataTypeInResultSet(columns, type))
			{
				if (length != null && !this.isDataLengthInResultSet(columns, length))
				{
					continue;
				}
				result = true;
				break;
			}
		}
		return result;
	}

	public boolean isColumnNullable(Connection con, String tableName, String columnName) throws SQLException
	{
		DatabaseMetaData dbm = con.getMetaData();
		ResultSet columns = dbm.getColumns(null, null, tableName, columnName);
		boolean result = false;
		while (columns.next())
		{
			if (this.isTableInResultSet(columns, tableName) && this.isColumnInResultSet(columnName, columns))
			{
				String isNullable = columns.getString("IS_NULLABLE");
				if (isNullable.equalsIgnoreCase("YES"))
				{
					result = true;
				}
				break;
			}
		}
		return result;
	}

	@Override
	public void setDatabaseVersion(String version)
	{
		this.infoDAO.setInfo(NetworkCoreInfo.DATABASE_VERSION, version);
	}

	/**
	 * Checks the connection metadata for a table with specified name.
	 * 
	 * @param tableName
	 * @return
	 * @throws SQLException
	 */
	public boolean tableExists(Connection con, String tableName) throws SQLException
	{
		DatabaseMetaData dbm = con.getMetaData();
		// check if "employee" table is there
		ResultSet tables = dbm.getTables(null, null, tableName, null);
		boolean result = false;
		while (tables.next())
		{
			if (this.isTableInResultSet(tables, tableName))
			{
				result = true;
				break;
			}
		}
		return result;
	}

	private boolean isColumnInResultSet(String columnName, ResultSet columns) throws SQLException
	{
		return columns.getString("COLUMN_NAME").equalsIgnoreCase(columnName);
	}

	private boolean isDataLengthInResultSet(ResultSet columns, int length) throws SQLException
	{
		return columns.getInt("COLUMN_SIZE") == length;
	}

	private boolean isDataTypeInResultSet(ResultSet columns, int type) throws SQLException
	{
		return columns.getInt("DATA_TYPE") == type;
	}

	private boolean isTableInResultSet(ResultSet tables, String tableName) throws SQLException
	{
		return tables.getString("TABLE_NAME").equalsIgnoreCase(tableName);
	}
}
