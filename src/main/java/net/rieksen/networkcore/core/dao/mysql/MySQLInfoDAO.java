package net.rieksen.networkcore.core.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.Validate;

import net.rieksen.networkcore.core.dao.DAOException;
import net.rieksen.networkcore.core.dao.InfoDAO;
import net.rieksen.networkcore.core.info.NetworkCoreInfo;
import net.rieksen.networkcore.core.util.IMySQL;

public class MySQLInfoDAO extends MySQLBaseDAO implements InfoDAO
{

	public MySQLInfoDAO(IMySQL mysql)
	{
		super(mysql);
	}

	@Override
	public Map<NetworkCoreInfo, String> findAllInfo() throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_info");
			ResultSet rs = ps.executeQuery();
			Map<NetworkCoreInfo, String> list = new HashMap<>();
			while (rs.next())
			{
				list.put(NetworkCoreInfo.valueOf(rs.getString("info")), rs.getString("value"));
			}
			return list;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find all info", e);
		}
	}

	@Override
	public String findInfo(NetworkCoreInfo info) throws DAOException
	{
		Validate.notNull(info, "Key cannot be null");
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_info WHERE info = ?");
			ps.setString(1, info.name());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) { return rs.getString("value"); }
			return null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find info", e);
		}
	}

	@Override
	public void setInfo(NetworkCoreInfo key, String value) throws DAOException
	{
		Validate.notNull(key, "Key cannot be null");
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("INSERT INTO ncore_info VALUES (?, ?) ON DUPLICATE KEY UPDATE value = ?");
			ps.setString(1, key.name());
			ps.setString(2, value);
			ps.setString(3, value);
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to set info", e);
		}
	}
}
