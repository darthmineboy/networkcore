package net.rieksen.networkcore.core.dao;

import java.util.Date;
import java.util.List;

import net.rieksen.networkcore.core.statistic.NetworkMonthlyUsersStatisticPojo;
import net.rieksen.networkcore.core.statistic.NetworkStatisticPojo;

public interface StatisticDAO
{

	void createNetworkStatistic(NetworkStatisticPojo pojo);

	List<NetworkMonthlyUsersStatisticPojo> findNetworkMonthlyUsersStatistic();

	/**
	 * Network statistics starting with the oldest statistic.
	 * 
	 * @param since
	 * @return
	 */
	List<NetworkStatisticPojo> findNetworkStatisticsSince(Date since);
}
