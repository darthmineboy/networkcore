package net.rieksen.networkcore.core.dao;

import java.util.Map;

import net.rieksen.networkcore.core.info.NetworkCoreInfo;

public interface InfoDAO
{

	public Map<NetworkCoreInfo, String> findAllInfo() throws DAOException;

	public String findInfo(NetworkCoreInfo key) throws DAOException;

	public void setInfo(NetworkCoreInfo key, String value) throws DAOException;
}
