package net.rieksen.networkcore.core.dao.mysql.update;

import java.sql.Connection;
import java.sql.SQLException;

import net.rieksen.networkcore.core.mysql.MySQLUtil;
import net.rieksen.networkcore.core.update.MySQLDAOUpdate;
import net.rieksen.networkcore.core.util.IMySQL;

class MySQLDAOUpdate_v2_3_5 extends MySQLDAOUpdate
{

	public MySQLDAOUpdate_v2_3_5(IMySQL mysql)
	{
		super(mysql, "2.2.1", "2.3.5", "Adds `ncore_task` table, Adds column `socket_auto` to table `ncore_server`");
	}

	@Override
	public void doTransaction(Connection connection) throws SQLException
	{
		connection.createStatement().executeUpdate(
			"CREATE TABLE IF NOT EXISTS ncore_task ( task_id INT NOT NULL AUTO_INCREMENT, plugin_id INT NOT NULL, task_name VARCHAR(255) NOT NULL, task_priority VARCHAR(15) NOT NULL, creation_date DATETIME NOT NULL, description VARCHAR(1024) NOT NULL, task_generator VARCHAR(255), resolved BOOLEAN NOT NULL, task_data VARCHAR(4096), PRIMARY KEY (task_id), CONSTRAINT fk_ncore_task_plugin FOREIGN KEY (plugin_id) REFERENCES ncore_plugin(plugin_id) ON DELETE CASCADE ) ENGINE=InnoDb DEFAULT CHARSET=utf8");
		if (!MySQLUtil.columnExists(connection, "ncore_server", "socket_auto"))
		{
			connection.createStatement().executeUpdate("ALTER TABLE ncore_server ADD COLUMN socket_auto BOOLEAN NOT NULL AFTER is_online");
			connection.createStatement().executeUpdate("UPDATE ncore_server SET socket_auto = 1");
		}
	}
}
