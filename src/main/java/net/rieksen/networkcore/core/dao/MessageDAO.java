package net.rieksen.networkcore.core.dao;

import java.util.List;

import net.rieksen.networkcore.core.message.pojo.GlobalMessageVariablePojo;
import net.rieksen.networkcore.core.message.pojo.LanguagePojo;
import net.rieksen.networkcore.core.message.pojo.LocaleLanguagePojo;
import net.rieksen.networkcore.core.message.pojo.MessagePojo;
import net.rieksen.networkcore.core.message.pojo.MessageSectionPojo;
import net.rieksen.networkcore.core.message.pojo.MessageTranslationPojo;
import net.rieksen.networkcore.core.message.pojo.MessageVariablePojo;

public interface MessageDAO
{

	public int createGlobalVariable(GlobalMessageVariablePojo pojo) throws DAOException;

	public int createLanguage(LanguagePojo language) throws DAOException;

	public void createLocaleLanguage(LocaleLanguagePojo localeLanguage) throws DAOException;

	public int createMessage(MessagePojo message) throws DAOException;

	public int createSection(MessageSectionPojo section) throws DAOException;

	public void createTranslation(MessageTranslationPojo translation) throws DAOException;

	public void createVariable(MessageVariablePojo variable);

	public void deleteGlobalVariable(int variableId);

	public void deleteLanguage(int languageId) throws DAOException;

	public void deleteLocaleLanguage(String localeCode) throws DAOException;

	public void deleteMessage(int messageId) throws DAOException;

	public void deleteSection(int sectionId) throws DAOException;

	public void deleteTranslation(int messageId, int languageId) throws DAOException;

	public void deleteVariable(int messageId, String name) throws DAOException;

	public GlobalMessageVariablePojo findGlobalVariable(int variableId);

	public GlobalMessageVariablePojo findGlobalVariable(String name) throws DAOException;

	public List<GlobalMessageVariablePojo> findGlobalVariables() throws DAOException;

	public LanguagePojo findLanguage(int languageId) throws DAOException;

	public LanguagePojo findLanguage(String language) throws DAOException;

	public List<LanguagePojo> findLanguages() throws DAOException;

	public LocaleLanguagePojo findLocale(String locale);

	public List<LocaleLanguagePojo> findLocaleLanguages() throws DAOException;

	public MessagePojo findMessage(int messageId) throws DAOException;

	public MessagePojo findMessage(int sectionId, String name) throws DAOException;

	public int findMessageCount() throws DAOException;

	public List<MessagePojo> findMessages(int sectionId) throws DAOException;

	public List<LanguagePojo> findMissingTranslations(int messageId);

	public MessageSectionPojo findSection(int sectionId) throws DAOException;

	public MessageSectionPojo findSection(int pluginId, String name) throws DAOException;

	public int findSectionCount(int pluginId) throws DAOException;

	public List<MessageSectionPojo> findSections(int pluginId) throws DAOException;

	public MessageTranslationPojo findTranslation(int messageId, int languageId) throws DAOException;

	public int findTranslationCount(int languageId);

	public List<MessageTranslationPojo> findTranslations(int messageId) throws DAOException;

	public List<MessageVariablePojo> findVariables(int messageId);

	public void updateGlobalVariable(GlobalMessageVariablePojo variable);

	public void updateLocaleLanguage(LocaleLanguagePojo localeLanguage) throws DAOException;

	public void updateMessage(MessagePojo message) throws DAOException;

	public void updateSection(MessageSectionPojo section) throws DAOException;

	public void updateTranslation(MessageTranslationPojo translation) throws DAOException;

	public void updateVariable(MessageVariablePojo variable) throws DAOException;
}
