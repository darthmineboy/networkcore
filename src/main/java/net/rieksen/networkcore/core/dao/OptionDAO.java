package net.rieksen.networkcore.core.dao;

import java.util.List;

import net.rieksen.networkcore.core.option.pojo.OptionPojo;
import net.rieksen.networkcore.core.option.pojo.OptionSectionPojo;
import net.rieksen.networkcore.core.option.pojo.OptionValuePojo;

public interface OptionDAO
{

	public int createOption(OptionPojo option) throws DAOException;

	public int createSection(OptionSectionPojo section) throws DAOException;

	public int createValue(OptionValuePojo value) throws DAOException;

	public void deleteOption(int optionId);

	public void deleteSection(int sectionId) throws DAOException;

	public void deleteValue(int valueId) throws DAOException;

	public OptionPojo findOption(int optionId) throws DAOException;

	public OptionPojo findOption(int sectionId, String name) throws DAOException;

	public List<OptionPojo> findOptions(int sectionId) throws DAOException;

	public OptionSectionPojo findSection(int sectionId) throws DAOException;

	public OptionSectionPojo findSection(int pluginId, String name) throws DAOException;

	/**
	 * The amount of sections that a plugin has.
	 * 
	 * @param pluginId
	 * @return
	 * @throws DAOException
	 */
	public int findSectionCount(int pluginId) throws DAOException;

	public List<OptionSectionPojo> findSections() throws DAOException;

	public List<OptionSectionPojo> findSections(int pluginId) throws DAOException;

	public List<OptionValuePojo> findValues(int optionId) throws DAOException;

	public void updateOption(OptionPojo pojo);

	public void updateSection(OptionSectionPojo section) throws DAOException;

	public void updateValue(OptionValuePojo value) throws DAOException;

	List<OptionValuePojo> findOptionValuesWithDuplicateIndexes();
}
