package net.rieksen.networkcore.core.dao.mysql;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.lang.Validate;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import net.rieksen.networkcore.core.dao.DAOException;
import net.rieksen.networkcore.core.dao.exception.DAOIntegrityException;
import net.rieksen.networkcore.core.util.IMySQL;

public abstract class MySQLBaseDAO
{

	private IMySQL mysql;

	public MySQLBaseDAO(IMySQL mysql)
	{
		Validate.notNull(mysql);
		this.mysql = mysql;
	}

	public DAOException error(String message, SQLException e)
	{
		if (e instanceof MySQLIntegrityConstraintViolationException) { return new DAOIntegrityException(message, e); }
		return new DAOException(message, e);
	}

	public Connection getConnection() throws SQLException
	{
		return this.mysql.getConnection();
	}
}
