package net.rieksen.networkcore.core.dao.mysql;

import net.rieksen.networkcore.core.dao.DAOException;
import net.rieksen.networkcore.core.dao.UserDAO;
import net.rieksen.networkcore.core.user.CooldownAttachment;
import net.rieksen.networkcore.core.user.UserActivityType;
import net.rieksen.networkcore.core.user.UserType;
import net.rieksen.networkcore.core.user.note.UserNotePojo;
import net.rieksen.networkcore.core.user.pojo.*;
import net.rieksen.networkcore.core.util.IMySQL;
import net.rieksen.networkcore.core.util.SQLUtil;
import net.rieksen.networkcore.core.util.SimplePage;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class MySQLUserDAO extends MySQLBaseDAO implements UserDAO {

    public final static String USER_TABLE = "ncore_user";

    public MySQLUserDAO(IMySQL mysql) throws DAOException {
        super(mysql);
    }

    @Override
    public long calculateOnlineTime(int userId) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT SUM(TIMESTAMPDIFF(SECOND, join_date, CASE WHEN quit_date IS NULL THEN ? ELSE quit_date END)) AS total FROM ncore_user_connect WHERE user_id=?");
            ps.setLong(1, System.currentTimeMillis());
            ps.setInt(2, userId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getLong("total");
            }
            throw new DAOException("Expected result");
        } catch (SQLException e) {
            throw new DAOException("Failed to calculate online time", e);
        }
    }

    @Override
    public long createChat(UserChatPojo chat) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(
                    "INSERT INTO ncore_user_chat (connect_id, date, message, cancelled) VALUES (?, ?, ?, ?)", new String[]{"chat_id"});
            ps.setInt(1, chat.getConnectId());
            ps.setTimestamp(2, SQLUtil.dateToTimestamp(chat.getDate()));
            ps.setString(3, chat.getMessage());
            ps.setBoolean(4, chat.isCancelled());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                return rs.getLong(1);
            }
            throw new DAOException("Expected generated column");
        } catch (SQLException e) {
            throw new DAOException("Failed to create user chat", e);
        }
    }

    @Override
    public int createConnect(UserConnectPojo connect) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(
                    "INSERT INTO ncore_user_connect (user_id, runtime_id, name, ip, join_date, quit_date) VALUES (?, ?, ?, ?, ?, ?)",
                    new String[]{"connect_id"});
            ps.setInt(1, connect.getUserId());
            ps.setInt(2, connect.getRuntimeId());
            ps.setString(3, connect.getName());
            ps.setString(4, connect.getIp());
            ps.setTimestamp(5, new Timestamp(connect.getJoinDate().getTime()));
            ps.setTimestamp(6, SQLUtil.dateToTimestamp(connect.getQuitDate()));
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                return rs.getInt(1);
            }
            throw new DAOException("Expected generated column");
        } catch (SQLException e) {
            throw new DAOException("Failed to create user connect", e);
        }
    }

    @Override
    public int createNote(UserNotePojo note) {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(
                    "INSERT INTO ncore_user_note (user_id, created_by_id, creation_date, message) VALUES (?, ?, ?, ?)", new String[]{"id"});
            ps.setInt(1, note.getUserId());
            ps.setInt(2, note.getCreatedById());
            ps.setTimestamp(3, SQLUtil.dateToTimestamp(note.getCreationDate()));
            ps.setString(4, note.getMessage());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (!rs.next()) {
                throw new IllegalStateException("Expected generated id");
            }
            return rs.getInt(1);
        } catch (SQLException e) {
            throw new DAOException("Failed to create note", e);
        }
    }

    @Override
    public int createUser(UserPojo user) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(
                    "INSERT INTO ncore_user (type, uuid, name, connect_id, check_name_history, auto_language) VALUES (?, ?, ?, ?, ?, ?)",
                    new String[]{"user_id"});
            ps.setInt(1, user.getUserType().getValue());
            ps.setString(2, user.getUUID().toString());
            ps.setString(3, user.getUserName());
            ps.setObject(4, user.getConnectId());
            ps.setBoolean(5, user.checkNameHistory());
            ps.setBoolean(6, user.isAutoLanguage());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                return rs.getInt(1);
            }
            throw new DAOException("Expected generated column");
        } catch (SQLException e) {
            throw new DAOException("Failed to create user", e);
        }
    }

    @Override
    public void createUserLanguage(UserLanguagePojo pojo) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps =
                    connection.prepareStatement("INSERT INTO ncore_user_language (user_id, language_id, priority) VALUES (?, ?, ?)");
            ps.setInt(1, pojo.getUserId());
            ps.setInt(2, pojo.getLanguageId());
            ps.setInt(3, pojo.getPriority());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Failed to create user language", e);
        }
    }

    @Override
    public void createUserNameHistory(UserNameHistoryPojo history) {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps =
                    connection.prepareStatement("INSERT INTO ncore_user_name_history (user_id, changed_at, name) VALUES (?, ?, ?)");
            ps.setInt(1, history.getUserId());
            ps.setTimestamp(2, SQLUtil.dateToTimestamp(history.getChangedAt()));
            ps.setString(3, history.getName());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Failed to create user name history", e);
        }
    }

    @Override
    public int deleteChatsBefore(Date dateBefore) {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("DELETE FROM ncore_user_chat WHERE `date` < ?");
            ps.setTimestamp(1, SQLUtil.dateToTimestamp(dateBefore));
            return ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Failed to delete chats before", e);
        }
    }

    @Override
    public void deleteExpiredCooldowns() throws DAOException {
        try (Connection connection = this.getConnection()) {
            connection.createStatement().executeUpdate("DELETE FROM ncore_user_cooldown WHERE expiration < NOW()");
        } catch (SQLException e) {
            throw new DAOException("Failed to delete expired cooldowns", e);
        }
    }

    @Override
    public void deleteNote(int id) {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("DELETE FROM ncore_user_note WHERE id=?");
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Failed to delete note", e);
        }
    }

    @Override
    public void deleteUser(int userId) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("DELETE FROM ncore_user WHERE user_id=?");
            ps.setInt(1, userId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Failed to delete user", e);
        }
    }

    @Override
    public void deleteUserLanguage(int userId, int languageId) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("DELETE FROM ncore_user_language WHERE user_id=? AND language_id=?");
            ps.setInt(1, userId);
            ps.setInt(2, languageId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Failed to delete user language", e);
        }
    }

    @Override
    public void deleteUserLanguages(int userId) {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("DELETE FROM ncore_user_language WHERE user_id=?");
            ps.setInt(1, userId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Failed to delete user languages", e);
        }
    }

    @Override
    public SimplePage<UserChatPojo> findAllLatestChats(Integer userId, String search, int startIndex, int maxResults) {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT SQL_CALC_FOUND_ROWS * FROM ncore_user_chat c WHERE (? IS NULL OR EXISTS (SELECT 1 FROM ncore_user_connect uc WHERE uc.connect_id = c.connect_id AND user_id=?)) AND (? IS NULL OR message LIKE ?) ORDER BY `date` DESC LIMIT ?,?");
            ps.setObject(1, userId);
            ps.setObject(2, userId);
            ps.setString(3, search);
            ps.setString(4, "%" + search + "%");
            ps.setInt(5, startIndex);
            ps.setInt(6, maxResults);
            ResultSet rs = ps.executeQuery();
            List<UserChatPojo> data = this.extractChats(rs);
            ResultSet totalRs = ps.executeQuery("SELECT FOUND_ROWS()");
            int total = totalRs.next() ? totalRs.getInt(1) : -1;
            return new SimplePage<>(startIndex * maxResults, maxResults, total, data);
        } catch (SQLException e) {
            throw new DAOException("Failed to find all latest chats", e);
        }
    }

    @Override
    public UserChatPojo findChat(long chatId) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_user_chat WHERE chat_id = ?");
            ps.setLong(1, chatId);
            ResultSet rs = ps.executeQuery();
            return rs.next() ? this.extractChat(rs) : null;
        } catch (SQLException e) {
            throw new DAOException("Failed to find user chat", e);
        }
    }

    @Override
    public List<UserChatPojo> findChats(int userId) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("SELECT a.* "
                    + "FROM ncore_user_chat a INNER JOIN ncore_user_connect b ON a.connect_id = b.connect_id WHERE b.user_id = ?");
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            return this.extractChats(rs);
        } catch (SQLException e) {
            throw new DAOException("Failed to find user chats", e);
        }
    }

    @Override
    public List<UserChatPojo> findChats(int userId, int startIndex, int maxResults) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("SELECT a.* "
                    + "FROM ncore_user_chat a INNER JOIN ncore_user_connect b ON a.connect_id = b.connect_id WHERE b.user_id = ? LIMIT ?,?");
            ps.setInt(1, userId);
            ps.setInt(2, startIndex);
            ps.setInt(3, maxResults);
            ResultSet rs = ps.executeQuery();
            return this.extractChats(rs);
        } catch (SQLException e) {
            throw new DAOException("Failed to find user chats", e);
        }
    }

    @Override
    public int findChatsCount(int userId, String search) {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("SELECT COUNT(*) "
                    + "FROM ncore_user_chat a INNER JOIN ncore_user_connect b ON a.connect_id = b.connect_id WHERE b.user_id = ? AND (? IS NULL OR a.message LIKE ?)");
            ps.setInt(1, userId);
            search = search == null ? null : "%" + search + "%";
            ps.setString(2, search);
            ps.setString(3, search);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
            throw new DAOException("Expected result");
        } catch (SQLException e) {
            throw new DAOException("Failed to find user chats count", e);
        }
    }

    @Override
    public UserConnectPojo findConnect(int connectId) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_user_connect WHERE connect_id = ?");
            ps.setInt(1, connectId);
            ResultSet rs = ps.executeQuery();
            return rs.next() ? this.extractConnect(rs) : null;
        } catch (SQLException e) {
            throw new DAOException("Failed to find user connect", e);
        }
    }

    @Override
    public List<UserConnectPojo> findConnects(int userId) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_user_connect WHERE user_id = ?");
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            return this.extractConnects(rs);
        } catch (SQLException e) {
            throw new DAOException("Failed to find user connects", e);
        }
    }

    @Override
    public List<UserConnectPojo> findConnects(int userId, int startIndex, int count) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_user_connect WHERE user_id = ? LIMIT ?, ?");
            ps.setInt(1, userId);
            ps.setInt(2, startIndex);
            ps.setInt(3, count);
            ResultSet rs = ps.executeQuery();
            return this.extractConnects(rs);
        } catch (SQLException e) {
            throw new DAOException("Failed to find user connects", e);
        }
    }

    @Override
    public int findConnectsOfUserCount(int userId) {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("SELECT COUNT(*) FROM ncore_user_connect WHERE user_id=?");
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
            throw new DAOException("Expected result");
        } catch (SQLException e) {
            throw new DAOException("Failed to find connects of user count", e);
        }
    }

    @Override
    public UserConnectPojo findFirstConnect(int userId) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps =
                    connection.prepareStatement("SELECT * FROM ncore_user_connect WHERE user_id = ? ORDER BY (connect_id) ASC LIMIT 0, 1");
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            return rs.next() ? this.extractConnect(rs) : null;
        } catch (SQLException e) {
            throw new DAOException("Failed to find user connect", e);
        }
    }

    @Override
    public IpPojo findIp(String ip) {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT ip, MIN(join_date) AS first_seen, MAX(quit_date) AS last_seen, COUNT(*) AS times_joined FROM ncore_user_connect WHERE ip = ? GROUP BY (ip)");
            ps.setString(1, ip);
            ResultSet rs = ps.executeQuery();
            return rs.next() ? this.extractIp(rs) : null;
        } catch (SQLException e) {
            throw new DAOException("Failed to find ip", e);
        }
    }

    @Override
    public List<UserChatPojo> findLatestChats(int userId, int startIndex, int maxResults) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("SELECT a.* "
                    + "FROM ncore_user_chat a INNER JOIN ncore_user_connect b ON a.connect_id = b.connect_id WHERE b.user_id = ? ORDER BY (a.chat_id) DESC LIMIT ?,?");
            ps.setInt(1, userId);
            ps.setInt(2, startIndex);
            ps.setInt(3, maxResults);
            ResultSet rs = ps.executeQuery();
            return this.extractChats(rs);
        } catch (SQLException e) {
            throw new DAOException("Failed to find user chats", e);
        }
    }

    @Override
    public List<UserChatPojo> findLatestChats(int userId, String search, int startIndex, Integer maxResults) {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("SELECT a.* "
                    + "FROM ncore_user_chat a INNER JOIN ncore_user_connect b ON a.connect_id = b.connect_id WHERE b.user_id = ? AND (? IS NULL OR a.message LIKE ?) ORDER BY (a.chat_id) DESC LIMIT ?,?");
            ps.setInt(1, userId);
            search = search == null ? null : "%" + search + "%";
            ps.setString(2, search);
            ps.setString(3, search);
            ps.setInt(4, startIndex);
            ps.setInt(5, maxResults);
            ResultSet rs = ps.executeQuery();
            return this.extractChats(rs);
        } catch (SQLException e) {
            throw new DAOException("Failed to find user chats", e);
        }
    }

    @Override
    public UserConnectPojo findLatestConnect(int userId) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps =
                    connection.prepareStatement("SELECT * FROM ncore_user_connect WHERE user_id = ? ORDER BY (connect_id) DESC LIMIT 0, 1");
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            return rs.next() ? this.extractConnect(rs) : null;
        } catch (SQLException e) {
            throw new DAOException("Failed to find user connect", e);
        }
    }

    @Override
    public List<UserConnectPojo> findLatestConnects(int userId, int startIndex, int count) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps =
                    connection.prepareStatement("SELECT * FROM ncore_user_connect WHERE user_id = ? ORDER BY (connect_id) DESC LIMIT ?, ?");
            ps.setInt(1, userId);
            ps.setInt(2, startIndex);
            ps.setInt(3, count);
            ResultSet rs = ps.executeQuery();
            return this.extractConnects(rs);
        } catch (SQLException e) {
            throw new DAOException("Failed to find user connects", e);
        }
    }

    @Override
    public List<UserPojo> findNewUsers(Date joinedAfter) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT b.* FROM ncore_user_connect a INNER JOIN ncore_user b ON a.user_id = b.user_id GROUP BY (user_id) HAVING MIN(join_date) > ?");
            ps.setTimestamp(1, SQLUtil.dateToTimestamp(joinedAfter));
            ResultSet rs = ps.executeQuery();
            return this.extractUsers(rs);
        } catch (SQLException e) {
            throw new DAOException("Failed to find new users", e);
        }
    }

    @Override
    public UserNotePojo findNote(int noteId) {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_user_note WHERE id=?");
            ps.setInt(1, noteId);
            ResultSet rs = ps.executeQuery();
            return rs.next() ? this.extractNote(rs) : null;
        } catch (SQLException e) {
            throw new DAOException("Failed to find note", e);
        }
    }

    @Override
    public SimplePage<UserNotePojo> findNotes(int userId, int startIndex, int maxResults) {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps =
                    connection.prepareStatement("SELECT SQL_CALC_FOUND_ROWS * FROM ncore_user_note WHERE user_id=? LIMIT ?,?");
            ps.setInt(1, userId);
            ps.setInt(2, startIndex);
            ps.setInt(3, maxResults);
            ResultSet rs = ps.executeQuery();
            List<UserNotePojo> data = this.extractNotes(rs);
            ResultSet totalRs = ps.executeQuery("SELECT FOUND_ROWS()");
            int total = totalRs.next() ? totalRs.getInt(1) : -1;
            return new SimplePage<>(startIndex * maxResults, maxResults, total, data);
        } catch (SQLException e) {
            throw new DAOException("Failed to find notes", e);
        }
    }

    @Override
    public int findOnlineUserCount(Date date) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT COUNT(*) FROM ncore_user_connect WHERE join_date<? AND quit_date>? OR join_date<? AND quit_date IS NULL");
            ps.setTimestamp(1, SQLUtil.dateToTimestamp(date));
            ps.setTimestamp(2, SQLUtil.dateToTimestamp(date));
            ps.setTimestamp(3, SQLUtil.dateToTimestamp(date));
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
            throw new DAOException("Expected result");
        } catch (SQLException e) {
            throw new DAOException("Failed to find online user count", e);
        }
    }

    @Override
    public List<UserPojo> findOnlineUsersOfServer(int serverId) {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT u.* FROM ncore_user_connect uc INNER JOIN ncore_server_runtime sr ON uc.runtime_id=sr.runtime_id INNER JOIN ncore_user u ON u.user_id=uc.user_id WHERE sr.server_id=? AND sr.stop_date IS NULL AND uc.quit_date IS NULL");
            ps.setInt(1, serverId);
            ResultSet rs = ps.executeQuery();
            return this.extractUsers(rs);
        } catch (SQLException e) {
            throw new DAOException("Failed to find online users of server", e);
        }
    }

    @Override
    public List<UserConnectPojo> findOpenConnects(int serverId) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT a.* FROM ncore_user_connect a INNER JOIN ncore_server_runtime b ON a.runtime_id = b.runtime_id WHERE b.server_id = ? AND a.quit_date IS NULL");
            ps.setInt(1, serverId);
            ResultSet rs = ps.executeQuery();
            return this.extractConnects(rs);
        } catch (SQLException e) {
            throw new DAOException("Failed to find open connects", e);
        }
    }

    @Override
    public UserPojo findUserById(int userId) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_user WHERE user_id=?");
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            return rs.next() ? this.extractUser(rs) : null;
        } catch (SQLException e) {
            throw new DAOException("Failed to find user by id", e);
        }
    }

    @Override
    public UserPojo findUserByTypeAndName(UserType type, String name) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_user WHERE type=? AND name=?");
            ps.setInt(1, type.getValue());
            ps.setString(2, name);
            ResultSet rs = ps.executeQuery();
            return rs.next() ? this.extractUser(rs) : null;
        } catch (SQLException e) {
            throw new DAOException("Failed to find user by type and name", e);
        }
    }

    @Override
    public UserPojo findUserByUUID(UUID uuid) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_user WHERE uuid=?");
            ps.setString(1, uuid.toString());
            ResultSet rs = ps.executeQuery();
            return rs.next() ? this.extractUser(rs) : null;
        } catch (SQLException e) {
            throw new DAOException("Failed to find user by uuid", e);
        }
    }

    @Override
    public int findUserCount() throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("SELECT COUNT(*) FROM ncore_user");
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
            throw new DAOException("Expected result");
        } catch (SQLException e) {
            throw new DAOException("Failed to find user count", e);
        }
    }

    @Override
    public SimplePage<UserIpPojo> findUserIps(int userId, int startIndex, int maxResults) {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT SQL_CALC_FOUND_ROWS u.*, ip, MIN(join_date) AS first_seen, MAX(quit_date) AS last_seen, COUNT(*) AS times_joined FROM ncore_user_connect uc INNER JOIN ncore_user u ON u.user_id = uc.user_id WHERE uc.user_id = ? GROUP BY (uc.ip) ORDER BY last_seen DESC LIMIT ?,?");
            ps.setInt(1, userId);
            ps.setInt(2, startIndex);
            ps.setInt(3, maxResults);
            ResultSet rs = ps.executeQuery();
            List<UserIpPojo> data = this.extractUserIPs(rs);
            ResultSet totalRs = ps.executeQuery("SELECT FOUND_ROWS()");
            int total = totalRs.next() ? totalRs.getInt(1) : -1;
            return new SimplePage<>(startIndex * maxResults, maxResults, total, data);
        } catch (SQLException e) {
            throw new DAOException("Failed to find users by IP", e);
        }
    }

    @Override
    public UserLanguagePojo findUserLanguage(int userId, int languageId) {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_user_language WHERE user_id=? AND language_id=?");
            ps.setInt(1, userId);
            ps.setInt(2, languageId);
            ResultSet rs = ps.executeQuery();
            return rs.next() ? this.extractUserLanguage(rs) : null;
        } catch (SQLException e) {
            throw new DAOException("Failed to find user language", e);
        }
    }

    @Override
    public List<UserLanguagePojo> findUserLanguages(int userId) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_user_language WHERE user_id=? ORDER BY (priority)");
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            return this.extractUserLanguages(rs);
        } catch (SQLException e) {
            throw new DAOException("Failed to find user language preferences", e);
        }
    }

    @Override
    public List<UserNameHistoryPojo> findUserNameHistory(int userId) {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps =
                    connection.prepareStatement("SELECT * FROM ncore_user_name_history WHERE user_id=? ORDER BY (changed_at)");
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            return this.extractNameHistories(rs);
        } catch (SQLException e) {
            throw new DAOException("Failed to find user name history", e);
        }
    }

    @Override
    public List<UserPojo> findUsers() throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_user");
            ResultSet rs = ps.executeQuery();
            return this.extractUsers(rs);
        } catch (SQLException e) {
            throw new DAOException("Failed to find users", e);
        }
    }

    @Override
    public List<UserPojo> findUsers(int startIndex, int count) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_user LIMIT ?,?");
            ps.setInt(1, startIndex);
            ps.setInt(2, count);
            ResultSet rs = ps.executeQuery();
            return this.extractUsers(rs);
        } catch (SQLException e) {
            throw new DAOException("Failed to find users", e);
        }
    }

    @Override
    public List<UserActivityPojo> findUsersActivity(int startIndex, int maxResults) {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(
                    "(SELECT 'CHAT' AS type, chat_id, connect_id, (SELECT user_id FROM ncore_user_connect WHERE connect_id = c.connect_id) AS user_id, (SELECT runtime_id FROM ncore_user_connect WHERE connect_id = c.connect_id) AS runtime_id, (SELECT name FROM ncore_user_connect WHERE connect_id = c.connect_id) AS name, (SELECT ip FROM ncore_user_connect WHERE connect_id = c.connect_id) AS ip, `date` AS activity_date, message FROM ncore_user_chat c)" //
                            + "UNION"//
                            + "(SELECT 'CONNECT', null, connect_id, user_id, runtime_id, name, ip, join_date, null FROM ncore_user_connect)"//
                            + "UNION"//
                            + "(SELECT 'DISCONNECT', null, connect_id, user_id, runtime_id, name, ip, quit_date, null FROM ncore_user_connect WHERE quit_date IS NOT NULL)"//
                            + "ORDER BY activity_date DESC LIMIT ?, ?");
            ps.setInt(1, startIndex);
            ps.setInt(2, maxResults);
            ResultSet rs = ps.executeQuery();
            return this.extractUsersActivity(rs);
        } catch (SQLException e) {
            throw new DAOException("Failed to find users activity", e);
        }
    }

    @Override
    public SimplePage<UserIpPojo> findUsersByIp(String ip, int startIndex, int maxResults) {
        try (Connection connection = this.getConnection()) {
            // TODO reduce two queries to one
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT u.*, ip, MIN(join_date) AS first_seen, MAX(quit_date) AS last_seen, COUNT(*) AS times_joined FROM ncore_user_connect uc INNER JOIN ncore_user u ON u.user_id = uc.user_id WHERE ip = ? GROUP BY (uc.user_id) ORDER BY last_seen DESC LIMIT ?,?");
            ps.setString(1, ip);
            ps.setInt(2, startIndex);
            ps.setInt(3, maxResults);
            ResultSet rs = ps.executeQuery();
            List<UserIpPojo> data = this.extractUserIPs(rs);
            int totalResults = this.findUsersByIPCount(ip);
            return new SimplePage<>(startIndex * maxResults, maxResults, totalResults, data);
        } catch (SQLException e) {
            throw new DAOException("Failed to find users by IP", e);
        }
    }

    public int findUsersByIPCount(String ip) {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection
                    .prepareStatement("SELECT COUNT(*) FROM ncore_user WHERE user_id IN (SELECT user_id FROM ncore_user_connect WHERE ip = ?)");
            ps.setString(1, ip);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
            throw new DAOException("Expected result");
        } catch (SQLException e) {
            throw new DAOException("Failed to find users by IP count", e);
        }
    }

    @Override
    public List<UserPojo> findUsersByName(String name, int startIndex, int count) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_user WHERE name LIKE ? LIMIT ?,?");
            ps.setString(1, name + "%");
            ps.setInt(2, startIndex);
            ps.setInt(3, count);
            ResultSet rs = ps.executeQuery();
            return this.extractUsers(rs);
        } catch (SQLException e) {
            throw new DAOException("Failed to find users", e);
        }
    }

    @Override
    public int findUsersByNameCount(String filterName) {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("SELECT COUNT(*) FROM ncore_user WHERE name LIKE ?");
            ps.setString(1, filterName + "%");
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
            throw new DAOException("Expected result");
        } catch (SQLException e) {
            throw new DAOException("Failed to find users", e);
        }
    }

    @Override
    public List<UserPojo> findUsersWithCheckNameHistory(int limit) {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_user WHERE check_name_history=1 LIMIT 0,?");
            ps.setInt(1, limit);
            ResultSet rs = ps.executeQuery();
            return this.extractUsers(rs);
        } catch (SQLException e) {
            throw new DAOException("Failed to find users", e);
        }
    }

    @Override
    public List<UserPojo> findUsersWithLatestIp(String ip) {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT u.* FROM ncore_user u INNER JOIN ncore_user_connect uc ON u.connect_id = uc.connect_id WHERE ip=?");
            ps.setString(1, ip);
            ResultSet rs = ps.executeQuery();
            return this.extractUsers(rs);
        } catch (SQLException e) {
            throw new DAOException("Failed to find users with latest ip", e);
        }
    }

    @Override
    public void loadCooldown(CooldownAttachment cooldown) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps =
                    connection.prepareStatement("SELECT plugin_id, unique_key, expiration FROM ncore_user_cooldown WHERE user_id=?");
            ps.setInt(1, cooldown.getUserId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                cooldown.setCooldown(rs.getInt("plugin_id"), rs.getString("unique_key"),
                        rs.getTimestamp("expiration").getTime() - System.currentTimeMillis(), false);
            }
        } catch (SQLException e) {
            throw new DAOException("Failed to load cooldown", e);
        }
    }

    @Override
    public void setCooldown(int userId, int pluginId, String key, long expiration) {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(
                    "INSERT INTO ncore_user_cooldown (user_id, plugin_id, unique_key, expiration) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE expiration=?");
            ps.setInt(1, userId);
            ps.setInt(2, pluginId);
            ps.setString(3, key);
            ps.setTimestamp(4, new Timestamp(expiration));
            ps.setTimestamp(5, new Timestamp(expiration));
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Failed to set cooldown", e);
        }
    }

    @Override
    public void updateConnect(UserConnectPojo connect) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(
                    "UPDATE ncore_user_connect SET user_id = ?, runtime_id = ?, name = ?, ip = ?, join_date = ?, quit_date = ? WHERE connect_id = ?");
            ps.setInt(1, connect.getUserId());
            ps.setInt(2, connect.getRuntimeId());
            ps.setString(3, connect.getName());
            ps.setString(4, connect.getIp());
            ps.setTimestamp(5, SQLUtil.dateToTimestamp(connect.getJoinDate()));
            ps.setTimestamp(6, SQLUtil.dateToTimestamp(connect.getQuitDate()));
            ps.setInt(7, connect.getConnectId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Failed to update user connect", e);
        }
    }

    @Override
    public void updateUser(UserPojo user) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(
                    "UPDATE ncore_user SET type=?, uuid=?, name=?, connect_id=?, check_name_history=?, auto_language=? WHERE user_id=?");
            ps.setInt(1, user.getUserType().getValue());
            ps.setString(2, user.getUUID().toString());
            ps.setString(3, user.getUserName());
            ps.setObject(4, user.getConnectId());
            ps.setBoolean(5, user.checkNameHistory());
            ps.setBoolean(6, user.isAutoLanguage());
            ps.setInt(7, user.getUserId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Failed to update user", e);
        }
    }

    @Override
    public void updateUserLanguage(UserLanguagePojo pojo) throws DAOException {
        try (Connection connection = this.getConnection()) {
            PreparedStatement ps =
                    connection.prepareStatement("UPDATE ncore_user_language SET priority=? WHERE user_id=? AND language_id=?");
            ps.setInt(1, pojo.getPriority());
            ps.setInt(2, pojo.getUserId());
            ps.setInt(3, pojo.getLanguageId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Failed to update user language", e);
        }
    }

    private UserChatPojo extractChat(ResultSet rs) throws SQLException {
        return new UserChatPojo(rs.getLong("chat_id"), rs.getInt("connect_id"), SQLUtil.timestampToDate(rs.getTimestamp("date")),
                rs.getString("message"), rs.getBoolean("cancelled"));
    }

    private List<UserChatPojo> extractChats(ResultSet rs) throws SQLException {
        List<UserChatPojo> list = new ArrayList<>();
        while (rs.next()) {
            list.add(this.extractChat(rs));
        }
        return list;
    }

    private UserConnectPojo extractConnect(ResultSet rs) throws SQLException {
        return new UserConnectPojo(rs.getInt("connect_id"), rs.getInt("user_id"), rs.getInt("runtime_id"), rs.getString("name"),
                rs.getString("ip"), SQLUtil.timestampToDate(rs.getTimestamp("join_date")),
                SQLUtil.timestampToDate(rs.getTimestamp("quit_date")));
    }

    private List<UserConnectPojo> extractConnects(ResultSet rs) throws SQLException {
        List<UserConnectPojo> list = new ArrayList<>();
        while (rs.next()) {
            list.add(this.extractConnect(rs));
        }
        return list;
    }

    private IpPojo extractIp(ResultSet rs) throws SQLException {
        return new IpPojo(rs.getString("ip"), SQLUtil.timestampToDate(rs.getTimestamp("first_seen")),
                SQLUtil.timestampToDate(rs.getTimestamp("last_seen")), rs.getInt("times_joined"));
    }

    private List<UserNameHistoryPojo> extractNameHistories(ResultSet rs) throws SQLException {
        List<UserNameHistoryPojo> list = new ArrayList<>();
        while (rs.next()) {
            list.add(this.extractNameHistory(rs));
        }
        return list;
    }

    private UserNameHistoryPojo extractNameHistory(ResultSet rs) throws SQLException {
        return new UserNameHistoryPojo(rs.getInt("user_id"), SQLUtil.timestampToDate(rs.getTimestamp("changed_at")), rs.getString("name"));
    }

    private UserNotePojo extractNote(ResultSet rs) throws SQLException {
        return new UserNotePojo(rs.getInt("id"), rs.getInt("user_id"), rs.getInt("created_by_id"),
                SQLUtil.timestampToDate(rs.getTimestamp("creation_date")), rs.getString("message"));
    }

    private List<UserNotePojo> extractNotes(ResultSet rs) throws SQLException {
        List<UserNotePojo> pojos = new ArrayList<>();
        while (rs.next()) {
            pojos.add(this.extractNote(rs));
        }
        return pojos;
    }

    private UserPojo extractUser(ResultSet rs) throws SQLException {
        return new UserPojo(rs.getInt("user_id"), UserType.fromValue(rs.getInt("type")), UUID.fromString(rs.getString("uuid")),
                rs.getString("name"), SQLUtil.getInteger(rs, "connect_id"), rs.getBoolean("check_name_history"),
                rs.getBoolean("auto_language"));
    }

    private UserActivityPojo extractUserActivity(ResultSet rs) throws SQLException {
        return new UserActivityPojo(UserActivityType.valueOf(rs.getString("type")), SQLUtil.getInteger(rs, "chat_id"),
                rs.getInt("connect_id"), rs.getInt("runtime_id"), rs.getInt("user_id"), rs.getString("name"), rs.getString("ip"),
                SQLUtil.timestampToDate(rs.getTimestamp("activity_date")), rs.getString("message"));
    }

    private UserIpPojo extractUserIP(ResultSet rs) throws SQLException {
        return new UserIpPojo(rs.getInt("user_id"), UserType.fromValue(rs.getInt("type")), UUID.fromString(rs.getString("uuid")),
                rs.getString("name"), rs.getString("ip"), rs.getTimestamp("first_seen"), rs.getTimestamp("last_seen"),
                rs.getInt("times_joined"));
    }

    private List<UserIpPojo> extractUserIPs(ResultSet rs) throws SQLException {
        List<UserIpPojo> pojos = new ArrayList<>();
        while (rs.next()) {
            pojos.add(this.extractUserIP(rs));
        }
        return pojos;
    }

    private UserLanguagePojo extractUserLanguage(ResultSet rs) throws SQLException {
        return new UserLanguagePojo(rs.getInt("user_id"), rs.getInt("language_id"), rs.getInt("priority"));
    }

    private List<UserLanguagePojo> extractUserLanguages(ResultSet rs) throws SQLException {
        List<UserLanguagePojo> list = new ArrayList<>();
        while (rs.next()) {
            list.add(this.extractUserLanguage(rs));
        }
        return list;
    }

    private List<UserPojo> extractUsers(ResultSet rs) throws SQLException {
        List<UserPojo> users = new ArrayList<>();
        while (rs.next()) {
            users.add(this.extractUser(rs));
        }
        return users;
    }

    private List<UserActivityPojo> extractUsersActivity(ResultSet rs) throws SQLException {
        List<UserActivityPojo> pojos = new ArrayList<>();
        while (rs.next()) {
            pojos.add(this.extractUserActivity(rs));
        }
        return pojos;
    }
}
