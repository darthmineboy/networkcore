package net.rieksen.networkcore.core.dao.mysql.update;

import java.sql.Connection;
import java.sql.SQLException;

import net.rieksen.networkcore.core.dao.DAOException;
import net.rieksen.networkcore.core.mysql.MySQLConstraintType;
import net.rieksen.networkcore.core.mysql.MySQLUtil;
import net.rieksen.networkcore.core.update.MySQLDAOUpdate;
import net.rieksen.networkcore.core.update.UpdateException;
import net.rieksen.networkcore.core.util.IMySQL;

class MySQLDAOUpdate_v2_0_1 extends MySQLDAOUpdate
{

	public MySQLDAOUpdate_v2_0_1(IMySQL mysql)
	{
		super(mysql, "2.0", "2.0.1", "Adds support for storing LocaleLanguage");
	}

	@Override
	public void doTransaction(Connection connection) throws SQLException
	{
		this.createTable(connection);
		this.dropConstraints(connection);
		this.createConstraints(connection);
	}

	@Override
	public void verifyUpdate() throws UpdateException
	{
		try (Connection connection = this.getConnection())
		{
			if (!MySQLUtil.tableExists(connection,
				"ncore_locale_language")) { throw new UpdateException("Table `ncore_locale_language` does not exist"); }
		} catch (SQLException e)
		{
			throw new DAOException("Unexpected exception", e);
		}
	}

	private void createConstraints(Connection connection) throws SQLException
	{
		MySQLUtil.addTableConstraint(connection, "ncore_locale_language", "fk_ncore_locale_language_language",
			"FOREIGN KEY (language_id) REFERENCES ncore_language(language_id) ON UPDATE CASCADE ON DELETE CASCADE");
	}

	private void createTable(Connection connection) throws SQLException
	{
		connection.createStatement()
			.executeUpdate("CREATE TABLE IF NOT EXISTS ncore_locale_language (" + "locale_code VARCHAR(15) NOT NULL," + "language_id INT,"
				+ "PRIMARY KEY (locale_code)" + ") ENGINE=InnoDb DEFAULT CHARSET=utf8");
	}

	private void dropConstraints(Connection connection)
	{
		try
		{
			MySQLUtil.dropConstraintsOnTable(connection, "ncore_locale_language", MySQLConstraintType.FOREIGN_KEY);
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
}
