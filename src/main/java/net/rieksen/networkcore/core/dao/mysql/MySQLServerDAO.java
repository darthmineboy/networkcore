package net.rieksen.networkcore.core.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import net.rieksen.networkcore.core.dao.DAOException;
import net.rieksen.networkcore.core.dao.ServerDAO;
import net.rieksen.networkcore.core.server.ServerType;
import net.rieksen.networkcore.core.server.pojo.ServerGroupPojo;
import net.rieksen.networkcore.core.server.pojo.ServerLogPojo;
import net.rieksen.networkcore.core.server.pojo.ServerPojo;
import net.rieksen.networkcore.core.server.pojo.ServerResourceUsagePojo;
import net.rieksen.networkcore.core.server.pojo.ServerRuntimePojo;
import net.rieksen.networkcore.core.server.pojo.ServerStatisticPojo;
import net.rieksen.networkcore.core.util.IMySQL;
import net.rieksen.networkcore.core.util.SQLUtil;

public class MySQLServerDAO extends MySQLBaseDAO implements ServerDAO
{

	public final static String SERVER_TABLE = "ncore_server";

	public MySQLServerDAO(IMySQL mysql) throws DAOException
	{
		super(mysql);
	}

	@Override
	public void createGroupServer(int groupId, int serverId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("INSERT INTO ncore_server_group_server (group_id, server_id) VALUES (?, ?)");
			ps.setInt(1, groupId);
			ps.setInt(2, serverId);
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to create group server", e);
		}
	}

	@Override
	public long createLog(ServerLogPojo log) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"INSERT INTO ncore_server_log (server_id, log_date, level, message, skipped) VALUES (?, ?, ?, ?, ?)",
				new String[] { "log_id" });
			ps.setInt(1, log.getServerId());
			ps.setTimestamp(2, SQLUtil.dateToTimestamp(log.getLogDate()));
			ps.setString(3, log.getLevel().getName());
			ps.setString(4, log.getMessage());
			ps.setInt(5, log.getSkipped());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) { return rs.getLong(1); }
			throw new DAOException("Expected generated column");
		} catch (SQLException e)
		{
			throw new DAOException("Failed to create server log", e);
		}
	}

	@Override
	public int createRuntime(ServerRuntimePojo runtime) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"INSERT INTO ncore_server_runtime (server_id, start_date, last_ping_date, stop_date) VALUES (?, ?, ?, ?)",
				new String[] { "runtime_id" });
			ps.setInt(1, runtime.getServerId());
			ps.setTimestamp(2, SQLUtil.dateToTimestamp(runtime.getStartDate()));
			ps.setTimestamp(3, SQLUtil.dateToTimestamp(runtime.getLastPingDate()));
			ps.setTimestamp(4, SQLUtil.dateToTimestamp(runtime.getStopDate()));
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) { return rs.getInt(1); }
			throw new DAOException("Expected result");
		} catch (SQLException e)
		{
			throw new DAOException("Failed to create server runtime", e);
		}
	}

	@Override
	public int createServer(ServerPojo server) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"INSERT INTO ncore_server (name, type, is_online, socket_auto, socket_bind_host, socket_host, socket_port, socket_key) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
				new String[] { "server_id" });
			ps.setString(1, server.getName());
			ps.setInt(2, server.getType().getValue());
			ps.setBoolean(3, server.isOnline());
			ps.setBoolean(4, server.isSocketAuto());
			ps.setString(5, server.getSocketBindHost());
			ps.setString(6, server.getSocketHost());
			ps.setObject(7, server.getSocketPort());
			ps.setString(8, server.getSocketKey());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) { return rs.getInt(1); }
			throw new DAOException("Expected generated column");
		} catch (SQLException e)
		{
			throw new DAOException("Failed to create server", e);
		}
	}

	@Override
	public int createServerGroup(ServerGroupPojo group) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"INSERT INTO ncore_server_group (group_name, group_display_name, group_description) VALUES (?, ?, ?)",
				new String[] { "group_id" });
			ps.setString(1, group.getName());
			ps.setString(2, group.getDisplayName());
			ps.setString(3, group.getDescription());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) { return rs.getInt(1); }
			throw new DAOException("Expected generated column");
		} catch (SQLException e)
		{
			throw new DAOException("Failed to create server group", e);
		}
	}

	@Override
	public void createServerResourceUsage(ServerResourceUsagePojo resource) throws DAOException
	{
		try (Connection con = this.getConnection())
		{
			PreparedStatement ps = con.prepareStatement(
				"INSERT INTO ncore_server_resource_usage (server_id, `current_date`, tps, node_cpu_usage, server_cpu_usage, memory_used, memory_available, memory_total) VALUES (?,?,?,?,?,?,?,?)");
			ps.setInt(1, resource.getServerId());
			ps.setTimestamp(2, SQLUtil.dateToTimestamp(resource.getCurrentDate()));
			ps.setFloat(3, resource.getTps());
			ps.setFloat(4, resource.getNodeCpuUsage());
			ps.setFloat(5, resource.getServerCpuUsage());
			ps.setInt(6, resource.getMemoryUsed());
			ps.setInt(7, resource.getMemoryAvailable());
			ps.setInt(8, resource.getMemoryTotal());
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to create server resource usage", e);
		}
	}

	@Override
	public void createServerStatistic(ServerStatisticPojo statistic) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"INSERT INTO ncore_server_statistic (server_id, `current_date`, online_players, tps, server_cpu_usage, memory_used, memory_available, memory_total) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
			ps.setInt(1, statistic.getServerId());
			ps.setTimestamp(2, SQLUtil.dateToTimestamp(statistic.getCurrentDate()));
			ps.setInt(3, (int) statistic.getOnlinePlayers());
			ps.setFloat(4, statistic.getTps());
			ps.setFloat(5, statistic.getServerCpuUsage());
			ps.setInt(6, (int) statistic.getMemoryUsed());
			ps.setInt(7, (int) statistic.getMemoryAvailable());
			ps.setInt(8, (int) statistic.getMemoryTotal());
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to create server statistic", e);
		}
	}

	@Override
	public void deleteGroupServer(int groupId, int serverId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("DELETE FROM ncore_server_group_server WHERE group_id=? AND server_id=?");
			ps.setInt(1, groupId);
			ps.setInt(2, serverId);
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to delete group server", e);
		}
	}

	@Override
	public int deleteLogsBefore(Date dateBefore)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("DELETE FROM ncore_server_log WHERE log_date < ?");
			ps.setTimestamp(1, SQLUtil.dateToTimestamp(dateBefore));
			return ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to delete logs before", e);
		}
	}

	@Override
	public void deleteServer(int serverId)
	{
		try (Connection con = this.getConnection())
		{
			PreparedStatement ps = con.prepareStatement("DELETE FROM ncore_server WHERE server_id = ?");
			ps.setInt(1, serverId);
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to delete server", e);
		}
	}

	@Override
	public void deleteServerGroup(int groupId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("DELETE FROM ncore_server_group WHERE group_id=?");
			ps.setInt(1, groupId);
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to delete server group", e);
		}
	}

	@Override
	public int deleteServerResourceUsageBefore(Date dateBefore)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("DELETE FROM ncore_server_resource_usage WHERE `current_date` < ?");
			ps.setTimestamp(1, SQLUtil.dateToTimestamp(dateBefore));
			return ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to delete server resource usage before date", e);
		}
	}

	@Override
	public List<ServerPojo> findGroupServers(int groupId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"SELECT s.* FROM ncore_server_group_server gs INNER JOIN ncore_server s ON s.server_id = gs.server_id WHERE group_id=?");
			ps.setInt(1, groupId);
			ResultSet rs = ps.executeQuery();
			return this.extractServers(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find group servers", e);
		}
	}

	@Override
	public List<ServerGroupPojo> findGroupsOfServer(int serverId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"SELECT g.* FROM ncore_server_group_server gs INNER JOIN ncore_server_group g ON g.group_id = gs.group_id WHERE server_id=?");
			ps.setInt(1, serverId);
			ResultSet rs = ps.executeQuery();
			return this.extractServerGroups(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find groups of server", e);
		}
	}

	@Override
	public List<ServerLogPojo> findLatestLogs(Integer serverId, int startIndex, int maxResults) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps;
			if (serverId == null)
			{
				ps = connection.prepareStatement("SELECT * FROM ncore_server_log ORDER BY (log_id) DESC LIMIT ?,?");
				ps.setInt(1, startIndex);
				ps.setInt(2, maxResults);
			} else
			{
				ps = connection.prepareStatement("SELECT * FROM ncore_server_log WHERE server_id=? ORDER BY (log_id) DESC LIMIT ?,?");
				ps.setInt(1, serverId);
				ps.setInt(2, startIndex);
				ps.setInt(3, maxResults);
			}
			ResultSet rs = ps.executeQuery();
			return this.extractLogs(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find latest server logs", e);
		}
	}

	@Override
	public List<ServerLogPojo> findLatestLogs(Integer serverId, Level level, String search, int startIndex, int maxResults)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"SELECT * FROM ncore_server_log WHERE (? IS NULL OR server_id = ?) AND (? IS NULL OR level = ?) AND (? IS NULL OR message LIKE ?) ORDER BY (log_id) DESC LIMIT ?, ?");
			ps.setObject(1, serverId);
			ps.setObject(2, serverId);
			ps.setObject(3, level == null ? null : level.getName());
			ps.setObject(4, level == null ? null : level.getName());
			ps.setString(5, search == null ? null : "%" + search + "%");
			ps.setString(6, search == null ? null : "%" + search + "%");
			ps.setInt(7, startIndex);
			ps.setInt(8, maxResults);
			ResultSet rs = ps.executeQuery();
			return this.extractLogs(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find latest logs", e);
		}
	}

	@Override
	public List<ServerLogPojo> findLatestLogs(Integer serverId, String search, int startIndex, int maxResults)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps;
			if (serverId == null)
			{
				ps = connection.prepareStatement("SELECT * FROM ncore_server_log WHERE message LIKE ? ORDER BY (log_id) DESC LIMIT ?,?");
				ps.setString(1, "%" + search + "%");
				ps.setInt(2, startIndex);
				ps.setInt(3, maxResults);
			} else
			{
				ps = connection.prepareStatement(
					"SELECT * FROM ncore_server_log WHERE server_id=? AND message LIKE ? ORDER BY (log_id) DESC LIMIT ?,?");
				ps.setInt(1, serverId);
				ps.setString(2, "%" + search + "%");
				ps.setInt(3, startIndex);
				ps.setInt(4, maxResults);
			}
			ResultSet rs = ps.executeQuery();
			return this.extractLogs(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find latest server logs", e);
		}
	}

	@Override
	public ServerRuntimePojo findLatestRuntime(int serverId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps =
				connection.prepareStatement("SELECT * FROM ncore_server_runtime WHERE server_id = ? ORDER BY (runtime_id) DESC LIMIT 1");
			ps.setInt(1, serverId);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractRuntime(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find latest runtime", e);
		}
	}

	@Override
	public List<ServerRuntimePojo> findLatestRuntimes(int serverId, int index, int startIndex) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps =
				connection.prepareStatement("SELECT * FROM ncore_server_runtime WHERE server_id = ? ORDER BY (runtime_id) DESC LIMIT ?,?");
			ps.setInt(1, serverId);
			ps.setInt(2, index);
			ps.setInt(3, startIndex);
			ResultSet rs = ps.executeQuery();
			return this.extractRuntimes(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find latest runtimes", e);
		}
	}

	@Override
	public ServerResourceUsagePojo findLatestServerResourceUsage(int serverId) throws DAOException
	{
		try (Connection con = this.getConnection())
		{
			PreparedStatement ps =
				con.prepareStatement("SELECT * FROM ncore_server_resource_usage WHERE server_id=? ORDER BY (`current_date`) DESC LIMIT 1");
			ps.setInt(1, serverId);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractResourceUsage(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find server resource usage", e);
		}
	}

	@Override
	public ServerLogPojo findLog(long logId)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_server_log WHERE log_id=?");
			ps.setLong(1, logId);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractLog(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find log", e);
		}
	}

	@Override
	public int findLogsCount(Integer serverId)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps;
			if (serverId == null)
			{
				ps = connection.prepareStatement("SELECT COUNT(*) FROM ncore_server_log");
			} else
			{
				ps = connection.prepareStatement("SELECT COUNT(*) FROM ncore_server_log WHERE server_id=?");
				ps.setInt(1, serverId);
			}
			ResultSet rs = ps.executeQuery();
			if (rs.next()) { return rs.getInt(1); }
			throw new DAOException("Expected result");
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find logs count", e);
		}
	}

	@Override
	public int findLogsCount(Integer serverId, Level level, String search)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps;
			if (serverId == null)
			{
				if (level == null)
				{
					if (search == null)
					{
						ps = connection.prepareStatement("SELECT COUNT(*) FROM ncore_server_log");
					} else
					{
						ps = connection.prepareStatement("SELECT COUNT(*) FROM ncore_server_log WHERE message LIKE ?");
						ps.setString(1, "%" + search + "%");
					}
				} else
				{
					if (search == null)
					{
						ps = connection.prepareStatement("SELECT COUNT(*) FROM ncore_server_log WHERE level = ?");
						ps.setString(1, level.getName());
					} else
					{
						ps = connection.prepareStatement("SELECT COUNT(*) FROM ncore_server_log WHERE level = ? AND message LIKE ?");
						ps.setString(1, level.getName());
						ps.setString(2, "%" + search + "%");
					}
				}
			} else
			{
				if (level == null)
				{
					if (search == null)
					{
						ps = connection.prepareStatement("SELECT COUNT(*) FROM ncore_server_log WHERE server_id=?");
						ps.setInt(1, serverId);
					} else
					{
						ps = connection.prepareStatement("SELECT COUNT(*) FROM ncore_server_log WHERE server_id=? AND message LIKE ?");
						ps.setInt(1, serverId);
						ps.setString(2, "%" + search + "%");
					}
				} else
				{
					if (search == null)
					{
						ps = connection.prepareStatement("SELECT COUNT(*) FROM ncore_server_log WHERE server_id=? AND level = ?");
						ps.setInt(1, serverId);
						ps.setString(2, level.getName());
					} else
					{
						ps = connection
							.prepareStatement("SELECT COUNT(*) FROM ncore_server_log WHERE server_id=? AND level = ? AND message LIKE ?");
						ps.setInt(1, serverId);
						ps.setString(2, level.getName());
						ps.setString(3, "%" + search + "%");
					}
				}
			}
			ResultSet rs = ps.executeQuery();
			if (rs.next()) { return rs.getInt(1); }
			throw new DAOException("Expected result");
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find logs count", e);
		}
	}

	@Override
	public int findLogsCount(Integer serverId, String search)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps;
			if (serverId == null)
			{
				ps = connection.prepareStatement("SELECT COUNT(*) FROM ncore_server_log WHERE message LIKE ?");
				ps.setString(1, "%" + search + "%");
			} else
			{
				ps = connection.prepareStatement("SELECT COUNT(*) FROM ncore_server_log WHERE server_id=? AND message LIKE ?");
				ps.setInt(1, serverId);
				ps.setString(2, "%" + search + "%");
			}
			ResultSet rs = ps.executeQuery();
			if (rs.next()) { return rs.getInt(1); }
			throw new DAOException("Expected result");
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find logs count", e);
		}
	}

	@Override
	public int findOnlineServerCount(Date date) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"SELECT COUNT(*) FROM ncore_server_runtime WHERE start_date<? AND stop_date>? OR start_date<? AND stop_date IS NULL");
			ps.setTimestamp(1, SQLUtil.dateToTimestamp(date));
			ps.setTimestamp(2, SQLUtil.dateToTimestamp(date));
			ps.setTimestamp(3, SQLUtil.dateToTimestamp(date));
			ResultSet rs = ps.executeQuery();
			if (rs.next()) { return rs.getInt(1); }
			throw new DAOException("Expected result");
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find online server count", e);
		}
	}

	@Override
	public List<ServerRuntimePojo> findOpenRuntimes(int serverId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps =
				connection.prepareStatement("SELECT * FROM ncore_server_runtime WHERE server_id = ? AND stop_date IS NULL");
			ps.setInt(1, serverId);
			ResultSet rs = ps.executeQuery();
			return this.extractRuntimes(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find open runtimes", e);
		}
	}

	@Override
	public ServerRuntimePojo findRuntime(int runtimeId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_server_runtime WHERE runtime_id = ?");
			ps.setInt(1, runtimeId);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractRuntime(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find server runtime", e);
		}
	}

	@Override
	public int findRuntimesOfServerCount(int serverId)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT COUNT(*) FROM ncore_server_runtime WHERE server_id=?");
			ps.setInt(1, serverId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) { return rs.getInt(1); }
			throw new DAOException("Expected result");
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find runtimes of server count", e);
		}
	}

	@Override
	public ServerPojo findServer(int serverId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_server WHERE server_id=?");
			ps.setInt(1, serverId);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractServer(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find server", e);
		}
	}

	@Override
	public ServerPojo findServer(String name) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_server WHERE name=?");
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractServer(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find server", e);
		}
	}

	@Override
	public ServerGroupPojo findServerGroup(int groupId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_server_group WHERE group_id=?");
			ps.setInt(1, groupId);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractServerGroup(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find server group", e);
		}
	}

	@Override
	public ServerGroupPojo findServerGroup(String groupName) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_server_group WHERE group_name=?");
			ps.setString(1, groupName);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractServerGroup(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find server group", e);
		}
	}

	@Override
	public List<ServerGroupPojo> findServerGroups() throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_server_group");
			ResultSet rs = ps.executeQuery();
			return this.extractServerGroups(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find server groups", e);
		}
	}

	@Override
	public List<ServerGroupPojo> findServerGroups(String search, int startIndex, int maxResults)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps =
				connection.prepareStatement("SELECT * FROM ncore_server_group WHERE (? IS NULL OR group_name LIKE ?) LIMIT ?,?");
			if (search != null)
			{
				search = "%" + search + "%";
			}
			ps.setString(1, search);
			ps.setString(2, search);
			ps.setInt(3, startIndex);
			ps.setInt(4, maxResults);
			ResultSet rs = ps.executeQuery();
			return this.extractServerGroups(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find server groups", e);
		}
	}

	@Override
	public List<ServerPojo> findServers()
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_server");
			ResultSet rs = ps.executeQuery();
			return this.extractServers(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find servers", e);
		}
	}

	@Override
	public List<ServerPojo> findServers(int startIndex, int count)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_server LIMIT ?,?");
			ps.setInt(1, startIndex);
			ps.setInt(2, count);
			ResultSet rs = ps.executeQuery();
			return this.extractServers(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find servers", e);
		}
	}

	@Override
	public List<ServerPojo> findServers(String name) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_server WHERE name LIKE ?");
			ps.setString(1, "%" + name + "%");
			ResultSet rs = ps.executeQuery();
			return this.extractServers(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find servers", e);
		}
	}

	@Override
	public List<ServerPojo> findServersByName(String name, int startIndex, int count)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_server WHERE name LIKE ? LIMIT ?,?");
			ps.setString(1, "%" + name + "%");
			ps.setInt(2, startIndex);
			ps.setInt(3, count);
			ResultSet rs = ps.executeQuery();
			return this.extractServers(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find servers by name", e);
		}
	}

	@Override
	public int findServersByNameCount(String name)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT COUNT(*) FROM ncore_server WHERE name LIKE ?");
			ps.setString(1, "%" + name + "%");
			ResultSet rs = ps.executeQuery();
			if (rs.next()) { return rs.getInt(1); }
			throw new DAOException("Expected result");
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find servers by name count", e);
		}
	}

	@Override
	public int findServersCount()
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT COUNT(*) FROM ncore_server");
			ResultSet rs = ps.executeQuery();
			if (rs.next()) { return rs.getInt(1); }
			throw new DAOException("Expected result");
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find servers count", e);
		}
	}

	@Override
	public List<ServerStatisticPojo> findServerStatistics(int serverId, Date since)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"SELECT * FROM ncore_server_statistic WHERE server_id=? AND `current_date` > ? ORDER BY `current_date` ASC");
			ps.setInt(1, serverId);
			ps.setTimestamp(2, SQLUtil.dateToTimestamp(since));
			ResultSet rs = ps.executeQuery();
			return this.extractServerStatistics(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find server statistics", e);
		}
	}

	@Override
	public List<ServerPojo> findServersWithStatus(boolean online) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM ncore_server WHERE is_online=?");
			ps.setBoolean(1, online);
			ResultSet rs = ps.executeQuery();
			return this.extractServers(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find online servers", e);
		}
	}

	@Override
	public boolean isServerInGroup(int groupId, int serverId)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT 1 FROM ncore_server_group_server WHERE group_id=? AND server_id=?");
			ps.setInt(1, groupId);
			ps.setInt(2, serverId);
			ResultSet rs = ps.executeQuery();
			return rs.next();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to check if server is in group", e);
		}
	}

	@Override
	public void updateRuntime(ServerRuntimePojo runtime) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"UPDATE ncore_server_runtime SET server_id = ?, start_date = ?, last_ping_date = ?, stop_date = ? WHERE runtime_id = ?");
			ps.setInt(1, runtime.getServerId());
			ps.setTimestamp(2, SQLUtil.dateToTimestamp(runtime.getStartDate()));
			ps.setTimestamp(3, SQLUtil.dateToTimestamp(runtime.getLastPingDate()));
			ps.setTimestamp(4, SQLUtil.dateToTimestamp(runtime.getStopDate()));
			ps.setInt(5, runtime.getRuntimeId());
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to update server runtime", e);
		}
	}

	@Override
	public void updateServer(ServerPojo server) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"UPDATE ncore_server SET name=?, is_online=?, socket_auto=?, socket_bind_host=?, socket_host=?, socket_port=?, socket_key=? WHERE server_id=?");
			ps.setString(1, server.getName());
			ps.setBoolean(2, server.isOnline());
			ps.setBoolean(3, server.isSocketAuto());
			ps.setString(4, server.getSocketBindHost());
			ps.setString(5, server.getSocketHost());
			ps.setObject(6, server.getSocketPort());
			ps.setString(7, server.getSocketKey());
			ps.setInt(8, server.getServerId());
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to update server", e);
		}
	}

	@Override
	public void updateServerGroup(int groupId, ServerGroupPojo group) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection
				.prepareStatement("UPDATE ncore_server_group SET group_name=?, group_display_name=?, group_description=? WHERE group_id=?");
			ps.setString(1, group.getName());
			ps.setString(2, group.getDisplayName());
			ps.setString(3, group.getDescription());
			ps.setInt(4, groupId);
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to update server group", e);
		}
	}

	private ServerLogPojo extractLog(ResultSet rs) throws SQLException
	{
		return new ServerLogPojo(rs.getLong("log_id"), rs.getInt("server_id"), SQLUtil.timestampToDate(rs.getTimestamp("log_date")),
			rs.getString("level"), rs.getString("message"), rs.getInt("skipped"));
	}

	private List<ServerLogPojo> extractLogs(ResultSet rs) throws SQLException
	{
		List<ServerLogPojo> pojos = new ArrayList<>();
		while (rs.next())
		{
			pojos.add(this.extractLog(rs));
		}
		return pojos;
	}

	private ServerResourceUsagePojo extractResourceUsage(ResultSet rs) throws SQLException
	{
		return new ServerResourceUsagePojo(rs.getInt("server_id"), SQLUtil.timestampToDate(rs.getTimestamp("current_date")),
			rs.getFloat("tps"), rs.getFloat("node_cpu_usage"), rs.getFloat("server_cpu_usage"), rs.getInt("memory_used"),
			rs.getInt("memory_available"), rs.getInt("memory_total"));
	}

	private ServerRuntimePojo extractRuntime(ResultSet rs) throws SQLException
	{
		return new ServerRuntimePojo(rs.getInt("runtime_id"), rs.getInt("server_id"), new Date(rs.getTimestamp("start_date").getTime()),
			new Date(rs.getTimestamp("last_ping_date").getTime()), SQLUtil.timestampToDate(rs.getTimestamp("stop_date")));
	}

	private List<ServerRuntimePojo> extractRuntimes(ResultSet rs) throws SQLException
	{
		List<ServerRuntimePojo> list = new ArrayList<>();
		while (rs.next())
		{
			list.add(this.extractRuntime(rs));
		}
		return list;
	}

	private ServerPojo extractServer(ResultSet rs) throws SQLException
	{
		return new ServerPojo(rs.getInt("server_id"), rs.getString("name"), ServerType.fromValue(rs.getInt("type")),
			rs.getBoolean("is_online"), rs.getBoolean("socket_auto"), rs.getString("socket_bind_host"), rs.getString("socket_host"),
			rs.getInt("socket_port"), rs.getString("socket_key"));
	}

	private ServerGroupPojo extractServerGroup(ResultSet rs) throws SQLException
	{
		return new ServerGroupPojo(rs.getInt("group_id"), rs.getString("group_name"), rs.getString("group_display_name"),
			rs.getString("group_description"));
	}

	private List<ServerGroupPojo> extractServerGroups(ResultSet rs) throws SQLException
	{
		List<ServerGroupPojo> pojos = new ArrayList<>();
		while (rs.next())
		{
			pojos.add(this.extractServerGroup(rs));
		}
		return pojos;
	}

	private List<ServerPojo> extractServers(ResultSet rs) throws SQLException
	{
		List<ServerPojo> servers = new ArrayList<>();
		while (rs.next())
		{
			servers.add(this.extractServer(rs));
		}
		return servers;
	}

	private ServerStatisticPojo extractServerStatistic(ResultSet rs) throws SQLException
	{
		return new ServerStatisticPojo(rs.getInt("server_id"), SQLUtil.timestampToDate(rs.getTimestamp("current_date")),
			rs.getInt("online_players"), rs.getFloat("tps"), rs.getFloat("server_cpu_usage"), rs.getInt("memory_used"),
			rs.getInt("memory_available"), rs.getInt("memory_total"));
	}

	private List<ServerStatisticPojo> extractServerStatistics(ResultSet rs) throws SQLException
	{
		List<ServerStatisticPojo> list = new ArrayList<>();
		while (rs.next())
		{
			list.add(this.extractServerStatistic(rs));
		}
		return list;
	}
}
