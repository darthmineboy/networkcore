package net.rieksen.networkcore.core.dao.exception;

import net.rieksen.networkcore.core.dao.DAOException;

public class DAOIntegrityException extends DAOException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DAOIntegrityException()
	{}

	public DAOIntegrityException(String message)
	{
		super(message);
	}

	public DAOIntegrityException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public DAOIntegrityException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public DAOIntegrityException(Throwable cause)
	{
		super(cause);
	}
}
