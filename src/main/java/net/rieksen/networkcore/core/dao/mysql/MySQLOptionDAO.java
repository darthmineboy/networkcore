package net.rieksen.networkcore.core.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import net.rieksen.networkcore.core.dao.DAOException;
import net.rieksen.networkcore.core.dao.OptionDAO;
import net.rieksen.networkcore.core.option.OptionType;
import net.rieksen.networkcore.core.option.pojo.OptionPojo;
import net.rieksen.networkcore.core.option.pojo.OptionSectionPojo;
import net.rieksen.networkcore.core.option.pojo.OptionValuePojo;
import net.rieksen.networkcore.core.util.IMySQL;

public class MySQLOptionDAO extends MySQLBaseDAO implements OptionDAO
{

	public final static String	OPTION_SECTION_TABLE	= "ncore_option_section";
	public final static String	OPTION_TABLE			= "ncore_option";
	public final static String	OPTION_VALUE_TABLE		= "ncore_option_value";

	public MySQLOptionDAO(IMySQL mysql) throws DAOException
	{
		super(mysql);
	}

	@Override
	public int createOption(OptionPojo option) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"INSERT INTO " + MySQLOptionDAO.OPTION_TABLE + " (section_id, name, type, description) VALUES (?, ?, ?, ?)",
				new String[] { "option_id" });
			ps.setInt(1, option.getSectionId());
			ps.setString(2, option.getName());
			ps.setInt(3, option.getOptionType().getValue());
			ps.setString(4, option.getDescription());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) { return rs.getInt(1); }
			throw new DAOException("Expected generated column");
		} catch (SQLException e)
		{
			throw this.error("Failed to create option", e);
		}
	}

	@Override
	public int createSection(OptionSectionPojo section) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"INSERT INTO " + MySQLOptionDAO.OPTION_SECTION_TABLE + " (plugin_id, name, description) VALUES (?, ?, ?)",
				new String[] { "section_id" });
			ps.setInt(1, section.getPluginId());
			ps.setString(2, section.getName());
			ps.setString(3, section.getDescription());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) { return rs.getInt(1); }
			throw new DAOException("Expected generated column");
		} catch (SQLException e)
		{
			throw new DAOException("Failed to create option section", e);
		}
	}

	@Override
	public int createValue(OptionValuePojo value) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"INSERT INTO " + MySQLOptionDAO.OPTION_VALUE_TABLE + " (option_id, server_id, `index`, `value`) VALUES (?, ?, ?, ?)",
				new String[] { "value_id" });
			ps.setInt(1, value.getOptionId());
			ps.setObject(2, value.getServerId());
			ps.setInt(3, value.getIndex());
			ps.setString(4, value.getValue());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) { return rs.getInt(1); }
			throw new DAOException("Expected generated column");
		} catch (SQLException e)
		{
			throw new DAOException("Failed to create option value", e);
		}
	}

	@Override
	public void deleteOption(int optionId)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("DELETE FROM ncore_option WHERE option_id=?");
			ps.setInt(1, optionId);
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to delete option", e);
		}
	}

	@Override
	public void deleteSection(int sectionId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("DELETE FROM ncore_option_section WHERE section_id=?");
			ps.setInt(1, sectionId);
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to delete option section", e);
		}
	}

	@Override
	public void deleteValue(int valueId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("DELETE FROM " + MySQLOptionDAO.OPTION_VALUE_TABLE + " WHERE value_id=?");
			ps.setInt(1, valueId);
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to delete option value", e);
		}
	}

	@Override
	public OptionPojo findOption(int optionId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM " + MySQLOptionDAO.OPTION_TABLE + " WHERE option_id=?");
			ps.setInt(1, optionId);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractOption(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find option", e);
		}
	}

	@Override
	public OptionPojo findOption(int sectionId, String name) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps =
				connection.prepareStatement("SELECT * FROM " + MySQLOptionDAO.OPTION_TABLE + " WHERE section_id=? AND name=?");
			ps.setInt(1, sectionId);
			ps.setString(2, name);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractOption(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find option pojo", e);
		}
	}

	@Override
	public List<OptionPojo> findOptions(int sectionId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM " + MySQLOptionDAO.OPTION_TABLE + " WHERE section_id=?");
			ps.setInt(1, sectionId);
			ResultSet rs = ps.executeQuery();
			return this.extractOptions(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find options", e);
		}
	}

	@Override
	public List<OptionValuePojo> findOptionValuesWithDuplicateIndexes()
	{
		try (Connection connection = this.getConnection())
		{
			ResultSet rs = connection.createStatement().executeQuery(
				"SELECT * FROM ncore_option_value o WHERE EXISTS (SELECT 1 FROM ncore_option_value v WHERE v.value_id!=o.value_id AND v.option_id=o.option_id AND (SELECT IF (v.server_id IS NULL,0,v.server_id))=(SELECT IF(o.server_id IS NULL,0,o.server_id)) AND v.index=o.index)");
			return this.extractValues(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find option values with duplicate indexes", e);
		}
	}

	@Override
	public OptionSectionPojo findSection(int sectionId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps =
				connection.prepareStatement("SELECT * FROM " + MySQLOptionDAO.OPTION_SECTION_TABLE + " WHERE section_id=?");
			ps.setInt(1, sectionId);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractSection(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find option section", e);
		}
	}

	@Override
	public OptionSectionPojo findSection(int pluginId, String name) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps =
				connection.prepareStatement("SELECT * FROM " + MySQLOptionDAO.OPTION_SECTION_TABLE + " WHERE plugin_id=? AND name=?");
			ps.setInt(1, pluginId);
			ps.setString(2, name);
			ResultSet rs = ps.executeQuery();
			return rs.next() ? this.extractSection(rs) : null;
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find option section", e);
		}
	}

	@Override
	public int findSectionCount(int pluginId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps =
				connection.prepareStatement("SELECT COUNT(*) FROM " + MySQLOptionDAO.OPTION_SECTION_TABLE + " WHERE plugin_id=?");
			ps.setInt(1, pluginId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) { return rs.getInt(1); }
			throw new DAOException("Expected result");
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find option section count", e);
		}
	}

	@Override
	public List<OptionSectionPojo> findSections() throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM " + MySQLOptionDAO.OPTION_SECTION_TABLE);
			ResultSet rs = ps.executeQuery();
			return this.extractSections(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find option sections", e);
		}
	}

	@Override
	public List<OptionSectionPojo> findSections(int pluginId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps =
				connection.prepareStatement("SELECT * FROM " + MySQLOptionDAO.OPTION_SECTION_TABLE + " WHERE plugin_id=?");
			ps.setInt(1, pluginId);
			ResultSet rs = ps.executeQuery();
			return this.extractSections(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find option sections", e);
		}
	}

	@Override
	public List<OptionValuePojo> findValues(int optionId) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM " + MySQLOptionDAO.OPTION_VALUE_TABLE + " WHERE option_id=?");
			ps.setInt(1, optionId);
			ResultSet rs = ps.executeQuery();
			return this.extractValues(rs);
		} catch (SQLException e)
		{
			throw new DAOException("Failed to find option values", e);
		}
	}

	@Override
	public void updateOption(OptionPojo pojo)
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement("UPDATE ncore_option SET description=? WHERE option_id=?");
			ps.setString(1, pojo.getDescription());
			ps.setInt(2, pojo.getOptionId());
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to update option", e);
		}
	}

	@Override
	public void updateSection(OptionSectionPojo section) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"UPDATE " + MySQLOptionDAO.OPTION_SECTION_TABLE + " SET plugin_id=?, name=?, description=? WHERE section_id=?");
			ps.setInt(1, section.getPluginId());
			ps.setString(2, section.getName());
			ps.setString(3, section.getDescription());
			ps.setInt(4, section.getSectionId());
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to update option section", e);
		}
	}

	@Override
	public void updateValue(OptionValuePojo value) throws DAOException
	{
		try (Connection connection = this.getConnection())
		{
			PreparedStatement ps = connection.prepareStatement(
				"UPDATE " + MySQLOptionDAO.OPTION_VALUE_TABLE + " SET option_id=?, server_id=?, `index`=?, `value`=? WHERE value_id=?");
			ps.setInt(1, value.getOptionId());
			ps.setObject(2, value.getServerId());
			ps.setInt(3, value.getIndex());
			ps.setString(4, value.getValue());
			ps.setInt(5, value.getValueId());
			ps.executeUpdate();
		} catch (SQLException e)
		{
			throw new DAOException("Failed to update option value", e);
		}
	}

	private OptionPojo extractOption(ResultSet rs) throws SQLException
	{
		return new OptionPojo(rs.getInt("option_id"), rs.getInt("section_id"), rs.getString("name"), OptionType.fromInt(rs.getInt("type")),
			rs.getString("description"));
	}

	private List<OptionPojo> extractOptions(ResultSet rs) throws SQLException
	{
		List<OptionPojo> list = new ArrayList<>();
		while (rs.next())
		{
			list.add(this.extractOption(rs));
		}
		return list;
	}

	private OptionSectionPojo extractSection(ResultSet rs) throws SQLException
	{
		return new OptionSectionPojo(rs.getInt("section_id"), rs.getInt("plugin_id"), rs.getString("name"), rs.getString("description"));
	}

	private List<OptionSectionPojo> extractSections(ResultSet rs) throws SQLException
	{
		List<OptionSectionPojo> list = new ArrayList<>();
		while (rs.next())
		{
			list.add(this.extractSection(rs));
		}
		return list;
	}

	private OptionValuePojo extractValue(ResultSet rs) throws SQLException
	{
		Integer serverId = rs.getInt("server_id");
		serverId = rs.wasNull() ? null : serverId;
		return new OptionValuePojo(rs.getInt("value_id"), rs.getInt("option_id"), serverId, rs.getInt("index"), rs.getString("value"));
	}

	private List<OptionValuePojo> extractValues(ResultSet rs) throws SQLException
	{
		List<OptionValuePojo> list = new ArrayList<>();
		while (rs.next())
		{
			list.add(this.extractValue(rs));
		}
		return list;
	}
}
