package net.rieksen.networkcore.core.dao.mysql.update;

import java.sql.Connection;
import java.sql.SQLException;

import net.rieksen.networkcore.core.update.MySQLDAOUpdate;
import net.rieksen.networkcore.core.util.IMySQL;

class MySQLDAOUpdate_v2_2 extends MySQLDAOUpdate
{

	public MySQLDAOUpdate_v2_2(IMySQL mysql)
	{
		super(mysql, "2.1", "2.2", "Added server groups");
	}

	@Override
	public void doTransaction(Connection connection) throws SQLException
	{
		connection.createStatement()
			.executeUpdate("CREATE TABLE IF NOT EXISTS ncore_server_group ("//
				+ "group_id INT NOT NULL AUTO_INCREMENT,"//
				+ "group_name VARCHAR(255) NOT NULL,"//
				+ "group_display_name VARCHAR(255) NOT NULL,"//
				+ "group_description VARCHAR(1024) NULL,"//
				+ "PRIMARY KEY (group_id),"//
				+ "CONSTRAINT uq_ncore_server_group_name UNIQUE (group_name)"//
				+ ") ENGINE=InnoDb DEFAULT CHARSET=utf8");
		connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS ncore_server_group_server ("//
			+ "group_id INT NOT NULL,"//
			+ "server_id INT NOT NULL,"//
			+ "PRIMARY KEY (group_id, server_id),"//
			+ "CONSTRAINT fk_ncore_server_group_server_group FOREIGN KEY (group_id) REFERENCES ncore_server_group (group_id) ON DELETE CASCADE,"//
			+ "CONSTRAINT fk_ncore_server_group_server_server FOREIGN KEY (server_id) REFERENCES ncore_server (server_id) ON DELETE CASCADE"//
			+ ") ENGINE=InnoDb DEFAULT CHARSET=utf8");
	}
}
