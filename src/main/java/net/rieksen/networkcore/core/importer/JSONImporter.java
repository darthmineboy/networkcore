package net.rieksen.networkcore.core.importer;

import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.message.Language;
import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.message.MessageSection;
import net.rieksen.networkcore.core.message.MessageTranslation;
import net.rieksen.networkcore.core.message.MessageVariable;
import net.rieksen.networkcore.core.message.pojo.GlobalMessageVariablePojo;
import net.rieksen.networkcore.core.message.pojo.LanguagePojo;
import net.rieksen.networkcore.core.option.OptionSection;
import net.rieksen.networkcore.core.option.OptionType;
import net.rieksen.networkcore.core.option.OptionValue;
import net.rieksen.networkcore.core.option.pojo.OptionPojo;
import net.rieksen.networkcore.core.option.pojo.OptionValuePojo;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;

public class JSONImporter
{

	public static JSONImporter loadFile(NetworkCore provider, File file)
	{
		Validate.notNull(file);
		try
		{
			Object parsed = new JSONParser().parse(new FileReader(file));
			JSONObject jsonObject = (JSONObject) parsed;
			return new JSONImporter(provider, jsonObject);
		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public static JSONImporter loadInputStream(NetworkCore provider, InputStream stream)
	{
		Validate.notNull(stream);
		try
		{
			Object parsed = new JSONParser().parse(new InputStreamReader(stream));
			JSONObject jsonObject = (JSONObject) parsed;
			return new JSONImporter(provider, jsonObject);
		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	private Map<Option, Object>	options	= new HashMap<>();
	private final JSONObject	json;
	private final NetworkCore	provider;

	public JSONImporter(NetworkCore provider, JSONObject json)
	{
		this.provider = provider;
		this.json = json;
	}

	public boolean getOptionBoolean(Option option)
	{
		return (Boolean) this.options.get(option);
	}

	public String getOptionString(Option option)
	{
		return (String) this.options.get(option);
	}

	public boolean importData()
	{
		try
		{
			this.loadOptions();
			this.populateMessageSections();
			this.populateOptionSections();
			this.populateGlobalMessageVariables();
			return true;
		} catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}

	private Language getDefaultLanguage()
	{
		String languageName = this.getOptionString(Option.LANGUAGE);
		Language language = Language.getLanguage(languageName);
		if (language == null)
		{
			language = this.provider.getMessageModule().createLanguage(new LanguagePojo(0, languageName));
		}
		return language;
	}

	private void loadOptions()
	{
		JSONObject options = (JSONObject) this.json.get("options");
		if (options == null) { return; }
		for (Option option : Option.values())
		{
			if (options.containsKey(option.getKey()))
			{
				this.options.put(option, options.get(option.getKey()));
			} else
			{
				this.options.put(option, option.getDefaultValue());
			}
		}
	}

	private String objectToOptionValue(Object o)
	{
		return o == null ? null : String.valueOf(o);
	}

	private void populateGlobalMessageVariables()
	{
		JSONObject vars = (JSONObject) this.json.get("global-message-variables");
		if (vars == null) { return; }
		for (Object key : vars.keySet())
		{
			Object value = vars.get(key);
			String name = (String) key;
			if (this.provider.getMessageModule().getGlobalVariable(name) == null)
			{
				this.provider.getMessageModule().createGlobalVariable(new GlobalMessageVariablePojo(0, name, (String) value));
			}
		}
	}

	@Deprecated
	private void populateMessageSections()
	{
		JSONObject sections = (JSONObject) this.json.get("message-sections");
		if (sections == null) { return; }
		String pluginName = this.getOptionString(Option.PLUGIN);
		NetworkPlugin plugin = NetworkPlugin.getPlugin(pluginName);
		if (plugin == null)
		{
			plugin = this.provider.getPluginModule().createPlugin(pluginName);
		}
		for (Object sectionName : sections.keySet())
		{
			JSONObject sectionValueJSON = (JSONObject) sections.get(sectionName);
			String sectionDescription = (String) sectionValueJSON.get("description");
			MessageSection section = plugin.getMessageSection((String) sectionName);
			if (section == null)
			{
				section = plugin.createMessageSection((String) sectionName, sectionDescription);
			}
			if (this.getOptionBoolean(Option.UPDATE_MESSAGE_SECTION_DESCRIPTION))
			{
				if (!StringUtils.equals(section.getDescription(), sectionDescription))
				{
					section.changeDescription(sectionDescription);
				}
			}
			JSONObject messages = (JSONObject) sectionValueJSON.get("messages");
			for (Object messageName : messages.keySet())
			{
				JSONObject messageJSON = (JSONObject) messages.get(messageName);
				String messageDescription = (String) messageJSON.get("description");
				Message message;
				message = section.getMessage((String) messageName);
				if (message == null)
				{
					// TODO defaultMessage 2nd argument is null
					message = section.createMessage((String) messageName, messageDescription);
				}
				if (this.getOptionBoolean(Option.UPDATE_MESSAGE_DESCRIPTION))
				{
					if (!StringUtils.equals(message.getDescription(), messageDescription))
					{
						message.changeDescription(messageDescription);
					}
				}
				String translation = (String) messageJSON.get("translation");
				Language language = this.getDefaultLanguage();
				MessageTranslation messageTranslation = message.getTranslation(language.getLanguageId());
				if (messageTranslation == null)
				{
					messageTranslation = message.createTranslation(language.getLanguageId(), translation);
				} else
				{
					if (this.getOptionBoolean(Option.UPDATE_MESSAGE_TRANSLATION))
					{
						if (!messageTranslation.getMessage().equals(translation))
						{
							messageTranslation.changeMessage(translation);
						}
					}
				}
				JSONObject variables = (JSONObject) messageJSON.get("variables");
				if (variables != null)
				{
					for (Object variableName : variables.keySet())
					{
						String variableDescription = (String) variables.get(variableName);
						MessageVariable variable = message.getVariable((String) variableName);
						if (variable == null)
						{
							variable = message.createVariable((String) variableName, variableDescription);
						}
						if (this.getOptionBoolean(Option.UPDATE_MESSAGE_VARIABLE_DESCRIPTION))
						{
							if (!StringUtils.equals(variable.getDescription(), variableDescription))
							{
								variable.changeDescription(variableDescription);
							}
						}
					}
				}
			}
		}
	}

	private void populateOptionSections()
	{
		JSONObject sections = (JSONObject) this.json.get("option-sections");
		if (sections == null) { return; }
		String pluginName = this.getOptionString(Option.PLUGIN);
		NetworkPlugin plugin = NetworkPlugin.getPlugin(pluginName);
		if (plugin == null)
		{
			plugin = this.provider.getPluginModule().createPlugin(pluginName);
		}
		for (Object sectionName : sections.keySet())
		{
			JSONObject sectionValueJSON = (JSONObject) sections.get(sectionName);
			String sectionDescription = (String) sectionValueJSON.get("description");
			OptionSection section = plugin.getOptionSection((String) sectionName);
			if (section == null)
			{
				section = plugin.createOptionSection((String) sectionName, sectionDescription);
			}
			JSONObject options = (JSONObject) sectionValueJSON.get("options");
			for (Object optionName : options.keySet())
			{
				JSONObject optionValueJSON = (JSONObject) options.get(optionName);
				String optionDescription = (String) optionValueJSON.get("description");
				OptionType optionType = OptionType.valueOf((String) optionValueJSON.get("type"));
				Object defaultOptionValue = optionValueJSON.get("value");
				net.rieksen.networkcore.core.option.Option option = section.getOption((String) optionName);
				if (option == null)
				{
					option =
						section.createOption(new OptionPojo(0, section.getSectionId(), (String) optionName, optionType, optionDescription));
				}
				List<OptionValue> values = option.getApplyingValues(null);
				if (values.isEmpty())
				{
					if (optionType != OptionType.LIST)
					{
						option.createValue(
							new OptionValuePojo(0, option.getOptionId(), null, 0, this.objectToOptionValue(defaultOptionValue)));
					} else
					{
						JSONArray defaultOptionValueJSON = (JSONArray) defaultOptionValue;
						int index = 0;
						for (Object defaultOptionListValue : defaultOptionValueJSON)
						{
							option.createValue(new OptionValuePojo(0, option.getOptionId(), null, index,
								this.objectToOptionValue(defaultOptionListValue)));
							index++;
						}
					}
				}
			}
		}
	}

	private static enum Option
	{
		PLUGIN(null, String.class),
		LANGUAGE("English", String.class),
		UPDATE_MESSAGE_SECTION_DESCRIPTION(true, Boolean.class),
		UPDATE_MESSAGE_DESCRIPTION(true, Boolean.class),
		UPDATE_MESSAGE_VARIABLE_DESCRIPTION(true, Boolean.class),
		UPDATE_MESSAGE_TRANSLATION(false, Boolean.class),
		UPDATE_OPTION_SECTION_DESCRIPTION(true, Boolean.class),
		UPDATE_OPTION_DESCRIPTION(true, Boolean.class);

		private final Object	defaultValue;
		@SuppressWarnings("unused")
		private final Class<?>	type;

		Option(Object defaultValue, Class<?> type)
		{
			this.defaultValue = defaultValue;
			this.type = type;
		}

		public Object getDefaultValue()
		{
			return this.defaultValue;
		}

		public String getKey()
		{
			return this.name().toLowerCase().replace("_", "-");
		}
	}
}
