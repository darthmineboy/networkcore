package net.rieksen.networkcore.core;

import java.util.logging.Logger;

import co.aikar.taskchain.TaskChainFactory;
import net.rieksen.networkcore.core.dao.DAOManager;
import net.rieksen.networkcore.core.enums.PluginStatus;
import net.rieksen.networkcore.core.info.InfoModule;
import net.rieksen.networkcore.core.issue.IssueModule;
import net.rieksen.networkcore.core.message.MessageModule;
import net.rieksen.networkcore.core.network.NetworkModule;
import net.rieksen.networkcore.core.option.OptionModule;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.core.plugin.PluginModule;
import net.rieksen.networkcore.core.server.ServerModule;
import net.rieksen.networkcore.core.socket.ServerSocketModule;
import net.rieksen.networkcore.core.user.UserModule;
import net.rieksen.networkcore.core.util.MojangAPI;
import net.rieksen.networkcore.core.world.WorldModule;
import net.rieksen.networkcore.spigot.file.AutomaticServerConfig;

public interface NetworkCore
{

	void debug(String message);

	AutomaticServerConfig getAutomaticConfig();

	DAOManager getDAO();

	InfoModule getInfoModule();

	Logger getLogger();

	MessageModule getMessageModule();

	MojangAPI getMojangAPI();

	NetworkModule getNetworkModule();

	NetworkPlugin getNetworkPlugin();

	OptionModule getOptionModule();

	PluginModule getPluginModule();

	ServerModule getServerModule();

	ServerSocketModule getSocketModule();

	PluginStatus getStatus();

	TaskChainFactory getTaskChainFactory();

	IssueModule getIssueModule();

	UserModule getUserModule();

	WorldModule getWorldModule();
}
