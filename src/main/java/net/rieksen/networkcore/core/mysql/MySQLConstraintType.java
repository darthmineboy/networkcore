package net.rieksen.networkcore.core.mysql;

public enum MySQLConstraintType
{
	PRIMARY_KEY("PRIMARY KEY"),
	UNIQUE("UNIQUE"),
	FOREIGN_KEY("FOREIGN KEY");

	public static MySQLConstraintType fromMySQLType(String constraintType)
	{
		for (MySQLConstraintType constraint : MySQLConstraintType.values())
		{
			if (constraint.constraintType.equalsIgnoreCase(constraintType)) { return constraint; }
		}
		throw new IllegalStateException("Unrecognized constraint type " + constraintType);
	}

	private String constraintType;

	/**
	 * @param constraintType
	 *            name of the constraint in MySQL
	 */
	private MySQLConstraintType(String constraintType)
	{
		this.constraintType = constraintType;
	}
}
