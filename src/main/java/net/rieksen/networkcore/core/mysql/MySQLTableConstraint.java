package net.rieksen.networkcore.core.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MySQLTableConstraint
{

	public static MySQLTableConstraint fromResultSet(ResultSet rs) throws SQLException
	{
		String constraintCatalog = rs.getString("CONSTRAINT_CATALOG");
		String constraintSchema = rs.getString("CONSTRAINT_SCHEMA");
		String constraintName = rs.getString("CONSTRAINT_NAME");
		String tableSchema = rs.getString("TABLE_SCHEMA");
		String tableName = rs.getString("TABLE_NAME");
		MySQLConstraintType constraintType = MySQLConstraintType.fromMySQLType(rs.getString("CONSTRAINT_TYPE"));
		return new MySQLTableConstraint(constraintCatalog, constraintSchema, constraintName, tableSchema, tableName, constraintType);
	}

	private String				constraintCatalog;
	private String				constraintSchema;
	private String				constraintName;
	private String				tableSchema;
	private String				tableName;
	private MySQLConstraintType	constraintType;

	public MySQLTableConstraint(String constraintCatalog, String constraintSchema, String constraintName, String tableSchema,
		String tableName, MySQLConstraintType constraintType)
	{
		this.constraintCatalog = constraintCatalog;
		this.constraintSchema = constraintSchema;
		this.constraintName = constraintName;
		this.tableSchema = tableSchema;
		this.tableName = tableName;
		this.constraintType = constraintType;
	}

	public String getConstraintCatalog()
	{
		return this.constraintCatalog;
	}

	public String getConstraintName()
	{
		return this.constraintName;
	}

	public String getConstraintSchema()
	{
		return this.constraintSchema;
	}

	public MySQLConstraintType getConstraintType()
	{
		return this.constraintType;
	}

	public String getTableName()
	{
		return this.tableName;
	}

	public String getTableSchema()
	{
		return this.tableSchema;
	}
}
