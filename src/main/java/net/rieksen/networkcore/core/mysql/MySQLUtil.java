package net.rieksen.networkcore.core.mysql;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySQLUtil {

    public static void addTableConstraint(Connection connection, String tableName, String constraintName, String constraint) throws SQLException {
        PreparedStatement ps =
                connection.prepareStatement("ALTER TABLE `" + tableName + "` ADD CONSTRAINT `" + constraintName + "` " + constraint);
        ps.executeUpdate();
    }

    /**
     * Silently close a connection. Connection may be null.
     *
     * @param connection
     */
    public static void closeConnection(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean columnExists(Connection con, String tableName, String columnName) throws SQLException {
        DatabaseMetaData dbm = con.getMetaData();
        ResultSet columns = dbm.getColumns(null, null, tableName, columnName);
        while (columns.next()) {
            if (MySQLUtil.isTableInResultSet(columns, tableName) && MySQLUtil.isColumnInResultSet(columnName, columns)) {
                return true;
            }
        }
        return false;
    }

    public static void dropConstraintOnTable(Connection connection, MySQLTableConstraint constraint) throws SQLException {
        switch (constraint.getConstraintType()) {
            case FOREIGN_KEY: {
                MySQLUtil.dropForeignKey(connection, constraint.getTableName(), constraint.getConstraintName());
                break;
            }
            case PRIMARY_KEY:
            case UNIQUE:
                MySQLUtil.dropIndexOnTable(connection, constraint.getTableName(), constraint.getConstraintName());
                break;
            default:
                break;
        }
    }

    public static boolean dropConstraintsOnTable(Connection connection, String tableName, MySQLConstraintType constraintType)
            throws SQLException {
        List<MySQLTableConstraint> constraints = MySQLUtil.findTableConstraints(connection, tableName);
        boolean success = true;
        for (MySQLTableConstraint constraint : constraints) {
            if (constraint.getConstraintType() != constraintType) {
                continue;
            }
            try {
                MySQLUtil.dropConstraintOnTable(connection, constraint);
            } catch (SQLException e) {
                if (e.getErrorCode() == 1091) {
                    // Someone experienced issues where constraints were being deleted that did not exist
                    // MySQLSyntaxErrorException: Can't DROP 'fk_ncore_location_world'; check that column/key exists
                    success = false;
                } else {
                    throw e;
                }
            }
        }
        return success;
    }

    public static void dropForeignKey(Connection connection, String tableName, String foreignKey) throws SQLException {
        PreparedStatement ps = connection.prepareStatement("ALTER TABLE " + tableName + " DROP FOREIGN KEY " + foreignKey);
        ps.executeUpdate();
    }

    public static void dropIndexOnTable(Connection connection, String tableName, String indexName) throws SQLException {
        PreparedStatement ps = connection.prepareStatement("DROP INDEX `" + indexName + "` ON `" + tableName + "`");
        ps.executeUpdate();
    }

    /**
     * Queries the `information_schema` database to find all constraints on a
     * table.
     *
     * @param connection
     * @param tableSchema
     * @param tableName
     * @return
     * @throws SQLException
     */
    public static List<MySQLTableConstraint> findTableConstraints(Connection connection, String tableName) throws SQLException {
        PreparedStatement ps = connection.prepareStatement(
                "SELECT * FROM information_schema.TABLE_CONSTRAINTS " + "WHERE information_schema.TABLE_CONSTRAINTS.TABLE_NAME = ?");
        ps.setString(1, tableName);
        ResultSet rs = ps.executeQuery();
        List<MySQLTableConstraint> list = new ArrayList<>();
        while (rs.next()) {
            list.add(MySQLTableConstraint.fromResultSet(rs));
        }
        return list;
    }

    public static boolean isColumnDataTypeEqual(Connection con, String tableName, String columnName, int type, Integer length)
            throws SQLException {
        DatabaseMetaData dbm = con.getMetaData();
        ResultSet columns = dbm.getColumns(null, null, tableName, columnName);
        boolean result = false;
        while (columns.next()) {
            if (MySQLUtil.isTableInResultSet(columns, tableName) && MySQLUtil.isColumnInResultSet(columnName, columns)
                    && MySQLUtil.isDataTypeInResultSet(columns, type)) {
                if (length != null && !MySQLUtil.isDataLengthInResultSet(columns, length)) {
                    continue;
                }
                result = true;
                break;
            }
        }
        return result;
    }

    public static boolean isColumnNullable(Connection con, String tableName, String columnName) throws SQLException {
        DatabaseMetaData dbm = con.getMetaData();
        ResultSet columns = dbm.getColumns(null, null, tableName, columnName);
        boolean result = false;
        while (columns.next()) {
            if (MySQLUtil.isTableInResultSet(columns, tableName) && MySQLUtil.isColumnInResultSet(columnName, columns)) {
                String isNullable = columns.getString("IS_NULLABLE");
                if (isNullable.equalsIgnoreCase("YES")) {
                    result = true;
                }
                break;
            }
        }
        return result;
    }

    /**
     * Checks the connection metadata for a table with specified name.
     *
     * @param tableName
     * @return
     * @throws SQLException
     */
    public static boolean tableExists(Connection con, String tableName) throws SQLException {
        DatabaseMetaData dbm = con.getMetaData();
        ResultSet tables = dbm.getTables(null, null, tableName, null);
        boolean result = false;
        while (tables.next()) {
            if (MySQLUtil.isTableInResultSet(tables, tableName)) {
                result = true;
                break;
            }
        }
        return result;
    }

    private static boolean isColumnInResultSet(String columnName, ResultSet columns) throws SQLException {
        return columns.getString("COLUMN_NAME").equalsIgnoreCase(columnName);
    }

    private static boolean isDataLengthInResultSet(ResultSet columns, int length) throws SQLException {
        return columns.getInt("COLUMN_SIZE") == length;
    }

    private static boolean isDataTypeInResultSet(ResultSet columns, int type) throws SQLException {
        return columns.getInt("DATA_TYPE") == type;
    }

    private static boolean isTableInResultSet(ResultSet tables, String tableName) throws SQLException {
        return tables.getString("TABLE_NAME").equalsIgnoreCase(tableName);
    }
}
