package net.rieksen.networkcore.core.plugin;

import java.util.Map;

@Deprecated
public interface IUpdateInfo
{

	/**
	 * Add a tag to this update.
	 * 
	 * @param tag
	 * @param value
	 * @throws IllegalStateException
	 *             when tag is null
	 */
	void addTag(String tag, String value);

	/**
	 * Get the description of this update.
	 * 
	 * @return
	 */
	String getDescription();

	/**
	 * Get from where the update can be downloaded.
	 * 
	 * @return
	 */
	String getDownloadURL();

	/**
	 * Get where we can find info on the plugin.
	 * 
	 * @return
	 */
	String getInfoURL();

	/**
	 * Get the value of a tag.
	 * 
	 * @param tag
	 * @return
	 */
	String getTag(String tag);

	/**
	 * Get a copy of the tags.
	 * 
	 * @return
	 */
	Map<String, String> getTags();

	/**
	 * Get the version of this update.
	 * 
	 * @return
	 */
	String getVersion();

	/**
	 * Whether a tag is already set.
	 * 
	 * @param tag
	 * @return
	 */
	boolean hasTag(String tag);
}