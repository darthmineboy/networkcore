package net.rieksen.networkcore.core.plugin;

/**
 * A server can run plugins. This object contains information about a plugin
 * running on a server.
 */
public class ServerPluginPojo
{

	private int		serverId;
	private int		pluginId;
	private String	pluginVersion;
	private boolean	enabled;
	private String	previousPluginVersion;

	public ServerPluginPojo()
	{}

	@Deprecated
	public ServerPluginPojo(int serverId, int pluginId, String pluginVersion, boolean enabled)
	{
		this.serverId = serverId;
		this.pluginId = pluginId;
		this.pluginVersion = pluginVersion;
		this.enabled = enabled;
	}

	public ServerPluginPojo(int serverId, int pluginId, String pluginVersion, boolean enabled, String previousPluginVersion)
	{
		this.serverId = serverId;
		this.pluginId = pluginId;
		this.pluginVersion = pluginVersion;
		this.enabled = enabled;
		this.previousPluginVersion = previousPluginVersion;
	}

	public int getPluginId()
	{
		return this.pluginId;
	}

	public String getPluginVersion()
	{
		return this.pluginVersion;
	}

	/**
	 * Version of the previous runtime. May be null.
	 * 
	 * @return
	 */
	public String getPreviousPluginVersion()
	{
		return this.previousPluginVersion;
	}

	public int getServerId()
	{
		return this.serverId;
	}

	public boolean isEnabled()
	{
		return this.enabled;
	}

	public void setEnabled(boolean enabled)
	{
		this.enabled = enabled;
	}

	public void setPluginId(int pluginId)
	{
		this.pluginId = pluginId;
	}

	public void setPluginVersion(String pluginVersion)
	{
		this.pluginVersion = pluginVersion;
	}

	public void setPreviousPluginVersion(String previousPluginVersion)
	{
		this.previousPluginVersion = previousPluginVersion;
	}

	public void setServerId(int serverId)
	{
		this.serverId = serverId;
	}
}
