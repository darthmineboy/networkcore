package net.rieksen.networkcore.core.plugin.task;

import net.rieksen.networkcore.core.issue.IssueGenerator;
import net.rieksen.networkcore.core.plugin.PluginTaskExecutor;
import net.rieksen.networkcore.core.plugin.pojo.PluginTaskPojo;
import net.rieksen.networkcore.core.plugin.pojo.PluginTaskPojo.ExecutionStatus;

public class IssueGeneratorTaskExecutor implements PluginTaskExecutor
{

	private IssueGenerator generator;

	public IssueGeneratorTaskExecutor(IssueGenerator generator)
	{
		this.generator = generator;
	}

	@Override
	public ExecutionStatus execute(PluginTaskPojo task)
	{
		this.generator.generateIssues();
		return ExecutionStatus.SUCCESS;
	}
}
