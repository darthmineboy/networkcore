package net.rieksen.networkcore.core.plugin;

import net.rieksen.networkcore.core.plugin.pojo.PluginTaskPojo;

/**
 * @see PluginTaskExecutorExtra
 */
public interface PluginTaskExecutor
{

	PluginTaskPojo.ExecutionStatus execute(PluginTaskPojo task);
}
