package net.rieksen.networkcore.core.plugin;

public interface PluginTaskExecutorExtra extends PluginTaskExecutor
{

	public static final int MAX_SUMMARY_LENGTH = 8192;

	String getSummary();
}
