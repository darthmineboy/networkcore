package net.rieksen.networkcore.core.plugin.converter;

import java.util.Map;

public abstract class PluginDataConverter implements IPluginDataConverter
{

	protected boolean getBoolean(Map<PluginDataConverterOptions, String> options, PluginDataConverterOptions option)
	{
		return Boolean.parseBoolean(this.getString(options, option));
	}

	protected String getString(Map<PluginDataConverterOptions, String> options, PluginDataConverterOptions option)
	{
		return options == null ? option.getString() : options.getOrDefault(option, option.getString());
	}
}
