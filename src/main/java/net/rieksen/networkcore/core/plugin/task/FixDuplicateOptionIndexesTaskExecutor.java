package net.rieksen.networkcore.core.plugin.task;

import java.util.List;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.option.Option;
import net.rieksen.networkcore.core.option.OptionValue;
import net.rieksen.networkcore.core.option.pojo.OptionValuePojo;
import net.rieksen.networkcore.core.plugin.PluginTaskExecutor;
import net.rieksen.networkcore.core.plugin.pojo.PluginTaskPojo;
import net.rieksen.networkcore.core.plugin.pojo.PluginTaskPojo.ExecutionStatus;

public class FixDuplicateOptionIndexesTaskExecutor implements PluginTaskExecutor
{

	private NetworkCore provider;

	public FixDuplicateOptionIndexesTaskExecutor(NetworkCore provider)
	{
		this.provider = provider;
	}

	@Override
	public ExecutionStatus execute(PluginTaskPojo task)
	{
		List<OptionValuePojo> values = this.provider.getDAO().getOptionDAO().findOptionValuesWithDuplicateIndexes();
		if (!values.isEmpty())
		{
			this.provider.getLogger().info("Fixing duplicate option indexes");
		}
		for (OptionValuePojo value : values)
		{
			Option option = this.provider.getOptionModule().getOption(value.getOptionId());
			if (option.getValue(value.getServerId(), value.getIndex()).getValueId() == value.getValueId())
			{
				continue;
			}
			OptionValue optionValue = option.findValue(value.getValueId());
			optionValue.changeIndex(option.getHighestIndex(value.getServerId()) + 1);
		}
		return PluginTaskPojo.ExecutionStatus.SUCCESS;
	}
}
