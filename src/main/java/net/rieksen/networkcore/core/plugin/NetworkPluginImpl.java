package net.rieksen.networkcore.core.plugin;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.cache.CacheComponent;
import net.rieksen.networkcore.core.message.Language;
import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.message.MessageSection;
import net.rieksen.networkcore.core.message.pojo.MessageSectionPojo;
import net.rieksen.networkcore.core.option.Option;
import net.rieksen.networkcore.core.option.OptionFactory;
import net.rieksen.networkcore.core.option.OptionSection;
import net.rieksen.networkcore.core.option.pojo.OptionSectionPojo;
import net.rieksen.networkcore.core.plugin.converter.PluginDataConverterOptions;
import net.rieksen.networkcore.core.plugin.converter.yaml.YamlPluginDataConverter;
import net.rieksen.networkcore.core.plugin.pojo.NetworkPluginPojo;
import net.rieksen.networkcore.core.plugin.pojo.PluginTaskExecutionPojo;
import net.rieksen.networkcore.core.plugin.pojo.PluginTaskPojo;
import net.rieksen.networkcore.core.plugin.pojo.PluginTaskPojo.ExecutionStatus;
import net.rieksen.networkcore.core.socket.IServerSocketRequestHandler;
import net.rieksen.networkcore.core.socket.IServerSocketResponse;
import net.rieksen.networkcore.core.util.PluginUtil;
import net.rieksen.networkcore.spigot.plugin.NetworkPluginMessagesFile;
import net.rieksen.networkcore.spigot.plugin.NetworkPluginOptionsFile;

public class NetworkPluginImpl implements NetworkPlugin
{

	private final NetworkCore							provider;
	private final int									pluginId;
	private final String								name;
	CacheComponent<List<MessageSection>>				sectionCache	=
		new CacheComponent<List<MessageSection>>(TimeUnit.SECONDS.toMillis(300))
																			{

																				@Override
																				public void refreshCache()
																				{
																					List<MessageSection> sections = new ArrayList<>();
																					NetworkPluginImpl.this.provider.getDAO().getMessageDAO()
																						.findSections(NetworkPluginImpl.this.pluginId)
																						.forEach(pojo ->
																																							{
																																								sections
																																									.add(
																																										NetworkPluginImpl.this.provider
																																											.getMessageModule()
																																											.getFactory()
																																											.createSection(
																																												pojo));
																																							});
																					this.cachedObject = sections;
																				}
																			};
	CacheComponent<List<OptionSection>>					optionCache		=
		new CacheComponent<List<OptionSection>>(TimeUnit.SECONDS.toMillis(300))
																			{

																				@Override
																				public void refreshCache()
																				{
																					List<OptionSection> sections = new ArrayList<>();
																					NetworkPluginImpl.this.provider.getDAO().getOptionDAO()
																						.findSections(NetworkPluginImpl.this.pluginId)
																						.forEach(pojo ->
																																							{
																																								sections
																																									.add(
																																										OptionFactory
																																											.createSection(
																																												NetworkPluginImpl.this.provider,
																																												pojo));
																																							});
																					this.cachedObject = sections;
																				}
																			};
	// cache
	private boolean										isCached;
	private boolean										keepCached;
	private long										lastUpdate		= System.currentTimeMillis();
	private String										databaseVersion;
	private Integer										languageId;
	private boolean										licenseValid	= true;
	private Map<String, PluginTaskExecutor>				tasks			= new HashMap<>();
	private String										downloadedUpdateVersion;
	private Map<String, IServerSocketRequestHandler>	socketHandlers	= new ConcurrentHashMap<>();
	private NetworkPluginMessagesFile					messagesFile;
	private NetworkPluginOptionsFile					optionsFile;

	public NetworkPluginImpl(NetworkCore provider, NetworkPluginPojo pojo)
	{
		Validate.notNull(provider);
		Validate.notNull(pojo);
		Validate.notNull(pojo.getName());
		this.provider = provider;
		this.pluginId = pojo.getPluginId();
		this.name = pojo.getName();
		this.databaseVersion = pojo.getDatabaseVersion();
		this.languageId = pojo.getLanguageId();
	}

	/**
	 * @param pluginId
	 * @param name
	 * @throws IllegalStateException
	 *             when name is null
	 */
	protected NetworkPluginImpl(NetworkCore provider, int pluginId, String name, String databaseVersion, Integer languageId)
	{
		Validate.notNull(provider);
		Validate.notNull(name, "Name cannot be null");
		this.provider = provider;
		this.pluginId = pluginId;
		this.name = name;
		this.databaseVersion = databaseVersion;
		this.languageId = languageId;
	}

	@Override
	public void changeDatabaseVersion(String version)
	{
		NetworkPluginPojo pojo = this.toPojo();
		pojo.setDatabaseVersion(version);
		this.provider.getDAO().getPluginDAO().updatePlugin(pojo);
		this.databaseVersion = version;
	}

	@Override
	public MessageSection createMessageSection(String name, String description)
	{
		MessageSectionPojo pojo = new MessageSectionPojo(0, this.pluginId, name, description);
		pojo.setSectionId(this.provider.getDAO().getMessageDAO().createSection(pojo));
		MessageSection section = this.provider.getMessageModule().getFactory().createSection(pojo);
		this.sectionCache.getCache().add(section);
		return section;
	}

	@Override
	public OptionSection createOptionSection(String sectionName, String description)
	{
		OptionSectionPojo pojo = new OptionSectionPojo(0, this.pluginId, sectionName, description);
		pojo.setSectionId(this.provider.getDAO().getOptionDAO().createSection(pojo));
		OptionSection section = OptionFactory.createSection(this.provider, pojo);
		this.optionCache.getCache().add(section);
		return section;
	}

	@Override
	public void deleteMessageSection(String name)
	{
		MessageSection section = this.getMessageSection(name);
		if (section != null)
		{
			this.provider.getDAO().getMessageDAO().deleteSection(section.getSectionId());
			this.sectionCache.getCache().remove(section);
		}
	}

	@Override
	public void deleteOptionSection(String name)
	{
		OptionSection section = this.getOptionSection(name);
		if (section != null)
		{
			this.provider.getDAO().getOptionDAO().deleteSection(section.getSectionId());
			this.optionCache.getCache().remove(section);
		}
	}

	@Override
	public String getDatabaseVersion()
	{
		return this.databaseVersion;
	}

	@Override
	public String getDownloadedUpdateVersion()
	{
		return this.downloadedUpdateVersion;
	}

	@Override
	public Language getLanguage()
	{
		return this.provider.getMessageModule().getLanguage(this.languageId);
	}

	@Override
	public Integer getLanguageId()
	{
		return this.languageId;
	}

	@Override
	public String getLatestVersion()
	{
		List<ServerPlugin> plugins = this.getServerPlugins();
		String latestVersion = null;
		for (ServerPlugin plugin : plugins)
		{
			if (latestVersion == null)
			{
				latestVersion = plugin.getPluginVersion();
				continue;
			}
			if (PluginUtil.isVersionGreater(latestVersion, plugin.getPluginVersion()))
			{
				latestVersion = plugin.getPluginVersion();
			}
		}
		return latestVersion;
	}

	@Override
	public Logger getLogger()
	{
		Object platformPlugin = this.getPlatformPlugin();
		if (platformPlugin instanceof Plugin) { return ((Plugin) platformPlugin).getLogger(); }
		return this.provider.getLogger();
	}

	@Override
	public Message getMessage(int messageId)
	{
		Message message;
		for (MessageSection section : this.getMessageSections())
		{
			if ((message = section.getMessage(messageId)) != null) { return message; }
		}
		return null;
	}

	@Override
	public Message getMessage(String sectionName, String messageName)
	{
		MessageSection section = this.getMessageSection(sectionName);
		return section == null ? null : section.getMessage(messageName);
	}

	@Override
	public MessageSection getMessageSection(int sectionId)
	{
		return this.getMessageSections().stream().filter(section -> section.getSectionId() == sectionId).findAny().orElse(null);
	}

	@Override
	public MessageSection getMessageSection(String sectionName)
	{
		List<MessageSection> sections = this.sectionCache.getCache();
		return sections.stream().filter(section -> section.getName().equalsIgnoreCase(sectionName)).findAny().orElse(null);
	}

	@Override
	public List<MessageSection> getMessageSections()
	{
		return Collections.unmodifiableList(this.sectionCache.getCache());
	}

	@Override
	public NetworkPluginMessagesFile getMessagesFile()
	{
		if (this.messagesFile == null)
		{
			Object platformPlugin = this.getPlatformPlugin();
			if (platformPlugin instanceof JavaPlugin)
			{
				this.messagesFile = new NetworkPluginMessagesFile((JavaPlugin) platformPlugin, this);
			}
		}
		return this.messagesFile;
	}

	@Override
	public String getName()
	{
		return this.name;
	}

	@Override
	public Option getOption(String sectionName, String optionName)
	{
		OptionSection section = this.getOptionSection(sectionName);
		return section == null ? null : section.getOption(optionName);
	}

	@Override
	public OptionSection getOptionSection(String sectionName)
	{
		List<OptionSection> sections = this.optionCache.getCache();
		return sections.stream().filter(section -> section.getName().equalsIgnoreCase(sectionName)).findAny().orElse(null);
	}

	@Override
	public List<OptionSection> getOptionSections()
	{
		return Collections.unmodifiableList(this.optionCache.getCache());
	}

	@Override
	public NetworkPluginOptionsFile getOptionsFile()
	{
		if (this.optionsFile == null)
		{
			Object platformPlugin = this.getPlatformPlugin();
			if (platformPlugin instanceof JavaPlugin)
			{
				this.optionsFile = new NetworkPluginOptionsFile((JavaPlugin) platformPlugin, this);
			}
		}
		return this.optionsFile;
	}

	@Override
	public OptionSection getOrCreateSection(String sectionName, String description)
	{
		OptionSection section = this.getOptionSection(sectionName);
		if (section == null)
		{
			section = this.createOptionSection(sectionName, description);
		}
		return section;
	}

	@Override
	public Object getPlatformPlugin()
	{
		return Bukkit.getPluginManager().getPlugin(this.name);
	}

	@Override
	public int getPluginId()
	{
		return this.pluginId;
	}

	@Override
	public ServerPlugin getServerPlugin(int serverId)
	{
		return this.getServerPlugins().stream().filter(plugin -> plugin.getServerId() == serverId).findAny().orElse(null);
	}

	@Override
	public List<ServerPlugin> getServerPlugins()
	{
		return this.provider.getDAO().getPluginDAO().findServersWithPlugin(this.pluginId).stream()
			.map(plugin -> this.provider.getPluginModule().getFactory().createServerPlugin(plugin)).collect(Collectors.toList());
	}

	@Override
	public List<PluginTaskPojo> getTaskPojos()
	{
		// TODO cache
		return this.provider.getDAO().getPluginDAO().findPluginTasks(this.pluginId);
	}

	@Override
	public void importDefaults(InputStream input, String version, Map<PluginDataConverterOptions, String> options)
	{
		YamlPluginDataConverter converter = new YamlPluginDataConverter(this.provider);
		if (version != null)
		{
			ServerPlugin serverPlugin = this.getServerPlugin(this.provider.getServerModule().getLocalServer().getServerId());
			if (serverPlugin != null && StringUtils.equals(version, serverPlugin.getPreviousPluginVersion()))
			{
				this.provider.getTaskChainFactory().newChain().async(() -> converter.importFromInputStream(this, input, options)).execute();
				return;
			}
		}
		converter.importFromInputStream(this, input, options);
	}

	@Override
	public boolean isCached()
	{
		return this.isCached;
	}

	@Override
	public void isCached(boolean isCached)
	{
		this.isCached = isCached;
	}

	@Override
	public boolean isCacheExpired()
	{
		return System.currentTimeMillis() - this.lastUpdate > 300000L;
	}

	@Override
	public boolean isLicenseValid()
	{
		return this.licenseValid;
	}

	@Override
	public boolean keepCached()
	{
		return this.keepCached;
	}

	@Override
	public void keepCached(boolean keepCached)
	{
		this.keepCached = keepCached;
	}

	@Override
	public IServerSocketResponse processSocketMessage(String id, byte[] data)
	{
		IServerSocketRequestHandler handler = this.socketHandlers.get(id);
		return handler == null ? null : handler.process(data);
	}

	@Override
	public void registerSocketRequestHandler(String id, IServerSocketRequestHandler handler)
	{
		this.socketHandlers.put(id, handler);
	}

	@Override
	public void resetCacheExpiration()
	{
		this.lastUpdate = System.currentTimeMillis();
	}

	@Override
	public void runTask(PluginTaskPojo task)
	{
		Validate.notNull(task);
		PluginTaskExecutor exec = this.tasks.get(task.getTaskName());
		if (exec == null)
		{
			this.provider.getLogger().warning(
				"Could not execute task " + task.getTaskName() + " of plugin " + this.name + " because no task executor is registered");
			return;
		}
		PluginTaskPojo.ExecutionStatus status = null;
		String summary = null;
		long startTime = System.currentTimeMillis();
		try
		{
			status = exec.execute(task);
		} catch (Exception e)
		{
			status = ExecutionStatus.ERROR;
			this.provider.getLogger().log(Level.SEVERE,
				"An error occurred while executing task " + task.getTaskName() + " of plugin " + this.name, e);
		}
		if (exec instanceof PluginTaskExecutorExtra)
		{
			PluginTaskExecutorExtra extra = (PluginTaskExecutorExtra) exec;
			summary = extra.getSummary();
			if (summary != null && summary.length() > PluginTaskExecutorExtra.MAX_SUMMARY_LENGTH)
			{
				summary = summary.substring(0, PluginTaskExecutorExtra.MAX_SUMMARY_LENGTH);
			}
		}
		task.setExecutionStatus(status);
		Date executionDate = new Date();
		task.setLastRun(executionDate);
		task.setClaimedTill(null);
		task.setClaimedServerId(null);
		this.provider.getDAO().getPluginDAO().updatePluginTask(task);
		this.provider.getDAO().getPluginDAO()
			.createPluginTaskExecution(new PluginTaskExecutionPojo(task.getPluginId(), task.getTaskName(),
				this.provider.getServerModule().getLocalServer().getServerId(), executionDate, status, summary,
				System.currentTimeMillis() - startTime));
	}

	@Override
	public void setDownloadedUpdateVersion(String latestVersion)
	{
		this.downloadedUpdateVersion = latestVersion;
	}

	@Override
	public void setLicenseValid(boolean licenseValid)
	{
		this.licenseValid = licenseValid;
	}

	@Override
	public void setTaskExecutor(String taskName, PluginTaskExecutor exec)
	{
		Validate.notNull(taskName);
		this.tasks.put(taskName, exec);
	}

	@Override
	public void setupTask(String taskName, int interval, boolean global, PluginTaskExecutor exec)
	{
		PluginTaskPojo task = this.provider.getDAO().getPluginDAO().findPluginTask(this.pluginId, taskName);
		if (task == null)
		{
			this.provider.getDAO().getPluginDAO()
				.createPluginTask(new PluginTaskPojo(this.pluginId, taskName, interval,
					new Date(System.currentTimeMillis() - TimeUnit.SECONDS.toMillis(interval)), PluginTaskPojo.ExecutionStatus.SUCCESS,
					null, null, global));
		}
		this.setTaskExecutor(taskName, exec);
	}

	@Override
	public void setupTask(String taskName, int interval, PluginTaskExecutor exec)
	{
		this.setupTask(taskName, interval, true, exec);
	}

	@Override
	public String toString()
	{
		return "NetworkPlugin [pluginId=" + this.pluginId + ", name=" + this.name + "]";
	}

	private NetworkPluginPojo toPojo()
	{
		return new NetworkPluginPojo(this.pluginId, this.name, this.databaseVersion, this.languageId);
	}
}
