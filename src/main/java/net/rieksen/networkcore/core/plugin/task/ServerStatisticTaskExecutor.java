package net.rieksen.networkcore.core.plugin.task;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.plugin.PluginTaskExecutor;
import net.rieksen.networkcore.core.plugin.pojo.PluginTaskPojo;
import net.rieksen.networkcore.core.plugin.pojo.PluginTaskPojo.ExecutionStatus;
import net.rieksen.networkcore.core.server.Server;
import net.rieksen.networkcore.core.server.pojo.ServerResourceUsagePojo;
import net.rieksen.networkcore.core.server.pojo.ServerStatisticPojo;

public class ServerStatisticTaskExecutor implements PluginTaskExecutor
{

	private NetworkCore provider;

	public ServerStatisticTaskExecutor(NetworkCore provider)
	{
		this.provider = provider;
	}

	@Override
	public ExecutionStatus execute(PluginTaskPojo task)
	{
		Server server = this.provider.getServerModule().getLocalServer();
		ServerResourceUsagePojo resourceUsage = server.getLatestResourceUsage();
		if (resourceUsage == null)
		{
			// silence for now, will be optimized once resource usage is
			// redesigned
			return ExecutionStatus.SUCCESS;
		}
		this.provider.getDAO().getServerDAO()
			.createServerStatistic(new ServerStatisticPojo(server.getServerId(), resourceUsage.getCurrentDate(), server.getUserCount(),
				resourceUsage.getTps(), resourceUsage.getServerCpuUsage(), resourceUsage.getMemoryUsed(),
				resourceUsage.getMemoryAvailable(), resourceUsage.getMemoryTotal()));
		return ExecutionStatus.SUCCESS;
	}
}
