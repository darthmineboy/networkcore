package net.rieksen.networkcore.core.plugin.task;

import java.util.List;
import java.util.stream.Collectors;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.plugin.PluginTaskExecutor;
import net.rieksen.networkcore.core.plugin.PluginTaskExecutorExtra;
import net.rieksen.networkcore.core.plugin.pojo.PluginTaskPojo;
import net.rieksen.networkcore.core.plugin.pojo.PluginTaskPojo.ExecutionStatus;
import net.rieksen.networkcore.core.user.pojo.UserPojo;

public class CheckUserNameHistoryExecutor implements PluginTaskExecutor, PluginTaskExecutorExtra
{

	private NetworkCore	provider;
	private int			limit;
	private String		summary;

	/**
	 * @param provider
	 * @param limit
	 *            The maximum amount of users to check per execution
	 */
	public CheckUserNameHistoryExecutor(NetworkCore provider, int limit)
	{
		this.provider = provider;
		this.limit = limit;
	}

	@Override
	public ExecutionStatus execute(PluginTaskPojo task)
	{
		List<UserPojo> users = this.provider.getDAO().getUserDAO().findUsersWithCheckNameHistory(this.limit);
		this.provider.getUserModule().fromPojos(users).forEach(user -> user.checkNameHistoryNow());
		if (!users.isEmpty())
		{
			String userNames = users.stream().map(user -> user.getUserName()).collect(Collectors.joining(", "));
			this.summary = String.format("Checked history of: %s", userNames);
		}
		return ExecutionStatus.SUCCESS;
	}

	@Override
	public String getSummary()
	{
		return this.summary;
	}
}
