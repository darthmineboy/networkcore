package net.rieksen.networkcore.core.plugin;

import net.rieksen.networkcore.core.plugin.pojo.NetworkPluginPojo;

public interface PluginFactory
{

	NetworkPlugin createPlugin(int pluginId, String name, String databaseVersion, Integer languageId);

	NetworkPlugin createPlugin(NetworkPluginPojo pojo);

	ServerPlugin createServerPlugin(ServerPluginPojo serverPlugin);
}
