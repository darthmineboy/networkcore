package net.rieksen.networkcore.core.plugin;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.plugin.pojo.NetworkPluginPojo;

public class PluginFactoryImpl implements PluginFactory
{

	public static NetworkPlugin createPlugin(NetworkCore provider, int pluginId, String name, String databaseVersion, Integer languageId)
	{
		return new NetworkPluginImpl(provider, pluginId, name, databaseVersion, languageId);
	}

	public static NetworkPlugin createPlugin(NetworkCore provider, NetworkPluginPojo pojo)
	{
		return new NetworkPluginImpl(provider, pojo);
	}

	private NetworkCore provider;

	public PluginFactoryImpl(NetworkCore provider)
	{
		this.provider = provider;
	}

	@Override
	public NetworkPlugin createPlugin(int pluginId, String name, String databaseVersion, Integer languageId)
	{
		return new NetworkPluginImpl(this.provider, pluginId, name, databaseVersion, languageId);
	}

	@Override
	public NetworkPlugin createPlugin(NetworkPluginPojo pojo)
	{
		return new NetworkPluginImpl(this.provider, pojo);
	}

	@Override
	public ServerPlugin createServerPlugin(ServerPluginPojo serverPlugin)
	{
		return new ServerPluginImpl(this.provider, serverPlugin);
	}
}
