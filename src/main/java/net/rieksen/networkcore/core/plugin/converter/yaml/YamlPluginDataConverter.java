package net.rieksen.networkcore.core.plugin.converter.yaml;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.message.Language;
import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.message.MessageSection;
import net.rieksen.networkcore.core.message.MessageTranslation;
import net.rieksen.networkcore.core.message.MessageType;
import net.rieksen.networkcore.core.message.MessageVariable;
import net.rieksen.networkcore.core.message.pojo.LanguagePojo;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.core.plugin.converter.PluginDataConverter;
import net.rieksen.networkcore.core.plugin.converter.PluginDataConverterOptions;

public class YamlPluginDataConverter extends PluginDataConverter
{

	private NetworkCore provider;

	public YamlPluginDataConverter(NetworkCore provider)
	{
		this.provider = provider;
	}

	@Override
	public void exportToFile(NetworkPlugin plugin, File file, Map<PluginDataConverterOptions, String> options) throws IOException
	{
		YamlConfiguration yaml = new YamlConfiguration();
		this.exportOptions(yaml, options);
		this.exportMessageSections(yaml, plugin);
		yaml.save(file);
	}

	@Override
	public void importFromFile(NetworkPlugin plugin, File file, Map<PluginDataConverterOptions, String> options)
	{
		YamlConfiguration yaml = YamlConfiguration.loadConfiguration(file);
		this.importFromYamlConfiguration(plugin, yaml, options);
	}

	public void importFromInputStream(NetworkPlugin plugin, InputStream input, Map<PluginDataConverterOptions, String> options)
	{
		YamlConfiguration yaml = YamlConfiguration.loadConfiguration(new InputStreamReader(input));
		this.importFromYamlConfiguration(plugin, yaml, options);
	}

	public void importFromYamlConfiguration(NetworkPlugin plugin, YamlConfiguration yaml, Map<PluginDataConverterOptions, String> options)
	{
		if (options == null)
		{
			options = new HashMap<>();
		}
		ConfigurationSection importOptions = yaml.getConfigurationSection("options");
		for (String importOption : importOptions.getKeys(false))
		{
			PluginDataConverterOptions option = PluginDataConverterOptions.valueOf(importOption);
			options.put(option, importOptions.getString(importOption));
		}
		ConfigurationSection messageSections = yaml.getConfigurationSection("message-sections");
		for (String messageSectionName : messageSections.getKeys(false))
		{
			ConfigurationSection messageSection = messageSections.getConfigurationSection(messageSectionName);
			ConfigurationSection messages = messageSection.getConfigurationSection("messages");
			MessageSection messageSectionPojo = plugin.getMessageSection(messageSectionName);
			if (messageSectionPojo == null)
			{
				messageSectionPojo = plugin.createMessageSection(messageSectionName, messageSection.getString("description"));
			}
			if (this.getBoolean(options, PluginDataConverterOptions.UPDATE_MESSAGE_SECTION_DESCRIPTION))
			{
				messageSectionPojo.changeDescription(messageSection.getString("description"));
			}
			for (String messageName : messages.getKeys(false))
			{
				ConfigurationSection message = messages.getConfigurationSection(messageName);
				Message messagePojo = messageSectionPojo.getMessage(messageName);
				if (messagePojo == null)
				{
					messagePojo = messageSectionPojo.createMessage(messageName, message.getString("description"));
				}
				if (this.getBoolean(options, PluginDataConverterOptions.UPDATE_MESSAGE_DESCRIPTION))
				{
					messagePojo.changeDescription(message.getString("description"));
				}
				ConfigurationSection translations = message.getConfigurationSection("translations");
				if (translations != null)
				{
					for (String translation : translations.getKeys(false))
					{
						Language translationLanguage = this.provider.getMessageModule().getLanguage(translation);
						if (translationLanguage == null)
						{
							translationLanguage = this.provider.getMessageModule().createLanguage(new LanguagePojo(0, translation));
						}
						MessageTranslation translationPojo = messagePojo.getTranslation(translationLanguage.getLanguageId());
						MessageType type = MessageType.REGULAR;
						String typeString = translations.getString(translation + ".type");
						try
						{
							type = MessageType.valueOf(typeString);
						} catch (IllegalArgumentException | NullPointerException e)
						{
							this.provider.getLogger().warning(String.format("Invalid MessageType '%s', using default '%s' instead",
								typeString, MessageType.REGULAR.name()));
						}
						if (translationPojo == null)
						{
							messagePojo.createTranslation(translationLanguage.getLanguageId(),
								translations.getString(translation + ".message"), type);
						} else
						{
							if (this.getBoolean(options, PluginDataConverterOptions.UPDATE_MESSAGE_TRANSLATION))
							{
								translationPojo.changeMessage(translations.getString(translation + ".message"));
								translationPojo.changeMessageType(type);
							}
							if (this.getBoolean(options, PluginDataConverterOptions.UPDATE_MESSAGE_TRANSLATION_DEFAULTS))
							{
								translationPojo.changeDefaultMessage(translations.getString(translation + ".message"));
								translationPojo.changeDefaultMessageType(type);
							}
						}
					}
				}
				String defaultLanguage = this.getString(options, PluginDataConverterOptions.DEFAULT_LANGUAGE);
				if (defaultLanguage != null)
				{
					Language language = this.provider.getMessageModule().getLanguage(defaultLanguage);
					if (language == null)
					{
						this.provider.getLogger()
							.warning(String.format("Default language '%s', specified during import, does not exists", defaultLanguage));
					} else
					{
						MessageTranslation translation = messagePojo.getTranslation(language.getLanguageId());
						if (translation == null)
						{
							this.provider.getLogger()
								.warning(String.format("Missing '%s' default translation for message '%s'", defaultLanguage, messageName));
						} else
						{
							MessageTranslation defaultTranslation = messagePojo.getDefaultTranslation();
							if (defaultTranslation == null)
							{
								messagePojo.createDefaultTranslation(translation.getDefaultMessage(), translation.getDefaultMessageType());
							} else
							{
								defaultTranslation.changeDefaultMessage(translation.getDefaultMessage());
								defaultTranslation.changeDefaultMessageType(translation.getDefaultMessageType());
							}
						}
					}
				}
				ConfigurationSection variables = message.getConfigurationSection("variables");
				if (variables != null)
				{
					for (String variable : variables.getKeys(false))
					{
						MessageVariable variablePojo = messagePojo.getVariable(variable);
						String variableDescription = variables.getString(variable);
						if (variablePojo == null)
						{
							messagePojo.createVariable(variable, variableDescription);
						} else
						{
							if (this.getBoolean(options, PluginDataConverterOptions.UPDATE_MESSAGE_VARIABLE_DESCRIPTION))
							{
								variablePojo.changeDescription(variableDescription);
							}
						}
					}
				}
			}
		} ;
	}

	private void exportMessageSections(YamlConfiguration yaml, NetworkPlugin plugin)
	{
		ConfigurationSection messageSections = yaml.createSection("message-sections");
		plugin.getMessageSections().forEach(section ->
		{
			ConfigurationSection messageSection = messageSections.createSection(section.getName());
			messageSection.set("description", section.getDescription());
			ConfigurationSection messages = messageSection.createSection("messages");
			section.getMessages().forEach(messagePojo ->
			{
				ConfigurationSection message = messages.createSection(messagePojo.getName());
				message.set("description", messagePojo.getDescription());
				ConfigurationSection variables = message.createSection("variables");
				messagePojo.getVariables().forEach(variablePojo ->
				{
					variables.set(variablePojo.getName(), variablePojo.getDescription());
				});
				ConfigurationSection translations = message.createSection("translations");
				messagePojo.getTranslations().forEach(translationPojo ->
				{
					ConfigurationSection translation = translations.createSection(translationPojo.getLanguage().getName());
					translation.set("type", translationPojo.getMessageType().name());
					translation.set("message", translationPojo.getMessage());
				});
			});
		});
	}

	private void exportOptions(YamlConfiguration yaml, Map<PluginDataConverterOptions, String> options)
	{
		options = options == null ? new HashMap<>() : options;
		ConfigurationSection exportOptions = yaml.createSection("options");
		for (PluginDataConverterOptions option : PluginDataConverterOptions.values())
		{
			options.putIfAbsent(option, option.getString());
		}
		for (Entry<PluginDataConverterOptions, String> option : options.entrySet())
		{
			exportOptions.set(option.getKey().name(), option.getValue());
		}
	}
}
