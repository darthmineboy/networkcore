package net.rieksen.networkcore.core.plugin;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.Validate;

@Deprecated
public class UpdateInfo implements IUpdateInfo
{

	private String				version;
	private String				description;
	private String				infoURL;
	private String				downloadURL;
	private Map<String, String>	tags	= new HashMap<>();

	/**
	 * @param version
	 * @param description
	 * @param infoURL
	 * @param downloadURL
	 * @throws IllegalStateException
	 *             when version is null
	 */
	public UpdateInfo(String version, String description, String infoURL, String downloadURL)
	{
		Validate.notNull(version, "version cannot be null");
		this.version = version;
		this.description = description;
		this.infoURL = infoURL;
		this.downloadURL = downloadURL;
	}

	/* 
	 */
	@Override
	public void addTag(String tag, String value)
	{
		Validate.notNull(tag, "tag cannot be null");
		this.tags.put(tag, value);
	}

	/* 
	 */
	@Override
	public String getDescription()
	{
		return this.description;
	}

	/* 
	 */
	@Override
	public String getDownloadURL()
	{
		return this.downloadURL;
	}

	/* 
	 */
	@Override
	public String getInfoURL()
	{
		return this.infoURL;
	}

	/* 
	 */
	@Override
	public String getTag(String tag)
	{
		return this.tags.get(tag);
	}

	/* 
	 */
	@Override
	public Map<String, String> getTags()
	{
		return new HashMap<>(this.tags);
	}

	/* 
	 */
	@Override
	public String getVersion()
	{
		return this.version;
	}

	/* 
	 */
	@Override
	public boolean hasTag(String tag)
	{
		return this.tags.containsKey(tag);
	}
}
