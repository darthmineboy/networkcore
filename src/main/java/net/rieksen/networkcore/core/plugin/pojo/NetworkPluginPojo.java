package net.rieksen.networkcore.core.plugin.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class NetworkPluginPojo
{

	private int		pluginId;
	private String	pluginName;
	private String	databaseVersion;
	private Integer	languageId;

	public NetworkPluginPojo()
	{}

	public NetworkPluginPojo(int pluginId, String name, String databaseVersion, Integer languageId)
	{
		this.pluginId = pluginId;
		this.pluginName = name;
		this.databaseVersion = databaseVersion;
		this.languageId = languageId;
	}

	public String getDatabaseVersion()
	{
		return this.databaseVersion;
	}

	public Integer getLanguageId()
	{
		return this.languageId;
	}

	public String getName()
	{
		return this.pluginName;
	}

	public int getPluginId()
	{
		return this.pluginId;
	}

	public String getPluginName()
	{
		return this.pluginName;
	}

	public void setDatabaseVersion(String databaseVersion)
	{
		this.databaseVersion = databaseVersion;
	}

	public void setLanguageId(Integer languageId)
	{
		this.languageId = languageId;
	}

	public void setName(String name)
	{
		this.pluginName = name;
	}

	public void setPluginId(int pluginId)
	{
		this.pluginId = pluginId;
	}

	public void setPluginName(String pluginName)
	{
		this.pluginName = pluginName;
	}
}
