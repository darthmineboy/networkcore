package net.rieksen.networkcore.core.plugin.pojo;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import net.rieksen.networkcore.core.plugin.pojo.PluginTaskPojo.ExecutionStatus;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PluginTaskExecutionPojo
{

	private int				pluginId;
	private String			taskName;
	private int				serverId;
	private Date			executionDate;
	private ExecutionStatus	executionStatus;
	private String			summary;
	private long			duration;

	@Deprecated
	public PluginTaskExecutionPojo(int pluginId, String taskName, int serverId, Date executionDate, ExecutionStatus executionStatus)
	{
		this.pluginId = pluginId;
		this.taskName = taskName;
		this.serverId = serverId;
		this.executionDate = executionDate;
		this.executionStatus = executionStatus;
	}

	@Deprecated
	public PluginTaskExecutionPojo(int pluginId, String taskName, int serverId, Date executionDate, ExecutionStatus executionStatus,
		String summary)
	{
		this.pluginId = pluginId;
		this.taskName = taskName;
		this.serverId = serverId;
		this.executionDate = executionDate;
		this.executionStatus = executionStatus;
		this.summary = summary;
	}

	public PluginTaskExecutionPojo(int pluginId, String taskName, int serverId, Date executionDate, ExecutionStatus executionStatus,
		String summary, long duration)
	{
		this.pluginId = pluginId;
		this.taskName = taskName;
		this.serverId = serverId;
		this.executionDate = executionDate;
		this.executionStatus = executionStatus;
		this.summary = summary;
		this.duration = duration;
	}

	public long getDuration()
	{
		return this.duration;
	}

	public Date getExecutionDate()
	{
		return this.executionDate;
	}

	public ExecutionStatus getExecutionStatus()
	{
		return this.executionStatus;
	}

	public int getPluginId()
	{
		return this.pluginId;
	}

	public int getServerId()
	{
		return this.serverId;
	}

	public String getSummary()
	{
		return this.summary;
	}

	public String getTaskName()
	{
		return this.taskName;
	}

	public void setDuration(long duration)
	{
		this.duration = duration;
	}

	public void setExecutionDate(Date executionDate)
	{
		this.executionDate = executionDate;
	}

	public void setExecutionStatus(ExecutionStatus executionStatus)
	{
		this.executionStatus = executionStatus;
	}

	public void setPluginId(int pluginId)
	{
		this.pluginId = pluginId;
	}

	public void setServerId(int serverId)
	{
		this.serverId = serverId;
	}

	public void setSummary(String summary)
	{
		this.summary = summary;
	}

	public void setTaskName(String taskName)
	{
		this.taskName = taskName;
	}
}
