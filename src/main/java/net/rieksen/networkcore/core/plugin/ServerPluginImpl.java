package net.rieksen.networkcore.core.plugin;

import org.apache.commons.lang.StringUtils;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.server.Server;

public class ServerPluginImpl implements ServerPlugin
{

	private NetworkCore	provider;
	private int			serverId;
	private int			pluginId;
	private String		pluginVersion;
	private boolean		enabled;
	private String		previousPluginVersion;

	public ServerPluginImpl(NetworkCore provider, ServerPluginPojo pojo)
	{
		this.provider = provider;
		this.setPojo(pojo);
	}

	@Override
	public NetworkPlugin getPlugin()
	{
		return this.provider.getPluginModule().getPlugin(this.pluginId);
	}

	@Override
	public int getPluginId()
	{
		return this.pluginId;
	}

	@Override
	public String getPluginVersion()
	{
		return this.pluginVersion;
	}

	@Override
	public String getPreviousPluginVersion()
	{
		return this.previousPluginVersion;
	}

	@Override
	public Server getServer()
	{
		return this.provider.getServerModule().getServer(this.serverId);
	}

	@Override
	public int getServerId()
	{
		return this.serverId;
	}

	@Override
	public boolean isEnabled()
	{
		return this.enabled;
	}

	@Override
	public void updatePreviousPluginVersion()
	{
		if (StringUtils.equals(this.pluginVersion, this.previousPluginVersion)) { return; }
		ServerPluginPojo pojo = this.toPojo();
		pojo.setPreviousPluginVersion(this.pluginVersion);
		this.provider.getDAO().getPluginDAO().updateServerPlugin(pojo);
		this.setPojo(pojo);
	}

	private void setPojo(ServerPluginPojo pojo)
	{
		this.serverId = pojo.getServerId();
		this.pluginId = pojo.getPluginId();
		this.pluginVersion = pojo.getPluginVersion();
		this.enabled = pojo.isEnabled();
		this.previousPluginVersion = pojo.getPreviousPluginVersion();
	}

	private ServerPluginPojo toPojo()
	{
		return new ServerPluginPojo(this.serverId, this.pluginId, this.pluginVersion, this.enabled, this.previousPluginVersion);
	}
}
