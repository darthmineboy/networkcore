package net.rieksen.networkcore.core.plugin.pojo;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PluginTaskPojo
{

	private int								pluginId;
	private String							taskName;
	private int								taskInterval;
	private Date							lastRun;
	private ExecutionStatus					executionStatus;
	/**
	 * The date till other servers should not try to execute this task. This is
	 * used to prevent servers from executing the same task, where the first
	 * server sets the claimedTill and other servers should respect claims till
	 * expired.
	 */
	private Date							claimedTill;
	/**
	 * The server that claimed the task.
	 */
	private Integer							claimedServerId;
	private boolean							global;
	@SuppressWarnings("unused")
	private List<PluginTaskExecutionPojo>	executions;

	public PluginTaskPojo()
	{}

	@Deprecated
	public PluginTaskPojo(int pluginId, String taskName, int taskInterval, Date lastRun)
	{
		this.pluginId = pluginId;
		this.taskName = taskName;
		this.taskInterval = taskInterval;
		this.lastRun = lastRun;
	}

	@Deprecated
	public PluginTaskPojo(int pluginId, String taskName, int taskInterval, Date lastRun, ExecutionStatus executionStatus)
	{
		this.pluginId = pluginId;
		this.taskName = taskName;
		this.taskInterval = taskInterval;
		this.lastRun = lastRun;
		this.executionStatus = executionStatus;
	}

	@Deprecated
	public PluginTaskPojo(int pluginId, String taskName, int taskInterval, Date lastRun, ExecutionStatus executionStatus, Date claimedTill,
		Integer claimedServerId)
	{
		this.pluginId = pluginId;
		this.taskName = taskName;
		this.taskInterval = taskInterval;
		this.lastRun = lastRun;
		this.executionStatus = executionStatus;
		this.claimedTill = claimedTill;
		this.claimedServerId = claimedServerId;
	}

	public PluginTaskPojo(int pluginId, String taskName, int taskInterval, Date lastRun, ExecutionStatus executionStatus, Date claimedTill,
		Integer claimedServerId, boolean global)
	{
		this.pluginId = pluginId;
		this.taskName = taskName;
		this.taskInterval = taskInterval;
		this.lastRun = lastRun;
		this.executionStatus = executionStatus;
		this.claimedTill = claimedTill;
		this.claimedServerId = claimedServerId;
		this.global = global;
	}

	public Integer getClaimedServerId()
	{
		return this.claimedServerId;
	}

	public Date getClaimedTill()
	{
		return this.claimedTill;
	}

	public ExecutionStatus getExecutionStatus()
	{
		return this.executionStatus;
	}

	public Date getLastRun()
	{
		return this.lastRun;
	}

	public int getPluginId()
	{
		return this.pluginId;
	}

	public int getTaskInterval()
	{
		return this.taskInterval;
	}

	public String getTaskName()
	{
		return this.taskName;
	}

	/**
	 * If the task is global, the task is executed once per interval regardless
	 * of the amount of servers. If the task is not global, then the task is
	 * executed for each server individually.
	 * 
	 * @return
	 */
	public boolean isGlobal()
	{
		return this.global;
	}

	public void setClaimedServerId(Integer claimedServerId)
	{
		this.claimedServerId = claimedServerId;
	}

	public void setClaimedTill(Date claimedTill)
	{
		this.claimedTill = claimedTill;
	}

	public void setExecutions(List<PluginTaskExecutionPojo> executions)
	{
		this.executions = executions;
	}

	public void setExecutionStatus(ExecutionStatus executionStatus)
	{
		this.executionStatus = executionStatus;
	}

	public void setGlobal(boolean global)
	{
		this.global = global;
	}

	public void setLastRun(Date lastRun)
	{
		this.lastRun = lastRun;
	}

	public void setPluginId(int pluginId)
	{
		this.pluginId = pluginId;
	}

	public void setTaskInterval(int taskInterval)
	{
		this.taskInterval = taskInterval;
	}

	public void setTaskName(String taskName)
	{
		this.taskName = taskName;
	}

	public static enum ExecutionStatus
	{
		SUCCESS('S'),
		ERROR('E'),
		WARNING('W');

		public static ExecutionStatus fromId(char id)
		{
			for (ExecutionStatus status : ExecutionStatus.values())
			{
				if (status.id == id) { return status; }
			}
			return null;
		}

		private char id;

		private ExecutionStatus(char id)
		{
			this.id = id;
		}

		public char getId()
		{
			return this.id;
		}
	}
}
