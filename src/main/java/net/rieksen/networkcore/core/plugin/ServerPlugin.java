package net.rieksen.networkcore.core.plugin;

import net.rieksen.networkcore.core.server.Server;

public interface ServerPlugin
{

	NetworkPlugin getPlugin();

	int getPluginId();

	String getPluginVersion();

	/**
	 * Version of the previous runtime. May be null.
	 * 
	 * @return
	 */
	String getPreviousPluginVersion();

	Server getServer();

	int getServerId();

	boolean isEnabled();

	/**
	 * Sets {@link #getPreviousPluginVersion()} to {@link #getPluginVersion()}.
	 */
	void updatePreviousPluginVersion();
}
