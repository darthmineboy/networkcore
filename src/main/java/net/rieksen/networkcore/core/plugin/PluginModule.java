package net.rieksen.networkcore.core.plugin;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.bukkit.plugin.Plugin;

import net.rieksen.networkcore.core.module.Module;

public interface PluginModule extends Module
{

	public static final long PLUGIN_TASK_CLAIM_MS = TimeUnit.MINUTES.toMillis(5);

	/**
	 * Creates a new plugin, and sets the {@link pluginId}.
	 * 
	 * @param name
	 * @return
	 */
	NetworkPlugin createPlugin(String name);

	PluginFactory getFactory();

	NetworkPlugin getPlugin(int pluginId);

	NetworkPlugin getPlugin(Plugin plugin);

	NetworkPlugin getPlugin(String name);

	List<NetworkPlugin> getPlugins();

	List<NetworkPlugin> getPlugins(int startIndex, int count);

	void setPluginServerEnabled(Plugin plugin, int serverId, boolean enabled);

	void updatePreviousPluginVersions();
}