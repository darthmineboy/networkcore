package net.rieksen.networkcore.core.plugin.converter;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import net.rieksen.networkcore.core.plugin.NetworkPlugin;

/**
 * Used to export messages and options to a file, which can then be easily
 * edited by admins. When done the admin can synchronize the plugin with the
 * file to apply all changes made.
 */
public interface IPluginDataConverter
{

	/**
	 * Exports all the plugin data (messages, options) to a file.
	 * 
	 * @param plugin
	 *            the plugin to export to file
	 * @param file
	 *            the file to export to
	 * @throws IOException
	 */
	void exportToFile(NetworkPlugin plugin, File file, Map<PluginDataConverterOptions, String> options) throws IOException;

	/**
	 * Imports all the plugin data (messages, options) from a while and sets it
	 * in the plugin.
	 * 
	 * @param plugin
	 * @param file
	 */
	void importFromFile(NetworkPlugin plugin, File file, Map<PluginDataConverterOptions, String> options);
}
