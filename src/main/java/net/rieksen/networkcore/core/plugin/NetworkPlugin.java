package net.rieksen.networkcore.core.plugin;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import net.rieksen.networkcore.core.NetworkCoreAPI;
import net.rieksen.networkcore.core.cache.ICacheable;
import net.rieksen.networkcore.core.message.Language;
import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.message.MessageSection;
import net.rieksen.networkcore.core.option.Option;
import net.rieksen.networkcore.core.option.OptionSection;
import net.rieksen.networkcore.core.plugin.converter.PluginDataConverterOptions;
import net.rieksen.networkcore.core.plugin.converter.yaml.YamlPluginDataConverter;
import net.rieksen.networkcore.core.plugin.pojo.PluginTaskPojo;
import net.rieksen.networkcore.core.socket.IServerSocketRequestHandler;
import net.rieksen.networkcore.core.socket.IServerSocketResponse;
import net.rieksen.networkcore.spigot.plugin.NetworkPluginMessagesFile;
import net.rieksen.networkcore.spigot.plugin.NetworkPluginOptionsFile;

public interface NetworkPlugin extends ICacheable
{

	/**
	 * Get a plugin.
	 * 
	 * @param pluginId
	 * @return
	 */
	public static NetworkPlugin getPlugin(int pluginId)
	{
		return NetworkCoreAPI.getPluginModule().getPlugin(pluginId);
	}

	/**
	 * Get a plugin.
	 * 
	 * @param name
	 * @return
	 */
	public static NetworkPlugin getPlugin(String name)
	{
		return NetworkCoreAPI.getPluginModule().getPlugin(name);
	}

	void changeDatabaseVersion(String version);

	MessageSection createMessageSection(String name, String description);

	OptionSection createOptionSection(String sectionName, String description);

	void deleteMessageSection(String name);

	void deleteOptionSection(String name);

	String getDatabaseVersion();

	String getDownloadedUpdateVersion();

	/**
	 * The default language of the messages of this plugin.
	 * 
	 * @return
	 */
	Language getLanguage();

	/**
	 * The default language of the messages of this plugin.
	 * 
	 * @return
	 */
	Integer getLanguageId();

	String getLatestVersion();

	Logger getLogger();

	/**
	 * @param messageId
	 * @return null if it does not exist, or does not belong to this plugin
	 */
	Message getMessage(int messageId);

	/**
	 * Get a message in a section. This method does not throw a
	 * {@link NullPointerException} when the section does not exist.
	 * 
	 * @param sectionName
	 * @param messageName
	 * @return
	 */
	Message getMessage(String sectionName, String messageName);

	MessageSection getMessageSection(int sectionId);

	MessageSection getMessageSection(String sectionName);

	List<MessageSection> getMessageSections();

	NetworkPluginMessagesFile getMessagesFile();

	/**
	 * Get the name.
	 * 
	 * @return
	 */
	String getName();

	/**
	 * Get an option in a section. This may return null if the section or option
	 * does not exist.
	 * 
	 * @param sectionName
	 * @param optionName
	 * @return
	 */
	Option getOption(String sectionName, String optionName);

	OptionSection getOptionSection(String sectionName);

	List<OptionSection> getOptionSections();

	NetworkPluginOptionsFile getOptionsFile();

	/**
	 * Get a {@link OptionSection} by its name, or create if it does not exist.
	 * 
	 * @param sectionName
	 * @param description
	 * @return
	 */
	OptionSection getOrCreateSection(String sectionName, String description);

	Object getPlatformPlugin();

	/**
	 * Get the pluginId. Can return null.
	 * 
	 * @return
	 */
	int getPluginId();

	ServerPlugin getServerPlugin(int serverId);

	List<ServerPlugin> getServerPlugins();

	List<PluginTaskPojo> getTaskPojos();

	/**
	 * Imports defaults with {@link YamlPluginDataConverter}. If the current
	 * version of the plugin equals the version in the database, then imports
	 * are done async. If the version is different (after plugin update) the
	 * imports are done sync.
	 * 
	 * @param input
	 * @param version
	 * @param options
	 */
	void importDefaults(InputStream input, String version, Map<PluginDataConverterOptions, String> options);

	boolean isLicenseValid();

	IServerSocketResponse processSocketMessage(String id, byte[] data);

	void registerSocketRequestHandler(String id, IServerSocketRequestHandler handler);

	void runTask(PluginTaskPojo task);

	void setDownloadedUpdateVersion(String latestVersion);

	void setLicenseValid(boolean licenseValid);

	void setTaskExecutor(String taskName, PluginTaskExecutor exec);

	/**
	 * @param taskName
	 * @param interval
	 *            seconds
	 * @param global
	 * @param exec
	 */
	void setupTask(String taskName, int interval, boolean global, PluginTaskExecutor exec);

	/**
	 * Convenience method for
	 * {@link #setupTask(String, int, boolean, PluginTaskExecutor)} with
	 * <code>global = true</code>.
	 * 
	 * @param taskName
	 * @param interval
	 *            seconds
	 * @param exec
	 */
	void setupTask(String taskName, int interval, PluginTaskExecutor exec);
}