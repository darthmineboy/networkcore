package net.rieksen.networkcore.core.plugin.converter;

public enum PluginDataConverterOptions
{
	DEFAULT_LANGUAGE(null),
	UPDATE_MESSAGE_SECTION_DESCRIPTION("true"),
	UPDATE_MESSAGE_DESCRIPTION("true"),
	UPDATE_MESSAGE_VARIABLE_DESCRIPTION("true"),
	UPDATE_MESSAGE_TRANSLATION("false"),
	UPDATE_MESSAGE_TRANSLATION_DEFAULTS("false"),
	UPDATE_OPTION_SECTION_DESCRIPTION("true"),
	UPDATE_OPTION_DESCRIPTION("true"),
	UPDATE_OPTION_VALUE("false"),
	UPDATE_MESSAGE_LANGUAGE("false");

	private final String defaultValue;

	private PluginDataConverterOptions(String defaultValue)
	{
		this.defaultValue = defaultValue;
	}

	public boolean getBoolean()
	{
		return Boolean.parseBoolean(this.defaultValue);
	}

	public String getString()
	{
		return this.defaultValue;
	}

	@Override
	public String toString()
	{
		return this.name().toLowerCase().replace("_", "-");
	}
}
