package net.rieksen.networkcore.core.plugin;

import java.util.logging.Logger;

public interface PlatformPlugin
{

	Logger getLogger();
}
