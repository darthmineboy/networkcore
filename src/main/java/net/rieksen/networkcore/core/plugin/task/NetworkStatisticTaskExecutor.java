package net.rieksen.networkcore.core.plugin.task;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.plugin.PluginTaskExecutor;
import net.rieksen.networkcore.core.plugin.pojo.PluginTaskPojo;
import net.rieksen.networkcore.core.plugin.pojo.PluginTaskPojo.ExecutionStatus;
import net.rieksen.networkcore.core.server.Server;
import net.rieksen.networkcore.core.server.pojo.ServerResourceUsagePojo;
import net.rieksen.networkcore.core.statistic.NetworkStatisticPojo;

public class NetworkStatisticTaskExecutor implements PluginTaskExecutor
{

	private NetworkCore provider;

	public NetworkStatisticTaskExecutor(NetworkCore provider)
	{
		this.provider = provider;
	}

	@Override
	public ExecutionStatus execute(PluginTaskPojo task)
	{
		Date currentDate = new Date();
		int onlinePlayers = this.provider.getDAO().getUserDAO().findOnlineUserCount(currentDate);
		float cpuUsage = 0;
		long memoryUsage = 0;
		List<Server> onlineServers =
			this.provider.getServerModule().getServers().stream().filter(server -> server.isOnline()).collect(Collectors.toList());
		int onlineServersCount = onlineServers.size();
		for (Server server : onlineServers)
		{
			ServerResourceUsagePojo resource = server.getLatestResourceUsage();
			if (resource == null)
			{
				continue;
			}
			cpuUsage += resource.getServerCpuUsage();
			memoryUsage += resource.getMemoryUsed();
		}
		this.provider.getDAO().getStatisticDAO()
			.createNetworkStatistic(new NetworkStatisticPojo(currentDate, onlinePlayers, onlineServersCount, cpuUsage, memoryUsage));
		return PluginTaskPojo.ExecutionStatus.SUCCESS;
	}
}
