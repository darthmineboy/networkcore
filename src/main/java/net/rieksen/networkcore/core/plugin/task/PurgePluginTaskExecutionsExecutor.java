package net.rieksen.networkcore.core.plugin.task;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.core.plugin.PluginTaskExecutor;
import net.rieksen.networkcore.core.plugin.PluginTaskExecutorExtra;
import net.rieksen.networkcore.core.plugin.pojo.PluginTaskPojo;
import net.rieksen.networkcore.core.plugin.pojo.PluginTaskPojo.ExecutionStatus;

public class PurgePluginTaskExecutionsExecutor implements PluginTaskExecutor, PluginTaskExecutorExtra
{

	private NetworkCore	provider;
	private String		summary;

	public PurgePluginTaskExecutionsExecutor(NetworkCore provider)
	{
		this.provider = provider;
	}

	@Override
	public ExecutionStatus execute(PluginTaskPojo t)
	{
		int amount = this.provider.getNetworkPlugin().getOptionSection("Purge").getOption("plugin-task-executions").getInteger(null);
		List<String> summary = new ArrayList<>();
		for (NetworkPlugin plugin : this.provider.getPluginModule().getPlugins())
		{
			for (PluginTaskPojo task : plugin.getTaskPojos())
			{
				int deleted = this.provider.getDAO().getPluginDAO().deleteOldestPluginTaskExecutionsTill(task.getPluginId(),
					task.getTaskName(), amount);
				if (deleted > 0)
				{
					summary.add(
						String.format("Deleted %s execution(s) of task %s of plugin %s", deleted, task.getTaskName(), plugin.getName()));
				}
			}
		}
		if (summary.isEmpty())
		{
			summary.add("Deleted 0 executions");
		}
		this.summary = summary.stream().collect(Collectors.joining("\n"));
		return PluginTaskPojo.ExecutionStatus.SUCCESS;
	}

	@Override
	public String getSummary()
	{
		return this.summary;
	}
}
