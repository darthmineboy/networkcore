package net.rieksen.networkcore.core.plugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.Validate;
import org.bukkit.plugin.Plugin;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.cache.CacheCleanupThread;
import net.rieksen.networkcore.core.module.ModuleImpl;
import net.rieksen.networkcore.core.plugin.pojo.NetworkPluginPojo;

public class PluginModuleImpl extends ModuleImpl implements PluginModule
{

	private CacheCleanupThread<NetworkPlugin>	pluginCacheThread;
	private Collection<NetworkPlugin>			plugins	= Collections.synchronizedCollection(new HashSet<NetworkPlugin>());
	private NetworkCore							provider;
	private PluginFactory						factory;

	public PluginModuleImpl(NetworkCore provider)
	{
		this.provider = provider;
		this.factory = new PluginFactoryImpl(provider);
	}

	@Override
	public NetworkPlugin createPlugin(String name)
	{
		NetworkPluginPojo pojo = new NetworkPluginPojo(0, name, null, null);
		pojo.setPluginId(this.provider.getDAO().getPluginDAO().createPlugin(pojo));
		NetworkPlugin plugin = PluginFactoryImpl.createPlugin(this.provider, pojo);
		this.putInCache(plugin);
		return plugin;
	}

	@Override
	public PluginFactory getFactory()
	{
		return this.factory;
	}

	@Override
	public NetworkPlugin getPlugin(int pluginId)
	{
		NetworkPlugin plugin = this.findInCache(pluginId);
		if (plugin != null) { return plugin; }
		NetworkPluginPojo pojo = this.provider.getDAO().getPluginDAO().findPlugin(pluginId);
		if (pojo == null) { return null; }
		plugin = PluginFactoryImpl.createPlugin(this.provider, pojo);
		if (plugin != null)
		{
			this.putInCache(plugin);
		}
		return plugin;
	}

	@Override
	public NetworkPlugin getPlugin(Plugin plugin)
	{
		NetworkPlugin networkPlugin = this.getPlugin(plugin.getName());
		if (networkPlugin != null)
		{
			networkPlugin.keepCached(true);
			return networkPlugin;
		}
		networkPlugin = this.createPlugin(plugin.getName());
		networkPlugin.keepCached(true);
		return networkPlugin;
	}

	@Override
	public NetworkPlugin getPlugin(String name)
	{
		NetworkPlugin plugin = this.findInCache(name);
		if (plugin != null) { return plugin; }
		NetworkPluginPojo pojo = this.provider.getDAO().getPluginDAO().findPlugin(name);
		if (pojo == null) { return null; }
		plugin = PluginFactoryImpl.createPlugin(this.provider, pojo);
		if (plugin != null)
		{
			this.putInCache(plugin);
		}
		return plugin;
	}

	@Override
	public List<NetworkPlugin> getPlugins()
	{
		return this.fromPojos(this.provider.getDAO().getPluginDAO().findPlugins());
	}

	@Override
	public List<NetworkPlugin> getPlugins(int startIndex, int count)
	{
		return this.fromPojos(this.provider.getDAO().getPluginDAO().findPlugins(startIndex, count));
	}

	@Override
	public void setPluginServerEnabled(Plugin plugin, int serverId, boolean enabled)
	{
		NetworkPlugin networkPlugin = this.getPlugin(plugin);
		int pluginId = networkPlugin.getPluginId();
		ServerPluginPojo pojo = this.provider.getDAO().getPluginDAO().findServerPlugin(serverId, pluginId);
		String version = plugin.getDescription().getVersion();
		if (pojo == null)
		{
			pojo = new ServerPluginPojo(serverId, pluginId, version, enabled, null);
			this.provider.getDAO().getPluginDAO().createServerPlugin(pojo);
		} else
		{
			pojo.setEnabled(enabled);
			pojo.setPluginVersion(version);
			this.provider.getDAO().getPluginDAO().updateServerPlugin(pojo);
		}
	}

	@Override
	public void updatePreviousPluginVersions()
	{
		this.provider.getServerModule().getLocalServer().getServerPlugins()
			.forEach(serverPlugin -> serverPlugin.updatePreviousPluginVersion());
	}

	@Override
	protected void destroyModule() throws Exception
	{
		this.pluginCacheThread.disable();
	}

	@Override
	protected void initModule() throws Exception
	{
		this.pluginCacheThread = new CacheCleanupThread<NetworkPlugin>("PluginManager", "plugin", TimeUnit.SECONDS.toMillis(300))
		{

			@Override
			public Collection<NetworkPlugin> getCache()
			{
				return PluginModuleImpl.this.plugins;
			}
		};
		this.pluginCacheThread.start();
	}

	private NetworkPlugin findInCache(int pluginId)
	{
		for (NetworkPlugin plugin : this.plugins)
		{
			if (plugin.getPluginId() == pluginId) { return plugin; }
		}
		return null;
	}

	private NetworkPlugin findInCache(String name)
	{
		for (NetworkPlugin plugin : this.plugins)
		{
			if (plugin.getName().equalsIgnoreCase(name)) { return plugin; }
		}
		return null;
	}

	private List<NetworkPlugin> fromPojos(List<NetworkPluginPojo> pojos)
	{
		List<NetworkPlugin> list = new ArrayList<>(pojos.size());
		pojos.forEach(pojo ->
		{
			NetworkPlugin plugin = this.findInCache(pojo.getPluginId());
			if (plugin == null)
			{
				plugin = PluginFactoryImpl.createPlugin(this.provider, pojo);
				this.putInCache(plugin);
			}
			list.add(plugin);
		});
		return list;
	}

	private void putInCache(NetworkPlugin plugin)
	{
		Validate.notNull(plugin);
		this.plugins.add(plugin);
	}
}
