package net.rieksen.networkcore.core.plugin.task;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.plugin.PluginTaskExecutor;
import net.rieksen.networkcore.core.plugin.PluginTaskExecutorExtra;
import net.rieksen.networkcore.core.plugin.pojo.PluginTaskPojo;
import net.rieksen.networkcore.core.plugin.pojo.PluginTaskPojo.ExecutionStatus;

public class PurgeServerResourceUsageTaskExecutor implements PluginTaskExecutor, PluginTaskExecutorExtra
{

	private NetworkCore	provider;
	private String		summary;

	public PurgeServerResourceUsageTaskExecutor(NetworkCore provider)
	{
		this.provider = provider;
	}

	@Override
	public ExecutionStatus execute(PluginTaskPojo task)
	{
		int days = this.provider.getNetworkPlugin().getOptionSection("Purge").getOption("server-resource-usage").getInteger(null);
		int deleted = this.provider.getDAO().getServerDAO()
			.deleteServerResourceUsageBefore(new Date(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(days)));
		this.summary = String.format("Deleted %s entries", deleted);
		return PluginTaskPojo.ExecutionStatus.SUCCESS;
	}

	@Override
	public String getSummary()
	{
		return this.summary;
	}
}
