package net.rieksen.networkcore.core.statistic;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class NetworkMonthlyUsersStatisticPojo
{

	private int		year;
	private Date	month;
	private int		minUsers;
	private int		maxUsers;
	private int		avgUsers;

	public NetworkMonthlyUsersStatisticPojo()
	{}

	public NetworkMonthlyUsersStatisticPojo(int year, Date month, int minUsers, int maxUsers, int avgUsers)
	{
		this.year = year;
		this.month = month;
		this.minUsers = minUsers;
		this.maxUsers = maxUsers;
		this.avgUsers = avgUsers;
	}

	public int getAvgUsers()
	{
		return this.avgUsers;
	}

	public int getMaxUsers()
	{
		return this.maxUsers;
	}

	public int getMinUsers()
	{
		return this.minUsers;
	}

	public Date getMonth()
	{
		return this.month;
	}

	public int getYear()
	{
		return this.year;
	}

	public void setAvgUsers(int avgUsers)
	{
		this.avgUsers = avgUsers;
	}

	public void setMaxUsers(int maxUsers)
	{
		this.maxUsers = maxUsers;
	}

	public void setMinUsers(int minUsers)
	{
		this.minUsers = minUsers;
	}

	public void setMonth(Date month)
	{
		this.month = month;
	}

	public void setYear(int year)
	{
		this.year = year;
	}
}
