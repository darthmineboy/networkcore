package net.rieksen.networkcore.core.statistic;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class NetworkStatisticPojo
{

	private Date	date;
	private float	onlinePlayers;
	private float	onlineServers;
	private float	cpuUsage;
	private long	memoryUsage;

	public NetworkStatisticPojo()
	{}

	@Deprecated
	public NetworkStatisticPojo(Date date, float onlinePlayers)
	{
		this.date = date;
		this.onlinePlayers = onlinePlayers;
	}

	public NetworkStatisticPojo(Date date, float onlinePlayers, float onlineServers, float cpuUsage, long memoryUsage)
	{
		this.date = date;
		this.onlinePlayers = onlinePlayers;
		this.onlineServers = onlineServers;
		this.cpuUsage = cpuUsage;
		this.memoryUsage = memoryUsage;
	}

	public float getCpuUsage()
	{
		return this.cpuUsage;
	}

	public Date getDate()
	{
		return this.date;
	}

	public long getMemoryUsage()
	{
		return this.memoryUsage;
	}

	public float getOnlinePlayers()
	{
		return this.onlinePlayers;
	}

	public float getOnlineServers()
	{
		return this.onlineServers;
	}

	public void setCpuUsage(float cpuUsage)
	{
		this.cpuUsage = cpuUsage;
	}

	public void setDate(Date date)
	{
		this.date = date;
	}

	public void setMemoryUsage(long memoryUsage)
	{
		this.memoryUsage = memoryUsage;
	}

	public void setOnlinePlayers(float onlinePlayers)
	{
		this.onlinePlayers = onlinePlayers;
	}

	public void setOnlineServers(float onlineServers)
	{
		this.onlineServers = onlineServers;
	}
}
