package net.rieksen.networkcore.core.event.server;

public interface IServerEvent
{

	public int getServerId();
}
