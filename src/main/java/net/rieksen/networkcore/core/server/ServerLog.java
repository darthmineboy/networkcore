package net.rieksen.networkcore.core.server;

import java.util.Date;
import java.util.logging.Level;

import org.apache.commons.lang.Validate;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.server.pojo.ServerLogPojo;

public class ServerLog implements IServerLog
{

	private long	logId;
	private int		serverId;
	private Date	logDate;
	private String	message;
	private Level	level;
	private int		skipped;

	public ServerLog(long logId, int serverId, Date logDate, Level level, String message)
	{
		this.logId = logId;
		this.serverId = serverId;
		this.logDate = logDate;
		this.level = level;
		this.message = message;
	}

	public ServerLog(long logId, int serverId, Date logDate, String message, Level level, int skipped)
	{
		this.logId = logId;
		this.serverId = serverId;
		this.logDate = logDate;
		this.message = message;
		this.level = level;
		this.skipped = skipped;
	}

	public ServerLog(NetworkCore provider, ServerLogPojo pojo)
	{
		Validate.notNull(provider);
		Validate.notNull(pojo);
		this.logId = pojo.getLogId();
		this.serverId = pojo.getServerId();
		this.logDate = pojo.getLogDate();
		this.level = pojo.getLevel();
		this.message = pojo.getMessage();
		this.skipped = pojo.getSkipped();
	}

	@Override
	public Date getDate()
	{
		return this.logDate;
	}

	@Override
	public Level getLevel()
	{
		return this.level;
	}

	public Date getLogDate()
	{
		return this.logDate;
	}

	@Override
	public long getLogId()
	{
		return this.logId;
	}

	@Override
	public String getMessage()
	{
		return this.message;
	}

	@Override
	public int getServerId()
	{
		return this.serverId;
	}

	public int getSkipped()
	{
		return this.skipped;
	}
}
