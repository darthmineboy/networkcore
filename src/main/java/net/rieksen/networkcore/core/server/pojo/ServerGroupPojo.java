package net.rieksen.networkcore.core.server.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ServerGroupPojo
{

	private int		groupId;
	private String	groupName;
	private String	groupDisplayName;
	private String	groupDescription;

	public ServerGroupPojo()
	{}

	public ServerGroupPojo(int groupId, String name, String displayName, String description)
	{
		this.groupId = groupId;
		this.groupName = name;
		this.groupDisplayName = displayName;
		this.groupDescription = description;
	}

	public String getDescription()
	{
		return this.groupDescription;
	}

	public String getDisplayName()
	{
		return this.groupDisplayName;
	}

	public int getGroupId()
	{
		return this.groupId;
	}

	public String getName()
	{
		return this.groupName;
	}

	public void setDescription(String description)
	{
		this.groupDescription = description;
	}

	public void setDisplayName(String displayName)
	{
		this.groupDisplayName = displayName;
	}

	public void setGroupId(int groupId)
	{
		this.groupId = groupId;
	}

	public void setName(String name)
	{
		this.groupName = name;
	}
}
