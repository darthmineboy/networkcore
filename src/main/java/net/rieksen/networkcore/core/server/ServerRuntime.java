package net.rieksen.networkcore.core.server;

import java.util.Date;

public interface ServerRuntime
{

	/**
	 * Set the last ping date.
	 * 
	 * @throws IllegalStateException
	 *             when lastPingDate is null
	 * @param lastPingDate
	 */
	void changeLastPingDate(Date lastPingDate);

	/**
	 * Set the stop date.
	 * 
	 * @param stopDate
	 */
	void changeStopDate(Date stopDate);

	/**
	 * Get the last ping date.
	 * 
	 * @return
	 */
	Date getLastPingDate();

	/**
	 * Get the runtimeId. Can return null.
	 * 
	 * @return
	 */
	int getRuntimeId();

	Server getServer();

	/**
	 * Get the serverId.
	 * 
	 * @return
	 */
	int getServerId();

	/**
	 * Get the start date.
	 * 
	 * @return
	 */
	Date getStartDate();

	/**
	 * Get the stop date.
	 * 
	 * @return
	 */
	Date getStopDate();
}