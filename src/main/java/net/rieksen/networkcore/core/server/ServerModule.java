package net.rieksen.networkcore.core.server;

import java.util.List;

import net.rieksen.networkcore.core.module.Module;

public interface ServerModule extends Module
{

	Server createLocalServer(String name, ServerType type);

	Server createServer(String name, ServerType type);

	Server getLocalServer();

	ServerRuntime getRuntime(int runtimeId);

	Server getServer(int serverId);

	Server getServer(String name);

	List<Server> getServers();

	List<Server> getServers(int startIndex, int maxResults);

	void refreshCache();

	/**
	 * @param server
	 * @throws IllegalStateException
	 *             when localServer is already set
	 */
	void setLocalServer(Server server);
}