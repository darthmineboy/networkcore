package net.rieksen.networkcore.core.server;

import java.util.Date;

public class ServerResourceUsage implements IServerResourceUsage
{

	private int		serverId;
	private Date	currentDate;
	private float	tps;
	private float	nodeCpuUsage;
	private float	serverCpuUsage;
	private int		memoryUsed;
	private int		memoryAvailable;
	private int		memoryTotal;

	public ServerResourceUsage(int serverId, Date currentDate, float tps, float nodeCpuUsage, float serverCpuUsage, int memoryUsed,
		int memoryAvailable, int memoryTotal)
	{
		this.serverId = serverId;
		this.currentDate = currentDate;
		this.tps = tps;
		this.nodeCpuUsage = nodeCpuUsage;
		this.serverCpuUsage = serverCpuUsage;
		this.memoryUsed = memoryUsed;
		this.memoryAvailable = memoryAvailable;
		this.memoryTotal = memoryTotal;
	}

	/* 
	 */
	@Override
	public Date getCurrentDate()
	{
		return this.currentDate;
	}

	/* 
	 */
	@Override
	public int getMemoryAvailable()
	{
		return this.memoryAvailable;
	}

	/* 
	 */
	@Override
	public int getMemoryTotal()
	{
		return this.memoryTotal;
	}

	/* 
	 */
	@Override
	public int getMemoryUsed()
	{
		return this.memoryUsed;
	}

	/* 
	 */
	@Override
	public float getNodeCpuUsage()
	{
		return this.nodeCpuUsage;
	}

	/* 
	 */
	@Override
	public float getServerCpuUsage()
	{
		return this.serverCpuUsage;
	}

	/* 
	 */
	@Override
	public int getServerId()
	{
		return this.serverId;
	}

	/* 
	 */
	@Override
	public float getTps()
	{
		return this.tps;
	}
}
