package net.rieksen.networkcore.core.server;

public enum ServerType
{
	SPIGOT(0),
	BUNGEECORD(1),
	GROUP(2),
	CUSTOM(3);

	private int	i;

	ServerType(int i)
	{
		this.i = i;
	}

	public int getValue()
	{
		return i;
	}

	/**
	 * Get the server type from value. Can return null.
	 * 
	 * @param value
	 * @return
	 */
	public static ServerType fromValue(int value)
	{
		for (ServerType type : values())
		{
			if (type.i == value) { return type; }
		}
		return null;
	}
}
