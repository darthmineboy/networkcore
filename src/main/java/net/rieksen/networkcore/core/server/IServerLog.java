package net.rieksen.networkcore.core.server;

import java.util.Date;
import java.util.logging.Level;

public interface IServerLog
{

	public Level getLevel();

	Date getDate();

	long getLogId();

	String getMessage();

	int getServerId();
}