package net.rieksen.networkcore.core.server;

import java.util.Date;
import java.util.logging.Level;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.server.pojo.ServerLogPojo;
import net.rieksen.networkcore.core.server.pojo.ServerPojo;
import net.rieksen.networkcore.core.server.pojo.ServerRuntimePojo;

public class ServerFactory
{

	public static Server createLocalServer(NetworkCore provider, int serverId, String name, ServerType type)
	{
		return new LocalServerImpl(provider, serverId, name, type);
	}

	public static Server createLocalServer(NetworkCore provider, ServerPojo pojo)
	{
		return new LocalServerImpl(provider, pojo);
	}

	public static IServerLog createLog(NetworkCore provider, ServerLogPojo pojo)
	{
		return new ServerLog(provider, pojo);
	}

	public static IServerLog createLog(long logId, int serverId, Date logDate, Level level, String message)
	{
		return new ServerLog(logId, serverId, logDate, level, message);
	}

	public static ServerRuntime createRuntime(NetworkCore provider, int runtimeId, int serverId, Date startDate, Date lastPingDate,
		Date stopDate)
	{
		return new ServerRuntimeImpl(provider, runtimeId, serverId, startDate, lastPingDate, stopDate);
	}

	public static ServerRuntime createRuntime(NetworkCore provider, ServerRuntimePojo pojo)
	{
		return new ServerRuntimeImpl(provider, pojo);
	}

	public static Server createServer(NetworkCore provider, int serverId, String name, ServerType type)
	{
		return new ServerImpl(provider, serverId, name, type);
	}

	public static Server createServer(NetworkCore provider, ServerPojo pojo)
	{
		return new ServerImpl(provider, pojo);
	}
}
