package net.rieksen.networkcore.core.server.pojo;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ServerResourceUsagePojo
{

	private int		serverId;
	private Date	currentDate;
	private float	tps;
	private float	nodeCpuUsage;
	private float	serverCpuUsage;
	private int		memoryUsed;
	private int		memoryAvailable;
	private int		memoryTotal;

	public ServerResourceUsagePojo()
	{}

	public ServerResourceUsagePojo(int serverId, Date currentDate, float tps, float nodeCpuUsage, float serverCpuUsage, int memoryUsed,
		int memoryAvailable, int memoryTotal)
	{
		this.serverId = serverId;
		this.currentDate = currentDate;
		this.tps = tps;
		this.nodeCpuUsage = nodeCpuUsage;
		this.serverCpuUsage = serverCpuUsage;
		this.memoryUsed = memoryUsed;
		this.memoryAvailable = memoryAvailable;
		this.memoryTotal = memoryTotal;
	}

	public Date getCurrentDate()
	{
		return this.currentDate;
	}

	public int getMemoryAvailable()
	{
		return this.memoryAvailable;
	}

	public int getMemoryTotal()
	{
		return this.memoryTotal;
	}

	public int getMemoryUsed()
	{
		return this.memoryUsed;
	}

	public float getNodeCpuUsage()
	{
		return this.nodeCpuUsage;
	}

	public float getServerCpuUsage()
	{
		return this.serverCpuUsage;
	}

	public int getServerId()
	{
		return this.serverId;
	}

	public float getTps()
	{
		return this.tps;
	}

	public void setCurrentDate(Date currentDate)
	{
		this.currentDate = currentDate;
	}

	public void setMemoryAvailable(int memoryAvailable)
	{
		this.memoryAvailable = memoryAvailable;
	}

	public void setMemoryTotal(int memoryTotal)
	{
		this.memoryTotal = memoryTotal;
	}

	public void setMemoryUsed(int memoryUsed)
	{
		this.memoryUsed = memoryUsed;
	}

	public void setNodeCpuUsage(float nodeCpuUsage)
	{
		this.nodeCpuUsage = nodeCpuUsage;
	}

	public void setServerCpuUsage(float serverCpuUsage)
	{
		this.serverCpuUsage = serverCpuUsage;
	}

	public void setServerId(int serverId)
	{
		this.serverId = serverId;
	}

	public void setTps(float tps)
	{
		this.tps = tps;
	}

	@Override
	public String toString()
	{
		return "ServerResourceUsagePojo [serverId=" + this.serverId + ", currentDate=" + this.currentDate + ", tps=" + this.tps
			+ ", nodeCpuUsage=" + this.nodeCpuUsage + ", serverCpuUsage=" + this.serverCpuUsage + ", memoryUsed=" + this.memoryUsed
			+ ", memoryAvailable=" + this.memoryAvailable + ", memoryTotal=" + this.memoryTotal + "]";
	}
}
