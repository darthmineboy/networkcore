package net.rieksen.networkcore.core.server;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.bukkit.World;

import net.rieksen.networkcore.core.NetworkCoreAPI;
import net.rieksen.networkcore.core.plugin.ServerPlugin;
import net.rieksen.networkcore.core.server.pojo.ServerPojo;
import net.rieksen.networkcore.core.server.pojo.ServerResourceUsagePojo;
import net.rieksen.networkcore.core.user.UserConnect;
import net.rieksen.networkcore.core.world.ServerWorld;

public interface Server
{

	/**
	 * Get the local server.
	 * 
	 * @return
	 */
	public static Server getLocalServer()
	{
		ServerModule manager = NetworkCoreAPI.getServerModule();
		return manager == null ? null : manager.getLocalServer();
	}

	/**
	 * Get the server. Can return null.
	 * 
	 * @param serverId
	 * @return
	 */
	public static Server getServer(int serverId)
	{
		ServerModule manager = NetworkCoreAPI.getServerModule();
		return manager == null ? null : manager.getServer(serverId);
	}

	/**
	 * Get the server. Can return null.
	 * 
	 * @param name
	 * @return
	 */
	public static Server getServer(String name)
	{
		ServerModule manager = NetworkCoreAPI.getServerModule();
		return manager == null ? null : manager.getServer(name);
	}

	public IServerLog createLog(Date date, Level level, String message);

	/**
	 * Creates a record, with lines skipped due to rate limiting.
	 * 
	 * @param date
	 * @param level
	 * @param message
	 * @param skipped
	 * @return
	 */
	public IServerLog createLog(Date date, Level level, String message, int skipped);

	public void createResourceUsage(ServerResourceUsagePojo usage);

	public ServerRuntime createRuntime();

	public List<UserConnect> findOpenConnects();

	public List<ServerRuntime> findOpenRuntimes();

	/**
	 * Get the latest runtime.
	 */
	public ServerRuntime findRuntime();

	public ServerWorld findWorld(World world);

	public void refresh();

	public void setPojo(ServerPojo serverPojo);

	/**
	 * Set the runtime.
	 * 
	 * @param runtime
	 */
	public void setRuntimeCache(ServerRuntime runtime);

	/**
	 * Checks whether the server was online in the provided <code>period</code>
	 * and <code>timeUnit</code>.
	 * 
	 * @param period
	 * @param timeUnit
	 * @return
	 */
	public boolean wasOnline(int period, TimeUnit timeUnit);

	void broadcast(String message);

	void broadcastMessage(int messageId);

	void broadcastMessage(int messageId, Map<String, String> variables);

	void broadcastMessageWithPermission(String permission, int messageId);

	void broadcastMessageWithPermission(String permission, int messageId, Map<String, String> variables);

	void broadcastWithPermission(String message, String permission);

	/**
	 * Set the name
	 * 
	 * @param name
	 * @throws IllegalStateException
	 *             when name is null
	 */
	void changeName(String name);

	void changeOnline(boolean isOnline);

	void changeSocketAuto(boolean socketAuto);

	void changeSocketHost(String host);

	void changeSocketKey(String key);

	void changeSocketPort(int port);

	ServerWorld createWorld(String name);

	ServerResourceUsagePojo getLatestResourceUsage();

	/**
	 * Get the unique name of this server.
	 * 
	 * @return
	 */
	String getName();

	/**
	 * Get the serverId. Can return null.
	 * 
	 * @return
	 */
	int getServerId();

	List<ServerPlugin> getServerPlugins();

	String getSocketBindHost();

	String getSocketHost();

	String getSocketKey();

	Integer getSocketPort();

	/**
	 * Get the type.
	 * 
	 * @return
	 */
	ServerType getType();

	/**
	 * The amount of users connected.
	 * 
	 * @return
	 */
	int getUserCount();

	/**
	 * Whether this server should be online. This may not be an actual
	 * representation of the online state due to e.g. a server crash.
	 * 
	 * @return
	 */
	boolean isOnline();

	/**
	 * Should the socket be automatically configured. If you need to port
	 * forward your server, then this should be false.
	 * 
	 * @return
	 */
	boolean isSocketAuto();

	void notifyServersOfUpdate();

	void reload();

	void restart();

	void stop();

	ServerPojo toPojo();
}