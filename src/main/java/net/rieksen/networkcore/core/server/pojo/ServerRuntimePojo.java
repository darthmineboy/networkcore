package net.rieksen.networkcore.core.server.pojo;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ServerRuntimePojo
{

	private int		runtimeId;
	private int		serverId;
	private Date	startDate;
	private Date	lastPingDate;
	private Date	stopDate;

	public ServerRuntimePojo()
	{}

	public ServerRuntimePojo(int runtimeId, int serverId, Date startDate, Date lastPingDate, Date stopDate)
	{
		this.runtimeId = runtimeId;
		this.serverId = serverId;
		this.startDate = startDate;
		this.lastPingDate = lastPingDate;
		this.stopDate = stopDate;
	}

	public Date getLastPingDate()
	{
		return this.lastPingDate;
	}

	public int getRuntimeId()
	{
		return this.runtimeId;
	}

	public int getServerId()
	{
		return this.serverId;
	}

	public Date getStartDate()
	{
		return this.startDate;
	}

	public Date getStopDate()
	{
		return this.stopDate;
	}

	public void setLastPingDate(Date lastPingDate)
	{
		this.lastPingDate = lastPingDate;
	}

	public void setRuntimeId(int runtimeId)
	{
		this.runtimeId = runtimeId;
	}

	public void setServerId(int serverId)
	{
		this.serverId = serverId;
	}

	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}

	public void setStopDate(Date stopDate)
	{
		this.stopDate = stopDate;
	}
}
