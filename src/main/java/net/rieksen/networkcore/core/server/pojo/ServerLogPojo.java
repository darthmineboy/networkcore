package net.rieksen.networkcore.core.server.pojo;

import java.util.Date;
import java.util.logging.Level;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ServerLogPojo
{

	private long	logId;
	private int		serverId;
	private Date	logDate;
	private String	level;
	private String	message;
	private int		skipped;

	public ServerLogPojo()
	{}

	@Deprecated
	public ServerLogPojo(long logId, int serverId, Date logDate, String level, String message)
	{
		this.logId = logId;
		this.serverId = serverId;
		this.logDate = logDate;
		this.level = level;
		this.message = message;
	}

	public ServerLogPojo(long logId, int serverId, Date logDate, String level, String message, int skipped)
	{
		this.logId = logId;
		this.serverId = serverId;
		this.logDate = logDate;
		this.level = level;
		this.message = message;
		this.skipped = skipped;
	}

	public Level getLevel()
	{
		return Level.parse(this.level);
	}

	public Date getLogDate()
	{
		return this.logDate;
	}

	public long getLogId()
	{
		return this.logId;
	}

	public String getMessage()
	{
		return this.message;
	}

	public int getServerId()
	{
		return this.serverId;
	}

	public int getSkipped()
	{
		return this.skipped;
	}

	public void setLevel(String level)
	{
		this.level = level;
	}

	public void setLogDate(Date logDate)
	{
		this.logDate = logDate;
	}

	public void setLogId(long logId)
	{
		this.logId = logId;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public void setServerId(int serverId)
	{
		this.serverId = serverId;
	}

	public void setSkipped(int skipped)
	{
		this.skipped = skipped;
	}
}
