package net.rieksen.networkcore.core.server.pojo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import net.rieksen.networkcore.core.server.ServerType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class ServerPojo implements Serializable
{

	private static final long	serialVersionUID	= 1L;
	@XmlElement
	private int					serverId;
	@XmlElement
	private String				serverName;
	@XmlElement
	private ServerType			serverType;
	@XmlElement
	private boolean				isOnline;
	private boolean				socketAuto;
	private String				socketBindHost;
	private String				socketHost;
	private Integer				socketPort;
	private String				socketKey;

	public ServerPojo()
	{}

	public ServerPojo(int serverId, String serverName, ServerType serverType, boolean isOnline, boolean socketAuto, String socketBindHost,
		String socketHost, Integer socketPort, String socketKey)
	{
		this.serverId = serverId;
		this.serverName = serverName;
		this.serverType = serverType;
		this.isOnline = isOnline;
		this.socketAuto = socketAuto;
		this.socketBindHost = socketBindHost;
		this.socketHost = socketHost;
		this.socketPort = socketPort;
		this.socketKey = socketKey;
	}

	@Deprecated
	public ServerPojo(int serverId, String serverName, ServerType serverType, boolean isOnline, String socketHost, Integer socketPort,
		String socketKey)
	{
		this.serverId = serverId;
		this.serverName = serverName;
		this.serverType = serverType;
		this.isOnline = isOnline;
		this.socketHost = socketHost;
		this.socketPort = socketPort;
		this.socketKey = socketKey;
	}

	public String getName()
	{
		return this.serverName;
	}

	public int getServerId()
	{
		return this.serverId;
	}

	public String getServerName()
	{
		return this.serverName;
	}

	public ServerType getServerType()
	{
		return this.serverType;
	}

	public String getSocketBindHost()
	{
		return this.socketBindHost;
	}

	public String getSocketHost()
	{
		return this.socketHost;
	}

	public String getSocketKey()
	{
		return this.socketKey;
	}

	public Integer getSocketPort()
	{
		return this.socketPort;
	}

	public ServerType getType()
	{
		return this.serverType;
	}

	public boolean isOnline()
	{
		return this.isOnline;
	}

	public boolean isSocketAuto()
	{
		return this.socketAuto;
	}

	public void setName(String name)
	{
		this.serverName = name;
	}

	public void setOnline(boolean isOnline)
	{
		this.isOnline = isOnline;
	}

	public void setServerId(int serverId)
	{
		this.serverId = serverId;
	}

	public void setServerName(String serverName)
	{
		this.serverName = serverName;
	}

	public void setServerType(ServerType serverType)
	{
		this.serverType = serverType;
	}

	public void setSocketAuto(boolean socketAuto)
	{
		this.socketAuto = socketAuto;
	}

	public void setSocketBindHost(String socketBindHost)
	{
		this.socketBindHost = socketBindHost;
	}

	public void setSocketHost(String socketHost)
	{
		this.socketHost = socketHost;
	}

	public void setSocketKey(String socketKey)
	{
		this.socketKey = socketKey;
	}

	public void setSocketPort(Integer socketPort)
	{
		this.socketPort = socketPort;
	}

	public void setType(ServerType type)
	{
		this.serverType = type;
	}

	@Override
	public String toString()
	{
		return "ServerPojo [serverId=" + this.serverId + ", serverName=" + this.serverName + ", serverType=" + this.serverType
			+ ", isOnline=" + this.isOnline + ", socketAuto=" + this.socketAuto + ", socketHost=" + this.socketHost + ", socketPort="
			+ this.socketPort + "]";
	}
}
