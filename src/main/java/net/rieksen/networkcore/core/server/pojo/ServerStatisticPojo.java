package net.rieksen.networkcore.core.server.pojo;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ServerStatisticPojo
{

	public static List<ServerStatisticPojo> merge(List<ServerStatisticPojo> data, int max)
	{
		if (data.size() <= max || max <= 1) { return data; }
		float averageMergeItems = (float) data.size() / max;
		Iterator<ServerStatisticPojo> it = data.iterator();
		ServerStatisticPojo base = it.next();
		float mergedCounter = 1;
		while (it.hasNext())
		{
			ServerStatisticPojo merge = it.next();
			if (++mergedCounter >= averageMergeItems)
			{
				base = merge;
				mergedCounter -= averageMergeItems;
				continue;
			}
			base.merge(merge);
			it.remove();
		}
		return data;
	}

	private int		serverId;
	private Date	currentDate;
	private float	onlinePlayers;
	private float	tps;
	private float	serverCpuUsage;
	private float	memoryUsed;
	private float	memoryAvailable;
	private float	memoryTotal;
	// Other
	private int		mergeWeight	= 1;

	public ServerStatisticPojo(int serverId, Date currentDate, float onlinePlayers, float tps, float serverCpuUsage, float memoryUsed,
		float memoryAvailable, float memoryTotal)
	{
		this.serverId = serverId;
		this.currentDate = currentDate;
		this.onlinePlayers = onlinePlayers;
		this.tps = tps;
		this.serverCpuUsage = serverCpuUsage;
		this.memoryUsed = memoryUsed;
		this.memoryAvailable = memoryAvailable;
		this.memoryTotal = memoryTotal;
	}

	public Date getCurrentDate()
	{
		return this.currentDate;
	}

	public float getMemoryAvailable()
	{
		return this.memoryAvailable;
	}

	public float getMemoryTotal()
	{
		return this.memoryTotal;
	}

	public float getMemoryUsed()
	{
		return this.memoryUsed;
	}

	public float getOnlinePlayers()
	{
		return this.onlinePlayers;
	}

	public float getServerCpuUsage()
	{
		return this.serverCpuUsage;
	}

	public int getServerId()
	{
		return this.serverId;
	}

	public float getTps()
	{
		return this.tps;
	}

	public void merge(ServerStatisticPojo pojo)
	{
		int totalMergeWeight = this.mergeWeight + pojo.mergeWeight;
		this.onlinePlayers = ((this.onlinePlayers * this.mergeWeight) + (pojo.onlinePlayers * pojo.mergeWeight)) / totalMergeWeight;
		this.tps = ((this.tps * this.mergeWeight) + (pojo.tps * pojo.mergeWeight)) / totalMergeWeight;
		this.serverCpuUsage = ((this.serverCpuUsage * this.mergeWeight) + (pojo.serverCpuUsage * pojo.mergeWeight)) / totalMergeWeight;
		this.memoryUsed = ((this.memoryUsed * this.mergeWeight) + (pojo.memoryUsed * pojo.mergeWeight)) / totalMergeWeight;
		this.memoryAvailable = ((this.memoryAvailable * this.mergeWeight) + (pojo.memoryAvailable * pojo.mergeWeight)) / totalMergeWeight;
		this.memoryTotal = ((this.memoryTotal * this.mergeWeight) + (pojo.memoryTotal * pojo.mergeWeight)) / totalMergeWeight;
		this.mergeWeight += pojo.mergeWeight;
	}

	public void setCurrentDate(Date currentDate)
	{
		this.currentDate = currentDate;
	}

	public void setMemoryAvailable(float memoryAvailable)
	{
		this.memoryAvailable = memoryAvailable;
	}

	public void setMemoryTotal(float memoryTotal)
	{
		this.memoryTotal = memoryTotal;
	}

	public void setMemoryUsed(float memoryUsed)
	{
		this.memoryUsed = memoryUsed;
	}

	public void setOnlinePlayers(float onlinePlayers)
	{
		this.onlinePlayers = onlinePlayers;
	}

	public void setServerCpuUsage(float serverCpuUsage)
	{
		this.serverCpuUsage = serverCpuUsage;
	}

	public void setServerId(int serverId)
	{
		this.serverId = serverId;
	}

	public void setTps(float tps)
	{
		this.tps = tps;
	}
}
