package net.rieksen.networkcore.core.server;

import java.util.Date;

import org.apache.commons.lang.Validate;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.NetworkCoreAPI;
import net.rieksen.networkcore.core.server.pojo.ServerRuntimePojo;

/**
 * A server runtime represents server start till server end.
 * 
 * @author Darthmineboy
 */
public class ServerRuntimeImpl implements ServerRuntime
{

	/**
	 * Get a runtime.
	 * 
	 * @param runtimeId
	 * @return
	 */
	public static ServerRuntime getRuntime(int runtimeId)
	{
		return NetworkCoreAPI.getServerModule().getRuntime(runtimeId);
	}

	private final NetworkCore	provider;
	private final int			runtimeId;
	private final int			serverId;
	private final Date			startDate;
	private Date				lastPingDate;
	private Date				stopDate;

	/**
	 * @param runtimeId
	 * @param serverId
	 * @param startDate
	 * @param lastPingDate
	 * @param stopDate
	 * @throws IllegalStateException
	 *             when serverId, startDate or startDate is null
	 */
	public ServerRuntimeImpl(NetworkCore provider, int runtimeId, int serverId, Date startDate, Date lastPingDate, Date stopDate)
	{
		Validate.notNull(provider);
		Validate.notNull(startDate, "StartDate cannot be null");
		Validate.notNull(lastPingDate, "LastPingDate cannot be null");
		this.provider = provider;
		this.runtimeId = runtimeId;
		this.serverId = serverId;
		this.startDate = startDate;
		this.lastPingDate = lastPingDate;
		this.stopDate = stopDate;
	}

	public ServerRuntimeImpl(NetworkCore provider, ServerRuntimePojo pojo)
	{
		Validate.notNull(provider);
		Validate.notNull(pojo);
		Validate.notNull(pojo.getStartDate());
		Validate.notNull(pojo.getLastPingDate());
		this.provider = provider;
		this.runtimeId = pojo.getRuntimeId();
		this.serverId = pojo.getServerId();
		this.startDate = pojo.getStartDate();
		this.lastPingDate = pojo.getLastPingDate();
		this.stopDate = pojo.getStopDate();
	}

	@Override
	public void changeLastPingDate(Date lastPingDate)
	{
		Validate.notNull(lastPingDate, "LastPingDate cannot be null");
		ServerRuntimePojo pojo = this.toPojo();
		pojo.setLastPingDate(lastPingDate);
		this.provider.getDAO().getServerDAO().updateRuntime(pojo);
		this.lastPingDate = lastPingDate;
	}

	@Override
	public void changeStopDate(Date stopDate)
	{
		ServerRuntimePojo pojo = this.toPojo();
		pojo.setStopDate(stopDate);;
		this.provider.getDAO().getServerDAO().updateRuntime(pojo);
		this.stopDate = stopDate;
	}

	@Override
	public Date getLastPingDate()
	{
		return this.lastPingDate;
	}

	@Override
	public int getRuntimeId()
	{
		return this.runtimeId;
	}

	@Override
	public Server getServer()
	{
		return this.provider.getServerModule().getServer(this.serverId);
	}

	@Override
	public int getServerId()
	{
		return this.serverId;
	}

	@Override
	public Date getStartDate()
	{
		return this.startDate;
	}

	@Override
	public Date getStopDate()
	{
		return this.stopDate;
	}

	private ServerRuntimePojo toPojo()
	{
		return new ServerRuntimePojo(this.runtimeId, this.serverId, this.startDate, this.lastPingDate, this.stopDate);
	}
}
