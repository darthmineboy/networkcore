package net.rieksen.networkcore.core.server;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.enums.NetworkCoreSocketMessage;
import net.rieksen.networkcore.core.module.ModuleImpl;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.core.server.pojo.ServerPojo;
import net.rieksen.networkcore.core.server.pojo.ServerRuntimePojo;
import net.rieksen.networkcore.core.socket.IServerSocketResponse;
import net.rieksen.networkcore.core.socket.ServerSocketRequestHandler;
import net.rieksen.networkcore.core.socket.ServerSocketResponse;

public class ServerModuleImpl extends ModuleImpl implements ServerModule
{

	private NetworkCore								provider;
	private Collection<Server>						servers	= Collections.synchronizedCollection(new HashSet<Server>());
	private Server									localServer;
	private LoadingCache<Integer, ServerRuntime>	runtimeCache;

	public ServerModuleImpl(NetworkCore plugin)
	{
		this.provider = plugin;
		this.runtimeCache = CacheBuilder.newBuilder().maximumSize(1000).expireAfterWrite(5, TimeUnit.MINUTES)
			.build(new CacheLoader<Integer, ServerRuntime>()
			{

				@Override
				public ServerRuntime load(Integer runtimeId) throws Exception
				{
					ServerRuntimePojo runtime = ServerModuleImpl.this.provider.getDAO().getServerDAO().findRuntime(runtimeId);
					return runtime == null ? null : ServerFactory.createRuntime(ServerModuleImpl.this.provider, runtime);
				}
			});
	}

	@Override
	public Server createLocalServer(String name, ServerType type)
	{
		ServerPojo pojo = new ServerPojo(0, name, type, false, true, null, null, null, null);
		pojo.setServerId(this.provider.getDAO().getServerDAO().createServer(pojo));
		return ServerFactory.createLocalServer(this.provider, pojo);
	}

	@Override
	public Server createServer(String name, ServerType type)
	{
		ServerPojo pojo = new ServerPojo(0, name, type, false, true, null, null, null, null);
		pojo.setServerId(this.provider.getDAO().getServerDAO().createServer(pojo));
		return ServerFactory.createServer(this.provider, pojo);
	}

	@Override
	public void destroyModule()
	{
		this.servers.clear();
	}

	@Override
	public Server getLocalServer()
	{
		return this.localServer;
	}

	@Override
	public ServerRuntime getRuntime(int runtimeId)
	{
		return this.runtimeCache.getUnchecked(runtimeId);
	}

	@Override
	public Server getServer(int serverId)
	{
		return this.servers.stream().filter(server -> server.getServerId() == serverId).findAny().orElse(null);
	}

	@Override
	public Server getServer(String name)
	{
		return this.servers.stream().filter(server -> server.getName().equals(name)).findAny().orElse(null);
	}

	@Override
	public List<Server> getServers()
	{
		return Collections.unmodifiableList(new ArrayList<>(this.servers));
	}

	@Override
	public List<Server> getServers(int startIndex, int maxResults)
	{
		return Collections.unmodifiableList(new ArrayList<>(this.servers).subList(Math.min(startIndex, this.servers.size()),
			Math.min(startIndex + maxResults, this.servers.size())));
	}

	@Override
	public void initModule()
	{
		this.refreshCache();
		this.registerSocketRequestHandlers();
	}

	@Override
	public void refreshCache()
	{
		for (ServerPojo pojo : this.provider.getDAO().getServerDAO().findServers())
		{
			Server server = this.getServer(pojo.getServerId());
			if (server == null)
			{
				this.servers.add(ServerFactory.createServer(this.provider, pojo));
			} else
			{
				server.setPojo(pojo);
			}
		}
	}

	@Override
	public void setLocalServer(Server server)
	{
		if (this.localServer != null) { throw new IllegalStateException("There is already a local server registered"); }
		this.localServer = server;
		this.removeServerFromCache(server.getServerId());
		this.servers.add(server);
	}

	private void registerSocketRequestHandlers()
	{
		NetworkPlugin plugin = this.provider.getNetworkPlugin();
		plugin.registerSocketRequestHandler(NetworkCoreSocketMessage.UPDATE_SERVER.name(), new ServerSocketRequestHandler()
		{

			@Override
			public IServerSocketResponse process(byte[] data)
			{
				ServerPojo serverPojo = this.getObject(data, ServerPojo.class);
				Server server = ServerModuleImpl.this.getServer(serverPojo.getServerId());
				if (server == null)
				{
					ServerModuleImpl.this.refreshCache();
				} else
				{
					server.setPojo(serverPojo);
				}
				return null;
			}
		});
		plugin.registerSocketRequestHandler(NetworkCoreSocketMessage.RESTART_SERVER.name(), new ServerSocketRequestHandler()
		{

			@Override
			public IServerSocketResponse process(byte[] data)
			{
				ServerModuleImpl.this.provider.getTaskChainFactory().newChain().delay(500, TimeUnit.MILLISECONDS).sync(() ->
				{
					ServerModuleImpl.this.getLocalServer().restart();
				}).execute();
				return new ServerSocketResponse(true, 200, "Server is restarting", null);
			}
		});
		plugin.registerSocketRequestHandler(NetworkCoreSocketMessage.STOP_SERVER.name(), new ServerSocketRequestHandler()
		{

			@Override
			public IServerSocketResponse process(byte[] data)
			{
				ServerModuleImpl.this.provider.getTaskChainFactory().newChain().delay(500, TimeUnit.MILLISECONDS).sync(() ->
				{
					ServerModuleImpl.this.getLocalServer().stop();
				}).execute();
				return new ServerSocketResponse(true, 200, "Server is stopping", null);
			}
		});
		plugin.registerSocketRequestHandler(NetworkCoreSocketMessage.RELOAD_SERVER.name(), new ServerSocketRequestHandler()
		{

			@Override
			public IServerSocketResponse process(byte[] data)
			{
				ServerModuleImpl.this.provider.getTaskChainFactory().newChain().delay(500, TimeUnit.MILLISECONDS).sync(() ->
				{
					ServerModuleImpl.this.getLocalServer().reload();
				}).execute();
				return new ServerSocketResponse(true, 200, "Server is reloading", null);
			}
		});
		plugin.registerSocketRequestHandler(NetworkCoreSocketMessage.DELAYED_SOCKET_MODULE_RESTART.name(), new ServerSocketRequestHandler()
		{

			@Override
			public IServerSocketResponse process(byte[] data)
			{
				ServerModuleImpl.this.provider.getTaskChainFactory().newChain().delay(5000, TimeUnit.MILLISECONDS).sync(() ->
				{
					ServerModuleImpl.this.getLocalServer().refresh();
					ServerModuleImpl.this.provider.getSocketModule().destroy();
					ServerModuleImpl.this.provider.getSocketModule().init();
				}).execute();
				return new ServerSocketResponse(true, 200, "Server socket module will restart in 5 seconds", null);
			}
		});
	}

	private void removeServerFromCache(int serverId)
	{
		Server serverInCache = this.getServer(serverId);
		if (serverInCache != null)
		{
			this.servers.remove(serverInCache);
		}
	}
}
