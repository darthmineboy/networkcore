package net.rieksen.networkcore.core.server;

import java.util.Date;

public interface IServerResourceUsage
{

	Date getCurrentDate();

	/**
	 * @return in MB
	 */
	int getMemoryAvailable();

	/**
	 * @return in MB
	 */
	int getMemoryTotal();

	/**
	 * @return in MB
	 */
	int getMemoryUsed();

	float getNodeCpuUsage();

	float getServerCpuUsage();

	int getServerId();

	float getTps();
}