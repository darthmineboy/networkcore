package net.rieksen.networkcore.core.server;

import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.chat.ComponentSerializer;
import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.message.MessageTranslation;
import net.rieksen.networkcore.core.message.MessageType;
import net.rieksen.networkcore.core.server.pojo.ServerPojo;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.spigot.util.MinecraftUtil;

/**
 * Local server which stores the current runtime.
 */
public class LocalServerImpl extends ServerImpl
{

	public LocalServerImpl(NetworkCore provider, ServerPojo pojo)
	{
		super(provider, pojo);
	}

	protected LocalServerImpl(NetworkCore provider, int serverId, String name, ServerType type)
	{
		super(provider, serverId, name, type);
	}

	@Override
	public void broadcast(String message)
	{
		Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', message));
	}

	@Override
	public void broadcastMessage(int messageId, Map<String, String> variables)
	{
		this.broadcastMessageWithPermission(null, messageId, variables);
	}

	@Override
	public void broadcastMessageWithPermission(String permission, int messageId, Map<String, String> variables)
	{
		Message message = this.provider.getMessageModule().getMessage(messageId);
		if (message == null) { return; }
		for (Player player : Bukkit.getOnlinePlayers())
		{
			if (permission != null)
			{
				if (!player.hasPermission(permission))
				{
					continue;
				}
			}
			User user = this.provider.getUserModule().getUser(player);
			MessageTranslation translation = message.getTranslation(user.getLanguagePreferences());
			String msg = message.getMessage(user);
			if (translation.getMessageType() == MessageType.JSON)
			{
				BaseComponent[] components = ComponentSerializer.parse(msg);
				components = MinecraftUtil.replaceVariables(components, variables);
				player.spigot().sendMessage(components);
			} else
			{
				if (variables != null)
				{
					for (Entry<String, String> entry : variables.entrySet())
					{
						if (msg.contains(entry.getKey()))
						{
							msg = msg.replace(entry.getKey(), entry.getValue() == null ? "" : entry.getValue());
						}
					}
				}
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
			}
		}
	}

	@Override
	public void broadcastWithPermission(String message, String permission)
	{
		if (permission == null)
		{
			this.broadcast(ChatColor.translateAlternateColorCodes('&', message));
			return;
		}
		Bukkit.broadcast(ChatColor.translateAlternateColorCodes('&', message), permission);
	}

	@Override
	public int getUserCount()
	{
		return Bukkit.getOnlinePlayers().size();
	}

	@Override
	public void reload()
	{
		Bukkit.reload();
	}

	@Override
	public void restart()
	{
		Bukkit.spigot().restart();
	}

	@Override
	public void stop()
	{
		Bukkit.shutdown();
	}
}
