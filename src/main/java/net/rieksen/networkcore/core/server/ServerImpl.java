package net.rieksen.networkcore.core.server;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.stream.Collectors;

import org.apache.commons.lang.Validate;
import org.bukkit.World;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.cache.CacheComponent;
import net.rieksen.networkcore.core.enums.NetworkCoreSocketMessage;
import net.rieksen.networkcore.core.plugin.ServerPlugin;
import net.rieksen.networkcore.core.server.pojo.ServerLogPojo;
import net.rieksen.networkcore.core.server.pojo.ServerPojo;
import net.rieksen.networkcore.core.server.pojo.ServerResourceUsagePojo;
import net.rieksen.networkcore.core.server.pojo.ServerRuntimePojo;
import net.rieksen.networkcore.core.socket.IServerSocketRequest;
import net.rieksen.networkcore.core.socket.IServerSocketResponse;
import net.rieksen.networkcore.core.socket.ServerSocketRequest;
import net.rieksen.networkcore.core.user.UserConnect;
import net.rieksen.networkcore.core.world.ServerWorld;
import net.rieksen.networkcore.core.world.pojo.ServerWorldPojo;

public class ServerImpl implements Server
{

	protected NetworkCore					provider;
	private int								serverId;
	private String							name;
	private ServerType						type;
	private boolean							isOnline;
	private CacheComponent<ServerRuntime>	runtime	= new CacheComponent<ServerRuntime>(TimeUnit.MINUTES.toMillis(5))
													{

														@Override
														public void refreshCache()
														{
															ServerRuntimePojo pojo = ServerImpl.this.provider.getDAO().getServerDAO()
																.findLatestRuntime(ServerImpl.this.serverId);
															this.cachedObject = ServerFactory.createRuntime(ServerImpl.this.provider, pojo);
														}
													};
	private boolean							socketAuto;
	private String							socketBindHost;
	private String							socketHost;
	private Integer							socketPort;
	private String							socketKey;
	private HashSet<ServerWorld>			worlds;

	public ServerImpl(NetworkCore provider, ServerPojo pojo)
	{
		Validate.notNull(provider);
		Validate.notNull(pojo);
		Validate.notNull(pojo.getName());
		Validate.notNull(pojo.getType());
		this.provider = provider;
		this.serverId = pojo.getServerId();
		this.name = pojo.getName();
		this.type = pojo.getType();
		this.isOnline = pojo.isOnline();
		this.socketAuto = pojo.isSocketAuto();
		this.socketHost = pojo.getSocketHost();
		this.socketPort = pojo.getSocketPort();
		this.socketKey = pojo.getSocketKey();
	}

	/**
	 * @throws IllegalStateException
	 *             when provider, name or type is null
	 * @param serverId
	 * @param name
	 * @param type
	 */
	protected ServerImpl(NetworkCore provider, int serverId, String name, ServerType type)
	{
		Validate.notNull(provider, "Provider cannot be null");
		Validate.notNull(name, "Name cannot be null");
		Validate.notNull(type, "Type cannot be null");
		this.provider = provider;
		this.serverId = serverId;
		this.name = name;
		this.type = type;
	}

	ServerImpl()
	{
		// Bean test
	}

	@Override
	public void broadcast(String message)
	{
		this.broadcastWithPermission(message, null);
	}

	@Override
	public void broadcastMessage(int messageId)
	{
		this.broadcastMessage(messageId, null);
	}

	@Override
	public void broadcastMessage(int messageId, Map<String, String> variables)
	{
		this.broadcastMessageWithPermission(null, messageId, variables);
	}

	@Override
	public void broadcastMessageWithPermission(String permission, int messageId)
	{
		this.broadcastMessageWithPermission(permission, messageId, null);
	}

	@Override
	public void broadcastMessageWithPermission(String permission, int messageId, Map<String, String> variables)
	{
		this.provider.getNetworkModule().broadcastMessageWithPermission(this.serverId, permission, messageId, variables);
	}

	@Override
	public void broadcastWithPermission(String message, String permission)
	{
		this.provider.getNetworkModule().broadcastWithPermission(this.serverId, permission, message);
	}

	@Override
	public void changeName(String name)
	{
		Validate.notNull(name, "Name cannot be null");
		ServerPojo pojo = this.toPojo();
		pojo.setName(name);
		this.provider.getDAO().getServerDAO().updateServer(pojo);
		this.name = name;
		this.notifyServersOfUpdate();
	}

	@Override
	public void changeOnline(boolean isOnline)
	{
		ServerPojo pojo = this.toPojo();
		pojo.setOnline(isOnline);
		this.provider.getDAO().getServerDAO().updateServer(pojo);
		this.isOnline = isOnline;
		this.notifyServersOfUpdate();
	}

	@Override
	public void changeSocketAuto(boolean socketAuto)
	{
		ServerPojo pojo = this.toPojo();
		pojo.setSocketAuto(socketAuto);
		this.provider.getDAO().getServerDAO().updateServer(pojo);
		this.setPojo(pojo);
		this.notifyServersOfUpdate();
	}

	@Override
	public void changeSocketHost(String host)
	{
		ServerPojo pojo = this.toPojo();
		pojo.setSocketHost(host);
		this.provider.getDAO().getServerDAO().updateServer(pojo);
		this.socketHost = host;
		this.notifyServersOfUpdate();
	}

	@Override
	public void changeSocketKey(String key)
	{
		ServerPojo pojo = this.toPojo();
		pojo.setSocketKey(key);
		this.provider.getDAO().getServerDAO().updateServer(pojo);
		this.socketKey = key;
		this.notifyServersOfUpdate();
	}

	@Override
	public void changeSocketPort(int port)
	{
		ServerPojo pojo = this.toPojo();
		pojo.setSocketPort(port);
		this.provider.getDAO().getServerDAO().updateServer(pojo);
		this.socketPort = port;
		this.notifyServersOfUpdate();
	}

	@Override
	public IServerLog createLog(Date date, Level level, String message)
	{
		return this.createLog(date, level, message, 0);
	}

	@Override
	public IServerLog createLog(Date date, Level level, String message, int skipped)
	{
		ServerLogPojo pojo = new ServerLogPojo(0, this.serverId, date, level == null ? null : level.getName(), message, skipped);
		pojo.setLogId(this.provider.getDAO().getServerDAO().createLog(pojo));
		return ServerFactory.createLog(this.provider, pojo);
	}

	@Override
	public void createResourceUsage(ServerResourceUsagePojo usage)
	{
		usage.setServerId(this.serverId);
		this.provider.getDAO().getServerDAO().createServerResourceUsage(usage);
	}

	@Override
	public ServerRuntime createRuntime()
	{
		ServerRuntimePojo pojo = new ServerRuntimePojo(0, this.serverId, new java.util.Date(), new java.util.Date(), null);
		pojo.setRuntimeId(this.provider.getDAO().getServerDAO().createRuntime(pojo));
		return ServerFactory.createRuntime(this.provider, pojo);
	}

	@Override
	public ServerWorld createWorld(String name)
	{
		this.initWorlds();
		ServerWorldPojo pojo = new ServerWorldPojo(0, this.serverId, name);
		pojo.setWorldId(this.provider.getDAO().getWorldDAO().createWorld(pojo));
		ServerWorld world = this.provider.getWorldModule().getFactory().createWorld(pojo);
		this.worlds.add(world);
		return world;
	}

	@Override
	public List<UserConnect> findOpenConnects()
	{
		List<UserConnect> list = new ArrayList<>();
		this.provider.getDAO().getUserDAO().findOpenConnects(this.serverId).forEach(pojo ->
		{
			list.add(this.provider.getUserModule().getFactory().createConnect(pojo));
		});
		return list;
	}

	@Override
	public List<ServerRuntime> findOpenRuntimes()
	{
		List<ServerRuntime> list = new ArrayList<>();
		this.provider.getDAO().getServerDAO().findOpenRuntimes(this.serverId).forEach(pojo ->
		{
			list.add(ServerFactory.createRuntime(this.provider, pojo));
		});
		return list;
	}

	@Override
	public ServerRuntime findRuntime()
	{
		return this.runtime.getCache();
	}

	@Override
	public ServerWorld findWorld(World bukkitWorld)
	{
		this.initWorlds();
		ServerWorld serverWorld =
			this.worlds.stream().filter(world -> world.getName().equalsIgnoreCase(bukkitWorld.getName())).findAny().orElse(null);
		if (serverWorld == null)
		{
			serverWorld = this.createWorld(bukkitWorld.getName());
		} else
		{
			if (!bukkitWorld.getName().equals(serverWorld.getName()))
			{
				serverWorld.changeName(bukkitWorld.getName());
			}
		}
		return serverWorld;
	}

	@Override
	public ServerResourceUsagePojo getLatestResourceUsage()
	{
		return this.provider.getDAO().getServerDAO().findLatestServerResourceUsage(this.serverId);
	}

	@Override
	public String getName()
	{
		return this.name;
	}

	@Override
	public int getServerId()
	{
		return this.serverId;
	}

	@Override
	public List<ServerPlugin> getServerPlugins()
	{
		return this.provider.getDAO().getPluginDAO().findServerPlugins(this.serverId).stream()
			.map(serverPlugin -> this.provider.getPluginModule().getFactory().createServerPlugin(serverPlugin))
			.collect(Collectors.toList());
	}

	@Override
	public String getSocketBindHost()
	{
		return this.socketBindHost;
	}

	@Override
	public String getSocketHost()
	{
		return this.socketHost;
	}

	@Override
	public String getSocketKey()
	{
		return this.socketKey;
	}

	@Override
	public Integer getSocketPort()
	{
		return this.socketPort;
	}

	@Override
	public ServerType getType()
	{
		return this.type;
	}

	@Override
	public int getUserCount()
	{
		return this.findOpenConnects().size();
	}

	@Override
	public boolean isOnline()
	{
		return this.isOnline;
	}

	@Override
	public boolean isSocketAuto()
	{
		return this.socketAuto;
	}

	@Override
	public void notifyServersOfUpdate()
	{
		this.provider.getSocketModule().sendAll(this.provider.getNetworkPlugin().getPluginId(),
			NetworkCoreSocketMessage.UPDATE_SERVER.name(), new ServerSocketRequest(this.toPojo())
			{

				@Override
				public void onResponse(IServerSocketResponse response)
				{}
			});
	}

	@Override
	public void refresh()
	{
		ServerPojo pojo = this.provider.getDAO().getServerDAO().findServer(this.serverId);
		this.setPojo(pojo);
	}

	@Override
	public void reload()
	{
		this.provider.getSocketModule().send(this.serverId, this.provider.getNetworkPlugin().getPluginId(),
			NetworkCoreSocketMessage.RELOAD_SERVER.name(), new IServerSocketRequest()
			{

				@Override
				public byte[] getData()
				{
					return null;
				}

				@Override
				public void onResponse(IServerSocketResponse response)
				{}
			});
	}

	@Override
	public void restart()
	{
		this.provider.getSocketModule().send(this.serverId, this.provider.getNetworkPlugin().getPluginId(),
			NetworkCoreSocketMessage.RESTART_SERVER.name(), new IServerSocketRequest()
			{

				@Override
				public byte[] getData()
				{
					return null;
				}

				@Override
				public void onResponse(IServerSocketResponse response)
				{}
			});
	}

	@Override
	public void setPojo(ServerPojo pojo)
	{
		this.name = pojo.getName();
		this.type = pojo.getType();
		this.isOnline = pojo.isOnline();
		this.socketAuto = pojo.isSocketAuto();
		this.socketHost = pojo.getSocketHost();
		this.socketPort = pojo.getSocketPort();
		this.socketKey = pojo.getSocketKey();
	}

	/**
	 * Set the runtime.
	 * 
	 * @param runtime
	 */
	@Override
	public void setRuntimeCache(ServerRuntime runtime)
	{
		this.runtime.setCache(runtime);
	}

	@Override
	public void stop()
	{
		this.provider.getSocketModule().send(this.serverId, this.provider.getNetworkPlugin().getPluginId(),
			NetworkCoreSocketMessage.STOP_SERVER.name(), new IServerSocketRequest()
			{

				@Override
				public byte[] getData()
				{
					return null;
				}

				@Override
				public void onResponse(IServerSocketResponse response)
				{}
			});
	}

	@Override
	public ServerPojo toPojo()
	{
		return new ServerPojo(this.serverId, this.name, this.type, this.isOnline, this.socketAuto, this.socketBindHost, this.socketHost,
			this.socketPort, this.socketKey);
	}

	@Override
	public String toString()
	{
		return "Server [serverId=" + this.serverId + ", name=" + this.name + ", type=" + this.type + "]";
	}

	@Override
	public boolean wasOnline(int period, TimeUnit timeUnit)
	{
		ServerRuntime runtime = this.findRuntime();
		if (runtime == null) { return false; }
		Date stopDate = runtime.getStopDate();
		if (stopDate == null)
		{
			// Server is still online
			return true;
		}
		return stopDate.getTime() + timeUnit.toMillis(period) > System.currentTimeMillis();
	}

	private void initWorlds()
	{
		if (this.worlds != null) { return; }
		this.worlds = new HashSet<>();
		this.provider.getDAO().getWorldDAO().findWorlds(this.serverId).forEach(pojo ->
		{
			this.worlds.add(this.provider.getWorldModule().getFactory().createWorld(pojo));
		});
	}
}
