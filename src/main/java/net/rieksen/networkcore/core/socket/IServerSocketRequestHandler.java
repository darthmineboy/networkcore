package net.rieksen.networkcore.core.socket;

public interface IServerSocketRequestHandler
{

	/**
	 * Process the request, and return an optional response. If null is returned
	 * a default response is sent.
	 * 
	 * @param id
	 * @param data
	 * @return
	 */
	IServerSocketResponse process(byte[] data);
}
