package net.rieksen.networkcore.core.socket;

public class SocketStatusPojo
{

	private int		serverId;
	private String	msg;

	public SocketStatusPojo(int serverId, String msg)
	{
		this.serverId = serverId;
		this.msg = msg;
	}

	public String getMsg()
	{
		return this.msg;
	}

	public int getServerId()
	{
		return this.serverId;
	}

	public void setMsg(String msg)
	{
		this.msg = msg;
	}

	public void setServerId(int serverId)
	{
		this.serverId = serverId;
	}
}
