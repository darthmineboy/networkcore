package net.rieksen.networkcore.core.socket;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Enumeration;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ServerSocketFactory;
import javax.net.SocketFactory;

import org.apache.commons.lang.RandomStringUtils;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.enums.NetworkCoreSocketMessage;
import net.rieksen.networkcore.core.info.NetworkCoreInfo;
import net.rieksen.networkcore.core.module.ModuleImpl;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.core.server.Server;
import net.rieksen.networkcore.core.server.pojo.ServerPojo;

public class ServerSocketModuleImpl extends ModuleImpl implements ServerSocketModule
{

	private static final int	SOCKET_CONNECT_TIMEOUT	= 5000;
	private static final int	SOCKET_SO_TIMEOUT		= 5000;
	private SecretKeySpec		secretKey;
	protected NetworkCore		provider;
	private ServerSocket		serverSocket;
	private boolean				enabled;

	public ServerSocketModuleImpl(NetworkCore provider)
	{
		this.provider = provider;
	}

	@Override
	public void destroyModule()
	{
		this.enabled = false;
		try
		{
			if (this.serverSocket != null)
			{
				this.serverSocket.close();
			}
		} catch (IOException e)
		{}
	}

	@Override
	public void initModule()
	{
		this.enabled = true;
		String encryptionSecret = this.provider.getInfoModule().getInfo(NetworkCoreInfo.SOCKET_ENCRYPTION_SECRET);
		if (encryptionSecret == null)
		{
			try
			{
				KeyGenerator keyGen = KeyGenerator.getInstance("AES");
				keyGen.init(128);
				SecretKey secretKey = keyGen.generateKey();
				encryptionSecret = Base64.getEncoder().encodeToString(secretKey.getEncoded());
				this.provider.getInfoModule().setInfo(NetworkCoreInfo.SOCKET_ENCRYPTION_SECRET, encryptionSecret);
			} catch (NoSuchAlgorithmException e)
			{
				throw new RuntimeException("Failed to generate AES key", e);
			}
		}
		this.setEncryptionSecret(encryptionSecret);
		try
		{
			this.serverSocket = ServerSocketFactory.getDefault().createServerSocket();
			Server server = this.provider.getServerModule().getLocalServer();
			String host = server.isSocketAuto() || server.getSocketHost() == null ? this.getHost() : server.getSocketHost();
			int port = server.isSocketAuto() || server.getSocketPort() == null ? 0 : server.getSocketPort();
			try
			{
				String bind = server.getSocketBindHost();
				if (bind == null || bind.isEmpty())
				{
					bind = "0.0.0.0";
				}
				this.serverSocket.bind(new InetSocketAddress(bind, port));
				port = this.serverSocket.getLocalPort();
				this.provider.getLogger().info("Listening to " + bind + "/" + host + ":" + port);
			} catch (Exception e)
			{
				this.provider.getLogger().severe("Failed to bind server socket to port " + host + ":" + port);
				e.printStackTrace();
				return;
			}
			ServerPojo serverPojo = server.toPojo();
			serverPojo.setSocketHost(host);
			serverPojo.setSocketPort(this.serverSocket.getLocalPort());
			String socketKey = RandomStringUtils.randomAscii(64);
			serverPojo.setSocketKey(socketKey);
			server.setPojo(serverPojo);
			// The server online status is changed to true in onEnable after
			// this, so we do
			// not need to update the server in the DAO.
			if (server.isOnline())
			{
				this.provider.getDAO().getServerDAO().updateServer(server.toPojo());
				server.notifyServersOfUpdate();
			}
			new Thread(new Runnable()
			{

				@Override
				public void run()
				{
					while (ServerSocketModuleImpl.this.enabled)
					{
						try
						{
							Socket socket = ServerSocketModuleImpl.this.serverSocket.accept();
							new Thread(new Runnable()
							{

								private Socket socket;

								public Runnable init(Socket socket)
								{
									this.socket = socket;
									return this;
								}

								@Override
								public void run()
								{
									try (Socket socket = this.socket)
									{
										socket.setSoTimeout(5000);
										DataInputStream msginEncrypted = new DataInputStream(socket.getInputStream());
										byte[] bytesReceived = new byte[msginEncrypted.readInt()];
										ByteStreams.readFully(msginEncrypted, bytesReceived);
										ByteArrayDataInput msgin =
											ByteStreams.newDataInput(ServerSocketModuleImpl.this.decrypt(bytesReceived));
										Server server = ServerSocketModuleImpl.this.provider.getServerModule().getLocalServer();
										// Network Id
										msgin.readInt();
										int serverId = msgin.readInt();
										if (server.getServerId() != serverId)
										{
											ServerSocketModuleImpl.this.writeResponse(socket, false, 401, "The socket message was refused",
												null);
											return;
										}
										String socketKey = msgin.readUTF();
										if (!server.getSocketKey().equals(socketKey))
										{
											ServerSocketModuleImpl.this.writeResponse(socket, false, 401, "The socket message was refused",
												null);
											return;
										}
										int pluginId = msgin.readInt();
										String id = msgin.readUTF();
										boolean broadcast = msgin.readBoolean();
										byte[] data = new byte[msgin.readInt()];
										msgin.readFully(data);
										if (broadcast)
										{
											ServerSocketModuleImpl.this.sendAll(pluginId, id, new ServerSocketRequest(data)
											{

												@Override
												public void onResponse(IServerSocketResponse response)
												{}
											});
										}
										NetworkPlugin plugin = ServerSocketModuleImpl.this.provider.getPluginModule().getPlugin(pluginId);
										IServerSocketResponse response = null;
										if (id.startsWith("SERVERCOMMUNICATION"))
										{
											ServerSocketModuleImpl.this.provider.getLogger().warning("The plugin #" + pluginId
												+ " sent a socket message using deprecated code. While this is supported, it should soon be updated.");
											/*
											 * ServerSocketModuleImpl.this.
											 * provider.getCommunicationManager(
											 * ).onServerCommunicationReceived(
											 * new ServerCommunication(
											 * ServerSocketModuleImpl.this.
											 * provider.getCommunicationManager(
											 * ), 0, serverId, pluginId,
											 * Integer.parseInt(id.split(":")[1]
											 * ), data));
											 */
										}
										if (plugin != null)
										{
											try
											{
												response = plugin.processSocketMessage(id, data);
											} catch (Exception e)
											{
												e.printStackTrace();
												response = new ServerSocketResponse(false, 500,
													"An exception occurred while processing the socket message", null);
											}
										}
										response = response == null
											? new ServerSocketResponse(true, 200, "The socket message was processed", null) : response;
										ServerSocketModuleImpl.this.writeResponse(socket, response.success(), response.getCode(),
											response.getMessage(), response.getData());
									} catch (Exception e)
									{
										if (!this.socket.isClosed())
										{
											ServerSocketModuleImpl.this.writeResponse(this.socket, false, 500,
												"An unhandled exception occurred: " + e.getMessage(), null);
										}
										return;
									}
								}
							}.init(socket)).start();
						} catch (Exception e)
						{
							continue;
						}
					}
				}
			}).start();
		} catch (Exception e)
		{
			// Silence
		}
		this.registerSocketRequestHandlers();
	}

	@Override
	public void send(int serverId, int pluginId, String id, IServerSocketRequest request)
	{
		this.send(serverId, pluginId, id, request, false);
	}

	@Override
	public void send(int serverId, int pluginId, String id, IServerSocketRequest request, boolean broadcast)
	{
		this.send(serverId, pluginId, id, request, broadcast, true);
	}

	@Override
	public void sendAll(int pluginId, String id, IServerSocketRequest request)
	{
		new Thread(new Runnable()
		{

			@Override
			public void run()
			{
				int local = ServerSocketModuleImpl.this.provider.getServerModule().getLocalServer().getServerId();
				ServerSocketModuleImpl.this.provider.getServerModule().getServers().stream().filter(server -> server.getServerId() != local)
					.forEach(server -> ServerSocketModuleImpl.this.send(server.getServerId(), pluginId, id, request));
			}
		}).start();
	}

	protected void setEncryptionSecret(String encryptionSecret)
	{
		this.secretKey = new SecretKeySpec(Base64.getDecoder().decode(encryptionSecret), "AES");
	}

	byte[] decrypt(byte[] bytes)
	{
		try
		{
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.DECRYPT_MODE, this.secretKey);
			return cipher.doFinal(bytes);
		} catch (Exception e)
		{
			throw new RuntimeException("Failed to decrypt", e);
		}
	}

	byte[] encrypt(byte[] bytes)
	{
		try
		{
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, this.secretKey);
			return cipher.doFinal(bytes);
		} catch (Exception e)
		{
			throw new RuntimeException("Failed to encrypt", e);
		}
	}

	private String getHost() throws Exception
	{
		Enumeration<NetworkInterface> ns = NetworkInterface.getNetworkInterfaces();
		while (ns.hasMoreElements())
		{
			NetworkInterface n = ns.nextElement();
			Enumeration<InetAddress> ad = n.getInetAddresses();
			while (ad.hasMoreElements())
			{
				InetAddress a = ad.nextElement();
				if (!(a instanceof Inet4Address))
				{
					continue;
				}
				if (!a.isReachable(1000))
				{
					continue;
				}
				return a.getHostAddress();
			}
		}
		throw new IllegalStateException("Could not find host for socket to bind on");
	}

	private void registerSocketRequestHandlers()
	{
		NetworkPlugin plugin = this.provider.getNetworkPlugin();
		plugin.registerSocketRequestHandler(NetworkCoreSocketMessage.STATUS.name(), new ServerSocketRequestHandler()
		{

			@Override
			public IServerSocketResponse process(byte[] data)
			{
				SocketStatusPojo status =
					new SocketStatusPojo(ServerSocketModuleImpl.this.provider.getServerModule().getLocalServer().getServerId(), "OK");
				return new ServerSocketResponse(true, 200, "OK", status);
			}
		});
	}

	private void send(int serverId, int pluginId, String id, IServerSocketRequest request, boolean broadcast, boolean firstAttempt)
	{
		new Thread(new Runnable()
		{

			@Override
			public void run()
			{
				Server server = ServerSocketModuleImpl.this.provider.getServerModule().getServer(serverId);
				if (server == null || !server.isOnline()) { return; }
				String socketHost = server.getSocketHost();
				Integer socketPort = server.getSocketPort();
				String socketKey = server.getSocketKey();
				if (socketHost == null || socketPort == null || socketKey == null) { return; }
				try (Socket socket = SocketFactory.getDefault().createSocket())
				{
					socket.setSoTimeout(ServerSocketModuleImpl.SOCKET_SO_TIMEOUT);
					socket.connect(new InetSocketAddress(socketHost, socketPort), ServerSocketModuleImpl.SOCKET_CONNECT_TIMEOUT);
					ByteArrayDataOutput out = ByteStreams.newDataOutput();
					String networkId = ServerSocketModuleImpl.this.provider.getInfoModule().getInfo(NetworkCoreInfo.NETWORK_ID);
					out.writeInt(networkId == null ? 0 : Integer.parseInt(networkId));
					out.writeInt(serverId);
					out.writeUTF(socketKey);
					out.writeInt(pluginId);
					out.writeUTF(id);
					out.writeBoolean(broadcast);
					byte[] data = request == null || request.getData() == null ? new byte[0] : request.getData();
					out.writeInt(data.length);
					out.write(data);
					byte[] encrypted = ServerSocketModuleImpl.this.encrypt(out.toByteArray());
					ByteArrayDataOutput outEncrypted = ByteStreams.newDataOutput();
					outEncrypted.writeInt(encrypted.length);
					outEncrypted.write(encrypted);
					socket.getOutputStream().write(outEncrypted.toByteArray());
					DataInputStream msgin = new DataInputStream(socket.getInputStream());
					byte[] bytesResponse = new byte[msgin.readInt()];
					ByteStreams.readFully(msgin, bytesResponse);
					bytesResponse = ServerSocketModuleImpl.this.decrypt(bytesResponse);
					ByteArrayDataInput response = ByteStreams.newDataInput(bytesResponse);
					boolean success = response.readBoolean();
					int code = response.readInt();
					String message = response.readUTF();
					data = new byte[response.readInt()];
					response.readFully(data);
					if (!success)
					{
						if (code == 401 && firstAttempt)
						{
							ServerSocketModuleImpl.this.provider
								.debug("The socket code was 401 UNAUTHORIZED, refreshing server then resending message");
							server.refresh();
							ServerSocketModuleImpl.this.send(serverId, pluginId, id, request, broadcast, false);
							return;
						} else
						{
							ServerSocketModuleImpl.this.provider.getLogger().warning(
								"A socket message sent by plugin #" + pluginId + " returned with the error code " + code + ": " + message);
						}
					}
					if (request != null)
					{
						request.onResponse(new ServerSocketResponse(success, code, message, data));
					}
				} catch (Exception e)
				{
					// Silence
				}
			}
		}).start();
	}

	private void writeResponse(Socket socket, boolean success, int code, String message, byte[] data)
	{
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeBoolean(success);
		out.writeInt(code);
		out.writeUTF(message);
		data = data == null ? new byte[0] : data;
		out.writeInt(data.length);
		out.write(data);
		try
		{
			byte[] encrypted = this.encrypt(out.toByteArray());
			ByteArrayDataOutput outEncrypted = ByteStreams.newDataOutput();
			outEncrypted.writeInt(encrypted.length);
			outEncrypted.write(encrypted);
			socket.getOutputStream().write(outEncrypted.toByteArray());
		} catch (IOException e)
		{
			// Silence
		}
	}
}
