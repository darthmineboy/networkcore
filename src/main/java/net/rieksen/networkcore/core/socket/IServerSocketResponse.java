package net.rieksen.networkcore.core.socket;

public interface IServerSocketResponse extends IServerSocketMessage
{

	/**
	 * An error, or success code.
	 * 
	 * @return
	 */
	int getCode();

	/**
	 * An error, or success message.
	 * 
	 * @return
	 */
	String getMessage();

	/**
	 * Deserializes the bytes to an object of given class.
	 * 
	 * @param clazz
	 * @return
	 */
	<T> T getObject(Class<T> clazz);

	/**
	 * Whether the request was successfully processed.
	 * 
	 * @return
	 */
	boolean success();
}
