package net.rieksen.networkcore.core.socket;

import com.google.gson.Gson;

public class ServerSocketResponse implements IServerSocketResponse
{

	private boolean	success;
	private int		code;
	private String	message;
	private byte[]	data;

	public ServerSocketResponse(boolean success, int code, String message, byte[] data)
	{
		this.success = success;
		this.code = code;
		this.message = message;
		this.data = data;
	}

	public ServerSocketResponse(boolean success, int code, String message, Object data)
	{
		this.success = success;
		this.code = code;
		this.message = message;
		this.data = new Gson().toJson(data).getBytes();
	}

	@Override
	public int getCode()
	{
		return this.code;
	}

	@Override
	public byte[] getData()
	{
		return this.data;
	}

	@Override
	public String getMessage()
	{
		return this.message;
	}

	@Override
	public <T> T getObject(Class<T> clazz)
	{
		return new Gson().fromJson(new String(this.data), clazz);
	}

	@Override
	public boolean success()
	{
		return this.success;
	}
}
