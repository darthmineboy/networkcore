package net.rieksen.networkcore.core.socket;

import java.nio.charset.Charset;

import net.rieksen.networkcore.core.module.Module;

public interface ServerSocketModule extends Module
{

	public static final Charset CHARSET = Charset.forName("UTF-8");

	/**
	 * Sends the request to the server. This is a convenience method for
	 * {@link #send(int, IServerSocketRequest, boolean)}, where
	 * <code>broadcast = false</code>.
	 * 
	 * @param pluginId
	 * @param serverId
	 * @param request
	 */
	void send(int serverId, int pluginId, String id, IServerSocketRequest request);

	/**
	 * Sends the request to the server.
	 * 
	 * @param serverId
	 * @param pluginId
	 * @param request
	 * @param broadcast
	 *            Whether the receiving server should broadcast the message to
	 *            all other servers. This is used by the NetworkCore website so
	 *            it only has to connect to a single server.
	 */
	void send(int serverId, int pluginId, String id, IServerSocketRequest request, boolean broadcast);

	/**
	 * Sends the request to all servers except this server.
	 * 
	 * @param pluginId
	 * @param id
	 * @param data
	 */
	void sendAll(int pluginId, String id, IServerSocketRequest request);
}
