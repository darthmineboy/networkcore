package net.rieksen.networkcore.core.socket;

import com.google.gson.Gson;

public class ServerSocketRequest implements IServerSocketRequest
{

	private byte[] data;

	public ServerSocketRequest(byte[] data)
	{
		this.data = data;
	}

	public ServerSocketRequest(Object object)
	{
		this.data = new Gson().toJson(object).getBytes(ServerSocketModule.CHARSET);
	}

	@Override
	public final byte[] getData()
	{
		return this.data;
	}

	@Override
	public void onResponse(IServerSocketResponse response)
	{}
}
