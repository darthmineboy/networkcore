package net.rieksen.networkcore.core.socket;

public interface IServerSocketMessage
{

	byte[] getData();
}
