package net.rieksen.networkcore.core.socket;

import com.google.gson.Gson;

public abstract class ServerSocketRequestHandler implements IServerSocketRequestHandler
{

	public <T> T getObject(byte[] data, Class<T> clazz)
	{
		return new Gson().fromJson(new String(data, ServerSocketModule.CHARSET), clazz);
	}
}
