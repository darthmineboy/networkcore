package net.rieksen.networkcore.core.socket;

public interface IServerSocketRequest extends IServerSocketMessage
{

	void onResponse(IServerSocketResponse response);
}
