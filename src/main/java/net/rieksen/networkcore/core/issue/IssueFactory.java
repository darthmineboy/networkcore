package net.rieksen.networkcore.core.issue;

public interface IssueFactory
{

	Issue createIssue(IssuePojo issue);
}
