package net.rieksen.networkcore.core.issue;

import java.util.List;

import net.rieksen.networkcore.core.dao.IssueDAO;
import net.rieksen.networkcore.core.module.Module;

/**
 * Issues can be managed through the issue module.
 */
public interface IssueModule extends Module
{

	/**
	 * The maximum length of the issueData field.
	 */
	public static final int MAX_LENGTH_TASK_DATA = 4096;

	/**
	 * Creates an issue.
	 * 
	 * @param issue
	 * @return
	 * @throws IllegalArgumentException
	 *             <code>issue</code> is null
	 */
	Issue createIssue(IssuePojo issue);

	/**
	 * Finds all unresolved issues created by a generator.
	 * 
	 * @param pluginId
	 * @param name
	 * @return
	 */
	List<Issue> findUnresolvedIssuesOfGenerator(int pluginId, String name);

	IssueDAO getDAO();

	IssueFactory getFactory();
}
