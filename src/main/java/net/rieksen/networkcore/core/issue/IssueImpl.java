package net.rieksen.networkcore.core.issue;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

import net.rieksen.networkcore.core.NetworkCore;

public class IssueImpl implements Issue
{

	private NetworkCore	provider;
	private int			issueId;
	private int			pluginId;
	private String		issueName;
	private int			priority;
	private Date		creationDate;
	private String		description;
	private String		issueGenerator;
	private boolean		resolved;
	private String		issueData;

	public IssueImpl(NetworkCore provider, IssuePojo issue)
	{
		this.provider = provider;
		this.setPojo(issue);
	}

	@Override
	public void changeDescription(String description)
	{
		if (StringUtils.equals(this.description, description)) { return; }
		IssuePojo pojo = this.toPojo();
		pojo.setDescription(description);
		pojo = this.provider.getDAO().getIssueDAO().updateIssue(this.issueId, pojo);
		this.setPojo(pojo);
	}

	@Override
	public void changeResolved(boolean resolved)
	{
		if (this.resolved == resolved) { return; }
		IssuePojo pojo = this.toPojo();
		pojo.setResolved(resolved);
		pojo = this.provider.getDAO().getIssueDAO().updateIssue(this.issueId, pojo);
		this.setPojo(pojo);
	}

	@Override
	public Date getCreationDate()
	{
		return this.creationDate;
	}

	@Override
	public String getDescription()
	{
		return this.description;
	}

	@Override
	public int getPluginId()
	{
		return this.pluginId;
	}

	@Override
	public int getPriority()
	{
		return this.priority;
	}

	@Override
	public String getIssueData()
	{
		return this.issueData;
	}

	@Override
	public String getIssueGeneratorName()
	{
		return this.issueGenerator;
	}

	@Override
	public int getIssueId()
	{
		return this.issueId;
	}

	@Override
	public String getIssueName()
	{
		return this.issueName;
	}

	@Override
	public boolean isResolved()
	{
		return this.resolved;
	}

	public IssuePojo toPojo()
	{
		return new IssuePojo(this.issueId, this.pluginId, this.issueName, this.priority, this.creationDate, this.description,
			this.issueGenerator, this.resolved, this.issueData);
	}

	private void setPojo(IssuePojo issue)
	{
		this.issueId = issue.getIssueId();
		this.pluginId = issue.getPluginId();
		this.issueName = issue.getIssueName();
		this.priority = issue.getPriority();
		this.creationDate = issue.getCreationDate();
		this.description = issue.getDescription();
		this.issueGenerator = issue.getIssueGenerator();
		this.resolved = issue.isResolved();
		this.issueData = issue.getIssueData();
	}
}
