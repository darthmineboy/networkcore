package net.rieksen.networkcore.core.issue;

import net.rieksen.networkcore.core.NetworkCore;

public class IssueFactoryImpl implements IssueFactory
{

	private NetworkCore provider;

	public IssueFactoryImpl(NetworkCore provider)
	{
		this.provider = provider;
	}

	@Override
	public Issue createIssue(IssuePojo issue)
	{
		return new IssueImpl(this.provider, issue);
	}
}
