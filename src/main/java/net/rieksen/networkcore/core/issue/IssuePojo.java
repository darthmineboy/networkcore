package net.rieksen.networkcore.core.issue;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class IssuePojo
{

	private int		issueId;
	private int		pluginId;
	private String	issueName;
	private int		priority;
	private Date	creationDate;
	private String	description;
	private String	issueGenerator;
	private boolean	resolved;
	private String	issueData;

	public IssuePojo(int issueId, int pluginId, String issueName, int priority, Date creationDate, String description, String issueGenerator,
		boolean resolved, String issueData)
	{
		this.issueId = issueId;
		this.pluginId = pluginId;
		this.issueName = issueName;
		this.priority = priority;
		this.creationDate = creationDate;
		this.description = description;
		this.issueGenerator = issueGenerator;
		this.resolved = resolved;
		this.issueData = issueData;
	}

	IssuePojo()
	{
		// Bean Test
	}

	public Date getCreationDate()
	{
		return this.creationDate;
	}

	public String getDescription()
	{
		return this.description;
	}

	public int getPluginId()
	{
		return this.pluginId;
	}

	public int getPriority()
	{
		return this.priority;
	}

	public String getIssueData()
	{
		return this.issueData;
	}

	public String getIssueGenerator()
	{
		return this.issueGenerator;
	}

	public int getIssueId()
	{
		return this.issueId;
	}

	public String getIssueName()
	{
		return this.issueName;
	}

	public boolean isResolved()
	{
		return this.resolved;
	}

	public void setCreationDate(Date creationDate)
	{
		this.creationDate = creationDate;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setPluginId(int pluginId)
	{
		this.pluginId = pluginId;
	}

	public void setPriority(int priority)
	{
		this.priority = priority;
	}

	public void setResolved(boolean resolved)
	{
		this.resolved = resolved;
	}

	public void setIssueData(String issueData)
	{
		this.issueData = issueData;
	}

	public void setIssueGenerator(String issueGenerator)
	{
		this.issueGenerator = issueGenerator;
	}

	public void setIssueId(int issueId)
	{
		this.issueId = issueId;
	}

	public void setIssueName(String issueName)
	{
		this.issueName = issueName;
	}
}
