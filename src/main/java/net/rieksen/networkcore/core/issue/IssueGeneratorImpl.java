package net.rieksen.networkcore.core.issue;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.Validate;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;

public abstract class IssueGeneratorImpl implements IssueGenerator
{

	protected final NetworkCore	provider;
	protected final int			pluginId;
	protected final String		name;

	/**
	 * @param pluginId
	 * @param name
	 *            Name of the generator
	 */
	public IssueGeneratorImpl(NetworkCore provider, int pluginId, String name)
	{
		this.provider = provider;
		this.pluginId = pluginId;
		this.name = name;
	}

	@Override
	public Issue createIssue(IssuePojo issue)
	{
		Validate.notNull(issue);
		issue.setIssueGenerator(this.name);
		return this.provider.getIssueModule().createIssue(issue);
	}

	public List<Issue> filterIssuesWithName(String name, List<Issue> issues)
	{
		return issues.stream().filter(issue -> issue.getIssueName().equalsIgnoreCase(name)).collect(Collectors.toList());
	}

	@Override
	public List<Issue> findUnresolvedIssues()
	{
		return this.provider.getIssueModule().findUnresolvedIssuesOfGenerator(this.pluginId, this.name);
	}

	@Override
	public String getName()
	{
		return this.name;
	}

	@Override
	public NetworkPlugin getPlugin()
	{
		return this.provider.getPluginModule().getPlugin(this.pluginId);
	}
}
