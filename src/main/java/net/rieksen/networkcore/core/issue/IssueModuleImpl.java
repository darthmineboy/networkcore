package net.rieksen.networkcore.core.issue;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.Validate;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.dao.IssueDAO;
import net.rieksen.networkcore.core.module.ModuleImpl;

public class IssueModuleImpl extends ModuleImpl implements IssueModule
{

	private NetworkCore	provider;
	private IssueFactory	factory;

	public IssueModuleImpl(NetworkCore provider)
	{
		this.provider = provider;
		this.factory = new IssueFactoryImpl(provider);
	}

	@Override
	public Issue createIssue(IssuePojo issue)
	{
		Validate.notNull(issue);
		issue = this.getDAO().createIssue(issue);
		return this.getFactory().createIssue(issue);
	}

	@Override
	public List<Issue> findUnresolvedIssuesOfGenerator(int pluginId, String name)
	{
		return this.getDAO().findUnresolvedIssuesOfGenerator(pluginId, name).stream().map((issue) -> this.getFactory().createIssue(issue))
			.collect(Collectors.toList());
	}

	@Override
	public IssueDAO getDAO()
	{
		return this.provider.getDAO().getIssueDAO();
	}

	@Override
	public IssueFactory getFactory()
	{
		return this.factory;
	}

	@Override
	protected void destroyModule() throws Exception
	{}

	@Override
	protected void initModule() throws Exception
	{}
}
