package net.rieksen.networkcore.core.issue.generator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.issue.Issue;
import net.rieksen.networkcore.core.issue.IssueGeneratorImpl;
import net.rieksen.networkcore.core.issue.IssuePojo;
import net.rieksen.networkcore.core.server.Server;

/**
 * Checks whether there may be two servers running with the same serverId.
 */
public class DuplicateServerIdIssueGenerator extends IssueGeneratorImpl
{

	public static final String ISSUE_DESCRIPTION =
		"The serverId #%server-id% %server-name% is being used by multiple servers and thus not unique for every server. NetworkCore uses the serverId to e.g. store on which server chat messages were sent, realtime communication between servers and much more. It is VERY important that you fix this. For more information see https://networkcore.org/resources/NetworkCore.1/topics/Unique%20Server%20Id.8";

	public DuplicateServerIdIssueGenerator(NetworkCore provider)
	{
		super(provider, provider.getNetworkPlugin().getPluginId(), "Duplicate ServerId");
	}

	@Override
	public List<Issue> generateIssues()
	{
		List<Issue> issues = this.findUnresolvedIssues();
		List<Issue> generated = new ArrayList<>();
		for (Server server : this.provider.getServerModule().getServers())
		{
			Issue existingIssue =
				issues.stream().filter(issue -> Integer.parseInt(issue.getIssueData()) == server.getServerId()).findAny().orElse(null);
			existingIssue = this.checkServer(server, existingIssue);
			if (existingIssue != null)
			{
				generated.add(existingIssue);
			}
		}
		return generated;
	}

	private Issue checkServer(Server server, Issue issue)
	{
		boolean problem = server.findOpenRuntimes().size() > 1;
		String description = DuplicateServerIdIssueGenerator.ISSUE_DESCRIPTION.replace("%server-id%", String.valueOf(server.getServerId()))
			.replace("%server-name%", server.getName());
		if (problem)
		{
			if (issue == null)
			{
				IssuePojo newIssue = new IssuePojo(0, this.pluginId, "Duplicate Server Id #" + server.getServerId(), Issue.PRIORITY_HIGH,
					new Date(), description, this.name, false, String.valueOf(server.getServerId()));
				issue = this.createIssue(newIssue);
			}
		}
		if (issue != null)
		{
			if (!issue.isResolved() && !problem)
			{
				issue.changeResolved(true);
			} else
			{
				issue.changeDescription(description);
			}
		}
		return issue;
	}
}
