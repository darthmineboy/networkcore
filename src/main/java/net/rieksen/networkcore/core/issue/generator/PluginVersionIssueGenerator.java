package net.rieksen.networkcore.core.issue.generator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.issue.Issue;
import net.rieksen.networkcore.core.issue.IssueGeneratorImpl;
import net.rieksen.networkcore.core.issue.IssuePojo;
import net.rieksen.networkcore.core.option.Option;
import net.rieksen.networkcore.core.option.OptionSection;
import net.rieksen.networkcore.core.option.OptionType;
import net.rieksen.networkcore.core.option.pojo.OptionPojo;
import net.rieksen.networkcore.core.option.pojo.OptionValuePojo;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.core.plugin.ServerPlugin;
import net.rieksen.networkcore.core.server.Server;

/**
 * Checks whether all servers are running the same version of a plugin.
 */
public class PluginVersionIssueGenerator extends IssueGeneratorImpl
{

	public static final String ISSUE_DESCRIPTION =
		"The plugin %s is installed on multiple servers with different versions. The latest version of the plugin on this network is %s, please update the plugins on all servers to this version or disable the check for this plugin.";

	public PluginVersionIssueGenerator(NetworkCore provider)
	{
		super(provider, provider.getNetworkPlugin().getPluginId(), "Plugin Version");
	}

	@Override
	public List<Issue> generateIssues()
	{
		List<Issue> issues = this.findUnresolvedIssues();
		List<Issue> generated = new ArrayList<>();
		for (NetworkPlugin plugin : this.provider.getPluginModule().getPlugins())
		{
			Issue existingIssue =
				issues.stream().filter(issue -> Integer.parseInt(issue.getIssueData()) == plugin.getPluginId()).findAny().orElse(null);
			existingIssue = this.checkPlugin(plugin, existingIssue);
			if (existingIssue != null)
			{
				generated.add(existingIssue);
			}
		}
		return generated;
	}

	/**
	 * Checks the plugin for version mismatch for the provided plugin.
	 * 
	 * @param plugin
	 * @param issue
	 * @return
	 */
	private Issue checkPlugin(NetworkPlugin plugin, Issue issue)
	{
		String latestVersion = plugin.getLatestVersion();
		List<ServerPlugin> servers = plugin.getServerPlugins();
		boolean mismatch = false;
		if (this.isEnabled(plugin))
		{
			for (ServerPlugin serverPlugin : servers)
			{
				if (serverPlugin.getPluginVersion().equals(latestVersion) || !serverPlugin.isEnabled())
				{
					continue;
				}
				Server server = serverPlugin.getServer();
				if (server.wasOnline(1, TimeUnit.DAYS))
				{
					mismatch = true;
				}
			}
		}
		String description = String.format(PluginVersionIssueGenerator.ISSUE_DESCRIPTION, plugin.getName(), latestVersion);
		if (mismatch)
		{
			if (issue == null)
			{
				IssuePojo newIssue = new IssuePojo(0, this.pluginId, "Mismatching plugin version " + plugin.getName(), Issue.PRIORITY_LOW,
					new Date(), description, this.name, false, String.valueOf(plugin.getPluginId()));
				issue = this.createIssue(newIssue);
			}
		}
		if (issue != null)
		{
			if (!issue.isResolved() && !mismatch)
			{
				issue.changeResolved(true);
			} else
			{
				issue.changeDescription(description);
			}
		}
		return issue;
	}

	private boolean isEnabled(NetworkPlugin plugin)
	{
		OptionSection section = plugin.getOrCreateSection("Config", null);
		Option option = section.getOrCreateOption(new OptionPojo(0, 0, "check-mismatching-plugin-version", OptionType.BOOLEAN,
			"When enabled the version of the plugin on the servers are compared. If any mismatch is found an issue is generated to make you aware of the mismatch."));
		option.createDefault(new OptionValuePojo(0, 0, null, 0, Boolean.toString(true)));
		return option.getBoolean(null);
	}
}
