package net.rieksen.networkcore.core.issue.generator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.enums.NetworkCoreSocketMessage;
import net.rieksen.networkcore.core.issue.Issue;
import net.rieksen.networkcore.core.issue.IssueGeneratorImpl;
import net.rieksen.networkcore.core.issue.IssuePojo;
import net.rieksen.networkcore.core.server.Server;
import net.rieksen.networkcore.core.socket.IServerSocketResponse;
import net.rieksen.networkcore.core.socket.ServerSocketRequest;
import net.rieksen.networkcore.core.util.ValContainer;

public class SocketIssueGenerator extends IssueGeneratorImpl
{

	private static final int	WAIT_RESPONSE_TIMEOUT	= 5000;
	public static final String	ISSUE_DESCRIPTION		=
		"The socket of server %s could not be reached from server %s. For more information see https://networkcore.org/resources/NetworkCore.1/topics/Sockets.7";

	public SocketIssueGenerator(NetworkCore provider)
	{
		super(provider, provider.getNetworkPlugin().getPluginId(), "Socket");
	}

	@Override
	public List<Issue> generateIssues()
	{
		List<Issue> issues = this.findUnresolvedIssues();
		List<Issue> generated = new ArrayList<>();
		for (Server server : this.provider.getServerModule().getServers())
		{
			Issue existingIssue =
				issues.stream().filter(issue -> Integer.parseInt(issue.getIssueData()) == server.getServerId()).findAny().orElse(null);
			existingIssue = this.checkServer(server, existingIssue);
			if (existingIssue != null)
			{
				generated.add(existingIssue);
			}
		}
		return generated;
	}

	private Issue checkServer(Server server, Issue issue)
	{
		boolean problem = false;
		if (server.isOnline())
		{
			try
			{
				Object lock = new Object();
				ValContainer<IServerSocketResponse> val = new ValContainer<>();
				this.provider.getSocketModule().send(server.getServerId(), this.pluginId, NetworkCoreSocketMessage.STATUS.name(),
					new ServerSocketRequest(null)
					{

						@Override
						public void onResponse(IServerSocketResponse response)
						{
							val.setValue(response);
							synchronized (lock)
							{
								lock.notify();
							}
						}
					});
				synchronized (lock)
				{
					lock.wait(SocketIssueGenerator.WAIT_RESPONSE_TIMEOUT);
				}
				IServerSocketResponse response = val.getValue();
				if (response == null)
				{
					problem = true;
				}
			} catch (Exception e)
			{
				problem = true;
			}
		}
		String description = String.format(SocketIssueGenerator.ISSUE_DESCRIPTION, server.getName(),
			this.provider.getServerModule().getLocalServer().getName());
		if (problem)
		{
			if (issue == null)
			{
				IssuePojo newIssue = new IssuePojo(0, this.pluginId, "Socket not reachable " + server.getName(), Issue.PRIORITY_HIGH,
					new Date(), description, this.name, false, String.valueOf(server.getServerId()));
				issue = this.createIssue(newIssue);
			}
		}
		if (issue != null)
		{
			if (!issue.isResolved() && !problem)
			{
				issue.changeResolved(true);
			} else
			{
				issue.changeDescription(description);
			}
		}
		return issue;
	}
}
