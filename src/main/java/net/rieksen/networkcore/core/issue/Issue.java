package net.rieksen.networkcore.core.issue;

import java.util.Date;

public interface Issue
{

	public static final int	PRIORITY_LOW	= 0;
	public static final int	PRIORITY_MEDIUM	= 100;
	public static final int	PRIORITY_HIGH	= 200;

	void changeDescription(String description);

	void changeResolved(boolean resolved);

	Date getCreationDate();

	String getDescription();

	int getPluginId();

	int getPriority();

	String getIssueData();

	String getIssueGeneratorName();

	int getIssueId();

	String getIssueName();

	boolean isResolved();
}
