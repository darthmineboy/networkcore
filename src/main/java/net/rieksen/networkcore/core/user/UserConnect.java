package net.rieksen.networkcore.core.user;

import java.util.Date;

import net.rieksen.networkcore.core.server.Server;

public interface UserConnect
{

	void changeQuitDate(Date quitDate);

	int getConnectId();

	String getIp();

	Date getJoinDate();

	String getName();

	Date getQuitDate();

	int getRuntimeId();

	Server getServer();

	User getUser();

	int getUserId();
}