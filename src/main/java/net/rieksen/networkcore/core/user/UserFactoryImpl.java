package net.rieksen.networkcore.core.user;

import java.util.Date;
import java.util.UUID;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.user.note.UserNote;
import net.rieksen.networkcore.core.user.note.UserNoteImpl;
import net.rieksen.networkcore.core.user.note.UserNotePojo;
import net.rieksen.networkcore.core.user.pojo.UserChatPojo;
import net.rieksen.networkcore.core.user.pojo.UserConnectPojo;
import net.rieksen.networkcore.core.user.pojo.UserPojo;

public class UserFactoryImpl implements UserFactory
{

	private NetworkCore provider;

	public UserFactoryImpl(NetworkCore provider)
	{
		this.provider = provider;
	}

	@Override
	public UserChat createChat(long chatId, int connectId, Date date, String message, boolean cancelled)
	{
		return new UserChatImpl(chatId, connectId, date, message, cancelled);
	}

	@Override
	public UserChat createChat(UserChatPojo pojo)
	{
		return new UserChatImpl(this.provider, pojo);
	}

	@Override
	public UserConnect createConnect(int connectId, int userId, int runtimeId, String name, String ip, Date joinDate, Date quitDate)
	{
		return new UserConnectImpl(this.provider, connectId, userId, runtimeId, name, ip, joinDate, quitDate);
	}

	@Override
	public UserConnect createConnect(UserConnectPojo pojo)
	{
		return new UserConnectImpl(this.provider, pojo);
	}

	@Override
	public UserNote createNote(UserNotePojo pojo)
	{
		return new UserNoteImpl(this.provider, pojo);
	}

	@Override
	public User createUser(int userId, UserType type, UUID uuid, String name)
	{
		return new UserImpl(this.provider, userId, type, uuid, name, null, true, true);
	}

	@Override
	public User createUser(UserPojo pojo)
	{
		return new UserImpl(this.provider, pojo);
	}
}
