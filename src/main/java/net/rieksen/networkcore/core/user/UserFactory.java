package net.rieksen.networkcore.core.user;

import java.util.Date;
import java.util.UUID;

import net.rieksen.networkcore.core.user.note.UserNote;
import net.rieksen.networkcore.core.user.note.UserNotePojo;
import net.rieksen.networkcore.core.user.pojo.UserChatPojo;
import net.rieksen.networkcore.core.user.pojo.UserConnectPojo;
import net.rieksen.networkcore.core.user.pojo.UserPojo;

public interface UserFactory
{

	UserChat createChat(long chatId, int connectId, Date date, String message, boolean cancelled);

	UserChat createChat(UserChatPojo pojo);

	UserConnect createConnect(int connectId, int userId, int runtimeId, String name, String ip, Date joinDate, Date quitDate);

	UserConnect createConnect(UserConnectPojo pojo);

	UserNote createNote(UserNotePojo pojo);

	User createUser(int userId, UserType type, UUID uuid, String name);

	User createUser(UserPojo pojo);
}
