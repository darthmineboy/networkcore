package net.rieksen.networkcore.core.user;

import java.util.Date;

/**
 * Represents a chat message from a user. This also includes commands, which are
 * prefixed by a forward slash.
 */
public interface UserChat
{

	long getChatId();

	int getConnectId();

	Date getDate();

	String getMessage();

	/**
	 * Whether the event was cancelled. This does not guarantee the message
	 * wasn't delivered, as some chat format plugins cancel the message and then
	 * manually send the message.
	 * 
	 * @return
	 */
	boolean isCancelled();

	/**
	 * Whether the message starts with a forward slash.
	 * 
	 * @return
	 */
	boolean isCommand();
}