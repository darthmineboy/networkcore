package net.rieksen.networkcore.core.user;

import java.util.List;
import java.util.UUID;

import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import net.rieksen.networkcore.core.event.user.IUserQuitEvent;
import net.rieksen.networkcore.core.event.user.IUserUpdateEvent;
import net.rieksen.networkcore.core.module.Module;
import net.rieksen.networkcore.core.user.note.UserNote;
import net.rieksen.networkcore.core.user.pojo.UserPojo;

public interface UserModule extends Module
{

	<T> void addAttachmentCreate(Class<T> key, UserAttachmentCreate<T> create);

	/**
	 * Creates a new user, and sets the {@link UserId}.
	 * 
	 * @param type
	 * @param uuid
	 * @param name
	 * @return
	 */
	User createUser(UserType type, UUID uuid, String name);

	/**
	 * Deletes all cooldowns that are in the past.
	 */
	void deleteExpiredCooldowns();

	User findCachedUser(int userId);

	User findCachedUser(UserType type, String name);

	User findCachedUser(UUID uuid);

	UserNote findNote(int noteId);

	/**
	 * Find users whose latest Ip equals the provided Ip.
	 * 
	 * @param ip
	 * @return
	 */
	List<User> findUsersWithLatestIp(String ip);

	List<User> fromPojos(List<UserPojo> pojos);

	<T> UserAttachmentCreate<T> getAttachmentCreate(Class<T> key);

	UserChat getChat(long chatId);

	UserFactory getFactory();

	/**
	 * Finds a user associated with the {@link CommandSender}. For a player the
	 * {@link UUID} or name must match. A new {@link UserImpl} is returned if
	 * none yet exists for the {@link CommandSender}.
	 * 
	 * @param sender
	 * @return
	 */
	User getUser(CommandSender sender);

	User getUser(ConsoleCommandSender sender);

	User getUser(int userId);

	User getUser(OfflinePlayer player);

	User getUser(Player player);

	User getUser(UserType type, String name);

	User getUser(UUID uuid);

	int getUserCount();

	List<User> getUsers();

	List<User> getUsers(int startIndex, int count);

	void handleLogin(User user, String name, String host);

	void handleLogout(User user);

	void loadCooldown(CooldownAttachment cooldown);

	void onUserQuit(IUserQuitEvent e);

	void onUserUpdate(IUserUpdateEvent e);

	void refreshUser(int userId);

	<T> void removeAttachmentCreate(Class<T> key);
}