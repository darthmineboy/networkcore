package net.rieksen.networkcore.core.user;



/**
 * Used to create an attachment when {@link UserImpl#getAttachment(Class)} is
 * called.
 * 
 * @author Darthmineboy
 * @param <T>
 */
public abstract class UserAttachmentCreate<T>
{

	/**
	 * Creates the attachment.
	 * 
	 * @param user
	 * @return
	 */
	public abstract T createAttachment(User user);
}
