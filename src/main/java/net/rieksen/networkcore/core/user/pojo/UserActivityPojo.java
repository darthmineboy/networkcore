package net.rieksen.networkcore.core.user.pojo;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import net.rieksen.networkcore.core.user.UserActivityType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class UserActivityPojo
{

	private UserActivityType	type;
	private Integer				chatId;
	private int					connectId;
	private int					runtimeId;
	private int					userId;
	private String				name;
	private String				ip;
	private Date				date;
	private String				message;

	public UserActivityPojo()
	{}

	public UserActivityPojo(UserActivityType type, Integer chatId, int connectId, int runtimeId, int userId, String name, String ip,
		Date date, String message)
	{
		this.type = type;
		this.chatId = chatId;
		this.connectId = connectId;
		this.runtimeId = runtimeId;
		this.userId = userId;
		this.name = name;
		this.ip = ip;
		this.date = date;
		this.message = message;
	}

	public Integer getChatId()
	{
		return this.chatId;
	}

	public int getConnectId()
	{
		return this.connectId;
	}

	public Date getDate()
	{
		return this.date;
	}

	public String getIp()
	{
		return this.ip;
	}

	public String getMessage()
	{
		return this.message;
	}

	public String getName()
	{
		return this.name;
	}

	public int getRuntimeId()
	{
		return this.runtimeId;
	}

	public UserActivityType getType()
	{
		return this.type;
	}

	public int getUserId()
	{
		return this.userId;
	}

	public void setChatId(Integer chatId)
	{
		this.chatId = chatId;
	}

	public void setConnectId(int connectId)
	{
		this.connectId = connectId;
	}

	public void setDate(Date date)
	{
		this.date = date;
	}

	public void setIp(String ip)
	{
		this.ip = ip;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setRuntimeId(int runtimeId)
	{
		this.runtimeId = runtimeId;
	}

	public void setType(UserActivityType type)
	{
		this.type = type;
	}

	public void setUserId(int userId)
	{
		this.userId = userId;
	}
}
