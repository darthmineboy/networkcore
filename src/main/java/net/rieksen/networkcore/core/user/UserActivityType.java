package net.rieksen.networkcore.core.user;

public enum UserActivityType
{
	CHAT,
	CONNECT,
	DISCONNECT;
}
