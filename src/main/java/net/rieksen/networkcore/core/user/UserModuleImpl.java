package net.rieksen.networkcore.core.user;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.metadata.MetadataValueAdapter;
import org.bukkit.plugin.Plugin;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.cache.CacheCleanupThread;
import net.rieksen.networkcore.core.event.user.IUserQuitEvent;
import net.rieksen.networkcore.core.event.user.IUserUpdateEvent;
import net.rieksen.networkcore.core.module.Module;
import net.rieksen.networkcore.core.module.ModuleImpl;
import net.rieksen.networkcore.core.user.note.UserNote;
import net.rieksen.networkcore.core.user.note.UserNotePojo;
import net.rieksen.networkcore.core.user.pojo.UserChatPojo;
import net.rieksen.networkcore.core.user.pojo.UserPojo;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;

public class UserModuleImpl extends ModuleImpl implements UserModule
{

	private CacheCleanupThread<User>				userCacheThread;
	private Collection<User>						users				= Collections.synchronizedCollection(new HashSet<User>());
	private NetworkCore								provider;
	private UserFactory								factory;
	private Map<Class<?>, UserAttachmentCreate<?>>	attachmentCreates	= new ConcurrentHashMap<>();

	public UserModuleImpl(NetworkCore provider)
	{
		this.provider = provider;
		this.factory = new UserFactoryImpl(provider);
	}

	/**
	 * Add an attachment create.
	 * 
	 * @throws IllegalStateException
	 *             when key or create is null
	 * @param key
	 * @param create
	 */
	@Override
	public <T> void addAttachmentCreate(Class<T> key, UserAttachmentCreate<T> create)
	{
		Validate.notNull(key, "Key cannot be null");
		Validate.notNull(create, "Create cannot be null");
		this.attachmentCreates.put(key, create);
	}

	@Override
	public User createUser(UserType type, UUID uuid, String name)
	{
		UserPojo pojo = new UserPojo(0, type, uuid, name, null, true, true);
		pojo.setUserId(this.provider.getDAO().getUserDAO().createUser(pojo));
		User user = this.factory.createUser(pojo);
		this.putInCache(user);
		return user;
	}

	@Override
	public void deleteExpiredCooldowns()
	{
		this.provider.getDAO().getUserDAO().deleteExpiredCooldowns();
	}

	@Override
	public User findCachedUser(int userId)
	{
		return this.users.stream().filter(user -> user.getUserId() == userId).findAny().orElse(null);
	}

	@Override
	public User findCachedUser(UserType type, String name)
	{
		return this.users.stream().filter(user -> user.getType() == type && user.getName().equalsIgnoreCase(name)).findAny().orElse(null);
	}

	@Override
	public User findCachedUser(UUID uuid)
	{
		return this.users.stream().filter(user -> user.getUUID().equals(uuid)).findAny().orElse(null);
	}

	@Override
	public UserNote findNote(int noteId)
	{
		UserNotePojo pojo = this.provider.getDAO().getUserDAO().findNote(noteId);
		return pojo == null ? null : this.factory.createNote(pojo);
	}

	@Override
	public List<User> findUsersWithLatestIp(String ip)
	{
		List<UserPojo> users = this.provider.getDAO().getUserDAO().findUsersWithLatestIp(ip);
		return this.fromPojos(users);
	}

	@Override
	public List<User> fromPojos(List<UserPojo> pojos)
	{
		List<User> users = new ArrayList<>(pojos.size());
		pojos.forEach(pojo ->
		{
			User user = this.findCachedUser(pojo.getUserId());
			if (user == null)
			{
				user = this.factory.createUser(pojo);
				this.putInCache(user);
			}
			users.add(user);
		});
		return users;
	}

	/**
	 * Get the attachment create. Can return null.
	 * 
	 * @throws IllegalStateException
	 *             when key is null
	 * @param key
	 * @return
	 */
	@Override
	@SuppressWarnings("unchecked")
	public <T> UserAttachmentCreate<T> getAttachmentCreate(Class<T> key)
	{
		Validate.notNull(key, "Key cannot be null");
		return (UserAttachmentCreate<T>) this.attachmentCreates.get(key);
	}

	@Override
	public UserChat getChat(long chatId)
	{
		UserChatPojo pojo = this.provider.getDAO().getUserDAO().findChat(chatId);
		return pojo == null ? null : this.factory.createChat(pojo);
	}

	@Override
	public UserFactory getFactory()
	{
		return this.factory;
	}

	@Override
	public User getUser(CommandSender sender)
	{
		User user = null;
		if (sender instanceof Player)
		{
			user = this.getUser((Player) sender);
		} else if (sender instanceof ConsoleCommandSender)
		{
			user = this.getUser((ConsoleCommandSender) sender);
		}
		return user;
	}

	@Override
	public User getUser(ConsoleCommandSender sender)
	{
		User user = this.getUser(UserType.MINECRAFT_CONSOLE, sender.getName());
		if (user == null)
		{
			user = this.createUser(UserType.MINECRAFT_CONSOLE, UUID.randomUUID(), sender.getName());
		}
		return user;
	}

	@Override
	public User getUser(int userId)
	{
		User user = this.findCachedUser(userId);
		if (user != null) { return user; }
		UserPojo pojo = this.provider.getDAO().getUserDAO().findUserById(userId);
		if (pojo == null) { return null; }
		user = this.factory.createUser(pojo);
		if (user != null)
		{
			this.putInCache(user);
		}
		return user;
	}

	@Override
	public User getUser(OfflinePlayer player)
	{
		User user = null;
		user = this.getUser(player.getUniqueId());
		if (user != null) { return user; }
		user = this.getUser(UserType.MINECRAFT_PLAYER, player.getName());
		if (user != null) { return user; }
		user = this.createUser(UserType.MINECRAFT_PLAYER, player.getUniqueId(), player.getName());
		if (user != null) { return user; }
		return user;
	}

	@Override
	public User getUser(Player player)
	{
		User user = null;
		user = this.getUserFromPlayerMeta(player);
		if (user != null) { return user; }
		user = this.getUser((OfflinePlayer) player);
		user.keepCached(true);
		this.setUserInPlayerMeta(player, user);
		return user;
	}

	@Override
	public User getUser(UserType type, String name)
	{
		User user = this.findCachedUser(type, name);
		if (user != null) { return user; }
		UserPojo pojo = this.provider.getDAO().getUserDAO().findUserByTypeAndName(type, name);
		if (pojo == null) { return null; }
		user = this.factory.createUser(pojo);
		if (user != null)
		{
			this.putInCache(user);
		}
		return user;
	}

	@Override
	public User getUser(UUID uuid)
	{
		User user = this.findCachedUser(uuid);
		if (user != null) { return user; }
		UserPojo pojo = this.provider.getDAO().getUserDAO().findUserByUUID(uuid);
		if (pojo == null) { return null; }
		user = this.factory.createUser(pojo);
		if (user != null)
		{
			this.putInCache(user);
		}
		return user;
	}

	@Override
	public int getUserCount()
	{
		return this.provider.getDAO().getUserDAO().findUserCount();
	}

	@Override
	public List<User> getUsers()
	{
		List<UserPojo> pojos = this.provider.getDAO().getUserDAO().findUsers();
		return this.fromPojos(pojos);
	}

	@Override
	public List<User> getUsers(int startIndex, int count)
	{
		List<UserPojo> pojos = this.provider.getDAO().getUserDAO().findUsers(startIndex, count);
		return this.fromPojos(pojos);
	}

	@Override
	public void handleLogin(User user, String name, String host)
	{
		user.keepCached(true);
		if ("demo".equals(this.provider.getNetworkPlugin().getOption("Config", "Environment").getString(null)))
		{
			host = this.createRandomIp();
		}
		user.createLatestConnect(name, host);
	}

	@Override
	public void handleLogout(User user)
	{
		user.keepCached(false);
		this.provider.getTaskChainFactory().newChain().async(() ->
		{
			UserConnect connect = user.getLocalConnect();
			if (connect == null)
			{
				this.provider.getLogger().warning("User #" + user.getUserId() + " has no local connect");
				return;
			}
			connect.changeQuitDate(new Date(System.currentTimeMillis()));
			user.setLocalConnect(null);
		}).execute();
		user.setPlayer(null);
	}

	@Override
	public void loadCooldown(CooldownAttachment cooldown)
	{
		this.provider.getDAO().getUserDAO().loadCooldown(cooldown);
	}

	@Override
	public void onUserQuit(IUserQuitEvent e)
	{
		User user = this.findCachedUser(e.getUserId());
		if (user == null) { return; }
	}

	@Override
	public void onUserUpdate(IUserUpdateEvent e)
	{
		this.refreshUser(e.getUserId());
	}

	@Override
	public void refreshUser(int userId)
	{
		User user = this.findCachedUser(userId);
		if (user == null) { return; }
		user.refresh();
	}

	/**
	 * Remove an attachment create.
	 * 
	 * @throws IllegalStateException
	 *             when key or create is null
	 * @param key
	 */
	@Override
	public <T> void removeAttachmentCreate(Class<T> key)
	{
		Validate.notNull(key, "Key cannot be null");
		this.attachmentCreates.remove(key);
	}

	@Override
	public Module[] requiresModules()
	{
		return null;
	}

	@Override
	protected void destroyModule()
	{
		this.userCacheThread.disable();
	}

	@Override
	protected void initModule()
	{
		this.userCacheThread = new CacheCleanupThread<User>("UserManager", "user", TimeUnit.SECONDS.toMillis(60))
		{

			@Override
			public Collection<User> getCache()
			{
				return UserModuleImpl.this.users;
			}
		};
		this.userCacheThread.start();
		// Test
		if (this.provider instanceof NetworkCoreSpigot)
		{
			((NetworkCoreSpigot) this.provider).getServer().getOnlinePlayers().forEach(player ->
			{
				User userViaPlayer = this.getUser(player);
				User userViaId = this.getUser(userViaPlayer.getUserId());
				if (userViaPlayer != userViaId)
				{
					// TODO remove
					this.provider.getLogger().severe("For player " + player.getName() + " the User object did not match");
				}
			});
		}
	}

	private String createRandomIp()
	{
		Random r = new Random();
		String[] ip = new String[4];
		for (int i = 0; i < 4; i++)
		{
			ip[i] = String.valueOf(r.nextInt(255) + 1);
		}
		return StringUtils.join(ip, ".");
	}

	private User getUserFromPlayerMeta(Player player)
	{
		List<MetadataValue> playerMeta = player.getMetadata("user");
		for (MetadataValue meta : playerMeta)
		{
			NetworkCoreSpigot networkCoreSpigot = NetworkCoreSpigot.getInstance();
			if (meta.getOwningPlugin() != networkCoreSpigot && !(meta instanceof UserPlayerMeta))
			{
				continue;
			}
			UserPlayerMeta userMeta = (UserPlayerMeta) meta;
			User user = userMeta.getUser();
			if (!user.isCached())
			{
				player.removeMetadata("user", networkCoreSpigot);
				return null;
			}
			return user;
		}
		return null;
	}

	private void putInCache(User user)
	{
		Validate.notNull(user);
		user.isCached(true);
		this.users.add(user);
	}

	private void setUserInPlayerMeta(Player player, User user)
	{
		player.setMetadata("user", new UserPlayerMeta(NetworkCoreSpigot.getInstance(), user));
	}

	private static class UserPlayerMeta extends MetadataValueAdapter
	{

		private User user;

		protected UserPlayerMeta(Plugin owningPlugin, User user)
		{
			super(owningPlugin);
			this.user = user;
		}

		public User getUser()
		{
			return this.user;
		}

		@Override
		public void invalidate()
		{}

		@Override
		public Object value()
		{
			return null;
		}
	}
}
