package net.rieksen.networkcore.core.user.pojo;

import java.util.Date;
import java.util.UUID;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import net.rieksen.networkcore.core.user.UserType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class UserIpPojo
{

	private int			userId;
	private UserType	userType;
	private UUID		uuid;
	private String		userName;
	private String		ip;
	private Date		firstSeen;
	private Date		lastSeen;
	private int			timesJoined;

	public UserIpPojo()
	{}

	public UserIpPojo(int userId, UserType userType, UUID uuid, String userName, String ip, Date firstSeen, Date lastSeen, int timesJoined)
	{
		this.userId = userId;
		this.userType = userType;
		this.uuid = uuid;
		this.userName = userName;
		this.ip = ip;
		this.firstSeen = firstSeen;
		this.lastSeen = lastSeen;
		this.timesJoined = timesJoined;
	}

	public Date getFirstSeen()
	{
		return this.firstSeen;
	}

	public String getIp()
	{
		return this.ip;
	}

	public Date getLastSeen()
	{
		return this.lastSeen;
	}

	public int getTimesJoined()
	{
		return this.timesJoined;
	}

	public int getUserId()
	{
		return this.userId;
	}

	public String getUserName()
	{
		return this.userName;
	}

	public UserType getUserType()
	{
		return this.userType;
	}

	public UUID getUuid()
	{
		return this.uuid;
	}

	public void setFirstSeen(Date firstSeen)
	{
		this.firstSeen = firstSeen;
	}

	public void setIp(String ip)
	{
		this.ip = ip;
	}

	public void setLastSeen(Date lastSeen)
	{
		this.lastSeen = lastSeen;
	}

	public void setTimesJoined(int timesJoined)
	{
		this.timesJoined = timesJoined;
	}

	public void setUserId(int userId)
	{
		this.userId = userId;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public void setUserType(UserType userType)
	{
		this.userType = userType;
	}

	public void setUuid(UUID uuid)
	{
		this.uuid = uuid;
	}
}
