package net.rieksen.networkcore.core.user.pojo;

import java.util.Date;

public class IpPojo
{

	private String	ip;
	private Date	firstSeen;
	private Date	lastSeen;
	private int		timesJoined;

	public IpPojo()
	{}

	public IpPojo(String ip, Date firstSeen, Date lastSeen, int timesJoined)
	{
		this.ip = ip;
		this.firstSeen = firstSeen;
		this.lastSeen = lastSeen;
		this.timesJoined = timesJoined;
	}

	public Date getFirstSeen()
	{
		return this.firstSeen;
	}

	public String getIp()
	{
		return this.ip;
	}

	public Date getLastSeen()
	{
		return this.lastSeen;
	}

	public int getTimesJoined()
	{
		return this.timesJoined;
	}

	public void setFirstSeen(Date firstSeen)
	{
		this.firstSeen = firstSeen;
	}

	public void setIp(String ip)
	{
		this.ip = ip;
	}

	public void setLastSeen(Date lastSeen)
	{
		this.lastSeen = lastSeen;
	}

	public void setTimesJoined(int timesJoined)
	{
		this.timesJoined = timesJoined;
	}
}
