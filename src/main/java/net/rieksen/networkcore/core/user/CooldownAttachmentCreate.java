package net.rieksen.networkcore.core.user;

import org.apache.commons.lang.Validate;

import net.rieksen.networkcore.core.NetworkCore;

public class CooldownAttachmentCreate extends UserAttachmentCreate<CooldownAttachment>
{

	private NetworkCore provider;

	public CooldownAttachmentCreate(NetworkCore plugin)
	{
		Validate.notNull(plugin);
		this.provider = plugin;
	}

	@Override
	public CooldownAttachment createAttachment(User user)
	{
		CooldownAttachment att = new CooldownAttachment(this.provider, user.getUserId());
		this.provider.getUserModule().loadCooldown(att);
		return att;
	}
}
