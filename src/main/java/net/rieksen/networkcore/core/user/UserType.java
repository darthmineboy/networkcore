package net.rieksen.networkcore.core.user;

public enum UserType
{
	MINECRAFT_PLAYER(0, "Player"),
	MINECRAFT_CONSOLE(1, "Console"),
	MINECRAFT_COMMAND_BLOCK(2, "Command Block"),
	CUSTOM(3, "Custom");

	/**
	 * @param value
	 * @return NULL when no {@link UserType} with given value exists
	 */
	public static UserType fromValue(int value)
	{
		for (UserType type : UserType.values())
		{
			if (type.i == value) { return type; }
		}
		return null;
	}

	private final int		i;
	private final String	name;

	UserType(int i, String name)
	{
		this.i = i;
		this.name = name;
	}

	public int getValue()
	{
		return this.i;
	}

	@Override
	public String toString()
	{
		return this.name;
	}
}
