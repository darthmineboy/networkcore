package net.rieksen.networkcore.core.user.pojo;

import java.util.UUID;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import net.rieksen.networkcore.core.user.UserType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class UserPojo
{

	private int			userId;
	private UserType	userType;
	private UUID		uuid;
	private String		userName;
	private Integer		connectId;
	private boolean		checkNameHistory;
	private boolean		autoLanguage;

	public UserPojo()
	{}

	@Deprecated
	public UserPojo(int userId, UserType userType, UUID uuid, String userName)
	{
		this.userId = userId;
		this.userType = userType;
		this.uuid = uuid;
		this.userName = userName;
	}

	@Deprecated
	public UserPojo(int userId, UserType userType, UUID uuid, String userName, Integer connectId)
	{
		this.userId = userId;
		this.userType = userType;
		this.uuid = uuid;
		this.userName = userName;
		this.connectId = connectId;
	}

	@Deprecated
	public UserPojo(int userId, UserType userType, UUID uuid, String userName, Integer connectId, boolean checkNameHistory)
	{
		this.userId = userId;
		this.userType = userType;
		this.uuid = uuid;
		this.userName = userName;
		this.connectId = connectId;
		this.checkNameHistory = checkNameHistory;
	}

	public UserPojo(int userId, UserType userType, UUID uuid, String userName, Integer connectId, boolean checkNameHistory,
		boolean autoLanguage)
	{
		this.userId = userId;
		this.userType = userType;
		this.uuid = uuid;
		this.userName = userName;
		this.connectId = connectId;
		this.checkNameHistory = checkNameHistory;
		this.autoLanguage = autoLanguage;
	}

	public boolean checkNameHistory()
	{
		return this.checkNameHistory;
	}

	public Integer getConnectId()
	{
		return this.connectId;
	}

	public int getUserId()
	{
		return this.userId;
	}

	public String getUserName()
	{
		return this.userName;
	}

	public UserType getUserType()
	{
		return this.userType;
	}

	public UUID getUUID()
	{
		return this.uuid;
	}

	public boolean isAutoLanguage()
	{
		return this.autoLanguage;
	}

	public void setAutoLanguage(boolean autoLanguage)
	{
		this.autoLanguage = autoLanguage;
	}

	public void setCheckNameHistory(boolean checkNameHistory)
	{
		this.checkNameHistory = checkNameHistory;
	}

	public void setConnectId(Integer connectId)
	{
		this.connectId = connectId;
	}

	public void setUserId(int userId)
	{
		this.userId = userId;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public void setUserType(UserType type)
	{
		this.userType = type;
	}

	public void setUUID(UUID uuid)
	{
		this.uuid = uuid;
	}
}
