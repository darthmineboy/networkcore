package net.rieksen.networkcore.core.user.note;

import java.util.Date;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.user.User;

public class UserNoteImpl implements UserNote
{

	private final NetworkCore	provider;
	private int					id;
	private int					userId;
	private int					createdById;
	private Date				creationDate;
	private String				message;

	public UserNoteImpl(NetworkCore provider, UserNotePojo pojo)
	{
		this.provider = provider;
		this.setPojo(pojo);
	}

	@Override
	public void delete()
	{
		this.provider.getDAO().getUserDAO().deleteNote(this.id);
	}

	@Override
	public User getCreatedBy()
	{
		return this.provider.getUserModule().getUser(this.createdById);
	}

	@Override
	public int getCreatedById()
	{
		return this.createdById;
	}

	@Override
	public Date getCreationDate()
	{
		return this.creationDate;
	}

	@Override
	public int getId()
	{
		return this.id;
	}

	@Override
	public String getMessage()
	{
		return this.message;
	}

	@Override
	public User getUser()
	{
		return this.provider.getUserModule().getUser(this.userId);
	}

	@Override
	public int getUserId()
	{
		return this.userId;
	}

	private void setPojo(UserNotePojo pojo)
	{
		this.id = pojo.getId();
		this.userId = pojo.getUserId();
		this.createdById = pojo.getCreatedById();
		this.creationDate = pojo.getCreationDate();
		this.message = pojo.getMessage();
	}
}
