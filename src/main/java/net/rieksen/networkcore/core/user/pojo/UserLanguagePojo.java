package net.rieksen.networkcore.core.user.pojo;

public class UserLanguagePojo
{

	private int	userId;
	private int	languageId;
	private int	priority;

	public UserLanguagePojo()
	{}

	public UserLanguagePojo(int userId, int languageId, int priority)
	{
		this.userId = userId;
		this.languageId = languageId;
		this.priority = priority;
	}

	public int getLanguageId()
	{
		return this.languageId;
	}

	public int getPriority()
	{
		return this.priority;
	}

	public int getUserId()
	{
		return this.userId;
	}

	public void setLanguageId(int languageId)
	{
		this.languageId = languageId;
	}

	public void setPriority(int priority)
	{
		this.priority = priority;
	}

	public void setUserId(int userId)
	{
		this.userId = userId;
	}
}
