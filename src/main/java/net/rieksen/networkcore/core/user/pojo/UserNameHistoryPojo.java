package net.rieksen.networkcore.core.user.pojo;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class UserNameHistoryPojo
{

	private int		userId;
	private Date	changedAt;
	private String	name;

	public UserNameHistoryPojo()
	{}

	public UserNameHistoryPojo(int userId, Date changedAt, String name)
	{
		this.userId = userId;
		this.changedAt = changedAt;
		this.name = name;
	}

	public Date getChangedAt()
	{
		return this.changedAt;
	}

	public String getName()
	{
		return this.name;
	}

	public int getUserId()
	{
		return this.userId;
	}

	public void setChangedAt(Date changedAt)
	{
		this.changedAt = changedAt;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setUserId(int userId)
	{
		this.userId = userId;
	}
}
