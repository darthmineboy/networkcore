package net.rieksen.networkcore.core.user.pojo;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class UserConnectPojo
{

	private int		connectId;
	private int		userId;
	private int		runtimeId;
	private String	userName;
	private String	ip;
	private Date	joinDate;
	private Date	quitDate;

	public UserConnectPojo()
	{}

	public UserConnectPojo(int connectId, int userId, int runtimeId, String name, String ip, Date joinDate, Date quitDate)
	{
		this.connectId = connectId;
		this.userId = userId;
		this.runtimeId = runtimeId;
		this.userName = name;
		this.ip = ip;
		this.joinDate = joinDate;
		this.quitDate = quitDate;
	}

	public int getConnectId()
	{
		return this.connectId;
	}

	public String getIp()
	{
		return this.ip;
	}

	public Date getJoinDate()
	{
		return this.joinDate;
	}

	public String getName()
	{
		return this.userName;
	}

	public Date getQuitDate()
	{
		return this.quitDate;
	}

	public int getRuntimeId()
	{
		return this.runtimeId;
	}

	public int getUserId()
	{
		return this.userId;
	}

	public void setConnectId(int connectId)
	{
		this.connectId = connectId;
	}

	public void setIp(String ip)
	{
		this.ip = ip;
	}

	public void setJoinDate(Date joinDate)
	{
		this.joinDate = joinDate;
	}

	public void setName(String name)
	{
		this.userName = name;
	}

	public void setQuitDate(Date quitDate)
	{
		this.quitDate = quitDate;
	}

	public void setRuntimeId(int runtimeId)
	{
		this.runtimeId = runtimeId;
	}

	public void setUserId(int userId)
	{
		this.userId = userId;
	}
}
