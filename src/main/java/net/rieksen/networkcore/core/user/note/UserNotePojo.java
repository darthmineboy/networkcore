package net.rieksen.networkcore.core.user.note;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserNotePojo
{

	private int		id;
	private int		userId;
	private int		createdById;
	private Date	creationDate;
	private String	message;

	public UserNotePojo()
	{}

	public UserNotePojo(int id, int userId, int createdById, Date date, String message)
	{
		this.id = id;
		this.userId = userId;
		this.createdById = createdById;
		this.creationDate = date;
		this.message = message;
	}

	public int getCreatedById()
	{
		return this.createdById;
	}

	public Date getCreationDate()
	{
		return this.creationDate;
	}

	public int getId()
	{
		return this.id;
	}

	public String getMessage()
	{
		return this.message;
	}

	public int getUserId()
	{
		return this.userId;
	}

	public void setCreatedById(int createdById)
	{
		this.createdById = createdById;
	}

	public void setCreationDate(Date date)
	{
		this.creationDate = date;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public void setUserId(int userId)
	{
		this.userId = userId;
	}
}
