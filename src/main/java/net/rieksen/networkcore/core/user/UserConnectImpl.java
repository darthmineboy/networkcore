package net.rieksen.networkcore.core.user;

import java.util.Date;

import org.apache.commons.lang.Validate;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.server.ServerRuntime;
import net.rieksen.networkcore.core.server.Server;
import net.rieksen.networkcore.core.user.pojo.UserConnectPojo;

public class UserConnectImpl implements UserConnect
{

	private final NetworkCore	provider;
	private final int			connectId;
	private final int			userId;
	private final int			runtimeId;
	private final String		name;
	private final String		ip;
	private final Date			joinDate;
	private Date				quitDate;

	public UserConnectImpl(NetworkCore provider, int connectId, int userId, int runtimeId, String name, String ip, Date joinDate,
		Date quitDate)
	{
		Validate.notNull(provider);
		Validate.notNull(userId, "UserId cannot be null");
		Validate.notNull(runtimeId, "RuntimeId cannot be null");
		Validate.notNull(name, "Name cannot be null");
		Validate.notNull(ip, "IP cannot be null");
		Validate.notNull(joinDate, "JoinDate cannot be null");
		this.provider = provider;
		this.connectId = connectId;
		this.userId = userId;
		this.runtimeId = runtimeId;
		this.name = name;
		this.ip = ip;
		this.joinDate = joinDate;
		this.quitDate = quitDate;
	}

	public UserConnectImpl(NetworkCore provider, UserConnectPojo pojo)
	{
		Validate.notNull(provider);
		Validate.notNull(pojo);
		this.provider = provider;
		this.connectId = pojo.getConnectId();
		this.userId = pojo.getUserId();
		this.runtimeId = pojo.getRuntimeId();
		this.name = pojo.getName();
		this.ip = pojo.getIp();
		this.joinDate = pojo.getJoinDate();
		this.quitDate = pojo.getQuitDate();
	}

	@Override
	public void changeQuitDate(Date quitDate)
	{
		UserConnectPojo pojo = this.toPojo();
		pojo.setQuitDate(quitDate);
		this.provider.getDAO().getUserDAO().updateConnect(pojo);
		this.quitDate = quitDate;
	}

	@Override
	public int getConnectId()
	{
		return this.connectId;
	}

	@Override
	public String getIp()
	{
		return this.ip;
	}

	@Override
	public Date getJoinDate()
	{
		return this.joinDate;
	}

	@Override
	public String getName()
	{
		return this.name;
	}

	@Override
	public Date getQuitDate()
	{
		return this.quitDate;
	}

	@Override
	public int getRuntimeId()
	{
		return this.runtimeId;
	}

	@Override
	public Server getServer()
	{
		return this.getRuntime().getServer();
	}

	@Override
	public User getUser()
	{
		return this.provider.getUserModule().getUser(this.userId);
	}

	@Override
	public int getUserId()
	{
		return this.userId;
	}

	private ServerRuntime getRuntime()
	{
		return this.provider.getServerModule().getRuntime(this.runtimeId);
	}

	private UserConnectPojo toPojo()
	{
		return new UserConnectPojo(this.connectId, this.userId, this.runtimeId, this.name, this.ip, this.joinDate, this.quitDate);
	}
}
