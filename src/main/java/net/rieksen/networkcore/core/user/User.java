package net.rieksen.networkcore.core.user;

import java.util.UUID;

import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.chat.BaseComponent;
import net.rieksen.networkcore.core.NetworkCoreAPI;
import net.rieksen.networkcore.core.cache.ICacheable;
import net.rieksen.networkcore.core.message.UserLanguagePreferences;
import net.rieksen.networkcore.core.user.note.UserNote;
import net.rieksen.networkcore.core.user.note.UserNotePojo;
import net.rieksen.networkcore.core.user.pojo.UserChatPojo;
import net.rieksen.networkcore.core.user.pojo.UserConnectPojo;
import net.rieksen.networkcore.core.util.SimplePage;

public interface User extends ICacheable
{

	/**
	 * This currently supports {@link Player} and {@link ConsoleCommandSender}.
	 * Can return null.
	 * 
	 * @param sender
	 * @return
	 */
	public static User getUser(CommandSender sender)
	{
		return NetworkCoreAPI.getUserModule().getUser(sender);
	}

	/**
	 * Get a user by his userId.
	 * 
	 * @param userId
	 * @return
	 */
	public static User getUser(int userId)
	{
		return NetworkCoreAPI.getUserModule().getUser(userId);
	}

	public static User getUser(OfflinePlayer player)
	{
		return NetworkCoreAPI.getUserModule().getUser(player);
	}

	public static User getUser(Player player)
	{
		return NetworkCoreAPI.getUserModule().getUser(player);
	}

	public static User getUser(UserType type, String name)
	{
		return NetworkCoreAPI.getUserModule().getUser(type, name);
	}

	public static User getUser(UUID uuid)
	{
		return NetworkCoreAPI.getUserModule().getUser(uuid);
	}

	/**
	 * Add an attachment. Attachments can be used to store additional user data,
	 * for as long as this user object is cached.
	 * 
	 * @throws IllegalStateException
	 *             when key or value is null
	 */
	<T> void addAttachment(Class<T> key, T value);

	/**
	 * Changes whether the user language preferences are automatically
	 * configured.
	 * 
	 * @param autoLanguage
	 */
	void changeAutoLanguage(boolean autoLanguage);

	/**
	 * Whether to check the name history of this user.
	 * 
	 * @param checkNameHistory
	 */
	void changeCheckNameHistory(boolean checkNameHistory);

	/**
	 * Set the name.
	 * 
	 * @throws IllegalStateException
	 *             when name is null
	 * @param name
	 */
	void changeName(String name);

	void changeUUID(UUID uuid);

	/**
	 * Whether the name history of this user should be checked.
	 * 
	 * @return
	 */
	boolean checkNameHistory();

	/**
	 * Checks the name history of the user.
	 */
	void checkNameHistoryNow();

	UserChat createChat(UserChatPojo chat);

	UserConnect createConnect(UserConnectPojo userConnectPojo);

	/**
	 * Creates a new {@link UserConnect} and sets the connect as this users
	 * latest connect.
	 * 
	 * @param name
	 * @param host
	 * @see #createConnect(UserConnectPojo)
	 */
	void createLatestConnect(String name, String host);

	/**
	 * Creates an {@link UserNote}.
	 * 
	 * @param note
	 * @return
	 */
	UserNote createNote(UserNotePojo note);

	/**
	 * Finds {@link UserNote}.
	 * 
	 * @param startIndex
	 * @param maxResults
	 * @return
	 */
	SimplePage<UserNote> findNotes(int startIndex, int maxResults);

	/**
	 * Get the attachment. Can return null.
	 * <p>
	 * This will attempt to create the attachment when a
	 * {@link UserAttachmentCreate} is found for the key. If the created
	 * attachment != null, it is also added as attachment to the user.
	 *
	 * @param key
	 * @return
	 * @throws IllegalStateException
	 *             when key is null
	 */
	<T> T getAttachment(Class<T> key);

	/**
	 * Get the latest connect.
	 * 
	 * @return
	 */
	UserConnect getConnect();

	/**
	 * Get the latest known IP.
	 * 
	 * @return null when unknown
	 */
	String getIp();

	/**
	 * Get the user language.
	 * 
	 * @return
	 */
	UserLanguagePreferences getLanguagePreferences();

	/**
	 * The {@link UserConnect} to this server. This method should return null if
	 * the player is not online on the this server.
	 * 
	 * @return
	 */
	UserConnect getLocalConnect();

	/**
	 * Get the name.
	 * 
	 * @return
	 */
	String getName();

	Player getPlayer();

	/**
	 * Get the type.
	 * 
	 * @return
	 */
	UserType getType();

	/**
	 * Get the userId. Can return null.
	 * 
	 * @return
	 */
	int getUserId();

	/**
	 * Get the UUID.
	 * 
	 * @return
	 */
	UUID getUUID();

	/**
	 * Whether the user has an attachment.
	 * 
	 * @param key
	 * @throws IllegalStateException
	 *             when key is null
	 * @return
	 */
	boolean hasAttachment(Class<?> key);

	/**
	 * Whether this user has a connect.
	 * 
	 * @return
	 */
	boolean hasConnect();

	/**
	 * Check if the player has permission. This method works for both online as
	 * offline users.
	 * 
	 * @param permission
	 * @return
	 */
	boolean hasPermission(String permission);

	/**
	 * Whether the user has user language.
	 * 
	 * @return
	 */
	boolean hasUserLanguage();

	/**
	 * Whether the language preferences of this user should be automatically
	 * managed.
	 * 
	 * @return
	 */
	boolean isAutoLanguage();

	void refresh();

	/**
	 * Remove an attachment.
	 * 
	 * @param key
	 * @throws IllegalStateException
	 *             when key is null
	 */
	void removeAttachment(Class<?> key);

	/**
	 * Sends the message to this user only if he is online on this server.
	 * 
	 * @param components
	 */
	void sendLocalMessage(BaseComponent... components);

	/**
	 * Sends the message to this user only if he is online on this server.
	 * 
	 * @param message
	 * @see #sendMessage(String)
	 */
	void sendLocalMessage(String message);

	/**
	 * Sends the message to this user only if he is online on this server. The
	 * user only receives the message if he has the specified permission.
	 * 
	 * @param permission
	 * @param components
	 */
	void sendLocalMessageWithPermission(String permission, BaseComponent... components);

	/**
	 * Sends the message to this user only if he is online on this server. The
	 * user only receives the message if he has the specified permission.
	 * 
	 * @param message
	 * @param permission
	 * @see #sendMessageWithPermission(String, String)
	 */
	void sendLocalMessageWithPermission(String message, String permission);

	/**
	 * Sends the message to this user, even if he is on another server.
	 * 
	 * @param components
	 */
	void sendMessage(BaseComponent... components);

	/**
	 * Sends the message to this user, even if he is on another server.
	 * 
	 * @param message
	 * @see #sendLocalMessage(String)
	 */
	void sendMessage(String message);

	/**
	 * Sends the message to this user, even if he is on another server. The user
	 * only receives the message if he has the specified permission.
	 * 
	 * @param permission
	 * @param components
	 */
	void sendMessageWithPermission(String permission, BaseComponent... components);

	/**
	 * Sends the message to this user, even if he is on another server. The user
	 * only receives the message if he has the specified permission.
	 * 
	 * @param message
	 * @param permission
	 * @see #sendLocalMessageWithPermission(String, String)
	 */
	void sendMessageWithPermission(String message, String permission);

	/**
	 * Attemps to send this user to given server.
	 * 
	 * @param serverId
	 */
	void sendToServer(int serverId);

	/**
	 * Set the current connect.
	 * 
	 * @param connect
	 *            can be null
	 */
	void setConnect(UserConnect connect);

	/**
	 * Set the user language.
	 * 
	 * @param userLanguagePreferences
	 *            can be null
	 */
	void setLanguagePreferences(UserLanguagePreferences userLanguagePreferences);

	/**
	 * Set the {@link UserConnect} to this server.
	 * 
	 * @param localConnect
	 */
	void setLocalConnect(UserConnect localConnect);

	void setPlayer(Player player);
}