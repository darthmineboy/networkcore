package net.rieksen.networkcore.core.user.note;

import java.util.Date;

import net.rieksen.networkcore.core.user.User;

/**
 * Note on a {@link User}.
 */
public interface UserNote
{

	void delete();

	/**
	 * User who created note.
	 * 
	 * @return
	 */
	User getCreatedBy();

	/**
	 * User id who created note.
	 * 
	 * @return
	 */
	int getCreatedById();

	/**
	 * Creation date of note.
	 * 
	 * @return
	 */
	Date getCreationDate();

	/**
	 * Unique id of note.
	 * 
	 * @return
	 */
	int getId();

	/**
	 * Message of note.
	 * 
	 * @return
	 */
	String getMessage();

	/**
	 * User who note is about.
	 * 
	 * @return
	 */
	User getUser();

	/**
	 * User id who note is about.
	 * 
	 * @return
	 */
	int getUserId();
}
