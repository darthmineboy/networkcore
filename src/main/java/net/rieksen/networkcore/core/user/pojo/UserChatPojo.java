package net.rieksen.networkcore.core.user.pojo;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserChatPojo
{

	private long	chatId;
	private int		connectId;
	private Date	date;
	private String	message;
	private boolean	cancelled;

	public UserChatPojo()
	{}

	public UserChatPojo(long chatId, int connectId, Date date, String message, boolean cancelled)
	{
		this.chatId = chatId;
		this.connectId = connectId;
		this.date = date;
		this.message = message;
		this.cancelled = cancelled;
	}

	public long getChatId()
	{
		return this.chatId;
	}

	public int getConnectId()
	{
		return this.connectId;
	}

	public Date getDate()
	{
		return this.date;
	}

	public String getMessage()
	{
		return this.message;
	}

	public boolean isCancelled()
	{
		return this.cancelled;
	}

	public void setCancelled(boolean cancelled)
	{
		this.cancelled = cancelled;
	}

	public void setChatId(long chatId)
	{
		this.chatId = chatId;
	}

	public void setConnectId(int connectId)
	{
		this.connectId = connectId;
	}

	public void setDate(Date date)
	{
		this.date = date;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}
}
