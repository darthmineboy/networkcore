package net.rieksen.networkcore.core.user;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang.Validate;

import net.rieksen.networkcore.core.NetworkCore;

/**
 * User attachment for basic cooldowns. Cooldowns are stored per plugin by key.
 * 
 * @author Darthmineboy
 */
public class CooldownAttachment
{

	private final NetworkCore					provider;
	private final int							userId;
	private final Map<Integer, PluginCooldown>	pluginCooldowns	= new ConcurrentHashMap<>();

	public CooldownAttachment(NetworkCore provider, int userId)
	{
		Validate.notNull(userId, "UserId cannot be null");
		this.provider = provider;
		this.userId = userId;
	}

	/**
	 * Get the plugin cooldown. A new plugin cooldown is created when none
	 * exists yet for this plugin. Thus this will never return null.
	 * 
	 * @param pluginId
	 * @return
	 * @throws IllegalStateException
	 *             when pluginId is null
	 */
	public PluginCooldown getPluginCooldown(int pluginId)
	{
		Validate.notNull(pluginId);
		PluginCooldown pluginCooldown = this.pluginCooldowns.get(pluginId);
		if (pluginCooldown == null)
		{
			pluginCooldown = new PluginCooldown(pluginId);
			this.pluginCooldowns.put(pluginId, pluginCooldown);
		}
		return pluginCooldown;
	}

	/**
	 * Get the time left in millis of a cooldown. If the cooldown has expired, 0
	 * is returned.
	 * 
	 * @param pluginId
	 * @param key
	 * @return
	 */
	public long getRemaining(int pluginId, String key)
	{
		Validate.notNull(pluginId, "pluginId cannot be null");
		Validate.notNull(key, "Key cannot be null");
		PluginCooldown pluginCooldown = this.getPluginCooldown(pluginId);
		if (!pluginCooldown.onCooldown(key)) { return 0; }
		return pluginCooldown.getExpiration(key) - System.currentTimeMillis();
	}

	/**
	 * Get the userId.
	 * 
	 * @return
	 */
	public int getUserId()
	{
		return this.userId;
	}

	/**
	 * Get whether a pluginId and key are on cooldown.
	 * 
	 * @throws IllegalStateException
	 *             when pluginId or key is null
	 * @param pluginId
	 * @param key
	 * @return
	 */
	public boolean onCooldown(int pluginId, String key)
	{
		Validate.notNull(pluginId, "pluginId cannot be null");
		Validate.notNull(key, "Key cannot be null");
		PluginCooldown pluginCooldown = this.getPluginCooldown(pluginId);
		return pluginCooldown.onCooldown(key);
	}

	/**
	 * Removes a cooldown.
	 * 
	 * @throws IllegalStateException
	 *             when pluginId or key is null
	 * @param pluginId
	 * @param key
	 */
	public void removeCooldown(int pluginId, String key)
	{
		Validate.notNull(pluginId, "pluginId cannot be null");
		Validate.notNull(key, "Key cannot be null");
		PluginCooldown pluginCooldown = this.getPluginCooldown(pluginId);
		pluginCooldown.removeCooldown(key);
	}

	/**
	 * Set a cooldown.
	 * 
	 * @throws IllegalStateException
	 *             when pluginId or key is null
	 * @param pluginId
	 * @param key
	 * @param time
	 *            the length of the cooldown in milliseconds
	 * @param persistent
	 *            whether this cooldown is stored in the datasource
	 * @return
	 */
	public void setCooldown(int pluginId, String key, long time, boolean persistent)
	{
		Validate.notNull(pluginId, "pluginId cannot be null");
		Validate.notNull(key, "Key cannot be null");
		PluginCooldown pluginCooldown = this.getPluginCooldown(pluginId);
		pluginCooldown.setCooldown(key, System.currentTimeMillis() + time, persistent);
	}

	private class PluginCooldown
	{

		private final int				pluginId;
		private final Map<String, Long>	cooldowns	= new ConcurrentHashMap<>();

		public PluginCooldown(int pluginId)
		{
			Validate.notNull(pluginId, "pluginId cannot be null");
			this.pluginId = pluginId;
		}

		/**
		 * Get the system time after which the cooldown has expired. Can return
		 * null.
		 * 
		 * @param key
		 * @return
		 * @throws IllegalStateException
		 *             when key is null
		 */
		public Long getExpiration(String key)
		{
			Validate.notNull(key, "Key cannot be null");
			return this.cooldowns.get(key);
		}

		/**
		 * Whether the given key is on cooldown.
		 * 
		 * @param key
		 * @return
		 * @throws IllegalStateException
		 *             when key is null
		 */
		public boolean onCooldown(String key)
		{
			Validate.notNull(key, "Key cannot be null");
			Long expiration = this.getExpiration(key);
			if (expiration == null) { return false; }
			if (System.currentTimeMillis() > expiration)
			{
				this.removeCooldown(key);
				return false;
			}
			return true;
		}

		/**
		 * Removes a cooldown.
		 * 
		 * @throws IllegalStateException
		 *             when key is null
		 * @param key
		 */
		public void removeCooldown(String key)
		{
			Validate.notNull(key, "Key cannot be null");
			this.cooldowns.remove(key);
		}

		/**
		 * Set the cooldown.
		 * 
		 * @param key
		 * @param value
		 *            system time after which the cooldown has expired
		 * @throws IllegalStateException
		 *             when key is null
		 */
		public void setCooldown(String key, long value, boolean persistent)
		{
			Validate.notNull(key, "Key cannot be null");
			this.cooldowns.put(key, value);
			if (persistent)
			{
				CooldownAttachment.this.provider.getDAO().getUserDAO().setCooldown(CooldownAttachment.this.userId, this.pluginId, key,
					value);
			}
		}
	}
}
