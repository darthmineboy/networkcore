package net.rieksen.networkcore.core.user;

import java.util.Date;

import org.apache.commons.lang.Validate;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.NetworkCoreAPI;
import net.rieksen.networkcore.core.user.pojo.UserChatPojo;

/**
 * Represents a chat message.
 * 
 * @author Darthmineboy
 */
public class UserChatImpl implements UserChat
{

	public final static int MAX_MESSAGE_LENGTH = 4096;

	/**
	 * Get the chat. Can return null.
	 * 
	 * @param chatId
	 * @return
	 */
	public static UserChat getChat(long chatId)
	{
		return NetworkCoreAPI.getUserModule().getChat(chatId);
	}

	private final long		chatId;
	private final int		connectId;
	private final Date		date;
	private final String	message;
	private final boolean	cancelled;

	public UserChatImpl(NetworkCore provider, UserChatPojo pojo)
	{
		Validate.notNull(provider);
		Validate.notNull(pojo);
		this.chatId = pojo.getChatId();
		this.connectId = pojo.getConnectId();
		this.date = pojo.getDate();
		this.message = pojo.getMessage();
		this.cancelled = pojo.isCancelled();
	}

	/**
	 * The message is truncated should it exceed {@link #MAX_MESSAGE_LENGTH} in
	 * length.
	 * 
	 * @throws IllegalStateException
	 *             when chatId, connectId or message is null.
	 */
	public UserChatImpl(long chatId, int connectId, Date date, String message, boolean cancelled)
	{
		if (date == null) { throw new IllegalStateException("date cannot be null"); }
		if (message == null) { throw new IllegalStateException("message cannot be null"); }
		if (message.length() > UserChatImpl.MAX_MESSAGE_LENGTH)
		{
			message = message.substring(0, UserChatImpl.MAX_MESSAGE_LENGTH);
		}
		this.chatId = chatId;
		this.connectId = connectId;
		this.date = date;
		this.message = message;
		this.cancelled = cancelled;
	}

	/**
	 * Get the chat id. Can return null.
	 * 
	 * @return
	 */
	@Override
	public long getChatId()
	{
		return this.chatId;
	}

	/**
	 * Get the connect id.
	 * 
	 * @return
	 */
	@Override
	public int getConnectId()
	{
		return this.connectId;
	}

	/**
	 * Get the date.
	 * 
	 * @return
	 */
	@Override
	public Date getDate()
	{
		return this.date;
	}

	/**
	 * Get the message.
	 * 
	 * @return
	 */
	@Override
	public String getMessage()
	{
		return this.message;
	}

	@Override
	public boolean isCancelled()
	{
		return this.cancelled;
	}

	/**
	 * Whether the message starts with a /.
	 * 
	 * @return
	 */
	@Override
	public boolean isCommand()
	{
		return this.message.startsWith("/");
	}
}
