package net.rieksen.networkcore.core.user;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.lang.Validate;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import net.md_5.bungee.api.chat.BaseComponent;
import net.milkbowl.vault.permission.Permission;
import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.cache.SimpleCache;
import net.rieksen.networkcore.core.message.UserLanguagePreferences;
import net.rieksen.networkcore.core.server.Server;
import net.rieksen.networkcore.core.user.note.UserNote;
import net.rieksen.networkcore.core.user.note.UserNotePojo;
import net.rieksen.networkcore.core.user.pojo.UserChatPojo;
import net.rieksen.networkcore.core.user.pojo.UserConnectPojo;
import net.rieksen.networkcore.core.user.pojo.UserLanguagePojo;
import net.rieksen.networkcore.core.user.pojo.UserNameHistoryPojo;
import net.rieksen.networkcore.core.user.pojo.UserPojo;
import net.rieksen.networkcore.core.util.DateUtil;
import net.rieksen.networkcore.core.util.MojangAPI.NameHistory;
import net.rieksen.networkcore.core.util.SimplePage;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;

public class UserImpl implements User
{

	private final NetworkCore		provider;
	private final int				userId;
	private final UserType			type;
	private UUID					uuid;
	private String					name;
	private Integer					connectId;
	private boolean					checkNameHistory;
	private boolean					autoLanguage;
	private UserLanguagePreferences	userLanguagePreferences;
	private UserConnect				connect;
	private Map<Class<?>, Object>	attachments;
	private Player					player;
	private SimpleCache				cache	= new SimpleCache(TimeUnit.SECONDS.toMillis(60));
	private UserConnect				localConnect;

	@Deprecated
	public UserImpl(NetworkCore provider, int userId, UserType type, UUID uuid, String name)
	{
		Validate.notNull(provider);
		Validate.notNull(userId);
		Validate.notNull(type);
		Validate.notNull(uuid);
		Validate.notNull(name);
		this.provider = provider;
		this.userId = userId;
		this.type = type;
		this.uuid = uuid;
		this.name = name;
	}

	@Deprecated
	public UserImpl(NetworkCore provider, int userId, UserType type, UUID uuid, String name, Integer connectId, boolean checkNameHistory)
	{
		Validate.notNull(provider);
		Validate.notNull(userId);
		Validate.notNull(type);
		Validate.notNull(uuid);
		Validate.notNull(name);
		this.provider = provider;
		this.userId = userId;
		this.type = type;
		this.uuid = uuid;
		this.name = name;
		this.connectId = connectId;
		this.checkNameHistory = checkNameHistory;
	}

	public UserImpl(NetworkCore provider, int userId, UserType type, UUID uuid, String name, Integer connectId, boolean checkNameHistory,
		boolean autoLanguage)
	{
		Validate.notNull(provider);
		Validate.notNull(userId);
		Validate.notNull(type);
		Validate.notNull(uuid);
		Validate.notNull(name);
		this.provider = provider;
		this.userId = userId;
		this.type = type;
		this.uuid = uuid;
		this.name = name;
		this.connectId = connectId;
		this.checkNameHistory = checkNameHistory;
		this.autoLanguage = autoLanguage;
	}

	public UserImpl(NetworkCore provider, UserPojo pojo)
	{
		Validate.notNull(provider);
		Validate.notNull(pojo);
		Validate.notNull(pojo.getUserType());
		Validate.notNull(pojo.getUUID());
		Validate.notNull(pojo.getUserName());
		this.provider = provider;
		this.userId = pojo.getUserId();
		this.type = pojo.getUserType();
		this.uuid = pojo.getUUID();
		this.name = pojo.getUserName();
		this.connectId = pojo.getConnectId();
		this.checkNameHistory = pojo.checkNameHistory();
		this.autoLanguage = pojo.isAutoLanguage();
	}

	@Override
	public <T> void addAttachment(Class<T> key, T value)
	{
		Validate.notNull(key, "Key cannot be null");
		Validate.notNull(value, "Value cannot be null");
		this.initializeAttachments();
		this.attachments.put(key, value);
	}

	@Override
	public void changeAutoLanguage(boolean autoLanguage)
	{
		UserPojo pojo = this.toPojo();
		pojo.setAutoLanguage(autoLanguage);
		this.provider.getDAO().getUserDAO().updateUser(pojo);
		this.setPojo(pojo);
	}

	@Override
	public void changeCheckNameHistory(boolean checkNameHistory)
	{
		if (this.checkNameHistory == checkNameHistory) { return; }
		UserPojo pojo = this.toPojo();
		pojo.setCheckNameHistory(checkNameHistory);
		this.provider.getDAO().getUserDAO().updateUser(pojo);
		this.checkNameHistory = checkNameHistory;
	}

	@Override
	public void changeName(String name)
	{
		Validate.notNull(name);
		if (this.name.equals(name)) { return; }
		User previousUserOfName = this.provider.getUserModule().getUser(UserType.MINECRAFT_PLAYER, name);
		if (previousUserOfName != null && previousUserOfName.getUserId() != this.userId)
		{
			// Temporarily change the name in case of circular renames where two
			// or more users have shared the same name.
			this.changeName(name + "-changeName-" + System.currentTimeMillis());
			previousUserOfName.checkNameHistoryNow();
		}
		UserPojo pojo = this.toPojo();
		pojo.setUserName(name);
		this.provider.getDAO().getUserDAO().updateUser(pojo);
		this.name = name;
	}

	@Override
	public void changeUUID(UUID uuid)
	{
		Validate.notNull(uuid);
		if (this.uuid.equals(uuid)) { return; }
		UserPojo pojo = this.toPojo();
		pojo.setUUID(uuid);
		this.provider.getDAO().getUserDAO().updateUser(pojo);
		this.uuid = uuid;
	}

	@Override
	public boolean checkNameHistory()
	{
		return this.checkNameHistory;
	}

	@Override
	public void checkNameHistoryNow()
	{
		try
		{
			List<NameHistory> list = this.provider.getMojangAPI().getNameHistory(this.uuid);
			if (list != null)
			{
				for (NameHistory history : list)
				{
					this.createNameHistory(history.getChangedToAt(), history.getName());
				}
				List<NameHistory> sorted = list.stream().filter(h -> h.getChangedToAt() != null).collect(Collectors.toList());
				Collections.sort(sorted, Collections.reverseOrder(NameHistory.comparator()));
				NameHistory newestName = sorted.isEmpty() ? null : sorted.get(0);
				if (newestName != null)
				{
					if (!this.name.equals(newestName.getName()))
					{
						this.provider.getLogger()
							.info("Updating user's name " + this.name + " to " + newestName.getName() + " due to name change");
						this.changeName(newestName.getName());
					}
				}
			}
			this.changeCheckNameHistory(false);
		} catch (Exception e)
		{
			e.printStackTrace();
			this.provider.getLogger().warning("Failed to check name history of " + this.name);
		}
	}

	@Override
	public UserChat createChat(UserChatPojo pojo)
	{
		if (this.connect != null)
		{
			pojo.setConnectId(this.connect.getConnectId());
		}
		pojo.setChatId(this.provider.getDAO().getUserDAO().createChat(pojo));
		return this.provider.getUserModule().getFactory().createChat(pojo);
	}

	@Override
	public UserConnect createConnect(UserConnectPojo pojo)
	{
		pojo.setUserId(this.userId);
		pojo.setConnectId(this.provider.getDAO().getUserDAO().createConnect(pojo));
		return this.provider.getUserModule().getFactory().createConnect(pojo);
	}

	@Override
	public void createLatestConnect(String name, String host)
	{
		UserConnect connect = this.createConnect(new UserConnectPojo(0, this.userId,
			this.provider.getServerModule().getLocalServer().findRuntime().getRuntimeId(), name, host, new Date(), null));
		this.connect = connect;
		this.localConnect = connect;
		UserPojo pojo = this.toPojo();
		pojo.setConnectId(connect.getConnectId());
		this.provider.getDAO().getUserDAO().updateUser(pojo);
		this.setPojo(pojo);
	}

	@Override
	public UserNote createNote(UserNotePojo note)
	{
		note.setId(this.provider.getDAO().getUserDAO().createNote(note));
		return this.provider.getUserModule().getFactory().createNote(note);
	}

	@Override
	public SimplePage<UserNote> findNotes(int startIndex, int maxResults)
	{
		SimplePage<UserNotePojo> notes = this.provider.getDAO().getUserDAO().findNotes(this.userId, startIndex, maxResults);
		List<UserNote> mappedNotes =
			notes.getData().stream().map(note -> this.provider.getUserModule().getFactory().createNote(note)).collect(Collectors.toList());
		return new SimplePage<>(notes.getCurrentPage(), notes.getMaxResults(), notes.getTotalResults(), mappedNotes);
	}

	@Override
	public <T> T getAttachment(Class<T> key)
	{
		Validate.notNull(key, "Key cannot be null");
		this.initializeAttachments();
		T value = key.cast(this.attachments.get(key));
		if (value == null)
		{
			UserAttachmentCreate<T> create = this.provider.getUserModule().getAttachmentCreate(key);
			if (create != null)
			{
				value = create.createAttachment(this);
				if (value != null)
				{
					this.addAttachment(key, value);
				}
			}
		}
		return value;
	}

	@Override
	public UserConnect getConnect()
	{
		if (this.connect == null || this.connectId == null || this.connect.getConnectId() != this.connectId)
		{
			UserConnectPojo pojo = this.provider.getDAO().getUserDAO().findLatestConnect(this.userId);
			if (pojo != null)
			{
				this.connect = this.provider.getUserModule().getFactory().createConnect(pojo);
			}
		}
		return this.connect;
	}

	public Integer getConnectId()
	{
		return this.connectId;
	}

	@Override
	public String getIp()
	{
		Player player = this.getPlayer();
		if (player != null) { return player.getAddress().getHostString(); }
		UserConnect connect = this.getConnect();
		return connect == null ? null : connect.getIp();
	}

	@Override
	public UserLanguagePreferences getLanguagePreferences()
	{
		if (this.userLanguagePreferences == null)
		{
			List<UserLanguagePojo> languages = this.provider.getDAO().getUserDAO().findUserLanguages(this.userId);
			this.userLanguagePreferences = new UserLanguagePreferences(this.provider, this.userId, languages);
		}
		return this.userLanguagePreferences;
	}

	@Override
	public UserConnect getLocalConnect()
	{
		return this.localConnect;
	}

	@Override
	public String getName()
	{
		return this.name;
	}

	@Override
	public Player getPlayer()
	{
		if (this.type != UserType.MINECRAFT_PLAYER) { return null; }
		if (this.player == null)
		{
			if (this.provider instanceof NetworkCoreSpigot)
			{
				this.player = ((NetworkCoreSpigot) this.provider).getServer().getPlayer(this.uuid);
			}
		}
		if (this.player != null && !this.player.isOnline())
		{
			this.player = null;
		}
		return this.player;
	}

	@Override
	public UserType getType()
	{
		return this.type;
	}

	@Override
	public int getUserId()
	{
		return this.userId;
	}

	@Override
	public UUID getUUID()
	{
		return this.uuid;
	}

	@Override
	public boolean hasAttachment(Class<?> key)
	{
		Validate.notNull(key, "Key cannot be null");
		this.initializeAttachments();
		return this.attachments.containsKey(key);
	}

	@Override
	public boolean hasConnect()
	{
		return this.connect != null || this.connectId != null;
	}

	@Override
	public boolean hasPermission(String permission)
	{
		if (this.type == UserType.MINECRAFT_CONSOLE) { return true; }
		if (this.type != UserType.MINECRAFT_PLAYER) { return false; }
		Player player = this.getPlayer();
		if (player != null) { return player.hasPermission(permission); }
		if (this.provider instanceof NetworkCoreSpigot)
		{
			Permission permissionsAPI = ((NetworkCoreSpigot) this.provider).getPermissionsAPI();
			if (permissionsAPI != null) { return permissionsAPI.playerHas(null, this.getOfflinePlayer(), permission); }
		}
		return false;
	}

	@Override
	public boolean hasUserLanguage()
	{
		return this.userLanguagePreferences != null;
	}

	@Override
	public boolean isAutoLanguage()
	{
		return this.autoLanguage;
	}

	@Override
	public boolean isCached()
	{
		return this.cache.isCached();
	}

	@Override
	public void isCached(boolean isCached)
	{
		this.cache.isCached(isCached);
	}

	@Override
	public boolean isCacheExpired()
	{
		return this.cache.isCacheExpired();
	}

	@Override
	public boolean keepCached()
	{
		return this.cache.keepCached();
	}

	@Override
	public void keepCached(boolean keepCached)
	{
		this.cache.keepCached(keepCached);
	}

	@Override
	public void refresh()
	{
		UserPojo pojo = this.provider.getDAO().getUserDAO().findUserById(this.userId);
		this.setPojo(pojo);
	}

	@Override
	public void removeAttachment(Class<?> key)
	{
		Validate.notNull(key, "Key cannot be null");
		this.initializeAttachments();
		this.attachments.remove(key);
	}

	@Override
	public void resetCacheExpiration()
	{
		this.cache.resetCacheExpiration();
	}

	@Override
	public void sendLocalMessage(BaseComponent... components)
	{
		if (this.type == UserType.MINECRAFT_PLAYER)
		{
			Player player = this.getPlayer();
			if (player != null)
			{
				player.spigot().sendMessage(components);
				return;
			}
		} else if (this.type == UserType.MINECRAFT_CONSOLE && this.provider instanceof NetworkCore)
		{
			for (BaseComponent c : components)
			{
				((NetworkCoreSpigot) this.provider).getServer().getConsoleSender()
					.sendMessage(ChatColor.translateAlternateColorCodes('&', c.toLegacyText()));
			}
		}
	}

	@Override
	public void sendLocalMessage(String message)
	{
		if (this.type == UserType.MINECRAFT_PLAYER)
		{
			Player player = this.getPlayer();
			if (player != null)
			{
				player.sendMessage(message);
				return;
			}
		} else if (this.type == UserType.MINECRAFT_CONSOLE && this.provider instanceof NetworkCore)
		{
			((NetworkCoreSpigot) this.provider).getServer().getConsoleSender().sendMessage(message);
		}
	}

	@Override
	public void sendLocalMessageWithPermission(String permission, BaseComponent... components)
	{
		if (this.type == UserType.MINECRAFT_PLAYER)
		{
			Player player = this.getPlayer();
			if (player != null)
			{
				if (!player.hasPermission(permission)) { return; }
				player.spigot().sendMessage(components);
				return;
			}
		} else if (this.type == UserType.MINECRAFT_CONSOLE && this.provider instanceof NetworkCore)
		{
			for (BaseComponent c : components)
			{
				((NetworkCoreSpigot) this.provider).getServer().getConsoleSender()
					.sendMessage(ChatColor.translateAlternateColorCodes('&', c.toLegacyText()));
			}
		}
	}

	@Override
	public void sendLocalMessageWithPermission(String message, String permission)
	{
		if (this.type == UserType.MINECRAFT_PLAYER)
		{
			Player player = this.getPlayer();
			if (player != null)
			{
				if (!player.hasPermission(permission)) { return; }
				player.sendMessage(message);
				return;
			}
		} else if (this.type == UserType.MINECRAFT_CONSOLE)
		{
			((NetworkCoreSpigot) this.provider).getServer().getConsoleSender().sendMessage(message);
		}
	}

	@Override
	public void sendMessage(BaseComponent... components)
	{
		this.sendMessageWithPermission(null, components);
	}

	@Override
	public void sendMessage(String message)
	{
		this.sendMessageWithPermission(message, (String) null);
	}

	@Override
	public void sendMessageWithPermission(String permission, BaseComponent... components)
	{
		if (this.type == UserType.MINECRAFT_PLAYER)
		{
			Player player = this.getPlayer();
			if (player != null)
			{
				if (permission != null && !player.hasPermission(permission)) { return; }
				player.spigot().sendMessage(components);
				return;
			}
			this.provider.getNetworkModule().sendMessageWithPermission(this.userId, permission, components);
		} else if (this.type == UserType.MINECRAFT_CONSOLE)
		{
			for (BaseComponent c : components)
			{
				((NetworkCoreSpigot) this.provider).getServer().getConsoleSender()
					.sendMessage(ChatColor.translateAlternateColorCodes('&', c.toLegacyText()));
			}
		}
	}

	@Override
	public void sendMessageWithPermission(String message, String permission)
	{
		if (this.type == UserType.MINECRAFT_PLAYER)
		{
			Player player = this.getPlayer();
			if (player != null)
			{
				if (permission != null && !player.hasPermission(permission)) { return; }
				player.sendMessage(message);
				return;
			}
			this.provider.getNetworkModule().sendMessageWithPermission(this.userId, message, permission);
		} else if (this.type == UserType.MINECRAFT_CONSOLE)
		{
			((NetworkCoreSpigot) this.provider).getServer().getConsoleSender().sendMessage(message);
		}
	}

	@Override
	public void sendToServer(int serverId)
	{
		Validate.notNull(serverId);
		if (this.type != UserType.MINECRAFT_PLAYER) { throw new IllegalStateException(
			"Cannot send non-minecraft player to server; type " + this.type); }
		Player player = this.getPlayer();
		if (player != null)
		{
			ByteArrayDataOutput out = ByteStreams.newDataOutput();
			out.writeUTF("Connect");
			out.writeUTF(Server.getServer(serverId).getName());
			player.sendPluginMessage(NetworkCoreSpigot.getInstance(), "BungeeCord", out.toByteArray());
			return;
		}
		this.provider.getNetworkModule().sendUserToServer(this, serverId);
	}

	@Override
	public void setConnect(UserConnect connect)
	{
		this.connect = connect;
	}

	@Override
	public void setLanguagePreferences(UserLanguagePreferences userLanguagePreferences)
	{
		this.userLanguagePreferences = userLanguagePreferences;
	}

	@Override
	public void setLocalConnect(UserConnect localConnect)
	{
		if (localConnect != null)
		{
			if (localConnect.getServer().getServerId() != this.provider.getServerModule().getLocalServer()
				.getServerId()) { throw new IllegalStateException(
					"Cannot set local connect when the user connection is to another server"); }
		}
		this.localConnect = localConnect;
	}

	@Override
	public void setPlayer(Player player)
	{
		this.player = player;
	}

	@Override
	public String toString()
	{
		return "User [userId=" + this.userId + ", type=" + this.type + ", uuid=" + this.uuid + ", name=" + this.name + "]";
	}

	/**
	 * Creates a new name history record.
	 * 
	 * @param changedToAt
	 * @param name
	 */
	private void createNameHistory(Date changedToAt, String name)
	{
		UserNameHistoryPojo history = this.getNameHistory(changedToAt);
		if (history != null) { return; }
		this.provider.getDAO().getUserDAO().createUserNameHistory(new UserNameHistoryPojo(this.userId, changedToAt, name));
	}

	private List<UserNameHistoryPojo> getNameHistory()
	{
		return this.provider.getDAO().getUserDAO().findUserNameHistory(this.userId);
	}

	private UserNameHistoryPojo getNameHistory(Date changedToAt)
	{
		return this.getNameHistory().stream()
			.filter(history -> history.getChangedAt() == changedToAt
				|| history.getChangedAt() != null && history.getChangedAt().equals(changedToAt)
				|| DateUtil.differenceLessThan(history.getChangedAt(), changedToAt, 1000))
			.findAny().orElse(null);
	}

	private OfflinePlayer getOfflinePlayer()
	{
		OfflinePlayer player = this.getPlayer();
		if (player == null && this.provider instanceof NetworkCoreSpigot)
		{
			player = ((NetworkCoreSpigot) this.provider).getServer().getOfflinePlayer(this.uuid);
		}
		return player;
	}

	private void initializeAttachments()
	{
		if (this.attachments == null)
		{
			this.attachments = new ConcurrentHashMap<>();
		}
	}

	private void setPojo(UserPojo pojo)
	{
		this.uuid = pojo.getUUID();
		this.name = pojo.getUserName();
		this.connectId = pojo.getConnectId();
		this.autoLanguage = pojo.isAutoLanguage();
	}

	private UserPojo toPojo()
	{
		return new UserPojo(this.userId, this.type, this.uuid, this.name, this.connectId, this.checkNameHistory, this.autoLanguage);
	}
}
