package net.rieksen.networkcore.web;

public class NetworkCoreWebLink {
    public static final String NETWORKCORE_RESOURCE_URL ="https://networkcore.org/resources/NetworkCore.1";
    public static final String TOPIC_UNIQUE_SERVER_ID = NETWORKCORE_RESOURCE_URL + "/topics/Unique%20Server%20Id.8";
}
