package net.rieksen.networkcore.spigot.material;

public enum ItemHead
{
	ZOMBIE(2),
	PLAYER(3),
	CREEPER(4),
	DRAGON(5);

	private short data;

	private ItemHead(int data)
	{
		this.data = (short) data;
	}

	public short getDamage()
	{
		return this.data;
	}
}
