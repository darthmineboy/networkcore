package net.rieksen.networkcore.spigot.material;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import net.rieksen.networkcore.spigot.util.ReflectionUtil;

public class ItemStackUtil
{

	private static boolean VERSION_1_12 = ReflectionUtil.isVersionGreaterOrEqual(1, 12, Integer.MAX_VALUE);

	public static ItemStack createBanner(ItemColor color, int amount)
	{
		return ItemStackUtil.createColoredItem("BANNER", color, amount);
	}

	@SuppressWarnings("deprecation")
	public static ItemStack createHead(ItemHead headType, int amount)
	{
		if (ItemStackUtil.VERSION_1_12)
		{
			Material material = Material.valueOf("SKULL_ITEM");
			return new ItemStack(material, amount, headType.getDamage());
		}
		Material material = Material.valueOf(String.format("%s_%s", headType.toString(), "HEAD"));
		return new ItemStack(material, amount);
	}

	public static ItemStack createStainedGlassPane(ItemColor color, int amount)
	{
		return ItemStackUtil.createColoredItem("STAINED_GLASS_PANE", color, amount);
	}

	@SuppressWarnings("deprecation")
	public static void setSkullMetaOwner(SkullMeta meta, OfflinePlayer owner)
	{
		if (ItemStackUtil.VERSION_1_12)
		{
			meta.setOwner(owner.getName());
			return;
		}
		meta.setOwningPlayer(owner);
	}

	@SuppressWarnings("deprecation")
	public static void setSkullMetaOwner(SkullMeta meta, String owner)
	{
		if (ItemStackUtil.VERSION_1_12)
		{
			meta.setOwner(owner);
			return;
		}
		meta.setOwningPlayer(Bukkit.getOfflinePlayer(owner));
	}

	/**
	 * Since 1.13 item colors are defined by a prefix before the materialName.
	 * 
	 * @param materialName
	 *            name in < 1.13, e.g. STAINED_GLASS_PANE instead of
	 *            COLOR_STAINED_GLASS_PANE
	 * @param color
	 * @param amount
	 * @return
	 */
	@SuppressWarnings("deprecation")
	private static ItemStack createColoredItem(String materialName, ItemColor color, int amount)
	{
		if (ItemStackUtil.VERSION_1_12)
		{
			Material material = Material.valueOf(materialName);
			return new ItemStack(material, amount, color.getDamage());
		}
		Material material = Material.valueOf(String.format("%s_%s", color.toString(), materialName));
		return new ItemStack(material, amount);
	}
}
