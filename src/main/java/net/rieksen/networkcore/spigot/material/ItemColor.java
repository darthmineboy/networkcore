package net.rieksen.networkcore.spigot.material;

public enum ItemColor
{
	WHITE(0),
	ORANGE(1),
	MAGENTA(2),
	LIGHT_BLUE(3),
	YELLOW(4),
	LIME(5),
	PINK(6),
	GRAY(7),
	LIGHT_GRAY(8),
	CYAN(9),
	PURPLE(10),
	BLUE(11),
	BROWN(12),
	GREEN(13),
	RED(14),
	BLACK(15);

	public static ItemColor ofDamage(short damage)
	{
		for (ItemColor color : ItemColor.values())
		{
			if (color.damage == damage) { return color; }
		}
		return null;
	}

	private short damage;

	private ItemColor(int damage)
	{
		this.damage = (short) damage;
	}

	public short getDamage()
	{
		return this.damage;
	}
}
