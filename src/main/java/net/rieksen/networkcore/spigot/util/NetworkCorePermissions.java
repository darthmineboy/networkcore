package net.rieksen.networkcore.spigot.util;

public enum NetworkCorePermissions
{
	NETWORKCORE_EXPORT,
	NETWORKCORE_IMPORT;

	@Override
	public String toString()
	{
		return this.name().toLowerCase().replace("_", ".");
	}
}
