package net.rieksen.networkcore.spigot.util;

import org.bukkit.Bukkit;

public class ReflectionUtil
{

	private static String version;

	public static Class<?> getCraftClass(String className) throws ClassNotFoundException
	{
		return Class.forName("org.bukkit.craftbukkit." + ReflectionUtil.getVersion() + "." + className);
	}

	public static String getVersion()
	{
		if (ReflectionUtil.version != null) { return ReflectionUtil.version; }
		ReflectionUtil.version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
		return ReflectionUtil.version;
	}

	/**
	 * Whether the provided version is greater or equal to the installed
	 * version.
	 * 
	 * @param major
	 * @param minor
	 * @param patch
	 * @return true if provided >= installed
	 */
	public static boolean isVersionGreaterOrEqual(int major, int minor, int patch)
	{
		String version = ReflectionUtil.getVersion();
		String[] versionParts = version.split("\\_");
		if (versionParts.length < 2 || versionParts.length > 4) { throw new IllegalStateException(
			String.format("Unexpected version format: expected major.minor[.patch][-identifier] but was '%s'", version)); }
		int parsedMajor = Integer.parseInt(versionParts[0].replace("v", ""));
		if (major > parsedMajor)
		{
			return true;
		} else if (major < parsedMajor) { return false; }
		int parsedMinor = Integer.parseInt(versionParts[1]);
		if (minor > parsedMinor)
		{
			return true;
		} else if (minor < parsedMinor) { return false; }
		int parsedPatch = 0;
		if (versionParts.length == 3)
		{
			try
			{
				parsedPatch = Integer.parseInt(versionParts[2]);
			} catch (NumberFormatException e)
			{
				// is identifier, e.g. 1_12_R1 rather than 1_12_0_R1
			}
		} else
		{
			parsedPatch = Integer.parseInt(versionParts[2]);
		}
		// identifier is ignored
		return patch >= parsedPatch;
	}
}
