package net.rieksen.networkcore.spigot.util;

import java.util.List;
import java.util.Map;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.rieksen.networkcore.core.util.TextUtil;

public class MinecraftUtil
{

	public static BaseComponent[] replaceVariables(BaseComponent[] components, Map<String, String> variables)
	{
		for (BaseComponent c : components)
		{
			if (c instanceof TextComponent)
			{
				((TextComponent) c).setText(TextUtil.replaceVariables(((TextComponent) c).getText(), variables));
			}
			ClickEvent clickEvent = c.getClickEvent();
			if (clickEvent != null)
			{
				c.setClickEvent(new ClickEvent(clickEvent.getAction(), TextUtil.replaceVariables(clickEvent.getValue(), variables)));
			}
			HoverEvent hoverEvent = c.getHoverEvent();
			if (hoverEvent != null)
			{
				c.setHoverEvent(new HoverEvent(hoverEvent.getAction(),
					MinecraftUtil.replaceVariables(MinecraftUtil.replaceVariables(hoverEvent.getValue(), variables), variables)));
			}
			List<BaseComponent> extra = c.getExtra();
			if (extra != null)
			{
				MinecraftUtil.replaceVariables((BaseComponent[]) extra.toArray(), variables);
			}
		}
		return components;
	}
}
