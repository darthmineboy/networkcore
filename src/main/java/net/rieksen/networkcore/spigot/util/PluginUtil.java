package net.rieksen.networkcore.spigot.util;

import java.io.File;
import java.lang.reflect.Method;

import org.apache.commons.lang.Validate;
import org.bukkit.plugin.java.JavaPlugin;

public class PluginUtil
{

	/**
	 * Get the file (.jar) that is associated with this plugin.
	 * 
	 * @param plugin
	 * @return
	 */
	public static File getPluginFile(JavaPlugin plugin)
	{
		Validate.notNull(plugin);
		try
		{
			Method m = JavaPlugin.class.getDeclaredMethod("getFile");
			m.setAccessible(true);
			return (File) m.invoke(plugin);
		} catch (Exception e)
		{
			throw new RuntimeException("Failed to invoke method getFile()", e);
		}
	}
}
