package net.rieksen.networkcore.spigot;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipFile;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.FileUtil;

import com.google.common.io.Files;

import net.rieksen.networkcore.core.info.NetworkCoreInfo;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.core.util.PluginUtil;
import net.rieksen.networkcore.core.web.DownloadResponse;
import net.rieksen.networkcore.core.web.ResourceMeta;
import net.rieksen.networkcore.core.web.ResourcePojo;

public class ResourceUpdater
{

	public static final long	DELAY		= 20 * 10;
	public static final long	INTERVAL	= 20 * 86400;
	private NetworkCoreSpigot	provider;

	public ResourceUpdater(NetworkCoreSpigot provider)
	{
		this.provider = provider;
	}

	public void updateAll()
	{
		for (Plugin plugin : this.provider.getServer().getPluginManager().getPlugins())
		{
			try
			{
				this.updatePlugin(plugin);
			} catch (Exception e)
			{
				this.provider.getLogger().log(Level.WARNING, "Failed to check updates for " + plugin.getName(), e);
			}
		}
	}

	void downloadResource(File file, int resourceId, String version, String installationSecret)
	{
		file.getParentFile().mkdirs();
		try
		{
			DownloadResponse download =
				this.provider.getWebClient().downloadResourceWithInstallation(resourceId, version, installationSecret);
			InputStream is = download.getInputStream();
			OutputStream out = new FileOutputStream(file);
			long bytesDownloaded = 0;
			long bytesToDownload = download.getSize();
			byte[] buffer = new byte[4096];
			int bytesRead;
			int lastNotify = -1;
			while ((bytesRead = is.read(buffer)) != -1)
			{
				bytesDownloaded += bytesRead;
				int progress = (int) (bytesDownloaded * 100 / bytesToDownload);
				if (bytesToDownload != -1 && progress / 10 != lastNotify)
				{
					lastNotify = progress / 10;
					this.getLogger().info(String.format("Downloaded %s KB / %s KB (%s)", bytesDownloaded / 1024, bytesToDownload / 1024,
						String.valueOf(progress) + "%"));
				}
				out.write(buffer, 0, bytesRead);
			}
			out.close();
			is.close();
		} catch (Exception e)
		{
			throw new RuntimeException("Failed to download resource", e);
		}
	}

	/**
	 * Introduced for testing
	 * 
	 * @return
	 */
	Logger getLogger()
	{
		return this.provider.getLogger();
	}

	private boolean isValidZip(File file)
	{
		try (ZipFile zipFile = new ZipFile(file))
		{
			return true;
		} catch (IOException e)
		{
			return false;
		}
	}

	private void updatePlugin(Plugin plugin)
	{
		ResourceMeta meta = plugin.getClass().getAnnotation(ResourceMeta.class);
		if (meta == null) { return; }
		String currentVersion = plugin.getDescription().getVersion();
		this.provider.getLogger().info("Checking for updates for " + plugin.getName() + ", current version " + currentVersion);
		ResourcePojo resource;
		try
		{
			resource = this.provider.getWebClient().findResource(meta.resourceId());
		} catch (IllegalStateException e)
		{
			this.provider.getLogger().warning(String
				.format("Failed to find updates, restart the server to fix (known issue; caused by reload). Error: %s", e.getMessage()));
			return;
		} catch (Exception e)
		{
			this.provider.getLogger().warning(String.format("Failed to read resource: %s", e.getMessage()));
			// After update + reload the webclient classes are not loaded
			// properly, unable to find fix
			return;
		}
		if (resource == null)
		{
			this.provider.getLogger().info("Could not find resource with id #" + meta.resourceId());
			return;
		}
		String latestVersion = resource.getLatestVersion();
		if (latestVersion == null)
		{
			this.provider.getLogger().info("The resource with id #" + meta.resourceId() + " has no latest version");
			return;
		}
		if (latestVersion.equalsIgnoreCase(currentVersion))
		{
			this.provider.getLogger().info("You are running the latest version of " + plugin.getName());
			return;
		}
		NetworkPlugin networkPlugin = this.provider.getPluginModule().getPlugin(plugin);
		if (latestVersion.equalsIgnoreCase(networkPlugin.getDownloadedUpdateVersion()))
		{
			this.provider.getLogger().info("An update is available for " + plugin.getName() + ", restart required to install");
			return;
		}
		if (PluginUtil.isVersionGreater(currentVersion, latestVersion))
		{
			this.provider.getLogger().info("An update is available for " + plugin.getName() + ", version " + latestVersion);
			if (plugin instanceof JavaPlugin)
			{
				this.provider.getLogger().info("Downloading update for " + plugin.getName() + ", restart required to install");
				String pluginFile = net.rieksen.networkcore.spigot.util.PluginUtil.getPluginFile((JavaPlugin) plugin).getName();
				File updateFolder = this.provider.getServer().getUpdateFolderFile();
				updateFolder.mkdirs();
				File updateFile = new File(updateFolder, pluginFile);
				File tempFolder = new File(this.provider.getDataFolder(), "temp");
				tempFolder.mkdirs();
				File tempFile = new File(tempFolder, pluginFile);
				if (tempFile.exists())
				{
					tempFile.delete();
				}
				String installationSecret = this.provider.getInfoModule().getInfo(NetworkCoreInfo.INSTALLATION_SECRET);
				this.downloadResource(tempFile, meta.resourceId(), latestVersion, installationSecret);
				if (!this.isValidZip(tempFile))
				{
					this.provider.getLogger().severe(
						String.format("Downloaded update for %s, but the downloaded file is not a valid zip archive", plugin.getName()));
					return;
				}
				try
				{
					Files.move(tempFile, updateFile);
				} catch (IOException e)
				{
					e.printStackTrace();
				} catch (Throwable e)
				{
					FileUtil.copy(tempFile, updateFile);
					tempFile.delete();
				}
				networkPlugin.setDownloadedUpdateVersion(latestVersion);
			} else
			{
				this.provider.getLogger().info("This update needs to be downloaded manually");
			}
		} else
		{
			this.provider.getLogger()
				.info("You are running an unreleased (experimental/dev) version of " + plugin.getName() + ". Happy testing!");
		}
	}
}
