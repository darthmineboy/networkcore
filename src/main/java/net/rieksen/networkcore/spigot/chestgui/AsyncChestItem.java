package net.rieksen.networkcore.spigot.chestgui;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

public abstract class AsyncChestItem extends ChestItem {
    private final Plugin plugin;

    public AsyncChestItem(Plugin plugin, ItemStack itemStack) {
        super(itemStack);
        this.plugin = plugin;
    }

    @Override
    public final void onInventoryClick(InventoryClickEvent e) {
        plugin.getServer().getScheduler().runTask(plugin, () -> onAsyncInventoryClick(e));
    }

    public abstract void onAsyncInventoryClick(InventoryClickEvent e);

}
