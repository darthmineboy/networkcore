package net.rieksen.networkcore.spigot.chestgui.loader;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import net.rieksen.networkcore.spigot.chestgui.ChestGUI;
import net.rieksen.networkcore.spigot.material.ItemColor;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

public class ChestGUILoader implements IChestGUILoader
{

	private ChestGUI	gui;
	private JavaPlugin	plugin;
	private int			ticks;
	private BukkitTask	task;

	public ChestGUILoader(JavaPlugin plugin, ChestGUI gui)
	{
		this.gui = gui;
		this.plugin = plugin;
	}

	@Override
	public void start()
	{
		this.task = new BukkitRunnable()
		{

			@Override
			public void run()
			{
				ChestGUILoader.this.tick();
			}
		}.runTaskTimer(this.plugin, 0, 3);
	}

	@Override
	public void stop()
	{
		this.task.cancel();
	}

	public void tick()
	{
		int tick = this.ticks % 9;
		Inventory inv = this.getInventory();
		ItemStack background = ItemStackUtil.createStainedGlassPane(ItemColor.YELLOW, 1);
		ItemStack primary = ItemStackUtil.createStainedGlassPane(ItemColor.GRAY, 1);
		int slot = this.calculateSlot(0);
		for (int i = 0; i < 9; i++)
		{
			if (tick == i)
			{
				inv.setItem(slot + i, primary);
			} else
			{
				inv.setItem(slot + i, background);
			}
		}
		this.gui.updateViewers();
		this.ticks++;
	}

	private int calculateMiddleRow(Inventory inv)
	{
		return this.calculateRows(inv) / 2 + 1;
	}

	private int calculateRows(Inventory inv)
	{
		return inv.getSize() / 9;
	}

	private int calculateSlot(int slot)
	{
		Inventory inv = this.getInventory();
		int row = this.calculateMiddleRow(inv);
		return (row - 1) * 9 + slot;
	}

	private Inventory getInventory()
	{
		return this.gui.getInventory();
	}
}
