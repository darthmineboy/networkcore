package net.rieksen.networkcore.spigot.chestgui.defaults;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import net.md_5.bungee.api.ChatColor;
import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.util.LoreUtil;
import net.rieksen.networkcore.spigot.chestgui.ChestItem;
import net.rieksen.networkcore.spigot.chestgui.IChestItem;
import net.rieksen.networkcore.spigot.chestgui.PageChestGUI;
import net.rieksen.networkcore.spigot.material.ItemHead;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

public class UserListGUI extends PageChestGUI
{

	private NetworkCore plugin;

	public UserListGUI(NetworkCore plugin)
	{
		super(true);
		this.plugin = plugin;
	}

	@Override
	protected Inventory createInventory()
	{
		// TODO locale
		return Bukkit.createInventory(null, 54, ChatColor.translateAlternateColorCodes('&', "&4User List"));
	}

	@Override
	protected int[] getContentSlots()
	{
		return this.createContentSlots(0, 45);
	}

	@Override
	protected List<IChestItem> getDisplayingItems(int page, int itemsPerPage)
	{
		List<User> users = this.plugin.getUserModule().getUsers(page * itemsPerPage, itemsPerPage);
		List<IChestItem> items = new ArrayList<>();
		for (final User user : users)
		{
			ItemStack item = ItemStackUtil.createHead(ItemHead.PLAYER, 1);
			SkullMeta meta = (SkullMeta) item.getItemMeta();
			ItemStackUtil.setSkullMetaOwner(meta, user.getName());
			// TODO locale
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&6&l" + user.getName()));
			// TODO locale
			meta.setLore(LoreUtil.createLore("&eUser ID &8: &f#" + user.getUserId() + "\n&eUUID &8: &f" + user.getUUID() + "\n&eType &8: &f"
				+ user.getType().toString()));
			item.setItemMeta(meta);
			items.add(new ChestItem(item)
			{

				private int userId;

				public ChestItem init(int userId)
				{
					this.userId = userId;
					return this;
				}

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					new UserManagementGUI(this.userId).open((Player) e.getWhoClicked());;
				}
			}.init(user.getUserId()));
		}
		return items;
	}

	@Override
	protected int getTotalCount()
	{
		return this.plugin.getUserModule().getUserCount();
	}
}
