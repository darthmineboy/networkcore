package net.rieksen.networkcore.spigot.chestgui;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;

public abstract class ChestGUI
{

	private Inventory inventory;

	public final void close(Player player)
	{
		this.getChestManager().closeChestGUI(player, this);
		player.closeInventory();
	}

	public final Inventory getInventory()
	{
		if (this.inventory == null)
		{
			this.inventory = this.createInventory();
			return this.inventory;
		}
		return this.inventory;
	}

	public final void open(Player player)
	{
		this.getChestManager().openChestGUI(player, this);
		InventoryView view = player.openInventory(this.getInventory());
		if (view == null) { return; }
		// inventory is cloned
		this.inventory = view.getTopInventory();
	}

	/**
	 * Updates the inventory for all viewers. Useful for when inventory content
	 * has changed after the inventory has been opened.
	 */
	public void updateViewers()
	{
		if (this.inventory != null)
		{
			this.inventory.getViewers().stream().filter(viewer -> viewer instanceof Player)
				.forEach(player -> ((Player) player).updateInventory());
		}
	}

	protected abstract Inventory createInventory();

	protected ChestManager getChestManager()
	{
		return ChestManager.getInstance();
	}

	protected void onClose(Player player)
	{}

	protected abstract void onInventoryClick(InventoryClickEvent e);

	protected void onInventoryClose(InventoryCloseEvent e)
	{
		if (e.getPlayer() instanceof Player)
		{
			Player player = (Player) e.getPlayer();
			this.getChestManager().closeChestGUI(player, this);
			this.onClose(player);
		}
	}

	protected void onInventoryOpen(InventoryOpenEvent e)
	{
		if (e.getPlayer() instanceof Player)
		{
			this.onOpen((Player) e.getPlayer());
		}
	}

	protected void onOpen(Player player)
	{}
}
