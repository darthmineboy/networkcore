package net.rieksen.networkcore.spigot.chestgui;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public interface IChestItem {

    ItemStack getItemStack();

    void onInventoryClick(InventoryClickEvent e);

}
