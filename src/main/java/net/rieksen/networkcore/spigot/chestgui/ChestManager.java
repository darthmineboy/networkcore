package net.rieksen.networkcore.spigot.chestgui;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;

public class ChestManager implements Listener
{

	private static ChestManager instance = new ChestManager();

	public static ChestManager getInstance()
	{
		return ChestManager.instance;
	}

	private Map<Player, ChestGUI> players = new ConcurrentHashMap<>();

	@EventHandler
	public void onInventoryClick(InventoryClickEvent e)
	{
		if (e.getWhoClicked() instanceof Player)
		{
			ChestGUI gui = this.getCurrent((Player) e.getWhoClicked());
			if (gui != null)
			{
				e.setCancelled(true);
				gui.onInventoryClick(e);
			}
		}
	}

	@EventHandler
	public void onInventoryClose(InventoryCloseEvent e)
	{
		if (e.getPlayer() instanceof Player)
		{
			ChestGUI gui = this.getCurrent((Player) e.getPlayer());
			if (gui != null)
			{
				if (gui.getInventory() != e.getInventory()) { return; }
				gui.onInventoryClose(e);
			}
		}
	}

	@EventHandler
	public void onInventoryOpen(InventoryOpenEvent e)
	{
		if (e.getPlayer() instanceof Player)
		{
			ChestGUI gui = this.getCurrent((Player) e.getPlayer());
			if (gui != null)
			{
				gui.onInventoryOpen(e);
			}
		}
	}

	void closeChestGUI(Player player, ChestGUI gui)
	{
		this.players.remove(player, gui);
	}

	ChestGUI getCurrent(Player player)
	{
		return this.players.get(player);
	}

	void openChestGUI(Player player, ChestGUI gui)
	{
		ChestGUI currentGUI = this.getCurrent(player);
		if (currentGUI != null)
		{
			currentGUI.onClose(player);
		}
		this.players.put(player, gui);
	}
}
