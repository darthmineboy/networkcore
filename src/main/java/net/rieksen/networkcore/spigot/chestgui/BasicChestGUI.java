package net.rieksen.networkcore.spigot.chestgui;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang.Validate;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.chestgui.loader.ChestGUILoader;
import net.rieksen.networkcore.spigot.chestgui.loader.IChestGUILoader;

public abstract class BasicChestGUI extends ChestGUI
{

	protected Map<Integer, IChestItem>	items	= new ConcurrentHashMap<>();
	protected boolean					async;
	protected IChestGUILoader			loader;

	/**
	 * @param async
	 *            whether content should be set async
	 */
	public BasicChestGUI(boolean async)
	{
		this.async = async;
		// Loaders are awesome!
		this.loader = new ChestGUILoader(NetworkCoreSpigot.getInstance(), this);
	}

	protected void clearContent()
	{
		this.items.clear();
		this.getInventory().clear();
		this.updateViewers();
	}

	protected void clearItem(int slot)
	{
		this.items.remove(slot);
	}

	@Override
	protected void onInventoryClick(InventoryClickEvent e)
	{
		if (e.getClickedInventory() != this.getInventory()) { return; }
		if (this.items != null)
		{
			for (java.util.Map.Entry<Integer, IChestItem> entry : this.items.entrySet())
			{
				int slot = entry.getKey();
				if (slot == e.getSlot())
				{
					IChestItem item = entry.getValue();
					if (item == null) { return; }
					item.onInventoryClick(e);
				}
			}
		}
	}

	@Override
	protected void onOpen(Player player)
	{
		if (this.async)
		{
			new Thread(new Runnable()
			{

				@Override
				public void run()
				{
					BasicChestGUI.this.setContent(player);
					BasicChestGUI.this.updateViewers();
				}
			}).start();
			return;
		}
		this.setContent(player);
	}

	protected abstract Map<Integer, IChestItem> prepareContent(Player player);

	protected synchronized void setContent(Player player)
	{
		BasicChestGUI.this.startLoader(player);
		Map<Integer, IChestItem> content = this.prepareContent(player);
		this.stopLoader(player);
		this.clearContent();
		this.setItems(content);
	}

	protected void setItem(int slot, IChestItem item)
	{
		Validate.notNull(item);
		this.items.put(slot, item);
		this.getInventory().setItem(slot, item.getItemStack());
	}

	protected void setItems(Map<Integer, IChestItem> content)
	{
		Validate.notNull(content);
		content.forEach((slot, item) -> this.setItem(slot, item));
	}

	private void startLoader(Player player)
	{
		if (this.loader == null) { return; }
		this.loader.start();
	}

	private void stopLoader(Player player)
	{
		if (this.loader == null) { return; }
		this.loader.stop();
	}
}
