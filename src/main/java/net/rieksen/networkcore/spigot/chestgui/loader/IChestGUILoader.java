package net.rieksen.networkcore.spigot.chestgui.loader;

public interface IChestGUILoader
{

	public void start();

	public void stop();
}
