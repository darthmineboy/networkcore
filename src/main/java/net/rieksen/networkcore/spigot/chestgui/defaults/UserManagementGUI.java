package net.rieksen.networkcore.spigot.chestgui.defaults;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.util.LoreUtil;
import net.rieksen.networkcore.spigot.chestgui.BasicChestGUI;
import net.rieksen.networkcore.spigot.chestgui.ChestItem;
import net.rieksen.networkcore.spigot.chestgui.IChestItem;

public class UserManagementGUI extends BasicChestGUI
{

	private int targetId;

	public UserManagementGUI(int targetId)
	{
		super(true);
		this.targetId = targetId;
	}

	@Override
	protected Inventory createInventory()
	{
		// TODO locale
		return Bukkit.createInventory(null, 54, ChatColor.translateAlternateColorCodes('&', "&4User Management"));
	}

	@Override
	protected Map<Integer, IChestItem> prepareContent(Player player)
	{
		Map<Integer, IChestItem> content = new HashMap<>();
		User targetUser = User.getUser(this.targetId);
		{
			ItemStack item = new ItemStack(Material.NETHER_STAR);
			ItemMeta meta = item.getItemMeta();
			// TODO locale
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&6&l" + targetUser.getName()));
			// TODO locale
			meta.setLore(LoreUtil.createLore("&eUser ID &8: &f#" + targetUser.getUserId() + "\n&eUUID &8: &f" + targetUser.getUUID()
				+ "\n&eType &8: &f" + targetUser.getType().toString()));
			item.setItemMeta(meta);
			content.put(13, new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{}
			});
		}
		return content;
	}
}
