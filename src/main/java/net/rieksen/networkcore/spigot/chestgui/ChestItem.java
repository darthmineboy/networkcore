package net.rieksen.networkcore.spigot.chestgui;

import org.bukkit.inventory.ItemStack;

public abstract class ChestItem implements IChestItem {

    private ItemStack item;

    public ChestItem(ItemStack item) {
        this.item = item;
    }

    @Override
    public ItemStack getItemStack() {
        return this.item;
    }
}
