package net.rieksen.networkcore.spigot.chestgui;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;

import net.rieksen.networkcore.spigot.material.ItemColor;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

public abstract class PageChestGUI extends BasicChestGUI
{

	protected int	currentPage			= 0;
	protected int	pageBackItemSlot	= 45;
	protected int	pageForwardItemSlot	= 53;

	public PageChestGUI(boolean async)
	{
		super(async);
	}

	/**
	 * @param startIndex
	 *            inclusive
	 * @param endIndex
	 *            exclusive
	 * @return
	 */
	protected final int[] createContentSlots(int startIndex, int endIndex)
	{
		int slotsCount = endIndex - startIndex;
		int[] slots = new int[slotsCount];
		for (int i = 0; i < slotsCount; i++)
		{
			slots[i] = i + startIndex;
		}
		return slots;
	}

	protected abstract int[] getContentSlots();

	/**
	 * @param page
	 *            0 is the first page
	 * @param itemsPerPage
	 * @return
	 */
	protected abstract List<IChestItem> getDisplayingItems(int page, int itemsPerPage);

	protected IChestItem getPageBackHandler()
	{
		ItemStack item = ItemStackUtil.createBanner(ItemColor.GREEN, this.truncateMaxPage(this.currentPage));
		BannerMeta meta = (BannerMeta) item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&6&lPage Back"));
		meta.addPattern(new Pattern(DyeColor.YELLOW, PatternType.RHOMBUS_MIDDLE));
		meta.addPattern(new Pattern(DyeColor.GREEN, PatternType.HALF_VERTICAL_MIRROR));
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		item.setItemMeta(meta);
		return new ChestItem(item)
		{

			@Override
			public void onInventoryClick(InventoryClickEvent e)
			{
				if (PageChestGUI.this.currentPage <= 0) { return; }
				PageChestGUI.this.movePageDown((Player) e.getWhoClicked());
			}
		};
	}

	protected IChestItem getPageForwardHandler()
	{
		ItemStack item = ItemStackUtil.createBanner(ItemColor.GREEN, this.truncateMaxPage(this.calculateHighestPage() - this.currentPage));
		BannerMeta meta = (BannerMeta) item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&6&lPage Forward"));
		meta.addPattern(new Pattern(DyeColor.YELLOW, PatternType.RHOMBUS_MIDDLE));
		meta.addPattern(new Pattern(DyeColor.GREEN, PatternType.HALF_VERTICAL));
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		item.setItemMeta(meta);
		return new ChestItem(item)
		{

			@Override
			public void onInventoryClick(InventoryClickEvent e)
			{
				if (PageChestGUI.this.currentPage >= PageChestGUI.this.calculateHighestPage()) { return; }
				PageChestGUI.this.movePageUp((Player) e.getWhoClicked());
			}
		};
	}

	protected abstract int getTotalCount();

	protected final void movePageDown(Player player)
	{
		if (this.currentPage < 0) { return; }
		this.currentPage--;
		this.updateContent(player);
	}

	protected final void movePageUp(Player player)
	{
		this.currentPage++;
		this.updateContent(player);
	}

	@Override
	protected Map<Integer, IChestItem> prepareContent(Player player)
	{
		return this.preparePageContent();
	}

	protected final void setCurrentPage(Player player, int page)
	{
		if (page < 0) { return; }
		this.currentPage = page;
		this.updateContent(player);
	}

	private final int calculateHighestPage()
	{
		int items = this.getTotalCount();
		int itemsPerPage = this.getContentSlots().length;
		int pages = items / itemsPerPage;
		if (items % itemsPerPage != 0)
		{
			pages++;
		}
		pages--;
		return pages;
	}

	private Map<Integer, IChestItem> preparePageContent()
	{
		Map<Integer, IChestItem> content = new HashMap<>();
		int[] contentSlots = this.getContentSlots();
		/*
		 * for (int i : contentSlots) { this.clearItem(i); }
		 */
		List<IChestItem> items = this.getDisplayingItems(this.currentPage, contentSlots.length);
		int i = 0;
		for (IChestItem item : items)
		{
			content.put(contentSlots[i], item);
			i++;
			if (i >= contentSlots.length)
			{
				break;
			}
		}
		content.put(this.pageBackItemSlot, this.getPageBackHandler());
		content.put(this.pageForwardItemSlot, this.getPageForwardHandler());
		return content;
	}

	private int truncateMaxPage(int pages)
	{
		return Math.min(pages, 64);
	}

	private synchronized final void updateContent(Player player)
	{
		if (this.async)
		{
			new Thread(new Runnable()
			{

				@Override
				public void run()
				{
					PageChestGUI.this.setContent(player);
					PageChestGUI.this.updateViewers();
				}
			}).start();
		} else
		{
			this.setContent(player);
			this.updateViewers();
		}
	}
}
