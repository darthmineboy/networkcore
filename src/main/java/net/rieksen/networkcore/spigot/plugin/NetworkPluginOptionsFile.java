package net.rieksen.networkcore.spigot.plugin;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import net.rieksen.networkcore.core.option.Option;
import net.rieksen.networkcore.core.option.OptionSection;
import net.rieksen.networkcore.core.option.OptionType;
import net.rieksen.networkcore.core.option.OptionValue;
import net.rieksen.networkcore.core.option.pojo.OptionPojo;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.spigot.file.BasicYAML;

public class NetworkPluginOptionsFile extends BasicYAML<Plugin>
{

	private NetworkPlugin networkPlugin;

	public NetworkPluginOptionsFile(Plugin plugin, NetworkPlugin networkPlugin)
	{
		super(plugin, plugin.getDataFolder(), "networkcore-options.yml");
		this.networkPlugin = networkPlugin;
	}

	public void export()
	{
		this.yaml = new YamlConfiguration();
		this.networkPlugin.getOptionSections().forEach(section ->
		{
			ConfigurationSection optionSection = this.yaml.createSection(section.getName());
			optionSection.set("description", section.getDescription());
			ConfigurationSection options = optionSection.createSection("options");
			section.getOptions().forEach(optionPojo ->
			{
				ConfigurationSection option = options.createSection(optionPojo.getName());
				option.set("description", optionPojo.getDescription());
				option.set("type", optionPojo.getType().name());
				ConfigurationSection values = option.createSection("values");
				optionPojo.findValues().forEach(valuePojo ->
				{
					String serverId = valuePojo.getServerId() == null ? "default" : String.valueOf(valuePojo.getServerId());
					ConfigurationSection server = values.getConfigurationSection(serverId);
					if (server == null)
					{
						server = values.createSection(serverId);
					}
					server.set(String.valueOf(valuePojo.getIndex()), valuePojo.getValue());
				});
			});
		});
		this.save();
	}

	public void synchronize()
	{
		if (!this.load())
		{
			this.getLogger().severe("Failed to load file " + this.getFile().getPath());
			Exception e = this.exception;
			if (e instanceof InvalidConfigurationException)
			{
				this.getLogger().log(Level.SEVERE, "Invalid yaml! Creating backup and resetting...", e);
				this.copy(
					new File(this.file.getParentFile(), String.format("bk-%s-%s.yml", this.getFile().getName(), new Date().toString())));
				this.export();
			} else
			{
				return;
			}
		}
		Set<String> sectionNames = this.yaml.getKeys(false);
		// Delete sections that are not specified in the file
		new ArrayList<>(this.networkPlugin.getOptionSections()).stream().filter(section -> !sectionNames.contains(section.getName()))
			.forEach(section ->
			{
				this.getLogger().info("Deleting option section " + section.getName());
				this.networkPlugin.deleteOptionSection(section.getName());
			});
		for (String sectionName : sectionNames)
		{
			ConfigurationSection sectionYml = this.yaml.getConfigurationSection(sectionName);
			OptionSection section = this.getOrCreateOptionSection(sectionName, sectionYml);
			Set<String> optionNames = sectionYml.getConfigurationSection("options").getKeys(false);
			new ArrayList<>(section.getOptions()).stream().filter(option -> !optionNames.contains(option.getName())).forEach(option ->
			{
				this.getLogger().info("Deleting option " + option.getName() + " from section " + section.getName());
				section.deleteOption(option.getName());
			});
			for (String optionName : optionNames)
			{
				ConfigurationSection optionYml = sectionYml.getConfigurationSection("options." + optionName);
				Option option = this.getOrCreateOption(section, optionName, optionYml);
				Set<String> serverIds = optionYml.getConfigurationSection("values").getKeys(false);
				new ArrayList<>(option.findValues()).stream()
					.filter(value -> !serverIds.contains(value.getServerId() == null ? "default" : String.valueOf(value.getServerId())))
					.forEach(value ->
					{
						this.getLogger()
							.info("Deleting values of server "
								+ (value.getServerId() == null ? "default" : String.valueOf(value.getServerId())) + " of option "
								+ optionName + " in section " + section.getName());
						option.deleteValue(value.getValueId());
					});
				for (String serverId : serverIds)
				{
					ConfigurationSection serverYml = optionYml.getConfigurationSection("values." + serverId);
					Set<String> valueIndexes = serverYml.getKeys(false);
					new ArrayList<>(option.findValues()).stream().filter(value -> !valueIndexes.contains(String.valueOf(value.getIndex())))
						.forEach(value ->
						{
							this.getLogger().info("Deleting value index " + value.getIndex() + " of server " + serverId + " from option "
								+ option.getName() + " in section " + section.getName());
							option.deleteValue(serverId.equals("default") ? null : Integer.parseInt(serverId), value.getIndex());
						});
					for (String valueIndex : valueIndexes)
					{
						OptionValue value =
							option.getValue(serverId.equals("default") ? null : Integer.parseInt(serverId), Integer.parseInt(valueIndex));
						if (value == null)
						{
							this.getLogger().info("Creating value index " + valueIndex + " of server " + serverId + " from option "
								+ option.getName() + " in section " + section.getName());
							value = option.createValue(serverId.equals("default") ? null : Integer.parseInt(serverId),
								Integer.parseInt(valueIndex), serverYml.getString(valueIndex));
						}
						if (!StringUtils.equals(value.getValue(), serverYml.getString(valueIndex)))
						{
							this.getLogger().info("Updating value of option " + option.getName() + " in section " + section.getName()
								+ " of server " + serverId + " with index " + valueIndex + " to " + serverYml.getString(valueIndex));
							value.changeValue(serverYml.getString(valueIndex));
						}
					}
				}
			}
		}
	}

	private Logger getLogger()
	{
		return this.networkPlugin.getLogger();
	}

	private Option getOrCreateOption(OptionSection section, String optionName, ConfigurationSection optionYml)
	{
		Option option = section.getOption(optionName);
		String description = optionYml.getString("description");
		if (option == null)
		{
			this.getLogger().info("Creating option " + optionName + " in section " + section.getName());
			option = section.createOption(
				new OptionPojo(0, section.getSectionId(), optionName, OptionType.valueOf(optionYml.getString("type")), description));
		}
		if (!StringUtils.equals(option.getDescription(), description))
		{
			this.getLogger()
				.info("Changing the description of option " + optionName + " in section " + section.getName() + " to " + description);
			option.changeDescription(description);
		}
		return option;
	}

	private OptionSection getOrCreateOptionSection(String sectionName, ConfigurationSection sectionYml)
	{
		OptionSection section = this.networkPlugin.getOptionSection(sectionName);
		String description = sectionYml.getString("description");
		if (section == null)
		{
			this.getLogger().info("Creating option section " + sectionName);
			section = this.networkPlugin.createOptionSection(sectionName, description);
		}
		if (!StringUtils.equals(section.getDescription(), description))
		{
			this.getLogger().info("Changing the description of option section " + sectionName + " to " + description);
			section.changeDescription(description);
		}
		return section;
	}
}
