package net.rieksen.networkcore.spigot.plugin;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.option.Option;
import net.rieksen.networkcore.core.option.OptionSection;
import net.rieksen.networkcore.core.option.OptionType;
import net.rieksen.networkcore.core.option.pojo.OptionPojo;
import net.rieksen.networkcore.core.option.pojo.OptionValuePojo;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.spigot.file.AutomaticServerConfig;

/**
 * Synchronizes the content of messages.yml and options.yml with the database.
 */
public class NetworkPluginSync implements Runnable
{

	public final NetworkCore	provider;
	private boolean				firstRun	= true;

	public NetworkPluginSync(NetworkCore provider)
	{
		this.provider = provider;
	}

	@Override
	public void run()
	{
		for (NetworkPlugin plugin : this.provider.getPluginModule().getPlugins())
		{
			this.syncMessages(plugin);
			this.syncOptions(plugin);
		}
		if (this.firstRun)
		{
			this.firstRun = false;
		}
	}

	boolean isOptionsSyncEnabled(NetworkPlugin networkPlugin)
	{
		OptionSection config = networkPlugin.getOrCreateSection("Config", null);
		Option option = config.getOrCreateOption(new OptionPojo(0, 0, "synchronize-options-file", OptionType.BOOLEAN,
			"Whether the options should be synchronized to the file 'networkcore-options' within the plugin folder"));
		option.createDefault(new OptionValuePojo(0, 0, null, 0, Boolean.toString(true)));
		return option.getBoolean(null);
	}

	private void syncMessages(NetworkPlugin plugin)
	{
		if (plugin.getMessageSections().isEmpty()) { return; }
		AutomaticServerConfig autoConfig = this.provider.getAutomaticConfig();
		NetworkPluginMessagesFile file = plugin.getMessagesFile();
		if (file == null) { return; }
		if (!file.getFile().exists())
		{
			file.export();
			autoConfig.setLastSync(plugin.getName(), "messages", System.currentTimeMillis());
			return;
		}
		long lastModified = file.getFile().lastModified();
		long lastSync = autoConfig.getLastSync(plugin.getName(), "messages");
		if (lastSync == 0)
		{
			file.export();
			autoConfig.setLastSync(plugin.getName(), "messages", System.currentTimeMillis());
			return;
		}
		if (lastModified > lastSync)
		{
			plugin.getLogger().info("Synchronizing database with messages.yml");
			file.synchronize();
			autoConfig.setLastSync(plugin.getName(), "messages", System.currentTimeMillis());
		} else if (this.firstRun)
		{
			file.export();
			autoConfig.setLastSync(plugin.getName(), "messages", System.currentTimeMillis());
			return;
		}
	}

	private void syncOptions(NetworkPlugin plugin)
	{
		if (plugin.getOptionSections().isEmpty()) { return; }
		AutomaticServerConfig autoConfig = this.provider.getAutomaticConfig();
		NetworkPluginOptionsFile file = plugin.getOptionsFile();
		if (file == null) { return; }
		if (!this.isOptionsSyncEnabled(plugin))
		{
			if (file.getFile() != null)
			{
				file.getFile().delete();
			}
			return;
		}
		if (!file.getFile().exists())
		{
			file.export();
			autoConfig.setLastSync(plugin.getName(), "options", System.currentTimeMillis());
			return;
		}
		long lastModified = file.getFile().lastModified();
		long lastSync = autoConfig.getLastSync(plugin.getName(), "options");
		if (lastSync == 0)
		{
			file.export();
			autoConfig.setLastSync(plugin.getName(), "options", System.currentTimeMillis());
			return;
		}
		if (lastModified > lastSync)
		{
			plugin.getLogger().info("Synchronizing database with options.yml");
			file.synchronize();
			autoConfig.setLastSync(plugin.getName(), "options", System.currentTimeMillis());
		} else if (this.firstRun)
		{
			file.export();
			autoConfig.setLastSync(plugin.getName(), "options", System.currentTimeMillis());
			return;
		}
	}
}
