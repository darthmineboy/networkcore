package net.rieksen.networkcore.spigot.plugin;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import net.rieksen.networkcore.core.NetworkCoreAPI;
import net.rieksen.networkcore.core.message.Language;
import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.message.MessageSection;
import net.rieksen.networkcore.core.message.MessageTranslation;
import net.rieksen.networkcore.core.message.MessageType;
import net.rieksen.networkcore.core.message.MessageVariable;
import net.rieksen.networkcore.core.message.pojo.LanguagePojo;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.spigot.file.BasicYAML;

public class NetworkPluginMessagesFile extends BasicYAML<Plugin>
{

	private NetworkPlugin networkPlugin;

	public NetworkPluginMessagesFile(Plugin plugin, NetworkPlugin networkPlugin)
	{
		super(plugin, plugin.getDataFolder(), "networkcore-messages.yml");
		this.networkPlugin = networkPlugin;
	}

	public void export()
	{
		this.yaml = new YamlConfiguration();
		this.networkPlugin.getMessageSections().forEach(section ->
		{
			ConfigurationSection messageSection = this.yaml.createSection(section.getName());
			messageSection.set("description", section.getDescription());
			ConfigurationSection messages = messageSection.createSection("messages");
			section.getMessages().forEach(messagePojo ->
			{
				ConfigurationSection message = messages.createSection(messagePojo.getName());
				message.set("description", messagePojo.getDescription());
				// Language language = messagePojo.getLanguage();
				// message.set("default-language", language == null ? null :
				// language.getName());
				ConfigurationSection variables = message.createSection("variables");
				messagePojo.getVariables().forEach(variablePojo ->
				{
					variables.set(variablePojo.getName(), variablePojo.getDescription());
				});
				ConfigurationSection translations = message.createSection("translations");
				messagePojo.getTranslations().forEach(translationPojo ->
				{
					ConfigurationSection translation = translations.createSection(translationPojo.getLanguage().getName());
					translation.set("type", translationPojo.getMessageType().name());
					translation.set("message", translationPojo.getMessage());
				});
			});
		});
		this.save();
	}

	public void synchronize()
	{
		if (!this.load())
		{
			this.getLogger().severe("Failed to load file " + this.getFile().getPath());
			Exception e = this.exception;
			if (e instanceof InvalidConfigurationException)
			{
				this.getLogger().log(Level.SEVERE, "Invalid yaml! Creating backup and resetting...", e);
				this.copy(
					new File(this.file.getParentFile(), String.format("bk-%s-%s.yml", this.getFile().getName(), new Date().toString())));
				this.export();
			} else
			{
				return;
			}
		}
		Set<String> sectionNames = this.yaml.getKeys(false);
		// Delete sections that are not specified in the file
		new ArrayList<>(this.networkPlugin.getMessageSections()).stream().filter(section -> !sectionNames.contains(section.getName()))
			.forEach(section ->
			{
				this.getLogger().info("Deleting message section " + section.getName());
				this.networkPlugin.deleteMessageSection(section.getName());
			});
		for (String sectionName : sectionNames)
		{
			ConfigurationSection sectionYml = this.yaml.getConfigurationSection(sectionName);
			MessageSection section = this.getOrCreateMessageSection(sectionName, sectionYml);
			Set<String> messageNames = sectionYml.getConfigurationSection("messages").getKeys(false);
			new ArrayList<>(section.getMessages()).stream().filter(message -> !messageNames.contains(message.getName())).forEach(message ->
			{
				this.getLogger().info("Deleting message " + message.getName() + " from section " + section.getName());
				section.deleteMessage(message.getName());
			});
			for (String messageName : messageNames)
			{
				ConfigurationSection messageYml = sectionYml.getConfigurationSection("messages." + messageName);
				Message message = this.getOrCreateMessage(section, messageName, messageYml);
				Set<String> translationNames = messageYml.getConfigurationSection("translations").getKeys(false);
				new ArrayList<>(message.getTranslations()).stream()
					.filter(translation -> !translationNames.contains(translation.getLanguage().getName())).forEach(translation ->
					{
						this.getLogger().info("Deleting translation " + translation.getLanguage().getName() + " of message " + messageName
							+ " in section " + section.getName());
						message.deleteTranslation(translation.getLanguageId());
					});
				for (String translationName : translationNames)
				{
					Language language = this.getOrCreateLanguage(translationName);
					MessageTranslation translation = message.getTranslation(language.getLanguageId());
					MessageType type = MessageType.REGULAR;
					String typeString = messageYml.getString("translations." + translationName + ".type");
					try
					{
						type = MessageType.valueOf(typeString);
					} catch (IllegalArgumentException e)
					{
						this.getLogger().warning(
							String.format("Invalid MessageType '%s', using default '%s' instead", typeString, MessageType.REGULAR.name()));
					}
					String translatedMessage = messageYml.getString("translations." + translationName + ".message");
					if (translation == null)
					{
						this.getLogger().info("Creating the translation " + translationName + " of message " + messageName + " in section "
							+ section.getName());
						translation = message.createTranslation(language.getLanguageId(), translatedMessage, type);
					}
					if (!translation.getMessage().equals(translatedMessage))
					{
						this.getLogger().info("Changing the translation " + translationName + " of message " + messageName + " in section "
							+ section.getName() + " to " + translatedMessage);
						translation.changeMessage(translatedMessage);
					}
					if (translation.getMessageType() != type)
					{
						this.getLogger().info(String.format("Changing the type of translation %s of message %s in section %s to %s",
							translationName, messageName, section.getName(), type.name()));
						translation.changeMessageType(type);
					}
				}
				Set<String> variableNames = messageYml.getConfigurationSection("variables").getKeys(false);
				new ArrayList<>(message.getVariables()).stream().filter(variable -> !variableNames.contains(variable.getName()))
					.forEach(variable ->
					{
						this.getLogger().info(
							"Deleting variable " + variable.getName() + " of message " + messageName + " in section " + section.getName());
						message.deleteVariable(variable.getName());
					});
				for (String variableName : variableNames)
				{
					MessageVariable variable = message.getVariable(variableName);
					String description = messageYml.getString("variables." + variableName);
					if (variable == null)
					{
						this.getLogger().info(
							"Creating the variable " + variableName + " of message " + messageName + " in section " + section.getName());
						variable = message.createVariable(variableName, description);
					}
					if (!StringUtils.equals(variable.getDescription(), description))
					{
						this.getLogger().info("Changing the description of variable " + variableName + " of message " + messageName
							+ " in section " + section.getName() + " to " + description);
						variable.changeDescription(description);
					}
				}
			}
		}
	}

	private Logger getLogger()
	{
		return this.networkPlugin.getLogger();
	}

	private Language getOrCreateLanguage(String languageName)
	{
		Language language = NetworkCoreAPI.getMessageModule().getLanguage(languageName);
		if (language == null)
		{
			language = NetworkCoreAPI.getMessageModule().createLanguage(new LanguagePojo(0, languageName));
		}
		return language;
	}

	private Message getOrCreateMessage(MessageSection section, String messageName, ConfigurationSection messageYml)
	{
		Message message = section.getMessage(messageName);
		String description = messageYml.getString("description");
		if (message == null)
		{
			this.getLogger().info("Creating message " + messageName + " in section " + section.getName());
			message = section.createMessage(messageName, description);
		}
		if (!StringUtils.equals(message.getDescription(), description))
		{
			this.getLogger()
				.info("Changing the description of message " + messageName + " in section " + section.getName() + " to " + description);
			message.changeDescription(description);
		}
		return message;
	}

	private MessageSection getOrCreateMessageSection(String sectionName, ConfigurationSection sectionYml)
	{
		MessageSection section = this.networkPlugin.getMessageSection(sectionName);
		String description = sectionYml.getString("description");
		if (section == null)
		{
			this.getLogger().info("Creating message section " + sectionName);
			section = this.networkPlugin.createMessageSection(sectionName, description);
		}
		if (!StringUtils.equals(section.getDescription(), description))
		{
			this.getLogger().info("Changing the description of message section " + sectionName + " to " + description);
			section.changeDescription(description);
		}
		return section;
	}
}
