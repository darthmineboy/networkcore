package net.rieksen.networkcore.spigot.runnable;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import org.apache.commons.lang.Validate;
import org.bukkit.scheduler.BukkitRunnable;

import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.core.plugin.PluginModule;
import net.rieksen.networkcore.core.plugin.pojo.PluginTaskPojo;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;

public class PluginTaskRunnable extends BukkitRunnable
{

	private NetworkCoreSpigot provider;

	public PluginTaskRunnable(NetworkCoreSpigot provider)
	{
		Validate.notNull(provider);
		this.provider = provider;
	}

	@Override
	public void run()
	{
		try
		{
			List<PluginTaskPojo> tasks =
				this.provider.getDAO().getPluginDAO().findPendingPluginTasks(this.provider.getServerModule().getLocalServer().getServerId(),
					new Date(System.currentTimeMillis() + PluginModule.PLUGIN_TASK_CLAIM_MS));
			tasks.forEach(task ->
			{
				NetworkPlugin plugin = this.provider.getPluginModule().getPlugin(task.getPluginId());
				plugin.runTask(task);
			});
		} catch (Exception e)
		{
			this.provider.getLogger().log(Level.SEVERE, "An error occurred while executing plugin tasks", e);
		}
	}
}
