package net.rieksen.networkcore.spigot.runnable;

import java.sql.Date;

import org.bukkit.scheduler.BukkitRunnable;

import net.rieksen.networkcore.core.server.ServerRuntime;
import net.rieksen.networkcore.core.server.Server;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;

public class ServerRuntimeRunnable extends BukkitRunnable
{

	private final long	DELAY;
	private final long	PERIOD;

	public ServerRuntimeRunnable(NetworkCoreSpigot plugin)
	{
		this.DELAY = 20 * 10L;
		this.PERIOD = 20 * 10L;
		this.runTaskTimerAsynchronously(plugin, this.DELAY, this.PERIOD);
	}

	@Override
	public void run()
	{
		Server server = Server.getLocalServer();
		ServerRuntime runtime = server.findRuntime();
		runtime.changeLastPingDate(new Date(System.currentTimeMillis()));
	}
}
