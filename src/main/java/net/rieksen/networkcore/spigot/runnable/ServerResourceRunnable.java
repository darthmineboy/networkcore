package net.rieksen.networkcore.spigot.runnable;

import java.lang.management.ManagementFactory;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import net.rieksen.networkcore.web.NetworkCoreWebLink;
import org.bukkit.scheduler.BukkitRunnable;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import net.rieksen.networkcore.core.dao.DAOException;
import net.rieksen.networkcore.core.server.Server;
import net.rieksen.networkcore.core.server.pojo.ServerResourceUsagePojo;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;

public class ServerResourceRunnable extends BukkitRunnable
{

	private static final int		LOG_SERVER_RESOURCE_USAGE_TICKS	= 5 * 20;
	private static final int		CPU_TICKS						= 100;
	private static final int		MAX_TICKS						= 20 * 5;
	private static final long[]		ticks							= new long[ServerResourceRunnable.MAX_TICKS];
	/**
	 * To avoid spam we don't log every integrity exception. The exception is only logged every X milliseconds, where X is specified by this property.
	 */
	private static final long 		LOG_INTEGRITY_EXCEPTION_TIMEOUT = 60000;
	private static final int MYSQL_ERROR_CODE_DUPLICATE_ENTRY = 1062;
	private final NetworkCoreSpigot	plugin;
	private int						tickCount						= 0;
	private int						index							= -1;
	private long					lastCPUTime						= -1;
	private float					tps;
	private float					serverCpuUsage;
	private float					nodeCpuUsage;
	private long lastIntegrityException = -1;

	public ServerResourceRunnable(NetworkCoreSpigot networkCore)
	{
		this.plugin = networkCore;
	}

	/**
	 * Get the current TPS, calculated over the last 100 ticks.
	 */
	public float calculateCurrentTPS()
	{
		if (this.tickCount < ServerResourceRunnable.MAX_TICKS) { return 20; }
		long timePassed = this.getLastTick() - this.getFirstTick();
		float timePerTick = timePassed / (ServerResourceRunnable.MAX_TICKS - 1f);
		float tps = 1000 / timePerTick;
		if (tps > 99) { return 99; }
		return tps;
	}

	@Override
	public void run()
	{
		this.tickCount++;
		this.index++;
		if (this.index >= ServerResourceRunnable.MAX_TICKS)
		{
			this.index = 0;
		}
		ServerResourceRunnable.ticks[this.index] = System.currentTimeMillis();
		this.tps = this.calculateCurrentTPS();
		if (this.tickCount % ServerResourceRunnable.CPU_TICKS == 0)
		{
			this.calculateNodeCpuUsage();
			this.calculateServerCpuUsage();
		}
		if (this.tickCount % ServerResourceRunnable.LOG_SERVER_RESOURCE_USAGE_TICKS == 0)
		{
			Runtime rt = Runtime.getRuntime();
			int memoryFree = (int) (rt.freeMemory() / 1024 / 1024);
			int memoryAvailable = (int) (rt.totalMemory() / 1024 / 1024);
			int memoryUsed = memoryAvailable - memoryFree;
			int memoryTotal = (int) (rt.maxMemory() / 1024 / 1024);
			ServerResourceUsagePojo usage = new ServerResourceUsagePojo(Server.getLocalServer().getServerId(), new Date(), this.tps,
				this.nodeCpuUsage, this.serverCpuUsage, memoryUsed, memoryAvailable, memoryTotal);
			if (this.plugin.getNetworkPlugin().getOption("Config", "server-resource-usage-logging")
				.getBoolean(this.plugin.getServerModule().getLocalServer().getServerId()))
			{
				new BukkitRunnable()
				{

					@Override
					public void run()
					{
						final Server server = Server.getLocalServer();
						try
						{
							server.createResourceUsage(usage);
						} catch (DAOException e)
						{
							ServerResourceRunnable.this.handleError(e, server, usage);
						}
					}
				}.runTaskAsynchronously(this.plugin);
			}
		}
	}

	private void calculateNodeCpuUsage()
	{
		double loadAverage = ManagementFactory.getOperatingSystemMXBean().getSystemLoadAverage();
		int cores = Runtime.getRuntime().availableProcessors();
		this.nodeCpuUsage = (float) (loadAverage * 100 / cores);
	}

	private void calculateServerCpuUsage()
	{
		long currentCPUTime = ManagementFactory.getThreadMXBean().getCurrentThreadCpuTime();
		if (this.lastCPUTime == -1)
		{
			this.lastCPUTime = currentCPUTime;
			return;
		}
		double cpuTimeDifference = currentCPUTime - this.lastCPUTime;
		this.serverCpuUsage = (float) (cpuTimeDifference / TimeUnit.MILLISECONDS.toNanos(ServerResourceRunnable.CPU_TICKS * 50) * 100);
		// DecimalFormat df = new DecimalFormat("#0.00");
		// NetworkCoreAPI.debug("Server CPU: " + df.format(cpuUsage) + "%");
		this.lastCPUTime = currentCPUTime;
	}

	private long getFirstTick()
	{
		int i = this.index;
		i++;
		if (i >= ServerResourceRunnable.MAX_TICKS)
		{
			i = 0;
		}
		return ServerResourceRunnable.ticks[i];
	}

	private long getLastTick()
	{
		return ServerResourceRunnable.ticks[this.index];
	}

	private void handleError(DAOException e, Server server,  ServerResourceUsagePojo usage)
	{
		if (e.getCause() instanceof MySQLIntegrityConstraintViolationException)
		{
			if(((MySQLIntegrityConstraintViolationException) e.getCause()).getErrorCode() == MYSQL_ERROR_CODE_DUPLICATE_ENTRY) {
				final long time = System.currentTimeMillis();
				if (time - lastIntegrityException > LOG_INTEGRITY_EXCEPTION_TIMEOUT) {
					lastIntegrityException = time;
					plugin.getLogger().log(Level.WARNING, String.format("Failed to save server resource usage to database due to duplicate entry for server #%s (%S) - %s. This is likely caused by two servers running with the same server id (configured in './plugins/NetworkCore/automatic.yml'). For more information see %s", usage.getServerId(), server.getName(),usage.getCurrentDate(),NetworkCoreWebLink.TOPIC_UNIQUE_SERVER_ID));
				}
				return;
			}
		}
		plugin.getLogger().log(Level.SEVERE, "Failed to save server resource usage to database", e);
	}
}
