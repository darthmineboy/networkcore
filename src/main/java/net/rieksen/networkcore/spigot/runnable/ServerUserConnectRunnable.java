package net.rieksen.networkcore.spigot.runnable;

import java.util.Date;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.user.UserConnect;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;

/**
 * Fixes {@link UserConnect} which are still marked as online, while the
 * {@link User} is already offline on this server.
 */
public class ServerUserConnectRunnable extends BukkitRunnable
{

	private NetworkCoreSpigot plugin;

	public ServerUserConnectRunnable(NetworkCoreSpigot plugin)
	{
		this.plugin = plugin;
	}

	@Override
	public void run()
	{
		List<UserConnect> connects = this.plugin.getServerModule().getLocalServer().findOpenConnects();
		connects.forEach(connect ->
		{
			User user = connect.getUser();
			Player player = user.getPlayer();
			if (player == null)
			{
				connect.changeQuitDate(new Date());
			}
		});
	}
}
