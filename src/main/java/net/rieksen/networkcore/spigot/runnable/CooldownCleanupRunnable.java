package net.rieksen.networkcore.spigot.runnable;

import org.bukkit.scheduler.BukkitRunnable;

import net.rieksen.networkcore.core.NetworkCore;

public class CooldownCleanupRunnable extends BukkitRunnable
{

	private final NetworkCore plugin;

	public CooldownCleanupRunnable(NetworkCore plugin)
	{
		this.plugin = plugin;
	}

	@Override
	public void run()
	{
		this.plugin.getUserModule().deleteExpiredCooldowns();
	}
}
