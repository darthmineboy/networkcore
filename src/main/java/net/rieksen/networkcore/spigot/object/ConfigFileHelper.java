package net.rieksen.networkcore.spigot.object;

import java.util.logging.Logger;

import org.bukkit.configuration.ConfigurationSection;

import com.zaxxer.hikari.pool.PoolInitializationException;

import net.rieksen.networkcore.core.dao.DAOManager;
import net.rieksen.networkcore.core.dao.mysql.MySQLDAOManager;
import net.rieksen.networkcore.core.util.MySQL;
import net.rieksen.networkcore.core.util.MySQLSetting;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.file.BasicYAML;

public class ConfigFileHelper extends BasicYAML<NetworkCoreSpigot>
{

	private final double	LATEST_CONFIG_VERSION	= 1.0;
	private Logger			log;
	private double			configVersion;

	/**
	 * @param plugin
	 */
	public ConfigFileHelper(NetworkCoreSpigot plugin)
	{
		super(plugin, plugin.getDataFolder(), "config.yml");
		this.log = plugin.getLogger();
	}

	/**
	 * Sets up the config file, updates the config file, initializes the
	 * datasource and initializes the server.
	 * 
	 * @return true when successful
	 */
	public boolean init()
	{
		if (!this.load())
		{
			this.log.severe("Failed to load config.yml");
			return false;
		}
		this.configure();
		if (!this.addDefaults() || !this.loadVersion() || !this.update() || !this.initializeDataSource()) { return false; }
		return true;
	}

	private boolean addDefaults()
	{
		this.yaml.addDefault("config-version", 1.0);
		this.yaml.addDefault("settings.backend", "mysql");
		this.yaml.addDefault("settings.backends.mysql.host", "127.0.0.1");
		this.yaml.addDefault("settings.backends.mysql.database", "networkcore");
		this.yaml.addDefault("settings.backends.mysql.username", "networkcore");
		this.yaml.addDefault("settings.backends.mysql.password", "networkcore");
		if (!this.save())
		{
			this.log.severe("Failed to save config.yml");
			return false;
		}
		return true;
	}

	private void configure()
	{
		this.yaml.options().header(
			"NetworkCore\nAuthor: Darthmineboy\nSpigotMC: https://www.spigotmc.org/resources/networkcore.19456/\nWebsite: https://wwww.networkcore.org");
		this.yaml.options().copyDefaults(true);
		this.yaml.options().copyHeader(true);
	}

	private boolean initializeDataSource()
	{
		this.log.info("Initializing datasource...");
		ConfigurationSection csSettings = this.yaml.getConfigurationSection("settings");
		String backend = csSettings.getString("backend");
		ConfigurationSection csBackends = csSettings.getConfigurationSection("backends");
		ConfigurationSection csBackend = csBackends.getConfigurationSection(backend);
		if (csBackend == null)
		{
			this.log.severe("Backend not configured in config.yml");
			return false;
		}
		if ("mysql".equals(backend))
		{
			String host = csBackend.getString("host");
			String database = csBackend.getString("database");
			String username = csBackend.getString("username");
			String password = csBackend.getString("password");
			MySQLSetting mySQLSetting = new MySQLSetting(host, database, username, password);
			if (csBackend.isSet("port"))
			{
				mySQLSetting.setPort(csBackend.getInt("port"));
			}
			if (csBackend.isSet("minimum-idle"))
			{
				mySQLSetting.setMinimumIdle(csBackend.getInt("minimum-idle"));
			}
			if (csBackend.isSet("maximum-pool-size"))
			{
				mySQLSetting.setMaximumPoolSize(csBackend.getInt("maximum-pool-size"));
			}
			if (csBackend.isSet("connection-timeout"))
			{
				mySQLSetting.setConnectionTimeout(csBackend.getLong("connection-timeout"));
			}
			if (csBackend.isSet("idle-timeout"))
			{
				mySQLSetting.setIdleTimeout(csBackend.getLong("idle-timeout"));
			}
			if (csBackend.isSet("test-query"))
			{
				mySQLSetting.setTestQuery(csBackend.getString("test-query"));
			}
			DAOManager dao;
			try
			{
				MySQL mysql = new MySQL(mySQLSetting);
				dao = new MySQLDAOManager(this.plugin, mysql, true);
			} catch (PoolInitializationException e)
			{
				e.printStackTrace();
				this.log.severe(
					"Failed to establish MySQL database connection! Please configure './plugins/NetworkCore/config.yml' with your MySQL database credentials.");
				return false;
			}
			this.plugin.setDAOManager(dao);
		} else
		{
			this.log.severe("Unsupported backend " + backend);
			return false;
		}
		this.log.info("Successfully prepared datastore!");
		return true;
	}

	/**
	 * @return
	 */
	private boolean loadVersion()
	{
		if (this.yaml.isDouble("config-version"))
		{
			this.configVersion = this.yaml.getDouble("config-version");
			return true;
		}
		this.log.severe("config-version is not a double in config.yml");
		this.log.severe("Deleting config.yml...");
		this.file.delete();
		this.log.severe("Deleted config.yml!");
		return this.init();
	}

	private boolean update()
	{
		if (this.configVersion == this.LATEST_CONFIG_VERSION)
		{
			this.log.info("Config.yml is up-to-date!");
			return true;
		}
		this.log.severe("Illegal config-version!");
		this.log.severe("Deleting config.yml...");
		this.file.delete();
		this.log.severe("Deleted config.yml!");
		return this.init();
	}
}
