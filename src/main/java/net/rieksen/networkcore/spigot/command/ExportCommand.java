package net.rieksen.networkcore.spigot.command;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import net.rieksen.networkcore.core.plugin.converter.IPluginDataConverter;
import net.rieksen.networkcore.core.plugin.converter.PluginDataConverterOptions;
import net.rieksen.networkcore.core.plugin.converter.yaml.YamlPluginDataConverter;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.util.NetworkCorePermissions;

public class ExportCommand extends NetworkCoreCommand
{

	public ExportCommand(NetworkCoreSpigot plugin)
	{
		super(plugin, "export", NetworkCorePermissions.NETWORKCORE_EXPORT.toString());
	}

	@Override
	public void doCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		sender.sendMessage(this.color("&7Exporting plugin data..."));
		this.provider.getServer().getScheduler().runTaskAsynchronously(this.provider, () ->
		{
			IPluginDataConverter converter = new YamlPluginDataConverter(this.provider);
			File exportDirectory = new File(this.provider.getDataFolder(), "export");
			exportDirectory.mkdirs();
			for (File file : exportDirectory.listFiles())
			{
				file.delete();
			}
			this.provider.getPluginModule().getPlugins().forEach(networkPlugin ->
			{
				if (networkPlugin.getMessageSections().isEmpty() && networkPlugin.getOptionSections().isEmpty()) { return; }
				sender.sendMessage(this.color("&7Exporting plugin &e" + networkPlugin.getName()));
				try
				{
					Map<PluginDataConverterOptions, String> options = new HashMap<>();
					options.put(PluginDataConverterOptions.UPDATE_MESSAGE_TRANSLATION, "true");
					options.put(PluginDataConverterOptions.UPDATE_OPTION_VALUE, "true");
					converter.exportToFile(networkPlugin, new File(exportDirectory, networkPlugin.getName() + ".yml"), options);
				} catch (IOException e)
				{
					sender.sendMessage(this.color("&cFailed to export plugin &6" + networkPlugin.getName()));
				}
			});
			sender.sendMessage(this.color("&7Completed export, files are stored at &e" + exportDirectory.getPath()));
		});
	}

	@Override
	public List<String> doTabComplete(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		return null;
	}
}
