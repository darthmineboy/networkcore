package net.rieksen.networkcore.spigot.command;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.SimpleCommandMap;

import com.google.common.collect.Lists;

import net.rieksen.networkcore.core.NetworkCoreAPI;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.spigot.util.ReflectionUtil;

public abstract class NetworkCommand
{

	protected String				commandName;
	protected String				permission;
	protected List<NetworkCommand>	subCommands	= new ArrayList<>();
	protected List<String>			aliases		= new ArrayList<>();

	public NetworkCommand(String commandName)
	{
		Validate.notNull(commandName);
		this.commandName = commandName;
	}

	public NetworkCommand(String commandName, String permission)
	{
		this(commandName);
		this.permission = permission;
	}

	public void addAlias(String alias)
	{
		this.aliases.add(alias);
	}

	public final void addSubCommand(NetworkCommand subCommand)
	{
		this.subCommands.add(subCommand);
	}

	public final List<NetworkCommand> filterPermission(List<NetworkCommand> list, CommandSender sender)
	{
		Iterator<NetworkCommand> it = list.iterator();
		while (it.hasNext())
		{
			NetworkCommand command = it.next();
			if (command.getPermission() == null)
			{
				continue;
			}
			if (sender.hasPermission(command.getPermission()))
			{
				continue;
			}
			it.remove();
		}
		return list;
	}

	public final String getCommandName()
	{
		return this.commandName;
	}

	public final List<String> getCommandNames(List<NetworkCommand> commands)
	{
		List<String> list = Lists.newArrayList();
		for (NetworkCommand command : commands)
		{
			list.add(command.commandName);
		}
		return list;
	}

	public String getPermission()
	{
		return this.permission;
	}

	public final List<NetworkCommand> getSubCommands()
	{
		return this.subCommands;
	}

	public final List<NetworkCommand> getSubCommandsBeginningWith(String startsWith)
	{
		List<NetworkCommand> list = Lists.newArrayList();
		for (NetworkCommand command : this.subCommands)
		{
			if (command.commandName.toLowerCase().startsWith(startsWith.toLowerCase()))
			{
				list.add(command);
			}
		}
		return list;
	}

	/**
	 * @deprecated better alternative
	 *             {@link #getSubCommandsBeginningWith(String)}
	 */
	@Deprecated
	public final List<String> getSubCommandsStartingWith(String arg)
	{
		List<String> list = Lists.newArrayList();
		for (NetworkCommand subCommand : this.subCommands)
		{
			if (subCommand.commandName.toLowerCase().startsWith(arg.toLowerCase()))
			{
				list.add(subCommand.commandName);
			}
		}
		return list;
	}

	public User getUser(CommandSender sender)
	{
		return User.getUser(sender);
	}

	/**
	 * Checks whether the sender has the permission for this command.
	 * 
	 * @param sender
	 * @return true if there is no permission
	 */
	public boolean hasPermission(CommandSender sender)
	{
		if (this.permission == null) { return true; }
		return sender.hasPermission(this.permission);
	}

	public abstract boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args);

	public abstract List<String> onTabComplete(CommandSender sender, Command cmd, String commandLabel, String[] args);

	/**
	 * Register a command in the command map.
	 * 
	 * @param commandName
	 * @param fallbackPrefix
	 *            plugin name is standard, e.g. /<b>networkcore</b>:ncore
	 * @return true when successful
	 */
	public boolean registerCommand(String commandName, String fallbackPrefix)
	{
		return this.registerCommand(commandName, fallbackPrefix, false);
	}

	/**
	 * Register a command in the command map.
	 * 
	 * @param commandName
	 * @param fallbackPrefix
	 *            plugin name is standard, e.g. /<b>networkcore</b>:ncore
	 * @return true when successful
	 */
	public boolean registerCommand(String commandName, String fallbackPrefix, boolean force)
	{
		Server server = Bukkit.getServer();
		NetworkCommandBridge commandBridge = new NetworkCommandBridge(this);
		Command command = new Command(commandName)
		{

			private NetworkCommandBridge commandBridge;

			@Override
			public boolean execute(CommandSender sender, String commandLabel, String[] args)
			{
				return this.commandBridge.onCommand(sender, this, commandLabel, args);
			}

			public Command init(NetworkCommandBridge commandBridge)
			{
				this.commandBridge = commandBridge;
				return this;
			}

			@Override
			public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException
			{
				return this.commandBridge.onTabComplete(sender, this, alias, args);
			}
		}.init(commandBridge);
		command.setAliases(this.aliases);
		try
		{
			Class<?> craftServer = ReflectionUtil.getCraftClass("CraftServer");
			Method getCommandMapMethod = craftServer.getMethod("getCommandMap");
			SimpleCommandMap commandMap = (SimpleCommandMap) getCommandMapMethod.invoke(server);
			Command removedCommand = null;
			if (force)
			{
				removedCommand = commandMap.getCommand(command.getName());
				// Remove conflicting commands
				Map<String, Command> knownCommands = this.getKnownCommands(commandMap);
				if (removedCommand != knownCommands.remove(command.getName().toLowerCase(java.util.Locale.ENGLISH)))
				{
					NetworkCoreAPI.getLogger().warning("Expected different command to be removed");
				}
			}
			if (!commandMap.register(fallbackPrefix, command))
			{
				NetworkCoreAPI.getLogger()
					.info(String.format(
						"Registered command %s with fallback prefix %s because another plugin has already registered a command with the same name",
						command.getName(), fallbackPrefix));
			}
			if (removedCommand != null)
			{
				String fallbackPrefixRemove = null;
				if (removedCommand instanceof PluginCommand)
				{
					fallbackPrefixRemove = ((PluginCommand) removedCommand).getPlugin().getName();
				}
				commandMap.register(fallbackPrefixRemove, removedCommand);
			}
		} catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	private Map<String, Command> getKnownCommands(SimpleCommandMap map) throws IllegalArgumentException, IllegalAccessException,
		NoSuchFieldException, SecurityException, InvocationTargetException, NoSuchMethodException
	{
		try
		{
			Class<?> craftCommandMapClass = ReflectionUtil.getCraftClass("command.CraftCommandMap");
			return (Map<String, Command>) craftCommandMapClass.getMethod("getKnownCommands").invoke(map);
		} catch (ClassNotFoundException e)
		{
			Field field = map.getClass().getDeclaredField("knownCommands");
			field.setAccessible(true);
			return (Map<String, Command>) field.get(map);
		}
	}
}
