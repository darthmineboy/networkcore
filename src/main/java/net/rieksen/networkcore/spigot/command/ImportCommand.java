package net.rieksen.networkcore.spigot.command;

import java.io.File;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.core.plugin.converter.IPluginDataConverter;
import net.rieksen.networkcore.core.plugin.converter.yaml.YamlPluginDataConverter;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.util.NetworkCorePermissions;

public class ImportCommand extends NetworkCoreCommand
{

	public ImportCommand(NetworkCoreSpigot plugin)
	{
		super(plugin, "import", NetworkCorePermissions.NETWORKCORE_IMPORT.toString());
	}

	@Override
	public void doCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		sender.sendMessage(this.color("&7Importing plugin data..."));
		this.provider.getServer().getScheduler().runTaskAsynchronously(this.provider, () ->
		{
			IPluginDataConverter converter = new YamlPluginDataConverter(this.provider);
			File exportDirectory = new File(this.provider.getDataFolder(), "export");
			exportDirectory.mkdirs();
			for (File file : exportDirectory.listFiles())
			{
				sender.sendMessage(this.color("&7Importing file &e" + file.getName()));
				String pluginName = file.getName().split("\\.")[0];
				NetworkPlugin networkPlugin = this.provider.getPluginModule().getPlugin(pluginName);
				if (networkPlugin == null)
				{
					sender.sendMessage(this.color("&cPlugin &6" + pluginName + " not found; skipping file"));
					continue;
				}
				converter.importFromFile(networkPlugin, file, null);
			}
			sender.sendMessage(this.color("&7Completed import"));
		});
	}

	@Override
	public List<String> doTabComplete(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		return null;
	}
}
