package net.rieksen.networkcore.spigot.command;

import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.message.Language;
import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.user.User;

public class AdminLanguageListCommand extends NetworkCommand
{

	private final NetworkCore plugin;

	public AdminLanguageListCommand(NetworkCore plugin)
	{
		super("list");
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		User user = sender instanceof Player ? User.getUser((Player) sender) : null;
		if (!sender.hasPermission("ncore.cmd.ncore.language.list"))
		{
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
				Message.getMessage("NetworkCore", "General", "no-permission").getMessage(user)));
			return true;
		}
		for (Language language : this.plugin.getMessageModule().getLanguages())
		{
			sender.sendMessage("#" + language.getLanguageId() + " " + language.getName());
		}
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		return null;
	}
}
