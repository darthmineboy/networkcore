package net.rieksen.networkcore.spigot.command.note;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import net.rieksen.networkcore.core.user.note.UserNote;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.command.NetworkCoreCommand;

public class NoteDeleteCommand extends NetworkCoreCommand
{

	// Permissions
	private static final String	DELETE_PERMISSION			= "networkcore.note.delete";
	private static final String	DELETE_ANY_NOTE_PERMISSION	= "networkcore.note.delete.any";
	// Messages
	private static final String	MESSAGE_SECTION				= "User Notes";
	private static final String	DELETE_USAGE_MESSAGE		= "note-delete-usage";
	private static final String	ARGUMENT_NOT_NUMBER_MESSAGE	= "argument-not-number";
	private static final String	NOTE_NOT_FOUND_MESSAGE		= "note-not-found";
	private static final String	DELETE_ONLY_OWN_MESSAGE		= "note-delete-only-own";
	private static final String	NOTE_DELETED_MESSAGE		= "note-deleted";

	public NoteDeleteCommand(NetworkCoreSpigot plugin)
	{
		super(plugin, "delete", NoteDeleteCommand.DELETE_PERMISSION);
	}

	@Override
	public void doCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (args.length < 1)
		{
			this.getNetworkPlugin().getMessage(NoteDeleteCommand.MESSAGE_SECTION, NoteDeleteCommand.DELETE_USAGE_MESSAGE)
				.sendMessage(sender);
			return;
		}
		int noteId;
		try
		{
			noteId = Integer.parseInt(args[0]);
		} catch (NumberFormatException e)
		{
			Map<String, String> variables = new HashMap<>();
			variables.put("%input%", args[0]);
			this.getNetworkPlugin().getMessage(NoteDeleteCommand.MESSAGE_SECTION, NoteDeleteCommand.ARGUMENT_NOT_NUMBER_MESSAGE)
				.sendMessage(sender, variables);
			return;
		}
		this.provider.getTaskChainFactory().newChain().async(() ->
		{
			UserNote note = this.provider.getUserModule().findNote(noteId);
			if (note == null)
			{
				Map<String, String> variables = new HashMap<>();
				variables.put("%id%", String.valueOf(noteId));
				this.getNetworkPlugin().getMessage(NoteDeleteCommand.MESSAGE_SECTION, NoteDeleteCommand.NOTE_NOT_FOUND_MESSAGE)
					.sendMessage(sender, variables);
				return;
			}
			if (note.getCreatedBy().getUserId() != this.getUser(sender).getUserId())
			{
				if (!sender.hasPermission(NoteDeleteCommand.DELETE_ANY_NOTE_PERMISSION))
				{
					this.getNetworkPlugin().getMessage(NoteDeleteCommand.MESSAGE_SECTION, NoteDeleteCommand.DELETE_ONLY_OWN_MESSAGE)
						.sendMessage(sender);
					return;
				}
			}
			note.delete();
			Map<String, String> variables = new HashMap<>();
			variables.put("%id%", String.valueOf(note.getId()));
			this.getNetworkPlugin().getMessage(NoteDeleteCommand.MESSAGE_SECTION, NoteDeleteCommand.NOTE_DELETED_MESSAGE)
				.sendMessage(sender, variables);
		}).execute();
	}

	@Override
	public List<String> doTabComplete(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		return null;
	}
}
