package net.rieksen.networkcore.spigot.command;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.util.Action;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.chestmenu.ChestMenu;
import net.rieksen.networkcore.spigot.chestmenu.PluginManagementChestMenu;

public class PluginCommand extends NetworkCommand
{

	private final NetworkCoreSpigot plugin;

	public PluginCommand(NetworkCoreSpigot plugin)
	{
		super("plugin");
		this.plugin = plugin;
		this.permission = "ncore.cmd.ncore.plugin";
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (!(sender instanceof Player))
		{
			sender.sendMessage("Must be player!");
			return true;
		}
		final Player player = (Player) sender;
		User user = User.getUser(player);
		if (!sender.hasPermission(this.permission))
		{
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
				Message.getMessage("NetworkCore", "General", "no-permission").getMessage(user)));
			return true;
		}
		if (args.length < 1)
		{
			sender.sendMessage(
				ChatColor.translateAlternateColorCodes('&', Message.getMessage("NetworkCore", "Plugin Command", "usage").getMessage(user)));
			return true;
		}
		String pluginName = args[0];
		NetworkPlugin plugin = NetworkPlugin.getPlugin(pluginName);
		if (plugin == null)
		{
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
				Message.getMessage("NetworkCore", "Plugin Command", "plugin-not-found").getMessage(user).replace("%plugin%", pluginName)));
			return true;
		}
		ChestMenu chestMenu = new PluginManagementChestMenu(this.plugin, plugin.getPluginId(), player, user, new Action()
		{

			@Override
			public void onAction()
			{
				player.closeInventory();
			}
		});
		chestMenu.fill();
		this.plugin.getChestMenuContainer().openChestMenu(player, chestMenu);
		return false;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (!sender.hasPermission(this.permission)) { return null; }
		if (args.length == 1)
		{
			List<String> list = new ArrayList<>();
			for (NetworkPlugin plugin : this.plugin.getPluginModule().getPlugins())
			{
				if (!plugin.getName().toLowerCase().startsWith(args[0].toLowerCase()))
				{
					continue;
				}
				list.add(plugin.getName());
			}
			Collections.sort(list);
			return list;
		}
		return null;
	}
}
