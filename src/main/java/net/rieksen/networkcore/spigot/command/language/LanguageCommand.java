package net.rieksen.networkcore.spigot.command.language;

import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.chestmenu.ChestMenu;
import net.rieksen.networkcore.spigot.chestmenu.UserLanguageChestMenu;
import net.rieksen.networkcore.spigot.command.NetworkCoreCommand;

public class LanguageCommand extends NetworkCoreCommand
{

	public LanguageCommand(NetworkCoreSpigot plugin)
	{
		super(plugin, "language", "networkcore.language");
		this.addSubCommand(new LanguageSetCommand(plugin));
	}

	@Override
	public void doCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (!(sender instanceof Player))
		{
			sender.sendMessage("Not player!");
			return;
		}
		Player player = (Player) sender;
		ChestMenu chestMenu = new UserLanguageChestMenu(this.provider, player);
		chestMenu.fill();
		this.provider.getChestMenuContainer().openChestMenu(player, chestMenu);
		return;
	}

	@Override
	public List<String> doTabComplete(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (args.length == 1) { return this.getCommandNames(this.getSubCommandsBeginningWith(args[0])); }
		return null;
	}
}
