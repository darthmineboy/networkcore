package net.rieksen.networkcore.spigot.command;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import net.md_5.bungee.api.ChatColor;
import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.message.Language;
import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.user.User;

public class AdminLanguageDeleteCommand extends NetworkCommand
{

	private NetworkCore provider;

	public AdminLanguageDeleteCommand(NetworkCore provider)
	{
		super("delete", "ncore.cmd.ncore.language.delete");
		this.provider = provider;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		User user = User.getUser(sender);
		if (!sender.hasPermission(this.permission))
		{
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
				Message.getMessage("NetworkCore", "General", "no-permission").getMessage(user)));
			return true;
		}
		if (args.length < 1)
		{
			sender.sendMessage("Please specify a language to delete");
			return true;
		}
		String languageName = args[0];
		Language language = Language.getLanguage(languageName);
		if (language == null)
		{
			sender.sendMessage("Language not found: " + languageName);
			return true;
		}
		this.provider.getMessageModule().deleteLanguage(language.getLanguageId());
		sender.sendMessage("Deleted language " + languageName);
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (!sender.hasPermission(this.permission)) { return null; }
		if (args.length == 1)
		{
			List<String> list = new ArrayList<>();
			String languageName = args[0].toLowerCase();
			this.provider.getMessageModule().getLanguages().stream().filter(language ->
			{
				return language.getName().toLowerCase().startsWith(languageName);
			}).forEach(language -> list.add(language.getName()));
			Collections.sort(list);
			return list;
		}
		return null;
	}
}
