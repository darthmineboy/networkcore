package net.rieksen.networkcore.spigot.command.note;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import net.rieksen.networkcore.core.NetworkCoreAPI;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.user.UserType;
import net.rieksen.networkcore.core.user.note.UserNotePojo;
import net.rieksen.networkcore.core.user.pojo.UserPojo;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.command.NetworkCoreCommand;

public class NoteCreateCommand extends NetworkCoreCommand
{

	private final static String	MESSAGE_SECTION			= "User Notes";
	private final static String	USAGE_MESSAGE			= "note-create-usage";
	private final static String	USER_NOT_FOUND_MESSAGE	= "user-not-found";
	private final static String	NOTE_CREATED_MESSAGE	= "note-created";

	public NoteCreateCommand(NetworkCoreSpigot plugin)
	{
		super(plugin, "create", "networkcore.note.create");
	}

	@Override
	public void doCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (args.length < 2)
		{
			this.getNetworkPlugin().getMessage(NoteCreateCommand.MESSAGE_SECTION, NoteCreateCommand.USAGE_MESSAGE).sendMessage(sender);
			return;
		}
		User user = this.provider.getUserModule().getUser(UserType.MINECRAFT_PLAYER, args[0]);
		if (user == null)
		{
			Map<String, String> variables = new HashMap<>();
			variables.put("%name%", args[0]);
			this.getNetworkPlugin().getMessage(NoteCreateCommand.MESSAGE_SECTION, NoteCreateCommand.USER_NOT_FOUND_MESSAGE)
				.sendMessage(sender, variables);
			return;
		}
		String message = StringUtils.join(args, ' ', 1, args.length);
		this.provider.getTaskChainFactory().newChain().async(() ->
		{
			UserNotePojo pojo = new UserNotePojo(0, user.getUserId(), this.getUser(sender).getUserId(), new Date(), message);
			user.createNote(pojo);
			Map<String, String> variables = new HashMap<>();
			variables.put("%target%", user.getName());
			variables.put("%message%", message);
			this.getNetworkPlugin().getMessage(NoteCreateCommand.MESSAGE_SECTION, NoteCreateCommand.NOTE_CREATED_MESSAGE)
				.sendMessage(sender, variables);
		}).execute();
	}

	@Override
	public List<String> doTabComplete(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (args.length == 1) { return NetworkCoreAPI.getDAO().getUserDAO().findUsersByName(args[0], 0, 50).stream()
			.map(UserPojo::getUserName).collect(Collectors.toList()); }
		return null;
	}
}
