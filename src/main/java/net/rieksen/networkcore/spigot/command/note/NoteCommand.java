package net.rieksen.networkcore.spigot.command.note;

import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.command.NetworkCoreCommand;

public class NoteCommand extends NetworkCoreCommand
{

	private static final String	MESSAGE_SECTION	= "User Notes";
	private static final String	USAGE_MESSAGE	= "note-usage";

	public NoteCommand(NetworkCoreSpigot plugin)
	{
		super(plugin, "note", "networkcore.note");
		this.addSubCommand(new NoteCreateCommand(plugin));
		this.addSubCommand(new NoteListCommand(plugin));
		this.addSubCommand(new NoteDeleteCommand(plugin));
	}

	@Override
	public void doCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		this.getNetworkPlugin().getMessage(NoteCommand.MESSAGE_SECTION, NoteCommand.USAGE_MESSAGE).sendMessage(sender);
	}

	@Override
	public List<String> doTabComplete(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (args.length == 1) { return this.getCommandNames(this.filterPermission(this.getSubCommandsBeginningWith(args[0]), sender)); }
		return null;
	}
}
