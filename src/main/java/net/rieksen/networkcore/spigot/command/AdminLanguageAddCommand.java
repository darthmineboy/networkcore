package net.rieksen.networkcore.spigot.command;

import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.message.Language;
import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.message.pojo.LanguagePojo;
import net.rieksen.networkcore.core.user.User;

public class AdminLanguageAddCommand extends NetworkCommand
{

	private final NetworkCore plugin;

	public AdminLanguageAddCommand(NetworkCore plugin)
	{
		super("add");
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		User user = sender instanceof Player ? User.getUser((Player) sender) : null;
		if (!sender.hasPermission("ncore.cmd.ncore.language.add"))
		{
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
				Message.getMessage("NetworkCore", "General", "no-permission").getMessage(user)));
			return true;
		}
		if (args.length < 1)
		{
			sender.sendMessage("Please specify a language");
			return true;
		}
		String languageName = args[0];
		Language language = Language.getLanguage(languageName);
		if (language != null)
		{
			sender.sendMessage("A language named " + languageName + " already exists");
			return true;
		}
		language = this.plugin.getMessageModule().createLanguage(new LanguagePojo(0, languageName));
		sender.sendMessage("Successfully added language #" + language.getLanguageId() + " " + language.getName());
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		return null;
	}
}
