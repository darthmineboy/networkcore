package net.rieksen.networkcore.spigot.command;

import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.user.User;

public class AdminLanguageCommand extends NetworkCommand
{

	public AdminLanguageCommand(NetworkCore plugin)
	{
		super("language");
		this.addSubCommand(new AdminLanguageListCommand(plugin));
		this.addSubCommand(new AdminLanguageAddCommand(plugin));
		this.addSubCommand(new AdminLanguageDeleteCommand(plugin));
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		User user = sender instanceof Player ? User.getUser((Player) sender) : null;
		if (!sender.hasPermission("ncore.cmd.ncore.language"))
		{
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
				Message.getMessage("NetworkCore", "General", "no-permission").getMessage(user)));
			return true;
		}
		sender.sendMessage("/ncore language list");
		sender.sendMessage("/ncore language add <language>");
		sender.sendMessage("/ncore language delete <language>");
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (args.length == 1) { return this.getCommandNames(this.filterPermission(this.getSubCommandsBeginningWith(args[0]), sender)); }
		return null;
	}
}
