package net.rieksen.networkcore.spigot.command;

import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.chestmenu.ChestMenu;
import net.rieksen.networkcore.spigot.chestmenu.MenuChestMenu;

public class NCoreCommand extends NetworkCommand
{

	private final NetworkCoreSpigot plugin;

	public NCoreCommand(NetworkCoreSpigot plugin)
	{
		super("ncore");
		this.plugin = plugin;
		this.addSubCommand(new AdminLanguageCommand(plugin));
		this.addSubCommand(new PluginCommand(plugin));
		// this.addSubCommand(new MessageSectionCommand(plugin));
		this.addSubCommand(new ExportCommand(plugin));
		this.addSubCommand(new ImportCommand(plugin));
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (!(sender instanceof Player))
		{
			sender.sendMessage("Must be player!");
			return true;
		}
		Player player = (Player) sender;
		User user = User.getUser(player);
		if (!sender.hasPermission("ncore.cmd.ncore"))
		{
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
				Message.getMessage("NetworkCore", "General", "no-permission").getMessage(user)));
			return true;
		}
		ChestMenu chestMenu = new MenuChestMenu(this.plugin, player, user);
		chestMenu.fill();
		this.plugin.getChestMenuContainer().openChestMenu(player, chestMenu);
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (args.length == 1) { return this.getCommandNames(this.filterPermission(this.getSubCommandsBeginningWith(args[0]), sender)); }
		return null;
	}
}
