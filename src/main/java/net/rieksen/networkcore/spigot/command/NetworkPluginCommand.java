package net.rieksen.networkcore.spigot.command;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.core.user.User;

/**
 * Provides an easy way to check for permissions prior to executing the command.
 * The no permission message is taken by default from the section
 * <code>General</code> and message <code>no-permission</code>.
 */
public abstract class NetworkPluginCommand extends NetworkCommand
{

	protected String	messageSection	= "General";
	protected String	noPermission	= "no-permission";

	public NetworkPluginCommand(String commandName, String permission)
	{
		super(commandName, permission);
	}

	public String color(String message)
	{
		return ChatColor.translateAlternateColorCodes('&', message);
	}

	/**
	 * Runs after having checked the permission.
	 * 
	 * @param sender
	 * @param cmd
	 * @param commandLabel
	 * @param args
	 */
	public abstract void doCommand(CommandSender sender, Command cmd, String commandLabel, String[] args);

	/**
	 * Runs after having checked the permission.
	 */
	public abstract List<String> doTabComplete(CommandSender sender, Command cmd, String commandLabel, String[] args);

	public Message getMessage(String sectionName, String message)
	{
		return this.getNetworkPlugin().getMessageSection(sectionName).getMessage(message);
	}

	public String getMessage(String sectionName, String message, CommandSender sender)
	{
		User user = this.getUser(sender);
		return this.getMessage(sectionName, message).getMessage(user);
	}

	public abstract NetworkPlugin getNetworkPlugin();

	@Override
	public final boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (!this.hasPermission(sender))
		{
			User user = User.getUser(sender);
			String message = this.getNoPermissionMessageError().getMessage(user);
			sender.sendMessage(this.color(message));
			return true;
		}
		this.doCommand(sender, cmd, commandLabel, args);
		return true;
	}

	@Override
	public final List<String> onTabComplete(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (!this.hasPermission(sender)) { return null; }
		return this.doTabComplete(sender, cmd, commandLabel, args);
	}

	public void sendColoredMessage(CommandSender sender, String message)
	{
		sender.sendMessage(this.color(message));
	}

	private NetworkPlugin getNetworkPluginError()
	{
		NetworkPlugin p = this.getNetworkPlugin();
		if (p == null) { throw new IllegalStateException("NetworkPlugin is null"); }
		return p;
	}

	private Message getNoPermissionMessageError()
	{
		Message message = this.getNetworkPluginError().getMessage(this.messageSection, this.noPermission);
		if (message == null) { throw new IllegalStateException(
			String.format("No permission message is null, '%s' '%s'", this.messageSection, this.noPermission)); }
		return message;
	}
}
