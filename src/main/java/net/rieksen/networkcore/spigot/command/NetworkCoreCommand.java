package net.rieksen.networkcore.spigot.command;

import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;

public abstract class NetworkCoreCommand extends NetworkPluginCommand
{

	protected final NetworkCoreSpigot provider;

	public NetworkCoreCommand(NetworkCoreSpigot plugin, String commandName, String permission)
	{
		super(commandName, permission);
		this.provider = plugin;
	}

	@Override
	public final NetworkPlugin getNetworkPlugin()
	{
		return this.provider.getNetworkPlugin();
	}
}
