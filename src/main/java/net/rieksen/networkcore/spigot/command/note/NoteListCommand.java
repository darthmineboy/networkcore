package net.rieksen.networkcore.spigot.command.note;

import java.text.DateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import net.rieksen.networkcore.core.NetworkCoreAPI;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.user.UserType;
import net.rieksen.networkcore.core.user.note.UserNote;
import net.rieksen.networkcore.core.user.pojo.UserPojo;
import net.rieksen.networkcore.core.util.SimplePage;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.command.NetworkCoreCommand;

public class NoteListCommand extends NetworkCoreCommand
{

	private final static String	MESSAGE_SECTION				= "User Notes";
	private final static String	USAGE_MESSAGE				= "note-list-usage";
	private final static String	USER_NOT_FOUND_MESSAGE		= "user-not-found";
	private final static String	NOTE_LIST_HEADER_MESSAGE	= "note-list-header";
	private final static String	NOTE_LIST_ENTRY_MESSAGE		= "note-list-entry";
	private final int			RESULTS_PER_PAGE			= 5;

	public NoteListCommand(NetworkCoreSpigot plugin)
	{
		super(plugin, "list", "networkcore.note.list");
	}

	@Override
	public void doCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (args.length < 1)
		{
			this.getNetworkPlugin().getMessage(NoteListCommand.MESSAGE_SECTION, NoteListCommand.USAGE_MESSAGE).sendMessage(sender);
			return;
		}
		User user = this.provider.getUserModule().getUser(UserType.MINECRAFT_PLAYER, args[0]);
		if (user == null)
		{
			Map<String, String> variables = new HashMap<>();
			variables.put("%name%", args[0]);
			this.getNetworkPlugin().getMessage(NoteListCommand.MESSAGE_SECTION, NoteListCommand.USER_NOT_FOUND_MESSAGE).sendMessage(sender,
				variables);
			return;
		}
		int page = 1;
		if (args.length > 1)
		{
			try
			{
				page = Integer.parseInt(args[1]);
				if (page < 1)
				{
					page = 1;
				}
			} catch (NumberFormatException e)
			{
				// Silence
			}
		}
		final int finalPage = page;
		this.provider.getTaskChainFactory().newChain().async(() ->
		{
			SimplePage<UserNote> notes = user.findNotes((finalPage - 1) * this.RESULTS_PER_PAGE, this.RESULTS_PER_PAGE);
			Map<String, String> variables = new HashMap<>();
			variables.put("%name%", user.getName());
			variables.put("%max-page%", String.valueOf(finalPage));
			variables.put("%page%", String.valueOf(notes.getCurrentPage()));
			this.getNetworkPlugin().getMessage(NoteListCommand.MESSAGE_SECTION, NoteListCommand.NOTE_LIST_HEADER_MESSAGE)
				.sendMessage(sender, variables);
			DateFormat dateFormat = DateFormat.getInstance();
			for (UserNote note : notes.getData())
			{
				variables = new HashMap<>();
				variables.put("%id%", String.valueOf(note.getId()));
				variables.put("%creation-date%", dateFormat.format(note.getCreationDate()));
				variables.put("%created-by%", note.getCreatedBy().getName());
				variables.put("%message%", note.getMessage());
				this.getNetworkPlugin().getMessage(NoteListCommand.MESSAGE_SECTION, NoteListCommand.NOTE_LIST_ENTRY_MESSAGE)
					.sendMessage(sender, variables);
			}
		}).execute();
	}

	@Override
	public List<String> doTabComplete(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (args.length == 1) { return NetworkCoreAPI.getDAO().getUserDAO().findUsersByName(args[0], 0, 50).stream()
			.map(UserPojo::getUserName).collect(Collectors.toList()); }
		return null;
	}
}
