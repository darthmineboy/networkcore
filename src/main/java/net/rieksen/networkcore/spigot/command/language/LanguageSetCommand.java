package net.rieksen.networkcore.spigot.command.language;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import net.rieksen.networkcore.core.message.Language;
import net.rieksen.networkcore.core.message.UserLanguagePreferences;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.command.NetworkCoreCommand;

public class LanguageSetCommand extends NetworkCoreCommand
{

	public LanguageSetCommand(NetworkCoreSpigot plugin)
	{
		super(plugin, "set", "networkcore.language");
	}

	@Override
	public void doCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		User user = User.getUser(sender);
		if (args.length == 0)
		{
			String message =
				this.getNetworkPlugin().getMessageSection("Language Command").getMessage("language-set-insufficient-args").getMessage(user);
			user.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
			return;
		}
		String languageName = args[0];
		Language language = Language.getLanguage(languageName);
		if (language == null)
		{
			String message = this.getNetworkPlugin().getMessageSection("Language Command").getMessage("language-set-insufficient-args")
				.getMessage(user).replace("%language%", languageName);
			user.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
			return;
		}
		user.changeAutoLanguage(false);
		UserLanguagePreferences pref = user.getLanguagePreferences();
		pref.setLanguage(language.getLanguageId());
		String message = this.getNetworkPlugin().getMessageSection("Language Command").getMessage("language-set").getMessage(user)
			.replace("%language%", language.getName());
		user.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
	}

	@Override
	public List<String> doTabComplete(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (args.length == 1)
		{
			String languageName = args[0];
			List<Language> languages = this.provider.getMessageModule().getLanguages();
			List<String> list = new ArrayList<>();
			languages.stream().filter(language -> language.getName().toLowerCase().startsWith(languageName.toLowerCase()))
				.forEach(language -> list.add(language.getName()));
			return list;
		}
		return null;
	}
}
