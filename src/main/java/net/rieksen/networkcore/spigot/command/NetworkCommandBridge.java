package net.rieksen.networkcore.spigot.command;

import java.util.Arrays;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

/**
 * Used to register a {@link NetworkCommand} in the Bukkit API.
 */
public class NetworkCommandBridge implements CommandExecutor, TabCompleter
{

	private NetworkCommand command;

	public NetworkCommandBridge(NetworkCommand command)
	{
		this.command = command;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (args.length == 0) { return this.command.onCommand(sender, cmd, commandLabel, args); }
		NetworkCommand command = this.command;
		int i = 0;
		outerloop: for (; i < args.length; i++)
		{
			String arg = args[i];
			for (NetworkCommand subCommand : command.getSubCommands())
			{
				if (arg.equalsIgnoreCase(subCommand.getCommandName()))
				{
					command = subCommand;
					continue outerloop;
				}
			}
			break;
		}
		return command.onCommand(sender, cmd, commandLabel, Arrays.copyOfRange(args, i, args.length));
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (args.length == 0) { return this.command.onTabComplete(sender, cmd, commandLabel, args); }
		NetworkCommand command = this.command;
		int i = 0;
		outerloop: for (; i < args.length; i++)
		{
			String arg = args[i];
			for (NetworkCommand subCommand : command.getSubCommands())
			{
				if (arg.equalsIgnoreCase(subCommand.getCommandName()))
				{
					command = subCommand;
					continue outerloop;
				}
			}
			break;
		}
		return command.onTabComplete(sender, cmd, commandLabel, Arrays.copyOfRange(args, i, args.length));
	}
}
