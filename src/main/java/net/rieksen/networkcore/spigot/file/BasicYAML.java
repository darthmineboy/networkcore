package net.rieksen.networkcore.spigot.file;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.lang.Validate;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import net.rieksen.networkcore.core.util.FileUtil;

public class BasicYAML<T extends Plugin>
{

	protected final T			plugin;
	protected final File		file;
	protected YamlConfiguration	yaml;
	protected Exception			exception;

	/**
	 * A simple class to handle YAML files. When the directory is null, or not a
	 * directory, the plugin directory is used.
	 * 
	 * @param plugin
	 * @param directory
	 * @param fileName
	 * @throws IllegalStateException
	 *             when plugin or filename is null
	 */
	public BasicYAML(T plugin, File directory, String fileName)
	{
		if (directory == null || !directory.isDirectory())
		{
			if (plugin == null)
			{
				throw new IllegalStateException("plugin and directory cannot both be null");
			} else
			{
				directory = plugin.getDataFolder();
			}
		}
		if (fileName == null) { throw new IllegalStateException("fileName cannot be null!"); }
		this.plugin = plugin;
		this.file = new File(directory, fileName);
	}

	/**
	 * Copies the file. If the file does not exist, nothing happens. If the
	 * target file already exists it will be overwritten.
	 * 
	 * @param target
	 * @throws NullPointerException
	 *             target is null
	 */
	public void copy(File target)
	{
		Validate.notNull(target);
		if (!this.file.exists()) { return; }
		this.copyFile(this.file, target);
	}

	/**
	 * Creates the file parent directories.
	 * 
	 * @return
	 */
	public boolean createDirectories()
	{
		File directory = this.file.getParentFile();
		if (!directory.exists())
		{
			if (!directory.mkdirs()) { return false; }
		}
		return true;
	}

	/**
	 * Creates an file with the content of {@link InputStream} when the file
	 * does not yet exist.
	 * 
	 * @param stream
	 * @return true when successful
	 */
	public boolean createIfNotExists(InputStream stream)
	{
		if (this.exists()) { return true; }
		if (!this.createDirectories()) { return false; }
		try
		{
			FileUtil.saveFile(stream, this.file);
			return true;
		} catch (IOException e)
		{
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Whether the file exists.
	 * 
	 * @return
	 */
	public boolean exists()
	{
		return this.file.exists();
	}

	public File getFile()
	{
		return this.file;
	}

	/**
	 * Get the plugin.
	 * 
	 * @return
	 */
	public T getPlugin()
	{
		return this.plugin;
	}

	/**
	 * Get the {@link YAMLConfiguration}.
	 * 
	 * @return
	 */
	public YamlConfiguration getYaml()
	{
		return this.yaml;
	}

	/**
	 * Whether {@link #getYaml()} != null.
	 * 
	 * @return
	 */
	public boolean isLoaded()
	{
		return this.yaml != null;
	}

	/**
	 * Loads the {@link YamlConfiguration}. An empty file is generated and saved
	 * if no file exists.
	 * <p>
	 * If an exception occurred, this method returns false. The exception can be
	 * retrieved with {@link #exception}.
	 * 
	 * @return true when successful
	 */
	public boolean load()
	{
		try
		{
			if (!this.file.exists()) { return this.save(); }
			YamlConfiguration yaml = new YamlConfiguration();
			yaml.load(this.file);
			this.yaml = yaml;
			return true;
		} catch (Exception e)
		{
			this.exception = e;
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Saves the {@link YamlConfiguration} to disk.
	 * 
	 * @return true when successful
	 */
	public boolean save()
	{
		if (this.yaml == null)
		{
			this.yaml = new YamlConfiguration();
		}
		if (!this.createDirectories()) { return false; }
		try
		{
			this.yaml.save(this.file);
			return true;
		} catch (IOException e)
		{
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Helper function that copies a file from src to target.
	 * 
	 * @param src
	 * @param target
	 */
	protected void copyFile(File src, File target)
	{
		org.bukkit.util.FileUtil.copy(src, target);
	}
}
