package net.rieksen.networkcore.spigot.file;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.YamlConfiguration;

import net.rieksen.networkcore.core.server.Server;
import net.rieksen.networkcore.core.server.ServerFactory;
import net.rieksen.networkcore.core.server.ServerType;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;

/**
 * Handles the loading of the server-id if present, or else creates a new
 * server.
 */
public class AutomaticServerConfig extends BasicYAML<NetworkCoreSpigot>
{

	public AutomaticServerConfig(NetworkCoreSpigot plugin)
	{
		super(plugin, plugin.getDataFolder(), "automatic.yml");
	}

	public long getLastSync(String pluginName, String id)
	{
		return this.yaml.getLong("plugins." + pluginName + "." + id);
	}

	public void init()
	{
		this.load();
		this.yaml.options().header("Do not copy this configuration to other servers\nEvery server must have a unique server-id");
		this.yaml.options().copyHeader(true);
		int serverId = this.getServerId();
		if (serverId == 0)
		{
			// compatability
			serverId = this.checkOldServerId();
			if (serverId != 0)
			{
				this.plugin.getLogger().info("Found server-id in config.yml, moving to automatic.yml");
				this.setServerId(serverId);
				this.deleteOldServerId();
				try
				{
					this.loadServer(serverId);
				} catch (Exception e)
				{
					this.plugin.getLogger().severe("Failed to load server: " + e.getMessage());
				}
			} else
			{
				this.createServer();
			}
		} else
		{
			try
			{
				this.loadServer(serverId);
			} catch (Exception e)
			{
				this.plugin.getLogger().severe("Failed to load server: " + e.getMessage());
			}
		}
	}

	public void setLastSync(String pluginName, String id, long date)
	{
		this.yaml.set("plugins." + pluginName + "." + id, date);
		this.save();
	}

	private int checkOldServerId()
	{
		File oldFile = new File(this.plugin.getDataFolder(), "config.yml");
		if (!oldFile.exists()) { return 0; }
		YamlConfiguration old = YamlConfiguration.loadConfiguration(oldFile);
		return old.getInt("server-id");
	}

	private void createServer()
	{
		Server server = this.plugin.getServerModule().createLocalServer("temp-" + System.currentTimeMillis(), ServerType.SPIGOT);
		this.setServerId(server.getServerId());
		server.changeName("server" + server.getServerId());
		this.plugin.getServerModule().setLocalServer(server);
	}

	private void deleteOldServerId()
	{
		File oldFile = new File(this.plugin.getDataFolder(), "config.yml");
		if (!oldFile.exists()) { return; }
		YamlConfiguration old = YamlConfiguration.loadConfiguration(oldFile);
		old.set("server-id", null);
		try
		{
			old.save(oldFile);
		} catch (IOException e)
		{
			this.plugin.getLogger().info("Failed to save config.yml: " + e.getMessage());
		}
	}

	private int getServerId()
	{
		return this.yaml.getInt("server-id");
	}

	private void loadServer(int serverId) throws Exception
	{
		Server server = this.plugin.getServerModule().getServer(serverId);
		if (server == null) { throw new Exception(
			"Server #" + serverId + " does not exists! Fix the issue or delete server-id to generate a new server"); }
		server = ServerFactory.createLocalServer(this.plugin, server.toPojo());
		this.plugin.getServerModule().setLocalServer(server);
	}

	private void setServerId(int serverId)
	{
		this.yaml.set("server-id", serverId);
		this.save();
	}
}
