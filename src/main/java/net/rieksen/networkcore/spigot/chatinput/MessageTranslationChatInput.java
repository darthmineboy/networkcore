package net.rieksen.networkcore.spigot.chatinput;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.message.MessageTranslation;
import net.rieksen.networkcore.core.message.MessageVariable;
import net.rieksen.networkcore.core.message.Language;
import net.rieksen.networkcore.core.message.Message;

public abstract class MessageTranslationChatInput extends TextEditorChatInput
{

	private final int messageId;

	public MessageTranslationChatInput(NetworkCore plugin, int messageId, CommandSender sender)
	{
		super(sender, 1024);
		this.messageId = messageId;
		this.actionAdd = ChatColor.translateAlternateColorCodes('&', "&eAdd <text> - Add a chunk of text");
		this.actionAddMaxLengthReached = ChatColor.translateAlternateColorCodes('&', "&cMax text length reached!");
		this.actionCancel = ChatColor.translateAlternateColorCodes('&', "&eCancel - Cancel");
		this.actionDone = ChatColor.translateAlternateColorCodes('&', "&eDone - Save");
		this.actionReset = ChatColor.translateAlternateColorCodes('&', "&eReset - Reset text editor");
	}

	@Override
	public void onMessage(String message)
	{
		if (message.equalsIgnoreCase("translations"))
		{
			Message messageA = Message.getMessage(this.messageId);
			for (MessageTranslation translation : messageA.getTranslations())
			{
				this.sender.sendMessage(
					ChatColor.translateAlternateColorCodes('&', "&6&l" + Language.getLanguage(translation.getLanguageId()).getName()));
				this.sender.sendMessage(ChatColor.translateAlternateColorCodes('&', translation.getMessage()));
			}
			return;
		} else if (message.equalsIgnoreCase("variables"))
		{
			Message messageA = Message.getMessage(this.messageId);
			this.sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6Variables &8:"));
			for (MessageVariable variable : messageA.getVariables())
			{
				this.sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					"&e" + variable.getName() + " &8: &f" + (variable.getDescription() == null ? "" : variable.getDescription())));
			}
			return;
		}
		super.onMessage(message);
	}

	@Override
	public void onOpen()
	{
		this.sendHelp();
	}

	@Override
	public void onReset()
	{
		this.sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eReset Message Editor"));
	}

	@Override
	public void sendHelp()
	{
		this.sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eType a command to continue"));
		this.sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eTranslations - List translations"));
		this.sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eVariables - List variables"));
		this.sendActions();
	}
}
