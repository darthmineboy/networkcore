package net.rieksen.networkcore.spigot.chatinput;

import org.bukkit.command.CommandSender;

import net.rieksen.networkcore.core.server.Server;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;

public abstract class ServerChangeNameChatInput extends ChatInput
{

	private final NetworkCoreSpigot	plugin;
	private final int			serverId;
	private final CommandSender	sender;

	public ServerChangeNameChatInput(NetworkCoreSpigot plugin, int serverId, CommandSender sender)
	{
		this.plugin = plugin;
		this.serverId = serverId;
		this.sender = sender;
	}

	public abstract void onCancel();

	public abstract void onComplete();

	@Override
	public void onMessage(String message)
	{
		if (message.equalsIgnoreCase("cancel"))
		{
			this.plugin.getChatInputContainer().exitChatInput(this.sender);
			this.onCancel();
			return;
		}
		String[] args = message.split(" ");
		if (args.length != 1)
		{
			this.sender.sendMessage("Server name must be a single word. Optionally use underscores");
			return;
		}
		Server server = Server.getServer(this.serverId);
		if (server == null)
		{
			this.sender.sendMessage("Failed to retrieve the server #" + this.serverId);
			return;
		}
		Server duplicate = Server.getServer(message);
		if (duplicate != null)
		{
			if (server != duplicate)
			{
				this.sender.sendMessage("There is already a server with name " + message);
				return;
			}
		}
		server.changeName(message);
		this.sender.sendMessage("Updated server name to " + server.getName() + " for server #" + this.serverId);
		this.plugin.getChatInputContainer().exitChatInput(this.sender);
		this.onComplete();
	}

	@Override
	public void onOpen()
	{
		this.sender.sendMessage("Please specify a single-word to update the server name to for server #" + this.serverId);
		this.sender.sendMessage("Type cancel to cancel this operation");
	}
}
