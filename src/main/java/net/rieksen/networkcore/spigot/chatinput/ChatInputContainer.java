package net.rieksen.networkcore.spigot.chatinput;

import java.util.Map;

import org.bukkit.command.CommandSender;

import com.google.common.collect.Maps;

public class ChatInputContainer
{

	private Map<CommandSender, ChatInput>	chatInputMap	= Maps.newHashMap();

	public boolean hasChatInput(CommandSender sender)
	{
		return chatInputMap.containsKey(sender);
	}

	/**
	 * First exits the current {@link ChatInput} if present, and then opens the
	 * new {@link ChatInput}
	 * 
	 * @param player
	 * @param chatInput
	 */
	public void openChatInput(CommandSender sender, ChatInput chatInput)
	{
		ChatInput oldChatInput = chatInputMap.put(sender, chatInput);
		if (oldChatInput != null)
		{
			oldChatInput.onExit();
		}
		chatInput.onOpen();
	}

	/**
	 * Exits the current {@link ChatInput} if present
	 * 
	 * @param player
	 */
	public void exitChatInput(CommandSender sender)
	{
		ChatInput chatInput = chatInputMap.get(sender);
		if (chatInput != null)
		{
			chatInput.onExit();
			chatInputMap.remove(sender);
		}
	}

	public ChatInput getChatInput(CommandSender sender)
	{
		return chatInputMap.get(sender);
	}

	public void exitAllChatInputs()
	{
		for (CommandSender sender : chatInputMap.keySet())
		{
			exitChatInput(sender);
		}
	}
}
