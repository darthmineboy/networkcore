package net.rieksen.networkcore.spigot.chatinput;

import org.bukkit.event.player.AsyncPlayerChatEvent;

public abstract class ChatInput
{

	/**
	 * Called on
	 * {@link ChatInputContainer#openChatInput(org.bukkit.command.CommandSender, ChatInput)}
	 * .
	 */
	public void onOpen()
	{}

	/**
	 * Called on {@link PlayerQuitEvent}, when another {@link ChatInput} is
	 * opened.
	 */
	public void onExit()
	{}

	/**
	 * Called prior to {@link #onMessage(String)}. Event is cancelled by
	 * default. Invokes {@link #onMessage(String)}.
	 */
	public void onAsyncPlayerChat(AsyncPlayerChatEvent e)
	{
		e.setCancelled(true);
		onMessage(e.getMessage());
	}

	/**
	 * Called after {@link #onAsyncPlayerChat(AsyncPlayerChatEvent)}
	 * 
	 * @param message
	 */
	public void onMessage(String message)
	{}
}
