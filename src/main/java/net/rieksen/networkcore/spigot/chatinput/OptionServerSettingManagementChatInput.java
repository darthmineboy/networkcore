package net.rieksen.networkcore.spigot.chatinput;

import java.util.List;

import org.apache.commons.lang.Validate;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import net.rieksen.networkcore.core.option.Option;
import net.rieksen.networkcore.core.option.OptionSection;
import net.rieksen.networkcore.core.option.OptionValue;
import net.rieksen.networkcore.core.option.OptionImpl;
import net.rieksen.networkcore.core.option.OptionSectionImpl;
import net.rieksen.networkcore.core.option.OptionType;
import net.rieksen.networkcore.core.option.pojo.OptionValuePojo;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.core.server.Server;
import net.rieksen.networkcore.core.util.Action;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;

public class OptionServerSettingManagementChatInput extends ChatInput
{

	private final NetworkCoreSpigot	plugin;
	private final int			optionId;
	private final Integer		serverId;
	private final Player		player;
	private final Action		cancelAction;

	public OptionServerSettingManagementChatInput(NetworkCoreSpigot plugin, int optionId, Integer serverId, Player player, Action cancelAction)
	{
		Validate.notNull(plugin);
		Validate.notNull(optionId);
		this.plugin = plugin;
		this.optionId = optionId;
		this.serverId = serverId;
		this.player = player;
		this.cancelAction = cancelAction;
	}

	@Override
	public void onMessage(String message)
	{
		if (message.equalsIgnoreCase("cancel"))
		{
			this.plugin.getChatInputContainer().exitChatInput(this.player);
			if (this.cancelAction != null)
			{
				this.cancelAction.onAction();
			}
			return;
		}
		Option option = OptionImpl.getOption(this.optionId);
		if (option == null) { return; }
		String[] args = message.split(" ");
		if (option.getType() == OptionType.BOOLEAN)
		{
			if (args.length < 1)
			{
				this.sendInstruction();
				return;
			}
			boolean value;
			if (args[0].equalsIgnoreCase("true"))
			{
				value = true;
			} else if (args[0].equalsIgnoreCase("false"))
			{
				value = false;
			} else
			{
				this.sendInstruction();
				return;
			}
			List<OptionValue> values = option.getApplyingValues(this.serverId);
			if (values.isEmpty())
			{
				option.createValue(new OptionValuePojo(0, this.optionId, this.serverId, 0, Boolean.toString(value)));
				this.player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eAdded value &b" + Boolean.toString(value)));
			} else
			{
				OptionValue optionValue = values.get(0);
				optionValue.changeValue(Boolean.toString(value));
				this.player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eUpdated value to &b" + Boolean.toString(value)));
			}
		}
	}

	@Override
	public void onOpen()
	{
		Option option = OptionImpl.getOption(this.optionId);
		if (option == null) { return; }
		OptionSection section = OptionSectionImpl.getSection(option.getSectionId());
		if (section == null) { return; }
		NetworkPlugin plugin = NetworkPlugin.getPlugin(section.getPluginId());
		if (plugin == null) { return; }
		String serverName = this.serverId == null ? "Default" : Server.getServer(this.serverId).getName();
		this.player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eServer &8: &b" + serverName));
		this.player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&ePlugin &8: &b" + plugin.getName()));
		this.player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eSection &8: &b" + section.getName()));
		this.player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eOption &8: &b" + option.getName()));
		this.player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eType &8: &b" + option.getType().toString()));
		this.sendCurrentValue();
		this.sendInstruction();
	}

	private void sendCurrentValue()
	{
		Option option = OptionImpl.getOption(this.optionId);
		if (option == null) { return; }
		List<OptionValue> values = option.getApplyingValues(this.serverId);
		if (values == null) { return; }
		if (option.getType() == OptionType.LIST)
		{
			this.player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eCurrent values &8:"));
			for (OptionValue value : values)
			{
				this.player.sendMessage(ChatColor.translateAlternateColorCodes('&', " &b" + value.getValue()));
			}
			return;
		}
		if (values.isEmpty())
		{
			this.player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eCurrent value &8: &bNo value found"));
			return;
		}
		this.player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eCurrent value &8: &b" + values.get(0).getValue()));
	}

	private void sendInstruction()
	{
		Option option = OptionImpl.getOption(this.optionId);
		if (option == null) { return; }
		if (option.getType() == OptionType.BOOLEAN)
		{
			this.player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&ePlease type a boolean &btrue&e/&bfalse"));
		} else
		{
			this.player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cInstructions not specified"));
		}
		this.player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eType &bcancel &eto cancel this operation"));
	}
}
