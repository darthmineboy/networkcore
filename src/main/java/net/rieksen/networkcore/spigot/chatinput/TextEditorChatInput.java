package net.rieksen.networkcore.spigot.chatinput;

import net.rieksen.networkcore.spigot.NetworkCoreSpigot;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public abstract class TextEditorChatInput extends ChatInput
{

	protected final CommandSender	sender;
	protected StringBuilder			str							= new StringBuilder();
	protected final int				MAX_LENGTH;
	protected String				actionAdd					= "add <text>";
	protected String				actionAddMaxLengthReached	= "Max length reached!";
	protected String				actionDone					= "done";
	protected String				actionReset					= "reset";
	protected String				actionCancel				= "cancel";

	public TextEditorChatInput(CommandSender sender, int maxLength)
	{
		this.sender = sender;
		this.MAX_LENGTH = maxLength;
	}

	protected void sendActions()
	{
		sender.sendMessage(actionAdd);
		sender.sendMessage(actionDone);
		sender.sendMessage(actionReset);
		sender.sendMessage(actionCancel);
	}

	@Override
	public void onMessage(String message)
	{
		if (message.equalsIgnoreCase("cancel"))
		{
			NetworkCoreSpigot.getInstance().getChatInputContainer().exitChatInput(sender);
			onCancel();
			return;
		} else if (message.equalsIgnoreCase("reset"))
		{
			str = new StringBuilder();
			onReset();
			return;
		} else if (message.equalsIgnoreCase("done"))
		{
			NetworkCoreSpigot.getInstance().getChatInputContainer().exitChatInput(sender);
			if (str.length() >= 1)
			{
				onDone(str.toString().substring(0, str.length() - 1));
			} else
			{
				onDone("");
			}
			return;
		}
		String[] args = message.split(" ");
		if (args.length >= 1)
		{
			if (args[0].equalsIgnoreCase("add"))
			{
				if (args.length == 1)
				{
					sender.sendMessage(actionAdd);
					return;
				}
				for (int i = 1; i < args.length; i++)
				{
					if (str.length() + args[i].length() > MAX_LENGTH)
					{
						sender.sendMessage(actionAddMaxLengthReached);
						break;
					}
					str.append(args[i] + " ");
				}
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', str.toString().substring(0, str.length() - 1)));
				return;
			}
		}
		sendHelp();
	}

	public abstract void onCancel();

	public abstract void onReset();

	public abstract void sendHelp();

	public abstract void onDone(String text);
}
