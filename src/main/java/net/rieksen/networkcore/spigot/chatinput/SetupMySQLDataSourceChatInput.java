package net.rieksen.networkcore.spigot.chatinput;

import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.scheduler.BukkitRunnable;

import net.md_5.bungee.api.ChatColor;
import net.rieksen.networkcore.core.util.MySQL;
import net.rieksen.networkcore.core.util.MySQLSetting;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.object.ConfigFileHelper;

public class SetupMySQLDataSourceChatInput extends ChatInput
{

	private final NetworkCoreSpigot	plugin;
	private final CommandSender	sender;
	private String				host;
	private String				database;
	private String				username;
	private String				password;

	public SetupMySQLDataSourceChatInput(NetworkCoreSpigot plugin, CommandSender sender)
	{
		this.plugin = plugin;
		this.sender = sender;
	}

	@Override
	public void onMessage(String message)
	{
		if (message.equalsIgnoreCase("reset"))
		{
			this.resetInput();
		} else if (this.host == null)
		{
			this.host = message;
		} else if (this.database == null)
		{
			this.database = message;
		} else if (this.username == null)
		{
			this.username = message;
		} else if (this.password == null)
		{
			this.password = message;
			if (this.testConnection())
			{
				this.sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					"&a&lCongratulations! &aThe MySQL connection was successfully established"));
				this.sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eSaving configured MySQL settings..."));
				ConfigFileHelper config = this.plugin.getConfigHelper();
				YamlConfiguration yaml = config.getYaml();
				yaml.set("settings.backend", "mysql");
				ConfigurationSection mysql = yaml.getConfigurationSection("settings.backends.mysql");
				mysql.set("host", this.host);
				mysql.set("database", this.database);
				mysql.set("username", this.username);
				mysql.set("password", this.password);
				if (config.save())
				{
					this.sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eSaved MySQL settings"));
				} else
				{
					this.sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&4&lERROR: &cFailed to save MySQL settings"));
					return;
				}
				this.sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eRestarting server in 10 seconds..."));
				new BukkitRunnable()
				{

					@Override
					public void run()
					{
						SetupMySQLDataSourceChatInput.this.plugin.getServer().spigot().restart();;
					}
				}.runTaskLater(this.plugin, 200);
				return;
			} else
			{
				this.sender.sendMessage(
					ChatColor.translateAlternateColorCodes('&', "&c&lOh no... &cWe were unable to establish the MySQL connection"));
				this.sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cPlease try again..."));
				this.resetInput();
			}
		}
		this.sendStatus();
	}

	@Override
	public void onOpen()
	{
		this.sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7&lNetworkCore MySQL DataSource Setup"));
		this.sender.sendMessage("");
		this.sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eYou can type &6reset &eto reset previous input"));
		this.sendStatus();
	}

	private void resetInput()
	{
		this.host = null;
		this.database = null;
		this.username = null;
		this.password = null;
	}

	private void sendStatus()
	{
		this.sender.sendMessage("");
		if (this.host == null)
		{
			this.sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&ePlease specify the &bMySQL host (e.g. 127.0.0.1)"));
		} else if (this.database == null)
		{
			this.sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&ePlease specify the &bMySQL database (e.g. myDatabase)"));
		} else if (this.username == null)
		{
			this.sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&ePlease specify the &bMySQL username (e.g. myUsername)"));
		} else if (this.password == null)
		{
			this.sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&ePlease specify the &bMySQL password (e.g. myPassword)"));
		}
	}

	private boolean testConnection()
	{
		try
		{
			MySQLSetting setting = new MySQLSetting(this.host, this.database, this.username, this.password);
			new MySQL(setting);
			return true;
		} catch (Exception e)
		{
			return false;
		}
	}
}
