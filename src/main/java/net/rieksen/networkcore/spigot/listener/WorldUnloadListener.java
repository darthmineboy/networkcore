package net.rieksen.networkcore.spigot.listener;

import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.world.WorldUnloadEvent;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.world.ServerWorld;

public class WorldUnloadListener implements Listener
{

	private final NetworkCore plugin;

	public WorldUnloadListener(NetworkCore plugin)
	{
		this.plugin = plugin;
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onWorldUnload(WorldUnloadEvent e)
	{
		World world = e.getWorld();
		ServerWorld serverWorld = this.plugin.getWorldModule().getWorld(world);
		serverWorld.keepCached(false);
	}
}
