package net.rieksen.networkcore.spigot.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;

public class PlayerQuitListener implements Listener
{

	private NetworkCoreSpigot plugin;

	public PlayerQuitListener(NetworkCoreSpigot plugin)
	{
		this.plugin = plugin;
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerQuitLowest(PlayerQuitEvent e)
	{
		Player player = e.getPlayer();
		this.plugin.getChatInputContainer().exitChatInput(player);
		this.plugin.getChestMenuContainer().exitChestMenu(player);
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerQuitMonitor(PlayerQuitEvent e)
	{
		Player player = e.getPlayer();
		User user = User.getUser(player);
		this.plugin.getUserModule().handleLogout(user);
	}
}
