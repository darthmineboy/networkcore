package net.rieksen.networkcore.spigot.listener;

import java.util.UUID;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent.Result;

import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.user.UserImpl;
import net.rieksen.networkcore.core.user.UserModule;
import net.rieksen.networkcore.core.user.UserType;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;

/**
 * In the AsyncPlayerPreLoginEvent we attempt to create/get the {@link UserImpl}
 * for this player. The {@link UserImpl} object should be available and cached
 * in future events.
 * 
 * @author Darthmineboy
 */
public class AsyncPlayerPreLoginListener implements Listener
{

	private NetworkCoreSpigot plugin;

	public AsyncPlayerPreLoginListener(NetworkCoreSpigot plugin)
	{
		this.plugin = plugin;
	}

	@EventHandler
	public void onAsyncPlayerPreLogin(AsyncPlayerPreLoginEvent e)
	{
		String name = e.getName();
		UUID uuid = e.getUniqueId();
		User user = User.getUser(uuid);
		if (user == null)
		{
			user = User.getUser(UserType.MINECRAFT_PLAYER, name);
		}
		UserModule userModule = this.plugin.getUserModule();
		if (user == null)
		{
			user = userModule.createUser(UserType.MINECRAFT_PLAYER, uuid, name);
		}
		if (!user.getUUID().equals(uuid))
		{
			this.plugin.getLogger().warning("Player UUID has changed! Updating User...");
			user.changeUUID(uuid);
		}
		if (!user.getName().equals(name))
		{
			this.plugin.getLogger().warning("Player name has changed! Updating User...");
			user.changeName(name);
			user.changeCheckNameHistory(true);
		}
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onAsyncPlayerPreLoginMonitor(AsyncPlayerPreLoginEvent e)
	{
		if (e.getLoginResult() == Result.ALLOWED)
		{
			User user = User.getUser(e.getUniqueId());
			this.plugin.getUserModule().handleLogin(user, e.getName(), e.getAddress().getHostAddress());
		}
	}
}
