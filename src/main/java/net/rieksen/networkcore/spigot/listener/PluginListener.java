package net.rieksen.networkcore.spigot.listener;

import org.apache.commons.lang.Validate;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.event.server.PluginEnableEvent;

import net.rieksen.networkcore.core.enums.PluginStatus;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.core.server.Server;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;

public class PluginListener implements Listener
{

	private NetworkCoreSpigot plugin;

	public PluginListener(NetworkCoreSpigot plugin)
	{
		Validate.notNull(plugin);
		this.plugin = plugin;
	}

	@EventHandler
	public void onPluginDisable(PluginDisableEvent e)
	{
		if (this.plugin.getStatus() == PluginStatus.ERROR) { return; }
		this.plugin.getPluginModule().setPluginServerEnabled(e.getPlugin(), Server.getLocalServer().getServerId(), false);
	}

	@EventHandler
	public void onPluginEnable(PluginEnableEvent e)
	{
		if (this.plugin.getStatus() == PluginStatus.ERROR) { return; }
		this.plugin.getPluginModule().setPluginServerEnabled(e.getPlugin(), Server.getLocalServer().getServerId(), true);
		NetworkPlugin networkPlugin = this.plugin.getPluginModule().getPlugin(e.getPlugin());
		if (networkPlugin != null && !networkPlugin.isLicenseValid())
		{
			this.plugin.getServer().getPluginManager().disablePlugin(e.getPlugin());
		}
	}
}
