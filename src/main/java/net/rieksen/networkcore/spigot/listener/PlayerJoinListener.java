package net.rieksen.networkcore.spigot.listener;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitRunnable;

import net.rieksen.networkcore.core.dao.DAOManager;
import net.rieksen.networkcore.core.message.LocaleLanguage;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.chatinput.ChatInput;
import net.rieksen.networkcore.spigot.chatinput.SetupMySQLDataSourceChatInput;

public class PlayerJoinListener implements Listener
{

	private final NetworkCoreSpigot plugin;

	public PlayerJoinListener(NetworkCoreSpigot plugin)
	{
		this.plugin = plugin;
	}

	@EventHandler
	public void onPlayerJoinNormal(PlayerJoinEvent e)
	{
		Player player = e.getPlayer();
		this.handleSetup(player);
		this.handlePlayerLocale(player);
		this.checkLocalConnect(player);
	}

	/**
	 * Checks whether the player has a local connect set.
	 * 
	 * @param player
	 */
	private void checkLocalConnect(Player player)
	{
		User user = User.getUser(player);
		if (user.getLocalConnect() == null)
		{
			this.plugin.getLogger().warning("Player " + player.getName() + " has no local connect");
		}
	}

	private String getPlayerLocale(Player player)
	{
		try
		{
			Method m = player.getClass().getSuperclass().getMethod("getHandle");
			Object handle = m.invoke(player);
			Field localeField = handle.getClass().getDeclaredField("locale");
			localeField.setAccessible(true);
			String locale = (String) localeField.get(handle);
			return locale;
		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	private void handlePlayerLocale(Player player)
	{
		User user = User.getUser(player);
		if (!user.isAutoLanguage()) { return; }
		new BukkitRunnable()
		{

			@Override
			public void run()
			{
				String playerLocale = PlayerJoinListener.this.getPlayerLocale(player);
				LocaleLanguage localeLanguage = PlayerJoinListener.this.plugin.getMessageModule().getLocaleLanguage(playerLocale);
				if (localeLanguage == null) { return; }
				Integer languageId = localeLanguage.getLanguageId();
				if (languageId != null && !user.getLanguagePreferences().hasLanguage(languageId))
				{
					user.getLanguagePreferences().setLanguage(languageId);
					String message = PlayerJoinListener.this.plugin.getNetworkPlugin().getMessageSection("Language Command")
						.getMessage("language-auto-configure").getMessage(user)
						.replace("%language%", localeLanguage.getLanguage().getName());
					user.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
				}
			};
		}.runTaskLaterAsynchronously(this.plugin, 20);
	}

	private void handleSetup(final Player player)
	{
		if (!player.hasPermission("ncore.setup")) { return; }
		DAOManager dao = this.plugin.getDAO();
		if (dao != null) { return; }
		new BukkitRunnable()
		{

			@Override
			public void run()
			{
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7&lNetworkCore Installation"));
				player.sendMessage("");
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cNetworkCore failed to establish datasource connection"));
				player.sendMessage(
					ChatColor.translateAlternateColorCodes('&', "&ePlease follow the instructions to setup the datasource connection"));
				new BukkitRunnable()
				{

					@Override
					public void run()
					{
						ChatInput input = new SetupMySQLDataSourceChatInput(PlayerJoinListener.this.plugin, player);
						PlayerJoinListener.this.plugin.getChatInputContainer().openChatInput(player, input);
					}
				}.runTaskLater(PlayerJoinListener.this.plugin, 100);
			}
		}.runTaskLater(this.plugin, 100);
	}
}
