package net.rieksen.networkcore.spigot.listener;

import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.chestmenu.ChestMenu;
import net.rieksen.networkcore.spigot.chestmenu.ChestMenuContainer;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryClickListener implements Listener
{

	private NetworkCoreSpigot	plugin;

	public InventoryClickListener(NetworkCoreSpigot plugin)
	{
		this.plugin = plugin;
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onInventoryClick(InventoryClickEvent e)
	{
		if (!(e.getWhoClicked() instanceof Player)) { return; }
		ChestMenuContainer chestMenuContainer = plugin.getChestMenuContainer();
		Player player = (Player) e.getWhoClicked();
		ChestMenu chestMenu = chestMenuContainer.getChestMenu(player);
		if (chestMenu != null)
		{
			e.setCancelled(true);
			chestMenu.onInventoryClick(e);
		}
	}
}
