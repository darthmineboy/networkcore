package net.rieksen.networkcore.spigot.listener;

import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.world.WorldLoadEvent;

import net.rieksen.networkcore.core.NetworkCore;

public class WorldLoadListener implements Listener
{

	private final NetworkCore	plugin;

	public WorldLoadListener(NetworkCore plugin)
	{
		this.plugin = plugin;
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onWorldLoad(WorldLoadEvent e)
	{
		World world = e.getWorld();
		this.plugin.getWorldModule().getWorld(world);
	}
}
