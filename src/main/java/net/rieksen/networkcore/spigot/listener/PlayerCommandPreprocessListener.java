package net.rieksen.networkcore.spigot.listener;

import java.util.Date;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.scheduler.BukkitRunnable;

import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.user.UserConnect;
import net.rieksen.networkcore.core.user.pojo.UserChatPojo;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;

public class PlayerCommandPreprocessListener implements Listener
{

	private final NetworkCoreSpigot plugin;

	public PlayerCommandPreprocessListener(NetworkCoreSpigot plugin)
	{
		this.plugin = plugin;
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent e)
	{
		if (this.plugin.getStatus().isActive()) { return; }
		Player player = e.getPlayer();
		User user = User.getUser(player);
		if (user == null)
		{
			this.plugin.getLogger().severe("Failed to retrieve user for " + player.getName());
			return;
		}
		UserConnect connect = user.getConnect();
		if (connect == null)
		{
			this.plugin.getLogger().warning(
				String.format("Could not log chat of '%s': player has no connect. Check for errors in the logs", player.getName()));
			return;
		}
		UserChatPojo chat = new UserChatPojo(0, connect.getConnectId(), new Date(), e.getMessage(), e.isCancelled());
		new BukkitRunnable()
		{

			@Override
			public void run()
			{
				user.createChat(chat);
			}
		}.runTaskAsynchronously(this.plugin);
	}
}
