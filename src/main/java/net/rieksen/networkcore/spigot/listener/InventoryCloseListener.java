package net.rieksen.networkcore.spigot.listener;

import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.chestmenu.ChestMenu;
import net.rieksen.networkcore.spigot.chestmenu.ChestMenuContainer;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;

public class InventoryCloseListener implements Listener
{

	private NetworkCoreSpigot	plugin;

	public InventoryCloseListener(NetworkCoreSpigot plugin)
	{
		this.plugin = plugin;
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onInventoryClose(InventoryCloseEvent e)
	{
		Player player = (Player) e.getPlayer();
		ChestMenuContainer chestMenuContainer = plugin.getChestMenuContainer();
		ChestMenu chestMenu = chestMenuContainer.getChestMenu(player);
		if (chestMenu != null)
		{
			if (chestMenu.getInventory() != e.getView().getTopInventory()) { return; }
			chestMenuContainer.exitChestMenu(player);
		}
	}
}
