package net.rieksen.networkcore.spigot.listener;

import java.util.Date;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.scheduler.BukkitRunnable;

import net.rieksen.networkcore.core.server.Server;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.user.pojo.UserChatPojo;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.chatinput.ChatInput;
import net.rieksen.networkcore.spigot.chatinput.ChatInputContainer;

public class AsyncPlayerChatListener implements Listener
{

	private NetworkCoreSpigot plugin;

	public AsyncPlayerChatListener(NetworkCoreSpigot plugin)
	{
		this.plugin = plugin;
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onAsyncPlayerChatLowest(AsyncPlayerChatEvent e)
	{
		Player player = e.getPlayer();
		ChatInputContainer chatInputContainer = this.plugin.getChatInputContainer();
		if (chatInputContainer.hasChatInput(player))
		{
			ChatInput chatInput = chatInputContainer.getChatInput(player);
			chatInput.onAsyncPlayerChat(e);
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onAsyncPlayerChatMonitor(AsyncPlayerChatEvent e)
	{
		Player player = e.getPlayer();
		User user = User.getUser(player);
		if (user == null)
		{
			this.plugin.getLogger().severe("Failed to retrieve user for " + player.getName());
			return;
		}
		if (!this.plugin.getPlugin().getOptionSection("Config").getOption("chat-logging")
			.getBoolean(Server.getLocalServer().getServerId())) { return; }
		UserChatPojo pojo = new UserChatPojo(0, user.getConnect().getConnectId(), new Date(), e.getMessage(), e.isCancelled());
		new BukkitRunnable()
		{

			@Override
			public void run()
			{
				user.createChat(pojo);
			}
		}.runTaskAsynchronously(this.plugin);
	}
}
