package net.rieksen.networkcore.spigot.chestmenu;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.util.Action;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.material.ItemColor;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

public class LanguageManagementChestMenu extends ChestMenu
{

	private final Player	player;
	private final Action	action;

	public LanguageManagementChestMenu(NetworkCoreSpigot plugin, Player player, Action action)
	{
		super(plugin.getServer().createInventory(null, 54, ChatColor.translateAlternateColorCodes('&',
			Message.getMessage("NetworkCore", "Language Management Menu", "title").getMessage(User.getUser(player)))));
		this.player = player;
		this.action = action;
	}

	@Override
	public void fill()
	{
		User user = User.getUser(this.player);
		{
			ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.LIME, 1);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
				Message.getMessage("NetworkCore", "Language Management Menu", "language-list-item-name").getMessage(user)));
			item.setItemMeta(meta);
			this.addChestItem(0, new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					// TODO open language list
				}
			});
		}
		{
			ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.RED, 1);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(
				ChatColor.translateAlternateColorCodes('&', Message.getMessage("NetworkCore", "Menu", "back-item-name").getMessage(user)));
			item.setItemMeta(meta);
			this.addChestItem(53, new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					LanguageManagementChestMenu.this.action.onAction();
				}
			});
		}
	}
}
