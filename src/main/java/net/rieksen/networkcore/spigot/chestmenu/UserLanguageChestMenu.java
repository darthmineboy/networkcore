package net.rieksen.networkcore.spigot.chestmenu;

import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;

import net.rieksen.networkcore.core.message.Language;
import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.message.UserLanguagePreferences;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.user.pojo.UserLanguagePojo;
import net.rieksen.networkcore.core.util.ListUtil;
import net.rieksen.networkcore.core.util.LoreUtil;
import net.rieksen.networkcore.core.util.TextUtil;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.material.ItemColor;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

public class UserLanguageChestMenu extends ChestMenu
{

	private final ChestMenu			upperChestMenu	= this;
	private final NetworkCoreSpigot	plugin;
	private final Player			player;
	private final User				user;

	public UserLanguageChestMenu(NetworkCoreSpigot plugin, Player player)
	{
		super(plugin.getServer().createInventory(null, 9, ChatColor.translateAlternateColorCodes('&',
			Message.getMessage("NetworkCore", "Language Preference Menu", "title").getMessage(User.getUser(player)))));
		this.plugin = plugin;
		this.player = player;
		this.user = User.getUser(player);
	}

	@Override
	public void fill()
	{
		this.chestItems.clear();
		this.inventory.clear();
		UserLanguagePreferences prefs = this.user.getLanguagePreferences();
		List<UserLanguagePojo> list = prefs.getLanguages();
		{
			ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.ofDamage(this.user.isAutoLanguage() ? (short) 5 : 14), 1);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
				this.plugin.getNetworkPlugin().getMessage("Language Preference Menu", "auto-item-name").getMessage(this.user)));
			List<String> lore = LoreUtil.createLore(this.plugin.getNetworkPlugin()
				.getMessage("Language Preference Menu", "auto-item-lore-" + (this.user.isAutoLanguage() ? "enabled" : "disabled"))
				.getMessage(this.user));
			meta.setLore(lore);
			item.setItemMeta(meta);
			this.addChestItem(0, new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					UserLanguageChestMenu.this.user.changeAutoLanguage(!UserLanguageChestMenu.this.user.isAutoLanguage());
					UserLanguageChestMenu.this.fill();
					UserLanguageChestMenu.this.player.updateInventory();
				}
			});
		}
		for (int i = 0; i < 8; i++)
		{
			int itemSlot = i + 1;
			if (list.size() > i)
			{
				final UserLanguagePojo userLanguage = list.get(i);
				int languageId = userLanguage.getLanguageId();
				Language language = Language.getLanguage(languageId);
				ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.LIME, 1);
				ItemMeta meta = item.getItemMeta();
				meta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
					Message.getMessage("NetworkCore", "Language Preference Menu", "language-item-name").getMessage(this.user)
						.replace("%language%", language.getName())));
				List<String> lore = Lists.newArrayList();
				String[] args = Message.getMessage("NetworkCore", "Language Preference Menu", "language-item-lore-remove")
					.getMessage(this.user).split("\\|");
				for (String arg : args)
				{
					lore.addAll(TextUtil.splitMessage(ChatColor.translateAlternateColorCodes('&', "&f" + arg), 30));
				}
				meta.setLore(lore);
				item.setItemMeta(meta);
				this.addChestItem(itemSlot, new ChestItem(item)
				{

					@Override
					public void onInventoryClick(InventoryClickEvent e)
					{
						UserLanguagePreferences prefs = UserLanguageChestMenu.this.user.getLanguagePreferences();
						prefs.deleteLanguage(languageId);
						UserLanguageChestMenu.this.fill();
						UserLanguageChestMenu.this.player.updateInventory();
					}
				});
			} else
			{
				ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.YELLOW, 1);
				ItemMeta meta = item.getItemMeta();
				meta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
					Message.getMessage("NetworkCore", "Language Preference Menu", "item-name").getMessage(this.user)));
				List<String> lore = Lists.newArrayList();
				String[] args =
					Message.getMessage("NetworkCore", "Language Preference Menu", "item-lore-add").getMessage(this.user).split("\\|");
				for (String arg : args)
				{
					lore.addAll(TextUtil.splitMessage(ChatColor.translateAlternateColorCodes('&', "&f" + arg), 30));
				}
				meta.setLore(lore);
				item.setItemMeta(meta);
				this.addChestItem(itemSlot, new ChestItem(item)
				{

					@Override
					public void onInventoryClick(InventoryClickEvent e)
					{
						ALanguageListChestMenu languageList =
							new ALanguageListChestMenu(UserLanguageChestMenu.this.plugin.getServer().createInventory(null, 54,
								ChatColor.translateAlternateColorCodes('&',
									Message.getMessage("NetworkCore", "Language Preference Menu", "language-select-title")
										.getMessage(UserLanguageChestMenu.this.user))))
							{

								@Override
								public List<Language> getLanguageList()
								{
									return ListUtil.getSubList(
										UserLanguageChestMenu.this.user.getLanguagePreferences().getMissingLanguages().stream()
											.filter(language -> language.getLanguageId() != UserLanguageChestMenu.this.plugin
												.getMessageModule().getDefaultLanguage().getLanguageId())
											.collect(Collectors.toList()),
										this.page - 1, this.LIMIT);
								}

								@Override
								public int getTotalChestItems()
								{
									// -1 for default language
									return UserLanguageChestMenu.this.user.getLanguagePreferences().getMissingLanguages().size() - 1;
								}

								@Override
								public void onSelection(int languageId)
								{
									UserLanguagePreferences userLanguagePreferences =
										UserLanguageChestMenu.this.user.getLanguagePreferences();
									userLanguagePreferences.addLanguage(languageId);
									UserLanguageChestMenu.this.upperChestMenu.fill();
									UserLanguageChestMenu.this.plugin.getChestMenuContainer()
										.openChestMenu(UserLanguageChestMenu.this.player, UserLanguageChestMenu.this.upperChestMenu);
								}
							};
						languageList.fill();
						UserLanguageChestMenu.this.plugin.getChestMenuContainer().openChestMenu(UserLanguageChestMenu.this.player,
							languageList);
					}
				});
			}
		}
	}
}
