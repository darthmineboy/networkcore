package net.rieksen.networkcore.spigot.chestmenu;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;

import net.md_5.bungee.api.ChatColor;
import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.message.MessageSection;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.util.Action;
import net.rieksen.networkcore.core.util.ListUtil;
import net.rieksen.networkcore.core.util.TextUtil;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.material.ItemColor;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

@Deprecated
public class AdminPluginMessageSectionListChestMenu extends AMessageSectionListChestMenu
{

	private final NetworkCoreSpigot	plugin;
	private final int				pluginId;
	private final Player			player;
	private final User				user;
	private final Action			action;

	public AdminPluginMessageSectionListChestMenu(NetworkCoreSpigot plugin, int pluginId, Player player, User user, Action action)
	{
		super(plugin.getServer().createInventory(null, 54, ChatColor.translateAlternateColorCodes('&',
			Message.getMessage("NetworkCore", "Plugin Message Section List Menu", "title").getMessage(user))));
		this.plugin = plugin;
		this.pluginId = pluginId;
		this.player = player;
		this.user = user;
		this.action = action;
		this.pageForward = 7;
	}

	@Override
	public void afterFill()
	{
		ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.RED, 1);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(
			ChatColor.translateAlternateColorCodes('&', Message.getMessage("NetworkCore", "Menu", "back-item-name").getMessage(this.user)));
		item.setItemMeta(meta);
		this.addChestItem(this.getRelativeSlotLastRow(8), new ChestItem(item)
		{

			@Override
			public void onInventoryClick(InventoryClickEvent e)
			{
				AdminPluginMessageSectionListChestMenu.this.action.onAction();
			}
		});
	}

	@Override
	public List<String> getSectionItemLore(MessageSection section)
	{
		List<String> lore = Lists.newArrayList();
		for (String arg : Message.getMessage("NetworkCore", "Plugin Message Section List Menu", "section-item-lore").getMessage(this.user)
			.split("\\|"))
		{
			lore.addAll(TextUtil.splitMessage(ChatColor.translateAlternateColorCodes('&',
				arg.replace("%description%", section.getDescription() == null ? "N/A" : section.getDescription()).replace("%message-count%",
					section.getMessages().size() + "")),
				30));
		}
		return lore;
	}

	@Override
	public String getSectionItemName(MessageSection section)
	{
		return ChatColor.translateAlternateColorCodes('&',
			Message.getMessage("NetworkCore", "Plugin Message Section List Menu", "section-item-name").getMessage(this.user)
				.replace("%section%", section.getName()));
	}

	@Override
	public List<MessageSection> getSectionList()
	{
		return ListUtil.getSubList(NetworkPlugin.getPlugin(this.pluginId).getMessageSections(), this.page - 1, this.LIMIT);
	}

	@Override
	public int getTotalChestItems()
	{
		return NetworkPlugin.getPlugin(this.pluginId).getMessageSections().size();
	}

	@Override
	public void onSelection(int sectionId)
	{
		Action action = new ActionOpenChestMenu(this.player, this.chestMenu);
		ChestMenu chestMenu = new AdminMessageSectionMessageListChestMenu(this.plugin, sectionId, this.player, this.user, action);
		chestMenu.fill();
		this.plugin.getChestMenuContainer().openChestMenu(this.player, chestMenu);
	}
}
