package net.rieksen.networkcore.spigot.chestmenu;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;

import net.md_5.bungee.api.ChatColor;
import net.rieksen.networkcore.core.message.Language;
import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.message.MessageTranslation;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.util.Action;
import net.rieksen.networkcore.core.util.TextUtil;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.chatinput.ChatInput;
import net.rieksen.networkcore.spigot.chatinput.MessageTranslationChatInput;
import net.rieksen.networkcore.spigot.material.ItemColor;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

@Deprecated
public class MessageTranslationManagementChestMenu extends ChestMenu
{

	private final static int		SLOT_INFO	= 13;
	private final static int		SLOT_EDIT	= 30;
	private final static int		SLOT_DELETE	= 32;
	private final static int		SLOT_BACK	= 53;
	private final NetworkCoreSpigot	plugin;
	private final Player			player;
	private final User				user;
	private final int				messageId;
	private final int				languageId;
	private final Action			action;

	public MessageTranslationManagementChestMenu(NetworkCoreSpigot plugin, Player player, User user, int messageId, int languageId,
		Action action)
	{
		super(plugin.getServer().createInventory(null, 54, ChatColor.translateAlternateColorCodes('&',
			Message.getMessage("NetworkCore", "Message Translation Management Menu", "title").getMessage(user))));
		this.plugin = plugin;
		this.player = player;
		this.user = user;
		this.messageId = messageId;
		this.languageId = languageId;
		this.action = action;
	}

	@Override
	public void fill()
	{
		Language language = Language.getLanguage(this.languageId);
		Message message = Message.getMessage(this.messageId);
		MessageTranslation translation = message.getTranslation(this.languageId);
		{
			String itemName =
				Message.getMessage("NetworkCore", "Message Translation List Menu", "translation-item-name").getMessage(this.user);
			String[] itemLore = Message.getMessage("NetworkCore", "Message Translation List Menu", "translation-item-lore")
				.getMessage(this.user).split("\\|");
			ItemStack item = new ItemStack(Material.NETHER_STAR);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', itemName.replace("%language%", language.getName())));
			List<String> lore = Lists.newArrayList();
			for (String arg : itemLore)
			{
				lore.addAll(TextUtil
					.splitMessage(ChatColor.translateAlternateColorCodes('&', arg.replace("%message%", translation.getMessage())), 30));
			}
			meta.setLore(lore);
			item.setItemMeta(meta);
			this.addChestItem(MessageTranslationManagementChestMenu.SLOT_INFO, new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{}
			});
		}
		{
			ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.LIME, 1);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
				Message.getMessage("NetworkCore", "Message Translation Management Menu", "edit-item-name").getMessage(this.user)));
			item.setItemMeta(meta);
			this.addChestItem(MessageTranslationManagementChestMenu.SLOT_EDIT, new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					MessageTranslationManagementChestMenu.this.player.closeInventory();
					ChatInput chatInput = new MessageTranslationChatInput(MessageTranslationManagementChestMenu.this.plugin,
						MessageTranslationManagementChestMenu.this.messageId, MessageTranslationManagementChestMenu.this.player)
					{

						@Override
						public void onCancel()
						{
							MessageTranslationManagementChestMenu.this.plugin.getChestMenuContainer().openChestMenu(
								MessageTranslationManagementChestMenu.this.player, MessageTranslationManagementChestMenu.this.chestMenu);
						}

						@Override
						public void onDone(String text)
						{
							Message message = Message.getMessage(MessageTranslationManagementChestMenu.this.messageId);
							MessageTranslation translation = message.getTranslation(MessageTranslationManagementChestMenu.this.languageId);
							translation.changeMessage(text);
							MessageTranslationManagementChestMenu.this.chestMenu.fill();
							MessageTranslationManagementChestMenu.this.plugin.getChestMenuContainer().openChestMenu(
								MessageTranslationManagementChestMenu.this.player, MessageTranslationManagementChestMenu.this.chestMenu);
						}
					};
					MessageTranslationManagementChestMenu.this.plugin.getChatInputContainer()
						.openChatInput(MessageTranslationManagementChestMenu.this.player, chatInput);
				}
			});
		}
		{
			ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.RED, 1);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
				Message.getMessage("NetworkCore", "Message Translation Management Menu", "delete-item-name").getMessage(this.user)));
			item.setItemMeta(meta);
			this.addChestItem(MessageTranslationManagementChestMenu.SLOT_DELETE, new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					Message message = Message.getMessage(MessageTranslationManagementChestMenu.this.messageId);
					/*
					 * if (message.getLanguageId() ==
					 * MessageTranslationManagementChestMenu.this.languageId) {
					 * MessageTranslationManagementChestMenu.this.player.
					 * sendMessage(ChatColor.translateAlternateColorCodes('&',
					 * Message.getMessage("NetworkCore",
					 * "Message Translation Management Menu",
					 * "delete-default-language")
					 * .getMessage(MessageTranslationManagementChestMenu.this.
					 * user))); return; }
					 */
					message.deleteTranslation(MessageTranslationManagementChestMenu.this.languageId);
					MessageTranslationManagementChestMenu.this.action.onAction();
				}
			});
		}
		{
			ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.RED, 1);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
				Message.getMessage("NetworkCore", "Menu", "back-item-name").getMessage(this.user)));
			item.setItemMeta(meta);
			this.addChestItem(MessageTranslationManagementChestMenu.SLOT_BACK, new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					MessageTranslationManagementChestMenu.this.action.onAction();
				}
			});
		}
	}
}
