package net.rieksen.networkcore.spigot.chestmenu;

import java.util.List;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;

import net.md_5.bungee.api.ChatColor;
import net.rieksen.networkcore.core.message.Language;
import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.message.MessageTranslation;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.util.TextUtil;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.material.ItemColor;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

public abstract class AMessageTranslationListChestMenu extends PageChestMenu
{

	protected final NetworkCoreSpigot	plugin;
	protected final User				user;

	public AMessageTranslationListChestMenu(NetworkCoreSpigot plugin, Inventory inventory, User user)
	{
		super(inventory);
		this.plugin = plugin;
		this.user = user;
	}

	@Override
	public List<ChestItem> getChestItems()
	{
		List<ChestItem> list = Lists.newArrayList();
		String itemName = Message.getMessage("NetworkCore", "Message Translation List Menu", "translation-item-name").getMessage(this.user);
		String[] itemLore =
			Message.getMessage("NetworkCore", "Message Translation List Menu", "translation-item-lore").getMessage(this.user).split("\\|");
		for (final MessageTranslation message : this.getTranslationList())
		{
			ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.LIME, 1);
			ItemMeta meta = item.getItemMeta();
			Language language = Language.getLanguage(message.getLanguageId());
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', itemName.replace("%language%", language.getName())));
			List<String> lore = Lists.newArrayList();
			for (String arg : itemLore)
			{
				lore.addAll(
					TextUtil.splitMessage(ChatColor.translateAlternateColorCodes('&', arg.replace("%message%", message.getMessage())), 30));
			}
			meta.setLore(lore);
			item.setItemMeta(meta);
			list.add(new ChestItem(item)
			{

				private int	messageId	= message.getMessageId();
				private int	languageId	= message.getLanguageId();

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					AMessageTranslationListChestMenu.this.onSelection(this.messageId, this.languageId);
				}
			});
		}
		return list;
	}

	public abstract List<MessageTranslation> getTranslationList();

	public abstract void onSelection(int messageId, int languageId);
}
