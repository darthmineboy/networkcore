package net.rieksen.networkcore.spigot.chestmenu;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;

import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.spigot.material.ItemColor;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

public abstract class APluginListChestMenu extends PageChestMenu
{

	private final User user;

	public APluginListChestMenu(Inventory inventory, User user)
	{
		super(inventory);
		this.user = user;
	}

	@Override
	public List<ChestItem> getChestItems()
	{
		List<ChestItem> list = Lists.newArrayList();
		for (final NetworkPlugin plugin : this.getPluginList())
		{
			ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.LIME, 1);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(
				ChatColor.translateAlternateColorCodes('&', Message.getMessage("NetworkCore", "Plugin List Menu", "plugin-item-name")
					.getMessage(this.user).replace("%plugin%", plugin.getName())));
			item.setItemMeta(meta);
			list.add(new ChestItem(item)
			{

				private int pluginId = plugin.getPluginId();

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					APluginListChestMenu.this.onSelection(this.pluginId);
				}
			});
		}
		return list;
	}

	public abstract List<NetworkPlugin> getPluginList();

	public abstract void onSelection(int pluginId);
}
