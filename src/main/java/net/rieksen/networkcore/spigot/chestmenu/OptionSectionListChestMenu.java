package net.rieksen.networkcore.spigot.chestmenu;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.rieksen.networkcore.core.option.OptionSection;
import net.rieksen.networkcore.core.util.LoreUtil;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.material.ItemColor;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

public abstract class OptionSectionListChestMenu extends AdvancedPageChestMenu
{

	public OptionSectionListChestMenu(NetworkCoreSpigot plugin)
	{
		super(plugin.getServer().createInventory(null, 54, ChatColor.translateAlternateColorCodes('&', "&4Option Section List")));
	}

	@Override
	public List<ChestItem> getChestItems()
	{
		List<OptionSection> sections = this.getSections();
		if (sections == null) { return null; }
		List<ChestItem> list = new ArrayList<>();
		for (OptionSection section : sections)
		{
			ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.LIME, 1);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&6&l" + section.getName()));
			meta.setLore(LoreUtil
				.createLore("&eOptions &8: &f" + section.getOptions().size() + "\n&eDescription &8: &f" + section.getDescription()));
			item.setItemMeta(meta);
			final int sectionId = section.getSectionId();
			list.add(new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					OptionSectionListChestMenu.this.onSelect(sectionId);
				}
			});
		}
		return list;
	}

	public abstract List<OptionSection> getSections();

	public abstract void onSelect(int sectionId);
}
