package net.rieksen.networkcore.spigot.chestmenu;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.Validate;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.option.Option;
import net.rieksen.networkcore.core.option.OptionImpl;
import net.rieksen.networkcore.core.option.OptionSection;
import net.rieksen.networkcore.core.option.OptionSectionImpl;
import net.rieksen.networkcore.core.option.OptionType;
import net.rieksen.networkcore.core.option.OptionValue;
import net.rieksen.networkcore.core.option.pojo.OptionValuePojo;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.core.server.Server;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.util.Action;
import net.rieksen.networkcore.core.util.LoreUtil;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.chatinput.ChatInput;
import net.rieksen.networkcore.spigot.chatinput.OptionServerSettingManagementChatInput;
import net.rieksen.networkcore.spigot.material.ItemColor;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

@Deprecated
public class OptionManagementChestMenu extends AdvancedPageChestMenu
{

	private final static int		SLOT_INFO	= 13;
	private final static int		SLOT_BACK	= 53;
	private final NetworkCoreSpigot	plugin;
	private final int				optionId;
	private final Player			player;
	private final Action			backAction;

	public OptionManagementChestMenu(NetworkCoreSpigot plugin, int optionId, Player player, Action backAction)
	{
		super(plugin.getServer().createInventory(null, 54, ChatColor.translateAlternateColorCodes('&', "&4Option Management")));
		Validate.notNull(optionId);
		Validate.notNull(player);
		this.plugin = plugin;
		this.optionId = optionId;
		this.player = player;
		this.backAction = backAction;
		this.pageForwardSlot = 52;
		this.contentSlots = new int[] { 28, 29, 30, 31, 32, 33, 34, 37, 38, 39, 40, 41, 42, 43 };
	}

	@Override
	public void fill()
	{
		super.fill();
		User user = User.getUser(this.player);
		Option option = OptionImpl.getOption(this.optionId);
		OptionSection section = OptionSectionImpl.getSection(option.getSectionId());
		NetworkPlugin plugin = NetworkPlugin.getPlugin(section.getPluginId());
		{
			ItemStack item = new ItemStack(Material.NETHER_STAR);
			ItemMeta meta = item.getItemMeta();
			// configurable
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&6&l" + option.getName()));
			// configurable
			String description = option.getDescription() == null ? "N/A" : option.getDescription();
			meta.setLore(
				LoreUtil.createLore("&ePlugin &8: &f" + plugin.getName() + "\n&eSection &8: &f" + section.getName() + "\n&eName &8: &f"
					+ option.getName() + "\n&eType &8: &f" + option.getType().toString() + "\n&eDescription &8: &f" + description));
			item.setItemMeta(meta);
			this.addChestItem(OptionManagementChestMenu.SLOT_INFO, new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{}
			});
		}
		ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.RED, 1);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(
			ChatColor.translateAlternateColorCodes('&', Message.getMessage("NetworkCore", "Menu", "back-item-name").getMessage(user)));
		item.setItemMeta(meta);
		this.addChestItem(OptionManagementChestMenu.SLOT_BACK, new ChestItem(item)
		{

			@Override
			public void onInventoryClick(InventoryClickEvent e)
			{
				OptionManagementChestMenu.this.backAction.onAction();
			}
		});
	}

	@Override
	public List<ChestItem> getChestItems()
	{
		Option option = OptionImpl.getOption(this.optionId);
		List<Integer> servers = option.getServersWithSettings();
		servers.add(0, null);
		List<ChestItem> list = new ArrayList<>();
		// add pagination to option server list
		for (final Integer serverId : servers)
		{
			short damage;
			if (serverId == null)
			{
				damage = 1;
			} else
			{
				damage = 5;
			}
			if (option.getType() == OptionType.BOOLEAN)
			{
				if (option.getBoolean(serverId))
				{
					damage = 5;
				} else
				{
					damage = 14;
				}
			}
			ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.ofDamage(damage), 1);
			ItemMeta meta = item.getItemMeta();
			String displayName = serverId == null ? "Default" : "SERVER NOT FOUND #" + serverId;
			if (serverId != null)
			{
				Server server = Server.getServer(serverId);
				if (server != null)
				{
					displayName = server.getName();
				}
			}
			// customize
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&6&l" + displayName));
			String value;
			if (option.getType() != OptionType.LIST)
			{
				List<OptionValue> values = option.getApplyingValues(serverId);
				if (values.isEmpty())
				{
					value = "No values found";
				} else
				{
					value = values.get(0).getValue();
				}
			} else
			{
				List<OptionValue> values = option.getApplyingValues(serverId);
				StringBuilder str = new StringBuilder();
				for (OptionValue optionValue : values)
				{
					str.append(optionValue.getValue() + "\n");
				}
				value = str.toString();
			}
			List<String> lore;
			if (option.getType() == OptionType.BOOLEAN)
			{
				lore = LoreUtil.createLore("Click to toggle");
			} else
			{
				lore = LoreUtil.createLore("&eClick to edit value\n&eCurrent value &8: &f" + value);
			}
			meta.setLore(lore);
			item.setItemMeta(meta);
			list.add(new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					Option option = OptionImpl.getOption(OptionManagementChestMenu.this.optionId);
					if (option.getType() == OptionType.BOOLEAN)
					{
						List<OptionValue> values = option.getApplyingValues(serverId);
						if (values.isEmpty())
						{
							option.createValue(
								new OptionValuePojo(0, OptionManagementChestMenu.this.optionId, serverId, 0, Boolean.toString(false)));
						} else
						{
							OptionValue value = values.get(0);
							value.changeValue(Boolean.toString(!option.getBoolean(serverId)));
						}
						OptionManagementChestMenu.this.fill();
						OptionManagementChestMenu.this.updateInventoryViewers();
						return;
					}
					ChatInput chatInput = new OptionServerSettingManagementChatInput(OptionManagementChestMenu.this.plugin,
						OptionManagementChestMenu.this.optionId, serverId, OptionManagementChestMenu.this.player,
						new ActionOpenChestMenu(OptionManagementChestMenu.this.player, OptionManagementChestMenu.this.chestMenu));
					OptionManagementChestMenu.this.player.closeInventory();
					OptionManagementChestMenu.this.plugin.getChatInputContainer().openChatInput(OptionManagementChestMenu.this.player,
						chatInput);
				}
			});
		}
		return list;
	}

	@Override
	public int getTotalChestItems()
	{
		Option option = OptionImpl.getOption(this.optionId);
		List<Integer> servers = option.getServersWithSettings();
		if (!servers.contains(null))
		{
			servers.add(0, null);
		}
		return servers.size();
	}
}
