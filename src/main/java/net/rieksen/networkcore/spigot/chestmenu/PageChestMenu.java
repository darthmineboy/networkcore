package net.rieksen.networkcore.spigot.chestmenu;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.ItemMeta;

import net.rieksen.networkcore.spigot.material.ItemColor;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

public abstract class PageChestMenu extends ChestMenu
{

	protected final int	LIMIT;
	protected int		page		= 1;
	protected int		pageBack	= 0;
	protected int		pageRefresh	= 4;
	protected int		pageForward	= 8;

	public PageChestMenu(Inventory inventory)
	{
		super(inventory);
		if (inventory.getSize() < 18) { throw new IllegalStateException("Inventory must be at least 18 slots big!"); }
		this.LIMIT = inventory.getSize() - 9;
	}

	/**
	 * You could decide to add a few items in the bottom row
	 */
	public void afterFill()
	{}

	@Override
	public final void fill()
	{
		this.inventory.clear();
		this.chestItems.clear();
		int i = 0;
		List<ChestItem> chestItems = this.getChestItems();
		if (chestItems != null)
		{
			for (ChestItem chestItem : chestItems)
			{
				this.addChestItem(i, chestItem);
				i++;
				if (i == this.LIMIT)
				{
					break;
				}
			}
		}
		{
			ItemStack item = ItemStackUtil.createBanner(ItemColor.GREEN, this.page - 1);
			BannerMeta meta = (BannerMeta) item.getItemMeta();
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&6&lPage Back"));
			meta.addPattern(new Pattern(DyeColor.YELLOW, PatternType.RHOMBUS_MIDDLE));
			meta.addPattern(new Pattern(DyeColor.GREEN, PatternType.HALF_VERTICAL_MIRROR));
			meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
			item.setItemMeta(meta);
			this.addChestItem(this.getRelativeSlotLastRow(this.pageBack), new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					if (PageChestMenu.this.page > 1)
					{
						PageChestMenu.this.page--;
						PageChestMenu.this.fill();
						((Player) e.getWhoClicked()).updateInventory();
					}
				}
			});
		}
		{
			ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.BLUE, 1);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&6&lRefresh"));
			item.setItemMeta(meta);
			this.addChestItem(this.getRelativeSlotLastRow(this.pageRefresh), new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					PageChestMenu.this.fill();
					((Player) e.getWhoClicked()).updateInventory();
				}
			});
		}
		{
			ItemStack item = ItemStackUtil.createBanner(ItemColor.GREEN, this.getMaxPageCount() - this.page);
			BannerMeta meta = (BannerMeta) item.getItemMeta();
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&6&lPage Forward"));
			meta.addPattern(new Pattern(DyeColor.YELLOW, PatternType.RHOMBUS_MIDDLE));
			meta.addPattern(new Pattern(DyeColor.GREEN, PatternType.HALF_VERTICAL));
			meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
			item.setItemMeta(meta);
			this.addChestItem(this.getRelativeSlotLastRow(this.pageForward), new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					if (PageChestMenu.this.page < PageChestMenu.this.getMaxPageCount())
					{
						PageChestMenu.this.page++;
						PageChestMenu.this.fill();
						((Player) e.getWhoClicked()).updateInventory();
					}
				}
			});
		}
		this.afterFill();
	}

	/**
	 * Page 1 is the first page
	 * 
	 * @return
	 */
	public abstract List<ChestItem> getChestItems();

	/**
	 * Page 1 is the first page
	 * 
	 * @return
	 */
	public final int getMaxPageCount()
	{
		int total = this.getTotalChestItems();
		if (total == 0) { return 1; }
		return total % this.LIMIT == 0 ? total / this.LIMIT : total / this.LIMIT + 1;
	}

	public final int getRelativeSlotLastRow(int slot)
	{
		int inventorySize = this.inventory.getSize();
		return inventorySize - (9 - slot);
	}

	public abstract int getTotalChestItems();
}
