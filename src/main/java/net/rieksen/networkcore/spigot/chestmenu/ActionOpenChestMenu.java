package net.rieksen.networkcore.spigot.chestmenu;

import net.rieksen.networkcore.core.util.Action;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;

import org.bukkit.entity.Player;

public class ActionOpenChestMenu extends Action
{

	private final Player	player;
	private final ChestMenu	chestMenu;

	public ActionOpenChestMenu(Player player, ChestMenu chestMenu)
	{
		this.player = player;
		this.chestMenu = chestMenu;
	}

	/**
	 * Fills the ChestMenu, and opens it for the player.
	 */
	@Override
	public void onAction()
	{
		chestMenu.fill();
		NetworkCoreSpigot.getInstance().getChestMenuContainer().openChestMenu(player, chestMenu);
	}
}
