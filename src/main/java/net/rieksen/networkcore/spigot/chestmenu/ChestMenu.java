package net.rieksen.networkcore.spigot.chestmenu;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;

import com.google.common.collect.Maps;

public abstract class ChestMenu
{

	protected ChestMenu					chestMenu	= this;
	protected Inventory					inventory;
	protected Map<Integer, ChestItem>	chestItems	= Maps.newHashMap();

	public ChestMenu(Inventory inventory)
	{
		this.inventory = inventory;
	}

	public void onFailOpen()
	{}

	public void onOpen()
	{}

	public void onExit()
	{}

	public abstract void fill();

	public void onInventoryClick(InventoryClickEvent e)
	{
		Inventory inventory = e.getClickedInventory();
		if (inventory == null) { return; }
		if (inventory.getType() != InventoryType.CHEST) { return; }
		ChestItem chestItem = chestItems.get(e.getSlot());
		if (chestItem != null)
		{
			chestItem.onInventoryClick(e);
		}
	}

	public void addChestItem(int slot, ChestItem chestItem)
	{
		chestItems.put(slot, chestItem);
		inventory.setItem(slot, chestItem.getItemStack());
	}

	/**
	 * Clears inventory and sets all the chestItems in the inventory.
	 */
	public void updateInventory()
	{
		inventory.clear();
		for (Entry<Integer, ChestItem> entry : chestItems.entrySet())
		{
			inventory.setItem(entry.getKey(), entry.getValue().getItemStack());
		}
	}

	public final Inventory getInventory()
	{
		return inventory;
	}

	/**
	 * Updates the inventory for all viewers of this inventory.
	 */
	public void updateInventoryViewers()
	{
		for (Player player : getViewers())
		{
			player.updateInventory();
		}
	}

	/**
	 * Get the players viewing this inventory.
	 * 
	 * @return
	 */
	public Collection<Player> getViewers()
	{
		Collection<Player> players = new HashSet<>();
		for (HumanEntity human : inventory.getViewers())
		{
			if (!(human instanceof Player))
			{
				continue;
			}
			Player player = (Player) human;
			players.add(player);
		}
		return players;
	}
}
