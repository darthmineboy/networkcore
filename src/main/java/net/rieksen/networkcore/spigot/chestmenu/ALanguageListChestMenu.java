package net.rieksen.networkcore.spigot.chestmenu;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;

import net.rieksen.networkcore.core.message.Language;
import net.rieksen.networkcore.spigot.material.ItemColor;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

public abstract class ALanguageListChestMenu extends PageChestMenu
{

	public ALanguageListChestMenu(Inventory inventory)
	{
		super(inventory);
	}

	@Override
	public List<ChestItem> getChestItems()
	{
		List<ChestItem> list = Lists.newArrayList();
		for (final Language language : this.getLanguageList())
		{
			ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.LIME, 1);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&6&l" + language.getName()));
			item.setItemMeta(meta);
			list.add(new ChestItem(item)
			{

				private int languageId = language.getLanguageId();

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					ALanguageListChestMenu.this.onSelection(this.languageId);
				}
			});
		}
		return list;
	}

	public abstract List<Language> getLanguageList();

	public abstract void onSelection(int languageId);
}
