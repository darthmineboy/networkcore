package net.rieksen.networkcore.spigot.chestmenu;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.md_5.bungee.api.ChatColor;
import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.message.MessageSectionImpl;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.util.Action;
import net.rieksen.networkcore.core.util.ListUtil;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.material.ItemColor;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

/**
 * Lists all the messages in a message section.
 * 
 * @author Darthmineboy
 */
@Deprecated
public class AdminMessageSectionMessageListChestMenu extends AMessageListChestMenu
{

	private final NetworkCoreSpigot	plugin;
	private final int				sectionId;
	private final Player			player;
	private final User				user;
	private final Action			action;

	public AdminMessageSectionMessageListChestMenu(NetworkCoreSpigot plugin, int sectionId, Player player, User user, Action action)
	{
		super(plugin, plugin.getServer().createInventory(null, 54, ChatColor.translateAlternateColorCodes('&',
			Message.getMessage("NetworkCore", "Admin Message Section Message List Menu", "title").getMessage(user))), user);
		this.plugin = plugin;
		this.sectionId = sectionId;
		this.player = player;
		this.user = user;
		this.action = action;
		this.pageForward = 7;
	}

	@Override
	public void afterFill()
	{
		ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.RED, 1);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(
			ChatColor.translateAlternateColorCodes('&', Message.getMessage("NetworkCore", "Menu", "back-item-name").getMessage(this.user)));
		item.setItemMeta(meta);
		this.addChestItem(this.getRelativeSlotLastRow(8), new ChestItem(item)
		{

			@Override
			public void onInventoryClick(InventoryClickEvent e)
			{
				AdminMessageSectionMessageListChestMenu.this.action.onAction();
			}
		});
	}

	@Override
	public List<Message> getMessageList()
	{
		return ListUtil.getSubList(MessageSectionImpl.getSection(this.sectionId).getMessages(), this.page - 1, this.LIMIT);
	}

	@Override
	public int getTotalChestItems()
	{
		return MessageSectionImpl.getSection(this.sectionId).getMessages().size();
	}

	@Override
	public void onSelection(int messageId)
	{
		Action action = new ActionOpenChestMenu(this.player, this.chestMenu);
		ChestMenu chestMenu = new MessageManagementChestMenu(this.plugin, messageId, this.player, this.user, action);
		chestMenu.fill();
		this.plugin.getChestMenuContainer().openChestMenu(this.player, chestMenu);
	}
}
