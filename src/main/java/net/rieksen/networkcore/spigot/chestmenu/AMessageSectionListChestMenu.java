package net.rieksen.networkcore.spigot.chestmenu;

import java.util.List;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;

import net.rieksen.networkcore.core.message.MessageSection;
import net.rieksen.networkcore.spigot.material.ItemColor;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

public abstract class AMessageSectionListChestMenu extends PageChestMenu
{

	public AMessageSectionListChestMenu(Inventory inventory)
	{
		super(inventory);
	}

	@Override
	public List<ChestItem> getChestItems()
	{
		List<ChestItem> list = Lists.newArrayList();
		for (final MessageSection section : this.getSectionList())
		{
			ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.LIME, 1);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(this.getSectionItemName(section));
			meta.setLore(this.getSectionItemLore(section));
			item.setItemMeta(meta);
			list.add(new ChestItem(item)
			{

				private int sectionId = section.getSectionId();

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					AMessageSectionListChestMenu.this.onSelection(this.sectionId);
				}
			});
		}
		return list;
	}

	public abstract List<String> getSectionItemLore(MessageSection section);

	public abstract String getSectionItemName(MessageSection section);

	public abstract List<MessageSection> getSectionList();

	public abstract void onSelection(int sectionId);
}
