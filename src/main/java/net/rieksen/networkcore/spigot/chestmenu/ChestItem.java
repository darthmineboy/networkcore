package net.rieksen.networkcore.spigot.chestmenu;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public abstract class ChestItem
{

	protected ItemStack	itemStack;

	public ChestItem(ItemStack itemStack)
	{
		this.itemStack = itemStack;
	}

	public ItemStack getItemStack()
	{
		return itemStack;
	}

	public abstract void onInventoryClick(InventoryClickEvent e);
}
