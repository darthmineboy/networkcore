package net.rieksen.networkcore.spigot.chestmenu;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.md_5.bungee.api.ChatColor;
import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.util.Action;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.material.ItemColor;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

public class AdminPluginListChestMenu extends APluginListChestMenu
{

	private final NetworkCoreSpigot	plugin;
	private final Player			player;
	private final User				user;
	private final Action			action;

	public AdminPluginListChestMenu(NetworkCoreSpigot plugin, Player player, User user, Action action)
	{
		super(plugin.getServer().createInventory(null, 54, ChatColor.translateAlternateColorCodes('&',
			Message.getMessage("NetworkCore", "Admin Plugin List Menu", "title").getMessage(user))), user);
		this.plugin = plugin;
		this.player = player;
		this.user = user;
		this.action = action;
		this.pageForward = 7;
	}

	@Override
	public void afterFill()
	{
		ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.RED, 1);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(
			ChatColor.translateAlternateColorCodes('&', Message.getMessage("NetworkCore", "Menu", "back-item-name").getMessage(this.user)));
		item.setItemMeta(meta);
		this.addChestItem(this.getRelativeSlotLastRow(8), new ChestItem(item)
		{

			@Override
			public void onInventoryClick(InventoryClickEvent e)
			{
				AdminPluginListChestMenu.this.action.onAction();
			}
		});
	};

	@Override
	public List<NetworkPlugin> getPluginList()
	{
		return this.plugin.getPluginModule().getPlugins((this.page - 1) * this.LIMIT, this.LIMIT);
	}

	@Override
	public int getTotalChestItems()
	{
		return this.plugin.getPluginModule().getPlugins().size();
	}

	@Override
	public void onSelection(int pluginId)
	{
		Action action = new ActionOpenChestMenu(this.player, this.chestMenu);
		ChestMenu chestMenu = new PluginManagementChestMenu(this.plugin, pluginId, this.player, this.user, action);
		chestMenu.fill();
		this.plugin.getChestMenuContainer().openChestMenu(this.player, chestMenu);
	}
}
