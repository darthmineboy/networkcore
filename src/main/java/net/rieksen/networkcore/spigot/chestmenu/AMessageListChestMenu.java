package net.rieksen.networkcore.spigot.chestmenu;

import java.util.List;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;

import net.md_5.bungee.api.ChatColor;
import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.message.Language;
import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.message.MessageTranslation;
import net.rieksen.networkcore.core.message.MessageVariable;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.util.TextUtil;
import net.rieksen.networkcore.spigot.material.ItemColor;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

public abstract class AMessageListChestMenu extends PageChestMenu
{

	protected final NetworkCore	plugin;
	protected final User		user;

	public AMessageListChestMenu(NetworkCore plugin, Inventory inventory, User user)
	{
		super(inventory);
		this.plugin = plugin;
		this.user = user;
	}

	@Override
	public List<ChestItem> getChestItems()
	{
		List<ChestItem> list = Lists.newArrayList();
		String descriptionHeader = Message.getMessage("NetworkCore", "Message List Menu", "description-header").getMessage(this.user);
		String defaultMessageHeader =
			Message.getMessage("NetworkCore", "Message List Menu", "default-message-header").getMessage(this.user);
		String variablesHeader = Message.getMessage("NetworkCore", "Message List Menu", "variables-header").getMessage(this.user);
		String variableString = Message.getMessage("NetworkCore", "Message List Menu", "variable").getMessage(this.user);
		String translationsHeader = Message.getMessage("NetworkCore", "Message List Menu", "translations-header").getMessage(this.user);
		String translationString = Message.getMessage("NetworkCore", "Message List Menu", "translation").getMessage(this.user);
		for (final Message message : this.getMessageList())
		{
			ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.LIME, 1);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(
				ChatColor.translateAlternateColorCodes('&', Message.getMessage("NetworkCore", "Message List Menu", "message-item-name")
					.getMessage(this.user).replace("%name%", message.getName())));
			List<String> lore = Lists.newArrayList();
			lore.add(ChatColor.translateAlternateColorCodes('&', descriptionHeader));
			if (message.getDescription() != null)
			{
				for (String a : TextUtil.splitMessage(ChatColor.translateAlternateColorCodes('&', message.getDescription()), 30))
				{
					lore.add(ChatColor.translateAlternateColorCodes('&', "&f" + a));
				}
			} else
			{
				lore.add(ChatColor.translateAlternateColorCodes('&', "&fN/A"));
			}
			lore.add(ChatColor.translateAlternateColorCodes('&', defaultMessageHeader));
			for (String a : TextUtil.splitMessage(ChatColor.translateAlternateColorCodes('&', message.getMessage(this.user)), 30))
			{
				lore.add(ChatColor.translateAlternateColorCodes('&', "&f" + a));
			}
			lore.add(ChatColor.translateAlternateColorCodes('&', variablesHeader));
			for (MessageVariable variable : message.getVariables())
			{
				lore.add(ChatColor.translateAlternateColorCodes('&',
					variableString.replace("%description%", variable.getDescription() == null ? "" : variable.getDescription())
						.replace("%name%", variable.getName())));
			}
			lore.add(ChatColor.translateAlternateColorCodes('&', translationsHeader));
			for (MessageTranslation translation : message.getTranslations())
			{
				Language language = Language.getLanguage(translation.getLanguageId());
				lore.addAll(TextUtil.splitMessage(ChatColor.translateAlternateColorCodes('&',
					translationString.replace("%language%", language.getName()).replace("%message%", translation.getMessage())), 30));
			}
			meta.setLore(lore);
			item.setItemMeta(meta);
			list.add(new ChestItem(item)
			{

				private int messageId = message.getMessageId();

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					AMessageListChestMenu.this.onSelection(this.messageId);
				}
			});
		}
		return list;
	}

	public abstract List<Message> getMessageList();

	public abstract void onSelection(int messageId);
}
