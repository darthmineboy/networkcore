package net.rieksen.networkcore.spigot.chestmenu;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.md_5.bungee.api.ChatColor;
import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.util.Action;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.material.ItemColor;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

public class PluginManagementChestMenu extends ChestMenu
{

	private final static int		SLOT_INFO	= 13;
	// private final static int SLOT_MESSAGE_SECTION_LIST = 30;
	// private final static int SLOT_OPTION_SECTION_LIST = 32;
	private final static int		SLOT_BACK	= 53;
	private final NetworkCoreSpigot	plugin;
	// private final int pluginId;
	// private final Player player;
	private final User				user;
	private final Action			action;

	public PluginManagementChestMenu(NetworkCoreSpigot plugin, int pluginId, Player player, User user, Action action)
	{
		super(plugin.getServer().createInventory(null, 54, ChatColor.translateAlternateColorCodes('&',
			Message.getMessage("NetworkCore", "Plugin Management Menu", "title").getMessage(user))));
		this.plugin = plugin;
		// this.pluginId = pluginId;
		// this.player = player;
		this.user = user;
		this.action = action;
	}

	@Override
	public void fill()
	{
		{
			ItemStack item = new ItemStack(Material.NETHER_STAR);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(
				ChatColor.translateAlternateColorCodes('&', Message.getMessage("NetworkCore", "Plugin List Menu", "plugin-item-name")
					.getMessage(this.user).replace("%plugin%", this.plugin.getName())));
			item.setItemMeta(meta);
			this.addChestItem(PluginManagementChestMenu.SLOT_INFO, new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{}
			});
		}
		/*
		 * { ItemStack item = new ItemStack(Material.STAINED_GLASS_PANE, 1,
		 * (short) 5); ItemMeta meta = item.getItemMeta();
		 * meta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
		 * Message.getMessage("NetworkCore", "Plugin Management Menu",
		 * "message-section-list-item-name").getMessage(this.user)));
		 * List<String> lore = Lists.newArrayList(); for (String arg :
		 * Message.getMessage("NetworkCore", "Plugin Management Menu",
		 * "message-section-list-item-lore")
		 * .getMessage(this.user).split("\\|")) {
		 * lore.addAll(TextUtil.splitMessage(
		 * ChatColor.translateAlternateColorCodes('&',
		 * arg.replace("%message-section-count%",
		 * NetworkPlugin.getPlugin(this.pluginId).getMessageSections().size() +
		 * "")), 30)); } meta.setLore(lore); item.setItemMeta(meta);
		 * this.addChestItem(PluginManagementChestMenu.
		 * SLOT_MESSAGE_SECTION_LIST, new ChestItem(item) {
		 * @Override public void onInventoryClick(InventoryClickEvent e) {
		 * Action action = new
		 * ActionOpenChestMenu(PluginManagementChestMenu.this.player,
		 * PluginManagementChestMenu.this.chestMenu); ChestMenu chestMenu = new
		 * AdminPluginMessageSectionListChestMenu(PluginManagementChestMenu.this
		 * .plugin, PluginManagementChestMenu.this.pluginId,
		 * PluginManagementChestMenu.this.player,
		 * PluginManagementChestMenu.this.user, action); chestMenu.fill();
		 * PluginManagementChestMenu.this.plugin.getChestMenuContainer().
		 * openChestMenu(PluginManagementChestMenu.this.player, chestMenu); }
		 * }); }
		 */
		/*
		 * { ItemStack item = new ItemStack(Material.STAINED_GLASS_PANE, 1,
		 * (short) 5); ItemMeta meta = item.getItemMeta(); // customize
		 * meta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
		 * "&6&lOption Section List")); item.setItemMeta(meta);
		 * this.addChestItem(PluginManagementChestMenu.SLOT_OPTION_SECTION_LIST,
		 * new ChestItem(item) {
		 * @Override public void onInventoryClick(InventoryClickEvent e) {
		 * ChestMenu menu = new
		 * OptionSectionListForPluginChestMenu(PluginManagementChestMenu.this.
		 * plugin, PluginManagementChestMenu.this.pluginId,
		 * PluginManagementChestMenu.this.player, new
		 * ActionOpenChestMenu(PluginManagementChestMenu.this.player,
		 * PluginManagementChestMenu.this)); menu.fill();
		 * PluginManagementChestMenu.this.plugin.getChestMenuContainer().
		 * openChestMenu(PluginManagementChestMenu.this.player, menu); } }); }
		 */
		{
			ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.RED, 1);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
				Message.getMessage("NetworkCore", "Menu", "back-item-name").getMessage(this.user)));
			item.setItemMeta(meta);
			this.addChestItem(PluginManagementChestMenu.SLOT_BACK, new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					PluginManagementChestMenu.this.action.onAction();
				}
			});
		}
	}
}
