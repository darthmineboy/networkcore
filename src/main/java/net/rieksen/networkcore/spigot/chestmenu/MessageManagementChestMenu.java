package net.rieksen.networkcore.spigot.chestmenu;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;

import net.rieksen.networkcore.core.message.Language;
import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.message.MessageTranslation;
import net.rieksen.networkcore.core.message.MessageVariable;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.util.Action;
import net.rieksen.networkcore.core.util.TextUtil;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.material.ItemColor;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

@Deprecated
public class MessageManagementChestMenu extends ChestMenu
{

	private final static int		SLOT_INFO				= 13;
	private final static int		SLOT_LANGUAGE			= 29;
	private final static int		SLOT_LIST_TRANSLATIONS	= 31;
	private final static int		SLOT_ADD_TRANSLATION	= 33;
	private final static int		SLOT_BACK				= 53;
	private final NetworkCoreSpigot	plugin;
	private final int				messageId;
	private final Player			player;
	private final User				user;
	private final Action			action;

	public MessageManagementChestMenu(NetworkCoreSpigot plugin, int messageId, Player player, User user, Action action)
	{
		super(plugin.getServer().createInventory(null, 54, ChatColor.translateAlternateColorCodes('&',
			Message.getMessage("NetworkCore", "Message Management Menu", "title").getMessage(user))));
		this.plugin = plugin;
		this.messageId = messageId;
		this.player = player;
		this.user = user;
		this.action = action;
	}

	@Override
	public void fill()
	{
		Message message = Message.getMessage(this.messageId);
		// Language language = Language.getLanguage(message.getLanguageId());
		{
			String descriptionHeader = Message.getMessage("NetworkCore", "Message List Menu", "description-header").getMessage(this.user);
			String defaultMessageHeader =
				Message.getMessage("NetworkCore", "Message List Menu", "default-message-header").getMessage(this.user);
			String variablesHeader = Message.getMessage("NetworkCore", "Message List Menu", "variables-header").getMessage(this.user);
			String variableString = Message.getMessage("NetworkCore", "Message List Menu", "variable").getMessage(this.user);
			String translationsHeader = Message.getMessage("NetworkCore", "Message List Menu", "translations-header").getMessage(this.user);
			String translationString = Message.getMessage("NetworkCore", "Message List Menu", "translation").getMessage(this.user);
			ItemStack item = new ItemStack(Material.NETHER_STAR);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(
				ChatColor.translateAlternateColorCodes('&', Message.getMessage("NetworkCore", "Message List Menu", "message-item-name")
					.getMessage(this.user).replace("%name%", message.getName())));
			List<String> lore = Lists.newArrayList();
			lore.add(ChatColor.translateAlternateColorCodes('&', descriptionHeader));
			if (message.getDescription() != null)
			{
				for (String a : TextUtil.splitMessage(ChatColor.translateAlternateColorCodes('&', message.getDescription()), 30))
				{
					lore.add(ChatColor.translateAlternateColorCodes('&', "&f" + a));
				}
			} else
			{
				lore.add(ChatColor.translateAlternateColorCodes('&', "&fN/A"));
			}
			lore.add(ChatColor.translateAlternateColorCodes('&', defaultMessageHeader));
			String defaultMessage = "Translation for default language not found";
			try
			{
				defaultMessage = message.getMessage((User) null);
			} catch (Exception e)
			{
				// No default message
			}
			for (String a : TextUtil.splitMessage(ChatColor.translateAlternateColorCodes('&', defaultMessage), 30))
			{
				lore.add(ChatColor.translateAlternateColorCodes('&', "&f" + a));
			}
			lore.add(ChatColor.translateAlternateColorCodes('&', variablesHeader));
			for (MessageVariable variable : message.getVariables())
			{
				lore.add(ChatColor.translateAlternateColorCodes('&',
					variableString.replace("%description%", variable.getDescription() == null ? "" : variable.getDescription())
						.replace("%name%", variable.getName())));
			}
			lore.add(ChatColor.translateAlternateColorCodes('&', translationsHeader));
			for (MessageTranslation translation : message.getTranslations())
			{
				Language translationLanguage = Language.getLanguage(translation.getLanguageId());
				lore.addAll(TextUtil.splitMessage(ChatColor.translateAlternateColorCodes('&',
					translationString.replace("%language%", translationLanguage.getName()).replace("%message%", translation.getMessage())),
					30));
			}
			meta.setLore(lore);
			item.setItemMeta(meta);
			this.addChestItem(MessageManagementChestMenu.SLOT_INFO, new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{}
			});
		}
		{
			ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.LIME, 1);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
				Message.getMessage("NetworkCore", "Message Management Menu", "change-language-item-name").getMessage(this.user)));
			List<String> lore = Lists.newArrayList();
			String[] args = Message.getMessage("NetworkCore", "Message Management Menu", "change-language-item-lore").getMessage(this.user)
				.split("\\|");
			for (String arg : args)
			{
				lore.addAll(TextUtil.splitMessage(ChatColor.translateAlternateColorCodes('&',
					arg/* .replace("%language%", language.getName()) */), 30));
			}
			meta.setLore(lore);
			item.setItemMeta(meta);
			this.addChestItem(MessageManagementChestMenu.SLOT_LANGUAGE, new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					Action action =
						new ActionOpenChestMenu(MessageManagementChestMenu.this.player, MessageManagementChestMenu.this.chestMenu);
					ChestMenu chestMenu =
						new MessageChangeLanguageChestMenu(MessageManagementChestMenu.this.plugin, MessageManagementChestMenu.this.player,
							MessageManagementChestMenu.this.user, MessageManagementChestMenu.this.messageId, action);
					chestMenu.fill();
					MessageManagementChestMenu.this.plugin.getChestMenuContainer().openChestMenu(MessageManagementChestMenu.this.player,
						chestMenu);
				}
			});
		}
		{
			ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.LIME, 1);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
				Message.getMessage("NetworkCore", "Message Management Menu", "translations-item-name").getMessage(this.user)));
			item.setItemMeta(meta);
			this.addChestItem(MessageManagementChestMenu.SLOT_LIST_TRANSLATIONS, new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					Action action =
						new ActionOpenChestMenu(MessageManagementChestMenu.this.player, MessageManagementChestMenu.this.chestMenu);
					ChestMenu chestMenu = new AdminMessageTranslationListChestMenu(MessageManagementChestMenu.this.plugin,
						MessageManagementChestMenu.this.player, MessageManagementChestMenu.this.user,
						MessageManagementChestMenu.this.messageId, action);
					chestMenu.fill();
					MessageManagementChestMenu.this.plugin.getChestMenuContainer().openChestMenu(MessageManagementChestMenu.this.player,
						chestMenu);
				}
			});
		}
		{
			{
				ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.LIME, 1);
				ItemMeta meta = item.getItemMeta();
				meta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
					Message.getMessage("NetworkCore", "Message Management Menu", "add-translation-item-name").getMessage(this.user)));
				item.setItemMeta(meta);
				this.addChestItem(MessageManagementChestMenu.SLOT_ADD_TRANSLATION, new ChestItem(item)
				{

					@Override
					public void onInventoryClick(InventoryClickEvent e)
					{
						Action action =
							new ActionOpenChestMenu(MessageManagementChestMenu.this.player, MessageManagementChestMenu.this.chestMenu);
						ChestMenu chestMenu = new AddMessageTranslationChestMenu(MessageManagementChestMenu.this.plugin,
							MessageManagementChestMenu.this.messageId, MessageManagementChestMenu.this.player,
							MessageManagementChestMenu.this.user, action);
						chestMenu.fill();
						MessageManagementChestMenu.this.plugin.getChestMenuContainer().openChestMenu(MessageManagementChestMenu.this.player,
							chestMenu);
					}
				});
			}
		}
		{
			ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.RED, 1);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
				Message.getMessage("NetworkCore", "Menu", "back-item-name").getMessage(this.user)));
			item.setItemMeta(meta);
			this.addChestItem(MessageManagementChestMenu.SLOT_BACK, new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					MessageManagementChestMenu.this.action.onAction();
				}
			});
		}
	}
}
