package net.rieksen.networkcore.spigot.chestmenu;

import java.util.List;

import org.apache.commons.lang.Validate;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.option.OptionSection;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.util.Action;
import net.rieksen.networkcore.core.util.ListUtil;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.material.ItemColor;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

@Deprecated
public class OptionSectionListForPluginChestMenu extends OptionSectionListChestMenu
{

	private final static int		SLOT_INFO	= 13;
	private final static int		SLOT_BACK	= 53;
	private final NetworkCoreSpigot	plugin;
	private final int				pluginId;
	private final Player			player;
	private final Action			backAction;

	public OptionSectionListForPluginChestMenu(NetworkCoreSpigot plugin, int pluginId, Player player, Action backAction)
	{
		super(plugin);
		Validate.notNull(pluginId);
		this.plugin = plugin;
		this.pluginId = pluginId;
		this.player = player;
		this.backAction = backAction;
		this.pageForwardSlot = 52;
		this.contentSlots = new int[] { 28, 29, 30, 31, 32, 33, 34, 37, 38, 39, 40, 41, 42, 43 };
	}

	@Override
	public void fill()
	{
		super.fill();
		NetworkPlugin plugin = NetworkPlugin.getPlugin(this.pluginId);
		User user = User.getUser(this.player);
		{
			ItemStack item = new ItemStack(Material.NETHER_STAR);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&6&l" + plugin.getName()));
			item.setItemMeta(meta);
			this.addChestItem(OptionSectionListForPluginChestMenu.SLOT_INFO, new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{}
			});
		}
		ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.RED, 1);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(
			ChatColor.translateAlternateColorCodes('&', Message.getMessage("NetworkCore", "Menu", "back-item-name").getMessage(user)));
		item.setItemMeta(meta);
		this.addChestItem(OptionSectionListForPluginChestMenu.SLOT_BACK, new ChestItem(item)
		{

			@Override
			public void onInventoryClick(InventoryClickEvent e)
			{
				OptionSectionListForPluginChestMenu.this.backAction.onAction();
			}
		});
	}

	@Override
	public List<OptionSection> getSections()
	{
		return ListUtil.getSubList(NetworkPlugin.getPlugin(this.pluginId).getOptionSections(), this.page - 1, this.getContentSlotCount());
	}

	@Override
	public int getTotalChestItems()
	{
		return NetworkPlugin.getPlugin(this.pluginId).getOptionSections().size();
	}

	@Override
	public void onSelect(int sectionId)
	{
		ChestMenu menu =
			new OptionSectionManagementChestMenu(this.plugin, sectionId, this.player, new ActionOpenChestMenu(this.player, this.chestMenu));
		menu.fill();
		this.plugin.getChestMenuContainer().openChestMenu(this.player, menu);
	}
}
