package net.rieksen.networkcore.spigot.chestmenu;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;

import net.rieksen.networkcore.core.server.Server;
import net.rieksen.networkcore.spigot.material.ItemColor;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

public abstract class AServerListChestMenu extends PageChestMenu
{

	public AServerListChestMenu(Inventory inventory)
	{
		super(inventory);
	}

	@Override
	public List<ChestItem> getChestItems()
	{
		List<ChestItem> list = Lists.newArrayList();
		for (final Server server : this.getServerList())
		{
			ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.LIME, 1);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&6&l" + server.getName()));
			List<String> lore = meta.hasLore() ? meta.getLore() : Lists.newArrayList(new String[0]);
			lore.add(ChatColor.translateAlternateColorCodes('&', "&eServer ID &8: &f#" + server.getServerId()));
			meta.setLore(lore);
			item.setItemMeta(meta);
			list.add(new ChestItem(item)
			{

				private int serverId = server.getServerId();

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					AServerListChestMenu.this.onSelection(this.serverId);
				}
			});
		}
		return list;
	}

	public abstract List<Server> getServerList();

	public abstract void onSelection(int serverId);
}
