package net.rieksen.networkcore.spigot.chestmenu;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.ItemMeta;

import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.material.ItemColor;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

public abstract class AdvancedPageChestMenu extends ChestMenu
{

	protected int[]	contentSlots;
	protected int	page	= 1;
	protected int	pageBackSlot;
	protected int	pageRefreshSlot;
	protected int	pageForwardSlot;

	public AdvancedPageChestMenu(Inventory inventory)
	{
		super(inventory);
		this.pageBackSlot = this.getRelativeSlotLastRow(0);
		this.pageRefreshSlot = this.getRelativeSlotLastRow(4);
		this.pageForwardSlot = this.getRelativeSlotLastRow(8);
		int rows = this.getRows();
		this.contentSlots = new int[(rows - 1) * 9];
		for (int i = 0; i < rows * 9 - 9; i++)
		{
			this.contentSlots[i] = i;
		}
	}

	@Override
	public void fill()
	{
		this.inventory.clear();
		this.chestItems.clear();
		List<ChestItem> list = this.getChestItems();
		if (list != null)
		{
			int i = 0;
			for (ChestItem chestItem : list)
			{
				if (i == this.getContentSlotCount())
				{
					NetworkCoreSpigot.getInstance().getLogger()
						.warning("PERFORMANCE ISSUE: paginated ChestMenu content overflow"
							+ "\nA plugin provided more content for a paginated ChestMenu than fits" + "\nOverflow was truncated"
							+ "\nInventory name: " + this.inventory.getName() + "\nContent slots: " + this.getContentSlotCount()
							+ "\nOverflow: " + (list.size() - this.getContentSlotCount()));
					break;
				}
				this.addChestItem(this.contentSlots[i], chestItem);
				i++;
			}
		}
		{
			this.addChestItem(this.pageBackSlot, new ChestItem(this.getPageBackItem())
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					if (AdvancedPageChestMenu.this.page > 1)
					{
						AdvancedPageChestMenu.this.page--;
						AdvancedPageChestMenu.this.fill();
						((Player) e.getWhoClicked()).updateInventory();
					}
				}
			});
		}
		{
			this.addChestItem(this.pageRefreshSlot, new ChestItem(this.getPageRefreshItem())
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					AdvancedPageChestMenu.this.fill();
					((Player) e.getWhoClicked()).updateInventory();
				}
			});
		}
		{
			this.addChestItem(this.pageForwardSlot, new ChestItem(this.getPageForwardItem())
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					if (AdvancedPageChestMenu.this.page < AdvancedPageChestMenu.this.getMaxPageCount())
					{
						AdvancedPageChestMenu.this.page++;
						AdvancedPageChestMenu.this.fill();
						((Player) e.getWhoClicked()).updateInventory();
					}
				}
			});
		}
	}

	/**
	 * Page 1 is the first page
	 * 
	 * @return
	 */
	public abstract List<ChestItem> getChestItems();

	public final int getContentSlotCount()
	{
		return this.contentSlots.length;
	}

	/**
	 * Page 1 is the first page
	 * 
	 * @return
	 */
	public final int getMaxPageCount()
	{
		int total = this.getTotalChestItems();
		if (total == 0) { return 1; }
		int limit = this.getContentSlotCount();
		return total % limit == 0 ? total / limit : total / limit + 1;
	}

	public ItemStack getPageBackItem()
	{
		ItemStack item = ItemStackUtil.createBanner(ItemColor.GREEN, this.page - 1);
		BannerMeta meta = (BannerMeta) item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&6&lPage Back"));
		meta.addPattern(new Pattern(DyeColor.YELLOW, PatternType.RHOMBUS_MIDDLE));
		meta.addPattern(new Pattern(DyeColor.GREEN, PatternType.HALF_VERTICAL_MIRROR));
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		item.setItemMeta(meta);
		return item;
	}

	public ItemStack getPageForwardItem()
	{
		ItemStack item = ItemStackUtil.createBanner(ItemColor.GREEN, this.getMaxPageCount() - this.page);
		BannerMeta meta = (BannerMeta) item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&6&lPage Forward"));
		meta.addPattern(new Pattern(DyeColor.YELLOW, PatternType.RHOMBUS_MIDDLE));
		meta.addPattern(new Pattern(DyeColor.GREEN, PatternType.HALF_VERTICAL));
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		item.setItemMeta(meta);
		return item;
	}

	public ItemStack getPageRefreshItem()
	{
		ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.BLUE, 1);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&6&lRefresh"));
		item.setItemMeta(meta);
		return item;
	}

	public final int getRelativeSlotLastRow(int slot)
	{
		return this.inventory.getSize() - (9 - slot);
	}

	public final int getRows()
	{
		return this.inventory.getSize() / 9;
	}

	public final int getSlot(int row, int slot)
	{
		return row * 9 + slot;
	}

	public abstract int getTotalChestItems();
}
