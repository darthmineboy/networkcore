package net.rieksen.networkcore.spigot.chestmenu;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;

import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.server.Server;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.util.Action;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.chatinput.ChatInput;
import net.rieksen.networkcore.spigot.chatinput.ServerChangeNameChatInput;
import net.rieksen.networkcore.spigot.material.ItemColor;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

public class ServerManagementChestMenu extends ChestMenu
{

	private final static int		SLOT_SERVER_INFO		= 13;
	private final static int		SLOT_SERVER_EDIT_NAME	= 31;
	private final static int		SLOT_BACK				= 53;
	private final NetworkCoreSpigot	plugin;
	private final Player			player;
	private final User				user;
	private final int				serverId;
	private final Action			action;

	public ServerManagementChestMenu(NetworkCoreSpigot plugin, Player player, User user, int serverId, Action action)
	{
		super(plugin.getServer().createInventory(null, 54, ChatColor.translateAlternateColorCodes('&', "&4Server Management")));
		this.plugin = plugin;
		this.player = player;
		this.user = user;
		this.serverId = serverId;
		this.action = action;
	}

	@Override
	public void fill()
	{
		Server server = Server.getServer(this.serverId);
		{
			ItemStack item = new ItemStack(Material.NETHER_STAR);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&6&l" + server.getName()));
			List<String> lore = meta.hasLore() ? meta.getLore() : Lists.newArrayList(new String[0]);
			lore.add(ChatColor.translateAlternateColorCodes('&', "&eServer ID &8: &f#" + server.getServerId()));
			meta.setLore(lore);
			item.setItemMeta(meta);
			this.addChestItem(ServerManagementChestMenu.SLOT_SERVER_INFO, new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{}
			});
		}
		{
			ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.LIME, 1);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&6&lEdit Name"));
			item.setItemMeta(meta);
			this.addChestItem(ServerManagementChestMenu.SLOT_SERVER_EDIT_NAME, new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					ServerManagementChestMenu.this.player.closeInventory();
					ChatInput chatInput = new ServerChangeNameChatInput(ServerManagementChestMenu.this.plugin,
						ServerManagementChestMenu.this.serverId, ServerManagementChestMenu.this.player)
					{

						@Override
						public void onCancel()
						{
							ServerManagementChestMenu.this.plugin.getChestMenuContainer()
								.openChestMenu(ServerManagementChestMenu.this.player, ServerManagementChestMenu.this.chestMenu);
						}

						@Override
						public void onComplete()
						{
							ServerManagementChestMenu.this.plugin.getChestMenuContainer()
								.openChestMenu(ServerManagementChestMenu.this.player, ServerManagementChestMenu.this.chestMenu);
						}
					};
					ServerManagementChestMenu.this.plugin.getChatInputContainer().openChatInput(ServerManagementChestMenu.this.player,
						chatInput);
				}
			});
		}
		ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.RED, 1);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(
			ChatColor.translateAlternateColorCodes('&', Message.getMessage("NetworkCore", "Menu", "back-item-name").getMessage(this.user)));
		item.setItemMeta(meta);
		this.addChestItem(ServerManagementChestMenu.SLOT_BACK, new ChestItem(item)
		{

			@Override
			public void onInventoryClick(InventoryClickEvent e)
			{
				ServerManagementChestMenu.this.action.onAction();
			}
		});
	}
}
