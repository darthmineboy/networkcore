package net.rieksen.networkcore.spigot.chestmenu;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;

import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.util.Action;
import net.rieksen.networkcore.core.util.LoreUtil;
import net.rieksen.networkcore.core.util.TextUtil;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.chestgui.defaults.UserListGUI;
import net.rieksen.networkcore.spigot.material.ItemColor;
import net.rieksen.networkcore.spigot.material.ItemHead;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

public class MenuChestMenu extends ChestMenu
{

	private final static int		SLOT_SERVER_LIST	= 29;
	private final static int		SLOT_PLUGIN_LIST	= 33;
	private final NetworkCoreSpigot	plugin;
	private final Player			player;
	private final User				user;

	public MenuChestMenu(NetworkCoreSpigot plugin, Player player, User user)
	{
		super(plugin.getServer().createInventory(null, 54, ChatColor.translateAlternateColorCodes('&', "&4Menu")));
		this.plugin = plugin;
		this.player = player;
		this.user = user;
	}

	@Override
	public void fill()
	{
		{
			ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.LIME, 1);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
				Message.getMessage("NetworkCore", "Admin Menu", "server-list-item-name").getMessage(this.user)));
			String lore = Message.getMessage("NetworkCore", "Admin Menu", "server-list-item-lore").getMessage(this.user);
			if (lore.contains("%server-count%"))
			{
				lore = lore.replace("%server-count%", this.plugin.getServerModule().getServers().size() + "");
			}
			meta.setLore(LoreUtil.createLore(lore));
			item.setItemMeta(meta);
			this.addChestItem(MenuChestMenu.SLOT_SERVER_LIST, new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					Action action = new ActionOpenChestMenu(MenuChestMenu.this.player, MenuChestMenu.this.chestMenu);
					ChestMenu chestMenu =
						new AdminServerListChestMenu(MenuChestMenu.this.plugin, MenuChestMenu.this.player, MenuChestMenu.this.user, action);
					chestMenu.fill();
					MenuChestMenu.this.plugin.getChestMenuContainer().openChestMenu(MenuChestMenu.this.player, chestMenu);
				}
			});
		}
		{
			ItemStack item = ItemStackUtil.createHead(ItemHead.PLAYER, 1);
			ItemMeta meta = item.getItemMeta();
			// TODO locale
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&6&lUser List"));
			item.setItemMeta(meta);
			this.addChestItem(31, new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					new UserListGUI(MenuChestMenu.this.plugin).open(MenuChestMenu.this.player);
				}
			});
		}
		{
			ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.LIME, 1);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
				Message.getMessage("NetworkCore", "Admin Menu", "plugin-list-item-name").getMessage(this.user)));
			List<String> lore = Lists.newArrayList();
			for (String arg : Message.getMessage("NetworkCore", "Admin Menu", "plugin-list-item-lore").getMessage(this.user).split("\\|"))
			{
				lore.addAll(TextUtil.splitMessage(ChatColor.translateAlternateColorCodes('&',
					arg.replace("%plugin-count%", this.plugin.getPluginModule().getPlugins().size() + "")), 30));
			}
			meta.setLore(lore);
			item.setItemMeta(meta);
			this.addChestItem(MenuChestMenu.SLOT_PLUGIN_LIST, new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					Action action = new ActionOpenChestMenu(MenuChestMenu.this.player, MenuChestMenu.this.chestMenu);
					ChestMenu chestMenu =
						new AdminPluginListChestMenu(MenuChestMenu.this.plugin, MenuChestMenu.this.player, MenuChestMenu.this.user, action);
					chestMenu.fill();
					MenuChestMenu.this.plugin.getChestMenuContainer().openChestMenu(MenuChestMenu.this.player, chestMenu);
				}
			});
		}
	}
}
