package net.rieksen.networkcore.spigot.chestmenu;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.message.MessageTranslation;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.util.Action;
import net.rieksen.networkcore.core.util.ListUtil;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.material.ItemColor;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

@Deprecated
public class AdminMessageTranslationListChestMenu extends AMessageTranslationListChestMenu
{

	private final Player	player;
	private final int		messageId;
	private final Action	action;

	public AdminMessageTranslationListChestMenu(NetworkCoreSpigot plugin, Player player, User user, int messageId, Action action)
	{
		super(plugin, plugin.getServer().createInventory(null, 54, ChatColor.translateAlternateColorCodes('&',
			Message.getMessage("NetworkCore", "Admin Message Translation List Menu", "title").getMessage(user))), user);
		this.player = player;
		this.messageId = messageId;
		this.action = action;
		this.pageForward = 7;
	}

	@Override
	public void afterFill()
	{
		{
			ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.RED, 1);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
				Message.getMessage("NetworkCore", "Menu", "back-item-name").getMessage(this.user)));
			item.setItemMeta(meta);
			this.addChestItem(this.getRelativeSlotLastRow(8), new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					AdminMessageTranslationListChestMenu.this.action.onAction();
				}
			});
		}
	}

	@Override
	public int getTotalChestItems()
	{
		return Message.getMessage(this.messageId).getTranslations().size();
	}

	@Override
	public List<MessageTranslation> getTranslationList()
	{
		return ListUtil.getSubList(Message.getMessage(this.messageId).getTranslations(), this.page - 1, this.LIMIT);
	}

	@Override
	public void onSelection(int messageId, int languageId)
	{
		Action action = new ActionOpenChestMenu(this.player, this.chestMenu);
		ChestMenu chestMenu = new MessageTranslationManagementChestMenu(this.plugin, this.player, this.user, messageId, languageId, action);
		chestMenu.fill();
		this.plugin.getChestMenuContainer().openChestMenu(this.player, chestMenu);
	}
}
