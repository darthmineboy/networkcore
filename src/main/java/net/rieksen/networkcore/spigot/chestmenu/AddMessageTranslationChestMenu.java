package net.rieksen.networkcore.spigot.chestmenu;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.rieksen.networkcore.core.message.Language;
import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.message.MessageType;
import net.rieksen.networkcore.core.message.pojo.MessageTranslationPojo;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.util.Action;
import net.rieksen.networkcore.core.util.ListUtil;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.chatinput.ChatInput;
import net.rieksen.networkcore.spigot.chatinput.MessageTranslationChatInput;
import net.rieksen.networkcore.spigot.material.ItemColor;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

@Deprecated
public class AddMessageTranslationChestMenu extends ALanguageListChestMenu
{

	private final NetworkCoreSpigot	plugin;
	private final int				messageId;
	private final Player			player;
	private final User				user;
	private final Action			action;

	public AddMessageTranslationChestMenu(NetworkCoreSpigot plugin, int messageId, Player player, User user, Action action)
	{
		super(plugin.getServer().createInventory(null, 54, ChatColor.translateAlternateColorCodes('&',
			Message.getMessage("NetworkCore", "Add Message Translation Menu", "title").getMessage(user))));
		this.plugin = plugin;
		this.messageId = messageId;
		this.player = player;
		this.user = user;
		this.action = action;
		this.pageForward = 7;
	}

	@Override
	public void afterFill()
	{
		{
			ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.RED, 1);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
				Message.getMessage("NetworkCore", "Menu", "back-item-name").getMessage(this.user)));
			item.setItemMeta(meta);
			this.addChestItem(this.getRelativeSlotLastRow(8), new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					AddMessageTranslationChestMenu.this.action.onAction();
				}
			});
		}
	}

	@Override
	public List<Language> getLanguageList()
	{
		Message message = Message.getMessage(this.messageId);
		return ListUtil.getSubList(message.getMissingTranslations(), this.page - 1, this.LIMIT);
	}

	@Override
	public int getTotalChestItems()
	{
		Message message = Message.getMessage(this.messageId);
		return message.getMissingTranslations().size();
	}

	@Override
	public void onSelection(final int languageId)
	{
		this.player.closeInventory();
		ChatInput chatInput = new MessageTranslationChatInput(this.plugin, this.messageId, this.player)
		{

			@Override
			public void onCancel()
			{
				AddMessageTranslationChestMenu.this.plugin.getChestMenuContainer().openChestMenu(AddMessageTranslationChestMenu.this.player,
					AddMessageTranslationChestMenu.this.chestMenu);
			}

			@Override
			public void onDone(String text)
			{
				Message message = Message.getMessage(AddMessageTranslationChestMenu.this.messageId);
				message.createTranslation(
					new MessageTranslationPojo(AddMessageTranslationChestMenu.this.messageId, languageId, text, MessageType.REGULAR));
				AddMessageTranslationChestMenu.this.action.onAction();
			}
		};
		this.plugin.getChatInputContainer().openChatInput(this.player, chatInput);
	}
}
