package net.rieksen.networkcore.spigot.chestmenu;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.message.Language;
import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.util.Action;
import net.rieksen.networkcore.core.util.ListUtil;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.material.ItemColor;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

@Deprecated
public class MessageChangeLanguageChestMenu extends ALanguageListChestMenu
{

	private NetworkCore		plugin;
	private final User		user;
	// private final int messageId;
	private final Action	action;

	public MessageChangeLanguageChestMenu(NetworkCoreSpigot plugin, Player player, User user, int messageId, Action action)
	{
		super(plugin.getServer().createInventory(null, 54, ChatColor.translateAlternateColorCodes('&',
			Message.getMessage("NetworkCore", "Message Management Menu", "change-language-title").getMessage(user))));
		this.plugin = plugin;
		this.user = user;
		// this.messageId = messageId;
		this.action = action;
		this.pageForward = 7;
	}

	@Override
	public void afterFill()
	{
		ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.RED, 1);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(
			ChatColor.translateAlternateColorCodes('&', Message.getMessage("NetworkCore", "Menu", "back-item-name").getMessage(this.user)));
		item.setItemMeta(meta);
		this.addChestItem(this.getRelativeSlotLastRow(8), new ChestItem(item)
		{

			@Override
			public void onInventoryClick(InventoryClickEvent e)
			{
				MessageChangeLanguageChestMenu.this.action.onAction();
			}
		});
	};

	@Override
	public List<Language> getLanguageList()
	{
		return ListUtil.getSubList(this.plugin.getMessageModule().getLanguages(), this.page - 1, this.LIMIT);
	}

	@Override
	public int getTotalChestItems()
	{
		return this.plugin.getMessageModule().getLanguages().size();
	}

	@Override
	public void onSelection(int languageId)
	{
		// Message message = Message.getMessage(this.messageId);
		// message.changeLanguageId(languageId);
		this.action.onAction();
	}
}
