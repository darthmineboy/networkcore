package net.rieksen.networkcore.spigot.chestmenu;

import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;

import com.google.common.collect.Maps;

public class ChestMenuContainer
{

	private Map<Player, ChestMenu>	chestMenuMap	= Maps.newHashMap();

	public boolean hasChestMenu(Player player)
	{
		return chestMenuMap.containsKey(player);
	}

	public ChestMenu getChestMenu(Player player)
	{
		return chestMenuMap.get(player);
	}

	/**
	 * Does not close the {@link Inventory}. Use {@link Player#closeInventory()}
	 * 
	 * @param player
	 */
	public void exitChestMenu(Player player)
	{
		ChestMenu chestMenu = chestMenuMap.get(player);
		if (chestMenu != null)
		{
			chestMenu.onExit();
			chestMenuMap.remove(player);
		}
	}

	/**
	 * Sets the current {@link ChestMenu} and opens the inventory.
	 * 
	 * @param player
	 * @param chestMenu
	 */
	public void openChestMenu(final Player player, ChestMenu chestMenu)
	{
		ChestMenu oldChestMenu = chestMenuMap.put(player, chestMenu);
		if (oldChestMenu != null)
		{
			oldChestMenu.onExit();
		}
		InventoryView view = player.openInventory(chestMenu.inventory);
		// Apparently the inventory is cloned, CraftInventoryCustom ->
		// CraftInventory
		chestMenu.inventory = view.getTopInventory();
		if (player.getOpenInventory() == view)
		{
			chestMenu.onOpen();
		} else
		{
			chestMenu.onFailOpen();
			exitChestMenu(player);
		}
	}

	/**
	 * Closes the inventory for all players with a ChestMenu
	 */
	public void exitAllChestMenus()
	{
		for (Player player : chestMenuMap.keySet())
		{
			player.closeInventory();
		}
	}
}
