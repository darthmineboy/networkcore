package net.rieksen.networkcore.spigot.chestmenu;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.Validate;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.rieksen.networkcore.core.message.Message;
import net.rieksen.networkcore.core.option.Option;
import net.rieksen.networkcore.core.option.OptionSection;
import net.rieksen.networkcore.core.option.OptionSectionImpl;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.util.Action;
import net.rieksen.networkcore.core.util.ListUtil;
import net.rieksen.networkcore.core.util.LoreUtil;
import net.rieksen.networkcore.spigot.NetworkCoreSpigot;
import net.rieksen.networkcore.spigot.material.ItemColor;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

@Deprecated
public class OptionSectionManagementChestMenu extends AdvancedPageChestMenu
{

	private static final int		SLOT_INFO	= 13;
	private static final int		SLOT_BACK	= 53;
	private final NetworkCoreSpigot	plugin;
	private final int				sectionId;
	private final Player			player;
	private final Action			backAction;

	public OptionSectionManagementChestMenu(NetworkCoreSpigot plugin, int sectionId, Player player, Action backAction)
	{
		// customize
		super(plugin.getServer().createInventory(null, 54, ChatColor.translateAlternateColorCodes('&', "&4Option Section Management")));
		Validate.notNull(sectionId);
		Validate.notNull(player);
		this.sectionId = sectionId;
		this.plugin = plugin;
		this.player = player;
		this.backAction = backAction;
		this.pageForwardSlot = 52;
		this.contentSlots = new int[] { 28, 29, 30, 31, 32, 33, 34, 37, 38, 39, 40, 41, 42, 43 };
	}

	@Override
	public void fill()
	{
		super.fill();
		User user = User.getUser(this.player);
		OptionSection section = OptionSectionImpl.getSection(this.sectionId);
		NetworkPlugin plugin = NetworkPlugin.getPlugin(section.getPluginId());
		{
			ItemStack item = new ItemStack(Material.NETHER_STAR);
			ItemMeta meta = item.getItemMeta();
			// customize
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&6&l" + section.getName()));
			String description = section.getDescription() == null ? "N/A" : section.getDescription();
			meta.setLore(LoreUtil.createLore(
				"&ePlugin &8: &f" + plugin.getName() + "\n&eSection &8: &f" + section.getName() + "\n&eDescription &8: &f" + description));
			item.setItemMeta(meta);
			this.addChestItem(OptionSectionManagementChestMenu.SLOT_INFO, new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{}
			});
		}
		ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.RED, 1);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(
			ChatColor.translateAlternateColorCodes('&', Message.getMessage("NetworkCore", "Menu", "back-item-name").getMessage(user)));
		item.setItemMeta(meta);
		this.addChestItem(OptionSectionManagementChestMenu.SLOT_BACK, new ChestItem(item)
		{

			@Override
			public void onInventoryClick(InventoryClickEvent e)
			{
				OptionSectionManagementChestMenu.this.backAction.onAction();
			}
		});
	}

	@Override
	public List<ChestItem> getChestItems()
	{
		OptionSection section = OptionSectionImpl.getSection(this.sectionId);
		List<Option> options = ListUtil.getSubList(section.getOptions(), this.page - 1, this.getContentSlotCount());
		if (options == null) { return null; }
		List<ChestItem> list = new ArrayList<>();
		for (Option option : options)
		{
			ItemStack item = ItemStackUtil.createStainedGlassPane(ItemColor.LIME, 1);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&6&l" + option.getName()));
			String description = option.getDescription() == null ? "N/A" : option.getDescription();
			meta.setLore(LoreUtil.createLore("&eType &8: &f" + option.getType().toString() + "\n&eDescription &8: &f" + description));
			item.setItemMeta(meta);
			final int optionId = option.getOptionId();
			list.add(new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					ChestMenu menu = new OptionManagementChestMenu(OptionSectionManagementChestMenu.this.plugin, optionId,
						OptionSectionManagementChestMenu.this.player, new ActionOpenChestMenu(OptionSectionManagementChestMenu.this.player,
							OptionSectionManagementChestMenu.this.chestMenu));
					menu.fill();
					OptionSectionManagementChestMenu.this.plugin.getChestMenuContainer()
						.openChestMenu(OptionSectionManagementChestMenu.this.player, menu);
				}
			});
		}
		return list;
	}

	@Override
	public int getTotalChestItems()
	{
		OptionSection section = OptionSectionImpl.getSection(this.sectionId);
		return section.getOptions().size();
	}
}
