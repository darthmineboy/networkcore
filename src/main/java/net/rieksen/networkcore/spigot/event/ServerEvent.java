package net.rieksen.networkcore.spigot.event;

import org.apache.commons.lang.Validate;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import net.rieksen.networkcore.core.event.server.IServerEvent;

public class ServerEvent extends Event implements IServerEvent
{

	private static final HandlerList handlers = new HandlerList();

	public static HandlerList getHandlerList()
	{
		return ServerEvent.handlers;
	}

	protected final int serverId;

	public ServerEvent(int serverId)
	{
		Validate.notNull(serverId);
		this.serverId = serverId;
	}

	@Override
	public HandlerList getHandlers()
	{
		return ServerEvent.handlers;
	}

	@Override
	public int getServerId()
	{
		return this.serverId;
	}
}
