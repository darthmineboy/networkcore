package net.rieksen.networkcore.spigot.event;

import org.bukkit.event.HandlerList;

import net.rieksen.networkcore.core.event.server.IServerStartEvent;

public class ServerStopEvent extends ServerEvent implements IServerStartEvent
{

	private static final HandlerList handlers = new HandlerList();

	public static HandlerList getHandlerList()
	{
		return ServerStopEvent.handlers;
	}

	public ServerStopEvent(int serverId)
	{
		super(serverId);
	}

	@Override
	public HandlerList getHandlers()
	{
		return ServerStopEvent.handlers;
	}
}
