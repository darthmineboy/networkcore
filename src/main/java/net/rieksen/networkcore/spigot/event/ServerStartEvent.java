package net.rieksen.networkcore.spigot.event;

import org.bukkit.event.HandlerList;

import net.rieksen.networkcore.core.event.server.IServerStartEvent;

public class ServerStartEvent extends ServerEvent implements IServerStartEvent
{

	private static final HandlerList handlers = new HandlerList();

	public static HandlerList getHandlerList()
	{
		return ServerStartEvent.handlers;
	}

	public ServerStartEvent(int serverId)
	{
		super(serverId);
	}

	@Override
	public HandlerList getHandlers()
	{
		return ServerStartEvent.handlers;
	}
}
