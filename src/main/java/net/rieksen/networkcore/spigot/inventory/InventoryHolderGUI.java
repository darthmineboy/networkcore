package net.rieksen.networkcore.spigot.inventory;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.plugin.Plugin;

/**
 * In inventory related events NetworkCore checks whether the holder of the
 * inventory is an instance of this interface. If it is, the related method is
 * invoked.
 * 
 * @author Darthmineboy
 */
public interface InventoryHolderGUI extends InventoryHolder
{

	public Plugin getPlugin();

	public void onInventoryClick(InventoryClickEvent e);

	public void onInventoryClose(InventoryCloseEvent e);

	public void onInventoryOpen(InventoryOpenEvent e);
}
