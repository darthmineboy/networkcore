package net.rieksen.networkcore.spigot.inventory;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.Validate;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemStackBuilder
{

	private Material		type;
	private int				amount	= 1;
	private short			damage;
	private List<String>	lore;
	private String			displayName;

	public ItemStackBuilder(Material type)
	{
		Validate.notNull(type);
		this.type = type;
	}

	public ItemStackBuilder addLore(boolean color, String loreLine)
	{
		if (this.lore == null)
		{
			this.lore = new ArrayList<>();
		}
		if (color)
		{
			loreLine = ChatColor.translateAlternateColorCodes('&', loreLine);
		}
		this.lore.add(loreLine);
		return this;
	}

	public ItemStack build()
	{
		@SuppressWarnings("deprecation")
		ItemStack item = new ItemStack(this.type, this.amount, this.damage);
		ItemMeta meta = item.getItemMeta();
		if (this.lore != null)
		{
			meta.setLore(this.lore);
		}
		if (this.displayName != null)
		{
			meta.setDisplayName(this.displayName);
		}
		item.setItemMeta(meta);
		return item;
	}

	public ItemStackBuilder setAmount(int amount)
	{
		this.amount = amount;
		return this;
	}

	public ItemStackBuilder setDamage(int damage)
	{
		this.damage = (short) damage;
		return this;
	}

	public ItemStackBuilder setDisplayName(boolean color, String displayName)
	{
		this.displayName = color ? ChatColor.translateAlternateColorCodes('&', displayName) : displayName;
		return this;
	}

	public ItemStackBuilder setDisplayName(String displayName)
	{
		this.displayName = displayName;
		return this;
	}
}
