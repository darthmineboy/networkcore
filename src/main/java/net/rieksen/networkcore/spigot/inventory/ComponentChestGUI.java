package net.rieksen.networkcore.spigot.inventory;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.lang.Validate;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.Plugin;

public abstract class ComponentChestGUI extends ChestGUI
{

	private boolean								cancelClick	= true;
	private Map<Integer, InventoryComponent>	components	= new HashMap<>();

	public ComponentChestGUI(Plugin plugin)
	{
		super(plugin);
	}

	/**
	 * Get the component at given slot.
	 * 
	 * @param slot
	 * @return null if no component
	 */
	public InventoryComponent getComponent(int slot)
	{
		return this.components.get(slot);
	}

	/**
	 * Helper method to obtain a range of slots.
	 * 
	 * @param min
	 *            inclusive
	 * @param max
	 *            inclusive
	 * @return
	 */
	public final int[] getRange(int min, int max)
	{
		Validate.isTrue(max >= min, "min is greater than max");
		int slots = max - min + 1;
		int[] slotsArray = new int[slots];
		for (int i = 0; i < slots; i++)
		{
			int slot = min + i;
			slotsArray[i] = slot;
		}
		return slotsArray;
	}

	public void loadComponents(Inventory inventory, Player player)
	{
		// The hashset filters out duplicate components
		new HashSet<>(this.components.values()).forEach(component -> component.load(inventory, player));
	}

	@Override
	public void onInventoryClick(InventoryClickEvent e)
	{
		if (this.cancelClick)
		{
			e.setCancelled(true);
		}
		int slot = e.getSlot();
		InventoryComponent component = this.getComponent(slot);
		if (component == null) { return; }
		component.onInventoryClick(e);
	}

	@Override
	public void onInventoryOpen(InventoryOpenEvent e)
	{
		this.registerComponents();
		this.loadComponents(e.getInventory(), (Player) e.getPlayer());
	}

	/**
	 * Register all the components that will be in this ChestGUI.
	 */
	public abstract void registerComponents();

	public void setCancelClick(boolean cancelClick)
	{
		this.cancelClick = cancelClick;
	}

	public final void setComponent(InventoryComponent component, int slot)
	{
		Validate.notNull(component);
		this.components.put(slot, component);
	}

	public final void setComponent(InventoryComponent component, int[] slots)
	{
		for (int slot : slots)
		{
			this.setComponent(component, slot);
		}
	}
}
