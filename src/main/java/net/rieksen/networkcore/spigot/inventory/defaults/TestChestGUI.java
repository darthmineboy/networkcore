package net.rieksen.networkcore.spigot.inventory.defaults;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.Plugin;

import net.rieksen.networkcore.core.NetworkCoreAPI;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.spigot.inventory.ComponentChestGUI;
import net.rieksen.networkcore.spigot.inventory.PageComponent;
import net.rieksen.networkcore.spigot.inventory.PageContentComponent;
import net.rieksen.networkcore.spigot.material.ItemHead;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

public class TestChestGUI extends ComponentChestGUI
{

	public TestChestGUI(Plugin plugin)
	{
		super(plugin);
	}

	@Override
	public Inventory createInventory(Player player)
	{
		return Bukkit.createInventory(this, 54, "Test Inventory");
	}

	@Override
	public void registerComponents()
	{
		int[] slots = this.getRange(28, 34);
		this.setComponent(new PageComponent<User>(true, this.getRange(29, 33), 28, 34)
		{

			@Override
			public PageContentComponent buildContentComponent(int slot, User content)
			{
				return new PageContentComponent(true, slot)
				{

					@Override
					public ItemStack getItemStack(Player player)
					{
						ItemStack item = ItemStackUtil.createHead(ItemHead.PLAYER, 1);
						SkullMeta meta = (SkullMeta) item.getItemMeta();
						ItemStackUtil.setSkullMetaOwner(meta, content.getName());
						item.setItemMeta(meta);
						return item;
					}

					@Override
					public void onInventoryClick(InventoryClickEvent e)
					{}
				};
			}

			@Override
			public List<User> findContentData(int pageIndex, int maxResults)
			{
				return NetworkCoreAPI.getUserModule().getUsers(pageIndex * maxResults, maxResults);
			}

			@Override
			public int getTotal()
			{
				return NetworkCoreAPI.getUserModule().getUserCount();
			}
		}, slots);
	}
}
