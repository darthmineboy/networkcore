package net.rieksen.networkcore.spigot.inventory;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

public interface InventoryComponent
{

	/**
	 * Load this component. You could set items in the inventory here. Be aware
	 * that clicks are only forwarded to this component if you set it in the
	 * containing ComponentChestGUI.
	 * 
	 * @param inventory
	 * @param player
	 */
	public void load(Inventory inventory, Player player);

	/**
	 * Handle a click which happened in this component.
	 * 
	 * @param e
	 */
	public void onInventoryClick(InventoryClickEvent e);
}
