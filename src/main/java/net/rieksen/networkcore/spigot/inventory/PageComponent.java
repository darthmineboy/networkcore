package net.rieksen.networkcore.spigot.inventory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.Validate;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;

import net.rieksen.networkcore.spigot.material.ItemColor;
import net.rieksen.networkcore.spigot.material.ItemStackUtil;

public abstract class PageComponent<T> extends BaseComponent
{

	private int									pageIndex;
	private int[]								contentSlots;
	private Integer								pageBackSlot;
	private Integer								pageForwardSlot;
	private Map<Integer, PageContentComponent>	content	= new HashMap<>();
	private boolean								loading	= false;

	public PageComponent(boolean async, int[] contentSlots, Integer pageBackSlot, Integer pageForwardSlot)
	{
		super(async);
		Validate.notNull(contentSlots);
		this.contentSlots = contentSlots;
		this.pageBackSlot = pageBackSlot;
		this.pageForwardSlot = pageForwardSlot;
	}

	public abstract PageContentComponent buildContentComponent(int slot, T content);

	public final void changePage(Inventory inventory, Player player, int pageIndex)
	{
		if (pageIndex < 0 || this.loading) { return; }
		if (this.async())
		{
			new Thread(() ->
			{
				this.doChangePage(inventory, player, pageIndex);
			}).start();
		} else
		{
			this.doChangePage(inventory, player, pageIndex);
		}
	}

	public final void doChangePage(Inventory inventory, Player player, int pageIndex)
	{
		int total = this.calculateHighestPage();
		if (pageIndex >= total)
		{
			pageIndex = total - 1;
		}
		this.pageIndex = pageIndex;
		this.load(inventory, player);
	}

	@Override
	public final void doLoad(Inventory inventory, Player player)
	{
		if (this.loading) { return; }
		try
		{
			this.loading = true;
			this.setContent(inventory, player);
			this.setPageHandlers(inventory, player);
		} finally
		{
			this.loading = false;
		}
	}

	/**
	 * Find all the content that we should show on the current page, with
	 * maxResults on each page.
	 * 
	 * @param pageIndex
	 *            starts at 0
	 * @param maxResults
	 *            based on the amount of contentSlots
	 * @return
	 */
	public abstract List<T> findContentData(int pageIndex, int maxResults);

	/**
	 * Get the total size of all content. This is used to create the pages.
	 * 
	 * @return
	 */
	public abstract int getTotal();

	@Override
	public final void onInventoryClick(InventoryClickEvent e)
	{
		if (this.pageBackSlot != null && e.getSlot() == this.pageBackSlot)
		{
			this.changePage(e.getInventory(), (Player) e.getWhoClicked(), this.pageIndex - 1);
		} else if (this.pageForwardSlot != null && e.getSlot() == this.pageForwardSlot)
		{
			this.changePage(e.getInventory(), (Player) e.getWhoClicked(), this.pageIndex + 1);
		} else
		{
			PageContentComponent component = this.content.get(e.getSlot());
			if (component != null)
			{
				component.onInventoryClick(e);
			}
		}
	}

	private Map<Integer, PageContentComponent> buildContent()
	{
		Map<Integer, PageContentComponent> content = new HashMap<>();
		int maxResults = this.contentSlots.length;
		List<T> contentData = this.findContentData(this.pageIndex, maxResults);
		if (contentData.size() > maxResults)
		{
			contentData = this.trimContent(maxResults, contentData);
		}
		int i = 0;
		for (T contentItem : contentData)
		{
			int slot = this.contentSlots[i];
			content.put(slot, this.buildContentComponent(slot, contentItem));
			i++;
		}
		return content;
	}

	private int calculateHighestPage()
	{
		int total = this.getTotal();
		int maxResults = this.contentSlots.length;
		int pages = total / maxResults;
		if (total % maxResults == 0)
		{
			pages++;
		}
		return pages;
	}

	private void clearContent(Inventory inventory)
	{
		this.content.keySet().forEach(slot -> inventory.setItem(slot, null));
		this.content.clear();
	}

	private void setContent(Inventory inventory, Player player)
	{
		Map<Integer, PageContentComponent> content = this.buildContent();
		this.clearContent(inventory);
		this.content = content;
		content.values().forEach(contentItem -> contentItem.load(inventory, player));
	}

	private void setPageBackHandler(Inventory inventory, Player player)
	{
		if (this.pageBackSlot == null) { return; }
		ItemStack item = ItemStackUtil.createBanner(ItemColor.GREEN, Math.min(this.pageIndex, 64));
		BannerMeta meta = (BannerMeta) item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&6&lPage Back"));
		meta.addPattern(new Pattern(DyeColor.YELLOW, PatternType.RHOMBUS_MIDDLE));
		meta.addPattern(new Pattern(DyeColor.GREEN, PatternType.HALF_VERTICAL_MIRROR));
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		item.setItemMeta(meta);
		inventory.setItem(this.pageBackSlot, item);
	}

	private void setPageForwardHandler(Inventory inventory, Player player)
	{
		if (this.pageForwardSlot == null) { return; }
		ItemStack item = ItemStackUtil.createBanner(ItemColor.GREEN, Math.min(this.calculateHighestPage() - this.pageIndex, 64));
		BannerMeta meta = (BannerMeta) item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&6&lPage Forward"));
		meta.addPattern(new Pattern(DyeColor.YELLOW, PatternType.RHOMBUS_MIDDLE));
		meta.addPattern(new Pattern(DyeColor.GREEN, PatternType.HALF_VERTICAL));
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		item.setItemMeta(meta);
		inventory.setItem(this.pageForwardSlot, item);
	}

	private void setPageHandlers(Inventory inventory, Player player)
	{
		this.setPageBackHandler(inventory, player);
		this.setPageForwardHandler(inventory, player);
	}

	private List<T> trimContent(int maxResults, List<T> contentData)
	{
		return contentData.subList(0, maxResults);
	}
}
