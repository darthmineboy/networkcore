package net.rieksen.networkcore.spigot.inventory;

public abstract class PageContentComponent extends ItemComponent
{

	public PageContentComponent(boolean async, int slot)
	{
		super(async, slot);
	}
}
