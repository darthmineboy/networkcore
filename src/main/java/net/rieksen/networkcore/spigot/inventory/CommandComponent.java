package net.rieksen.networkcore.spigot.inventory;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

public abstract class CommandComponent extends ItemComponent
{

	private String command;

	public CommandComponent(boolean async, int slot, String command)
	{
		super(async, slot);
		Validate.notNull(command);
		this.command = command;
	}

	public CommandComponent(int[] slots, boolean async, String command)
	{
		super(slots, async);
		Validate.notNull(command);
		this.command = command;
	}

	@Override
	public void onInventoryClick(InventoryClickEvent e)
	{
		if (e.getWhoClicked() instanceof Player)
		{
			Bukkit.dispatchCommand(e.getWhoClicked(), this.command);
		}
	}
}
