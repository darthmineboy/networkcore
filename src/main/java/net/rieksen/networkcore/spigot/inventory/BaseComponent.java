package net.rieksen.networkcore.spigot.inventory;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public abstract class BaseComponent implements InventoryComponent
{

	private boolean async;

	public BaseComponent(boolean async)
	{
		this.async = async;
	}

	public final boolean async()
	{
		return this.async;
	}

	/**
	 * Performs the load sync, or async.
	 * 
	 * @param inventory
	 * @param player
	 */
	public abstract void doLoad(Inventory inventory, Player player);

	@Override
	public final void load(Inventory inventory, Player player)
	{
		if (this.async)
		{
			new Thread(() ->
			{
				this.doLoad(inventory, player);
			}).start();
		} else
		{
			this.doLoad(inventory, player);
		}
	}
}
