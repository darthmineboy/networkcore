package net.rieksen.networkcore.spigot.inventory;

import org.apache.commons.lang.Validate;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public abstract class ItemComponent extends BaseComponent
{

	private int[] slots;

	public ItemComponent(boolean async, int slot)
	{
		super(async);
		this.slots = new int[] { slot };
	}

	public ItemComponent(int[] slots, boolean async)
	{
		super(async);
		Validate.notNull(slots);
		this.slots = slots;
	}

	@Override
	public final void doLoad(Inventory inventory, Player player)
	{
		ItemStack item = this.getItemStack(player);
		for (int slot : this.slots)
		{
			inventory.setItem(slot, item);
		}
	}

	public abstract ItemStack getItemStack(Player player);
}
