package net.rieksen.networkcore.spigot.inventory;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.Plugin;

public abstract class ChestGUI implements InventoryHolderGUI
{

	private Inventory	inventory;
	private Plugin		plugin;

	public ChestGUI(Plugin plugin)
	{
		Validate.notNull(plugin);
		this.plugin = plugin;
	}

	/**
	 * Helper method to create an inventory. This automatically sets the
	 * InventoryHolder to this object.
	 * 
	 * @param size
	 * @return
	 */
	public Inventory createInventory(int size)
	{
		return Bukkit.createInventory(this, size);
	}

	/**
	 * Helper method to create an inventory. This automatically sets the
	 * InventoryHolder to this object.
	 * 
	 * @param size
	 * @param title
	 * @return
	 */
	public Inventory createInventory(int size, String title)
	{
		return Bukkit.createInventory(this, size, title);
	}

	/**
	 * This method is called when the ChestGUI is opened by the first player. It
	 * should return an inventory of which the InventoryHolder is this object.
	 * 
	 * @param player
	 * @return
	 */
	public abstract Inventory createInventory(Player player);

	@Override
	public Inventory getInventory()
	{
		return this.inventory;
	}

	@Override
	public Plugin getPlugin()
	{
		return this.plugin;
	}

	@Override
	public void onInventoryClick(InventoryClickEvent e)
	{}

	@Override
	public void onInventoryClose(InventoryCloseEvent e)
	{}

	@Override
	public void onInventoryOpen(InventoryOpenEvent e)
	{}

	public void open(Player player)
	{
		if (this.inventory == null)
		{
			this.inventory = this.createInventory(player);
		}
		player.openInventory(this.inventory);
	}

	public void updateViewers()
	{
		if (this.inventory == null) { return; }
		this.inventory.getViewers().forEach(viewer ->
		{
			if (viewer instanceof Player)
			{
				((Player) viewer).updateInventory();
			}
		});
	}
}
