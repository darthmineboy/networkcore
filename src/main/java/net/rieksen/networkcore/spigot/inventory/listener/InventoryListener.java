package net.rieksen.networkcore.spigot.inventory.listener;

import java.util.logging.Level;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

import net.rieksen.networkcore.spigot.inventory.InventoryHolderGUI;

public class InventoryListener implements Listener
{

	@EventHandler
	public void onInventoryClick(InventoryClickEvent e)
	{
		Inventory inventory = e.getInventory();
		if (inventory == null) { return; }
		InventoryHolder holder = inventory.getHolder();
		if (holder instanceof InventoryHolderGUI)
		{
			try
			{
				((InventoryHolderGUI) holder).onInventoryClick(e);
			} catch (Exception ex)
			{
				((InventoryHolderGUI) holder).getPlugin().getLogger().log(Level.SEVERE, "Failed to handle inventory click event", ex);
			}
		}
	}

	@EventHandler
	public void onInventoryClose(InventoryCloseEvent e)
	{
		Inventory inventory = e.getInventory();
		if (inventory == null) { return; }
		InventoryHolder holder = inventory.getHolder();
		if (holder instanceof InventoryHolderGUI)
		{
			try
			{
				((InventoryHolderGUI) holder).onInventoryClose(e);
			} catch (Exception ex)
			{
				((InventoryHolderGUI) holder).getPlugin().getLogger().log(Level.SEVERE, "Failed to handle inventory close event", ex);
			}
		}
	}

	@EventHandler
	public void onInventoryOpen(InventoryOpenEvent e)
	{
		Inventory inventory = e.getInventory();
		if (inventory == null) { return; }
		InventoryHolder holder = inventory.getHolder();
		if (holder instanceof InventoryHolderGUI)
		{
			try
			{
				((InventoryHolderGUI) holder).onInventoryOpen(e);
			} catch (Exception ex)
			{
				((InventoryHolderGUI) holder).getPlugin().getLogger().log(Level.SEVERE, "Failed to handle inventory open event", ex);
			}
		}
	}
}
