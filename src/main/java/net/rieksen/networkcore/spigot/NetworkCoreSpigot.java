package net.rieksen.networkcore.spigot;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import co.aikar.taskchain.BukkitTaskChainFactory;
import co.aikar.taskchain.TaskChainFactory;
import net.milkbowl.vault.permission.Permission;
import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.NetworkCoreAPI;
import net.rieksen.networkcore.core.dao.DAOManager;
import net.rieksen.networkcore.core.enums.PluginStatus;
import net.rieksen.networkcore.core.importer.JSONImporter;
import net.rieksen.networkcore.core.info.InfoModule;
import net.rieksen.networkcore.core.info.InfoModuleImpl;
import net.rieksen.networkcore.core.info.NetworkCoreInfo;
import net.rieksen.networkcore.core.issue.IssueModule;
import net.rieksen.networkcore.core.issue.IssueModuleImpl;
import net.rieksen.networkcore.core.issue.generator.DuplicateServerIdIssueGenerator;
import net.rieksen.networkcore.core.issue.generator.PluginVersionIssueGenerator;
import net.rieksen.networkcore.core.issue.generator.SocketIssueGenerator;
import net.rieksen.networkcore.core.message.MessageModule;
import net.rieksen.networkcore.core.message.MessageModuleImpl;
import net.rieksen.networkcore.core.network.NetworkModule;
import net.rieksen.networkcore.core.network.NetworkModuleImpl;
import net.rieksen.networkcore.core.option.OptionModule;
import net.rieksen.networkcore.core.option.OptionModuleImpl;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.core.plugin.PluginModule;
import net.rieksen.networkcore.core.plugin.task.CheckUserNameHistoryExecutor;
import net.rieksen.networkcore.core.plugin.task.FixDuplicateOptionIndexesTaskExecutor;
import net.rieksen.networkcore.core.plugin.task.IssueGeneratorTaskExecutor;
import net.rieksen.networkcore.core.plugin.task.NetworkCoreTask;
import net.rieksen.networkcore.core.plugin.task.NetworkStatisticTaskExecutor;
import net.rieksen.networkcore.core.plugin.task.PurgePluginTaskExecutionsExecutor;
import net.rieksen.networkcore.core.plugin.task.PurgeServerLogsTaskExecutor;
import net.rieksen.networkcore.core.plugin.task.PurgeServerResourceUsageTaskExecutor;
import net.rieksen.networkcore.core.plugin.task.PurgeUserChatTaskExecutor;
import net.rieksen.networkcore.core.plugin.task.ServerStatisticTaskExecutor;
import net.rieksen.networkcore.core.server.Server;
import net.rieksen.networkcore.core.server.ServerModule;
import net.rieksen.networkcore.core.server.ServerModuleImpl;
import net.rieksen.networkcore.core.server.ServerRuntime;
import net.rieksen.networkcore.core.server.ServerRuntimeImpl;
import net.rieksen.networkcore.core.socket.ServerSocketModule;
import net.rieksen.networkcore.core.socket.ServerSocketModuleImpl;
import net.rieksen.networkcore.core.user.CooldownAttachment;
import net.rieksen.networkcore.core.user.CooldownAttachmentCreate;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.core.user.UserConnect;
import net.rieksen.networkcore.core.user.UserModule;
import net.rieksen.networkcore.core.user.UserModuleImpl;
import net.rieksen.networkcore.core.util.LogHandler;
import net.rieksen.networkcore.core.util.MojangAPI;
import net.rieksen.networkcore.core.web.InstallationPojo;
import net.rieksen.networkcore.core.web.LicenseRequestPojo;
import net.rieksen.networkcore.core.web.LicensedResource;
import net.rieksen.networkcore.core.web.NetworkWebsiteClient;
import net.rieksen.networkcore.core.web.ResourceDownloadSource;
import net.rieksen.networkcore.core.web.ResourceLicensePojo;
import net.rieksen.networkcore.core.web.ResourceMeta;
import net.rieksen.networkcore.core.world.WorldModule;
import net.rieksen.networkcore.core.world.WorldModuleImpl;
import net.rieksen.networkcore.spigot.chatinput.ChatInputContainer;
import net.rieksen.networkcore.spigot.chestgui.ChestManager;
import net.rieksen.networkcore.spigot.chestmenu.ChestMenuContainer;
import net.rieksen.networkcore.spigot.command.NCoreCommand;
import net.rieksen.networkcore.spigot.command.NetworkCommandBridge;
import net.rieksen.networkcore.spigot.command.language.LanguageCommand;
import net.rieksen.networkcore.spigot.command.note.NoteCommand;
import net.rieksen.networkcore.spigot.file.AutomaticServerConfig;
import net.rieksen.networkcore.spigot.inventory.listener.InventoryListener;
import net.rieksen.networkcore.spigot.listener.AsyncPlayerChatListener;
import net.rieksen.networkcore.spigot.listener.AsyncPlayerPreLoginListener;
import net.rieksen.networkcore.spigot.listener.InventoryClickListener;
import net.rieksen.networkcore.spigot.listener.InventoryCloseListener;
import net.rieksen.networkcore.spigot.listener.PlayerCommandPreprocessListener;
import net.rieksen.networkcore.spigot.listener.PlayerJoinListener;
import net.rieksen.networkcore.spigot.listener.PlayerQuitListener;
import net.rieksen.networkcore.spigot.listener.PluginListener;
import net.rieksen.networkcore.spigot.listener.ServerListener;
import net.rieksen.networkcore.spigot.listener.WorldLoadListener;
import net.rieksen.networkcore.spigot.listener.WorldUnloadListener;
import net.rieksen.networkcore.spigot.object.ConfigFileHelper;
import net.rieksen.networkcore.spigot.plugin.NetworkPluginSync;
import net.rieksen.networkcore.spigot.runnable.CooldownCleanupRunnable;
import net.rieksen.networkcore.spigot.runnable.PluginTaskRunnable;
import net.rieksen.networkcore.spigot.runnable.ServerResourceRunnable;
import net.rieksen.networkcore.spigot.runnable.ServerRuntimeRunnable;
import net.rieksen.networkcore.spigot.runnable.ServerUserConnectRunnable;
import net.rieksen.networkcore.spigot.util.PluginUtil;

/**
 * Spigot implementation.
 */
@ResourceMeta(licensed = false, resourceId = 1, source = ResourceDownloadSource.SPIGOTMC)
public class NetworkCoreSpigot extends JavaPlugin implements NetworkCore
{

	private static NetworkCoreSpigot instance;

	public static NetworkCoreSpigot getInstance()
	{
		return NetworkCoreSpigot.instance;
	}

	private PluginStatus			status				= PluginStatus.UNINITIALIZED;
	private ChatInputContainer		chatInputContainer	= new ChatInputContainer();
	private ChestMenuContainer		chestMenuContainer	= new ChestMenuContainer();
	private NetworkPlugin			plugin;
	private ConfigFileHelper		configHelper		= new ConfigFileHelper(this);
	private MessageModule			messageModule		= new MessageModuleImpl(this);
	private DAOManager				daoManager;
	private UserModule				userModule			= new UserModuleImpl(this);
	private ServerModule			serverModule		= new ServerModuleImpl(this);
	private PluginModule			pluginModule		= new net.rieksen.networkcore.core.plugin.PluginModuleImpl(this);
	private WorldModule				worldModule			= new WorldModuleImpl(this);
	private OptionModule			optionModule		= new OptionModuleImpl(this);
	private LogHandler				logHandler;
	private NetworkWebsiteClient	webClient;
	private NetworkModule			networkModule		= new NetworkModuleImpl(this);
	private ServerSocketModule		socketModule		= new ServerSocketModuleImpl(this);
	private InfoModule				infoModule			= new InfoModuleImpl(this);
	private IssueModule				issueModule			= new IssueModuleImpl(this);
	private TaskChainFactory		taskChainFactory;
	private AutomaticServerConfig	automaticConfig;
	private Permission				permissionsAPI;
	private MojangAPI				mojangAPI			= new MojangAPI();

	@Override
	public void debug(String message)
	{
		this.getLogger().info(message);
	}

	@Override
	public AutomaticServerConfig getAutomaticConfig()
	{
		return this.automaticConfig;
	}

	public ChatInputContainer getChatInputContainer()
	{
		return this.chatInputContainer;
	}

	public ChestMenuContainer getChestMenuContainer()
	{
		return this.chestMenuContainer;
	}

	public ConfigFileHelper getConfigHelper()
	{
		return this.configHelper;
	}

	@Override
	public DAOManager getDAO()
	{
		return this.daoManager;
	}

	@Override
	public InfoModule getInfoModule()
	{
		return this.infoModule;
	}

	@Override
	public IssueModule getIssueModule()
	{
		return this.issueModule;
	}

	@Override
	public MessageModule getMessageModule()
	{
		return this.messageModule;
	}

	@Override
	public MojangAPI getMojangAPI()
	{
		return this.mojangAPI;
	}

	@Override
	public NetworkModule getNetworkModule()
	{
		return this.networkModule;
	}

	@Override
	public NetworkPlugin getNetworkPlugin()
	{
		return this.pluginModule.getPlugin(this);
	}

	@Override
	public OptionModule getOptionModule()
	{
		return this.optionModule;
	}

	public Permission getPermissionsAPI()
	{
		return this.permissionsAPI;
	}

	public NetworkPlugin getPlugin()
	{
		return this.plugin;
	}

	@Override
	public PluginModule getPluginModule()
	{
		return this.pluginModule;
	}

	@Override
	public ServerModule getServerModule()
	{
		return this.serverModule;
	}

	@Override
	public ServerSocketModule getSocketModule()
	{
		return this.socketModule;
	}

	@Override
	public PluginStatus getStatus()
	{
		return this.status;
	}

	@Override
	public TaskChainFactory getTaskChainFactory()
	{
		return this.taskChainFactory;
	}

	@Override
	public UserModule getUserModule()
	{
		return this.userModule;
	}

	public NetworkWebsiteClient getWebClient()
	{
		if (this.webClient == null)
		{
			this.webClient = new NetworkWebsiteClient();
		}
		return this.webClient;
	}

	@Override
	public WorldModule getWorldModule()
	{
		return this.worldModule;
	}

	@Override
	public void onDisable()
	{
		this.status = PluginStatus.DISABLING;
		try
		{
			// Should eventually be removed
			this.deletePluginOptionsFileIfCreatedByNetworkCore();
			for (Player player : this.getServer().getOnlinePlayers())
			{
				User user = User.getUser(player);
				if (user.hasConnect())
				{
					UserConnect connect = user.getConnect();
					connect.changeQuitDate(new Date(System.currentTimeMillis()));
					user.setConnect(null);
				}
			}
			Server server = Server.getLocalServer();
			// Refresh to not overwrite changes
			server.refresh();
			ServerRuntime runtime = server.findRuntime();
			runtime.changeLastPingDate(new Date(System.currentTimeMillis()));
			runtime.changeStopDate(new Date(System.currentTimeMillis()));
			this.serverModule.getLocalServer().changeOnline(false);
			this.disableAllServerPlugins();
			Logger.getLogger("").removeHandler(this.logHandler);
			this.chestMenuContainer.exitAllChestMenus();
			this.chatInputContainer.exitAllChatInputs();
			this.userModule.destroy();
			this.pluginModule.destroy();
			this.serverModule.destroy();
			this.messageModule.destroy();
			this.socketModule.destroy();
			this.infoModule.destroy();
			if (this.getDAO() != null)
			{
				this.getDAO().destroy();
			}
			NetworkCoreSpigot.instance = null;
			this.renameAllPluginsWithResourceMetaAnnotation();
		} catch (Exception e)
		{
			this.getLogger().log(Level.SEVERE, "An error occurred while disabling " + this.getName(), e);
		}
		this.status = PluginStatus.DISABLED;
	}

	@Override
	public void onEnable()
	{
		this.status = PluginStatus.ENABLING;
		try
		{
			this.setupUpdater();
			this.taskChainFactory = BukkitTaskChainFactory.create(this);
			NetworkCoreSpigot.instance = this;
			NetworkCoreAPI.setProvider(this);
			this.setupListeners();
			if (!this.configHelper.init())
			{
				this.getLogger().severe("Failed to enable successfully");
				this.status = PluginStatus.ERROR;
				return;
			}
			this.pluginModule.init();
			this.getDAO().init();
			this.infoModule.init();
			this.serverModule.init();
			this.automaticConfig = new AutomaticServerConfig(this);
			this.automaticConfig.init();
			this.socketModule.init();
			this.serverModule.getLocalServer().changeOnline(true);
			this.messageModule.init();
			this.userModule.init();
			this.networkModule.init();
			this.setupServerRuntime();
			this.fixUnfinishedUserConnections();
			this.setupDefaults();
			this.setupPlugin();
			this.setupWorlds();
			this.reloadCompatability();
			this.setupCommands();
			this.setupRunnables();
			this.setupDefaultUserAttachments();
			this.setupInstallationAndLicenses();
			this.registerTasks();
			this.setupHooks();
			this.status = PluginStatus.SUCCESS;
		} catch (Exception e)
		{
			this.status = PluginStatus.ERROR;
			this.getLogger().log(Level.SEVERE, String.format("An error occurred while enabling %s", this.getName()), e);
		}
	}

	public void setDAOManager(DAOManager daoManager)
	{
		this.daoManager = daoManager;
	}

	/**
	 * NetworkCore used to export options to a file named options.yml. This
	 * caused conflict with other plugins. This method determines if the
	 * options.yml was created by NetworkCore, if so it is deleted. Eventually
	 * this will be removed from code.
	 */
	private void deletePluginOptionsFileIfCreatedByNetworkCore()
	{
		for (Plugin plugin : this.getServer().getPluginManager().getPlugins())
		{
			File dataFolder = plugin.getDataFolder();
			if (!dataFolder.exists())
			{
				continue;
			}
			File optionsFile = new File(dataFolder, "options.yml");
			if (!optionsFile.exists())
			{
				continue;
			}
			YamlConfiguration yaml = new YamlConfiguration();
			try
			{
				yaml.load(optionsFile);
				// There is only one section
				if (yaml.getKeys(false).size() != 1)
				{
					continue;
				}
				ConfigurationSection section = yaml.getConfigurationSection("Config");
				if (section == null || section.getKeys(false).size() != 1)
				{
					continue;
				}
				section = section.getConfigurationSection("options");
				if (section == null || section.getKeys(false).size() != 1)
				{
					continue;
				}
				section = section.getConfigurationSection("check-mismatching-plugin-version");
				if (section == null || section.getKeys(false).size() != 3)
				{
					continue;
				}
				optionsFile.delete();
			} catch (IOException | InvalidConfigurationException e)
			{
				// silence
			}
		}
	}

	private void disableAllServerPlugins()
	{
		for (Plugin plugin : this.getServer().getPluginManager().getPlugins())
		{
			this.pluginModule.setPluginServerEnabled(plugin, Server.getLocalServer().getServerId(), false);
		}
	}

	private void fixUnfinishedUserConnections()
	{
		Server server = Server.getLocalServer();
		for (UserConnect userConnect : server.findOpenConnects())
		{
			if (userConnect.getRuntimeId() == server.findRuntime().getRuntimeId())
			{
				// Current runtime
				continue;
			}
			this.getLogger().warning("Discovered an unfinished connect for a user and this server! Did the server crash?");
			ServerRuntime runtime = ServerRuntimeImpl.getRuntime(userConnect.getRuntimeId());
			userConnect.changeQuitDate(runtime.getStopDate());
		}
	}

	private void registerTasks()
	{
		NetworkPlugin plugin = this.getPlugin();
		plugin.setupTask(NetworkCoreTask.COLLECT_SERVER_STATISTICS.name(), 15 * 60, false, new ServerStatisticTaskExecutor(this));
		plugin.setupTask(NetworkCoreTask.COLLECT_NETWORK_STATISTICS.name(), 15 * 60, new NetworkStatisticTaskExecutor(this));
		plugin.setupTask(NetworkCoreTask.PURGE_SERVER_LOGS.name(), 86400, new PurgeServerLogsTaskExecutor(this));
		plugin.setupTask(NetworkCoreTask.PURGE_SERVER_RESOURCE_USAGE.name(), 86400, new PurgeServerResourceUsageTaskExecutor(this));
		plugin.setupTask(NetworkCoreTask.PURGE_USER_CHAT.name(), 86400, new PurgeUserChatTaskExecutor(this));
		plugin.setupTask(NetworkCoreTask.PURGE_PLUGIN_TASK_EXECUTIONS.name(), 600, new PurgePluginTaskExecutionsExecutor(this));
		plugin.setupTask(NetworkCoreTask.CHECK_USER_NAME_HISTORY.name(), 600, new CheckUserNameHistoryExecutor(this, 100));
		plugin.setupTask(NetworkCoreTask.CHECK_PLUGIN_VERSION_MISMATCH.name(), 3600,
			new IssueGeneratorTaskExecutor(new PluginVersionIssueGenerator(this)));
		plugin.setupTask(NetworkCoreTask.CHECK_SOCKET_CONNECT.name(), 3600, new IssueGeneratorTaskExecutor(new SocketIssueGenerator(this)));
		plugin.setupTask(NetworkCoreTask.CHECK_DUPLICATE_SERVER_ID.name(), 3600,
			new IssueGeneratorTaskExecutor(new DuplicateServerIdIssueGenerator(this)));
		plugin.setupTask(NetworkCoreTask.FIX_DUPLICATE_OPTION_INDEXES.name(), 86400, new FixDuplicateOptionIndexesTaskExecutor(this));
	}

	private void reloadCompatability()
	{
		for (Player player : this.getServer().getOnlinePlayers())
		{
			User user = this.userModule.getUser(player);
			if (user == null)
			{
				this.getLogger().severe("Failed to retrieve User for Player " + player.getName() + "!");
				continue;
			}
			this.userModule.handleLogin(user, player.getName(), player.getAddress().getHostString());
		}
		this.pluginModule.updatePreviousPluginVersions();
		for (Plugin plugin : this.getServer().getPluginManager().getPlugins())
		{
			this.getPluginModule().setPluginServerEnabled(plugin, this.getServerModule().getLocalServer().getServerId(),
				plugin.isEnabled());
		}
	}

	private void renameAllPluginsWithResourceMetaAnnotation()
	{
		for (Plugin plugin : this.getServer().getPluginManager().getPlugins())
		{
			if (plugin instanceof JavaPlugin)
			{
				if (plugin.getClass().getAnnotation(ResourceMeta.class) == null)
				{
					continue;
				}
				File pluginFile = PluginUtil.getPluginFile((JavaPlugin) plugin);
				String targetName = plugin.getName() + "-" + plugin.getDescription().getVersion() + ".jar";
				if (!pluginFile.getName().equals(targetName))
				{
					this.getLogger().info("Renaming " + pluginFile.getName() + " to " + targetName);
					pluginFile.renameTo(new File(pluginFile.getParentFile(), targetName));
				}
			}
		}
	}

	private void setupCommands()
	{
		NCoreCommand ncoreCommand = new NCoreCommand(this);
		NetworkCommandBridge ncoreBridge = new NetworkCommandBridge(ncoreCommand);
		this.getCommand("ncore").setExecutor(ncoreBridge);
		this.getCommand("ncore").setTabCompleter(ncoreBridge);
		LanguageCommand languageCommand = new LanguageCommand(this);
		NetworkCommandBridge languageBridge = new NetworkCommandBridge(languageCommand);
		this.getCommand("language").setExecutor(languageBridge);
		this.getCommand("language").setTabCompleter(languageBridge);
		// Notes
		new NoteCommand(this).registerCommand("note", this.getName());
	}

	private void setupDefaults()
	{
		try
		{
			JSONImporter.loadInputStream(this, this.getResource("defaults.json")).importData();
			this.getNetworkPlugin().importDefaults(this.getResource("defaults.yml"), this.getDescription().getVersion(), null);
		} catch (Exception e)
		{
			this.getLogger().log(Level.SEVERE, "Failed to load defaults", e);
		}
	}

	private void setupDefaultUserAttachments()
	{
		this.userModule.addAttachmentCreate(CooldownAttachment.class, new CooldownAttachmentCreate(this));
	}

	private void setupHooks()
	{
		if (this.getServer().getPluginManager().getPlugin("Vault") != null)
		{
			RegisteredServiceProvider<Permission> rsp = this.getServer().getServicesManager().getRegistration(Permission.class);
			if (rsp != null)
			{
				this.permissionsAPI = rsp.getProvider();
			}
		}
	}

	private void setupInstallationAndLicenses()
	{
		this.getServer().getScheduler().runTaskLaterAsynchronously(this, () ->
		{
			NetworkWebsiteClient client = this.getWebClient();
			if (this.infoModule.getInfo(NetworkCoreInfo.INSTALLATION_ID) == null)
			{
				InstallationPojo installation = client.createInstallation();
				this.infoModule.setInfo(NetworkCoreInfo.INSTALLATION_ID, String.valueOf(installation.getInstallationId()));
				this.infoModule.setInfo(NetworkCoreInfo.INSTALLATION_SECRET, installation.getInstallationSecret());
			}
			PluginManager pluginManager = this.getServer().getPluginManager();
			for (Plugin plugin : pluginManager.getPlugins())
			{
				if (!(plugin instanceof LicensedResource))
				{
					continue;
				}
				LicensedResource licensedResource = (LicensedResource) plugin;
				try
				{
					InputStream antiPiracy = plugin.getResource("anti-piracy.yml");
					LicenseRequestPojo licenseRequest;
					if (antiPiracy == null)
					{
						// Test environment
						int userId =
							licensedResource.getUserId().equals("%%__USER__%%") ? 7404 : Integer.valueOf(licensedResource.getUserId());
						licenseRequest =
							new LicenseRequestPojo(userId, licensedResource.getResourceId(), licensedResource.getUniqueDownload());
					} else
					{
						YamlConfiguration antiPiracyYml = YamlConfiguration.loadConfiguration(new InputStreamReader(antiPiracy));
						licenseRequest =
							new LicenseRequestPojo(licensedResource.getResourceId(), antiPiracyYml.getString("license-secret"));
					}
					ResourceLicensePojo license;
					try
					{
						license = client.requestLicense(this.infoModule.getInfo(NetworkCoreInfo.INSTALLATION_SECRET), licenseRequest);
					} catch (IllegalStateException e)
					{
						this.getLogger().info(String.format(
							"Failed to update licenses (known issue; caused by reload; may be ignored). Error: %s", e.getMessage()));
						return;
					}
					NetworkPlugin networkPlugin = this.getPluginModule().getPlugin(plugin);
					networkPlugin.setLicenseValid(license.isLicenseValid());
					if (license.isLicenseValid())
					{
						this.getLogger().info("Thank you for using a licensed version of " + plugin.getName());
					} else
					{
						this.getLogger().severe(
							"Your license for " + plugin.getName() + " did not validate, how come? Do you need to tell us something?");
						if (plugin.isEnabled())
						{
							pluginManager.disablePlugin(plugin);
						}
					}
				} catch (Exception e)
				{
					this.getLogger().log(Level.WARNING, "Failed to request license for " + plugin.getName(), e);
				}
			}
		}, 20L);
	}

	private void setupListeners()
	{
		PluginManager pm = this.getServer().getPluginManager();
		pm.registerEvents(new AsyncPlayerChatListener(this), this);
		pm.registerEvents(new AsyncPlayerPreLoginListener(this), this);
		pm.registerEvents(new InventoryClickListener(this), this);
		pm.registerEvents(new InventoryCloseListener(this), this);
		pm.registerEvents(new PlayerCommandPreprocessListener(this), this);
		pm.registerEvents(new PlayerJoinListener(this), this);
		pm.registerEvents(new PlayerQuitListener(this), this);
		pm.registerEvents(new PluginListener(this), this);
		pm.registerEvents(new ServerListener(), this);
		pm.registerEvents(new WorldLoadListener(this), this);
		pm.registerEvents(new WorldUnloadListener(this), this);
		pm.registerEvents(ChestManager.getInstance(), this);
		pm.registerEvents(new InventoryListener(), this);
		this.logHandler = new LogHandler(this);
		this.logHandler.init();
		Logger.getLogger("").addHandler(this.logHandler);
	}

	private void setupPlugin()
	{
		this.plugin = this.getPluginModule().getPlugin(this);
	}

	private void setupRunnables()
	{
		new CooldownCleanupRunnable(this).runTaskTimerAsynchronously(this, 20, 20 * 3600);
		new ServerRuntimeRunnable(this);
		new ServerResourceRunnable(this).runTaskTimer(this, 20L, 1L);
		new PluginTaskRunnable(this).runTaskTimerAsynchronously(this, 100, 20 * 60);
		this.getServer().getScheduler().runTaskTimerAsynchronously(this, new NetworkPluginSync(this), 20, 60 * 20);
		// In case two servers startup at the same time, they may not be aware
		// that the other server is online.
		new BukkitRunnable()
		{

			@Override
			public void run()
			{
				NetworkCoreSpigot.this.serverModule.refreshCache();
			}
		}.runTaskLaterAsynchronously(this, 20L);
		new ServerUserConnectRunnable(this).runTaskTimerAsynchronously(this, 60 * 20, 60 * 20);
	}

	private void setupServerRuntime()
	{
		Server server = Server.getLocalServer();
		for (ServerRuntime runtime : this.getServerModule().getLocalServer().findOpenRuntimes())
		{
			this.getLogger().warning("Discovered an unfinished runtime for this server! Did the server crash?");
			runtime.changeStopDate(runtime.getLastPingDate());
		}
		ServerRuntime runtime = server.createRuntime();
		server.setRuntimeCache(runtime);
		this.getLogger().info("This server's runtime is #" + server.findRuntime().getRuntimeId());
	}

	private void setupUpdater()
	{
		new BukkitRunnable()
		{

			@Override
			public void run()
			{
				new ResourceUpdater(NetworkCoreSpigot.this).updateAll();
			}
		}.runTaskTimerAsynchronously(this, ResourceUpdater.DELAY, ResourceUpdater.INTERVAL);
	}

	private void setupWorlds()
	{
		for (World world : this.getServer().getWorlds())
		{
			this.worldModule.getWorld(world);
		}
	}
}
