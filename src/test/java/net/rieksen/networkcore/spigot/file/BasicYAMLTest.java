package net.rieksen.networkcore.spigot.file;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ThreadLocalRandom;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.google.common.io.Files;

public class BasicYAMLTest
{

	private Plugin			plugin;
	private File			tempDir;
	private BasicYAML<?>	yaml;

	@Test(expected = IllegalStateException.class)
	public void constructor_directoryAndPluginNull()
	{
		new BasicYAML<>(null, null, null);
	}

	@Test
	public void constructor_directoryNullUsePluginDataFolder()
	{
		this.plugin = Mockito.mock(Plugin.class);
		new BasicYAML<>(this.plugin, null, "test");
		Mockito.verify(this.plugin, Mockito.times(1)).getDataFolder();
	}

	@Test(expected = IllegalStateException.class)
	public void constructor_fileNameNull()
	{
		new BasicYAML<>(this.plugin, this.tempDir, null);
	}

	@Test(expected = IllegalStateException.class)
	public void constructor_notDirectory() throws IOException
	{
		File file = File.createTempFile("networkcore", null);
		file.createNewFile();
		Assert.assertFalse("File should not be directory", file.isDirectory());
		new BasicYAML<>(this.plugin, null, null);
		Mockito.verify(this.plugin, Mockito.times(1)).getDataFolder();
	}

	@Test(expected = IllegalStateException.class)
	public void constructor_notDirectoryPluginNull()
	{
		new BasicYAML<>(null, null, null);
	}

	@Test
	public void copy_fileExists() throws IOException
	{
		File target = new File(this.tempDir, "target.yml");
		byte[] bytes = new byte[128];
		ThreadLocalRandom.current().nextBytes(bytes);
		InputStream stream = new ByteArrayInputStream(bytes);
		Assert.assertTrue("Failed to create file from stream", this.yaml.createIfNotExists(stream));
		this.yaml.copy(target);
		Mockito.verify(this.yaml, Mockito.times(1)).copyFile(this.yaml.file, target);
	}

	@Test
	public void copy_fileNotExists()
	{
		File target = new File(this.tempDir, "target.yml");
		Assert.assertFalse("File should not exist", this.yaml.getFile().exists());
		this.yaml.copy(target);
		Mockito.verify(this.yaml, Mockito.times(0)).copyFile(this.yaml.file, target);
	}

	@Test
	public void getPlugin()
	{
		Assert.assertEquals("Expected getPlugin() to return same plugin from constructor", this.plugin, this.yaml.getPlugin());
	}

	@Test
	public void load()
	{
		Assert.assertTrue("Failed to copy 'config.yml' from jar",
			this.yaml.createIfNotExists(this.getClass().getClassLoader().getResourceAsStream("config.yml")));
		Assert.assertTrue("Failed to load yaml", this.yaml.load());
		Assert.assertNull("Expected no exception", this.yaml.exception);
		Assert.assertNotNull("Expected yaml to be loaded", this.yaml.getYaml());
	}

	@Test
	public void load_invalidConfigurationException()
	{
		byte[] bytes = new byte[128];
		ThreadLocalRandom.current().nextBytes(bytes);
		InputStream stream = new ByteArrayInputStream(bytes);
		Assert.assertTrue("Failed to create file from stream", this.yaml.createIfNotExists(stream));
		Assert.assertFalse("Loading invalid yaml should fail", this.yaml.load());
		Assert.assertTrue("Expected InvalidConfigurationException", this.yaml.exception instanceof InvalidConfigurationException);
	}

	@Before
	public void setUp()
	{
		this.plugin = Mockito.mock(JavaPlugin.class);
		this.tempDir = Files.createTempDir();
		this.yaml = Mockito.spy(new BasicYAML<>(this.plugin, this.tempDir, "temp.yml"));
	}
}
