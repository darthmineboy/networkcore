package net.rieksen.networkcore.spigot;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import net.rieksen.networkcore.core.web.DownloadResponse;
import net.rieksen.networkcore.core.web.NetworkWebsiteClient;

public class ResourceUpdaterTest
{

	private NetworkCoreSpigot	provider;
	private Logger				logger;
	private ResourceUpdater		updater;

	@Test
	public void downloadResource_showProgress() throws IOException
	{
		File file = File.createTempFile("NetworkCore", null);
		int size = 1024 * 1024;
		InputStream is = new ByteArrayInputStream(new byte[size]);
		DownloadResponse download = new DownloadResponse(is, size);
		NetworkWebsiteClient client = Mockito.mock(NetworkWebsiteClient.class);
		Mockito.when(this.provider.getWebClient()).thenReturn(client);
		Mockito.doReturn(download).when(client).downloadResourceWithInstallation(-1, null, null);
		this.updater.downloadResource(file, -1, null, null);
		Mockito.verify(this.logger, Mockito.times(11)).info(Matchers.anyString());
	}

	@Before
	public void setUp()
	{
		this.provider = Mockito.mock(NetworkCoreSpigot.class);
		this.logger = Mockito.spy(Logger.getLogger(this.getClass().getName()));
		this.updater = Mockito.spy(new ResourceUpdater(this.provider));
		Mockito.doReturn(this.logger).when(this.updater).getLogger();
	}
}
