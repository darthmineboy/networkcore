package net.rieksen.networkcore.spigot.plugin;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Logger;

import org.bukkit.plugin.Plugin;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import com.google.common.io.Files;

import net.rieksen.networkcore.core.plugin.NetworkPlugin;

public class NetworkPluginOptionsFileTest
{

	private File						tempDir;
	private Plugin						plugin;
	private NetworkPlugin				networkPlugin;
	private NetworkPluginOptionsFile	file;

	@Before
	public void setUp()
	{
		this.tempDir = Files.createTempDir();
		this.plugin = Mockito.mock(Plugin.class);
		Mockito.when(this.plugin.getDataFolder()).thenReturn(this.tempDir);
		this.networkPlugin = Mockito.mock(NetworkPlugin.class);
		Mockito.when(this.networkPlugin.getLogger()).thenReturn(Logger.getLogger("NetworkCore"));
		this.file = Mockito.spy(new NetworkPluginOptionsFile(this.plugin, this.networkPlugin));
	}

	@Test
	public void synchronize_backupAndExportIfCorrupt()
	{
		byte[] bytes = new byte[128];
		ThreadLocalRandom.current().nextBytes(bytes);
		InputStream stream = new ByteArrayInputStream(bytes);
		Assert.assertTrue("Failed to create file from stream", this.file.createIfNotExists(stream));
		this.file.synchronize();
		Mockito.verify(this.file, Mockito.times(1)).load();
		Mockito.verify(this.file, Mockito.times(1)).copy(Matchers.any());
	}
}
