package net.rieksen.networkcore.core.world;

import org.junit.Test;
import org.meanbean.test.BeanTester;

import net.rieksen.networkcore.core.world.pojo.ServerWorldPojo;
import net.rieksen.networkcore.core.world.pojo.WorldLocationPojo;

public class TestPojo
{

	@Test
	public void testServerWorld()
	{
		new BeanTester().testBean(ServerWorldPojo.class);
	}

	@Test
	public void testWorldLocation()
	{
		new BeanTester().testBean(WorldLocationPojo.class);
	}
}
