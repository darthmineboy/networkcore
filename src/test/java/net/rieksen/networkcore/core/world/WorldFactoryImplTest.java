package net.rieksen.networkcore.core.world;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.util.TestUtil;
import net.rieksen.networkcore.core.world.pojo.ServerWorldPojo;
import net.rieksen.networkcore.core.world.pojo.WorldLocationPojo;

public class WorldFactoryImplTest
{

	private NetworkCore			provider;
	private WorldFactoryImpl	factory;

	@Test
	public void createLocation()
	{
		WorldLocationPojo location = new WorldLocationPojo(1, 2, 3.0, 4.0, 5.0, 6.0f, 7.0f);
		WorldLocation created = this.factory.createLocation(location);
		WorldLocationPojo pojo = created.toPojo();
		TestUtil.assertPojoEquals(location, pojo);
	}

	@Test
	public void createWorld()
	{
		ServerWorldPojo world = new ServerWorldPojo(1, 2, "Test");
		ServerWorld created = this.factory.createWorld(world);
		ServerWorldPojo pojo = created.toPojo();
		TestUtil.assertPojoEquals(world, pojo);
	}

	@Before
	public void setUp()
	{
		this.provider = Mockito.mock(NetworkCore.class);
		this.factory = new WorldFactoryImpl(this.provider);
	}
}
