package net.rieksen.networkcore.core.world;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.dao.WorldDAO;
import net.rieksen.networkcore.core.world.pojo.WorldLocationPojo;

public class WorldModuleImplTest
{

	private NetworkCore		provider;
	private WorldModuleImpl	module;

	@Test
	public void getFactory()
	{
		Assert.assertNotNull(this.module.getFactory());
	}

	@Test
	public void getLocation()
	{
		WorldDAO dao = Mockito.mock(WorldDAO.class);
		WorldLocationPojo pojo = new WorldLocationPojo(1, 2, 3, 4, 5, 6, 7);
		Mockito.when(dao.findLocation(1)).thenReturn(pojo);
		Mockito.doReturn(dao).when(this.module).getWorldDAO();
		WorldFactory factory = Mockito.mock(WorldFactory.class);
		Mockito.when(this.module.getFactory()).thenReturn(factory);
		WorldLocation location = Mockito.mock(WorldLocation.class);
		Mockito.when(factory.createLocation(Matchers.any())).thenReturn(location);
		WorldLocation result = this.module.getLocation(1);
		Mockito.verify(dao).findLocation(1);
		Mockito.verify(factory).createLocation(pojo);
		Assert.assertEquals(location, result);
	}

	@Test
	public void getLocation_notFound()
	{
		WorldDAO dao = Mockito.mock(WorldDAO.class);
		Mockito.when(dao.findLocation(1)).thenReturn(null);
		Mockito.doReturn(dao).when(this.module).getWorldDAO();
		WorldFactory factory = Mockito.mock(WorldFactory.class);
		Mockito.when(this.module.getFactory()).thenReturn(factory);
		WorldLocation result = this.module.getLocation(1);
		Mockito.verify(dao).findLocation(1);
		Mockito.verify(factory, Mockito.never()).createLocation(Matchers.any());
		Assert.assertNull(result);
	}

	@Before
	public void setUp()
	{
		this.provider = Mockito.mock(NetworkCore.class);
		this.module = Mockito.spy(new WorldModuleImpl(this.provider));
	}
}
