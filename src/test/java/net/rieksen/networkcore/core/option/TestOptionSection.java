package net.rieksen.networkcore.core.option;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import junit.framework.Assert;
import net.rieksen.networkcore.core.NetworkCore;

public class TestOptionSection
{

	private OptionSection	section;
	private NetworkCore	provider;

	@Before
	public void setup()
	{
		this.provider = Mockito.mock(NetworkCore.class);
		this.section = OptionFactory.createSection(this.provider, 0, 0, "Test Section", null);
	}

	@Test
	public void testConstructor()
	{
		Assert.assertEquals(0, this.section.getSectionId());
		Assert.assertEquals(0, this.section.getPluginId());
		Assert.assertEquals("Test Section", this.section.getName());
		Assert.assertEquals(null, this.section.getDescription());
	}
}
