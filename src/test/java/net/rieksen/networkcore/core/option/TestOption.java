package net.rieksen.networkcore.core.option;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import junit.framework.Assert;
import net.rieksen.networkcore.core.NetworkCore;

public class TestOption
{

	private Option			option;
	private NetworkCore	provider;

	@Before
	public void setup()
	{
		this.provider = Mockito.mock(NetworkCore.class);
		this.option = OptionFactory.createOption(this.provider, 0, 0, "Test Option", OptionType.BOOLEAN, null);
	}

	@Test
	public void testConstructor()
	{
		Assert.assertEquals(0, this.option.getOptionId());
		Assert.assertEquals(0, this.option.getSectionId());
		Assert.assertEquals("Test Option", this.option.getName());
		Assert.assertEquals(OptionType.BOOLEAN, this.option.getType());
		Assert.assertEquals(null, this.option.getDescription());
	}
}
