package net.rieksen.networkcore.core.option;

import org.junit.Test;
import org.meanbean.test.BeanTester;

import net.rieksen.networkcore.core.option.pojo.OptionPojo;
import net.rieksen.networkcore.core.option.pojo.OptionSectionPojo;
import net.rieksen.networkcore.core.option.pojo.OptionValuePojo;

public class TestPojo
{

	@Test
	public void testOption()
	{
		new BeanTester().testBean(OptionPojo.class);
	}

	@Test
	public void testOptionSection()
	{
		new BeanTester().testBean(OptionSectionPojo.class);
	}

	@Test
	public void testOptionValue()
	{
		new BeanTester().testBean(OptionValuePojo.class);
	}
}
