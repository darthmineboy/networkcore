package net.rieksen.networkcore.core.option;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import junit.framework.Assert;
import net.rieksen.networkcore.core.NetworkCore;

public class TestOptionValue
{

	private NetworkCore	provider;
	private OptionValue	value;

	@Before
	public void setup()
	{
		this.provider = Mockito.mock(NetworkCore.class);
		this.value = new OptionValueImpl(this.provider, 0, 0, null, 0, null);
	}

	@Test
	public void testConstructor()
	{
		Assert.assertEquals(0, this.value.getValueId());
		Assert.assertEquals(0, this.value.getOptionId());
		Assert.assertEquals(null, this.value.getServerId());
		Assert.assertEquals(0, this.value.getIndex());
		Assert.assertEquals(null, this.value.getValue());
	}

	@Test
	public void testSorting()
	{
		List<OptionValueImpl> values = new ArrayList<>();
		values.add(new OptionValueImpl(this.provider, 0, 0, null, 10, "Highest"));
		values.add(new OptionValueImpl(this.provider, 0, 0, null, 5, "Lowest"));
		values.add(new OptionValueImpl(this.provider, 0, 0, null, 7, "Middle"));
		Collections.sort(values);
		Assert.assertEquals(values.get(0).getIndex(), 5);
		Assert.assertEquals(values.get(1).getIndex(), 7);
		Assert.assertEquals(values.get(2).getIndex(), 10);
	}
}
