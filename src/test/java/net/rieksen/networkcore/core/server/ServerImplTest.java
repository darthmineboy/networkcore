package net.rieksen.networkcore.core.server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.World;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import junit.framework.Assert;
import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.dao.DAOManager;
import net.rieksen.networkcore.core.dao.WorldDAO;
import net.rieksen.networkcore.core.server.pojo.ServerPojo;
import net.rieksen.networkcore.core.world.ServerWorld;
import net.rieksen.networkcore.core.world.WorldFactory;
import net.rieksen.networkcore.core.world.WorldFactoryImpl;
import net.rieksen.networkcore.core.world.WorldModule;
import net.rieksen.networkcore.core.world.pojo.ServerWorldPojo;

public class ServerImplTest
{

	private NetworkCore	provider;
	private ServerImpl	server;

	@Test
	public void findWorld_caseInsensitive()
	{
		WorldModule module = Mockito.mock(WorldModule.class);
		WorldFactory factory = new WorldFactoryImpl(this.provider);
		Mockito.when(module.getFactory()).thenReturn(factory);
		Mockito.when(this.provider.getWorldModule()).thenReturn(module);
		DAOManager dao = Mockito.mock(DAOManager.class);
		Mockito.when(this.provider.getDAO()).thenReturn(dao);
		WorldDAO worldDAO = Mockito.mock(WorldDAO.class);
		Mockito.when(dao.getWorldDAO()).thenReturn(worldDAO);
		List<ServerWorldPojo> worlds = Arrays.asList(new ServerWorldPojo(1, 2, "nether"), new ServerWorldPojo(3, 4, "test"));
		Mockito.when(worldDAO.findWorlds(Matchers.anyInt())).thenReturn(worlds);
		World worldLower = Mockito.mock(World.class);
		Mockito.when(worldLower.getName()).thenReturn("test");
		World worldUpper = Mockito.mock(World.class);
		Mockito.when(worldUpper.getName()).thenReturn("TEST");
		ServerWorld lowerCase = this.server.findWorld(worldLower);
		Assert.assertEquals("test", lowerCase.getName());
		ServerWorld upperCase = this.server.findWorld(worldUpper);
		Assert.assertNotNull(lowerCase);
		Assert.assertNotNull(upperCase);
		Assert.assertEquals("Expected world to be renamed", "TEST", lowerCase.getName());
		Assert.assertEquals(lowerCase, upperCase);
	}

	@Test
	public void findWorld_createIfNotExists()
	{
		DAOManager dao = Mockito.mock(DAOManager.class);
		Mockito.when(this.provider.getDAO()).thenReturn(dao);
		WorldDAO worldDAO = Mockito.mock(WorldDAO.class);
		Mockito.when(dao.getWorldDAO()).thenReturn(worldDAO);
		List<ServerWorldPojo> worlds = new ArrayList<>();
		Mockito.when(worldDAO.findWorlds(Matchers.anyInt())).thenReturn(worlds);
		World bukkitWorld = Mockito.mock(World.class);
		Mockito.when(bukkitWorld.getName()).thenReturn("test");
		ServerWorld createdWorld = Mockito.mock(ServerWorld.class);
		Mockito.when(createdWorld.getName()).thenReturn("test");
		Mockito.doReturn(createdWorld).when(this.server).createWorld("test");
		ServerWorld world = this.server.findWorld(bukkitWorld);
		Assert.assertEquals("test", world.getName());
		Mockito.verify(this.server).createWorld("test");
	}

	@Before
	public void setUp()
	{
		this.provider = Mockito.mock(NetworkCore.class);
		ServerPojo pojo = new ServerPojo(1, "Test", ServerType.SPIGOT, true, true, null, null, null, null);
		this.server = Mockito.spy(new ServerImpl(this.provider, pojo));
	}
}
