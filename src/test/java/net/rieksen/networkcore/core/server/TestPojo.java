package net.rieksen.networkcore.core.server;

import org.junit.Test;
import org.meanbean.test.BeanTester;

import net.rieksen.networkcore.core.server.pojo.ServerGroupPojo;
import net.rieksen.networkcore.core.server.pojo.ServerLogPojo;
import net.rieksen.networkcore.core.server.pojo.ServerPojo;
import net.rieksen.networkcore.core.server.pojo.ServerResourceUsagePojo;
import net.rieksen.networkcore.core.server.pojo.ServerRuntimePojo;

public class TestPojo
{

	@Test
	public void testServer()
	{
		new BeanTester().testBean(ServerPojo.class);
	}

	@Test
	public void testServerGroup()
	{
		new BeanTester().testBean(ServerGroupPojo.class);
	}

	@Test
	public void testServerLog()
	{
		new BeanTester().testBean(ServerLogPojo.class);
	}

	@Test
	public void testServerResourceUsage()
	{
		new BeanTester().testBean(ServerResourceUsagePojo.class);
	}

	@Test
	public void testServerRuntime()
	{
		new BeanTester().testBean(ServerRuntimePojo.class);
	}
}
