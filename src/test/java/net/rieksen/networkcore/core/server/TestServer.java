package net.rieksen.networkcore.core.server;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import junit.framework.Assert;
import net.rieksen.networkcore.core.NetworkCore;

public class TestServer
{

	private NetworkCore	provider;
	private int				serverId;
	private Server			server;
	private String			name;
	private ServerType		type;

	@Before
	public void setup()
	{
		this.provider = Mockito.mock(NetworkCore.class);
		this.serverId = 10;
		this.name = "Test Server";
		this.type = ServerType.SPIGOT;
		this.server = ServerFactory.createServer(this.provider, this.serverId, this.name, this.type);
	}

	@Test
	public void testConstructor()
	{
		Assert.assertEquals(this.serverId, this.server.getServerId());
		Assert.assertEquals(this.name, this.server.getName());
		Assert.assertEquals(this.type, this.server.getType());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testServerNameNull()
	{
		this.server.changeName(null);
	}

	@Test
	public void testToString()
	{
		Assert.assertNotNull(this.server.toString());
	}
}
