package net.rieksen.networkcore.core.issue;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.dao.IssueDAO;
import net.rieksen.networkcore.core.issue.IssuePojo;
import net.rieksen.networkcore.core.issue.IssueFactory;
import net.rieksen.networkcore.core.issue.IssueModule;
import net.rieksen.networkcore.core.issue.IssueModuleImpl;

public class TaskModuleTest
{

	private NetworkCore	provider	= Mockito.mock(NetworkCore.class);
	private IssueModule	issueModule;
	private IssueDAO		issueDAO		= Mockito.mock(IssueDAO.class);
	private IssueFactory	issueFactory	= Mockito.mock(IssueFactory.class);

	@Test(expected = IllegalArgumentException.class)
	public void createIssue_null()
	{
		this.issueModule.createIssue(null);
	}

	@Test
	public void createIssue_verifyDAOAndFactoryCall()
	{
		IssuePojo issue = new IssuePojo(0, 0, "Test", 0, new Date(), "Test", "Test", false, "No data");
		this.issueModule.createIssue(issue);
		Mockito.verify(this.issueDAO, Mockito.times(1)).createIssue(issue);
		// Doesn't match task, because the task DAO returns a new task instance
		Mockito.verify(this.issueFactory, Mockito.times(1)).createIssue(Matchers.any());
	}

	@Test
	public void findUnresolvedIssuesOfGenerator()
	{
		this.issueModule.findUnresolvedIssuesOfGenerator(0, "Test");
		Mockito.verify(this.issueDAO, Mockito.times(1)).findUnresolvedIssuesOfGenerator(0, "Test");
	}

	@Before
	public void setUp()
	{
		this.issueModule = Mockito.spy(new IssueModuleImpl(this.provider));
		Mockito.doAnswer((inv) -> this.issueDAO).when(this.issueModule).getDAO();
		Mockito.doAnswer((inv) -> this.issueFactory).when(this.issueModule).getFactory();
	}
}
