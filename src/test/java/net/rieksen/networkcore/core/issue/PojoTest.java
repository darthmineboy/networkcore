package net.rieksen.networkcore.core.issue;

import org.junit.Test;
import org.meanbean.test.BeanTester;

import net.rieksen.networkcore.core.issue.IssuePojo;

public class PojoTest
{

	@Test
	public void testIssue()
	{
		new BeanTester().testBean(IssuePojo.class);
	}
}
