package net.rieksen.networkcore.core.issue;

import java.util.Date;

import org.apache.commons.lang.Validate;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.issue.IssuePojo;
import net.rieksen.networkcore.core.issue.Issue;
import net.rieksen.networkcore.core.issue.IssueFactory;
import net.rieksen.networkcore.core.issue.IssueFactoryImpl;

public class IssueFactoryTest
{

	private NetworkCore	provider	= Mockito.mock(NetworkCore.class);
	private IssueFactory	factory;

	@Test
	public void createIssue()
	{
		IssuePojo issue = new IssuePojo(0, 0, "Test", 0, new Date(), "Test", "Test", false, "No data");
		Issue created = this.factory.createIssue(issue);
		Validate.notNull(created);
	}

	@Before
	public void setUp()
	{
		this.factory = new IssueFactoryImpl(this.provider);
	}
}
