package net.rieksen.networkcore.core.plugin;

import org.junit.Test;
import org.meanbean.test.BeanTester;

import net.rieksen.networkcore.core.plugin.pojo.NetworkPluginPojo;
import net.rieksen.networkcore.core.plugin.pojo.PluginTaskPojo;

public class TestPojo
{

	@Test
	public void testNetworkPlugin()
	{
		new BeanTester().testBean(NetworkPluginPojo.class);
	}

	@Test
	public void testPluginTask()
	{
		new BeanTester().testBean(PluginTaskPojo.class);
	}
}
