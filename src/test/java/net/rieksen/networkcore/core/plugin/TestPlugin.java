package net.rieksen.networkcore.core.plugin;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import junit.framework.Assert;
import net.rieksen.networkcore.core.NetworkCore;

public class TestPlugin
{

	private NetworkPlugin	plugin;
	private NetworkCore		provider;
	private int				pluginId	= 30;
	private String			name		= "Essentials";

	@Before
	public void setup()
	{
		this.provider = Mockito.mock(NetworkCore.class);
		this.plugin = PluginFactoryImpl.createPlugin(this.provider, this.pluginId, this.name, null, null);
	}

	@Test
	public void testConstructor()
	{
		Assert.assertEquals(this.pluginId, this.plugin.getPluginId());
		Assert.assertEquals(this.name, this.plugin.getName());
	}
}
