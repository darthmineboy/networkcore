package net.rieksen.networkcore.core.dao.mysql;

import net.rieksen.networkcore.core.dao.StatisticDAO;
import net.rieksen.networkcore.core.dao.TestStatisticDAO;

public class TestMySQLStatisticDAO extends TestStatisticDAO
{

	@Override
	protected StatisticDAO getStatisticDAO()
	{
		return TestMySQLDAO.getDAOInstance().getStatisticDAO();
	}
}
