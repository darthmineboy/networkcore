package net.rieksen.networkcore.core.dao.mysql;

import net.rieksen.networkcore.core.dao.InfoDAO;
import net.rieksen.networkcore.core.dao.TestInfoDAO;

public class TestMySQLInfoDAO extends TestInfoDAO
{

	@Override
	protected InfoDAO getInfoDAO()
	{
		return TestMySQLDAO.getDAOInstance().getInfoDAO();
	}
}
