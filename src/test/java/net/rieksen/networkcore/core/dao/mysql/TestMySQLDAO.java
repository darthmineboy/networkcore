package net.rieksen.networkcore.core.dao.mysql;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.TestNetworkCore;
import net.rieksen.networkcore.core.dao.DAOManager;
import net.rieksen.networkcore.core.util.MySQL;
import net.rieksen.networkcore.core.util.MySQLSetting;

public class TestMySQLDAO {

    private static final String MYSQL_PASSWORD = "ifyC59gCbfa8lycH";
    private static final String MYSQL_DATABASE = "networkcore_test";
    private static final String MYSQL_USER = "networkcore_test";
    private static final String MYSQL_HOST = "node8.rieksen.net";
    private static DAOManager dao;

    static synchronized DAOManager getDAOInstance() {
        if (TestMySQLDAO.dao == null) {
            NetworkCore provider = new TestNetworkCore();
            MySQL mysql = TestMySQLDAO.getMySQL();
            TestMySQLDAO.dao = new MySQLDAOManager(provider, mysql, true);
            TestMySQLDAO.dao.init();
        }
        return TestMySQLDAO.dao;
    }

    private static synchronized MySQL getMySQL() {
        MySQLSetting setting =
                new MySQLSetting(TestMySQLDAO.MYSQL_HOST, TestMySQLDAO.MYSQL_DATABASE, TestMySQLDAO.MYSQL_USER, TestMySQLDAO.MYSQL_PASSWORD);
        MySQL mysql = new MySQL(setting);
        return mysql;
    }
}
