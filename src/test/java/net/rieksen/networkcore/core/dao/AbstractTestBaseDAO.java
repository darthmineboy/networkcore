package net.rieksen.networkcore.core.dao;

import java.util.UUID;

import org.junit.Before;

import net.rieksen.networkcore.core.plugin.pojo.NetworkPluginPojo;
import net.rieksen.networkcore.core.server.ServerType;
import net.rieksen.networkcore.core.server.pojo.ServerPojo;
import net.rieksen.networkcore.core.user.UserType;
import net.rieksen.networkcore.core.user.pojo.UserPojo;

public abstract class AbstractTestBaseDAO
{

	protected DAOManager dao;

	@Before
	public void setUp()
	{
		this.dao = this.getDAO();
	}

	protected NetworkPluginPojo createTestPlugin()
	{
		NetworkPluginPojo plugin = this.dao.getPluginDAO().findPlugin("Test");
		if (plugin == null)
		{
			plugin = new NetworkPluginPojo(0, "Test", null, null);
			plugin.setPluginId(this.dao.getPluginDAO().createPlugin(plugin));
		}
		return plugin;
	}

	protected ServerPojo createTestServer()
	{
		ServerPojo server = this.dao.getServerDAO().findServer("Test");
		if (server == null)
		{
			server = new ServerPojo(0, "Test", ServerType.SPIGOT, false, false, null, null, null, null);
			server.setServerId(this.dao.getServerDAO().createServer(server));
		}
		return server;
	}

	protected UserPojo createTestUser()
	{
		UserPojo user = this.dao.getUserDAO().findUserByTypeAndName(UserType.MINECRAFT_PLAYER, "Test");
		if (user == null)
		{
			user = new UserPojo(0, UserType.MINECRAFT_PLAYER, UUID.randomUUID(), "Test", null, true, true);
			user.setUserId(this.dao.getUserDAO().createUser(user));
		}
		return user;
	}

	protected abstract DAOManager getDAO();
}
