package net.rieksen.networkcore.core.dao.mysql;

import net.rieksen.networkcore.core.dao.DAOManager;
import net.rieksen.networkcore.core.dao.WorldDAOTest;

public class MySQLWorldDAOTest extends WorldDAOTest
{

	@Override
	protected DAOManager getDAO()
	{
		return TestMySQLDAO.getDAOInstance();
	}
}
