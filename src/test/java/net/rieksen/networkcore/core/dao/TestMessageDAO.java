package net.rieksen.networkcore.core.dao;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;
import net.rieksen.networkcore.core.message.MessageType;
import net.rieksen.networkcore.core.message.pojo.GlobalMessageVariablePojo;
import net.rieksen.networkcore.core.message.pojo.LanguagePojo;
import net.rieksen.networkcore.core.message.pojo.LocaleLanguagePojo;
import net.rieksen.networkcore.core.message.pojo.MessagePojo;
import net.rieksen.networkcore.core.message.pojo.MessageSectionPojo;
import net.rieksen.networkcore.core.message.pojo.MessageTranslationPojo;
import net.rieksen.networkcore.core.message.pojo.MessageVariablePojo;
import net.rieksen.networkcore.core.plugin.pojo.NetworkPluginPojo;

public abstract class TestMessageDAO
{

	protected PluginDAO				pluginDAO;
	protected MessageDAO			messageDAO;
	protected NetworkPluginPojo		plugin;
	protected MessageSectionPojo	section;
	protected LanguagePojo			language;

	@After
	public void destroy()
	{
		for (LanguagePojo language : this.messageDAO.findLanguages())
		{
			this.messageDAO.deleteLanguage(language.getLanguageId());
		}
		for (GlobalMessageVariablePojo var : this.messageDAO.findGlobalVariables())
		{
			this.messageDAO.deleteGlobalVariable(var.getVariableId());
		}
		for (LocaleLanguagePojo localeLanguage : this.messageDAO.findLocaleLanguages())
		{
			this.messageDAO.deleteLocaleLanguage(localeLanguage.getLocaleCode());
		}
	}

	@Before
	public void setUp()
	{
		this.pluginDAO = this.getPluginDAO();
		this.messageDAO = this.getMessageDAO();
		this.plugin = this.createRandomPlugin();
		this.section = this.createRandomSection(this.plugin);
		this.language = this.createRandomLanguage();
	}

	@Test
	public void testCreateGlobalMessageVariable()
	{
		String name = "Test-" + System.currentTimeMillis();
		String replacement = "Replacement-" + System.currentTimeMillis();
		int variableId = this.messageDAO.createGlobalVariable(new GlobalMessageVariablePojo(0, name, replacement));
		GlobalMessageVariablePojo pojo = this.messageDAO.findGlobalVariable(variableId);
		Assert.assertEquals(variableId, pojo.getVariableId());
		Assert.assertEquals(name, pojo.getName());
		Assert.assertEquals(replacement, pojo.getReplacement());
	}

	@Test
	public void testCreateLocaleLanguage()
	{
		LocaleLanguagePojo locale = this.createRandomLocaleLanguage();
		List<LocaleLanguagePojo> found = this.messageDAO.findLocaleLanguages();
		Assert.assertEquals(1, found.size());
		Assert.assertEquals(locale.getLocaleCode(), found.get(0).getLocaleCode());
	}

	@Test
	public void testCreateMessage()
	{
		MessagePojo message = new MessagePojo(0, this.section.getSectionId(), "Test-Message", null);
		message.setMessageId(this.messageDAO.createMessage(message));
		MessagePojo messageFound = this.messageDAO.findMessage(message.getMessageId());
		Assert.assertEquals(message.getMessageId(), messageFound.getMessageId());
	}

	@Test
	public void testDeleteMessage()
	{
		MessagePojo message = this.createRandomMessage(this.section.getSectionId());
		this.messageDAO.deleteMessage(message.getMessageId());
		MessagePojo found = this.messageDAO.findMessage(message.getMessageId());
		Assert.assertNull(found);
	}

	@Test
	public void testDeleteMessageSection()
	{
		this.messageDAO.deleteSection(this.section.getSectionId());
		MessageSectionPojo found = this.messageDAO.findSection(this.section.getSectionId());
		Assert.assertNull(found);
	}

	@Test
	public void testDeleteMessageTranslation()
	{
		MessagePojo message = this.createRandomMessage(this.section.getSectionId());
		this.createRandomTranslation(message.getMessageId(), this.language.getLanguageId());
		this.messageDAO.deleteTranslation(message.getMessageId(), this.language.getLanguageId());
		MessageTranslationPojo found = this.messageDAO.findTranslation(message.getMessageId(), this.language.getLanguageId());
		Assert.assertNull(found);
	}

	@Test
	public void testDeleteMessageVariable()
	{
		MessageVariablePojo variable = this.createRandomVariable(
			this.createRandomMessage(this.createRandomSection(this.createRandomPlugin()).getSectionId()).getMessageId());
		this.messageDAO.deleteVariable(variable.getMessageId(), variable.getName());
		MessageVariablePojo found = this.messageDAO.findVariables(variable.getMessageId()).stream()
			.filter(tempVar -> tempVar.getName().equals(variable.getName())).findAny().orElse(null);
		Assert.assertNull(found);
	}

	@Test
	public void testFindGlobalMessageVariableByName()
	{
		GlobalMessageVariablePojo var = this.createRandomGlobalVariable();
		GlobalMessageVariablePojo found = this.messageDAO.findGlobalVariable(var.getName());
		Assert.assertNotNull(found);
	}

	@Test
	public void testFindGlobalVariables()
	{
		int vars = 3;
		for (int i = 0; i < vars; i++)
		{
			this.createRandomGlobalVariable();
		}
		List<GlobalMessageVariablePojo> found = this.messageDAO.findGlobalVariables();
		Assert.assertEquals(vars, found.size());
	}

	@Test
	public void testFindLanguageById()
	{
		LanguagePojo languageFound = this.messageDAO.findLanguage(this.language.getLanguageId());
		Assert.assertEquals(this.language.getLanguageId(), languageFound.getLanguageId());
	}

	@Test
	public void testFindLanguageByName()
	{
		LanguagePojo languageFound = this.messageDAO.findLanguage(this.language.getName());
		Assert.assertEquals(this.language.getLanguageId(), languageFound.getLanguageId());
	}

	@Test
	public void testFindLanguages()
	{
		int languages = 3;
		for (int i = 0; i < languages; i++)
		{
			this.createRandomLanguage();
		}
		List<LanguagePojo> languagesFound = this.messageDAO.findLanguages();
		Assert.assertTrue("Expected at least " + languages + " results, but got " + languagesFound.size(),
			languagesFound.size() >= languages);
	}

	@Test
	public void testFindLocale()
	{
		String localeCode = "T" + System.currentTimeMillis();
		this.messageDAO.createLocaleLanguage(new LocaleLanguagePojo(localeCode, localeCode, null));
		LocaleLanguagePojo locale = this.messageDAO.findLocale(localeCode);
		Assert.assertEquals(localeCode, locale.getLocaleCode());
		Assert.assertEquals(null, locale.getLanguageId());
	}

	@Test
	public void testFindMessageByName()
	{
		MessagePojo m = this.createRandomMessage(this.section.getSectionId());
		MessagePojo found = this.messageDAO.findMessage(m.getSectionId(), m.getName());
		Assert.assertEquals(m.getMessageId(), found.getMessageId());
	}

	@Test
	public void testFindMessageCount()
	{
		this.createRandomMessage(this.section.getSectionId());
		int found = this.messageDAO.findMessageCount();
		Assert.assertTrue("Expected at least 1 message", found > 0);
	}

	@Test
	public void testFindMessagesInSection()
	{
		int messages = 3;
		for (int i = 0; i < messages; i++)
		{
			this.createRandomMessage(this.section.getSectionId());
		}
		List<MessagePojo> found = this.messageDAO.findMessages(this.section.getSectionId());
		Assert.assertEquals(messages, found.size());
	}

	@Test
	public void testFindMessageTranslations()
	{
		MessagePojo message = this.createRandomMessage(this.section.getSectionId());
		this.createRandomTranslation(message.getMessageId(), this.language.getLanguageId());
		List<MessageTranslationPojo> found = this.messageDAO.findTranslations(message.getMessageId());
		Assert.assertEquals(1, found.size());
		found.forEach(t ->
		{
			Assert.assertEquals(message.getMessageId(), t.getMessageId());
		});
	}

	@Test
	public void testFindMissingTranslations()
	{
		MessagePojo message = this.createRandomMessage(this.section.getSectionId());
		List<LanguagePojo> found = this.messageDAO.findMissingTranslations(message.getMessageId());
		Assert.assertTrue("Expected at least 1 missing translation", found.size() > 0);
	}

	@Test
	public void testFindSectionByName()
	{
		MessageSectionPojo found = this.messageDAO.findSection(this.plugin.getPluginId(), this.section.getName());
		Assert.assertEquals(this.section.getSectionId(), found.getSectionId());
	}

	@Test
	public void testFindSectionCount()
	{
		int found = this.messageDAO.findSectionCount(this.section.getPluginId());
		Assert.assertTrue("Expected at least 1 section", found > 0);
	}

	@Test
	public void testFindSectionsOfPlugin()
	{
		int sections = 3;
		for (int i = 1; i < sections; i++)
		{
			this.createRandomSection(this.plugin);
		}
		List<MessageSectionPojo> list = this.messageDAO.findSections(this.plugin.getPluginId());
		Assert.assertEquals(sections, list.size());
	}

	@Test
	public void testFindTranslation()
	{
		MessagePojo message = this.createRandomMessage(this.section.getSectionId());
		MessageTranslationPojo t = this.createRandomTranslation(message.getMessageId(), this.language.getLanguageId());
		MessageTranslationPojo found = this.messageDAO.findTranslation(t.getMessageId(), t.getLanguageId());
		Assert.assertEquals(t.getMessageId(), found.getMessageId());
		Assert.assertEquals(t.getLanguageId(), found.getLanguageId());
	}

	@Test
	public void testFindTranslationCountOfLanguage()
	{
		int translations = 3;
		for (int i = 0; i < translations; i++)
		{
			MessagePojo message = this.createRandomMessage(this.section.getSectionId());
			this.createRandomTranslation(message.getMessageId(), this.language.getLanguageId());
		}
		int found = this.messageDAO.findTranslationCount(this.language.getLanguageId());
		Assert.assertEquals(translations, found);
	}

	@Test
	public void testFindTranslationsOfMessage()
	{
		MessagePojo message = this.createRandomMessage(this.section.getSectionId());
		int translations = 3;
		for (int i = 0; i < translations; i++)
		{
			LanguagePojo language = this.createRandomLanguage();
			this.createRandomTranslation(message.getMessageId(), language.getLanguageId());
		}
		List<MessageTranslationPojo> list = this.messageDAO.findTranslations(message.getMessageId());
		Assert.assertEquals(translations, list.size());
	}

	@Test
	public void testFindVariablesOfMessage()
	{
		MessagePojo message = this.createRandomMessage(this.section.getSectionId());
		int variables = 3;
		for (int i = 0; i < variables; i++)
		{
			this.createRandomVariable(message.getMessageId());
		}
		List<MessageVariablePojo> list = this.messageDAO.findVariables(message.getMessageId());
		Assert.assertEquals(variables, list.size());
	}

	@Test
	public void testLocaleLanguageNullAsLanguage()
	{
		LocaleLanguagePojo locale = this.createRandomLocaleLanguage();
		locale.setLanguageId(null);
		this.messageDAO.updateLocaleLanguage(locale);
		LocaleLanguagePojo found = this.messageDAO.findLocaleLanguages().stream()
			.filter(localeFound -> localeFound.getLocaleCode().equalsIgnoreCase(locale.getLocaleCode())).findAny().orElse(null);
		Assert.assertEquals(null, found.getLanguageId());
	}

	@Test
	public void testUpdateGlobalVariable()
	{
		GlobalMessageVariablePojo v = this.createRandomGlobalVariable();
		String name = "New" + System.currentTimeMillis();
		String replacement = "";
		v.setName(name);
		v.setReplacement(replacement);
		this.messageDAO.updateGlobalVariable(v);
		GlobalMessageVariablePojo found = this.messageDAO.findGlobalVariable(v.getVariableId());
		Assert.assertEquals(name, found.getName());
		Assert.assertEquals(replacement, found.getReplacement());
	}

	@Test
	public void testUpdateLocaleLanguage()
	{
		LocaleLanguagePojo locale = this.createRandomLocaleLanguage();
		LanguagePojo language = this.createRandomLanguage();
		locale.setLanguageId(language.getLanguageId());
		this.messageDAO.updateLocaleLanguage(locale);
		LocaleLanguagePojo found = this.messageDAO.findLocaleLanguages().stream()
			.filter(localeFound -> localeFound.getLocaleCode().equalsIgnoreCase(locale.getLocaleCode())).findAny().orElse(null);
		Assert.assertEquals(locale.getLanguageId(), found.getLanguageId());
	}

	@Test
	public void testUpdateMessage()
	{
		MessagePojo message = this.createRandomMessage(this.section.getSectionId());
		message.setDescription("New desc");
		message.setName("New name");
		this.messageDAO.updateMessage(message);
		MessagePojo found = this.messageDAO.findMessage(message.getMessageId());
		Assert.assertEquals(message.getName(), found.getName());
		Assert.assertEquals(message.getDescription(), found.getDescription());
	}

	@Test
	public void testUpdateMessageVariable()
	{
		MessagePojo message = this.createRandomMessage(this.section.getSectionId());
		MessageVariablePojo variable = this.createRandomVariable(message.getMessageId());
		variable.setDescription("New description");
		this.messageDAO.updateVariable(variable);
		List<MessageVariablePojo> found = this.messageDAO.findVariables(message.getMessageId());
		found.forEach(pojo ->
		{
			Assert.assertEquals(variable.getDescription(), pojo.getDescription());
		});
	}

	@Test
	public void testUpdateSection()
	{
		String description = "New Description";
		this.section.setDescription(description);
		this.messageDAO.updateSection(this.section);
		MessageSectionPojo sectionFound = this.messageDAO.findSection(this.section.getSectionId());
		Assert.assertEquals(description, sectionFound.getDescription());
	}

	@Test
	public void testUpdateTranslation()
	{
		MessagePojo m = this.createRandomMessage(this.section.getSectionId());
		MessageTranslationPojo t = this.createRandomTranslation(m.getMessageId(), this.language.getLanguageId());
		String message = "New message";
		t.setMessage(message);
		this.messageDAO.updateTranslation(t);
		MessageTranslationPojo found = this.messageDAO.findTranslation(m.getMessageId(), this.language.getLanguageId());
		Assert.assertEquals(message, found.getMessage());
	}

	protected abstract MessageDAO getMessageDAO();

	protected abstract PluginDAO getPluginDAO();

	private GlobalMessageVariablePojo createRandomGlobalVariable()
	{
		GlobalMessageVariablePojo v = new GlobalMessageVariablePojo(0, "Test" + System.currentTimeMillis(), "Test Replacement");
		v.setVariableId(this.messageDAO.createGlobalVariable(v));
		try
		{
			Thread.sleep(1);
		} catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		return v;
	}

	private LanguagePojo createRandomLanguage()
	{
		LanguagePojo language = new LanguagePojo(0, "Language-" + System.currentTimeMillis());
		language.setLanguageId(this.messageDAO.createLanguage(language));
		try
		{
			Thread.sleep(1);
		} catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		return language;
	}

	private LocaleLanguagePojo createRandomLocaleLanguage()
	{
		LanguagePojo language = this.createRandomLanguage();
		String localeCode = String.valueOf(System.currentTimeMillis());
		LocaleLanguagePojo localeLanguage = new LocaleLanguagePojo(localeCode, localeCode, language.getLanguageId());
		this.messageDAO.createLocaleLanguage(localeLanguage);
		try
		{
			Thread.sleep(1);
		} catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		return localeLanguage;
	}

	private MessagePojo createRandomMessage(int sectionId)
	{
		MessagePojo message = new MessagePojo(0, sectionId, "Test" + System.currentTimeMillis(), null);
		message.setMessageId(this.messageDAO.createMessage(message));
		try
		{
			Thread.sleep(1);
		} catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		return message;
	}

	private NetworkPluginPojo createRandomPlugin()
	{
		NetworkPluginPojo plugin = new NetworkPluginPojo(0, "Plugin-" + System.currentTimeMillis(), null, null);
		plugin.setPluginId(this.pluginDAO.createPlugin(plugin));
		return plugin;
	}

	private MessageSectionPojo createRandomSection(NetworkPluginPojo plugin)
	{
		MessageSectionPojo section = new MessageSectionPojo(0, plugin.getPluginId(), "Section-" + System.currentTimeMillis(), null);
		section.setSectionId(this.messageDAO.createSection(section));
		try
		{
			Thread.sleep(1);
		} catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		return section;
	}

	private MessageTranslationPojo createRandomTranslation(int messageId, int languageId)
	{
		MessageTranslationPojo translation = new MessageTranslationPojo(messageId, languageId, "Random translation", MessageType.REGULAR,
			"Default message", MessageType.REGULAR, false, false, new Date());
		this.messageDAO.createTranslation(translation);
		return translation;
	}

	private MessageVariablePojo createRandomVariable(int messageId)
	{
		MessageVariablePojo v = new MessageVariablePojo(messageId, "Test" + System.currentTimeMillis(), null);
		this.messageDAO.createVariable(v);
		try
		{
			Thread.sleep(1);
		} catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		return v;
	}
}
