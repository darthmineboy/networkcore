package net.rieksen.networkcore.core.dao.mysql;

import net.rieksen.networkcore.core.dao.ServerDAO;
import net.rieksen.networkcore.core.dao.TestServerDAO;

public class TestMySQLServerDAO extends TestServerDAO
{

	@Override
	protected ServerDAO getServerDAO()
	{
		return TestMySQLDAO.getDAOInstance().getServerDAO();
	}
}
