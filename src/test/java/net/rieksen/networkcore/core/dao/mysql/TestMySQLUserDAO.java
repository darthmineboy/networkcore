package net.rieksen.networkcore.core.dao.mysql;

import net.rieksen.networkcore.core.dao.DAOManager;
import net.rieksen.networkcore.core.dao.TestUserDAO;

public class TestMySQLUserDAO extends TestUserDAO
{

	@Override
	protected DAOManager getDAO()
	{
		return TestMySQLDAO.getDAOInstance();
	}
}
