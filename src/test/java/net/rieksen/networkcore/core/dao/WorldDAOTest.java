package net.rieksen.networkcore.core.dao;

import org.junit.Test;

import junit.framework.Assert;
import net.rieksen.networkcore.core.server.pojo.ServerPojo;
import net.rieksen.networkcore.core.world.pojo.ServerWorldPojo;

public abstract class WorldDAOTest extends AbstractTestBaseDAO
{

	@Test
	public void updateWorld()
	{
		ServerPojo server = this.createTestServer();
		ServerWorldPojo world = new ServerWorldPojo(0, server.getServerId(), "WorldDAOTest_test" + System.currentTimeMillis());
		WorldDAO worldDAO = this.getDAO().getWorldDAO();
		world.setWorldId(worldDAO.createWorld(world));
		world.setWorldName(world.getWorldName().toUpperCase());
		worldDAO.updateWorld(world);
		ServerWorldPojo found = worldDAO.findWorld(world.getWorldId());
		Assert.assertEquals(world.getWorldName(), found.getWorldName());
	}
}
