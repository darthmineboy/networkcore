package net.rieksen.networkcore.core.dao;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;
import net.rieksen.networkcore.core.server.ServerType;
import net.rieksen.networkcore.core.server.pojo.ServerGroupPojo;
import net.rieksen.networkcore.core.server.pojo.ServerLogPojo;
import net.rieksen.networkcore.core.server.pojo.ServerPojo;
import net.rieksen.networkcore.core.server.pojo.ServerResourceUsagePojo;
import net.rieksen.networkcore.core.server.pojo.ServerRuntimePojo;

public abstract class TestServerDAO
{

	protected ServerDAO serverDAO;

	@Before
	public void setUp()
	{
		this.serverDAO = this.getServerDAO();
	}

	@Test
	public void testCreateGroupServer()
	{
		ServerGroupPojo group = this.createRandomServerGroup();
		ServerPojo server = this.findTestServer();
		this.serverDAO.createGroupServer(group.getGroupId(), server.getServerId());
		List<ServerPojo> found = this.serverDAO.findGroupServers(group.getGroupId());
		Assert.assertEquals(1, found.size());
		Assert.assertEquals(server.getServerId(), found.get(0).getServerId());
	}

	@Test
	public void testCreateServerGroup()
	{
		ServerGroupPojo group = this.createRandomServerGroup();
		ServerGroupPojo found = this.serverDAO.findServerGroup(group.getGroupId());
		Assert.assertEquals(group.getGroupId(), found.getGroupId());
		Assert.assertEquals(group.getName(), found.getName());
		Assert.assertEquals(group.getDisplayName(), found.getDisplayName());
		Assert.assertEquals(group.getDescription(), found.getDescription());
	}

	@Test
	public void testCreateServerLog()
	{
		ServerPojo server = this.findTestServer();
		ServerLogPojo log = new ServerLogPojo(0, server.getServerId(), new Date(), Level.INFO.getName(), "Test entry", 0);
		log.setLogId(this.serverDAO.createLog(log));
		ServerLogPojo found = this.serverDAO.findLog(log.getLogId());
		Assert.assertEquals(log.getLogId(), found.getLogId());
		Assert.assertEquals(log.getServerId(), found.getServerId());
		Assert.assertEquals(log.getLevel(), found.getLevel());
		Assert.assertEquals(log.getMessage(), found.getMessage());
	}

	@Test
	public void testCreateServerResourceUsage()
	{
		ServerPojo server = this.findTestServer();
		ServerResourceUsagePojo resource = new ServerResourceUsagePojo(server.getServerId(), new Date(), 20, 30, 40, 50, 60, 70);
		this.serverDAO.createServerResourceUsage(resource);
		ServerResourceUsagePojo found = this.serverDAO.findLatestServerResourceUsage(server.getServerId());
		Assert.assertEquals(resource.getServerId(), found.getServerId());
		Assert.assertEquals(resource.getTps(), found.getTps());
		Assert.assertEquals(resource.getNodeCpuUsage(), found.getNodeCpuUsage());
		Assert.assertEquals(resource.getServerCpuUsage(), found.getServerCpuUsage());
		Assert.assertEquals(resource.getMemoryAvailable(), found.getMemoryAvailable());
		Assert.assertEquals(resource.getMemoryTotal(), found.getMemoryTotal());
		Assert.assertEquals(resource.getMemoryUsed(), found.getMemoryUsed());
	}

	@Test
	public void testDeleteGroupServer()
	{
		ServerGroupPojo group = this.createRandomServerGroup();
		ServerPojo server = this.findTestServer();
		this.serverDAO.createGroupServer(group.getGroupId(), server.getServerId());
		this.serverDAO.deleteGroupServer(group.getGroupId(), server.getServerId());
		List<ServerPojo> found = this.serverDAO.findGroupServers(group.getGroupId());
		Assert.assertEquals(0, found.size());
	}

	@Test
	public void testDeleteLogsBefore()
	{
		this.serverDAO.deleteLogsBefore(new Date());
	}

	@Test
	public void testDeleteServer()
	{
		ServerPojo server = this.findTestServer();
		this.serverDAO.deleteServer(server.getServerId());
		ServerPojo found = this.serverDAO.findServer(server.getServerId());
		Assert.assertNull(found);
	}

	@Test
	public void testDeleteServerGroup()
	{
		ServerGroupPojo group = this.createRandomServerGroup();
		this.serverDAO.deleteServerGroup(group.getGroupId());
		ServerGroupPojo found = this.serverDAO.findServerGroup(group.getGroupId());
		Assert.assertNull(found);
	}

	@Test
	public void testDeleteServerResourceUsageBefore()
	{
		this.serverDAO.deleteServerResourceUsageBefore(new Date());
	}

	@Test
	public void testFindGroupServer()
	{
		ServerGroupPojo group = this.createRandomServerGroup();
		ServerPojo server = this.findTestServer();
		this.serverDAO.createGroupServer(group.getGroupId(), server.getServerId());
		List<ServerPojo> found = this.serverDAO.findGroupServers(group.getGroupId());
		Assert.assertEquals(1, found.size());
	}

	@Test
	public void testFindGroupsOfServer()
	{
		ServerGroupPojo group = this.createRandomServerGroup();
		ServerPojo server = this.findTestServer();
		this.serverDAO.createGroupServer(group.getGroupId(), server.getServerId());
		List<ServerGroupPojo> found = this.serverDAO.findGroupsOfServer(server.getServerId());
		Assert.assertTrue("Expected at least 1 group", found.size() > 0);
	}

	@Test
	public void testFindLatestLogsOfAllIndexed()
	{
		int maxResults = 10;
		List<ServerLogPojo> logs = this.serverDAO.findLatestLogs(null, 0, maxResults);
		Assert.assertTrue("Logs size exceeds maxResults", logs.size() <= maxResults);
	}

	@Test
	public void testFindLatestLogsOfServerIndexed()
	{
		ServerPojo server = this.findTestServer();
		int maxResults = 10;
		List<ServerLogPojo> logs = this.serverDAO.findLatestLogs(server.getServerId(), 0, maxResults);
		Assert.assertTrue("Logs size exceeds maxResults", logs.size() <= maxResults);
		logs.forEach(log ->
		{
			Assert.assertEquals(server.getServerId(), log.getServerId());
		});
	}

	@Test
	public void testFindLatestLogsOfServerWithLevelIndexed()
	{
		ServerPojo server = this.findTestServer();
		int maxResults = 10;
		Level level = Level.INFO;
		List<ServerLogPojo> logs = this.serverDAO.findLatestLogs(server.getServerId(), level, null, 0, maxResults);
		Assert.assertTrue("Logs size exceeds maxResults", logs.size() <= maxResults);
		logs.forEach(log ->
		{
			Assert.assertEquals(server.getServerId(), log.getServerId());
			Assert.assertEquals(level, log.getLevel());
		});
	}

	@Test
	public void testFindLatestLogsOfServerWithSearchAndLevelIndexed()
	{
		ServerPojo server = this.findTestServer();
		int maxResults = 10;
		Level level = Level.INFO;
		String search = "T";
		List<ServerLogPojo> logs = this.serverDAO.findLatestLogs(server.getServerId(), level, search, 0, maxResults);
		Assert.assertTrue("Logs size exceeds maxResults", logs.size() <= maxResults);
		logs.forEach(log ->
		{
			Assert.assertEquals(server.getServerId(), log.getServerId());
			Assert.assertTrue("Log message does not contain search criteria",
				log.getMessage().toLowerCase().contains(search.toLowerCase()));
		});
	}

	@Test
	public void testFindOfflineServers()
	{
		List<ServerPojo> servers = this.getServerDAO().findServersWithStatus(false);
		for (ServerPojo server : servers)
		{
			Assert.assertFalse(server.isOnline());
		}
	}

	@Test
	public void testFindOnlineServers()
	{
		List<ServerPojo> servers = this.getServerDAO().findServersWithStatus(true);
		for (ServerPojo server : servers)
		{
			Assert.assertTrue(server.isOnline());
		}
	}

	@Test
	public void testFindRuntimesOfServerCount()
	{
		ServerPojo server = this.findTestServer();
		this.serverDAO.createRuntime(new ServerRuntimePojo(0, server.getServerId(), new Date(), new Date(), null));
		int found = this.serverDAO.findRuntimesOfServerCount(server.getServerId());
		Assert.assertTrue("Expected at least 1 server runtime", found > 0);
	}

	@Test
	public void testFindServerGroupById()
	{
		ServerGroupPojo group = this.createRandomServerGroup();
		ServerGroupPojo found = this.serverDAO.findServerGroup(group.getGroupId());
		Assert.assertEquals(group.getGroupId(), found.getGroupId());
		Assert.assertEquals(group.getName(), found.getName());
		Assert.assertEquals(group.getDisplayName(), found.getDisplayName());
		Assert.assertEquals(group.getDescription(), found.getDescription());
	}

	@Test
	public void testFindServerGroupByName()
	{
		ServerGroupPojo group = this.createRandomServerGroup();
		ServerGroupPojo found = this.serverDAO.findServerGroup(group.getName());
		Assert.assertEquals(group.getGroupId(), found.getGroupId());
		Assert.assertEquals(group.getName(), found.getName());
		Assert.assertEquals(group.getDisplayName(), found.getDisplayName());
		Assert.assertEquals(group.getDescription(), found.getDescription());
	}

	@Test
	public void testFindServerGroups()
	{
		this.createRandomServerGroup();
		List<ServerGroupPojo> found = this.serverDAO.findServerGroups();
		Assert.assertTrue("Expected at least 1 group", found.size() > 0);
	}

	@Test
	public void testFindServersCount()
	{
		this.findTestServer();
		int found = this.serverDAO.findServersCount();
		Assert.assertTrue("Expected at least 1 server", found >= 1);
	}

	@Test
	public void testUpdateServerGroup()
	{
		ServerGroupPojo group = this.createRandomServerGroup();
		group.setName("New name" + System.currentTimeMillis());
		group.setDisplayName("New display name");
		group.setDescription("New description");
		this.serverDAO.updateServerGroup(group.getGroupId(), group);
		ServerGroupPojo found = this.serverDAO.findServerGroup(group.getGroupId());
		Assert.assertEquals(group.getName(), found.getName());
		Assert.assertEquals(group.getDisplayName(), found.getDisplayName());
		Assert.assertEquals(group.getDescription(), found.getDescription());
	}

	@Test
	public void testUpdateServerName()
	{
		ServerPojo server = this.findTestServer();
		server.setName("Test" + System.currentTimeMillis());
		this.serverDAO.updateServer(server);
		ServerPojo found = this.serverDAO.findServer(server.getServerId());
		Assert.assertEquals(server.getName(), found.getName());
	}

	@Test
	public void testUpdateServerRuntime()
	{
		ServerPojo server = this.findTestServer();
		ServerRuntimePojo runtime = new ServerRuntimePojo(0, server.getServerId(), new Date(), new Date(), null);
		runtime.setRuntimeId(this.serverDAO.createRuntime(runtime));
		runtime.setStopDate(new Date());
		this.serverDAO.updateRuntime(runtime);
		ServerRuntimePojo found = this.serverDAO.findRuntime(runtime.getRuntimeId());
		Assert.assertNotNull(found.getStopDate());
	}

	protected abstract ServerDAO getServerDAO();

	private ServerGroupPojo createRandomServerGroup()
	{
		ServerGroupPojo group = new ServerGroupPojo(0, "Test" + System.currentTimeMillis(), "Test", "Description");
		group.setGroupId(this.serverDAO.createServerGroup(group));
		return group;
	}

	private ServerPojo findTestServer()
	{
		ServerPojo pojo = this.serverDAO.findServer("Test");
		if (pojo == null)
		{
			pojo = new ServerPojo(0, "Test", ServerType.SPIGOT, false, false, null, null, null, null);
			pojo.setServerId(this.serverDAO.createServer(pojo));
		}
		return pojo;
	}
}
