package net.rieksen.networkcore.core.dao;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import junit.framework.Assert;
import net.rieksen.networkcore.core.message.pojo.LanguagePojo;
import net.rieksen.networkcore.core.server.ServerType;
import net.rieksen.networkcore.core.server.pojo.ServerPojo;
import net.rieksen.networkcore.core.server.pojo.ServerRuntimePojo;
import net.rieksen.networkcore.core.user.UserType;
import net.rieksen.networkcore.core.user.pojo.UserActivityPojo;
import net.rieksen.networkcore.core.user.pojo.UserChatPojo;
import net.rieksen.networkcore.core.user.pojo.UserConnectPojo;
import net.rieksen.networkcore.core.user.pojo.UserIpPojo;
import net.rieksen.networkcore.core.user.pojo.UserLanguagePojo;
import net.rieksen.networkcore.core.user.pojo.UserPojo;
import net.rieksen.networkcore.core.util.SimplePage;

public abstract class TestUserDAO extends AbstractTestBaseDAO
{

	@Test
	public void testCalculateOnlineTime()
	{
		UserPojo user = this.findTestUser();
		long seconds = this.dao.getUserDAO().calculateOnlineTime(user.getUserId());
		Assert.assertTrue("Online time < 0", seconds >= 0);
	}

	@Test
	public void testCreateChat()
	{
		UserPojo user = this.findTestUser();
		ServerPojo server = this.findTestServer();
		ServerRuntimePojo runtime = this.createRuntime(server);
		UserConnectPojo connect =
			new UserConnectPojo(0, user.getUserId(), runtime.getRuntimeId(), user.getUserName(), "127.0.0.1", new Date(), null);
		connect.setConnectId(this.dao.getUserDAO().createConnect(connect));
		UserChatPojo chat = new UserChatPojo(0, connect.getConnectId(), new Date(), "Test chat", false);
		chat.setChatId(this.dao.getUserDAO().createChat(chat));
		UserChatPojo found = this.dao.getUserDAO().findChat(chat.getChatId());
		Assert.assertEquals(chat.getChatId(), found.getChatId());
		Assert.assertEquals(chat.getConnectId(), found.getConnectId());
		Assert.assertEquals(chat.getMessage(), found.getMessage());
		Assert.assertEquals(chat.isCancelled(), found.isCancelled());
	}

	@Test
	public void testCreateConnect()
	{
		UserPojo user = this.findTestUser();
		ServerPojo server = this.findTestServer();
		ServerRuntimePojo runtime = this.createRuntime(server);
		UserConnectPojo connect =
			new UserConnectPojo(0, user.getUserId(), runtime.getRuntimeId(), user.getUserName(), "127.0.0.1", new Date(), null);
		connect.setConnectId(this.dao.getUserDAO().createConnect(connect));
		UserConnectPojo found = this.dao.getUserDAO().findConnect(connect.getConnectId());
		Assert.assertEquals(connect.getConnectId(), found.getConnectId());
		Assert.assertEquals(connect.getUserId(), found.getUserId());
		Assert.assertEquals(connect.getRuntimeId(), found.getRuntimeId());
		Assert.assertEquals(connect.getName(), found.getName());
		Assert.assertEquals(connect.getIp(), found.getIp());
		// Assert.assertEquals(connect.getJoinDate().getTime() / 1000,
		// found.getJoinDate().getTime() / 1000);
		Assert.assertNull(found.getQuitDate());
	}

	@Test
	public void testCreateNameHistory()
	{}

	@Test
	public void testCreateUser()
	{
		UserPojo user =
			new UserPojo(0, UserType.MINECRAFT_PLAYER, UUID.randomUUID(), "Test-" + System.currentTimeMillis(), null, true, true);
		user.setUserId(this.dao.getUserDAO().createUser(user));
		UserPojo found = this.dao.getUserDAO().findUserById(user.getUserId());
		Assert.assertEquals(user.getUserId(), found.getUserId());
		Assert.assertEquals(user.getUserType(), found.getUserType());
		Assert.assertEquals(user.getUUID(), found.getUUID());
		Assert.assertEquals(user.getUserName(), found.getUserName());
	}

	@Test
	public void testCreateUserLanguage()
	{
		UserPojo user = this.findTestUser();
		LanguagePojo language = this.findTestLanguage();
		this.dao.getUserDAO().deleteUserLanguage(user.getUserId(), language.getLanguageId());
		UserLanguagePojo userLanguage = new UserLanguagePojo(user.getUserId(), language.getLanguageId(), 0);
		this.dao.getUserDAO().createUserLanguage(userLanguage);
		UserLanguagePojo found = this.dao.getUserDAO().findUserLanguage(user.getUserId(), language.getLanguageId());
		Assert.assertEquals(userLanguage.getUserId(), found.getUserId());
		Assert.assertEquals(userLanguage.getLanguageId(), found.getLanguageId());
		Assert.assertEquals(userLanguage.getPriority(), found.getPriority());
	}

	@Test
	public void testDeleteChatsBefore()
	{
		this.dao.getUserDAO().deleteChatsBefore(new Date());
	}

	@Test
	public void testDeleteExpiredCooldowns()
	{
		this.dao.getUserDAO().deleteExpiredCooldowns();
	}

	@Test
	public void testDeleteUser()
	{
		UserPojo user = this.findTestUser();
		this.dao.getUserDAO().deleteUser(user.getUserId());
		UserPojo found = this.dao.getUserDAO().findUserById(user.getUserId());
		Assert.assertNull(found);
	}

	@Test
	public void testDeleteUserLanguages()
	{
		UserPojo user = this.findTestUser();
		this.dao.getUserDAO().deleteUserLanguages(user.getUserId());
		List<UserLanguagePojo> found = this.dao.getUserDAO().findUserLanguages(user.getUserId());
		Assert.assertEquals(0, found.size());
	}

	@Test
	public void testFindChatsCountOfUserWithSearch()
	{
		UserPojo user = this.findTestUser();
		ServerRuntimePojo runtime = this.createRuntime(this.findTestServer());
		UserConnectPojo connect =
			new UserConnectPojo(0, user.getUserId(), runtime.getRuntimeId(), user.getUserName(), "127.0.0.1", new Date(), null);
		connect.setConnectId(this.dao.getUserDAO().createConnect(connect));
		String search = "Test message";
		UserChatPojo chat = new UserChatPojo(0, connect.getConnectId(), new Date(), search, true);
		chat.setChatId(this.dao.getUserDAO().createChat(chat));
		int found = this.dao.getUserDAO().findChatsCount(user.getUserId(), search);
		Assert.assertTrue("Expected at least 1 chat", found > 0);
	}

	@Test
	public void testFindChatsOfUser()
	{
		UserPojo user = this.findTestUser();
		ServerRuntimePojo runtime = this.createRuntime(this.findTestServer());
		UserConnectPojo connect =
			new UserConnectPojo(0, user.getUserId(), runtime.getRuntimeId(), user.getUserName(), "127.0.0.1", new Date(), null);
		connect.setConnectId(this.dao.getUserDAO().createConnect(connect));
		UserChatPojo chat = new UserChatPojo(0, connect.getConnectId(), new Date(), "Test message", true);
		chat.setChatId(this.dao.getUserDAO().createChat(chat));
		List<UserChatPojo> found = this.dao.getUserDAO().findChats(user.getUserId());
		Assert.assertTrue("Expected at least 1 chat", found.size() > 0);
	}

	@Test
	public void testFindChatsOfUserIndexed()
	{
		UserPojo user = this.findTestUser();
		ServerRuntimePojo runtime = this.createRuntime(this.findTestServer());
		UserConnectPojo connect =
			new UserConnectPojo(0, user.getUserId(), runtime.getRuntimeId(), user.getUserName(), "127.0.0.1", new Date(), null);
		connect.setConnectId(this.dao.getUserDAO().createConnect(connect));
		UserChatPojo chat = new UserChatPojo(0, connect.getConnectId(), new Date(), "Test message", true);
		chat.setChatId(this.dao.getUserDAO().createChat(chat));
		int maxResults = 10;
		List<UserChatPojo> found = this.dao.getUserDAO().findChats(user.getUserId(), 0, maxResults);
		Assert.assertTrue("Expected at least 1 chat", found.size() > 0);
		Assert.assertTrue("Found results exceeds maxResults", found.size() <= maxResults);
	}

	@Test
	public void testFindConnects()
	{
		UserPojo user = this.findTestUser();
		ServerPojo server = this.findTestServer();
		ServerRuntimePojo runtime = this.createRuntime(server);
		UserConnectPojo connect =
			new UserConnectPojo(0, user.getUserId(), runtime.getRuntimeId(), user.getUserName(), "127.0.0.1", new Date(), null);
		connect.setConnectId(this.dao.getUserDAO().createConnect(connect));
		List<UserConnectPojo> found = this.dao.getUserDAO().findConnects(user.getUserId());
		Assert.assertTrue("Expected at least one connect", found.size() >= 1);
		found.forEach(pojo ->
		{
			Assert.assertEquals(user.getUserId(), pojo.getUserId());
		});
	}

	@Test
	public void testFindConnectsIndexed()
	{
		UserPojo user = this.findTestUser();
		ServerPojo server = this.findTestServer();
		ServerRuntimePojo runtime = this.createRuntime(server);
		UserConnectPojo connect =
			new UserConnectPojo(0, user.getUserId(), runtime.getRuntimeId(), user.getUserName(), "127.0.0.1", new Date(), null);
		connect.setConnectId(this.dao.getUserDAO().createConnect(connect));
		int maxResults = 10;
		List<UserConnectPojo> found = this.dao.getUserDAO().findConnects(user.getUserId(), 0, maxResults);
		Assert.assertTrue("Returned connects exceeds maxResults", found.size() <= maxResults);
		found.forEach(pojo ->
		{
			Assert.assertEquals(user.getUserId(), pojo.getUserId());
		});
	}

	@Test
	public void testFindConnectsOfUserCount()
	{
		UserPojo user = this.findTestUser();
		ServerPojo server = this.findTestServer();
		ServerRuntimePojo runtime = this.createRuntime(server);
		UserConnectPojo connect =
			new UserConnectPojo(0, user.getUserId(), runtime.getRuntimeId(), user.getUserName(), "127.0.0.1", new Date(), null);
		connect.setConnectId(this.dao.getUserDAO().createConnect(connect));
		int found = this.dao.getUserDAO().findConnectsOfUserCount(user.getUserId());
		Assert.assertTrue("Expected at least 1 connect", found > 0);
	}

	@Test
	public void testFindFirstConnect()
	{
		UserPojo user = this.findTestUser();
		ServerPojo server = this.findTestServer();
		ServerRuntimePojo runtime = this.createRuntime(server);
		UserConnectPojo connect =
			new UserConnectPojo(0, user.getUserId(), runtime.getRuntimeId(), user.getUserName(), "127.0.0.1", new Date(), null);
		connect.setConnectId(this.dao.getUserDAO().createConnect(connect));
		UserConnectPojo found = this.dao.getUserDAO().findFirstConnect(user.getUserId());
		Assert.assertEquals(user.getUserId(), found.getUserId());
	}

	@Test
	public void testFindLatestChatsIndexed()
	{
		UserPojo user = this.findTestUser();
		ServerRuntimePojo runtime = this.createRuntime(this.findTestServer());
		UserConnectPojo connect =
			new UserConnectPojo(0, user.getUserId(), runtime.getRuntimeId(), user.getUserName(), "127.0.0.1", new Date(), null);
		connect.setConnectId(this.dao.getUserDAO().createConnect(connect));
		UserChatPojo chat = new UserChatPojo(0, connect.getConnectId(), new Date(), "Test message", true);
		chat.setChatId(this.dao.getUserDAO().createChat(chat));
		int maxResults = 10;
		List<UserChatPojo> found = this.dao.getUserDAO().findLatestChats(user.getUserId(), 0, maxResults);
		Assert.assertTrue("Expected at least 1 chat", found.size() > 0);
		Assert.assertTrue("Found results exceeds maxResults", found.size() <= maxResults);
	}

	@Test
	public void testFindLatestChatsWithSearchIndexed()
	{
		UserPojo user = this.findTestUser();
		ServerRuntimePojo runtime = this.createRuntime(this.findTestServer());
		UserConnectPojo connect =
			new UserConnectPojo(0, user.getUserId(), runtime.getRuntimeId(), user.getUserName(), "127.0.0.1", new Date(), null);
		connect.setConnectId(this.dao.getUserDAO().createConnect(connect));
		UserChatPojo chat = new UserChatPojo(0, connect.getConnectId(), new Date(), "Test message", true);
		chat.setChatId(this.dao.getUserDAO().createChat(chat));
		int maxResults = 10;
		String search = "T";
		List<UserChatPojo> found = this.dao.getUserDAO().findLatestChats(user.getUserId(), search, 0, maxResults);
		Assert.assertTrue("Expected at least 1 chat", found.size() > 0);
		Assert.assertTrue("Found results exceeds maxResults", found.size() <= maxResults);
		found.forEach(pojo ->
		{
			Assert.assertTrue("Result does not match search criteria", pojo.getMessage().toUpperCase().contains(search));
		});
	}

	@Test
	public void testFindLatestConnectOfUser()
	{
		UserPojo user = this.findTestUser();
		UserConnectPojo found = this.dao.getUserDAO().findLatestConnect(user.getUserId());
		if (found != null)
		{
			Assert.assertEquals(user.getUserId(), found.getUserId());
		}
	}

	@Test
	public void testFindLatestConnects()
	{
		UserPojo user = this.findTestUser();
		int maxResults = 10;
		List<UserConnectPojo> found = this.dao.getUserDAO().findLatestConnects(user.getUserId(), 0, maxResults);
		Assert.assertTrue("Found results count exceeds maxResults", maxResults >= found.size());
		found.forEach(pojo ->
		{
			Assert.assertEquals(user.getUserId(), pojo.getUserId());
		});
	}

	@Test
	public void testFindNewUsers()
	{
		List<UserPojo> found = this.dao.getUserDAO().findNewUsers(new Date());
		Assert.assertNotNull(found);
	}

	@Test
	public void testFindOnlineUserCount()
	{
		int found = this.dao.getUserDAO().findOnlineUserCount(new Date());
		Assert.assertTrue("onlineUsers < 0", found >= 0);
	}

	@Test
	public void testFindOpenConnects()
	{
		ServerPojo server = this.findTestServer();
		List<UserConnectPojo> found = this.dao.getUserDAO().findOpenConnects(server.getServerId());
		found.forEach(pojo ->
		{
			Assert.assertNull(pojo.getQuitDate());
		});
	}

	@Test
	public void testFindUserByUUID()
	{
		UserPojo user = this.findTestUser();
		UserPojo found = this.dao.getUserDAO().findUserByUUID(user.getUUID());
		Assert.assertEquals(user.getUserId(), found.getUserId());
	}

	@Test
	public void testFindUserCount()
	{
		this.findTestUser();
		int found = this.dao.getUserDAO().findUserCount();
		Assert.assertTrue("Expected at least 1 user", found >= 1);
	}

	@Test
	public void testFindUserIPs()
	{
		UserPojo user = this.findTestUser();
		ServerPojo server = this.findTestServer();
		ServerRuntimePojo runtime = this.createRuntime(server);
		UserConnectPojo connect =
			new UserConnectPojo(0, user.getUserId(), runtime.getRuntimeId(), user.getUserName(), "127.0.0.1", new Date(), null);
		this.dao.getUserDAO().createConnect(connect);
		connect = new UserConnectPojo(0, user.getUserId(), runtime.getRuntimeId(), user.getUserName(), "127.0.0.2", new Date(), null);
		this.dao.getUserDAO().createConnect(connect);
		SimplePage<UserIpPojo> found = this.dao.getUserDAO().findUserIps(user.getUserId(), 0, 10);
		Assert.assertTrue("Expected at least 2 results", found.getData().size() >= 2);
		Assert.assertTrue("Expected at most 10 results", found.getData().size() <= 10);
		Assert.assertTrue("TotalResults is incorrect", found.getTotalResults() == found.getData().size() || found.getTotalResults() > 10);
	}

	@Test
	public void testFindUsers()
	{
		this.findTestUser();
		List<UserPojo> found = this.dao.getUserDAO().findUsers();
		Assert.assertTrue("Expected at least 1 user", found.size() >= 1);
	}

	@Test
	public void testFindUsersActivity()
	{
		List<UserActivityPojo> found = this.dao.getUserDAO().findUsersActivity(0, 10);
		Assert.assertTrue("Expected at most 10 items", found.size() <= 10);
	}

	@Test
	public void testFindUsersByIp()
	{
		UserPojo user = this.findTestUser();
		ServerRuntimePojo rt = this.createRuntime(this.findTestServer());
		UserConnectPojo connect =
			new UserConnectPojo(0, user.getUserId(), rt.getRuntimeId(), user.getUserName(), "127.0.0.1", new Date(), null);
		connect.setConnectId(this.dao.getUserDAO().createConnect(connect));
		SimplePage<UserIpPojo> found = this.getDAO().getUserDAO().findUsersByIp("127.0.0.1", 0, 10);
		Assert.assertTrue("Expected at least 1 user", found.getData().size() >= 1);
		Assert.assertTrue("Expected at most 10 users", found.getData().size() <= 10);
	}

	@Test
	public void testFindUsersByNameCount()
	{
		UserPojo user = this.findTestUser();
		int found = this.dao.getUserDAO().findUsersByNameCount(user.getUserName());
		Assert.assertTrue("Expected at least 1 user", found >= 1);
	}

	@Test
	public void testFindUsersByNameIndexed()
	{
		UserPojo user = this.findTestUser();
		int maxResults = 10;
		String startsWith = user.getUserName().substring(0, 1);
		List<UserPojo> found = this.dao.getUserDAO().findUsersByName(startsWith, 0, maxResults);
		Assert.assertTrue("Expected at least 1 user", found.size() >= 1);
		Assert.assertTrue("Results count exceed maxResults", found.size() <= maxResults);
		found.forEach(userFound ->
		{
			Assert.assertTrue("Found user name " + userFound.getUserName() + " does not start with " + startsWith,
				StringUtils.startsWithIgnoreCase(userFound.getUserName(), startsWith));
		});
	}

	@Test
	public void testFindUsersIndexed()
	{
		this.findTestUser();
		int maxResults = 10;
		List<UserPojo> found = this.dao.getUserDAO().findUsers(0, maxResults);
		Assert.assertTrue("Expected at least 1 user", found.size() >= 1);
		Assert.assertTrue("Results count exceed maxResults", found.size() <= maxResults);
	}

	@Test
	public void testFindUsersWithLatestIp()
	{
		UserPojo user = this.findTestUser();
		ServerPojo server = this.findTestServer();
		ServerRuntimePojo runtime = this.createRuntime(server);
		UserConnectPojo connect =
			new UserConnectPojo(0, user.getUserId(), runtime.getRuntimeId(), user.getUserName(), "127.0.0.1", new Date(), null);
		connect.setConnectId(this.dao.getUserDAO().createConnect(connect));
		user.setConnectId(connect.getConnectId());
		this.getDAO().getUserDAO().updateUser(user);
		List<UserPojo> found = this.getDAO().getUserDAO().findUsersWithLatestIp("127.0.0.1");
		UserPojo userFound = found.stream().filter(u -> u.getUserId() == user.getUserId()).findAny().orElse(null);
		Assert.assertNotNull(userFound);
	}

	@Test
	public void testOnlineUsersOfServer()
	{
		ServerPojo server = this.findTestServer();
		List<UserPojo> found = this.dao.getUserDAO().findOnlineUsersOfServer(server.getServerId());
		Assert.assertNotNull(found);
	}

	@Test
	public void testUpdateConnect()
	{
		UserPojo user = this.findTestUser();
		ServerPojo server = this.findTestServer();
		ServerRuntimePojo runtime = this.createRuntime(server);
		UserConnectPojo connect =
			new UserConnectPojo(0, user.getUserId(), runtime.getRuntimeId(), user.getUserName(), "127.0.0.1", new Date(), null);
		connect.setConnectId(this.dao.getUserDAO().createConnect(connect));
		connect.setQuitDate(new Date());
		this.dao.getUserDAO().updateConnect(connect);
		UserConnectPojo found = this.dao.getUserDAO().findConnect(connect.getConnectId());
		Assert.assertNotNull(found.getQuitDate());
	}

	@Test
	public void testUpdateUser()
	{
		UserPojo user = this.findTestUser();
		user.setUserName("Test" + System.currentTimeMillis());
		user.setUUID(UUID.randomUUID());
		this.dao.getUserDAO().updateUser(user);
		UserPojo found = this.dao.getUserDAO().findUserById(user.getUserId());
		Assert.assertEquals(user.getUserId(), found.getUserId());
		Assert.assertEquals(user.getUserName(), found.getUserName());
		Assert.assertEquals(user.getUUID(), found.getUUID());
	}

	@Test
	public void testUpdateUserLanguage()
	{
		UserPojo user = this.findTestUser();
		LanguagePojo language = this.findTestLanguage();
		UserLanguagePojo userLanguage = this.dao.getUserDAO().findUserLanguage(user.getUserId(), language.getLanguageId());
		if (userLanguage == null)
		{
			userLanguage = new UserLanguagePojo(user.getUserId(), language.getLanguageId(), 0);
			this.dao.getUserDAO().createUserLanguage(userLanguage);
		}
		userLanguage.setPriority(userLanguage.getPriority() + 1);
		this.dao.getUserDAO().updateUserLanguage(userLanguage);
		UserLanguagePojo found = this.dao.getUserDAO().findUserLanguage(user.getUserId(), language.getLanguageId());
		Assert.assertEquals(userLanguage.getPriority(), found.getPriority());
	}

	private ServerRuntimePojo createRuntime(ServerPojo server)
	{
		ServerRuntimePojo pojo = new ServerRuntimePojo(0, server.getServerId(), new Date(), new Date(), null);
		pojo.setRuntimeId(this.dao.getServerDAO().createRuntime(pojo));
		return pojo;
	}

	private LanguagePojo findTestLanguage()
	{
		LanguagePojo language = this.dao.getMessageDAO().findLanguage("Test");
		if (language == null)
		{
			language = new LanguagePojo(0, "Test");
			language.setLanguageId(this.dao.getMessageDAO().createLanguage(language));
		}
		return language;
	}

	private ServerPojo findTestServer()
	{
		ServerPojo pojo = this.dao.getServerDAO().findServer("Test");
		if (pojo == null)
		{
			pojo = new ServerPojo(0, "Test", ServerType.SPIGOT, false, false, null, null, null, null);
			pojo.setServerId(this.dao.getServerDAO().createServer(pojo));
		}
		return pojo;
	}

	private UserPojo findTestUser()
	{
		UserPojo pojo = this.dao.getUserDAO().findUserByTypeAndName(UserType.MINECRAFT_PLAYER, "Test");
		if (pojo == null)
		{
			pojo = new UserPojo(0, UserType.MINECRAFT_PLAYER, UUID.randomUUID(), "Test", null, true, true);
			pojo.setUserId(this.dao.getUserDAO().createUser(pojo));
		}
		return pojo;
	}
}
