package net.rieksen.networkcore.core.dao.mysql;

import net.rieksen.networkcore.core.dao.MessageDAO;
import net.rieksen.networkcore.core.dao.PluginDAO;
import net.rieksen.networkcore.core.dao.TestMessageDAO;

public class TestMySQLMessageDAO extends TestMessageDAO
{

	@Override
	protected MessageDAO getMessageDAO()
	{
		return TestMySQLDAO.getDAOInstance().getMessageDAO();
	}

	@Override
	protected PluginDAO getPluginDAO()
	{
		return TestMySQLDAO.getDAOInstance().getPluginDAO();
	}
}
