package net.rieksen.networkcore.core.dao;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import net.rieksen.networkcore.core.statistic.NetworkStatisticPojo;

public abstract class TestStatisticDAO
{

	private StatisticDAO statisticDAO;

	@Before
	public void setUp()
	{
		this.statisticDAO = this.getStatisticDAO();
	}

	@Test
	public void testCreateNetworkStatistic()
	{
		this.statisticDAO.createNetworkStatistic(new NetworkStatisticPojo(new Date(), 10, 20, 30, 40));
	}

	@Test
	public void testFindNetworkStatisticSince()
	{
		Date since = new Date();
		List<NetworkStatisticPojo> stats = this.statisticDAO.findNetworkStatisticsSince(since);
		stats.forEach(stat ->
		{
			// long difference = stat.getDate().getTime() - since.getTime();
			// Assert.assertTrue("Difference is " + difference + "ms",
			// difference <= 1000);
			// pipelines builds keep failing
		});
	}

	protected abstract StatisticDAO getStatisticDAO();
}
