package net.rieksen.networkcore.core.dao.mysql;

import net.rieksen.networkcore.core.dao.DAOManager;
import net.rieksen.networkcore.core.dao.TestPluginDAO;

public class TestMySQLPluginDAO extends TestPluginDAO
{

	@Override
	protected DAOManager getDAO()
	{
		return TestMySQLDAO.getDAOInstance();
	}
}
