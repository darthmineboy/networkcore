package net.rieksen.networkcore.core.dao;

import java.util.Date;
import java.util.List;

import org.junit.Test;

import junit.framework.Assert;
import net.rieksen.networkcore.core.plugin.pojo.NetworkPluginPojo;
import net.rieksen.networkcore.core.plugin.pojo.PluginTaskPojo;

public abstract class TestPluginDAO extends AbstractTestBaseDAO
{

	@Test
	public void testCreatePlugin()
	{
		NetworkPluginPojo plugin = new NetworkPluginPojo(0, "Test-" + System.currentTimeMillis(), null, null);
		plugin.setPluginId(this.dao.getPluginDAO().createPlugin(plugin));
		NetworkPluginPojo found = this.dao.getPluginDAO().findPlugin(plugin.getPluginId());
		Assert.assertEquals(plugin.getPluginId(), found.getPluginId());
		Assert.assertEquals(plugin.getName(), found.getName());
		Assert.assertEquals(plugin.getDatabaseVersion(), found.getDatabaseVersion());
	}

	@Test
	public void testDeletePluginTask()
	{
		NetworkPluginPojo plugin = this.findTestPlugin();
		PluginTaskPojo task = this.dao.getPluginDAO().findPluginTask(plugin.getPluginId(), "Test");
		if (task == null)
		{
			task =
				new PluginTaskPojo(plugin.getPluginId(), "Test", 10, new Date(), PluginTaskPojo.ExecutionStatus.SUCCESS, null, null, false);
			this.dao.getPluginDAO().createPluginTask(task);
		}
		PluginTaskPojo found = this.dao.getPluginDAO().findPluginTask(plugin.getPluginId(), "Test");
		Assert.assertEquals(task.getPluginId(), found.getPluginId());
		Assert.assertEquals(task.getTaskName(), found.getTaskName());
		this.dao.getPluginDAO().deletePluginTask(plugin.getPluginId(), task.getTaskName());
		task = this.dao.getPluginDAO().findPluginTask(plugin.getPluginId(), "Test");
		Assert.assertNull(task);
	}

	@Test
	public void testFindPluginTask()
	{
		NetworkPluginPojo plugin = this.findTestPlugin();
		PluginTaskPojo task = this.dao.getPluginDAO().findPluginTask(plugin.getPluginId(), "Test");
		if (task == null)
		{
			task =
				new PluginTaskPojo(plugin.getPluginId(), "Test", 10, new Date(), PluginTaskPojo.ExecutionStatus.SUCCESS, null, null, true);
			this.dao.getPluginDAO().createPluginTask(task);
		}
		PluginTaskPojo found = this.dao.getPluginDAO().findPluginTask(plugin.getPluginId(), "Test");
		Assert.assertEquals(task.getPluginId(), found.getPluginId());
		Assert.assertEquals(task.getTaskName(), found.getTaskName());
	}

	@Test
	public void testFindPluginTasks()
	{
		NetworkPluginPojo plugin = this.findTestPlugin();
		PluginTaskPojo task = this.dao.getPluginDAO().findPluginTask(plugin.getPluginId(), "Test");
		if (task == null)
		{
			task =
				new PluginTaskPojo(plugin.getPluginId(), "Test", 10, new Date(), PluginTaskPojo.ExecutionStatus.SUCCESS, null, null, true);
			this.dao.getPluginDAO().createPluginTask(task);
		}
		List<PluginTaskPojo> found = this.dao.getPluginDAO().findPluginTasks(plugin.getPluginId());
		Assert.assertTrue("Expected at least 1 task", found.size() > 0);
	}

	protected NetworkPluginPojo findTestPlugin()
	{
		NetworkPluginPojo plugin = this.dao.getPluginDAO().findPlugin("Test");
		if (plugin == null)
		{
			plugin = new NetworkPluginPojo(0, "Test", null, null);
			plugin.setPluginId(this.dao.getPluginDAO().createPlugin(plugin));
		}
		return plugin;
	}
}
