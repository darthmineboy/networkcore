package net.rieksen.networkcore.core.dao.mysql;

import net.rieksen.networkcore.core.dao.DAOManager;
import net.rieksen.networkcore.core.dao.TestTaskDAO;

public class TestMySQLTaskDAO extends TestTaskDAO
{

	@Override
	protected DAOManager getDAO()
	{
		return TestMySQLDAO.getDAOInstance();
	}
}
