package net.rieksen.networkcore.core.dao;

import java.util.Date;
import java.util.List;

import org.junit.Test;

import junit.framework.Assert;
import net.rieksen.networkcore.core.issue.IssuePojo;
import net.rieksen.networkcore.core.util.TestUtil;

public abstract class TestTaskDAO extends AbstractTestBaseDAO
{

	@Test
	public void findUnresolvedIssuesOfGenerator()
	{
		String generator = "TestGen";
		int pluginId = this.createTestPlugin().getPluginId();
		IssuePojo issue = new IssuePojo(0, pluginId, "Test-" + System.currentTimeMillis(), 0, new Date(), "Please complete this issue",
			generator, false, null);
		issue = this.dao.getIssueDAO().createIssue(issue);
		List<IssuePojo> found = this.dao.getIssueDAO().findUnresolvedIssuesOfGenerator(pluginId, generator);
		Assert.assertFalse("Expected at least 1 result", found.isEmpty());
		for (IssuePojo i : found)
		{
			Assert.assertFalse("Issue is resolved", i.isResolved());
			Assert.assertEquals(generator, issue.getIssueGenerator());
		}
	}

	@Test
	public void testCreateAndFindIssue()
	{
		IssuePojo issue = this.createIssue();
		IssuePojo found = this.dao.getIssueDAO().findIssue(issue.getIssueId());
		TestUtil.assertPojoEquals(issue, found);
	}

	private IssuePojo createIssue()
	{
		IssuePojo issue = new IssuePojo(0, this.createTestPlugin().getPluginId(), "Test-" + System.currentTimeMillis(), 0, new Date(),
			"Please complete this issue", null, false, null);
		issue = this.dao.getIssueDAO().createIssue(issue);
		return issue;
	}
}
