package net.rieksen.networkcore.core.dao;

import java.util.List;

import org.junit.Test;

import junit.framework.Assert;
import net.rieksen.networkcore.core.option.OptionType;
import net.rieksen.networkcore.core.option.pojo.OptionPojo;
import net.rieksen.networkcore.core.option.pojo.OptionSectionPojo;
import net.rieksen.networkcore.core.option.pojo.OptionValuePojo;
import net.rieksen.networkcore.core.plugin.pojo.NetworkPluginPojo;

public abstract class TestOptionDAO extends AbstractTestBaseDAO
{

	@Test
	public void testCreateOptionValue()
	{
		OptionPojo pojo = this.createTestOption();
		OptionValuePojo value = new OptionValuePojo(0, pojo.getOptionId(), null, 0, "Hello");
		value.setValueId(this.getDAO().getOptionDAO().createValue(value));
		List<OptionValuePojo> found = this.getDAO().getOptionDAO().findValues(pojo.getOptionId());
		Assert.assertEquals(1, found.size());
		Assert.assertEquals(value.getValue(), found.get(0).getValue());
	}

	@Test
	public void testCreateSection()
	{
		OptionSectionPojo pojo = this.createTestSection();
		OptionSectionPojo found = this.getDAO().getOptionDAO().findSection(pojo.getSectionId());
		Assert.assertEquals(pojo.getSectionId(), found.getSectionId());
		Assert.assertEquals(pojo.getPluginId(), found.getPluginId());
		Assert.assertEquals(pojo.getName(), found.getName());
		Assert.assertEquals(pojo.getDescription(), found.getDescription());
	}

	@Test
	public void testDeleteOption()
	{
		OptionPojo pojo = this.createTestOption();
		this.getDAO().getOptionDAO().deleteOption(pojo.getOptionId());
		OptionPojo found = this.getDAO().getOptionDAO().findOption(pojo.getOptionId());
		Assert.assertNull(found);
	}

	@Test
	public void testDeleteSection()
	{
		OptionSectionPojo section = this.createTestSection();
		this.getDAO().getOptionDAO().deleteSection(section.getSectionId());
		OptionSectionPojo found = this.getDAO().getOptionDAO().findSection(section.getSectionId());
		Assert.assertNull(found);
	}

	@Test
	public void testFindOptionByName()
	{
		OptionPojo pojo = this.createTestOption();
		OptionPojo found = this.getDAO().getOptionDAO().findOption(pojo.getSectionId(), pojo.getName());
		Assert.assertEquals(pojo.getOptionId(), found.getOptionId());
	}

	@Test
	public void testFindOptionsInSection()
	{
		OptionPojo pojo = this.createTestOption();
		List<OptionPojo> found = this.getDAO().getOptionDAO().findOptions(pojo.getSectionId());
		Assert.assertFalse("Expected at least 1 option", found.isEmpty());
	}

	@Test
	public void testFindSectionByPluginAndName()
	{
		OptionSectionPojo section = this.createTestSection();
		OptionSectionPojo found = this.getDAO().getOptionDAO().findSection(section.getPluginId(), section.getName());
		Assert.assertEquals(section.getSectionId(), found.getSectionId());
	}

	@Test
	public void testFindSectionCountOfPlugin()
	{
		OptionSectionPojo section = this.createTestSection();
		int found = this.getDAO().getOptionDAO().findSectionCount(section.getPluginId());
		Assert.assertTrue("Expected at least 1 section", found >= 1);
	}

	@Test
	public void testFindSections()
	{
		this.createTestSection();
		int found = this.getDAO().getOptionDAO().findSections().size();
		Assert.assertTrue(found >= 1);
	}

	@Test
	public void testFindSectionsOfPlugin()
	{
		OptionSectionPojo section = this.createTestSection();
		List<OptionSectionPojo> found = this.getDAO().getOptionDAO().findSections(section.getPluginId());
		Assert.assertFalse(found.isEmpty());
		for (OptionSectionPojo e : found)
		{
			Assert.assertEquals(section.getPluginId(), e.getPluginId());
		}
	}

	private OptionPojo createTestOption()
	{
		OptionSectionPojo section = this.createTestSection();
		OptionPojo pojo =
			new OptionPojo(0, section.getSectionId(), "Test-" + System.currentTimeMillis(), OptionType.BOOLEAN, "Test Description");
		pojo.setOptionId(this.getDAO().getOptionDAO().createOption(pojo));
		return pojo;
	}

	private OptionSectionPojo createTestSection()
	{
		NetworkPluginPojo plugin = this.createTestPlugin();
		OptionSectionPojo pojo = new OptionSectionPojo(0, plugin.getPluginId(), "Test-" + System.currentTimeMillis(), "Test Description");
		pojo.setSectionId(this.getDAO().getOptionDAO().createSection(pojo));
		return pojo;
	}
}
