package net.rieksen.networkcore.core.dao.mysql;

import net.rieksen.networkcore.core.dao.DAOManager;
import net.rieksen.networkcore.core.dao.TestOptionDAO;

public class TestMySQLOptionDAO extends TestOptionDAO
{

	@Override
	protected DAOManager getDAO()
	{
		return TestMySQLDAO.getDAOInstance();
	}
}
