package net.rieksen.networkcore.core.dao;

import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;
import net.rieksen.networkcore.core.info.NetworkCoreInfo;

public abstract class TestInfoDAO
{

	private InfoDAO infoDAO;

	@Before
	public void setUp()
	{
		this.infoDAO = this.getInfoDAO();
	}

	@Test
	public void testFindAllInfo()
	{
		this.infoDAO.setInfo(NetworkCoreInfo.NETWORK_BASE_URL, "https://");
		this.infoDAO.setInfo(NetworkCoreInfo.NETWORK_ID, "10");
		Assert.assertTrue(this.infoDAO.findAllInfo().size() >= 2);
	}

	@Test
	public void testSetInfo()
	{
		String expected = "10";
		NetworkCoreInfo key = NetworkCoreInfo.NETWORK_ID;
		this.infoDAO.setInfo(key, expected);
		String actual = this.infoDAO.findInfo(key);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testSetInfoNull()
	{
		String expected = null;
		NetworkCoreInfo key = NetworkCoreInfo.NETWORK_ID;
		this.infoDAO.setInfo(key, expected);
		String actual = this.infoDAO.findInfo(key);
		Assert.assertEquals(expected, actual);
	}

	protected abstract InfoDAO getInfoDAO();
}
