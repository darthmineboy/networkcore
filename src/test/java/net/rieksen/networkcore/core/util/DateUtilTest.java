package net.rieksen.networkcore.core.util;

import java.util.Date;

import org.junit.Test;

import junit.framework.Assert;

public class DateUtilTest
{

	@Test
	public void differenceLessThan_equal()
	{
		Date date = new Date();
		Assert.assertTrue(DateUtil.differenceLessThan(date, date, 1000));
	}

	@Test
	public void differenceLessThan_firstNull()
	{
		Assert.assertFalse(DateUtil.differenceLessThan(null, new Date(999), 1000));
	}

	@Test
	public void differenceLessThan_less()
	{
		Assert.assertTrue(DateUtil.differenceLessThan(new Date(1), new Date(1000), 1000));
	}

	@Test
	public void differenceLessThan_more()
	{
		Assert.assertFalse(DateUtil.differenceLessThan(new Date(1), new Date(1001), 1000));
	}

	@Test
	public void differenceLessThan_secondNull()
	{
		Assert.assertFalse(DateUtil.differenceLessThan(new Date(999), null, 1000));
	}

	@Test
	public void getDifference_chronological()
	{
		Date old = new Date(1000);
		Date newDate = new Date(2000);
		Assert.assertEquals(1000, DateUtil.getDifference(old, newDate));
	}

	@Test
	public void getDifference_null()
	{
		Assert.assertEquals(Long.MAX_VALUE, DateUtil.getDifference(new Date(), null));
		Assert.assertEquals(Long.MAX_VALUE, DateUtil.getDifference(null, new Date()));
	}

	@Test
	public void getDifference_reverse_chronological()
	{
		Date old = new Date(1000);
		Date newDate = new Date(2000);
		Assert.assertEquals(1000, DateUtil.getDifference(newDate, old));
	}

	@Test
	public void getDifference_sameDate()
	{
		Date date = new Date();
		Assert.assertEquals(0, DateUtil.getDifference(date, date));
	}
}
