package net.rieksen.networkcore.core.util;

import org.junit.Assert;
import org.junit.Test;

public class TestPluginUtil
{

	@Test
	public void isVersionGreater_emptyString()
	{
		// Some plugin developers think it's a good idea to have an empty
		// version string :|, this caused a NumberFormatException
		String oldVersion = "";
		String newVersion = "";
		Assert.assertFalse(PluginUtil.isVersionGreater(oldVersion, newVersion));
	}

	@Test
	public void testSnapshotOlderThanRelease()
	{
		String oldVersion = "2.1.3-SNAPSHOT";
		String newVersion = "2.1.3";
		Assert.assertTrue(PluginUtil.isVersionGreater(oldVersion, newVersion));
	}

	@Test
	public void testVersionGreaterEqual()
	{
		String version = "1.0";
		Assert.assertFalse(PluginUtil.isVersionGreater(version, version));
	}

	@Test
	public void testVersionGreaterFalse()
	{
		String oldVersion = "2.0";
		String newVersion = "1.0";
		Assert.assertFalse(PluginUtil.isVersionGreater(oldVersion, newVersion));
	}

	@Test
	public void testVersionGreaterFalseExtra()
	{
		String oldVersion = "2.0";
		String newVersion = "1.1.4";
		Assert.assertFalse(PluginUtil.isVersionGreater(oldVersion, newVersion));
	}

	@Test
	public void testVersionGreaterTrue()
	{
		String oldVersion = "1.9";
		String newVersion = "1.9.1";
		Assert.assertTrue(PluginUtil.isVersionGreater(oldVersion, newVersion));
	}

	@Test
	public void testVersionGreaterTrueExtra()
	{
		String oldVersion = "1.1.4";
		String newVersion = "2.0";
		Assert.assertTrue(PluginUtil.isVersionGreater(oldVersion, newVersion));
	}
}
