package net.rieksen.networkcore.core.util;

import java.lang.reflect.Field;

import junit.framework.Assert;

public class TestUtil
{

	/**
	 * Checks if the fields of both objects are equal.
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static void assertPojoEquals(Object a, Object b)
	{
		if (a == b) { throw new IllegalStateException("Comparing the same instance, of course it's equal!"); }
		try
		{
			Assert.assertEquals("Class mismatch", a.getClass(), b.getClass());
			for (Field f : a.getClass().getFields())
			{
				f.setAccessible(true);
				Assert.assertEquals("Field " + f.getName() + " mismatch", f.get(a), f.get(b));
			}
		} catch (Exception e)
		{
			throw new RuntimeException(e.getMessage(), e);
		}
	}
}
