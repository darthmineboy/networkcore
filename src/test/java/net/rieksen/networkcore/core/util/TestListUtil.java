package net.rieksen.networkcore.core.util;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;
import junit.framework.TestCase;
import net.rieksen.networkcore.core.util.ListUtil;

import org.junit.Test;

public class TestListUtil extends TestCase
{

	@Test
	public void testGetSubList()
	{
		List<Integer> list = new ArrayList<>();
		for (int i = 0; i < 100; i++)
		{
			list.add(i);
		}
		List<Integer> sublist = ListUtil.getSubList(list, 0, 10);
		Assert.assertEquals(10, sublist.size());
		Assert.assertEquals(0, (int) sublist.get(0));
		Assert.assertEquals(9, (int) sublist.get(9));
		//
		sublist = ListUtil.getSubList(list, 1, 10);
		Assert.assertEquals(10, sublist.size());
		Assert.assertEquals(10, (int) sublist.get(0));
		Assert.assertEquals(19, (int) sublist.get(9));
		//
		sublist = ListUtil.getSubList(list, 10, 10);
		Assert.assertEquals(0, sublist.size());
	}
}
