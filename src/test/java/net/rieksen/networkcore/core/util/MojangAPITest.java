package net.rieksen.networkcore.core.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.meanbean.test.BeanTester;

import junit.framework.Assert;
import net.rieksen.networkcore.core.util.MojangAPI.NameHistory;

public class MojangAPITest
{

	@Test
	public void beanTest()
	{
		new BeanTester().testBean(NameHistory.class);
	}

	@Test
	public void nameHistory_comparator()
	{
		List<NameHistory> history = Arrays.asList(new NameHistory("Piet", new Date(56)), new NameHistory("Henk", null),
			new NameHistory("Griet", new Date(20)), new NameHistory("Bom", new Date(70)));
		Collections.sort(history, NameHistory.comparator());
		Assert.assertEquals("Henk", history.get(0).getName());
		Assert.assertEquals("Griet", history.get(1).getName());
		Assert.assertEquals("Piet", history.get(2).getName());
		Assert.assertEquals("Bom", history.get(3).getName());
	}
}
