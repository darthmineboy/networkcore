package net.rieksen.networkcore.core.web;

import org.junit.Test;
import org.meanbean.test.BeanTester;

public class TestPojo
{

	@Test
	public void testInstallation()
	{
		new BeanTester().testBean(InstallationPojo.class);
	}

	@Test
	public void testLicenseRequest()
	{
		new BeanTester().testBean(LicenseRequestPojo.class);
	}

	@Test
	public void testResource()
	{
		new BeanTester().testBean(ResourcePojo.class);
	}

	@Test
	public void testResourceLicense()
	{
		new BeanTester().testBean(ResourceLicensePojo.class);
	}

	@Test
	public void testResourceVersion()
	{
		new BeanTester().testBean(ResourceVersionPojo.class);
	}
}
