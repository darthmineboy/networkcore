package net.rieksen.networkcore.core.statistic;

import org.junit.Test;
import org.meanbean.test.BeanTester;

public class TestPojo
{

	@Test
	public void testNetworkMonthlyUsersStatistic()
	{
		new BeanTester().testBean(NetworkMonthlyUsersStatisticPojo.class);
	}

	@Test
	public void testNetworkStatistic()
	{
		new BeanTester().testBean(NetworkStatisticPojo.class);
	}
}
