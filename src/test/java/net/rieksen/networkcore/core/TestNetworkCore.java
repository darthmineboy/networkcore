package net.rieksen.networkcore.core;

import java.util.logging.Logger;

import co.aikar.taskchain.TaskChainFactory;
import net.rieksen.networkcore.core.dao.DAOManager;
import net.rieksen.networkcore.core.enums.PluginStatus;
import net.rieksen.networkcore.core.info.InfoModule;
import net.rieksen.networkcore.core.issue.IssueModule;
import net.rieksen.networkcore.core.message.MessageModule;
import net.rieksen.networkcore.core.network.NetworkModule;
import net.rieksen.networkcore.core.option.OptionModule;
import net.rieksen.networkcore.core.plugin.NetworkPlugin;
import net.rieksen.networkcore.core.plugin.PluginModule;
import net.rieksen.networkcore.core.server.ServerModule;
import net.rieksen.networkcore.core.socket.ServerSocketModule;
import net.rieksen.networkcore.core.user.UserModule;
import net.rieksen.networkcore.core.util.MojangAPI;
import net.rieksen.networkcore.core.world.WorldModule;
import net.rieksen.networkcore.spigot.file.AutomaticServerConfig;

public class TestNetworkCore implements NetworkCore
{

	@Override
	public void debug(String message)
	{}

	@Override
	public AutomaticServerConfig getAutomaticConfig()
	{
		return null;
	}

	@Override
	public DAOManager getDAO()
	{
		return null;
	}

	@Override
	public InfoModule getInfoModule()
	{
		return null;
	}

	@Override
	public Logger getLogger()
	{
		return Logger.getGlobal();
	}

	@Override
	public MessageModule getMessageModule()
	{
		return null;
	}

	@Override
	public MojangAPI getMojangAPI()
	{
		return null;
	}

	@Override
	public NetworkModule getNetworkModule()
	{
		return null;
	}

	@Override
	public NetworkPlugin getNetworkPlugin()
	{
		return null;
	}

	@Override
	public OptionModule getOptionModule()
	{
		return null;
	}

	@Override
	public PluginModule getPluginModule()
	{
		return null;
	}

	@Override
	public ServerModule getServerModule()
	{
		return null;
	}

	@Override
	public ServerSocketModule getSocketModule()
	{
		return null;
	}

	@Override
	public PluginStatus getStatus()
	{
		return null;
	}

	@Override
	public TaskChainFactory getTaskChainFactory()
	{
		return null;
	}

	@Override
	public IssueModule getIssueModule()
	{
		return null;
	}

	@Override
	public UserModule getUserModule()
	{
		return null;
	}

	@Override
	public WorldModule getWorldModule()
	{
		return null;
	}
}
