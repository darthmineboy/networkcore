package net.rieksen.networkcore.core.user.note;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.mockito.Mockito;

import net.rieksen.networkcore.core.NetworkCore;

public class UserNoteImplTest
{

	private UserNotePojo	pojo;
	private UserNoteImpl	note;

	public void constructor()
	{
		Assert.assertEquals(this.pojo.getId(), this.note.getId());
		Assert.assertEquals(this.pojo.getUserId(), this.note.getUserId());
		Assert.assertEquals(this.pojo.getCreatedById(), this.note.getCreatedById());
		Assert.assertEquals(this.pojo.getCreationDate(), this.note.getCreationDate());
		Assert.assertEquals(this.pojo.getMessage(), this.note.getMessage());
	}

	@Before
	public void setUp()
	{
		this.pojo = new UserNotePojo(1, 2, 3, new Date(), "Hello");
		this.note = new UserNoteImpl(Mockito.mock(NetworkCore.class), this.pojo);
	}
}
