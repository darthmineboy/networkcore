package net.rieksen.networkcore.core.user;

import java.util.UUID;

import org.junit.Test;
import org.meanbean.lang.Factory;
import org.meanbean.test.BeanTester;
import org.meanbean.test.Configuration;
import org.meanbean.test.ConfigurationBuilder;

import net.rieksen.networkcore.core.user.pojo.UserActivityPojo;
import net.rieksen.networkcore.core.user.pojo.UserChatPojo;
import net.rieksen.networkcore.core.user.pojo.UserConnectPojo;
import net.rieksen.networkcore.core.user.pojo.UserLanguagePojo;
import net.rieksen.networkcore.core.user.pojo.UserNameHistoryPojo;
import net.rieksen.networkcore.core.user.pojo.UserPojo;

public class TestPojo
{

	@Test
	public void testUser()
	{
		Configuration config = new ConfigurationBuilder().overrideFactory("UUID", new Factory<UUID>()
		{

			@Override
			public UUID create()
			{
				return UUID.randomUUID();
			}
		}).build();
		new BeanTester().testBean(UserPojo.class, config);
	}

	@Test
	public void testUserActivity()
	{
		new BeanTester().testBean(UserActivityPojo.class);;
	}

	@Test
	public void testUserChat()
	{
		new BeanTester().testBean(UserChatPojo.class);
	}

	@Test
	public void testUserConnect()
	{
		new BeanTester().testBean(UserConnectPojo.class);
	}

	@Test
	public void testUserLanguage()
	{
		new BeanTester().testBean(UserLanguagePojo.class);
	}

	@Test
	public void testUserNameHistory()
	{
		new BeanTester().testBean(UserNameHistoryPojo.class);
	}
}
