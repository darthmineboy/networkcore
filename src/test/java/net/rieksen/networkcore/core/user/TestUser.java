package net.rieksen.networkcore.core.user;

import java.util.UUID;

import org.bukkit.entity.Player;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import junit.framework.Assert;
import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.message.UserLanguagePreferences;

public class TestUser
{

	private User		user;
	private NetworkCore	provider;
	private int			userId;
	private UserType	type;
	private UUID		uuid;
	private String		name;

	@Before
	public void setup()
	{
		this.provider = Mockito.mock(NetworkCore.class);
		this.userId = 20;
		this.type = UserType.MINECRAFT_PLAYER;
		this.uuid = UUID.randomUUID();
		this.name = "Darthmineboy";
		this.user = new UserImpl(this.provider, this.userId, this.type, this.uuid, this.name, null, true, true);
	}

	@Test
	public void testConstructor()
	{
		Assert.assertEquals(this.userId, this.user.getUserId());
		Assert.assertEquals(this.type, this.user.getType());
		Assert.assertEquals(this.uuid, this.user.getUUID());
		Assert.assertEquals(this.name, this.user.getName());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNameNull()
	{
		this.user.changeName(null);
	}

	@Test
	public void testPlayer()
	{
		Player player = Mockito.mock(Player.class);
		Mockito.when(player.isOnline()).thenReturn(true);
		this.user.setPlayer(player);
		Assert.assertEquals(player, this.user.getPlayer());
	}

	@Test
	public void testUserLanguagePreferences()
	{
		UserLanguagePreferences pref = Mockito.mock(UserLanguagePreferences.class);
		this.user.setLanguagePreferences(pref);
		Assert.assertEquals(pref, this.user.getLanguagePreferences());
	}
}
