package net.rieksen.networkcore.core.message;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import junit.framework.Assert;
import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.message.pojo.GlobalMessageVariablePojo;
import net.rieksen.networkcore.core.message.pojo.MessagePojo;
import net.rieksen.networkcore.core.message.pojo.MessageSectionPojo;
import net.rieksen.networkcore.core.message.pojo.MessageTranslationPojo;
import net.rieksen.networkcore.core.message.pojo.MessageVariablePojo;

public class MessageFactoryImplTest
{

	private NetworkCore			provider;
	private MessageFactoryImpl	factory;

	@Test
	public void createGlobalVariable()
	{
		GlobalMessageVariablePojo pojo = new GlobalMessageVariablePojo(1, "Test", "Replacement");
		GlobalMessageVariable result = this.factory.createGlobalVariable(pojo);
		Assert.assertEquals(pojo.getVariableId(), result.getVariableId());
		Assert.assertEquals(pojo.getName(), result.getName());
		Assert.assertEquals(pojo.getReplacement(), result.getReplacement());
	}

	@Test
	public void createMessage()
	{
		MessagePojo pojo = new MessagePojo(1, 2, "Test", "Description");
		Message result = this.factory.createMessage(pojo);
		Assert.assertEquals(pojo.getMessageId(), result.getMessageId());
		Assert.assertEquals(pojo.getSectionId(), result.getSectionId());
		Assert.assertEquals(pojo.getName(), result.getName());
		Assert.assertEquals(pojo.getDescription(), result.getDescription());
	}

	@Test
	public void createSection()
	{
		MessageSectionPojo pojo = new MessageSectionPojo(1, 2, "Name", "Description");
		MessageSection result = this.factory.createSection(pojo);
		Assert.assertEquals(pojo.getSectionId(), result.getSectionId());
		Assert.assertEquals(pojo.getPluginId(), result.getPluginId());
		Assert.assertEquals(pojo.getName(), result.getName());
		Assert.assertEquals(pojo.getDescription(), result.getDescription());
	}

	@Test
	public void createTranslation()
	{
		MessageTranslationPojo pojo = new MessageTranslationPojo(1, 2, "Message", MessageType.REGULAR, "Default message",
			MessageType.REGULAR, false, false, new Date());
		MessageTranslation result = this.factory.createTranslation(pojo);
		Assert.assertEquals(pojo.getMessageId(), result.getMessageId());
		Assert.assertEquals(pojo.getLanguageId(), result.getLanguageId());
		Assert.assertEquals(pojo.getMessage(), result.getMessage());
		Assert.assertEquals(pojo.getMessageType(), result.getMessageType());
		Assert.assertEquals(pojo.getDefaultMessage(), result.getDefaultMessage());
		Assert.assertEquals(pojo.useDefaultMessage(), result.useDefaultMessage());
		Assert.assertEquals(pojo.requiresReview(), result.requiresReview());
		Assert.assertEquals(pojo.getLastReviewed(), result.getLastReviewed());
	}

	@Test
	public void createVariable()
	{
		MessageVariablePojo pojo = new MessageVariablePojo(1, "Name", "Description");
		MessageVariable result = this.factory.createVariable(pojo);
		Assert.assertEquals(pojo.getMessageId(), result.getMessageId());
		Assert.assertEquals(pojo.getName(), result.getName());
		Assert.assertEquals(pojo.getDescription(), result.getDescription());
	}

	@Before
	public void setUp()
	{
		this.provider = Mockito.mock(NetworkCore.class);
		this.factory = new MessageFactoryImpl(this.provider);
	}
}
