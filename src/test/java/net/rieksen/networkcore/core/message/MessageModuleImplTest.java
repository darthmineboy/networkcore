package net.rieksen.networkcore.core.message;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import net.rieksen.networkcore.core.NetworkCore;

public class MessageModuleImplTest
{

	private NetworkCore			provider;
	private MessageModuleImpl	module;

	@Test
	public void getFactory()
	{
		Assert.assertNotNull(this.module.getFactory());
	}

	@Before
	public void setUp()
	{
		this.provider = Mockito.mock(NetworkCore.class);
		this.module = new MessageModuleImpl(this.provider);
	}
}
