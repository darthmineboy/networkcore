package net.rieksen.networkcore.core.message;

import org.junit.Test;
import org.meanbean.test.BeanTester;

import net.rieksen.networkcore.core.message.pojo.GlobalMessageVariablePojo;
import net.rieksen.networkcore.core.message.pojo.LanguagePojo;
import net.rieksen.networkcore.core.message.pojo.LocaleLanguagePojo;
import net.rieksen.networkcore.core.message.pojo.MessagePojo;
import net.rieksen.networkcore.core.message.pojo.MessageSectionPojo;
import net.rieksen.networkcore.core.message.pojo.MessageTranslationPojo;
import net.rieksen.networkcore.core.message.pojo.MessageVariablePojo;

public class TestPojo
{

	@Test
	public void testGlobalMessageVariable()
	{
		new BeanTester().testBean(GlobalMessageVariablePojo.class);
	}

	@Test
	public void testLanguage()
	{
		new BeanTester().testBean(LanguagePojo.class);
	}

	@Test
	public void testLocaleLanguage()
	{
		new BeanTester().testBean(LocaleLanguagePojo.class);
	}

	@Test
	public void testMessage()
	{
		new BeanTester().testBean(MessagePojo.class);
	}

	@Test
	public void testMessageSection()
	{
		new BeanTester().testBean(MessageSectionPojo.class);
	}

	@Test
	public void testMessageTranslation()
	{
		new BeanTester().testBean(MessageTranslationPojo.class);
	}

	@Test
	public void testMessageVariable()
	{
		new BeanTester().testBean(MessageVariablePojo.class);
	}
}
