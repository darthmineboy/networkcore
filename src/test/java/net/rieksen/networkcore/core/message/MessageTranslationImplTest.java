package net.rieksen.networkcore.core.message;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.message.pojo.MessageTranslationPojo;
import net.rieksen.networkcore.core.util.TestUtil;

public class MessageTranslationImplTest
{

	private NetworkCore				provider;
	private MessageTranslationPojo	pojo;
	private MessageTranslation		translation;

	@Before
	public void setUp()
	{
		this.provider = Mockito.mock(NetworkCore.class);
		this.pojo =
			new MessageTranslationPojo(1, 2, "Message", MessageType.JSON, "Default message", MessageType.JSON, false, false, new Date());
		this.translation = new MessageTranslationImpl(this.provider, this.pojo);
	}

	@Test
	public void toPojo()
	{
		MessageTranslationPojo result = this.translation.toPojo();
		TestUtil.assertPojoEquals(this.pojo, result);
	}
}
