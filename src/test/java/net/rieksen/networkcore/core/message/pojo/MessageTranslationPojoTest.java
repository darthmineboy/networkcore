package net.rieksen.networkcore.core.message.pojo;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;
import net.rieksen.networkcore.core.message.MessageType;

public class MessageTranslationPojoTest
{

	private MessageTranslationPojo pojo;

	@Test
	public void requiresReview()
	{
		boolean reverse = !this.pojo.requiresReview();
		this.pojo.requiresReview(reverse);
		Assert.assertEquals(reverse, this.pojo.requiresReview());
	}

	@Before
	public void setUp()
	{
		this.pojo =
			new MessageTranslationPojo(1, 2, "Hello", MessageType.REGULAR, "Default", MessageType.REGULAR, false, false, new Date());
	}

	@Test
	public void useDefaultMessage()
	{
		boolean reverse = !this.pojo.useDefaultMessage();
		this.pojo.useDefaultMessage(reverse);
		Assert.assertEquals(reverse, this.pojo.useDefaultMessage());
	}
}
