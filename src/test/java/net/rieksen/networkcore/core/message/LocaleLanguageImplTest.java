package net.rieksen.networkcore.core.message;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import junit.framework.Assert;
import net.rieksen.networkcore.core.NetworkCore;
import net.rieksen.networkcore.core.dao.DAOManager;
import net.rieksen.networkcore.core.dao.MessageDAO;
import net.rieksen.networkcore.core.message.pojo.LocaleLanguagePojo;
import net.rieksen.networkcore.core.util.TestUtil;

public class LocaleLanguageImplTest
{

	private NetworkCore			provider;
	private LocaleLanguagePojo	pojo;
	private LocaleLanguageImpl	locale;

	@Test
	public void changeLanguageId()
	{
		MessageDAO messageDAO = Mockito.mock(MessageDAO.class);
		DAOManager dao = Mockito.mock(DAOManager.class);
		Mockito.when(this.provider.getDAO()).thenReturn(dao);
		Mockito.when(dao.getMessageDAO()).thenReturn(messageDAO);
		this.pojo.setLanguageId(5);
		this.locale.changeLanguageId(5);
		Mockito.verify(messageDAO).updateLocaleLanguage(this.pojo);
	}

	@Test
	public void changeLanguageId_null()
	{
		MessageDAO messageDAO = Mockito.mock(MessageDAO.class);
		DAOManager dao = Mockito.mock(DAOManager.class);
		Mockito.when(this.provider.getDAO()).thenReturn(dao);
		Mockito.when(dao.getMessageDAO()).thenReturn(messageDAO);
		this.pojo.setLanguageId(null);
		this.locale.changeLanguageId(null);
		Mockito.verify(messageDAO).updateLocaleLanguage(this.pojo);
	}

	@Test
	public void changeLanguageId_same()
	{
		MessageDAO messageDAO = Mockito.mock(MessageDAO.class);
		DAOManager dao = Mockito.mock(DAOManager.class);
		Mockito.when(this.provider.getDAO()).thenReturn(dao);
		Mockito.when(dao.getMessageDAO()).thenReturn(messageDAO);
		this.locale.changeLanguageId(this.locale.getLanguageId());
		Mockito.verify(messageDAO, Mockito.never()).updateLocaleLanguage(Matchers.any());
	}

	@Test
	public void constructor()
	{
		this.locale = new LocaleLanguageImpl(this.provider, "de_DE", "German", 2);
		Assert.assertEquals("de_DE", this.locale.getLocaleCode());
		Assert.assertEquals("German", this.locale.getLanguageName());
		Assert.assertEquals(2, (int) this.locale.getLanguageId());
	}

	@Test
	public void getLanguage()
	{
		MessageModule module = Mockito.mock(MessageModule.class);
		Mockito.when(this.provider.getMessageModule()).thenReturn(module);
		Language language = Mockito.mock(Language.class);
		Mockito.when(module.getLanguage(Matchers.anyInt())).thenReturn(language);
		Language result = this.locale.getLanguage();
		Mockito.verify(module).getLanguage(this.locale.getLanguageId());
		Assert.assertEquals("Expected language to be returned from MessageModule", language, result);
	}

	@Test
	public void getLanguage_null()
	{
		MessageModule module = Mockito.mock(MessageModule.class);
		Mockito.when(this.provider.getMessageModule()).thenReturn(module);
		this.pojo.setLanguageId(null);
		this.locale = new LocaleLanguageImpl(this.provider, this.pojo);
		Language language = this.locale.getLanguage();
		Assert.assertNull(language);
		Mockito.verify(module, Mockito.never()).getLanguage(Matchers.any());
	}

	@Test
	public void setPojo()
	{
		this.pojo = new LocaleLanguagePojo("de_DE", "German", 2);
		this.locale.setPojo(this.pojo);
		LocaleLanguagePojo result = this.locale.toPojo();
		TestUtil.assertPojoEquals(this.pojo, result);
	}

	@Before
	public void setUp()
	{
		this.provider = Mockito.mock(NetworkCore.class);
		this.pojo = new LocaleLanguagePojo("en_EN", "English", 1);
		this.locale = Mockito.spy(new LocaleLanguageImpl(this.provider, this.pojo));
	}

	@Test
	public void toPojo()
	{
		LocaleLanguagePojo result = this.locale.toPojo();
		TestUtil.assertPojoEquals(this.pojo, result);
	}
}
