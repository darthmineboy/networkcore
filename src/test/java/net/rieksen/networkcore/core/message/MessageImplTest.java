package net.rieksen.networkcore.core.message;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import junit.framework.Assert;
import net.rieksen.networkcore.core.TestNetworkCore;
import net.rieksen.networkcore.core.message.pojo.MessagePojo;

public class MessageImplTest
{

	private TestNetworkCore	provider;
	private MessageImpl		message;

	@Test
	public void createTranslation_languageAndMessage()
	{
		MessageTranslation translation = Mockito.mock(MessageTranslation.class);
		Mockito.doReturn(translation).when(this.message).createTranslation(Matchers.any());
		Assert.assertEquals(translation, this.message.createTranslation(1, "Test message"));
		Mockito.verify(this.message).createTranslation(Matchers.any());
	}

	@Before
	public void setUp()
	{
		this.provider = Mockito.mock(TestNetworkCore.class);
		this.message = Mockito.spy(new MessageImpl(this.provider, new MessagePojo(1, 1, "Test message", "Test description")));
	}
}
