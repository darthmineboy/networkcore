package net.rieksen.networkcore.core.socket;

import org.bukkit.ChatColor;
import org.junit.Test;

import com.google.gson.Gson;

import junit.framework.Assert;

public class GsonTest
{

	@Test
	public void testColoredMessage()
	{
		String msg = ChatColor.translateAlternateColorCodes('&', "&cColor&dFest");
		Message message = new Message(msg);
		String gson = new Gson().toJson(message);
		Message output = new Gson().fromJson(gson, Message.class);
		Assert.assertEquals(msg, output.message);
	}

	static class Message
	{

		private String message;

		public Message(String message)
		{
			this.message = message;
		}
	}
}
