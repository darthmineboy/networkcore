package net.rieksen.networkcore.core.socket;

import org.bukkit.ChatColor;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import junit.framework.Assert;
import net.rieksen.networkcore.core.NetworkCore;

public class TestServerSocketModuleImpl
{

	private ServerSocketModuleImpl mod;

	@Before
	public void setUp()
	{
		this.mod = new ServerSocketModuleImpl(Mockito.mock(NetworkCore.class));
	}

	@Test
	public void testEncryption()
	{
		String message = "My message is not long";
		this.mod.setEncryptionSecret("8BPQ6orQP4xRz89xM2oeJQ==");
		byte[] messageBytes = message.getBytes();
		byte[] encrypted = this.mod.encrypt(messageBytes);
		byte[] decrypted = this.mod.decrypt(encrypted);
		String decryptedMsg = new String(decrypted);
		Assert.assertEquals(message, decryptedMsg);
	}

	@Test
	public void testEncryptionWithChatColors()
	{
		String message = ChatColor.translateAlternateColorCodes('&', "&cMy message &dis not long");
		this.mod.setEncryptionSecret("8BPQ6orQP4xRz89xM2oeJQ==");
		byte[] messageBytes = message.getBytes();
		byte[] encrypted = this.mod.encrypt(messageBytes);
		byte[] decrypted = this.mod.decrypt(encrypted);
		String decryptedMsg = new String(decrypted);
		Assert.assertEquals(message, decryptedMsg);
	}
}
